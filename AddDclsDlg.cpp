// UpdateRandTreeData2.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "AddDclsDlg.h"
#include "MDITabbedView.h"
#include "DCLSTreeReportView.h"

#include "ResLangFileReader.h"

// CAddDclsDlg dialog

IMPLEMENT_DYNAMIC(CAddDclsDlg, CDialog)

BEGIN_MESSAGE_MAP(CAddDclsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAddDclsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CAddDclsDlg::OnBnClickedButton1)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_REPORT, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnReportItemValueClick)
	ON_BN_CLICKED(IDC_BUTTON2, &CAddDclsDlg::OnBnClickedButton2)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

CAddDclsDlg::CAddDclsDlg(vecTransactionSpecies &vecSpecies,
						 vecTransactionPlot &vecTraktPlot,
						 CUMEstimateDB *pDB,
						 int nTraktID,
						 int nPage,
						 CWnd* pParent /*=NULL*/)
	: CDialog(CAddDclsDlg::IDD, pParent)
{
	m_yta=-1;
	m_from_d_class=0.0;
	m_tr�dslag=_T("");
	m_vecSpecies = vecSpecies;
	m_vecTraktPlot = vecTraktPlot;
	m_pDB = pDB;
	m_nTraktID = nTraktID;
	m_nPage = nPage;
}

CAddDclsDlg::~CAddDclsDlg()
{
}

void CAddDclsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_BUTTON1, m_wndButton1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndButton2);
	DDX_Control(pDX, IDCANCEL, m_wndButtonCancel);
	DDX_Control(pDX, IDOK, m_wndButtonOk);
}

INT_PTR CAddDclsDlg::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

void CAddDclsDlg::myOnSize()
{

	CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport, 10, 35, rect.right-20, rect.bottom-100, TRUE);
	setResize(&m_wndButton1, 10, 10, 70, 20, TRUE);
	setResize(&m_wndButton2, 84, 10, 70, 20, TRUE);
	setResize(&m_wndButtonOk, 160, rect.bottom-40, 70, 23, TRUE);
	setResize(&m_wndButtonCancel, 250, rect.bottom-40, 70, 23, TRUE);
	Invalidate();
}

void CAddDclsDlg::OnGetMinMaxInfo(MINMAXINFO * lpMMI)
{
	lpMMI->ptMinTrackSize.x = 600;
	lpMMI->ptMinTrackSize.y = 300;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CAddDclsDlg::OnSize(UINT nType,int cx,int cy)
{
	myOnSize();
	CDialog::OnSize(nType,cx,cy);
}


BOOL CAddDclsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	RLFReader xml;
	if (fileExists(m_sLangFN))
	{
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING8002)));
			m_wndButton1.SetWindowText(xml.str(IDS_STRING8003));
			m_wndButton2.SetWindowText(xml.str(IDS_STRING8008));	
			m_sErrNoSpecieSet = xml.str(IDS_STRING8005);
			m_sErrNoDclsSet = xml.str(IDS_STRING8006);
			m_sErrNoNumberSet = xml.str(IDS_STRING8007);

			m_sarrInventoryMethods.Add(_T("<") + xml.str(IDS_STRING3110) + _T(">"));
			m_sarrInventoryMethods.Add(xml.str(IDS_STRING3111));
			m_sarrInventoryMethods.Add(xml.str(IDS_STRING3112));
			m_sarrInventoryMethods.Add(xml.str(IDS_STRING3113));
			m_sarrInventoryMethods.Add(xml.str(IDS_STRING3114));
			m_sarrInventoryMethods.Add(xml.str(IDS_STRING31141));
		}
	}

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		CXTPReportColumn *pCol = NULL;

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
		m_wndReport.GetReportHeader()->SetAutoColumnSizing( FALSE );
		m_wndReport.EnableScrollBar(SB_HORZ, TRUE );
		m_wndReport.EnableScrollBar(SB_VERT, TRUE );
		m_wndReport.ShowWindow( SW_NORMAL );
		m_wndReport.FocusSubItems(TRUE);


		int inventoryMethod=0;
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView != NULL)
		{
			CPageThreeFormView *pView = pTabView->getPageThreeFormView();
			if (pView != NULL)
			{
				inventoryMethod=pView->getSelectedInvMethodIndex();
			}
		}


		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (m_sarrInventoryMethods.GetAt(inventoryMethod)), 80));
		pCol->AllowRemove(FALSE);
		if (inventoryMethod != 1) pCol->GetEditOptions()->m_bAllowEdit = TRUE; else pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		//pCol->GetEditOptions()->AddExpandButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING263)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING264)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING2720)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING2721)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE; //Edit not allowed #2011
		pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING266)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml.str(IDS_STRING1351)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml.str(IDS_STRING267)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_8, (xml.str(IDS_STRING268)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_9, (xml.str(IDS_STRING269)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_10, (xml.str(IDS_STRING270)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_11, (xml.str(IDS_STRING2700)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_12, (xml.str(IDS_STRING271)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_13, (xml.str(IDS_STRING273)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_14, (xml.str(IDS_STRING274)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_15, (xml.str(IDS_STRING1352)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetVisible(FALSE);

		CXTPReportColumn *pSpcCol = m_wndReport.GetColumns()->Find(COLUMN_2);
		if (pSpcCol != NULL)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
		}

		m_wndReport.SetMultipleSelection( FALSE );	//#4607 Ska bara kunna markera en rad i taget
		m_wndReport.AllowEdit(TRUE);
		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
	}

	m_sAddDclsFailed = xml.str(IDS_STRING8004);
	m_sDCLSErrorMsg5 = xml.str(IDS_STRING3244);
	m_sDCLSErrorMsg6 = xml.str(IDS_STRING3245);

	CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport,10,35,rect.right-20,350,TRUE);

	xml.clean();

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	myOnSize();

	return TRUE;
}

void CAddDclsDlg::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportRow* pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_yta=pRec->getColumnInt(COLUMN_0);
			m_from_d_class= pRec->getColumnFloat(COLUMN_3); //used to temporarly store the value
			m_tr�dslag = pRec->getColumnText(COLUMN_2);
		}
	}
}

void CAddDclsDlg::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CString sMsg;
	int nIndex = -1;
	int nAddNumOfRandTrees = 0;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
		if (pRecItem != NULL)
		{
			CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)pRecItem->GetRecord();
			if (pRec != NULL)
			{
				// Set specie id in Tr�dID column; 070515 p�d		
				CXTPReportColumn *pColumn2 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_2);
				if (pColumn2 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn2);
						if (pEdOptions != NULL)
						{
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pColumn2));
							if (pConst != NULL)
							{
								pRec->setColumnInt(COLUMN_1,(int)pConst->m_dwData);
								// User changed specie; 070521 p�d
								//m_bIsDirty = TRUE;
							}	// if (pConst != NULL)
						}	// if (pEdOptions != NULL)
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
				}	// if (pColumn2 != NULL)

				// Set the "to d.class" depending on the "from d.class" and "diameter setting"		
				//#2011
				CXTPReportColumn *pColumn3 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_3);
				CXTPReportColumn *pColumn0 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_0);
				if (pColumn3 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn3->GetIndex() ||
						pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex() ||
						pItemNotify->pColumn->GetIndex() == pColumn0->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn3);
						int currentRowIndex=pRec->GetIndex();
						if (pEdOptions != NULL && currentRowIndex>=0)
						{
							CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
							double diamClass=0.0, fromDClass=pRec->getColumnFloat(COLUMN_3), toDClass=0.0;
							
							int yta=pRec->getColumnInt(COLUMN_0);
							CString tr�dslag=pRec->getColumnText(COLUMN_2);
							bool overlappingValue=false;
							if (pFrame != NULL)
							{
								diamClass=pFrame->getDiamClass();
							}

							toDClass=fromDClass+diamClass;

							CXTPReportRecords *records=m_wndReport.GetRecords();
							double rowFromDClass=0.0, rowToDClass=0.0;
							CString rowTr�dslag;
							int rowYta;

							for(int i=0;i<records->GetCount();i++)
							{ //check the records if the value is overlapping any existing.
								CTraktDCLSTreeReportRec *pRowRec = (CTraktDCLSTreeReportRec*)records->GetAt(i);

								rowFromDClass=pRowRec->getColumnFloat(COLUMN_3);
								rowToDClass=pRowRec->getColumnFloat(COLUMN_4);
								rowTr�dslag=pRowRec->getColumnText(COLUMN_2);
								rowYta=pRowRec->getColumnInt(COLUMN_0);
								
								//Do not compare with the value that is in the same cells
								if(i!=currentRowIndex)
								{
									//#4465 l�t den h�r kontrollen vara kvar, ska inte g� att l�gga till 2 rader med samma yta, tr�dslag och diameterklass									
									//check the from d.klass och to d.klass value so it does not overlapp any other if it is the same tree-class.
									if((!tr�dslag.IsEmpty() && tr�dslag.CompareNoCase(rowTr�dslag)==0) && (yta == rowYta) && ((fromDClass>=rowFromDClass && fromDClass<rowToDClass) ||
										(toDClass>rowFromDClass && toDClass<=rowToDClass)))
									{ 
										overlappingValue=true;
										break;
									}
								}
							}

							CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
							if (!overlappingValue && pTabView != NULL)	//#4465 on�digt att g�ra kontrollen nedan om overlappingValue=true
							{
								CPageThreeFormView *pView = pTabView->getPageThreeFormView();
								if (pView != NULL)
								{
									if (m_nPage == 0)
										records = pView->getDCLSReport()->GetRecords();
									else if (m_nPage == 1)
										records = pView->getDCLS1Report()->GetRecords();

									for(int i=0;i<records->GetCount();i++)
									{ //check the records if the value is overlapping any existing.
										CTraktDCLSTreeReportRec *pRowRec = (CTraktDCLSTreeReportRec*)records->GetAt(i);

										rowFromDClass=pRowRec->getColumnFloat(COLUMN_3);
										rowToDClass=pRowRec->getColumnFloat(COLUMN_4);
										rowTr�dslag=pRowRec->getColumnText(COLUMN_2);
										rowYta=pRowRec->getColumnInt(COLUMN_0);

										//check the from d.klass och to d.klass value so it does not overlapp any other if it is the same tree-class.
										if((!tr�dslag.IsEmpty() && tr�dslag.CompareNoCase(rowTr�dslag)==0) && (yta == rowYta))
										{
											if(fromDClass==rowFromDClass && toDClass==rowToDClass)
											{
												//#4465 till�tet att l�gga till tr�d i redan existerande klass
												break;
											}
											else if((fromDClass>=rowFromDClass && fromDClass<rowToDClass) ||
												(toDClass>rowFromDClass && toDClass<=rowToDClass))
											{ 
												overlappingValue=true;
												break;
											}	
										}
									}
								}
							}

							if(toDClass>MAX_TO_D_CLASS || overlappingValue)
							{
								CString sMsg;

								if(overlappingValue)
								{
									sMsg.Format(m_sDCLSErrorMsg6,tr�dslag, (float)fromDClass, (float)toDClass,(float)rowFromDClass,(float)rowToDClass);
									pRec->setColumnFloat(COLUMN_4,toDClass);
								}
								else
								{
									sMsg.Format(m_sDCLSErrorMsg5,MAX_TO_D_CLASS);
								}
								//go back to the value that was there before the change
								pRec->setColumnFloat(COLUMN_3,m_from_d_class);
								pRec->setColumnFloat(COLUMN_4,m_from_d_class+diamClass);
								pRec->setColumnText(COLUMN_2,m_tr�dslag);
								pRec->setColumnInt(COLUMN_0,m_yta);
								UMMessageBox(sMsg,MB_ICONEXCLAMATION | MB_OK);
							}
							else
							{										
								pRec->setColumnFloat(COLUMN_4,toDClass);
							}		
						}
					}	
				}
			}
		}
	}
}


void CAddDclsDlg::OnBnClickedOk()
{
	CTraktDCLSTreeReportRec *pRecord;
	CString sErrMsg, sRow;
	bool bClassExists = false;

	for (UINT i=0; i<m_wndReport.GetRows()->GetCount(); i++)
	{
		pRecord = (CTraktDCLSTreeReportRec *)m_wndReport.GetRows()->GetAt(i)->GetRecord();

		sRow.Format(_T(" %d"), i+1);

		if (pRecord->getColumnInt(COLUMN_1) <= 0)
			sErrMsg += m_sErrNoSpecieSet + sRow + _T("\n");

		if (pRecord->getColumnFloat(COLUMN_3) <= 0 || pRecord->getColumnFloat(COLUMN_3) > MAX_TO_D_CLASS)
			sErrMsg += m_sErrNoDclsSet + sRow + _T("\n");
		
		if (pRecord->getColumnInt(COLUMN_6) <= 0)
			sErrMsg += m_sErrNoNumberSet + sRow + _T("\n");
	}

	if (sErrMsg.IsEmpty())
	{
		for (UINT i=0; i<m_wndReport.GetRecords()->GetCount(); i++)
		{
			pRecord = (CTraktDCLSTreeReportRec *)m_wndReport.GetRecords()->GetAt(i);

			int nMaxTreeID = -1;
			
			//#4465 kontrollera om diameterklassen redan finns, i s� fall ska antalet tr�d adderas till klassen annras ska en ny klass skapas
			//kontrollerna d� cellerna editeras ska ta hand om och se till att klassen inte �verlappar redan befintlig klass
			bClassExists = false;

			int yta=pRecord->getColumnInt(COLUMN_0);
			CString tr�dslag=pRecord->getColumnText(COLUMN_2);
			double fromDClass=pRecord->getColumnFloat(COLUMN_3);
			double toDClass=pRecord->getColumnFloat(COLUMN_4);		

			CXTPReportRecords *records=NULL;	
			double rowFromDClass=0.0, rowToDClass=0.0;
			CString rowTr�dslag;
			int rowYta;			

			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			if (pTabView != NULL)	
			{
				CPageThreeFormView *pView = pTabView->getPageThreeFormView();
				if (pView != NULL)
				{
					if (m_nPage == 0)
						records = pView->getDCLSReport()->GetRecords();
					else if (m_nPage == 1)
						records = pView->getDCLS1Report()->GetRecords();

					for(int i=0;i<records->GetCount();i++)
					{ 
						CTraktDCLSTreeReportRec *pRowRec = (CTraktDCLSTreeReportRec*)records->GetAt(i);

						rowFromDClass=pRowRec->getColumnFloat(COLUMN_3);
						rowToDClass=pRowRec->getColumnFloat(COLUMN_4);
						rowTr�dslag=pRowRec->getColumnText(COLUMN_2);
						rowYta=pRowRec->getColumnInt(COLUMN_0);

						if((!tr�dslag.IsEmpty() && tr�dslag.CompareNoCase(rowTr�dslag)==0) && (yta == rowYta))
						{
							if(fromDClass==rowFromDClass && toDClass==rowToDClass)
							{
								//#4465 till�tet att l�gga till tr�d i redan existerande klass
								bClassExists=true;
								nMaxTreeID = 0; 
								break;
							}
						}
					}
				}
			}

			if(!bClassExists)
			{
				if (m_nPage == 0)
				{
					nMaxTreeID = m_pDB->getMaxDCLSTreeID(m_nTraktID);	
				}
				else if (m_nPage == 1)	
				{
					nMaxTreeID = m_pDB->getMaxDCLS1TreeID(m_nTraktID);
				}
			}
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			int nRowCounter = nMaxTreeID + 1;

			CTransaction_dcls_tree data = CTransaction_dcls_tree(nRowCounter,
																									 m_nTraktID,
																									 pRecord->getColumnInt(COLUMN_0),
																									 pRecord->getColumnInt(COLUMN_1),
																									 pRecord->getColumnText(COLUMN_2),
																									 pRecord->getColumnFloat(COLUMN_3),	
																									 pRecord->getColumnFloat(COLUMN_4),	
																									 pRecord->getColumnFloat(COLUMN_5),
																									 pRecord->getColumnInt(COLUMN_6),
																									 pRecord->getColumnFloat(COLUMN_7),
																									 pRecord->getColumnFloat(COLUMN_8),	
																									 pRecord->getColumnFloat(COLUMN_9),	
																									 pRecord->getColumnFloat(COLUMN_10),
																									 pRecord->getColumnFloat(COLUMN_11),
																									 pRecord->getColumnFloat(COLUMN_12),
																									 pRecord->getColumnInt(COLUMN_13),	
																									 pRecord->getColumnInt(COLUMN_14),
																									 pRecord->getColumnInt(COLUMN_15),
																									 _T("") );

			if (m_nPage == 0)	//uttag
			{
				//#4465 kolla om �ka p� befintlig eller skapa ny
				if(bClassExists)
				{
					//klassen finns, �ka antalet tr�d
					if(!m_pDB->updIncNumOfToDclsUttag(data))
					{
						UMMessageBox(m_sAddDclsFailed);
					}
				}
				else
				{
					//l�gg till ny klass
					if(!m_pDB->addDCLSTrees(data))
					{
						UMMessageBox(m_sAddDclsFailed);
					}
				}
			}
			else if (m_nPage == 1)	//kvarl�mnat
			{
				//#4465 kolla om �ka p� befintlig eller skapa ny
				if(bClassExists)
				{
					//klassen finns, �ka antalet tr�d
					if(!m_pDB->updIncNumOfToDclsRemain(data))
					{
						UMMessageBox(m_sAddDclsFailed);
					}
				}
				else
				{
					//l�gg till ny klass
					if (!m_pDB->addDCLS1Trees(data))
					{
						UMMessageBox(m_sAddDclsFailed);
					}
				}
			}
		}

		OnOK();
	}
	else
	{
		UMMessageBox(sErrMsg, MB_ICONEXCLAMATION|MB_OK);
	}
}

void CAddDclsDlg::OnBnClickedButton1()
{
	CTraktDCLSTreeReportRec *pRec = new CTraktDCLSTreeReportRec;
	pRec->setColumnInt(0,1);
	pRec->setColumnInt(1,0);
	m_wndReport.AddRecord(pRec);
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CAddDclsDlg::OnBnClickedButton2()
{
	//#4607 kan ta bort markerad rad
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		m_wndReport.RemoveRowEx(pRow);
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}
