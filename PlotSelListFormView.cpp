// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"
#include "PlotSelListFormView.h"

#include "ResLangFileReader.h"

// CPlotSelListFormView

IMPLEMENT_DYNCREATE(CPlotSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPlotSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportValueChanged)
END_MESSAGE_MAP()

CPlotSelListFormView::CPlotSelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
}

CPlotSelListFormView::~CPlotSelListFormView()
{
}

void CPlotSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupReport();

//	LoadReportState();

	m_bIsDirty = FALSE;


}

BOOL CPlotSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CPlotSelListFormView::OnDestroy()
{
	// Always save Plots before closing; 070515 p�d
	savePlots();

	SaveReportState();

	setResetReportRecordSelected();

	if (m_pDB != NULL)
		delete m_pDB;

	m_vecTraktPlot.clear();

	// Try to clear records on exit (for memory deallocation); 080215 p�d
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		pRecs->RemoveAll();
	}

	CXTPReportView::OnDestroy();	
}

BOOL CPlotSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPlotSelListFormView diagnostics

#ifdef _DEBUG
void CPlotSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPlotSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPlotSelListFormView message handlers

// CPlotSelListFormView message handlers
void CPlotSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CPlotSelListFormView::OnSetFocus(CWnd*)
{
/*	Commented out 2008-02-14 p�d
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

LRESULT CPlotSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SAVE_ITEM :
		{
			savePlots();
			populateReport();
			break;
		}	// case ID_SAVE_ITEM :
	}	// switch (wParam)
	return 0L;
}

// Create and add Assortment settings reportwindow
BOOL CPlotSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				m_sPlot = xml->str(IDS_STRING6003);

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );
					m_nInvMethodIndex = getInvMethodIndex();
					// Setup columns in Report, depending on selected
					// Inverntory-method; 070514 p�d
					if (m_nInvMethodIndex == 1)	// "Totaltaxering"
					{
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
						pCol->AllowRemove(FALSE);
						pCol->GetEditOptions()->m_bAllowEdit = FALSE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 200));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING287)), 100));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					} 
					else if (m_nInvMethodIndex == 2 ||	// "Cirkelytor"
									 m_nInvMethodIndex == 4)		// "Snabbtaxering"
					{
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
						pCol->AllowRemove(FALSE);
						pCol->GetEditOptions()->m_bAllowEdit = FALSE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 150));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING283)), 50));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING284)), 50));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING287)), 100));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					}
					else if (m_nInvMethodIndex == 3)	// "Rektangul�ra ytor"
					{
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
						pCol->AllowRemove(FALSE);
						pCol->GetEditOptions()->m_bAllowEdit = FALSE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 150));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING283)), 50));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING285)), 50));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING286)), 50));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING287)), 100));
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					}

					populateReport();			

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(TRUE);

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPlotSelListFormView::populateReport(void)
{
	int nInvMethodIndex = getInvMethodIndex();
	CTransaction_plot data;
	BOOL bEnable = TRUE;
	GetReportCtrl().GetRecords()->RemoveAll();
	getPlots();
	if (m_vecTraktPlot.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktPlot.size();i++)
		{
			CTransaction_plot data = m_vecTraktPlot[i];
			if (data.getPlotType() == nInvMethodIndex)
			{
				GetReportCtrl().AddRecord(new CPlotSelListReportRec(data.getPlotID(),data.getPlotType(), data));
		
				// Criteria for enabling Toobar button AddPlot (group); 070515 p�d
				bEnable = (data.getPlotType() >= 1 && data.getPlotType() <= 5);
			}	// if (data.getPlotType() == nInvMethodIndex)
		}
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	// Depending on Inventory-method and if there's any plots (groups) for
	// this Trakt, set AddPlot Toolbar button; 070515 p�d
	CPlotSelListFrame *pFrame = (CPlotSelListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pFrame != NULL)
	{
		pFrame->setOnUpdateAddPlotTBBtnEnable(bEnable);
	}

}
// PUBLIC
void CPlotSelListFormView::addGroupToReport(CTransaction_plot &rec)
{
	int nNumOfRows = 0;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pRows != NULL)
	{
		nNumOfRows = pRows->GetCount();
	}
	if (m_nInvMethodIndex == 1)	// "Totaltaxering"
	{
		if (nNumOfRows == 0)
		{
			GetReportCtrl().AddRecord(new CPlotSelListReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nNumOfRows+1,
																												rec.getTraktID(),
																												m_sPlot,
																												m_nInvMethodIndex,
																												pRecord->getColumnFloat(2),
																												0.0,0.0,0.0,
																												_T(""),0,_T(""));
					GetReportCtrl().AddRecord(new CPlotSelListReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	else 	if (m_nInvMethodIndex == 2)	// "Cirkalytor"
	{
		if (nNumOfRows == 0)
		{
			GetReportCtrl().AddRecord(new CPlotSelListReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nNumOfRows+1,
																												rec.getTraktID(),
																												m_sPlot,
																												m_nInvMethodIndex,
																												pRecord->getColumnFloat(3),
																												0.0,0.0,
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					GetReportCtrl().AddRecord(new CPlotSelListReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	else 	if (m_nInvMethodIndex == 3)	// "Rektangul�ra ytor"
	{
		if (nNumOfRows == 0)
		{
			GetReportCtrl().AddRecord(new CPlotSelListReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);	
			if (pRow != NULL)
			{
				CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nNumOfRows+1,
																												rec.getTraktID(),
																												m_sPlot,
																												m_nInvMethodIndex,
																												0.0,
																												pRecord->getColumnFloat(3),
																												pRecord->getColumnFloat(4),
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					GetReportCtrl().AddRecord(new CPlotSelListReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d

	}
	else 	if (m_nInvMethodIndex == 4)	// "Snabbtaxering"
	{
		if (nNumOfRows == 0)
		{
			GetReportCtrl().AddRecord(new CPlotSelListReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nNumOfRows+1,
																												rec.getTraktID(),
																												m_sPlot,
																												m_nInvMethodIndex,
																												pRecord->getColumnFloat(3),
																												0.0,0.0,
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					GetReportCtrl().AddRecord(new CPlotSelListReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	// Save changes imdiately; 080318 p�d
	savePlots();

}

void CPlotSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// If user click inside report, set Toolbar button
	// enabling deletion of Group; 070515 p�d
	// Depending on Inventory-method and if there's any plots (groups) for
	// this Trakt, set AddPlot Toolbar button; 070515 p�d
	CPlotSelListFrame *pFrame = (CPlotSelListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pFrame != NULL)
	{
		pFrame->setOnUpdateDelPlotTBBtnEnable(TRUE);
	}

	// Also set selected data as GroupID
	CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
	if (pRecItem != NULL)
	{
		CPlotSelListReportRec *pRec = (CPlotSelListReportRec*)pRecItem->GetRecord();
		if (pRec != NULL)
		{
			setGroupID(pRec->getGroupID());
			setReportRecordGroupID(pRec->getGroupID());
			setGroupName(pRec->getColumnText(1));
		}
	}

}

void CPlotSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CPlotSelListFormView::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
  XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pRow)
	{
		CPlotSelListReportRec *pRec = (CPlotSelListReportRec*)pItemNotify->pItem->GetRecord();
		// Depending on Inventory method, calculate Area if user enters data
		// into fields for Radius,Length and Width; 070514 p�d
		if ((m_nInvMethodIndex == 2 && pItemNotify->pColumn->GetIndex() == 2) ||	// "Cirkelytor"
				(m_nInvMethodIndex == 4 && pItemNotify->pColumn->GetIndex() == 2))		// "Snabbtaxering"
		{
			double fArea = pRec->getColumnFloat(2);
			double fRadius = 0.0;
			if (fArea > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fRadius = sqrt(fArea/M_PI);

				pRec->setColumnFloat(3,fRadius);

				m_bIsDirty = TRUE;

			}
		}
		else if ((m_nInvMethodIndex == 2 && pItemNotify->pColumn->GetIndex() == 3) ||	// "Cirkelytor"
	  				 (m_nInvMethodIndex == 4 && pItemNotify->pColumn->GetIndex() == 3))		// "Snabbtaxering"
		{
			double fRadius = pRec->getColumnFloat(3);
			double fArea = 0.0;
			if (fRadius > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fArea = pow(fRadius,2) * M_PI;

				pRec->setColumnFloat(2,fArea);

				m_bIsDirty = TRUE;
			}
		}
		else if (m_nInvMethodIndex == 3 && pItemNotify->pColumn->GetIndex() > 2)	// "Rektangul�r yta"
		{
			double fArea = 0.0;
			double fLength = pRec->getColumnFloat(3);
			double fWidth = pRec->getColumnFloat(4);
			if (fWidth > 0.0 && fLength > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fArea = fLength*fWidth;
				pRec->setColumnFloat(2,fArea);
				m_bIsDirty = TRUE;
			}
		}
	}	// if (pItemNotify->pRow)
}

// CPlotSelListFormView message handlers

void CPlotSelListFormView::getPlots(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			m_bConnected = m_pDB->getPlots(pTrakt->getTraktID(),m_vecTraktPlot);
		}	// if (m_pDB != NULL)
	}
}

void CPlotSelListFormView::savePlots(void)
{
	int nInvMethod = -1;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CXTPReportRow *pRow = NULL;
	if (m_bConnected)	
	{
		// Make suew there's something to save; 070515 p�d
		if (pRows != NULL && m_pDB != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRow = pRows->GetAt(i);
				if (pRow != NULL)
				{
					CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRow->GetRecord();
					if (pRecord != NULL)
					{
						nInvMethod = pRecord->getRecord().getPlotType();
						// Name of Group'll always entered; 070515 p�d
						// Only value needed for InvMethods 1 and 5; 070515 p�d
						pRecord->getRecord().setPlotName(pRecord->getColumnText(1));
						// Based on Inventory method (PlotType), set CTransaction_plot record
						// accordingly. E.g. values set in Reportgrid, we need to get from grid.
						// Only information on TraktID,PlotID and InventoryMethod can be
						// read directory from pRecord->getRecord(); 070515 p�d
						if (nInvMethod == 1)	// "Totaltaxering"
						{
							/*
							pRecord->getRecord().setArea(0.0);
							pRecord->getRecord().setRadius(0.0);
							pRecord->getRecord().setLength1(0.0);
							pRecord->getRecord().setWidth1(0.0);
							*/
							pRecord->getRecord().setCoord(pRecord->getColumnText(2));
						}
						
						if (nInvMethod == 2 ||	// "Cirkelytor"
								nInvMethod == 4)		// "Snabbtaxering"
						{
							pRecord->getRecord().setArea(pRecord->getColumnFloat(2));
							pRecord->getRecord().setRadius(pRecord->getColumnFloat(3));
							pRecord->getRecord().setCoord(pRecord->getColumnText(4));
							/*
							pRecord->getRecord().setLength1(0.0);
							pRecord->getRecord().setWidth1(0.0);
							*/
						}
						else if (nInvMethod == 3)	// "Rektangul�r"
						{
							pRecord->getRecord().setArea(pRecord->getColumnFloat(2));
							pRecord->getRecord().setLength1(pRecord->getColumnFloat(3));
							pRecord->getRecord().setWidth1(pRecord->getColumnFloat(4));
							pRecord->getRecord().setCoord(pRecord->getColumnText(5));
							/*
							pRecord->getRecord().setRadius(0.0);
							*/
						}
//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
	CString sDateDone;
	CString sLangFN;
//UMMessageBox(_T("CPlotSelListFormView::savePlots 1"));

	if (m_pDB->checkTraktPlotDate(pRecord->getRecord(),sDateDone) == 0)
	{	
		sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		SecurityMessage(this->GetSafeHwnd(),sLangFN,sDateDone);
		return;
	}
#endif
					
						if (!m_pDB->addPlot(pRecord->getRecord()))
							m_pDB->updPlot(pRecord->getRecord());
/*
						if (m_bIsDirty)
						{
							runDoCalulationInPageThree();
						}
*/
					}	// if (pRecord != NULL)
				}	// if (pRow != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows != NULL)
	}
}

void CPlotSelListFormView::delPlot(void)
{
	CXTPReportRecordItem *pRecItem = GetReportCtrl().GetActiveItem();
	if (m_bConnected)	
	{
		if (pRecItem != NULL)
		{
			CPlotSelListReportRec *pRecord = (CPlotSelListReportRec *)pRecItem->GetRecord();
			if (pRecord != NULL && m_pDB != NULL)
			{
				m_pDB->delPlot(pRecord->getRecord());
			}	// if (pRecord != NULL && m_pDB != NULL)
		}	// if (pRecItem != NULL)
		// Refresh data in report; 070515 p�d
		populateReport();
	}
}

void CPlotSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PLOT_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CPlotSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PLOT_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

