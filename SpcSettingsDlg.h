#pragma once

#include "Resource.h"

#include "SpecieDataDialog.h"	// Holds Records; 091103 p�d

// CSpcSettingsDlg dialog

class CSpcSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CSpcSettingsDlg)
	
	bool	bHasCalculated;
	void	SetCalculated();

	CButton m_wndBtnAddTransfer;
	CButton m_wndBtnDelTransfer;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
	
	CXTResizeGroupBox m_wndGroup2;
	CXTResizeGroupBox m_wndGroup3;

	CString m_sCapMsg; 
	CString m_sMsgAssortmentMissing; 

	int m_nActiveTraktID;
	int m_nSpecieID;
	CString m_sSpecieName;

	//CMyPropertyGrid m_wndPropertyGrid;

	CMyReportCtrl m_wndReport_assort;
	CMyReportCtrl m_wndReport_trans;	// "�verf�ringar"

	BOOL m_bInitialized;
	BOOL m_bIsDirty;
	BOOL m_bIsFocus;

	CString m_sLangFN;

	enumACTION m_enumAction;

	void getTrakt(void);

	vecTransactionSpecies m_vecSpecies;

	vecTransactionTraktData m_vecTraktData;
	CTransaction_trakt_data m_recTraktData;

	void getTraktDataFromDB(void);
	void getTraktDataFromDB_specie(int trakt_id,int spc_id);
	void saveTraktDataToDB(void);

	// Get active trakt data from database; 071217 p�d
	CTransaction_trakt m_recTrakt;
	void getTraktFromDB(void);

	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(void);

	vecTransactionTraktAss m_vecTraktAss;
	CTransaction_trakt_ass m_recTraktAss;

	void getTraktAssFromDB(void);
	void getTraktAssFromDB_exclude(void);
	void saveTraktAssToDB(void);
	void delTraktAssFromDB(void);
	void resetTraktAssInDB(void);

	double getLastKrPerM3(CTransaction_trakt_ass rec);

	CTransaction_trakt_trans m_recTraktAssTrans;
	vecTransactionTraktTrans m_vecTraktAssTrans;

	void getTraktAssTrans(int trakt_id);
	void saveTraktAssTrans(void);
	void delTraktAssTrans(void);

	BOOL setupReport_assort(void);
	BOOL setupReport_trans(void);

	UINT m_nSelectedTraktDataIdx;


	BOOL isSpcUsed(int id);

	void populateData(void);

	BOOL getIsDirty(void);

	void exitViewAction(void);

	CAssortmentReportRec *m_pSelFromAssortInReport;
	CAssortmentReportRec *m_pSelToAssortInReport;

	int findFromAssortmentID(LPCTSTR assort_name);
	int findToAssortmentID(LPCTSTR assort_name);


	int m_nPriceSetIn;	// M3To or M3Fub: from Pricelist; 090615 p�d

	BOOL m_bAddOnlyPulp;
	CString m_sFromAssortment;

	void recalculateTransfers(void);
	void recalculateNoTransfers(void);


	// Data in esti_trakt_set_spc_table, settings for specie in Trakt; 070504 p�d
	vecTransactionTraktSetSpc m_vecTransactionTraktSetSpc;
	void getTraktSetSpc(void);

	// add_to = 0; add to both from and to
	// add_to = 1; add to from
	// add_to = 2; add to to based on selection made in from
	void addConstraintsToReport(short add_to = 0);

	vecTransactionAssort m_vecAssortmentsPerSpc;	// XML pricelist vector
	void getAssortmentsInPricelist(void);

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;

	short m_nToDoSetting;

	BOOL saveData(void);
public:
	CSpcSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	CSpcSettingsDlg(CString spc_name,int spc_id,int trakt_id,CUMEstimateDB* db = NULL);
	virtual ~CSpcSettingsDlg();

	virtual INT_PTR DoModal();

	// Added 2010-10-07 p�d
	// todo = 1 => Settings
	// todo = 2 => Assortments
	void whatToHandle(short todo)	{ m_nToDoSetting = todo; }

// Dialog Data
	enum { IDD = IDD_DIALOG6 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpcSettingsDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDISpecieDataFrame)
	afx_msg void OnReportAssortValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportTransValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportSelChanged(NMHDR* pNotifyStruct, LRESULT* result);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton61();
	afx_msg void OnBnClickedButton62();
	afx_msg void OnBnClickedOk();
};
