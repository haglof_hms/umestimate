// CategoryFormView.cpp : implementation file
//

#include "stdafx.h"
//#include "Forrest.h"
#include "TraktTypeFormView.h"

#include "ResLangFileReader.h"

// CTraktTypeFormView

IMPLEMENT_DYNCREATE(CTraktTypeFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTraktTypeFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CTraktTypeFormView::CTraktTypeFormView()
	: CXTResizeFormView(CTraktTypeFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CTraktTypeFormView::~CTraktTypeFormView()
{
}

void CTraktTypeFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecTraktTypeData.clear();

	m_wndReport1.ClearReport();
}

void CTraktTypeFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CTraktTypeFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CTraktTypeFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CTraktTypeFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL CTraktTypeFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CTraktTypeFormView diagnostics

#ifdef _DEBUG
void CTraktTypeFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CTraktTypeFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


void CTraktTypeFormView::doSetNavigationBar()
{
	if (m_vecTraktTypeData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

}

// CTraktTypeFormView message handlers

BOOL CTraktTypeFormView::getTraktTypes(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getTraktTypes(m_vecTraktTypeData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CTraktTypeFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CTraktTypeFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_TRAKT_TYPE_REPORT, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				m_sMsgCap	= xml->str(IDS_STRING151);
				m_sTraktTypeID	= xml->str(IDS_STRING152);
				m_sTraktType	= xml->str(IDS_STRING153);
				m_sTraktTypeNotes	= xml->str(IDS_STRING154);
				m_sOKBtn	= xml->str(IDS_STRING155);
				m_sCancelBtn	= xml->str(IDS_STRING156);
				m_sText1	= xml->str(IDS_STRING1570);
				m_sText2	= xml->str(IDS_STRING1580);
				m_sText3	= xml->str(IDS_STRING1590);
				m_sDoneSavingMsg = xml->str(IDS_STRING161);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (m_sTraktTypeID), 30));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->SetVisible( FALSE );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (m_sTraktType), 60));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, (m_sTraktTypeNotes), 120));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getTraktTypes();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 1,rect.bottom - 1);
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CTraktTypeFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CTraktTypeFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addTraktType();
			if (saveTraktType())
			{
				getTraktTypes();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveTraktType())
			{
				getTraktTypes();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getTraktTypes();
			if (removeTraktType())
			{
				getTraktTypes();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CTraktTypeFormView::populateReport(void)
{
	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecTraktTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktTypeData.size();i++)
		{
			CTransaction_trakt_type rec = m_vecTraktTypeData[i];
			m_wndReport1.AddRecord(new CTraktTypeReportRec(rec.getID(),rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL CTraktTypeFormView::addTraktType(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CTraktTypeReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

BOOL CTraktTypeFormView::saveTraktType(void)
{
	CTransaction_trakt_type rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CTraktTypeReportRec *pRec = (CTraktTypeReportRec *)pRecs->GetAt(i);

					rec = CTransaction_trakt_type(pRec->getColumnInt(0),
 										 										pRec->getColumnText(1),
											 									pRec->getColumnText(2),
																				_T(""));
					if (!m_pDB->addTraktType(rec))
						m_pDB->updTraktType(rec);
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			//populateReport();
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CTraktTypeFormView::removeTraktType(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_trakt_type data;
	CTraktTypeReportRec *pRec = NULL;
	CString sMsg;
	if (m_bConnected)	
	{
		pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CTraktTypeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				data = CTransaction_trakt_type(pRec->getID(),pRec->getColumnText(1),pRec->getColumnText(2),_T(""));
				if (m_pDB != NULL)
				{
					// Get Regions in database; 061002 p�d	
					if (!isTraktTypeUsed(data))
					{
						// Setup a message for user upon deleting machine; 061010 p�d
						sMsg.Format(_T("%s\n\n%s : %d\n%s : %s\n%s : %s\n\n%s\n\n%s"),
												m_sText1,
												m_sTraktTypeID,
												data.getID(),
												m_sTraktType,
												data.getTraktType(),
												m_sTraktTypeNotes,
												data.getNotes(),
												m_sText2,
												m_sText3);

						if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						{
							// Delete Machine and ALL underlying 
							//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
							m_pDB->removeTraktType(data);
						}	// if (messageDlg(m_sCaption,m_sOKBtn,m_sCancelBtn,sMsg))
					}	// if (!isMachineUsed(data))
				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}	// if (pRow)
		pRow = NULL;
		pRec = NULL;
		return TRUE;
	}
	return FALSE;
}

void CTraktTypeFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}

BOOL CTraktTypeFormView::isTraktTypeUsed(CTransaction_trakt_type &)
{
	return FALSE;
}
