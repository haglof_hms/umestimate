#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"

#include "PageThreeFormView.h"

/////////////////////////////////////////////////////////////////////////////
// CTraktDCLS2TreeReportRec

class CTraktDCLS2TreeReportRec : public CXTPReportRecord
{
	CTransaction_dcls_tree m_recTree;
protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	// Special for Combobox items
	class CComboBoxItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CComboBoxItem(CString sValue,int index) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTraktDCLS2TreeReportRec(int grp_id = -1,int spc_id = -1,LPCTSTR lang_fn = _T(""),HWND parent = NULL)
	{
		AddItem(new CIntItemInplaceBtn(grp_id,lang_fn,parent));	// Plot (Group) ID
		AddItem(new CIntItem(spc_id));					// 
		AddItem(new CTextItem(_T("")));					// Name of Specie (Combobox)
		AddItem(new CFloatItem(0.0,sz1dec));		// Diameter class from (cm)
		AddItem(new CFloatItem(0.0,sz1dec));		// Diameter class to (cm)
		AddItem(new CFloatItem(0.0,sz1dec));		// Height of tree
		AddItem(new CIntItem(0));								// Num of trees in diamterclass
		AddItem(new CFloatItem(0.0,sz0dec));		// Percent "Gr�nkrona"
		AddItem(new CFloatItem(0.0,sz1dec));		// Bark thickness mm
		AddItem(new CFloatItem(0.0,sz1dec));		// Volume m3sk
		AddItem(new CFloatItem(0.0,sz1dec));		// Volume m3fub
		AddItem(new CFloatItem(0.0,sz1dec));		// Volume m3ub
		AddItem(new CFloatItem(0.0,sz1dec));		// GROT kg
		AddItem(new CIntItem(0));								// Age
		AddItem(new CIntItem(0));								// Growth
	}

	CTraktDCLS2TreeReportRec(CTransaction_dcls_tree &rec,LPCTSTR lang_fn,HWND parent)
	{
		m_recTree = rec;	
		AddItem(new CIntItemInplaceBtn(rec.getPlotID(),lang_fn,parent));	// Plot (Group) ID
		AddItem(new CIntItem(rec.getSpcID()));								// Specie id
		AddItem(new CTextItem((rec.getSpcName())));					// Name of Specie (Combobox)
		AddItem(new CFloatItem(rec.getDCLS_from(),sz1dec));		// Diameterclass from (cm)
		AddItem(new CFloatItem(rec.getDCLS_to(),sz1dec));			// Diameterclass to	(cm)
		AddItem(new CFloatItem(rec.getHgt(),sz1dec));					// Height of tree
		AddItem(new CIntItem(rec.getNumOf()));								// Num of trees in diamterclass
		AddItem(new CFloatItem(rec.getGCrownPerc(),sz0dec));	// Percent "Gr�nkrona"
		AddItem(new CFloatItem(rec.getBarkThick(),sz1dec));		// Bark thickness mm
		AddItem(new CFloatItem(rec.getM3sk(),sz3dec));				// Volume m3sk
		AddItem(new CFloatItem(rec.getM3fub(),sz3dec));				// Volume m3fub
		AddItem(new CFloatItem(rec.getM3ub(),sz3dec));				// Volume m3ub
		AddItem(new CFloatItem(rec.getGROT(),sz1dec));				// GROT kg
		AddItem(new CIntItem(rec.getAge()));									// Age
		AddItem(new CIntItem(rec.getGrowth()));								// "Tillv�xt"
	}

	CTransaction_dcls_tree& getTreeRec(void)
	{
		return m_recTree;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item,int value)	
	{ 
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}
};

//////////////////////////////////////////////////////////////////////////////
// CDCLS2TreeReportView form view

class CDCLS2TreeReportView : public CMyReportView
{
	DECLARE_DYNCREATE(CDCLS2TreeReportView)

	double m_from_d_class;
	CString m_tr�dslag;
	BOOL m_bInitialized;
	CXTPReportSubListControl m_wndSubList;
	CString m_sFieldChooser;
	CString m_sPrintPreview;

	CString m_sMsgCap;
	CString m_sDCLSErrorMsg1;
	CString m_sDCLSErrorMsg2;
	CString m_sDCLSErrorMsg3;
	CString m_sDCLSErrorMsg4;
	CString m_sDCLSErrorMsg5;
	CString m_sDCLSErrorMsg6;
protected:
	CDCLS2TreeReportView();           // protected constructor used by dynamic creation
	virtual ~CDCLS2TreeReportView();

	vecTransactionPricelist m_vecPricelistData;
	BOOL setupReport(void);

	void LoadReportState(void);
	void SaveReportState(void);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:

	void doRunPrintPreview(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnPrintPreview();
	afx_msg void OnFilePrint();
	afx_msg void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
