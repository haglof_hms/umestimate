#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"

//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CTraktSelListFrame; 070108 p�d

class CTraktReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CTraktReportFilterEditControl)
public:
	CTraktReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CTraktSelListReportRec

class CTraktSelListReportRec : public CXTPReportRecord
{
	CTransaction_trakt recTrakt;
	UINT m_nIndex;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTraktSelListReportRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));					// Traktnumber
		AddItem(new CTextItem(_T("")));					// Trakt partnumber
		AddItem(new CTextItem(_T("")));					// Traktname
		AddItem(new CTextItem(_T("")));					// Fastighetsnamn
		AddItem(new CTextItem(_T("")));					// Fastighetsnummer
		AddItem(new CFloatItem(0.0,sz1dec));		// Areal
		AddItem(new CFloatItem(0.0,sz1dec));		// H�nsyn
		AddItem(new CFloatItem(0.0,sz1dec));		// �tg�rdat
		AddItem(new CIntItem(0));								// Hgt over sea
		AddItem(new CIntItem(0));								// Age
		AddItem(new CTextItem(_T("")));					// SI (H100)
		AddItem(new CTextItem(_T("")));					// Mapinfo
		AddItem(new CIntItem(0));								// Latitude
		AddItem(new CIntItem(0));								// Longitude
		AddItem(new CTextItem(_T("")));					// X
		AddItem(new CTextItem(_T("")));					// Y
		AddItem(new CTextItem(_T("")));					// Trakttype
		AddItem(new CTextItem(_T("")));					// Origin
		AddItem(new CTextItem(_T("")));					// GYL
		AddItem(new CTextItem(_T("")));					// Cut. class
		AddItem(new CFloatItem(0.0,sz1dec));		// Withdraw %
		AddItem(new CFloatItem(0.0,sz1dec));		// GROT
		AddItem(new CTextItem(_T("")));					// Hgt class
		AddItem(new CTextItem(_T("")));					// Input method
		AddItem(new CTextItem(_T("")));					// Created by
		AddItem(new CTextItem(_T("")));					// Date created
	}

	CTraktSelListReportRec(int idx,CTransaction_trakt& rec)
	{
		CString sGYL;
		m_nIndex = idx;
		recTrakt = rec;
		AddItem(new CTextItem((rec.getTraktNum())));									// Traktnumber
		AddItem(new CTextItem((rec.getTraktPNum())));									// Trakt partnumber
		AddItem(new CTextItem((rec.getTraktName())));									// Traktname
		AddItem(new CTextItem((rec.getTraktPropName())));							// Fastighetsnamn
		AddItem(new CTextItem((rec.getTraktPropNum())));							// Fastighetsnummer
		AddItem(new CFloatItem(rec.getTraktAreal(),sz1dec));						// Areal
		AddItem(new CFloatItem(rec.getTraktArealConsiderd(),sz1dec));		// H�nsyn
		AddItem(new CFloatItem(rec.getTraktArealHandled(),sz1dec));			// �tg�rdat
		AddItem(new CIntItem(rec.getTraktHgtOverSea()));								// Hgt over sea
		AddItem(new CIntItem(rec.getTraktAge()));												// Age
		AddItem(new CTextItem((rec.getTraktSIH100())));								// SI (H100)
		AddItem(new CTextItem((rec.getTraktMapdata())));							// Mapinfo
		AddItem(new CIntItem(rec.getTraktLatitude()));									// Latitude
		AddItem(new CIntItem(rec.getTraktLongitude()));									// Longitude
		AddItem(new CTextItem((rec.getTraktXCoord())));								// X
		AddItem(new CTextItem((rec.getTraktYCoord())));								// Y
		AddItem(new CTextItem((rec.getTraktType())));									// Trakttype
		AddItem(new CTextItem((rec.getTraktOrigin())));								// Origin
		sGYL.Format(_T("%d,%d,%d"),
								rec.getTraktGround(),
								rec.getTraktSurface(),
								rec.getTraktRake());
		AddItem(new CTextItem((sGYL)));																// GYL
		AddItem(new CTextItem((rec.getTraktCutClass())));							// Cut. class
		AddItem(new CFloatItem(rec.getTraktWithdraw(),sz1dec));					// Withdraw %
		AddItem(new CTextItem((rec.getTraktHgtClass())));							// Hgt class
		AddItem(new CTextItem((rec.getTraktInpMethod())));						// Input method
		AddItem(new CTextItem((rec.getTraktCreatedBy())));						// Created by
		AddItem(new CTextItem((rec.getTraktDate())));									// Date created
	}

	CTransaction_trakt& getRecord(void)
	{
		return recTrakt;
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

//////////////////////////////////////////////////////////////////////////////
// CTraktSelListFormView form view

class CTraktSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CTraktSelListFormView)

protected:
	CTraktSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CTraktSelListFormView();

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	vecTransactionTrakt m_vecTraktData;
	void getTrakt(void);

	BOOL setupReport(void);
	void populateReport(void);

	CXTPReportSubListControl m_wndSubList;
	CTraktReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setFilterWindow(void);
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
