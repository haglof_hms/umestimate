#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"


/////////////////////////////////////////////////////////////////////////////
// CPlotSelListReportRec

class CPlotSelListReportRec : public CXTPReportRecord
{
	CTransaction_plot recPlot;
	int m_nGroupID;
	int m_nInvMethodIndex;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CPlotSelListReportRec(void)
	{
		m_nGroupID = -1;
		m_nInvMethodIndex = -1;
		AddItem(new CIntItem(0));						// Plot (Group) ID
		AddItem(new CTextItem(_T("")));			// Inventory type
		AddItem(new CTextItem(_T("")));			// Name of Plot (Group)
	}

	CPlotSelListReportRec(int group_id,int inv_method_idx,CTransaction_plot& rec)
	{
		m_nGroupID = group_id;
		m_nInvMethodIndex = inv_method_idx;
		recPlot = rec;
		// Setup columns based on Inventory method; 070514 p�d
		if (m_nInvMethodIndex == 1)	// "Totaltaxering"
		{
			AddItem(new CIntItem((rec.getPlotID())));				// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));		// Name of Plot (Group)
			AddItem(new CTextItem(rec.getCoord()));						// Koordinater
		}
		else if (m_nInvMethodIndex == 2 ||	// "Cirkelytor"
						 m_nInvMethodIndex == 4)		// "Snabbtaxering"
		{
			AddItem(new CIntItem((rec.getPlotID())));					// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));			// Name of Plot (Group)
			AddItem(new CFloatItem(rec.getArea(),sz2dec ));			// Area (m2)
			AddItem(new CFloatItem(rec.getRadius(),sz1dec ));		// Radius (m)
			AddItem(new CTextItem(rec.getCoord()));							// Koordinater
		}
		else if (m_nInvMethodIndex == 3) // "Rektangul�ra"
		{
			AddItem(new CIntItem((rec.getPlotID())));						// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));				// Name of Plot (Group)
			AddItem(new CFloatItem(rec.getArea(),sz2dec ));				// Area (m2)
			AddItem(new CFloatItem(rec.getLength1(),sz2dec ));		// Length (m)
			AddItem(new CFloatItem(rec.getWidth1(),sz2dec ));			// Width (m)
			AddItem(new CTextItem(rec.getCoord()));								// Koordinater
		}
	}

	CTransaction_plot& getRecord(void)
	{
		return recPlot;
	}

	int getGroupID(void)
	{
		return m_nGroupID;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

//////////////////////////////////////////////////////////////////////////////
// CPlotSelListFormView form view

class CPlotSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CPlotSelListFormView)

	BOOL m_bIsDirty;

protected:
	CPlotSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CPlotSelListFormView();

	vecTransactionPlot m_vecTraktPlot;
	void getPlots(void);
	void savePlots(void);

	BOOL setupReport(void);
	void populateReport(void);

	int m_nInvMethodIndex;

	BOOL getIsDirty(void)
	{
		return m_bIsDirty;
	}

	void LoadReportState(void);
	void SaveReportState(void);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CString m_sPlot;
public:
	// Need to be Public
	void delPlot(void);

	void addGroupToReport(CTransaction_plot &rec);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
