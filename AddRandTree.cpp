// UpdateRandTreeData2.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "AddRandTree.h"
#include "MDITabbedView.h"

#include "ResLangFileReader.h"

// CAddRandTree dialog

IMPLEMENT_DYNAMIC(CAddRandTree, CDialog)

BEGIN_MESSAGE_MAP(CAddRandTree, CDialog)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnReportItemValChange)
	ON_BN_CLICKED(IDC_BUTTON_ADD_RANDTREE, &CAddRandTree::OnBnClickedButtonAddRandtree)
	ON_BN_CLICKED(IDOK, &CAddRandTree::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CAddRandTree::OnBnClickedCancel)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_RANDTREE, &CAddRandTree::OnBnClickedButtonRemoveRandtree)
END_MESSAGE_MAP()

CAddRandTree::CAddRandTree(int nTraktId,int nTaxeringsTyp,CStringArray &sarrInventoryMethods,
										   CStringArray &sarrTreeTypes,
										   vecTransactionSampleTreeCategory &vecSmpTreeCategories,
										   vecTransactionSpecies &vecSpecies,CTransaction_trakt_misc_data &recTraktMiscData,
										   CWnd* pParent /*=NULL*/)
	: CDialog(CAddRandTree::IDD, pParent)
{
	m_nTaxeringsTyp=nTaxeringsTyp;
	m_nTraktId=nTraktId;
	m_sarrInventoryMethods.Copy(sarrInventoryMethods);
	m_sarrTreeTypes.Copy(sarrTreeTypes);
	m_vecSmpTreeCategories = vecSmpTreeCategories;
	m_vecSpecies = vecSpecies;
	m_recTraktMiscData=recTraktMiscData;
}

CAddRandTree::~CAddRandTree()
{
}


void CAddRandTree::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_BUTTON_ADD_RANDTREE, m_wndButtonAddTree);
	DDX_Control(pDX, IDCANCEL, m_wndButtonCancel);
	DDX_Control(pDX, IDOK, m_wndButtonOk);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_RANDTREE, m_wndButtonRemoveTree);
	
}
INT_PTR CAddRandTree::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

void CAddRandTree::myOnSize()
{

	CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport			,10 ,35,rect.right-20,rect.bottom-100,TRUE);
	setResize(&m_wndButtonAddTree	,10	,10,70,20,TRUE);
	setResize(&m_wndButtonRemoveTree,90,10,70,20,TRUE);
	setResize(&m_wndButtonOk		,160,rect.bottom-40,70 ,23,TRUE);
	setResize(&m_wndButtonCancel	,250,rect.bottom-40,70 ,23,TRUE);
	Invalidate();
}

void CAddRandTree::OnGetMinMaxInfo(MINMAXINFO * lpMMI)
{
	lpMMI->ptMinTrackSize.x = 600;
	lpMMI->ptMinTrackSize.y = 300;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CAddRandTree::OnSize(UINT nType,int cx,int cy)
{

	/*CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport			,10 ,35,rect.right-20,rect.bottom-100,TRUE);
	setResize(&m_wndButtonAddTree	,10	,10,70,20,TRUE);
	setResize(&m_wndButtonOk		,160,rect.bottom-40,70 ,23,TRUE);
	setResize(&m_wndButtonCancel	,250,rect.bottom-40,70 ,23,TRUE);
	Invalidate();*/
	myOnSize();
	CDialog::OnSize(nType,cx,cy);
}

BOOL CAddRandTree::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	RLFReader xml;
	if (fileExists(m_sLangFN))
	{
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING8000)));
			m_wndButtonAddTree.SetWindowText(xml.str(IDS_STRING8001));
			m_wndButtonCancel.SetWindowTextW(xml.str(IDS_STRING3275));
			m_wndButtonRemoveTree.SetWindowText(xml.str(IDS_STRING8009));
		}
	}

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		CXTPReportColumn *pCol = NULL;

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
		m_wndReport.GetReportHeader()->SetAutoColumnSizing( FALSE );
		m_wndReport.EnableScrollBar(SB_HORZ, TRUE );
		m_wndReport.EnableScrollBar(SB_VERT, TRUE );
		m_wndReport.ShowWindow( SW_NORMAL );
		m_wndReport.FocusSubItems(TRUE);

		

		m_nYtNr=1;
		switch(m_nTaxeringsTyp)
		{
		case -1:
			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GROUP, (xml.str(IDS_STRING262)), 80));	//Grupp
			m_nYtNr=0;
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;
			break;
		case 1:
			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GROUP, m_sarrInventoryMethods.GetAt(m_nTaxeringsTyp), 80));	//Grupp/Ytnr
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;
			break;
		default:
			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GROUP, m_sarrInventoryMethods.GetAt(m_nTaxeringsTyp), 80));	//Grupp/Ytnr
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			break;
		}
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		//pCol->GetEditOptions()->AddExpandButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_TYPE, (xml.str(IDS_STRING275)), 150));	//Tr�dtyp
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_SPCID, (xml.str(IDS_STRING263)), 100));	// Tr�dId
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_SPCNAME, (xml.str(IDS_STRING264)), 100));	//Tr�dslag
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_DBH, (xml.str(IDS_STRING265)), 100));	//DBH
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_HGT, (xml.str(IDS_STRING266)), 100));	//H�jd
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_AGE, (xml.str(IDS_STRING273)), 100));	//�lder
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_BON, (xml.str(IDS_STRING276)), 120));	// H100
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_UPPERCASE; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_CAT, xml.str(IDS_STRING294), 100));	// _T("Kategori")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_FAS, xml.str(IDS_STRING295), 100));	// _T("Fasavst�nd")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_TOP, xml.str(IDS_STRING296), 120));	// _T("Toppningsv�rde")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GRK, (xml.str(IDS_STRING267)), 120));	//Gr�nkroneandel
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_BRK, (xml.str(IDS_STRING268)), 100));	//Enkel bark
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GROT, (xml.str(IDS_STRING271)), 120));	//Skogsbr�nsle ton ts
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 


		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_GROTWH, (xml.str(IDS_STRING274)), 100));	//Tillv�xt
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_FROM, (xml.str(IDS_STRING2720)), 100));	// dcls fr�n
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_ADD_TREE_TO, (xml.str(IDS_STRING2721)), 100)); //dcls till
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;







		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport.SetMultipleSelection( FALSE );
		m_wndReport.AllowEdit(TRUE);
		m_wndReport.GetColumns()->Find(COLUMN_ADD_TREE_GROUP)->SetTreeColumn(TRUE);

	}

	CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport,10,35,rect.right-20,350,TRUE);

	xml.clean();
/*
	for (UINT i = 0;i < m_vecTraktSampleTrees.size();i++)
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;

		pRec->AddItem(new CTextItem(_T("")))->HasCheckbox(TRUE);
		pRec->AddItem(new CIntItemInplaceBtn(m_vecTraktSampleTrees[i].getPlotID(),m_sLangFN,GetParent()->GetSafeHwnd()));	// Plot (Group) ID
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getSpcID()));								// Specioe id
		pRec->AddItem(new CTextItem((m_vecTraktSampleTrees[i].getSpcName())));					// Name of Specie (Combobox)
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDBH(),sz1dec));					// Diameter at breastheight
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getHgt(),sz1dec));					// Height of tree
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getGCrownPerc(),sz1dec));	// Percent "Gr�nkrona"
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getBarkThick(),sz1dec));		// Bark thickness mm
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getGROT(),sz1dec));				// GROT kg
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getAge()));									// Age
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getGrowth()));								// "Tillv�xt"
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDCLS_from(),sz1dec));		// Diameter class (cm)
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDCLS_to(),sz1dec));			// Diameter class (cm)

		if (m_vecTraktSampleTrees[i].getTreeType() >= 0 && m_vecTraktSampleTrees[i].getTreeType() < m_sarrTreeTypes.GetCount())
		{
			pRec->AddItem(new CTextItem(m_sarrTreeTypes.GetAt(m_vecTraktSampleTrees[i].getTreeType())));// Type tree (Provtr�d etc)
		}
		else if (m_sarrTreeTypes.GetCount() > 0)
		{
			pRec->AddItem(new CTextItem(m_sarrTreeTypes.GetAt(0)));// Type tree (Provtr�d etc)
		}
		pRec->AddItem(new CExTextH100Item((m_vecTraktSampleTrees[i].getBonitet())));					// H100 (m)

		if(m_vecTraktSampleTrees[i].getCategory() != -1)
			pRec->AddItem(new CTextItem( getValueForCategory(&m_vecSmpTreeCategories, m_vecTraktSampleTrees[i].getCategory()) ));					// Category
		else
			pRec->AddItem(new CTextItem(_T("")));

		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getDistCable()));								// Dist cable
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getTopCut()));								// Toppningsv�rde
		pRec->AddItem(new CTextItem( m_vecTraktSampleTrees[i].getCoord() ));					// Koordinat

		m_wndReport.AddRecord(pRec);
	}
*/
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	myOnSize();

	return TRUE;
}

void CAddRandTree::CAddRandTreeToReport(int yt_id,int spc_id)
{

	CXTPReportRecord *pRec = new CXTPReportRecord;
	
	addConstraintsToReport();

	pRec->AddItem(new CIntItem(yt_id));						// Plot (Group) ID
	pRec->AddItem(new CTextItem(TREE_TYPE));					// Type tree (Provtr�d etc)
	pRec->AddItem(new CIntItem(spc_id));						// Spcid
	pRec->AddItem(new CTextItem(_T("")));						// Name of Specie (Combobox)
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Diameter at breastheight
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Height of tree
	pRec->AddItem(new CIntItem(0));								// Age
	pRec->AddItem(new CTextItem(_T("")));						// Jonsson bonitet
	pRec->AddItem(new CTextItem(_T("")));						// Category
	pRec->AddItem(new CIntItem(0));								// Dist cable
	pRec->AddItem(new CIntItem(0));								// "Toppningsv�rde"	
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Percent "Gr�nkrona"
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Bark thickness mm
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// GROT kg
	pRec->AddItem(new CIntItem(0));								// Growth
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Diameter class (cm)
	pRec->AddItem(new CFloatItem(0.0,sz1dec));					// Diameter class (cm)

	m_wndReport.AddRecord(pRec);

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CAddRandTree::addConstraintsToReport(void)
{
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();	
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 2; 070509 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_ADD_TREE_SPCNAME);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)


		// Add tree types to combobox in Column_15; 070510 p�d
		CXTPReportColumn *pTreeTypesCol = pColumns->Find(COLUMN_ADD_TREE_TYPE);
		if (pTreeTypesCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pTreeTypesCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			for (int i = 0;i < m_sarrTreeTypes.GetCount();i++)
			{
				if (i != 7) // Skip Pos.tr�d
					pTreeTypesCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(i),i);
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
		}	// if (pSpcCol != NULL)

		// Add categories to Combobox in Column 17
		CXTPReportColumn *pCatCol = pColumns->Find(COLUMN_ADD_TREE_CAT);
		if (pCatCol != NULL)
		{
			// Get constraints for Category column and remove all items
			pCons = pCatCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)


			if (m_vecSmpTreeCategories.size() > 0)
			{
				pCatCol->GetEditOptions()->AddConstraint(_T(""), 0);
				for (UINT i = 0;i < m_vecSmpTreeCategories.size();i++)
				{
					pCatCol->GetEditOptions()->AddConstraint(m_vecSmpTreeCategories[i].getCategory(), m_vecSmpTreeCategories[i].getID());
				}
			}
		}
	}	// if (pColumns != NULL)
}

void CAddRandTree::OnReportItemValChange(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	int nIndex = -1, nTmp=-1, nPrevType=-1,nNewType=-1;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
		if (pRecItem != NULL)
		{
			// Set specie id in Tr�dID column; 070515 p�d		
			CXTPReportColumn *pColumn2 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_ADD_TREE_SPCNAME);
			if (pColumn2 != NULL)
			{
				if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
				{
					CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn2);
					if (pEdOptions != NULL)
					{
						CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pColumn2));
						if (pConst != NULL)
						{
							((CIntItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_SPCID))->setIntItem((int)pConst->m_dwData);
							//pRec->setColumnInt(COLUMN_ADD_TREE_SPCID,(int)pConst->m_dwData);
							//int nSpcId=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_SPCID)))->getIntItem(); 

						}	// if (pConst != NULL)
					}	// if (pEdOptions != NULL)
				}	// if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
			}	// if (pColumn2 != NULL)


			//Kolla diameter, uppdatera fr�n till klasser
			CXTPReportColumn *pColDia = pItemNotify->pColumn->GetColumns()->Find(COLUMN_ADD_TREE_DBH);
			if (pColumn2 != NULL)
			{
				if (pItemNotify->pColumn->GetIndex() == pColDia->GetIndex())
				{
					CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn2);
					if (pEdOptions != NULL)
					{
						float fDbh=0.0;
						fDbh=((CFloatItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_DBH))->getFloatItem();

						int nDcls_from_new = (int(fDbh) / 10) - ((int(fDbh) / 10) % int(m_recTraktMiscData.getDiamClass()));
						int nDcls_to_new = nDcls_from_new + m_recTraktMiscData.getDiamClass();


						((CFloatItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_FROM))->setFloatItem(nDcls_from_new);
						((CFloatItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_TO))->setFloatItem(nDcls_to_new);

					}	// if (pEdOptions != NULL)
				}	// if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
			}	// if (pColumn2 != NULL)

			// Enable editing height (if not a sample tree with calculated height)
			CXTPReportColumn *pColumn14 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_ADD_TREE_TYPE);
			if (pColumn14 != NULL)
			{
				int nTreeType=0;
				CString cTreeType=_T("");	

				if (pItemNotify->pColumn->GetIndex() == pColumn14->GetIndex())
				{
					BOOL editable = TRUE;
					int ct = pColumn14->GetEditOptions()->GetConstraints()->GetCount();
					for( int i=0; i < pColumn14->GetEditOptions()->GetConstraints()->GetCount(); i++ )
					{
						CString constraint = pColumn14->GetEditOptions()->GetConstraints()->GetAt(i)->m_strConstraint;
						cTreeType=((CTextItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_TYPE))->getTextItem();
						
						if( _tcscmp(pColumn14->GetEditOptions()->GetConstraints()->GetAt(i)->m_strConstraint, cTreeType /*pRec->getColumnText(COLUMN_15)*/) == 0 )
						{											
							nTreeType=getIndexForTreeType(cTreeType);	//#4609 20151005 J� M�ste plocka ut tr�dtyp mha namnet p� tr�dslag iom att i denna dialog saknas positionstr�d d� kan man inte g� p� index i dropdown

							if( nTreeType == TREE_OUTSIDE_NO_SAMP ||
								nTreeType == TREE_POS_NO_SAMP ||
								nTreeType == TREE_OUTSIDE_LEFT_NO_SAMP ||
								nTreeType == TREE_INSIDE_LEFT_NO_SAMP)
							{
								editable = FALSE;
							}
							break;
						}
					}
					((CFloatItem*)pRecItem->GetRecord()->GetItem(COLUMN_ADD_TREE_HGT))->SetEditable(editable);
					//pRec->GetItem(COLUMN_ADD_TREE_HGT)->SetEditable(editable);
				}
			}
		}	// if (pRecItem != NULL)
	}	// if (pItemNotify != NULL)

}

int CAddRandTree::getIndexForCategory(LPCTSTR value)
{
	// If there's species, add to Specie column in report; 070509 p�d
	for (int i = 0;i < m_vecSmpTreeCategories.size();i++)
	{
		if (_tcscmp(m_vecSmpTreeCategories[i].getCategory(),value) == 0)
			return m_vecSmpTreeCategories[i].getID();
	}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	return -1;
}

int CAddRandTree::getIndexForTreeType(LPCTSTR value)
{
	// If there's species, add to Specie column in report; 070509 p�d
	for (int i = 0;i < m_sarrTreeTypes.GetCount();i++)
	{
		if (_tcscmp(m_sarrTreeTypes.GetAt(i),value) == 0)
			return i;
	}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	return -1;
}

BOOL CAddRandTree::CheckData()
{
	BOOL bRet=FALSE;
	CString cMsg=_T("");
	CString cMsg2=_T("");
	CString cSpec=_T("");
	CXTPReportRows *pRows = m_wndReport.GetRows();
	//CXTPReportRecords *pRecs = m_wndReport.GetRecords();

	int nSpcId=-1;
	int nAge=-1;
	int nTreeType=-1;
	CString cBon=_T("");
	CString cTreeType=_T("");		
	float fHgt=-1;
	float fDbh=-1;
	
	for(UINT i=0; i<pRows->GetCount();i++)
	//for (UINT i=0; i<pRecs->GetCount(); i++)
	{	
		nSpcId = ((CIntItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_SPCID)))->getIntItem();
		fDbh=((CFloatItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_DBH)))->getFloatItem(); 
		fHgt=((CFloatItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_HGT)))->getFloatItem(); 
		nAge=((CIntItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_AGE)))->getIntItem(); 
		cBon=((CTextItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_BON)))->getTextItem(); 
		cSpec=((CTextItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_SPCNAME)))->getTextItem(); 
		cTreeType=((CTextItem*)(pRows->GetAt(i)->GetRecord()->GetItem(COLUMN_ADD_TREE_TYPE)))->getTextItem();
		nTreeType=getIndexForTreeType(cTreeType);
		/*
		nSpcId=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_SPCID)))->getIntItem(); 
		fDbh=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_DBH)))->getFloatItem(); 
		fHgt=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_HGT)))->getFloatItem(); 
		nAge=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_AGE)))->getIntItem(); 
		cBon=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_BON)))->getTextItem(); 
		cTreeType=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_TYPE)))->getTextItem();
		nTreeType=getIndexForTreeType(cTreeType);*/

		if(fDbh<=0.0)
		{
			cMsg.Format(_T("%s%d\n"),_T("Ingen diameter satt rad "),i+1);
			cMsg2+=cMsg;
		}

		if(cSpec.GetLength()<1)
		{
			cMsg.Format(_T("%s%d\n"),_T("Inget tr�dslag satt rad "),i+1);
			cMsg2+=cMsg;
		}
		

		switch(nTreeType)
		{
		case SAMPLE_TREE:		// A sampletree. I.e. it has a heigth etc
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_OUTSIDE:		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(nAge<=0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen �lder satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(cBon.GetLength()<3)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen bonitet satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_OUTSIDE_NO_SAMP:		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
			if(nAge<=0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen �lder satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(cBon.GetLength()<3)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen bonitet satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_SAMPLE_REMAINIG:		// "Provtr�d (Kvarl�mmnat)"
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_SAMPLE_EXTRA:		// "Provtr�d (Extra inl�sta)"
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_OUTSIDE_LEFT:	// Kanttr�d l�mnas (Provtr�d)
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(nAge<=0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen �lder satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(cBon.GetLength()<3)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen bonitet satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_OUTSIDE_LEFT_NO_SAMP:	// Kanttr�d l�mnas  (Ber�knad h�jd)
			if(nAge<=0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen �lder satt rad "),i+1);
				cMsg2+=cMsg;
			}
			if(cBon.GetLength()<3)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen bonitet satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_POS_NO_SAMP:	// Pos.tr�d (uttag)
			break;
		case TREE_INSIDE_LEFT:	// Tr�d i gatan l�mnas (Provtr�d)
			if(fHgt<=0.0)
			{
				cMsg.Format(_T("%s%d\n"),_T("Ingen h�jd satt rad "),i+1);
				cMsg2+=cMsg;
			}
			break;
		case TREE_INSIDE_LEFT_NO_SAMP:	// Tr�d i gatan l�mnas (Ber�knad h�jd)			
			break;
		default:
			cMsg.Format(_T("%s%d\n"),_T("Ingen tr�dtyp vald rad "),i+1);
			break;
		}


	}

	if(cMsg2.GetLength()>1)
	{
		if (UMMessageBox(this->GetSafeHwnd(),cMsg2,_T("Kontrollera ifylld data"),MB_ICONEXCLAMATION | MB_OK ))
		{
		}
	}
	else
		bRet=TRUE;
	return bRet;
}



void CAddRandTree::OnBnClickedButtonAddRandtree()
{
	CAddRandTreeToReport(m_nYtNr,0);
}



void CAddRandTree::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	int nPlotId=0;
	int nSpcId=-1;
	CString cSpcName=_T("");
	float fDbh=-1;
	float fHgt=-1;
	float fGrk=-1;
	float fBrk=-1;
	float fGrot=-1;
	int nAge=-1;
	int nGrowth=-1;
	float fDclFrom=-1;
	float fDclTo=-1;
	int nType=-1;
	CString cBon=_T("");
	int nCat=-1;
	CString cCat=_T("");
	int nFas=-1;
	int nTop=-1;
	CString cType=_T("");		



	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			CXTPReportRecords *pRecs = m_wndReport.GetRecords();
			if(pRecs->GetCount()>0)
			{
				if(CheckData())
				{
					for (UINT i=0; i<pRecs->GetCount(); i++)
					{
						nPlotId=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_GROUP)))->getIntItem(); 
						nSpcId=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_SPCID)))->getIntItem(); 
						cSpcName=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_SPCNAME)))->getTextItem();
						fDbh=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_DBH)))->getFloatItem(); 
						fHgt=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_HGT)))->getFloatItem(); 
						fGrk=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_GRK)))->getFloatItem(); 
						fBrk=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_BRK)))->getFloatItem(); 
						fGrot=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_GROT)))->getFloatItem(); 
						nAge=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_AGE)))->getIntItem(); 
						nGrowth=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_GROTWH)))->getIntItem(); 
						fDclFrom=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_FROM)))->getFloatItem(); 
						fDclTo=((CFloatItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_TO)))->getFloatItem(); 
						cType=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_TYPE)))->getTextItem();
						nType=getIndexForTreeType(cType);
						cBon=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_BON)))->getTextItem(); 
						cCat=((CTextItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_CAT)))->getTextItem(); 
						nCat=getIndexForCategory(cCat);
						nFas=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_FAS)))->getIntItem(); 
						nTop=((CIntItem*)(pRecs->GetAt(i)->GetItem(COLUMN_ADD_TREE_TOP)))->getIntItem(); 
						
						CTransaction_sample_tree data2 = CTransaction_sample_tree(-1
						,m_nTraktId
						,nPlotId
						,nSpcId
						,cSpcName
						,fDbh
						,fHgt
						,fGrk
						,fBrk
						,fGrot
						,nAge
						,nGrowth
						,fDclFrom
						,fDclTo
						,0.0
						,0.0
						,0.0
						,nType 
						,cBon
						,_T("")
						,_T("")
						,nFas
						,nCat
						,nTop);
						pView->doAddTree(data2);
					}
						OnOK();
				}
			}
		}
	}
}

void CAddRandTree::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

void CAddRandTree::OnBnClickedButtonRemoveRandtree()
{
	// TODO: Add your control notification handler code here
	//#4652 kan ta bort markerad rad
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		m_wndReport.RemoveRowEx(pRow);
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}
