#include "stdafx.h"
#include "Resource.h"

#include "CutClassFrame.h"
#include "CutClassFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CMDICutClassFrame

IMPLEMENT_DYNCREATE(CMDICutClassFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICutClassFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICutClassFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICutClassFrame::CMDICutClassFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
}

CMDICutClassFrame::~CMDICutClassFrame()
{
}

void CMDICutClassFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CUT_CLASS_KEY);
	SavePlacement(this, csBuf);
}

int CMDICutClassFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgCap = xml->str(IDS_STRING151);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml->str(IDS_STRING185),
						xml->str(IDS_STRING187));
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDICutClassFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICutClassFrame diagnostics

#ifdef _DEBUG
void CMDICutClassFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICutClassFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDICutClassFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CUT_CLASS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CUT_CLASS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICutClassFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDICutClassFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CCutClassFormView *pView = (CCutClassFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}

}

// load the placement in OnShowWindow()
void CMDICutClassFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CUT_CLASS_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICutClassFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDICutClassFrame::OnClose(void)
{

	CCutClassFormView *pView = (CCutClassFormView *)GetActiveView();
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (UMMessageBox(this->GetSafeHwnd(),m_sMsg1,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
//			{
				pView->saveCutClass();
//			}	// if (UMMessageBox(this->GetSafeHwnd(),m_sMsg1,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	CMDIChildWnd::OnClose();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICutClassFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDICutClassFrame message handlers
