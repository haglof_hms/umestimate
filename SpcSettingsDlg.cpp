// SpcSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "SpcSettingsDlg.h"

#include "ResLangFileReader.h"

#include <algorithm>

// CSpcSettingsDlg dialog

IMPLEMENT_DYNAMIC(CSpcSettingsDlg, CDialog)

BEGIN_MESSAGE_MAP(CSpcSettingsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSpcSettingsDlg::OnBnClickedOk)

	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnReportAssortValueChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT1, OnReportTransValueChanged)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_REPORT1, OnReportSelChanged)

	ON_BN_CLICKED(IDC_BUTTON6_1, &CSpcSettingsDlg::OnBnClickedButton61)
	ON_BN_CLICKED(IDC_BUTTON6_2, &CSpcSettingsDlg::OnBnClickedButton62)
END_MESSAGE_MAP()

CSpcSettingsDlg::CSpcSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSpcSettingsDlg::IDD, pParent)
{
}

CSpcSettingsDlg::CSpcSettingsDlg(CString spc_name,int spc_id,int trakt_id,CUMEstimateDB* db)
	: CDialog(CSpcSettingsDlg::IDD, NULL),
		m_pDB(db),
		m_sSpecieName(spc_name),
		m_nSpecieID(spc_id),
		m_nActiveTraktID(trakt_id)
{
	bHasCalculated=false;
}


CSpcSettingsDlg::~CSpcSettingsDlg()
{
}

void CSpcSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSpcSettingsDlg)
	DDX_Control(pDX, IDC_GRP6_2, m_wndGroup2);
	DDX_Control(pDX, IDC_GRP6_3, m_wndGroup3);

	DDX_Control(pDX, IDC_BUTTON6_1, m_wndBtnAddTransfer);
	DDX_Control(pDX, IDC_BUTTON6_2, m_wndBtnDelTransfer);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);

	//}}AFX_DATA_MAP

}

INT_PTR CSpcSettingsDlg::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL CSpcSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CRect rect;
	GetClientRect(&rect);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			CString sTmpCap;
			sTmpCap.Format(_T("%s - %s"),xml.str(IDS_STRING12),m_sSpecieName);
			SetWindowText(sTmpCap);
			m_wndGroup2.SetWindowText(xml.str(IDS_STRING1270));
			m_wndGroup3.SetWindowText(xml.str(IDS_STRING1271));

			m_wndBtnAddTransfer.SetWindowText(xml.str(IDS_STRING297));
			m_wndBtnDelTransfer.SetWindowText(xml.str(IDS_STRING298));
			m_wndBtnOK.SetWindowText(xml.str(IDS_STRING156));
			m_wndBtnCancel.SetWindowText(xml.str(IDS_STRING156));
			
			m_sMsgAssortmentMissing = xml.str(IDS_STRING3055);
			
			m_sCapMsg = xml.str(IDS_STRING289);
		}
		xml.clean();
	}


	setupReport_assort();
	setupReport_trans();

	getTraktDataFromDB();
	getTraktAssFromDB();

	populateData();

	return TRUE;
}

BOOL CSpcSettingsDlg::setupReport_assort(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;
	if (m_wndReport_assort.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReport_assort.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport_assort.GetSafeHwnd() != NULL)
				{
				
					m_wndReport_assort.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING204)), 80));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING249)), 60));
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING250)), 60));
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING251)), 60));
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING252)), 60));
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING303)), 60));
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING304)), 60));
					pCol->SetEditable( FALSE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_assort.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING2520)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					m_wndReport_assort.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport_assort.GetReportHeader()->AllowColumnSort(FALSE);
					m_wndReport_assort.SetMultipleSelection( FALSE );
					m_wndReport_assort.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport_assort.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport_assort.FocusSubItems(TRUE);
					m_wndReport_assort.AllowEdit(TRUE);
					
					CRect rect;
					GetClientRect(&rect);
					setResize(&m_wndReport_assort,10,30,rect.right-20,180,TRUE);

				}	// if (m_wndReport_assort.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

BOOL CSpcSettingsDlg::setupReport_trans(void)
{

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;
	if (m_wndReport_trans.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport_trans.Create(this, IDC_REPORT1, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport_trans.GetSafeHwnd() != NULL)
				{
				
					m_wndReport_trans.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport_trans.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING299)), 90));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndReport_trans.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING300)), 90));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndReport_trans.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING301)), 60));
					pCol->SetEditable( TRUE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetVisible(FALSE);	// hide possibility to do transfer by m3fub; 091021 p�d

					pCol = m_wndReport_trans.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING302)), 60));
					pCol->SetEditable( TRUE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport_trans.AddColumn(new CXTPReportColumn(COLUMN_4, xml->str(IDS_STRING306), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					m_wndReport_trans.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport_trans.GetReportHeader()->AllowColumnSort(FALSE);
					m_wndReport_trans.SetMultipleSelection( FALSE );
					m_wndReport_trans.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport_trans.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport_trans.FocusSubItems(TRUE);
					m_wndReport_trans.AllowEdit(TRUE);
					m_wndReport_trans.SetFocus();

					CRect rect;
					GetClientRect(&rect);
					setResize(&m_wndReport_trans,10,280,500,170,TRUE);
					// Need to set size of Report control; 051219 p�d
//					setResize(&m_wndReport_trans,340,150,300,140);

				}	// if (m_wndReport_assort.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}



void CSpcSettingsDlg::OnReportAssortValueChanged(NMHDR * pNotifyStruct, LRESULT * result)
{
	CString S;
	double fM3Fub;
	double fM3To;
	double fValueM3Fub_original;
	double fValueM3Fub_new;
	double fValueM3To_original;
	double fValueM3To_new;
	double fKrPerM3_last;
	double fKrPerM3_entered;
	CXTPReportColumn *pCol = NULL;
	CAssortmentReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		m_nPriceSetIn	= 2;	// Price set as defualt, in m3to; 090615 p�d
		//---------------------------------------------------------------------------------
		// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
		getTraktMiscDataFromDB();
		// .. and get information in pricelist (xml-file), saved; 070502 p�d
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get assortments species in pricelist; 070601 p�d
				pPrlParser->getHeaderPriceIn(&m_nPriceSetIn);
			}
			delete pPrlParser;
		}
			pCol = pItemNotify->pColumn;
			if (pCol != NULL)
			{
				if (pCol->GetItemIndex() == COLUMN_7)
				{
					pRec = (CAssortmentReportRec*)pItemNotify->pRow->GetRecord();
					if (pRec != NULL)
					{
						//Lagt till att spara och st�ngknappen skall uppdateras �ven n�r sortimentsf�nstret redigerats
						//20111214 J� Bug #2674
						SetCalculated();
						CTransaction_trakt_ass rec = pRec->getRecord();

						// Caluclate added kr/m3 (+/-); 080305 p�d
						fValueM3Fub_new = 0.0;
						fValueM3To_new = 0.0;
						fM3Fub = rec.getM3FUB();
						fM3To = rec.getM3TO();
						fKrPerM3_last = getLastKrPerM3(rec) * (-1.0);	// To revese calculation; 080305 p�d
						fKrPerM3_entered = pRec->getColumnFloat(COLUMN_7);

						if (rec.getValueM3FUB() > 0.0)
//						if (m_nPriceSetIn == 1)
						{
							// Try to get back to original value, from calculated value; 080304 p�d
							fValueM3Fub_original = rec.getValueM3FUB()+(fM3Fub*fKrPerM3_last);
							// Calculate new value for M3Fub; 080304 p�d
							fValueM3Fub_new = fValueM3Fub_original+(fM3Fub*fKrPerM3_entered);
							pRec->setColumnFloat(COLUMN_5,fValueM3Fub_new);

							// Calculate new value for M3Fub; 080304 p�d
							fValueM3To_new = 0.0;
							pRec->setColumnFloat(COLUMN_6,fValueM3To_new);

						}
						if (rec.getValueM3TO() > 0.0)
//						else if (m_nPriceSetIn == 2)
						{
							// Try to get back to original value, from calculated value; 080304 p�d
							fValueM3To_original = rec.getValueM3TO()+(fM3To*fKrPerM3_last);
							// Calculate new value for M3Fub; 080304 p�d
							fValueM3To_new = fValueM3To_original+(fM3To*fKrPerM3_entered);
							pRec->setColumnFloat(COLUMN_6,fValueM3To_new);

							// Calculate new value for M3Fub; 080304 p�d
							fValueM3Fub_new = 0.0;
							pRec->setColumnFloat(COLUMN_5,fValueM3Fub_new);
			
						}
						// Update database; adding "kronor per kubikmeter); 080305 p�d
						if (m_pDB != NULL)
						{
							m_pDB->updTraktAss_kr_per_m3(rec.getTAssTraktID(),
																					 rec.getTAssTraktDataID(),
																					 rec.getTAssTraktDataType(),
																					 rec.getTAssID(),
																					 fValueM3Fub_new,
																					 fValueM3To_new,
																					 fKrPerM3_entered);
							//Kanske ladda in sortimenten igen efter man sparat en ny kr/m3 till�gg, bug #2708 J� 20111227
							getTraktAssFromDB();
						}
					}	// if (pRec != NULL)
				}	// if (pCol->GetItemIndex() == COLUMN_7)
			}	// if (pCol != NULL)
	}	// if (pItemNotify != NULL)
}

void CSpcSettingsDlg::OnReportTransValueChanged(NMHDR * pNotifyStruct, LRESULT * result)
{
	CXTPReportColumn *pCol = NULL;
	CTransferReportRec *pRec = NULL;
	CTransaction_trakt_trans *pTrans=NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	int nFromSort=-1,nToSort=-1;

	if (pItemNotify != NULL)
	{
		pRec = (CTransferReportRec*)pItemNotify->pRow->GetRecord();
		pCol = pItemNotify->pColumn;
		if (pCol && pRec)
		{
			if (pCol->GetItemIndex() == COLUMN_0)
			{
				CTransaction_trakt_trans rec = pRec->getRecord();
				if (m_vecAssortmentsPerSpc.size() > 0)
				{
					for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
					{
						if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)	// make sure it is the correct species
						{
							CTransaction_assort recAss = m_vecAssortmentsPerSpc[i];
							if (recAss.getAssortName().CompareNoCase(pRec->getColumnText(COLUMN_0)) == 0)
							{
								m_bAddOnlyPulp = (recAss.getPulpType() > 0);
								m_sFromAssortment = recAss.getAssortName();
								pRec->setColumnText(COLUMN_1, L"");	// clear the destination column
								addConstraintsToReport(2 /* Only add to to */);
								break;
							}	// if (recAss.getAssortName().CompareNoCase(pRec->getColumnText(COLUMN_0)) == 0)
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pCol->GetItemIndex() == COLUMN_0)
			else if (pCol->GetItemIndex() == COLUMN_1)
			{
				CTransaction_trakt_trans rec = pRec->getRecord();
				if (m_vecAssortmentsPerSpc.size() > 0)
				{
					for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
					{
						if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)	// make sure it is the correct species
						{
							CTransaction_assort recAss = m_vecAssortmentsPerSpc[i];
							if (recAss.getAssortName().CompareNoCase(pRec->getColumnText(COLUMN_1)) == 0 && 
									pRec->getColumnFloat(COLUMN_3) > 0.0)
							{
								m_bAddOnlyPulp = (recAss.getPulpType() > 0);
								m_sFromAssortment = recAss.getAssortName();
									recalculateTransfers();
								break;
							}	// if (recAss.getAssortName().CompareNoCase(pRec->getColumnText(COLUMN_0)) == 0)
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pCol->GetItemIndex() == COLUMN_0)
			else if (pCol->GetItemIndex() == COLUMN_3)
			{  
					recalculateTransfers();
			}
		}
	}
}

// CSpcSettingsDlg message handlers

// Get pricelist for trakt in esti_trakt_pricelist_table; 070430 p�d
void CSpcSettingsDlg::getTraktMiscDataFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTraktMiscData(rec->getTraktID(),m_recTraktMiscData);
	}	// if (m_pDB != NULL)
}

void CSpcSettingsDlg::getTraktFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTrakt(rec->getTraktID(),m_recTrakt);
	}	// if (m_pDB != NULL)
}



// Get settings for species in this Trakt; 070504 p�d
void CSpcSettingsDlg::getTraktSetSpc(void)
{
	if (m_pDB != NULL)
	{
		m_vecTransactionTraktSetSpc.clear();
		// Only select data for This trakt; 070504 p�d
		 m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,getActiveTraktData()->getTDataTraktID());
	}	// if (m_pDB != NULL)
}

// Get Trakt data in database; 070312 p�d
void CSpcSettingsDlg::getTraktDataFromDB(void)
{
	m_vecTraktData.clear();
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,m_nActiveTraktID);
	}	// if (pDB != NULL)
}

// Get Trakt data in database for specie; 070312 p�d
void CSpcSettingsDlg::getTraktDataFromDB_specie(int trakt_id,int spc_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_recTraktData,trakt_id,spc_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

// Added 2007-07-03 p�d
// Save traktdata information to db; 070703 p�d
void CSpcSettingsDlg::saveTraktDataToDB(void)
{
	if (m_pDB != NULL)
	{
		if (!m_pDB->addTraktData(m_recTraktData))
			m_pDB->updTraktData(m_recTraktData);
	}	// if (m_pDB != NULL)
}

void CSpcSettingsDlg::getTraktAssFromDB(void)
{
	m_vecTraktAss.clear();
	getTrakt();
	if (m_pDB != NULL)
	{
		m_pDB->getTraktAss(m_vecTraktAss,m_recTrakt.getTraktID());
	}	// if (m_pDB != NULL)
}

// Special function; get Assort data from database-table, into
// m_vecTraktAss, excluding the specie set in spc_id.
// This specie'll value are set in getTraktAss() method; 070315 p�d
void CSpcSettingsDlg::getTraktAssFromDB_exclude(void)
{
	m_vecTraktAss.clear();

	if (m_pDB != NULL)
	{
		m_pDB->getTraktAss_exclude(m_vecTraktAss,m_nSpecieID);
	}	// if (m_pDB != NULL)
}

void CSpcSettingsDlg::saveTraktAssToDB(void)
{
	if (m_vecTraktAss.size() > 0)
	{
		if (m_pDB != NULL)
		{
			// Readd Assortment information in m_vecTraktAss vector; 070613 p�d
			for (UINT i = 0;i < m_vecTraktAss.size();i++)
			{
				if (!m_pDB->addTraktAss(m_vecTraktAss[i]))
					m_pDB->updTraktAss(m_vecTraktAss[i]);
			}
		}	// if (m_pDB != NULL)
	}
}

void CSpcSettingsDlg::delTraktAssFromDB(void)
{
	if (m_vecTraktAss.size() > 0)
	{
		// Start by removing ALL Exchangeinformation from esti_trakt_spc_assort_table; 070613 p�d
		if (m_pDB != NULL)
		{
			m_pDB->removeTraktAss(m_recTraktData.getTDataTraktID(),m_recTraktData.getTDataID());
		}	// if (m_pDB != NULL)
	}
}

void CSpcSettingsDlg::resetTraktAssInDB(void)
{
	if (m_vecTraktAss.size() > 0)
	{
		// Start by removing ALL Exchangeinformation from esti_trakt_spc_assort_table; 070613 p�d
		if (m_pDB != NULL)
		{
			// Readd Assortment information in m_vecTraktAss vector; 070613 p�d
			for (UINT i = 0;i < m_vecTraktAss.size();i++)
			{
				CTransaction_trakt_ass data = m_vecTraktAss[i];
				if (m_recTraktData.getTDataID() == data.getTAssTraktDataID() && 
						m_recTraktData.getTDataID() == m_nSpecieID &&		
						m_recTraktData.getTDataTraktID() == data.getTAssTraktID())
				{
					data.setM3FUB(0.0);
					data.setM3TO(0.0);
					data.setValueM3FUB(0.0);
					data.setValueM3TO(0.0);
					//data.setKrPerM3(0.0); //Kanske skulle ta bort nollst�llningen av kr/m3 till�gget f�r sortimenten, bug #2708 20111227 J�
					if (!m_pDB->addTraktAss(data))
						m_pDB->updTraktAss(data);
				}
			}
		}	// if (m_pDB != NULL)
	}

}

double CSpcSettingsDlg::getLastKrPerM3(CTransaction_trakt_ass rec)
{
	if (m_vecTraktAss.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktAss.size();i++)
		{
			CTransaction_trakt_ass rec1 = m_vecTraktAss[i];
			if (rec1.getTAssID() == rec.getTAssID() &&
					rec1.getTAssTraktDataID() == rec.getTAssTraktDataID() &&
					rec1.getTAssTraktDataType() == rec.getTAssTraktDataType() &&
					rec1.getTAssTraktID() == rec.getTAssTraktID())
					return rec1.getKrPerM3();
		}	// for (UINT i = 0;i < m_vecTraktAss.size();i++)
	}	// if (m_vecTraktAss.size() > 0)

	return 0.0;
}


// Get transfers made for this trakt; 070611 p�d
void CSpcSettingsDlg::getTraktAssTrans(int trakt_id)
{
	m_vecTraktAssTrans.clear();

	if (m_pDB != NULL)
	{
		m_pDB->getTraktTrans(m_vecTraktAssTrans,trakt_id);
	}	// if (m_pDB != NULL)
}

void CSpcSettingsDlg::saveTraktAssTrans(void)
{
	int nTraktDataID = -1;
	int nTraktID = -1;
	CTransferReportRec *pRec = NULL;
	CTransaction_trakt_trans data;
	CXTPReportRecords *pRecs = NULL;
	double fM3Trans = 0.0;

	CString S;
	
	getTrakt();

	m_wndReport_trans.Populate();
	pRecs = m_wndReport_trans.GetRecords();
	if (pRecs == NULL) return;

//	S.Format(L"saveTraktAssTrans\nAntal rader %d",pRecs->GetCount());
//	UMMessageBox(S);

	nTraktID	= m_recTraktData.getTDataTraktID();
	nTraktDataID = m_recTraktData.getTDataID();

	if (m_pDB)
		m_pDB->removeTraktTrans2(nTraktID,m_nSpecieID);

	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (CTransferReportRec *)pRecs->GetAt(i);

		fM3Trans = pRec->getColumnFloat(COLUMN_4);
		if (fM3Trans <= 0.0) fM3Trans = 0.0;
		data = CTransaction_trakt_trans(m_nSpecieID,
																		nTraktID,
																		findFromAssortmentID(pRec->getColumnText(COLUMN_0)),
																		findToAssortmentID(pRec->getColumnText(COLUMN_1)),
																		m_nSpecieID, //nTraktDataID,
																		STMP_LEN_WITHDRAW,
																		pRec->getColumnText(COLUMN_0),
																		pRec->getColumnText(COLUMN_1),
																		m_recTraktData.getSpecieName(),
																		i+1.0,
																		pRec->getColumnFloat(COLUMN_3),
																		fM3Trans,
																		_T(""));
/*
		S.Format(L"saveTraktAssTrans\nFromID %d AssName %s\nToID %d  ToName %s\nTransDataID %d\nTraktID %d\nm3Trans %f",
			data.getTTransFromID(),
			data.getFromName(),
			data.getTTransToID(),
			data.getToName(),
			data.getTTransDataID(),
			data.getTTransTraktID(),
			data.getM3Trans());
	UMMessageBox(S);
*/
		if (m_pDB != NULL)
		{
			if (!m_pDB->addTraktTrans(data))
				m_pDB->updTraktTrans(data);
		}	// if (m_pDB != NULL)
	}
}

void CSpcSettingsDlg::delTraktAssTrans(void)
{
	int nTraktDataID;
	int nTraktID;
	CXTPReportRecords *pRecs = m_wndReport_trans.GetRecords();
	if (pRecs != NULL)
	{
		getTrakt();

		if (m_enumAction == UPD_ITEM)
		{
			nTraktID	= m_recTraktData.getTDataTraktID();
			nTraktDataID = m_recTraktData.getTDataID();
		}

		if (m_pDB != NULL)
		{
			m_pDB->removeTraktTrans(nTraktDataID,nTraktID);
		}	// if (m_pDB != NULL)
	}	// if (pRecs != NULL)
}


BOOL CSpcSettingsDlg::isSpcUsed(int id)
{
	if (m_vecTraktData.size() > 0)
	{
		getTrakt();
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			if (m_recTrakt.getTraktID() == m_vecTraktData[i].getTDataTraktID() &&
 				  m_vecTraktData[i].getTDataID() == id)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

void CSpcSettingsDlg::populateData(void)
{
	CString sTmp;
	if (m_vecTraktData.size() > 0)
	{
		getTrakt();
		// Find which trakt is selected and populate data
		// for that trakt; 070315 p�d
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			m_recTraktData = m_vecTraktData[i];
			if (m_recTrakt.getTraktID() == m_recTraktData.getTDataTraktID() &&
 				  m_recTraktData.getTDataID() == m_nSpecieID)
			{
				m_nSelectedTraktDataIdx = i;	// Index of selected data

				m_wndReport_assort.ClearReport();
				if (m_vecTraktAss.size() > 0)
				{
					// Add assortments to m_wndReport_assort; 070612 p�d
					for (UINT j = 0;j < m_vecTraktAss.size();j++)
					{
						m_recTraktAss = m_vecTraktAss[j];
						if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
								m_recTraktData.getTDataID() == m_nSpecieID &&		
								m_recTraktData.getTDataTraktID() == m_recTraktAss.getTAssTraktID())
						{
							m_wndReport_assort.AddRecord(new CAssortmentReportRec(j+1,m_recTraktAss));				
						}	// if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
					}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)

				}	// if (m_vecTraktAss.size() > 0)

				// Add assortments transfers to m_wndReport_trans; 070611 p�d
				m_wndReport_trans.ResetContent();
				// Get trakt transfers; 070611 p�d
				getTraktAssTrans(m_recTraktData.getTDataTraktID());
				if (m_vecTraktAssTrans.size() > 0)
				{
					for (UINT j = 0;j < m_vecTraktAssTrans.size();j++)
					{
						m_recTraktAssTrans = m_vecTraktAssTrans[j];
						if (m_recTraktAssTrans.getSpecieID() == m_nSpecieID )
						{
							// Add assortments to m_wndReport; 070612 p�d
							for (UINT j = 0;j < m_vecTraktAss.size();j++)
							{
								m_recTraktAss = m_vecTraktAss[j];
								if (m_recTraktAss.getTAssName().CompareNoCase(m_recTraktAssTrans.getFromName()) == 0)
								{
									if (m_recTraktAss.getPriceM3FUB() > 0.0) sTmp = L"fub";
									else if (m_recTraktAss.getPriceM3FUB() == 0.0 && m_recTraktAss.getPriceM3TO() > 0.0) sTmp = L"to";
									break;
								}
							}

							addConstraintsToReport();	// Need this here; 070925 p�d
							m_wndReport_trans.AddRecord(new CTransferReportRec(i+1,m_recTraktAssTrans,sTmp));
						}	// if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
					}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)
				}	// if (m_vecTraktAss.size() > 0)
				break;
			}	// if (m_recTrakt.getTraktID() == m_recTraktData.getTDataTraktID())
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)

	m_wndReport_assort.Populate();
	m_wndReport_assort.UpdateWindow();

	m_wndReport_trans.Populate();
	m_wndReport_trans.UpdateWindow();
}

BOOL CSpcSettingsDlg::getIsDirty(void)
{
	BOOL bIsFrameDirty = isSpecieDialogFrameDirty();
	BOOL bIsDialogDirty1 = m_wndReport_assort.isDirty();
	BOOL bIsDialogDirty2 = m_wndReport_trans.isDirty();
	return bIsFrameDirty || bIsDialogDirty1 || bIsDialogDirty2;
}

void CSpcSettingsDlg::exitViewAction(void)
{
	if (getIsDirty())
	{
		// Save changes to database; 070703 p�d
		// First save as "St�mplingsl�ngd - Uttag"
		saveTraktDataToDB();
	}	//	if (getIsDirty())
}

int CSpcSettingsDlg::findFromAssortmentID(LPCTSTR assort_name)
{
	CString S;
	CXTPReportColumns *pCols = m_wndReport_trans.GetColumns();
	if (pCols != NULL)
	{
		CXTPReportColumn *pCol = pCols->Find(COLUMN_0);
		if (pCol != NULL)
		{
			CXTPReportRecordItemConstraint* pConst = pCol->GetEditOptions()->FindConstraint(assort_name);
			if (pConst != NULL)
			{
				return (int)pConst->m_dwData;
			}	// if (pConst != NULL)
		}	// if (pCol != NULL)
	}	// if (pCols != NULL)
	return -1;
}

int CSpcSettingsDlg::findToAssortmentID(LPCTSTR assort_name)
{

	CXTPReportColumns *pCols = m_wndReport_trans.GetColumns();
	if (pCols != NULL)
	{
		CXTPReportColumn *pCol = pCols->Find(COLUMN_0);
		if (pCol != NULL)
		{
			CXTPReportRecordItemConstraint* pConst = pCol->GetEditOptions()->FindConstraint(assort_name);
			if (pConst != NULL)
			{
				return (int)pConst->m_dwData;
			}	// if (pConst != NULL)
		}	// if (pCol != NULL)
	}	// if (pCols != NULL)


	return -1;
}

bool MyDataSort_transfers(CTransaction_trakt_trans& d1,CTransaction_trakt_trans& d2)
{
	return (d1.getToName() < d2.getToName());
}

void CSpcSettingsDlg::SetCalculated()
{
	bHasCalculated=true;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
			m_wndBtnOK.SetWindowText(xml.str(IDS_STRING1595));
		xml.clean();
	}
}
void CSpcSettingsDlg::recalculateTransfers(void)
{
	CString S;
	CXTPReportRow *pRow = m_wndReport_trans.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport_trans.GetFocusedColumn();
	vecTransactionTraktTrans vecTransfers;
	int nFocusedRowIndex = -1;
	int nPriceSetIn = 2;
	short nOnlyForTransfers = 0;
	vecTransactionAssort vecAssortmentsPrl;	// XML pricelist vector

	// Set wait cursor; 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	if (pRow != NULL && pCol != NULL)
	{
		SetCalculated();
		
		nFocusedRowIndex = pRow->GetIndex();

		// .. and get information in pricelist (xml-file), saved; 070502 p�d
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				pPrlParser->getHeaderPriceIn(&nPriceSetIn);
				//---------------------------------------------------------------------------------
				pPrlParser->getAssortmentPerSpecie(vecAssortmentsPrl);	// Added 100811 p�d

			}
			delete pPrlParser;
		}

		// Save transfers made; 070612 p�d
		saveTraktAssTrans();

		// After saving, reload transfer-data; 081016 p�d
		getTrakt();	// Active trakt; 081016 p�d
		if (m_pDB != NULL)
		{
			// Load
				m_pDB->getTraktTrans(vecTransfers,m_recTrakt.getTraktID());
		}	// if (m_pDB != NULL)

		// Recalculate, so we're back at the start, no transfers; 081016 p�d
		resetTraktAssInDB();
		runDoCalulationInPageThree(m_nSpecieID,TRUE /* Clear transfers */,TRUE,FALSE /* Don't show message */,false /* Don't do transfers in HFuncLib */);
		getTraktAssFromDB();

		// Add vecTransfers to m_wndReport_trans
		m_wndReport_trans.ResetContent();
		// Do we have Transfers; 081016 p�d
		if (vecTransfers.size() > 0)
		{
			for (UINT i = 0;i < vecTransfers.size();i++)
			{
				// We need to check specie also. Because assortments can have the same
				// name in different species; 090922 p�d
				if (vecTransfers[i].getSpecieID() == m_nSpecieID)
				{
					// Only reset values for Percent or M3Fub, on row that's been changed; 081017 p�d
					if (i == nFocusedRowIndex)
					{
						if (pCol->GetIndex() == COLUMN_2)	vecTransfers[i].setPercent(0.0);
						else if (pCol->GetIndex() == COLUMN_3)vecTransfers[i].setM3FUB(0.0);
						else if (pCol->GetIndex() == COLUMN_4)vecTransfers[i].setM3Trans(0.0);
					}	// if (i == nFocusedRowIndex)
					m_wndReport_trans.AddRecord(new CTransferReportRec(i+1,vecTransfers[i],L""));
				}	// if (vecTransfers[i].getSpcID() == m_nSpecieID)
			}	// for (UINT i = 0;i < vecTransfers.size();i++)
		}
		m_wndReport_trans.Populate();
		m_wndReport_trans.UpdateWindow();

		// Save transfers made; 070612 p�d
		saveTraktAssTrans();
//		getTraktAssFromDB();
		double fVolTrans = 0.0;
		bool bFirstTime = true;
		int nAssortIndex = -1;
		vecTransactionTraktTrans vecTrans;
		CTransaction_trakt_trans recTrans;
		CXTPReportRows *pRows = m_wndReport_trans.GetRows();
		if (pRows != NULL)
		{
			// Add data from transfers to vector and sort it based on
			// assortments 'to'; 101028 p�d
			for (int i = 0;i < pRows->GetCount();i++)
			{
				CTransferReportRec *pRec1 = (CTransferReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec1 != NULL)
				{
					pRec1->getRecord().setM3FUB(i+1.0);
					vecTrans.push_back(pRec1->getRecord());
				}
			}
	//		std::sort(vecTrans.begin(),vecTrans.end(),MyDataSort_transfers);

			for (UINT i = 0;i < vecTrans.size();i++)
			{
				recTrans = vecTrans[i];
				nOnlyForTransfers = 0;
				// Try to find out if the assortment is only for Transfers; i.e. min.diam = 0.0 ; 100811 p�d
				if (vecAssortmentsPrl.size() > 0)
				{
					for (UINT iii = 0;iii < vecAssortmentsPrl.size();iii++)
					{
						CTransaction_assort rec = vecAssortmentsPrl[iii];
						if (rec.getAssortName().CompareNoCase(recTrans.getToName()) == 0)
						{
							if (rec.getMinDiam() == 0.0) 
							{
								if (nAssortIndex != iii)
									bFirstTime = true;
								nOnlyForTransfers = 1;
								nAssortIndex = iii;
							}
							break;
						}	// if (rec.getAssortName().CompareNoCase(pRec1->getColumnText(COLUMN_1)) == 0)
					}	// for (UINT iii = 0;iii < vecAssortmentsPrl.size();iii++)
				}	// if (vecAssortmentsPrl.size() > 0)

				// Function in HMSFuncLib.h; 081017 p�d
				calcTransfersByPercent(m_nSpecieID,
															 recTrans.getFromName(),
															 recTrans.getToName(),
															 recTrans.getPercent(),
															 m_recTraktData,
															 m_vecTraktAss,
															 nOnlyForTransfers,
															 bFirstTime,
															 &fVolTrans);
				bFirstTime = false;

				recTrans.setM3Trans(fVolTrans);
				if (m_pDB)
				{
					m_pDB->updTraktTrans(recTrans);								 
				}

				saveTraktAssToDB();
				getTraktAssFromDB();
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows != NULL)

		populateData();

		vecTransfers.clear();
		vecTrans.clear();
	}	// if (pRow != NULL && pCol != NULL)
	else
		recalculateNoTransfers();

	// Set back to arrow cursor (default); 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

}

void CSpcSettingsDlg::recalculateNoTransfers(void)
{
	SetCalculated();
	// Set wait cursor; 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	m_wndReport_trans.ResetContent();
	m_wndReport_trans.Populate();
	m_wndReport_trans.UpdateWindow();
	resetTraktAssInDB();
	if (runDoCalulationInPageThree(m_nSpecieID /* Used only when TRUE = Remove transfers for specie */, 
																 TRUE /* REMOVE TRAKT TRANSFERS */,TRUE,
																 FALSE /* Don't show message */,
																 true,
																 false))
	{
		// Reload Trakt assortment data; 070613 p�d
		getTraktAssFromDB();
		populateData();
	}

	// Set back to arrow cursor (default); 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}

void CSpcSettingsDlg::getTrakt(void)
{
	getTraktFromDB();
}

void CSpcSettingsDlg::addConstraintsToReport(short add_to)
{
	int nCounter;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport_trans.GetColumns();
	CXTPReportRows *pRows = m_wndReport_trans.GetRows();
	if (pColumns != NULL)
	{
		// Get assortments pricelist for this trakt.
		// Get info. from esti_trakt_misc_data_table; 070509 p�d
		getAssortmentsInPricelist();
		if (add_to == 0 || add_to == 1)
		{
			// Add species in Pricelist to Combobox in Column 1; 070509 p�d
			CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_0);
			if (pFromCol != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pFromCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)
				if (add_to == 1)
				{
					// Add species in Pricelist to Combobox in Column 1; 070509 p�d
					CXTPReportColumn *pToCol = pColumns->Find(COLUMN_1);
					if (pToCol != NULL)
					{
						// Get constraints for Specie column and remove all items; 070509 p�d
						pCons = pToCol->GetEditOptions()->GetConstraints();
						if (pCons != NULL)
						{
							pCons->RemoveAll();
						}	// if (pCons != NULL)
					}
				}
				// If there's species, add to Specie column in report; 070509 p�d
				if (m_vecAssortmentsPerSpc.size() > 0)
				{
					nCounter = 0;
					for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
					{
						if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)
						{
							pFromCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
							nCounter++;
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pFromCol != NULL)
		}
/*
		if (add_to == 0)
		{
			// Add species in Pricelist to Combobox in Column 1; 070509 p�d
			CXTPReportColumn *pToCol = pColumns->Find(COLUMN_1);
			if (pToCol != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pToCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)

				// If there's species, add to Specie column in report; 070509 p�d
				if (m_vecAssortmentsPerSpc.size() > 0)
				{
					nCounter = 0;
					for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
					{
						if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)
						{
							pToCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
							nCounter++;
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pFromCol != NULL)
		}
*/
		if (add_to == 2)
		{
			CTransferReportRec *pRec = NULL;
			CXTPReportRow *pRow = m_wndReport_trans.GetFocusedRow();
			CString csSource;	// contains source assortment

			if (pRow)
			{
				pRec = (CTransferReportRec *)pRow->GetRecord();
				csSource = pRec->getColumnText(COLUMN_0);	// get selected assortment in source column

				for( UINT i = 0; i < m_vecAssortmentsPerSpc.size(); i++ )
				{
					if( m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID )	// make sure it is the right species
					{
						if( csSource.CompareNoCase(m_vecAssortmentsPerSpc[i].getAssortName()) == 0 )
						{
							m_bAddOnlyPulp = (m_vecAssortmentsPerSpc[i].getPulpType() > 0);
						}
					}
				}
			}

			// Add species in Pricelist to Combobox in Column 1; 070509 p�d
			CXTPReportColumn *pToCol = pColumns->Find(COLUMN_1);
			if (pToCol != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pToCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
//					if (pRec)
//						pRec->setColumnText(COLUMN_1,L"");
					pCons->RemoveAll();
				}	// if (pCons != NULL)

				// If there's species, add to Specie column in report; 070509 p�d
				if (m_vecAssortmentsPerSpc.size() > 0)
				{
					nCounter = 0;
					for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
					{
						if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)	// make sure it is the right species
						{
							if( csSource.CompareNoCase(m_vecAssortmentsPerSpc[i].getAssortName()) != 0 )	// exclude source from destination
							{
								if( m_bAddOnlyPulp && (m_vecAssortmentsPerSpc[i].getPulpType() == 1 || m_vecAssortmentsPerSpc[i].getPulpType() == 2) )
								{
									pToCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
								}
								else if( !m_bAddOnlyPulp )
								{
									pToCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
								}
							}

							nCounter++;
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pToCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					if (pCons->GetCount() == 0)
						UMMessageBox(this->GetSafeHwnd(),m_sMsgAssortmentMissing,m_sCapMsg,MB_ICONASTERISK | MB_OK);
				}
			}	// if (pFromCol != NULL)
		}
	}	// if (pColumns != NULL)

}

void CSpcSettingsDlg::getAssortmentsInPricelist(void)
{
	//---------------------------------------------------------------------------------
	// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
	getTraktMiscDataFromDB();
	// .. and get information in pricelist (xml-file), saved; 070502 p�d
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070601 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssortmentsPerSpc);
		}
		delete pPrlParser;
	}

}

void CSpcSettingsDlg::OnBnClickedButton61()
{
	// Add a CAssortmentReportRec; 070309 p�d
	m_wndReport_trans.AddRecord(new CTransferReportRec());
	m_wndReport_trans.Populate();
	m_wndReport_trans.UpdateWindow();

	addConstraintsToReport(1 /* Only add to From assortment */);
}

// Remove transfer
void CSpcSettingsDlg::OnBnClickedButton62()
{
	CTransaction_trakt_trans rec;
	CTransferReportRec *pRec = NULL;
	int nIndex = -1;
	BOOL bRecalc;
	CXTPReportRecords *pRecs = m_wndReport_trans.GetRecords();
	if (pRecs != NULL)
	{
		CXTPReportRow *pRow = m_wndReport_trans.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CTransferReportRec *)pRecs->GetAt(pRow->GetIndex());
			if (pRec != NULL)
			{
				rec = pRec->getRecord();
				bRecalc = (!pRec->getColumnText(COLUMN_1).IsEmpty());
				nIndex = pRow->GetIndex();
				if (m_pDB != NULL)
				{
					m_pDB->removeTraktTrans(rec);
				}	// if (m_pDB != NULL)
				pRecs->RemoveAt(pRow->GetIndex());
				m_wndReport_trans.Populate();
				m_wndReport_trans.UpdateWindow();
				m_wndReport_trans.setIsDirty(TRUE);
				if (bRecalc)
					recalculateTransfers();
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
	}	// if (pRecs != NULL)
	else
		recalculateNoTransfers();
}

BOOL CSpcSettingsDlg::saveData(void)
{
	return TRUE;
}

// Save data and Quit dialog; 091103 p�d
void CSpcSettingsDlg::OnBnClickedOk()
{
	if (getIsDirty() || bHasCalculated) 
		OnOK();
	else 
		OnCancel();
}

void CSpcSettingsDlg::OnReportSelChanged(NMHDR* /*pNotifyStruct*/, LRESULT* /*result*/)
{
	CXTPReportColumn* pCol = m_wndReport_trans.GetFocusedColumn();
	if( pCol != NULL )
	{
		if( pCol->GetIndex() == 1 )
		{
			// repopulate the constraints for the destination column
			addConstraintsToReport(2);
		}
	}	

}
