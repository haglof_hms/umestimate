#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"

//extern CStringArray global_TREE_TYPES;
/////////////////////////////////////////////////////////////////////////////
// PrivItem for inplace button; 070306 p�d
class CIntItemInplaceBtn : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
	CString m_sLangFN;
	HWND hWndParent;
public:
	CIntItemInplaceBtn(int nValue,LPCTSTR lang_fn,HWND parent) : CXTPReportRecordItemNumber()
	{
		m_nValue = nValue;
		SetValue(m_nValue);
		m_sLangFN = lang_fn;
		hWndParent = parent;
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
		m_nValue = _tstoi(szText);
		SetValue(m_nValue);
	}

	int getIntItem(void)	{	return m_nValue; 	}
	
	void setIntItem(int value)	
	{ 
		m_nValue = value; 
		SetValue(value);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CTraktSampleTreeReportRec

class CTraktSampleTreeReportRec : public CXTPReportRecord
{
	CTransaction_sample_tree m_recTree;

	
protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CExTextH100Item : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		int m_nConstraintIndex;
	public:
		CExTextH100Item(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_sText = szText;
			if (m_sText.FindOneOf(_T("TGB")) > -1 || m_sText.IsEmpty())
			{
				m_sText.Delete(3,m_sText.GetLength());
				SetValue(m_sText);
			}
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}

		void setIsBold(void)		{ SetBold();	}

		void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
		void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	};

	// Special for Combobox items
	class CComboBoxItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CComboBoxItem(CString sValue,int index) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTraktSampleTreeReportRec(int grp_id = -1,int spc_id = -1,LPCTSTR lang_fn = _T(""),HWND parent = NULL)
	{
		AddItem(new CIntItemInplaceBtn(grp_id,lang_fn,parent));	// Plot (Group) ID
		AddItem(new CIntItem(spc_id));					// 
		AddItem(new CTextItem(_T("")));					// Name of Specie (Combobox)
		AddItem(new CFloatItem(0.0,sz1dec));		// Diameter at breastheight
		AddItem(new CFloatItem(0.0,sz1dec));		// Height of tree
		AddItem(new CFloatItem(0.0,sz1dec));		// Percent "Gr�nkrona"
		AddItem(new CFloatItem(0.0,sz1dec));		// Bark thickness mm
		AddItem(new CFloatItem(0.0,sz1dec));		// GROT kg
		AddItem(new CIntItem(0));								// Age
		AddItem(new CIntItem(0));								// Growth
		AddItem(new CFloatItem(0.0,sz1dec));		// Diameter class (cm)
		AddItem(new CFloatItem(0.0,sz1dec));		// Diameter class (cm)
		AddItem(new CFloatItem(0.0,sz1dec));		// M3Sk
		AddItem(new CFloatItem(0.0,sz1dec));		// M3Fub
		AddItem(new CFloatItem(0.0,sz1dec));		// M3Ub
		AddItem(new CTextItem(TREE_TYPE));			// Type tree (Provtr�d etc)
		AddItem(new CTextItem(_T("")));					// Jonsson bonitet
		AddItem(new CTextItem(_T("")));	// Category
		AddItem(new CIntItem(0));	// Dist cable
		AddItem(new CIntItem(0));	// "Toppningsv�rde"
		AddItem(new CTextItem(_T("")));					// Koordinat
	}

	CTraktSampleTreeReportRec(CTransaction_sample_tree &rec,LPCTSTR lang_fn,HWND parent,CStringArray &tree_types, vecTransactionSampleTreeCategory &pvec_categories)
	{
		m_recTree = rec;	
		AddItem(new CIntItemInplaceBtn(rec.getPlotID(),lang_fn,parent));	// Plot (Group) ID
		AddItem(new CIntItem(rec.getSpcID()));								// Specioe id
		AddItem(new CTextItem((rec.getSpcName())));					// Name of Specie (Combobox)
		AddItem(new CFloatItem(rec.getDBH(),sz1dec));					// Diameter at breastheight
		AddItem(new CFloatItem(rec.getHgt(),sz1dec));					// Height of tree
		AddItem(new CFloatItem(rec.getGCrownPerc(),sz1dec));	// Percent "Gr�nkrona"
		AddItem(new CFloatItem(rec.getBarkThick(),sz1dec));		// Bark thickness mm
		AddItem(new CFloatItem(rec.getGROT(),sz1dec));				// GROT kg
		AddItem(new CIntItem(rec.getAge()));									// Age
		AddItem(new CIntItem(rec.getGrowth()));								// "Tillv�xt"
		AddItem(new CFloatItem(rec.getDCLS_from(),sz1dec));		// Diameter class (cm)
		AddItem(new CFloatItem(rec.getDCLS_to(),sz1dec));			// Diameter class (cm)
		AddItem(new CFloatItem(rec.getM3Sk(),sz3dec));				// M3Sk
		AddItem(new CFloatItem(rec.getM3Fub(),sz3dec));				// M3Fub
		AddItem(new CFloatItem(rec.getM3Ub(),sz3dec));				// M3Ub
		if (rec.getTreeType() >= 0 && rec.getTreeType() < tree_types.GetCount())
		{
			AddItem(new CTextItem(tree_types.GetAt(rec.getTreeType())));// Type tree (Provtr�d etc)
		}
		else if (tree_types.GetCount() > 0)
		{
			AddItem(new CTextItem(tree_types.GetAt(0)));// Type tree (Provtr�d etc)
		}
		AddItem(new CExTextH100Item((rec.getBonitet())));					// H100 (m)

		if(rec.getCategory() != -1)
			AddItem(new CTextItem( getValueForCategory(&pvec_categories, rec.getCategory()) ));					// Category
		else
			AddItem(new CTextItem(_T("")));

		AddItem(new CIntItem(rec.getDistCable()));								// Dist cable
		AddItem(new CIntItem(rec.getTopCut()));								// Toppningsv�rde
		AddItem(new CTextItem( rec.getCoord() ));					// Koordinat
	}

	CTransaction_sample_tree& getTreeRec(void)
	{
		return m_recTree;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item,int value)	
	{ 
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	private:
	LPCTSTR getValueForCategory(vecTransactionSampleTreeCategory *pvec_categories, int id)
	{
		for (int i = 0; i < pvec_categories->size(); i++)
		{
			if (pvec_categories->at(i).getID() == id)
				return pvec_categories->at(i).getCategory();
		}
		return _T("");
	}
	
};

//////////////////////////////////////////////////////////////////////////////
// CSampleTreeReportView form view

class CSampleTreeReportView : public  CMyReportView
{
	DECLARE_DYNCREATE(CSampleTreeReportView)

	BOOL m_bInitialized;

	BOOL m_bIsDirty;

	CXTPReportSubListControl m_wndSubList;
	CString m_sFieldChooser;
	CString m_sPrintPreview;
protected:
	CSampleTreeReportView();           // protected constructor used by dynamic creation
	virtual ~CSampleTreeReportView();

	vecTransactionPricelist m_vecPricelistData;
	BOOL setupReport(void);

	void LoadReportState(void);
	void SaveReportState(void);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	
public:

	void doRunPrintPreview(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);

	afx_msg void OnReportReqEdit(NMHDR*  pNotifyStruct, LRESULT* /*result*/);

	afx_msg void OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnPrintPreview();
	afx_msg void OnFilePrint();
	afx_msg void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
