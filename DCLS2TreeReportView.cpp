// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "StandEntryDoc.h"
#include "MDITabbedView.h"
#include "DCLS2TreeReportView.h"
#include "PageThreeFormView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CDCLS2TreeReportView

IMPLEMENT_DYNCREATE(CDCLS2TreeReportView,  CMyReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDCLS2TreeReportView,  CMyReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
END_MESSAGE_MAP()

CDCLS2TreeReportView::CDCLS2TreeReportView()
	: CMyReportView()
{
	m_pDB = NULL;
	m_bInitialized = FALSE;
	m_from_d_class=0.0;
	m_tr�dslag=_T("");
}

CDCLS2TreeReportView::~CDCLS2TreeReportView()
{
}

void CDCLS2TreeReportView::OnInitialUpdate()
{
	CMyReportView::OnInitialUpdate();

	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST4, &pWnd->m_wndFieldChooserDlg12);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (!m_bInitialized)
	{

//		VERIFY(CXTPReportControl::UseReportCustomHeap());
//		CXTPReportControl::UseRowBatchAllocation();

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

		LoadReportState();
		
		m_bInitialized = TRUE;

	}

}

BOOL CDCLS2TreeReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CMyReportView::OnCopyData(pWnd, pData);
}

void CDCLS2TreeReportView::OnDestroy()
{
	SaveReportState();
	if (m_pDB != NULL)
		delete m_pDB;

	CXTPReportView::OnDestroy();	
}

BOOL CDCLS2TreeReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CMyReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CDCLS2TreeReportView diagnostics

#ifdef _DEBUG
void CDCLS2TreeReportView::AssertValid() const
{
	CMyReportView::AssertValid();
}

void CDCLS2TreeReportView::Dump(CDumpContext& dc) const
{
	CMyReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CDCLS2TreeReportView message handlers

// CDCLS2TreeReportView message handlers
void CDCLS2TreeReportView::OnSize(UINT nType,int cx,int cy)
{
	CMyReportView::OnSize(nType,cx,cy);
}

void CDCLS2TreeReportView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

// Create and add Assortment settings reportwindow
BOOL CDCLS2TreeReportView::setupReport(void)
{

	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					// Also setup text strings; 070509 p�d					
					m_sFieldChooser = (xml->str(IDS_STRING147));
					m_sPrintPreview = (xml->str(IDS_STRING316));

					m_sMsgCap = (xml->str(IDS_STRING151));
					m_sDCLSErrorMsg1 = (xml->str(IDS_STRING3240));
					m_sDCLSErrorMsg2 = (xml->str(IDS_STRING3241));
					m_sDCLSErrorMsg3 = (xml->str(IDS_STRING3242));
					m_sDCLSErrorMsg4 = (xml->str(IDS_STRING3243));
					m_sDCLSErrorMsg5 = (xml->str(IDS_STRING3244));
					m_sDCLSErrorMsg6 = (xml->str(IDS_STRING3245));


					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					GetReportCtrl().ShowWindow( SW_NORMAL );
					GetReportCtrl().ShowGroupBy( TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->AddExpandButton();

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING263)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING264)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2720)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING2721)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING266)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING1351)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING267)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING268)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING269)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING270)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING2700)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING271)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, (xml->str(IDS_STRING273)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml->str(IDS_STRING274)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;


					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().AllowEdit(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}

void CDCLS2TreeReportView::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();
	if (pRow != NULL)
	{
		CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{	
			m_from_d_class= pRec->getColumnFloat(COLUMN_3); //used to temporarly store the value
			m_tr�dslag = pRec->getColumnText(COLUMN_2);
			int nGrpID = pRec->getColumnInt(COLUMN_0);
			setGroupID(nGrpID);
			setGroupName(nGrpID);
			m_bIsDirty = TRUE;	// Notify that something's happend on the grid; 070530 p�d
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)
}

void CDCLS2TreeReportView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	int nIndex = -1;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
		if (pRecItem != NULL)
		{
			CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)pRecItem->GetRecord();
			if (pRec != NULL)
			{
				// Set specie id in Tr�dID column; 070515 p�d		
				CXTPReportColumn *pColumn2 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_2);
				if (pColumn2 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn2);
						if (pEdOptions != NULL)
						{
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pColumn2));
							if (pConst != NULL)
							{
								pRec->setColumnInt(COLUMN_1,(int)pConst->m_dwData);
								// User changed specie; 070521 p�d
								m_bIsDirty = TRUE;
							}	// if (pConst != NULL)
						}	// if (pEdOptions != NULL)
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
				}	// if (pColumn2 != NULL)
				// Set the "to d.class" depending on the "from d.class" and "diameter setting"		

				// Set the "to d.class" depending on the "from d.class" and "diameter setting"		
				//#2011
				CXTPReportColumn *pColumn3 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_3);
				if (pColumn3 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn3->GetIndex() || 
						pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn3);
						int currentRowIndex=pRec->GetIndex();
						if (pEdOptions != NULL && currentRowIndex>=0)
						{										
							CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
							double diamClass=0.0, fromDClass=pRec->getColumnFloat(COLUMN_3), toDClass=0.0;

							CString tr�dslag=pRec->getColumnText(COLUMN_2);
							bool overlappingValue=false;
							if (pFrame != NULL)
							{
								diamClass=pFrame->getDiamClass();
							}

							toDClass=fromDClass+diamClass;

							CXTPReportRecords *records=GetReportCtrl().GetRecords();
							double rowFromDClass=0.0, rowToDClass=0.0;
							CString rowTr�dslag;

							for(int i=0;i<records->GetCount();i++)
							{ //check the records if the value is overlapping any existing.
								CTraktDCLSTreeReportRec *pRowRec = (CTraktDCLSTreeReportRec*)records->GetAt(i);

								rowFromDClass=pRowRec->getColumnFloat(COLUMN_3);
								rowToDClass=pRowRec->getColumnFloat(COLUMN_4);
								rowTr�dslag=pRowRec->getColumnText(COLUMN_2);

								//Do not compare with the value that is in the same cells
								if(i!=currentRowIndex)
								{	
									//check the from d.klass och to d.klass value so it does not overlapp any other if it is the same tree-class.
									if((!tr�dslag.IsEmpty() && tr�dslag.CompareNoCase(rowTr�dslag)==0) && ((fromDClass>=rowFromDClass && fromDClass<rowToDClass) ||
										(toDClass>rowFromDClass && toDClass<=rowToDClass)))
									{ 
										overlappingValue=true;
										break;
									}
								}
							}

							if(toDClass>MAX_TO_D_CLASS || overlappingValue)
							{
								CString sMsg;

								if(overlappingValue)
								{
									sMsg.Format(m_sDCLSErrorMsg6,tr�dslag, (float)fromDClass, (float)toDClass,(float)rowFromDClass,(float)rowToDClass);
									pRec->setColumnFloat(COLUMN_4,toDClass);
								}
								else
								{
									sMsg.Format(m_sDCLSErrorMsg5,MAX_TO_D_CLASS);
								}
								//go back to the value that was there before the change
								pRec->setColumnFloat(COLUMN_3,m_from_d_class);
								pRec->setColumnFloat(COLUMN_4,m_from_d_class+diamClass);
								pRec->setColumnText(COLUMN_2,m_tr�dslag);

								UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
							}
							else
							{										
								pRec->setColumnFloat(COLUMN_4,toDClass);
								m_bIsDirty = TRUE;
							}		
						}
					}	
				}

				// Get column for Group ID and compare entered value
				// to Plot data (Groups), for this Trakt; 070515 p�d
				CXTPReportColumn *pColumn0 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_0);
				if (pColumn0 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn0->GetIndex())
					{
						CPageThreeFormView *pView = (CPageThreeFormView *)getFormViewByID(IDD_FORMVIEW9);
						if (pView != NULL)
						{
							pView->isPlotGroupID(pRec->getColumnInt(COLUMN_0));
						}
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn0->GetIndex())
				}	// if (pColumn0 != NULL)

				// Get column for number of trees and check that user entered a value
				// within limit, else tell user; 071031 p�d
				CXTPReportColumn *pColumn6 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_6);
				if (pColumn6 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn6->GetIndex())
					{
						CXTPReportRow *pRow = pItemNotify->pRow;
						if (pRow != NULL)
						{
							CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)pRow->GetRecord();
							if (pRec != NULL)
							{
								long nNumOf = pRec->getColumnInt(COLUMN_6);
								CString S;
								if (nNumOf > NUMOF_TREES_IN_DCLS)
								{
									S.Format(_T("%s\n\n%s: %s\n%s: %.1f - %.1f\n\n%s: %d\n\n"),
										m_sDCLSErrorMsg1,
										m_sDCLSErrorMsg2,pRec->getColumnText(COLUMN_2),
										m_sDCLSErrorMsg3,pRec->getColumnFloat(COLUMN_3),pRec->getColumnFloat(COLUMN_4),
										m_sDCLSErrorMsg4,NUMOF_TREES_IN_DCLS);
									UMMessageBox(this->GetSafeHwnd(),S,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
									pRec->setColumnFloat(COLUMN_6,0.0);
								}	// if (nNumOf > NUMOF_TREES_IN_DCLS)
							}	// if (pRec != NULL)
						}	// if (pRow != NULL)
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn6->GetIndex())
				}	// if (pColumn6 != NULL)

			}	// if (pRec != NULL)
		}	// if (pRecItem != NULL)
	}	// if (pItemNotify != NULL)
}

void CDCLS2TreeReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
/* Commented out 2008-01-14 p�d
	EditItem not used insted
	-------------------------------------------------------
	pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
	-------------------------------------------------------
	have been added to editable columns.

	if (pItemNotify->pRow && pItemNotify->pColumn)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0 || 
				pItemNotify->pColumn->GetItemIndex() == COLUMN_2 ||
				pItemNotify->pColumn->GetItemIndex() == COLUMN_3 ||
				pItemNotify->pColumn->GetItemIndex() == COLUMN_4 ||
				pItemNotify->pColumn->GetItemIndex() == COLUMN_6)
		{
			XTP_REPORTRECORDITEM_ARGS itemArgs(&GetReportCtrl(), pItemNotify->pRow, pItemNotify->pColumn);
			GetReportCtrl().EditItem(&itemArgs);

		}
	}
*/
}

void CDCLS2TreeReportView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
/* Commented out 2008-01-14 p�d
	EditItem not used insted
	-------------------------------------------------------
	pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
	-------------------------------------------------------
	have been added to editable columns.

//	if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
//	{
		CXTPReportColumn *pColumn = GetReportCtrl().GetFocusedColumn();
		CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
		if (pColumn != NULL && pRow != NULL)
		{
			if (pColumn->GetItemIndex() == COLUMN_0 || 
					pColumn->GetItemIndex() == COLUMN_2 ||
					pColumn->GetItemIndex() == COLUMN_3 ||
					pColumn->GetItemIndex() == COLUMN_4 ||
					pColumn->GetItemIndex() == COLUMN_6)
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&GetReportCtrl(), pRow, pColumn);
				GetReportCtrl().EditItem(&itemArgs);
			}	// if (pColumn->GetIndex() == 2)
		}	// if (pColumn != NULL && pRow != NULL)
//	}	// if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
*/
}


void CDCLS2TreeReportView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenuW(MF_STRING, ID_SHOW_FIELDCHOOSER1, m_sFieldChooser);
	menu.AppendMenuW(MF_STRING, ID_PREVIEW1, m_sPrintPreview);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER1 :
		{
			CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
			if (pWnd != NULL)
			{
				BOOL bShow = !pWnd->m_wndFieldChooserDlg12.IsVisible();
				pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg12, bShow, FALSE);
			}
		}
		break;

		case ID_PREVIEW1 :
			OnPrintPreview();
		break;

	}
	menu.DestroyMenu();
}


void CDCLS2TreeReportView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

	// Disable print preview button on Toolbar. This is because if user clicks the
	// preview button there'll be multiple instances on Prieview.
	// We doesn't wan't that; 071218 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,TRUE,FALSE);
	}

}

void CDCLS2TreeReportView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	// Enable print preview button on Toolbar on ending preview; 071218 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,TRUE,TRUE);
	}

	CMyReportView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CDCLS2TreeReportView::doRunPrintPreview(void)
{
	OnPrintPreview();
}

void CDCLS2TreeReportView::OnFilePrint()
{
	CMyReportView::OnFilePrint();
}

void CDCLS2TreeReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_DCLS2_TREE_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CDCLS2TreeReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_DCLS2_TREE_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

