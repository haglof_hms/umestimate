#include "stdafx.h"
#include "afxwin.h"
#include "Resource.h"

#include "StandEntryDoc.h"

#include "InformationFormView.h"

#include "XTPPreviewView.h"

// CMDIInformationView

IMPLEMENT_DYNCREATE(CMDIInformationView, CHtmlView)

BEGIN_MESSAGE_MAP(CMDIInformationView, CHtmlView)
	//{{AFX_MSG_MAP(CMDIInformationView)
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
//	ON_WM_SIZE()
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIInformationView::CMDIInformationView()
{
}

CMDIInformationView::~CMDIInformationView()
{
}

int CMDIInformationView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sLangFN;
	CString sAbrevLang;
	if (CHtmlView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Setup language filename; 051214 p�d
	sAbrevLang = getLangSet();
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,sAbrevLang,LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

void CMDIInformationView::OnInitialUpdate() 
{
  CSize size(MIN_X_SIZE_INFOVIEW,MIN_Y_SIZE_INFOVIEW);
	SetScrollSizes(MM_TEXT,size);
	
	Navigate(_T(""));

}

BOOL CMDIInformationView::OnEraseBkgnd(CDC*)
{
	return TRUE;
}

void CMDIInformationView::OnSize(UINT nType, int cx, int cy)
{
	CHtmlView::OnSize(nType, cx, cy);
}

BOOL CMDIInformationView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if (!CHtmlView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;

}

// CMDIInformationView drawing

void CMDIInformationView::OnDraw(CDC* pDC)
{
/*
	int i;
	CRect rect;
	int nTab[] = {10,10,10,10,10,10,10};
	CString S;

	GetWindowRect(&rect);

	pDC->TextOutA(10,10,"Prislista:",10);

	pDC->TextOutA(10,40,"Kval./Dcls:",11);
	for (i = 0;i < 10;i++)
	{
		S.Format("%d",120 + (i*20));	
		pDC->TextOutA(50 + 35*(i+1),40,S,S.GetLength());
	}
	pDC->TextOutA(10,70,"Kval 1",6);
	pDC->TextOutA(10,85,"Kval 2",6);
	pDC->TextOutA(10,100,"Kval 3",6);
	pDC->TextOutA(10,115,"Kval 4",6);

	SetScrollSizes(MM_TEXT, CSize(50 + 35*(i+1),rect.bottom));
*/
}


BOOL CMDIInformationView::OnPreparePrinting(CPrintInfo* pInfo)
{
	ASSERT(pInfo) ;
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMDIInformationView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMDIInformationView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}
/*
void CMDIInformationView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	CString	text ;
	CSize	csText ;

	ASSERT(pDC) ;
	ASSERT(pInfo) ;

	int i;
	CString S;

	pDC->DrawText("Prislista",9,pInfo->m_rectDraw,DT_CENTER | DT_TOP);

//	pDC->TextOut(10,40,"Kval./Dcls:",11);
	pDC->SetTextColor(RGB(0,255,0,));

	for (i = 0;i < 10;i++)
	{
		S.Format(" %d",120 + (i*20));	
		csText = pDC->GetTextExtent(S);
		pDC->TextOut(215 + ((csText.cx+20)*(i+1)),csText.cy*5,S,S.GetLength());
		pDC->MoveTo(220 + ((csText.cx+20)*(i+1)),csText.cy*5);
		pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*16);
	}
	pDC->SelectObject(m_fontLabel);

	pDC->MoveTo(10,csText.cy*6);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*6);

	pDC->TextOut(10,csText.cy*6 + 30,"Kvalite 1",6);
	pDC->MoveTo(10,csText.cy*8-2);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*8-2);

	pDC->TextOut(10,csText.cy*8 + 30,"Kvalite 2",6);
	pDC->MoveTo(10,csText.cy*10-2);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*10-2);

	pDC->TextOut(10,csText.cy*10 + 30,"Kvalite 3",6);
	pDC->MoveTo(10,csText.cy*12-2);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*12-2);
	
	pDC->TextOut(10,csText.cy*12 + 30,"Kvalite 4",6);
	pDC->MoveTo(10,csText.cy*14-2);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*14-2);

	pDC->MoveTo(10,csText.cy*16-2);
	pDC->LineTo(220 + ((csText.cx+20)*(i+1)),csText.cy*16-2);

}
*/

// CMDIInformationView diagnostics

#ifdef _DEBUG
void CMDIInformationView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CMDIInformationView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CMDIFrameDoc* CTabbedViewView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CTabbedViewDoc*)m_pDocument;
}
#endif //_DEBUG
