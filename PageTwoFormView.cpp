// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"
#include "PageTwoFormView.h"
#include "SpecieDataDialog.h"
#include "SpcSettingsDlg.h"
#include "HandleSettingsDlg.h"
#include "ResLangFileReader.h"


// CPageTwoFormView

IMPLEMENT_DYNCREATE(CPageTwoFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPageTwoFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, IDC_REPORT, OnReportColumnRClick)
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnReportItemClick)
	ON_NOTIFY(NM_RCLICK, IDC_REPORT, OnReportItemRClick)
	ON_COMMAND(ID_TBBTN_COLUMNS_DLG, OnShowFieldChooser)
	ON_NOTIFY(XTP_NM_REPORT_ROWEXPANDED, IDC_REPORT2, OnReport1RowExpandChanged)
END_MESSAGE_MAP()

CPageTwoFormView::CPageTwoFormView()
	: CXTResizeFormView(CPageTwoFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDirty = FALSE;
	m_pDB = NULL;
	m_enumAction = NO_ACTION;

	m_pRotpostHeaderFont = new CFont;
}

CPageTwoFormView::~CPageTwoFormView()
{
}

void CPageTwoFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL2_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL2_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL2_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL2_8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL2_9, m_wndLbl9);
/*
	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl11);
	DDX_Control(pDX, IDC_LBL3_3, m_wndLbl12);
*/
	DDX_Control(pDX, IDC_EDIT1 , m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT28, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT29, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT30, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT31, m_wndEdit9);

	DDX_Control(pDX, IDC_GROUP2_2, m_wndGroup2);
	DDX_Control(pDX, IDC_GROUP2_3, m_wndGroup3);
	//}}AFX_DATA_MAP

}

void CPageTwoFormView::OnDestroy()
{
	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	if (m_pRotpostHeaderFont != NULL)
		delete m_pRotpostHeaderFont;

//	m_vecSpecies.clear();
	m_vecTraktPlot.clear();
	m_vecCTransaction_trakt_rotpost.clear();

	CXTResizeFormView::OnDestroy();	
}

void CPageTwoFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNS, &pWnd->m_wndFieldChooserDlg);
		m_wndReport.GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (! m_bInitialized )
	{
			SetFlag(xtResizeNoTransparentGroup);
//		VERIFY(CXTPReportControl::UseReportCustomHeap());
//		CXTPReportControl::UseRowBatchAllocation();
	
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit5.SetEnabledColor(BLACK,INFOBK);
		m_wndEdit5.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit5.SetReadOnly();

		m_wndEdit6.SetEnabledColor(BLACK,INFOBK);
		m_wndEdit6.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit6.SetReadOnly();

		m_wndEdit7.SetEnabledColor(BLACK,INFOBK);
		m_wndEdit7.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit7.SetReadOnly();

		m_wndEdit8.SetEnabledColor(BLACK,INFOBK);
		m_wndEdit8.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit8.SetReadOnly();

		m_wndEdit9.SetEnabledColor(BLACK,INFOBK);
		m_wndEdit9.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit9.SetReadOnly();
	/*
		m_wndLbl10.SetTextColor( BLUE );
		m_wndLbl10.SetLblFontEx(-1,FW_BOLD,TRUE);
		m_wndLbl11.SetTextColor( BLUE );
		m_wndLbl11.SetLblFontEx(-1,FW_BOLD,TRUE);
		m_wndLbl12.SetTextColor( BLUE );
		m_wndLbl12.SetLblFontEx(-1,FW_BOLD,TRUE);
*/
//		getSpeciesFromDB();
		setupReport();

		m_bInitialized = TRUE;

		LoadReportState();
	}
}

BOOL CPageTwoFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CPageTwoFormView::OnSetFocus(CWnd*)
{

	// Make sure the New Toolbarbutton on Main Toolbar is disabled.
	// To add a new Specie/Assortments, use View toolbar add button; 070308 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		// This is for running the OnSetFocus(), setting HMSShell Toolbars; 070315 p�d
		CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
																							CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(0)->GetHandle()));
		{
			pView->SetFocus();
		}
	}

	// When this view gets focus, anable the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,FALSE,FALSE);
	}

/*	Commented out 2008-10-13 p�d
	Not used, slows down program!; 081013 p�d
	if (::IsWindow(m_wndReport.GetSafeHwnd()))
	{
		populateData();
	}
*/

}


// CPageTwoFormView diagnostics

#ifdef _DEBUG
void CPageTwoFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPageTwoFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CPageTwoFormView message handlers

void CPageTwoFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);
	int horz=GetScrollPos(SB_HORZ);
	int vert=GetScrollPos(SB_VERT);

	if (m_wndGroup2.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndGroup2,7-horz,7-vert,800,50);
	}

	if (m_wndGroup3.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndGroup3,7-horz,60-vert,800,215,TRUE);
	}

	if (m_wndRotpost1.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndRotpost1,20-horz,75-vert,370,134+60);
	}

	if (m_wndRotpost2.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndRotpost2,395-horz,75-vert,400,134+60);
	}

	if (m_wndReport.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		int d_width=((rect.right - 20)>800)?(rect.right - 20):800;
		int d_height=((rect.bottom - 285)>215)?(rect.bottom - 285):215;
		setResize(&m_wndReport,10-horz,200+80-vert,d_width,d_height+vert);
	}
}

void CPageTwoFormView::populateRotpostInfo(BOOL is_data)
{
	BOOL bAddRecord = FALSE;
	UINT ui1,ui2;
	int nTranspDist1;
	int nTranspDist2;
	int nSetAs;
	double fSumGrotTonne = 0.0;
	double fSumGrotPrice = 0.0;
	double fSumGrotCost = 0.0;
	double fSumGrotNetto = 0.0;
	vecTransaction_trakt_rotpost_spc vecRotpostSpc;
	CTransaction_trakt_set_spc recRotSpc;
	vecTransaction_trakt_rotpost_other vecRotpostOtc;
	vecTransactionTraktSetSpc vecTraktSetSpc;
	m_wndGroup3.SetWindowText(m_sGroup3_cap2);
	CXTPReportRecord *pRecord = NULL;
	CTraktRotpostRec *pRecRot = NULL;
	CTraktRotpostCostsRec *pRecCost = NULL;
	std::map<int,int> vecTranspDist1PerSpc;
	std::map<int,int> vecTranspDist2PerSpc;

	CString S;

	CString sVolume;
	CString sPulpVol;
	CString sValue;
	CString sNetto;
	CString sCutting;
	CString sHarvester;
	CString sSkotare;
	CString sTransport;
	CString sTransportToRoad;
	CString sTransportToIndustry;
	CString sRest;
	CString sPermanent;
	CString sPerM3Fub1;
	CString sPerM3Fub2;
	CString sPerM3Fub3;
	CString sPerM3Fub4;
	CString sPerM3Fub5;
	CString sPerM3Fub6;
	CString sSumCost;
	CString sGrotTonne;
	CString sSumGrotPrice;
	CString sSumGrotCost;
	CString sSumGrot;

	sVolume = _T("0");
	sPulpVol = _T("0");
	sValue =  _T("0");
	sNetto =  _T("0");
	sCutting =  _T("0");
	sHarvester = _T("0");
	sSkotare = _T("0");
	sTransport = _T("0");
	sTransportToRoad = _T("0");
	sTransportToIndustry = _T("0");
	sRest = _T("0");
	sPermanent = _T("");
	sPerM3Fub1 = _T("0");
	sPerM3Fub2 = _T("0");
	sPerM3Fub3 = _T("0");
	sPerM3Fub4 = _T("0");
	sPerM3Fub5 = _T("0");
	sPerM3Fub6 = _T("0");
	sSumCost = _T("0");
	sGrotTonne = _T("0");
	sSumGrotPrice = _T("0");
	sSumGrotCost = _T("0");
	sSumGrot = _T("0");

	getTraktRotpost(m_pTraktRecord->getTraktID());

	//--------------------------------------------------------------------
	// Grot added 100316 p�d
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			if (m_vecTraktData[i].getTDataType() == STMP_LEN_WITHDRAW && 
					m_vecTraktData[i].getTDataTraktID() == m_recTraktRotpost.getRotTraktID())
				fSumGrotTonne += m_vecTraktData[i].getGrot();
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)
	if (fSumGrotTonne/1000.0 >= 10.0)
		sGrotTonne.Format(L"%.0f",fSumGrotTonne/1000.0 /* ton */);
	else if (fSumGrotTonne/1000.0 < 10.0 && fSumGrotTonne/1000.0 >= 1.0)
		sGrotTonne.Format(L"%.1f",fSumGrotTonne/1000.0 /* ton */);
	else if (fSumGrotTonne/1000.0 < 1.0 && fSumGrotTonne/1000.0 >= 0.1)
		sGrotTonne.Format(L"%.2f",fSumGrotTonne/1000.0 /* ton */);
	else
		sGrotTonne.Format(L"%.3f",fSumGrotTonne/1000.0 /* ton */);
	//--------------------------------------------------------------------

	// Try to collect data for "Rotpost/Tr�dslag".
	// "Huggning",Transport till v�g och Transport till Indistri"; 080317 p�d
	if (m_pDB != NULL)
	{
		m_pDB->getRotpostSpc(vecRotpostSpc,m_recTraktRotpost.getRotTraktID());
		m_pDB->getRotpostOtc(vecRotpostOtc,m_recTraktRotpost.getRotTraktID());
		m_pDB->getTraktSetSpc(vecTraktSetSpc,m_recTraktRotpost.getRotTraktID());
	}	// if (m_pDB != NULL)

	//--------------------------------------------------------------------
	// Calculate price for GROT; 100316 p�d
	if (m_vecTraktData.size() > 0 && vecTraktSetSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			if (m_vecTraktData[i].getTDataType() == STMP_LEN_WITHDRAW && 
					m_vecTraktData[i].getTDataTraktID() == m_recTraktRotpost.getRotTraktID())
			{
				for (UINT ii = 0;ii < vecTraktSetSpc.size();ii++)
				{
					if (m_vecTraktData[i].getSpecieID() == vecTraktSetSpc[ii].getSpcID())
					{
						// Grot in tonne
						fSumGrotPrice += (m_vecTraktData[i].getGrot()/1000.0)*vecTraktSetSpc[ii].getGrotPrice(); 
						fSumGrotCost += (m_vecTraktData[i].getGrot()/1000.0)*vecTraktSetSpc[ii].getGrotCost();
					}
				}
			}
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}
	sSumGrotPrice = formatNumber(fSumGrotPrice,0);
	sSumGrotCost = formatNumber(fSumGrotCost,0);
	// Calc. GROT Netto; 100316 p�d
	fSumGrotNetto = fSumGrotPrice - fSumGrotCost;
	sSumGrot = formatNumber(fSumGrotNetto,0);

	// Check origin of "Rotpost"
	if (m_nRotpostOrigin == ROTPOST_ORIGIN1) // || m_nRotpostOrigin == ROTPOST_ORIGIN2_1)
	{
		m_wndGroup3.SetWindowText(m_sGroup3_cap1);
		if (vecRotpostSpc.size() > 0)
		{
			for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
			{
				CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
				// We'll try to find out the distance set in "esti_trakt_set_spc_table"; 080318 p�d
				if (vecTraktSetSpc.size() > 0)
				{
					for (ui2 = 0;ui2 < vecTraktSetSpc.size();ui2++)
					{
						recRotSpc = vecTraktSetSpc[ui2];
						if (rec.getRotSpcID_pk() == recRotSpc.getSpcID())
						{
							vecTranspDist1PerSpc[rec.getRotSpcID_pk()] = recRotSpc.getTranspDist1();
							vecTranspDist2PerSpc[rec.getRotSpcID_pk()] = recRotSpc.getTranspDist2();
							break;
						}	// if (rec.getRotSpcID_pk() == vecTraktSetSpc[ui2].getSpcID())
					}	// for (ui2 = 0;ui2 < vecTraktSetSpc.size();ui2++)
				}	// if (vecTraktSetSpc.size() > 0)
			}	// for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
		}	// if (vecRotpostSpc.size() > 0 && pRecord != NULL)

		if (is_data)
		{
			// Check volume and pulpvolume, to see if we should; 080919 p�d
			// add decimals: 
			//	- Volume > 10.0 dec 0
			//	- Volume < 10.0 and > 1.0 dec 1
			//	- Volume < 1.0 dec 2
			if (m_recTraktRotpost.getVolume() >= 10.0)
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),0);
			else if (m_recTraktRotpost.getVolume() < 10.0 && m_recTraktRotpost.getVolume() >= 1.0)
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),1);
			else
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),2);
	
			if (m_recTraktRotpost.getCost9() >= 10.0)
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),0);
			else if (m_recTraktRotpost.getCost9() < 10.0 && m_recTraktRotpost.getCost9() >= 1.0)
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),1);
			else
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),2);

			sValue = formatNumber(m_recTraktRotpost.getValue(),0);
			// Added Sum. for GROT to NETTO; 100316 p�d
			sNetto = formatNumber(m_recTraktRotpost.getNetto(),0);
			sCutting = formatNumber(m_recTraktRotpost.getCost1(),0);
			sTransportToRoad = formatNumber(m_recTraktRotpost.getCost5(),0);
			sTransportToIndustry = formatNumber(m_recTraktRotpost.getCost8(),0);
			sRest = formatNumber(m_recTraktRotpost.getCost6(),0);
			sPermanent = formatNumber(m_recTraktRotpost.getCost7(),0);
			sHarvester = formatNumber(m_recTraktRotpost.getCost2(),0);
			sSkotare = formatNumber(m_recTraktRotpost.getCost3(),0);

			sSumCost = formatNumber(m_recTraktRotpost.getCost1() +
														  m_recTraktRotpost.getCost2() +
															m_recTraktRotpost.getCost3() +
															m_recTraktRotpost.getCost5() +
														  m_recTraktRotpost.getCost6() +
														  m_recTraktRotpost.getCost7() +
														  m_recTraktRotpost.getCost8(),0);

			if (m_recTraktRotpost.getVolume() > 0.0)
			{
				if (m_recTraktRotpost.getCost1() > 0.0)	sPerM3Fub1.Format(_T("%.0f"),m_recTraktRotpost.getCost1()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost2() > 0.0)	sPerM3Fub2.Format(_T("%.0f"),m_recTraktRotpost.getCost2()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost3() > 0.0)	sPerM3Fub3.Format(_T("%.0f"),m_recTraktRotpost.getCost3()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost4() > 0.0)	sPerM3Fub4.Format(_T("%.0f"),m_recTraktRotpost.getCost4()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost5() > 0.0)	sPerM3Fub5.Format(_T("%.0f"),m_recTraktRotpost.getCost5()/m_recTraktRotpost.getVolume());
			}
			// Handle just volume for Pulpwood; 080318 p�d
			if (m_recTraktRotpost.getCost9() > 0.0)
			{
				if (m_recTraktRotpost.getCost8() > 0.0)	
					sPerM3Fub6.Format(_T("%.0f"),m_recTraktRotpost.getCost8()/m_recTraktRotpost.getCost9());
			}
		}

		// Add costs to m_wndRotpost1
		m_wndRotpost1.ResetContent();
		CTraktRotpostCostsRec *pRecCost = NULL;
		CTraktRotpostRec *pRecRot = NULL;
		m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostCutting,sCutting,m_sKR,sPerM3Fub1,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
		
		m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostHarvester,sHarvester,m_sKR,sPerM3Fub2,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
		
		m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostSkotare,sSkotare,m_sKR,sPerM3Fub3,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
/*	NOT USED, ONLY "Fasta"; 080918 p�d
		m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostRest,sRest,m_sKR,sPerM3Fub4,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
*/
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
		//	ROTPOST_ORIGIN2_1
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
		if (m_nRotpostOrigin == ROTPOST_ORIGIN1)
		{
			pRecord =	m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sTransportToRoad,sTransportToRoad,m_sKR,sPerM3Fub5,m_sKR_M3FUB)));
			pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
			// Check if there's data to add to child; 080317 p�d
			if (vecRotpostSpc.size() > 0 && pRecord != NULL)
			{
				for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
				{
					CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
					bAddRecord = (!rec.getSpcName().IsEmpty());

					if (bAddRecord)
					{
						// We'll try to find out the distance set in "esti_trakt_set_spc_table"; 080318 p�d
						nTranspDist1 = vecTranspDist1PerSpc[rec.getRotSpcID_pk()];
						pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T(" - %s (%d %s)"),
																																															rec.getSpcName(),
																																															nTranspDist1,
																																															m_sMeter),
																										 																					formatData(_T("%.0f"),rec.getCost2()),
																																															m_sKR,
																																															formatData(_T("%.0f"),rec.getCost1()),
																																															m_sKR_M3FUB)));
						pRecCost->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
					}
				}
			}

			if (bAddRecord)
			{
				pRecord =	m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sTransportToIndustry,sTransportToIndustry,m_sKR,sPerM3Fub6,m_sKR_M3FUB)));
				pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
			}
			// Check if there's data to add to child; 080317 p�d
			if (vecRotpostSpc.size() > 0 && pRecord != NULL)
			{
				for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
				{
					CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
					bAddRecord = (!rec.getSpcName().IsEmpty());

					if (bAddRecord)
					{

						nTranspDist2 = vecTranspDist2PerSpc[rec.getRotSpcID_pk()];
						pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T(" - %s (%d %s)"),
																																															rec.getSpcName(),
																																															nTranspDist2,
																																															m_sKiloMeter),
																										 																					formatData(_T("%.0f"),rec.getCost6()),
																																															m_sKR,
																																															formatData(_T("%.0f"),rec.getCost5()),
																																															m_sKR_M3FUB)));
						pRecCost->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
					}
				}
			}
		}	// else 	if (m_nRotpostOrigin == ROTPOST_ORIGIN2_1)
		if (bAddRecord)
		{
			pRecord =	m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostPermanent,sPermanent,m_sKR,_T(""),_T(""))));
			pRecCost->setColumnBold(1,COLUMN_2);
		}
		CString S;	
		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostOtc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostOtc.size();ui1++)
			{
				CTransaction_trakt_rotpost_other rec = vecRotpostOtc[ui1];
				if (rec.getType() == OTHER_COST_2)
				{
//					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData("%s",rec.getText()),
//																										 														 _T(""),_T(""),_T(""),_T(""))));
					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T("%s"),rec.getText()),
																										 														 formatNumber(rec.getCost2(),0),
																										 														 //formatData(_T("%.0f"),rec.getCost2()),
																																								 m_sKR,_T(""),_T(""))));
					pRecCost->setColumnTextColor(RGB(0,0,255),3,COLUMN_0,COLUMN_1,COLUMN_2);
				}
			}
		}
/*
			S.Format(_T("ROTPOST_ORIGIN1\nm_nRotpostOrigin %d\nm_recTraktRotpost.getValue() %f\nValue %s"),
				m_nRotpostOrigin,m_recTraktRotpost.getValue(),sValue);
			UMMessageBox(S);
*/
		// Add sum. to m_wndRotpost2
		m_wndRotpost2.ClearReport();
		m_wndRotpost2.ResetContent();
#ifdef CALCULATE_GROT
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sVolume),(sVolume),(m_sM3FUB))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sPulpVol),(sPulpVol),(m_sM3FUB))));
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sGrot,sGrotTonne,m_sTonne)));
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sValueTimber),(sValue),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sCostCap),(sSumCost),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sSumGrotPrice,L"",L"",sSumGrotPrice,m_sKR)));
		pRecRot->setColumnBold(1,COLUMN_4);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sSumGrotCost,L"",L"",sSumGrotCost,m_sKR)));
		pRecRot->setColumnBold(1,COLUMN_4);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sNETTO,sNetto,m_sKR,sSumGrot,m_sKR,true)));
		pRecRot->setColumnFont(COLUMN_1,FALSE);
		pRecRot->setColumnFont(COLUMN_2,TRUE);
		pRecRot->setColumnFont(COLUMN_3,TRUE);
		pRecRot->setColumnFont(COLUMN_4,TRUE);
		pRecRot->setColumnFont(COLUMN_5,TRUE);
#else
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sVolume),(sVolume),(m_sM3FUB))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sPulpVol),(sPulpVol),(m_sM3FUB))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sValueTimber),(sValue),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sCostCap),(sSumCost),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sNETTO),(sNetto),(m_sKR),L"",L"",true)));
		pRecRot->setColumnFont(COLUMN_1,FALSE);
		pRecRot->setColumnFont(COLUMN_2,TRUE);
		pRecRot->setColumnFont(COLUMN_3,TRUE);
#endif

		m_wndRotpost1.Populate();
		m_wndRotpost1.UpdateWindow();
		m_wndRotpost2.Populate();
		m_wndRotpost2.UpdateWindow();
	}
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
	//	ROTPOST_ORIGIN2_2
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
	else if (m_nRotpostOrigin == ROTPOST_ORIGIN2_1)
	{
		vecTranspDist1PerSpc.clear();
		vecTranspDist2PerSpc.clear();
		if (vecRotpostSpc.size() > 0)
		{
			for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
			{
				CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
				// We'll try to find out the distance set in "esti_trakt_set_spc_table"; 080318 p�d
				if (vecTraktSetSpc.size() > 0)
				{
					for (ui2 = 0;ui2 < vecTraktSetSpc.size();ui2++)
					{
						recRotSpc = vecTraktSetSpc[ui2];
						if (rec.getRotSpcID_pk() == recRotSpc.getSpcID())
						{
							vecTranspDist1PerSpc[rec.getRotSpcID_pk()] = recRotSpc.getTranspDist1();
							vecTranspDist2PerSpc[rec.getRotSpcID_pk()] = recRotSpc.getTranspDist2();
							break;
						}	// if (rec.getRotSpcID_pk() == vecTraktSetSpc[ui2].getSpcID())
					}	// for (ui2 = 0;ui2 < vecTraktSetSpc.size();ui2++)
				}	// if (vecTraktSetSpc.size() > 0)
			}	// for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
		}	// if (vecRotpostSpc.size() > 0 && pRecord != NULL)

		if (is_data)
		{
			// Check volume and pulpvolume, to see if we should; 080919 p�d
			// add decimals: 
			//	- Volume > 10.0 dec 0
			//	- Volume < 10.0 and > 1.0 dec 1
			//	- Volume < 1.0 dec 2
			if (m_recTraktRotpost.getVolume() >= 10.0)
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),0);
//				sVolume.Format(_T("%.0f"),m_recTraktRotpost.getVolume());
			else if (m_recTraktRotpost.getVolume() < 10.0 && m_recTraktRotpost.getVolume() >= 1.0)
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),1);
//				sVolume.Format(_T("%.1f"),m_recTraktRotpost.getVolume());
			else
				sVolume = formatNumber(m_recTraktRotpost.getVolume(),2);
//				sVolume.Format(_T("%.2f"),m_recTraktRotpost.getVolume());
	
			if (m_recTraktRotpost.getCost9() >= 10.0)
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),0);
//				sPulpVol.Format(_T("%.0f"),m_recTraktRotpost.getCost9());
			else if (m_recTraktRotpost.getCost9() < 10.0 && m_recTraktRotpost.getCost9() >= 1.0)
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),1);
//				sPulpVol.Format(_T("%.1f"),m_recTraktRotpost.getCost9());
			else
				sPulpVol = formatNumber(m_recTraktRotpost.getCost9(),2);

			sValue = formatNumber(m_recTraktRotpost.getValue(),0);
			// Added Sum. for GROT to NETTO; 100316 p�d
			sNetto = formatNumber(m_recTraktRotpost.getNetto(),0);
			sCutting = formatNumber(m_recTraktRotpost.getCost1(),0);
			sTransportToRoad = formatNumber(m_recTraktRotpost.getCost5(),0);
			sTransportToIndustry = formatNumber(m_recTraktRotpost.getCost8(),0);
			sRest = formatNumber(m_recTraktRotpost.getCost6(),0);
			sPermanent = formatNumber(m_recTraktRotpost.getCost7(),0);
			sSumCost = formatNumber(m_recTraktRotpost.getCost1() +
														 m_recTraktRotpost.getCost5() +
														 m_recTraktRotpost.getCost6() +
														 m_recTraktRotpost.getCost7() +
														 m_recTraktRotpost.getCost8(),0);

			if (m_recTraktRotpost.getVolume() > 0.0)
			{
				if (m_recTraktRotpost.getCost1() > 0.0)	
					sPerM3Fub1 = formatNumber(m_recTraktRotpost.getCost1()/m_recTraktRotpost.getVolume(),0);
//					sPerM3Fub1.Format(_T("%.0f"),m_recTraktRotpost.getCost1()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost5() > 0.0)	
					sPerM3Fub3 = formatNumber(m_recTraktRotpost.getCost5()/m_recTraktRotpost.getVolume(),0);
//					sPerM3Fub3.Format(_T("%.0f"),m_recTraktRotpost.getCost5()/m_recTraktRotpost.getVolume());
				if (m_recTraktRotpost.getCost6() > 0.0)	
					sPerM3Fub5 = formatNumber(m_recTraktRotpost.getCost6()/m_recTraktRotpost.getVolume(),0);
//					sPerM3Fub5.Format(_T("%.0f"),m_recTraktRotpost.getCost6()/m_recTraktRotpost.getVolume());
			}
			// Handle just volume for Pulpwood; 080318 p�d
			if (m_recTraktRotpost.getCost9() > 0.0)
			{
				if (m_recTraktRotpost.getCost8() > 0)	
					sPerM3Fub4 = formatNumber(m_recTraktRotpost.getCost8()/m_recTraktRotpost.getCost9(),0);
//					sPerM3Fub4.Format(_T("%.0f"),m_recTraktRotpost.getCost8()/m_recTraktRotpost.getCost9());
			}
		}

		// Get CutCostSetAs to determine if "Huggningskostnad i tabell medelstam (m3fub)" (as m_nRotpostOrigin does not tell this); 150417 Peter
		nSetAs = -1;
		if( m_pDB->getTraktMiscData(m_pTraktRecord->getTraktID(),m_recTraktMiscData) )
		{
			xmlliteCostsParser *parser = new xmlliteCostsParser();
			if (parser != NULL)
			{
				if (parser->loadStream(m_recTraktMiscData.getXMLCosts() ))
				{
					parser->getCutCostSetAs(&nSetAs);	// 1 = "Huggningskostnad i kr/m3" 2 = "Huggningskostnad i tabell (dgv)"
				}
				delete parser;
			}
		}

		// Add costs to m_wndRotpost1
		m_wndRotpost1.ResetContent();
		// "Huggningskostnad"
		pRecord = m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostCutting,sCutting,m_sKR,sPerM3Fub1,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostSpc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
			{
				CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
				bAddRecord = (!rec.getSpcName().IsEmpty()) && nSetAs != 3; // Not per specie if "Huggningskostnad i tabell medelstam (m3fub)"; 150417 Peter

				if (bAddRecord)
				{

					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T("  %s"),rec.getSpcName()),
																										 															formatData(_T("%.0f"),rec.getCost4()),
																																									m_sKR,
																																									formatData(_T("%.0f"),rec.getCost3()),
																																									m_sKR_M3FUB)));
					pRecCost->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
				}
			}
		}
		// "Transportkostnad till v�g"
		pRecord = m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sTransportToRoad,sTransportToRoad,m_sKR,sPerM3Fub3,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostSpc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
			{
				CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
				// We'll try to find out the distance set in "esti_trakt_set_spc_table"; 080318 p�d
				nTranspDist1 = vecTranspDist1PerSpc[rec.getRotSpcID_pk()];
				bAddRecord = (!rec.getSpcName().IsEmpty());

				if (bAddRecord)
				{

					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T(" - %s (%d %s)"),
																																														rec.getSpcName(),
																																														nTranspDist1,
																																														m_sMeter),
																										 																				formatData(_T("%.0f"),rec.getCost2()),
																																														m_sKR,
																																														formatData(_T("%.0f"),rec.getCost1()),
																																														m_sKR_M3FUB)));
					pRecCost->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
				}
			}
		}
		// "Transportkostnad till Industri"
		pRecord = m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sTransportToIndustry,sTransportToIndustry,m_sKR,sPerM3Fub4,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);
		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostSpc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostSpc.size();ui1++)
			{
				CTransaction_trakt_rotpost_spc rec = vecRotpostSpc[ui1];
				nTranspDist2 = vecTranspDist2PerSpc[rec.getRotSpcID_pk()];
				bAddRecord = (!rec.getSpcName().IsEmpty());

				if (bAddRecord)
				{
					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T(" - %s (%d %s)"),
																																														 rec.getSpcName(),
																																														 nTranspDist2,
																																														 m_sKiloMeter),
																										 															formatData(_T("%.0f"),rec.getCost6()),
																																									m_sKR,
																																									formatData(_T("%.0f"),rec.getCost5()),
																																									m_sKR_M3FUB)));
						pRecCost->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);

				}
			}
		}
		// "�vrig ers�ttning (Intr�ng)"
/*	NOT USED; 080918 p�d
		pRecord = m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostRest,sRest,m_sKR,sPerM3Fub5,m_sKR_M3FUB)));
		pRecCost->setColumnBold(2,COLUMN_2,COLUMN_4);

		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostOtc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostOtc.size();ui1++)
			{
				CTransaction_trakt_rotpost_other rec = vecRotpostOtc[ui1];
				if (rec.getType() == OTHER_COST_1)
				{
					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),rec.getText(),
																										 														 formatData("%.0f",rec.getCost1()),
																																								 m_sKR,_T(""),_T(""))));
					pRecCost->setColumnTextColor(RGB(0,0,255),3,COLUMN_0,COLUMN_1,COLUMN_2);
				}
			}
		}
*/
		// "�vriga fasta kostnader"
		pRecord = m_wndRotpost1.AddRecord((pRecCost = new CTraktRotpostCostsRec(_T(""),m_sCostPermanent,sPermanent,m_sKR,_T(""),_T(""))));
		pRecCost->setColumnBold(1,COLUMN_2);
		// Check if there's data to add to child; 080317 p�d
		if (vecRotpostOtc.size() > 0 && pRecord != NULL)
		{
			for (ui1 = 0;ui1 < vecRotpostOtc.size();ui1++)
			{
				CTransaction_trakt_rotpost_other rec = vecRotpostOtc[ui1];
				if (rec.getType() == OTHER_COST_2)
				{
					pRecord->GetChilds()->Add((pRecCost = new CTraktRotpostCostsRec(_T(""),formatData(_T("%s"),rec.getText()),
																										 														 formatNumber(rec.getCost2(),0),
//																										 														 formatData(_T("%.0f"),rec.getCost2()),
																																								 m_sKR,_T(""),_T(""))));
					pRecCost->setColumnTextColor(RGB(0,0,255),3,COLUMN_0,COLUMN_1,COLUMN_2);
				}
			}
		}
/*
			S.Format(_T("ROTPOST_ORIGIN2_1\nm_nRotpostOrigin %d\nm_recTraktRotpost.getValue() %f\nValue %s"),
				m_nRotpostOrigin,m_recTraktRotpost.getValue(),sValue);
			UMMessageBox(S);
*/
		m_wndRotpost2.ClearReport();
		m_wndRotpost2.ResetContent();
#ifdef CALCULATE_GROT
		// Add sum. to m_wndRotpost2
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sVolume),(sVolume),(m_sM3FUB))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sPulpVol),(sPulpVol),(m_sM3FUB))));
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sGrot,sGrotTonne,m_sTonne)));
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sValueTimber),(sValue),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sCostCap),(sSumCost),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sSumGrotPrice,L"",L"",sSumGrotPrice,m_sKR)));
		pRecRot->setColumnBold(1,COLUMN_4);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),m_sSumGrotCost,L"",L"",sSumGrotCost,m_sKR)));
		pRecRot->setColumnBold(1,COLUMN_4);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sNETTO),(sNetto),(m_sKR),sSumGrot,m_sKR,true)));
		pRecRot->setColumnFont(COLUMN_1,FALSE);
		pRecRot->setColumnFont(COLUMN_2,TRUE);
		pRecRot->setColumnFont(COLUMN_3,FALSE);
		pRecRot->setColumnFont(COLUMN_4,TRUE);
		pRecRot->setColumnFont(COLUMN_5,TRUE);
#else
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sVolume),(sVolume),(m_sM3FUB))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sPulpVol),(sPulpVol),(m_sM3FUB))));
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sValueTimber),(sValue),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sCostCap),(sSumCost),(m_sKR))));
		pRecRot->setColumnBold(1,COLUMN_2);
		m_wndRotpost2.AddRecord(new CTraktRotpostRec());
		m_wndRotpost2.AddRecord((pRecRot = new CTraktRotpostRec(_T(""),(m_sNETTO),(sNetto),(m_sKR),L"",m_sKR,true)));
		pRecRot->setColumnFont(COLUMN_1,FALSE);
		pRecRot->setColumnFont(COLUMN_2,TRUE);
		pRecRot->setColumnFont(COLUMN_3,FALSE);
#endif
		m_wndRotpost1.Populate();
		m_wndRotpost1.UpdateWindow();
		m_wndRotpost2.Populate();
		m_wndRotpost2.UpdateWindow();
	}

	vecRotpostSpc.clear();
	vecRotpostOtc.clear();
	vecTraktSetSpc.clear();
	vecTranspDist1PerSpc.clear();
	vecTranspDist2PerSpc.clear();
}

BOOL CPageTwoFormView::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;
	CXTPReportHeader *pHeader = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, TRUE, FALSE))
		{
			TRACE0( "Failed to create m_wndReport.\n" );
			return FALSE;
		}
	}

	if (m_wndRotpost1.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndRotpost1.Create(this, IDC_REPORT2, TRUE, FALSE))
		{
			TRACE0( "Failed to create m_wndRotpost1.\n" );
			return FALSE;
		}
	}

	if (m_wndRotpost2.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndRotpost2.Create(this, 2, FALSE, FALSE))
		{
			TRACE0( "Failed to create m_wndRotpost2.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				// Also, in this method, set string values for labels etc.; 070220 p�d
				m_wndLbl5.SetWindowText((xml->str(IDS_STRING136)));
				m_wndLbl6.SetWindowText((xml->str(IDS_STRING137)));
				m_wndLbl7.SetWindowText((xml->str(IDS_STRING138)));
				m_wndLbl8.SetWindowText((xml->str(IDS_STRING139)));
				m_wndLbl9.SetWindowText((xml->str(IDS_STRING150)));

				m_sFieldChooser = (xml->str(IDS_STRING147));
				m_wndGroup2.SetWindowText((xml->str(IDS_STRING146)));

				m_sM3TO	= (xml->str(IDS_STRING207));
				m_sM3FUB	= (xml->str(IDS_STRING206));

				m_sPriceM3TO	= (xml->str(IDS_STRING251));
				m_sPriceM3FUB	= (xml->str(IDS_STRING252));

				m_sRemoveCapMsg = (xml->str(IDS_STRING226));
				m_sRemoveMsg1 = (xml->str(IDS_STRING226));
				m_sRemoveMsg2 = (xml->str(IDS_STRING229));
				m_sSuspciusCaluclationMsg.Format(_T("%s\n\n%s\n - %s\n - %s\n - %s"),
							(xml->str(IDS_STRING3220)),
							(xml->str(IDS_STRING3221)),
							(xml->str(IDS_STRING3222)),
							(xml->str(IDS_STRING3223)),
							(xml->str(IDS_STRING3224)));

				m_sExplRemoveMsg.Format(_T("%s\n%s"),
							(xml->str(IDS_STRING227)),
							(xml->str(IDS_STRING228)));

				m_sGroup3_cap1 = (xml->str(IDS_STRING3250));
				m_sGroup3_cap2 = (xml->str(IDS_STRING3251));

				m_sMsgCaption	= (xml->str(IDS_STRING151));
				m_sMsgChanges.Format(_T("%s\n\n%s"),
							(xml->str(IDS_STRING221)),
							(xml->str(IDS_STRING223)));
				m_sMsgChangesSaved = (xml->str(IDS_STRING222));
				m_sMsgSpecieShare = (xml->str(IDS_STRING224));

				m_sTraktNum	= (xml->str(IDS_STRING1000));
				m_sTraktPNum =	(xml->str(IDS_STRING1001));
				m_sTraktName = (xml->str(IDS_STRING101));
				m_sTraktSpcName = (xml->str(IDS_STRING133));

				m_sMeter = (xml->str(IDS_STRING3320));
				m_sKiloMeter  = (xml->str(IDS_STRING3321));

				m_sVolume = (xml->str(IDS_STRING3252));
				m_sPulpVol = (xml->str(IDS_STRING3266));
				m_sValueTimber= (xml->str(IDS_STRING3253));
				m_sCostCap= (xml->str(IDS_STRING3254));
				m_sCostCutting= (xml->str(IDS_STRING3255));
				m_sCostHarvester= (xml->str(IDS_STRING3256));
				m_sCostSkotare= (xml->str(IDS_STRING3257));
				m_sCostRest= (xml->str(IDS_STRING3258));
				m_sCostPermanent= (xml->str(IDS_STRING3259));
				m_sNETTO = (xml->str(IDS_STRING3260));
				m_sKR_M3FUB = (xml->str(IDS_STRING3261));
				m_sSum = (xml->str(IDS_STRING3262));
				m_sKR = (xml->str(IDS_STRING3181));
				m_sTonne = (xml->str(IDS_STRING3191));
				m_sGrot = (xml->str(IDS_STRING3267));
				m_sSumGrotPrice = (xml->str(IDS_STRING3268));
				m_sSumGrotCost = (xml->str(IDS_STRING3269));
				m_sTransport = (xml->str(IDS_STRING3263));
				m_sTransportToRoad.Format(_T("%s %s"), (xml->str(IDS_STRING3263)),(xml->str(IDS_STRING2598)));
				m_sTransportToIndustry.Format(_T("%s %s"), (xml->str(IDS_STRING3263)),(xml->str(IDS_STRING2599)));
				m_sMsgDataMissing1 = (xml->str(IDS_STRING3264));
				m_sMsgDataMissing2 = (xml->str(IDS_STRING3265));
				
				m_sHandleSettings = xml->str(IDS_STRING600);
				m_sHandleAssortments = xml->str(IDS_STRING601);


				if (m_wndReport.GetSafeHwnd() != NULL)
				{
					
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					m_wndReport.SetImageList(&m_ilIcons);
				
					m_wndReport.ShowWindow( SW_NORMAL );

					m_wndReport.SetFreezeColumnsCount( 1 );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING133)), 80));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING134)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING135)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING136)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING137)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING138)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING139)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING140)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING150)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING149)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;	// #4513
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					//pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING267)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING141)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING142)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_13, (xml->str(IDS_STRING319)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_14, (xml->str(IDS_STRING143)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_15, (xml->str(IDS_STRING144)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_16, (xml->str(IDS_STRING320)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 


					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_17, (xml->str(IDS_STRING272)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					m_wndReport.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport.FocusSubItems(TRUE);
					m_wndReport.AllowEdit(TRUE);
					m_wndReport.GetColumns()->Find(COLUMN_0)->SetTreeColumn(TRUE);
					m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				}	// if (m_wndReport.GetSafeHwnd() != NULL)

				if (m_wndRotpost1.GetSafeHwnd() != NULL)
				{
		
					m_wndRotpost1.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 16, FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING3254)), 115, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_2, _T(""), 80, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_3, _T(""), 40, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_4, _T(""), 40, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost1.AddColumn(new CXTPReportColumn(COLUMN_5, _T(""), 60, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					m_wndRotpost1.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndRotpost1.SetMultipleSelection( FALSE );
					m_wndRotpost1.SetGridStyle( TRUE, xtpReportGridNoLines );
					m_wndRotpost1.SetGridStyle( FALSE, xtpReportGridNoLines );
					m_wndRotpost1.AllowEdit(FALSE);
//					m_wndRotpost1.ShowHeader( FALSE );
					m_wndRotpost1.FocusSubItems(FALSE);
					m_wndRotpost1.ModifyStyle(WS_TABSTOP,0);
					m_wndRotpost1.GetPaintManager()->m_bHideSelection = TRUE;
					m_wndRotpost1.GetPaintManager()->m_clrHighlightText = RGB(0,0,0);
					m_wndRotpost1.GetPaintManager()->m_clrHighlight = INFOBK;
					m_wndRotpost1.GetPaintManager()->m_clrControlBack = INFOBK;
					m_wndRotpost1.GetPaintManager()->m_clrHeaderControl = INFOBK;
					m_wndRotpost1.GetPaintManager()->m_clrControlLightLight = INFOBK;
					m_wndRotpost1.GetPaintManager()->m_clrControlDark = INFOBK;
					m_wndRotpost1.GetColumns()->Find(COLUMN_0)->SetTreeColumn(TRUE);

					LOGFONT lf;
					memset(&lf,0,sizeof(LOGFONT));
					lf.lfUnderline = TRUE;
					lf.lfHeight = 13;
					lf.lfWeight = FW_BOLD;
					m_wndRotpost1.GetPaintManager()->SetCaptionFont(lf);
					m_wndRotpost1.GetPaintManager()->m_clrCaptionText = RGB(0,0,255);
				}	// if (m_wndRotpost1.GetSafeHwnd() != NULL)

				if (m_wndRotpost2.GetSafeHwnd() != NULL)
				{
		
					m_wndRotpost2.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 10, FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING3262)), 100, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_2, _T(""), 80, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_3, _T(""), 40, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_4, _T(""), 80, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->SetSortable( FALSE );

					pCol = m_wndRotpost2.AddColumn(new CXTPReportColumn(COLUMN_5, _T(""), 40, FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_CENTER );
					pCol->SetSortable( FALSE );

					m_wndRotpost2.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndRotpost2.SetMultipleSelection( FALSE );
					m_wndRotpost2.SetGridStyle( TRUE, xtpReportGridNoLines );
					m_wndRotpost2.SetGridStyle( FALSE, xtpReportGridNoLines );
					m_wndRotpost2.AllowEdit(FALSE);
//					m_wndRotpost2.ShowHeader( FALSE );
					m_wndRotpost2.FocusSubItems(FALSE);
					m_wndRotpost2.ModifyStyle(WS_TABSTOP,0);
					m_wndRotpost2.GetPaintManager()->m_bHideSelection = TRUE;
					m_wndRotpost2.GetPaintManager()->m_clrHighlightText = RGB(0,0,0);
					m_wndRotpost2.GetPaintManager()->m_clrHighlight = INFOBK;
					m_wndRotpost2.GetPaintManager()->m_clrControlBack = INFOBK;
					m_wndRotpost2.GetPaintManager()->m_clrHeaderControl = INFOBK;
					m_wndRotpost2.GetPaintManager()->m_clrControlLightLight = INFOBK;
					m_wndRotpost2.GetPaintManager()->m_clrControlDark = INFOBK;

					LOGFONT lf;
					memset(&lf,0,sizeof(LOGFONT));
					lf.lfUnderline = TRUE;
					lf.lfHeight = 13;
					lf.lfWeight = FW_BOLD;
					m_wndRotpost2.GetPaintManager()->SetCaptionFont(lf);
					m_wndRotpost2.GetPaintManager()->m_clrCaptionText = RGB(0,0,255);
				}	// if (m_wndRotpost1.GetSafeHwnd() != NULL)

				// create a view to occupy the client area of the frame
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}

void CPageTwoFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();	
		break;
	}
	menu.DestroyMenu();
}

int CPageTwoFormView::showMenu(short todo)
{
	// Show a Popupmenu on rightclick; 070502 p�d
	CPoint curPos;
	CMenu menu;
	CString sMenu1,sStr1,sStr2;
	int nMenuResult;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();

	CXTPReportColumn *pCol = m_wndReport.GetFocusedColumn();
	if (pCol != NULL)
	{
		if (pCol->GetItemIndex() == 0)
		{
			CXTPReportRow *pRow = m_wndReport.GetFocusedRow();

			if (pRow != NULL)
			{
				if (pRow->GetTreeDepth() == 0)
				{
					CTraktDataReportRec *pRec = (CTraktDataReportRec*)pRow->GetRecord();
					if (pRec != NULL)
					{
						// On selecting a specie in m_wndReport, make sure that the
						// active TraktData record also is set; 070503 p�d
						m_recTraktDataActive = pRec->getRecTData();
						m_recTraktDataOnClick = pRec->getRecTData();
					}	// if (pRec != NULL && pRow != NULL)
				}	// if (pRow->GetTreeDepth() == 0)
			}	// if (pRow != NULL)
		}
	}

	VERIFY(menu.CreatePopupMenu());
	GetCursorPos(&curPos);
	// create main menu items
	if (todo == 1)
	{
		if (pCol != NULL && pRow != NULL)
		{
			if (pCol->GetItemIndex() == 0)
			{
				if (pRow->GetTreeDepth() == 0)
				{
					sStr1.Format(L"%s (%s)",m_sHandleSettings,m_recTraktDataActive.getSpecieName());
					sStr2.Format(L"%s (%s)",m_sHandleAssortments,m_recTraktDataActive.getSpecieName());
					menu.AppendMenu(MF_STRING, IDC_OPEN_SETTINGS, sStr1);
					menu.AppendMenu(MF_STRING, IDC_OPEN_SET_ASSORT, sStr2);
					// track menu
					nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
																	curPos.x, curPos.y, this, NULL);
				}	// if (pRow->GetTreeDepth() == 0)
			}	// if (pCol->GetItemIndex() == 0)
		}	// if (pCol != NULL && pRow != NULL)
	}
	else if (todo == 2)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				sMenu1 = xml.str(IDS_STRING225);
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}
		menu.AppendMenu(MF_STRING, ID_POPUPMENU_SHOW_HIDE_ASSORTMENTS, (sMenu1));
		// track menu
		nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
											curPos.x, curPos.y, this, NULL);
	}

	// other general items
	switch (nMenuResult)
	{
		case IDC_OPEN_SETTINGS :
		{
			// Do update of specie; 070503 p�d
			updTraktData(m_recTraktDataActive,1);
			break;
		}
		case IDC_OPEN_SET_ASSORT :
		{
			// Do update of specie; 070503 p�d
			updTraktData(m_recTraktDataActive,2);
			break;
		}
		case ID_POPUPMENU_SHOW_HIDE_ASSORTMENTS :
		{
			doShowHideAssortmentInfo();
			break;
		}
	}

	return nMenuResult;
}

void CPageTwoFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->updateGeneralInfoDialog();
	}
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pRow)
			showMenu(1);
	}
}

void CPageTwoFormView::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		showMenu(2);
	}
}

void CPageTwoFormView::OnReport1RowExpandChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
  if (pItemNotify != NULL)
	{
		m_wndRotpost1.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
	}
}


void CPageTwoFormView::OnShowFieldChooser()
{
	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPageTwoFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	int nIndex = -1; 
	CXTPTabManagerItem *pManager;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			nIndex = pManager->GetIndex();
		}	// if (pManager != NULL)
	}	// if (pTabView)

	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			if (nIndex == 1)
			{
				if (pTabView != NULL)
				{
					CPageOneFormView *pPageOne = pTabView->getPageOneFormView();
					if (pPageOne != NULL)
					{
						pPageOne->addTrakt();
					}	// if (pPageOne != NULL)
				}	// if (pTabView != NULL)
			}	// if (nIndex == 1)
			clearAll();
			break;
		}	// case ID_SAVE_ITEM :

		case ID_SAVE_ITEM :
		{
			if (nIndex == 1)
			{
				saveTraktDataAndAssToDB();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			if (nIndex == 1)
			{
					CPageOneFormView *pPageOne = pTabView->getPageOneFormView();
					if (pPageOne != NULL)
					{
						pPageOne->removeTrakt();
					}	// if (pPageOne != NULL)
//				removeTraktDataSpecieFromDB();
			}
			break;
		}	// case ID_DELETE_ITEM :
	}	// switch (wParam)
	return 0L;
}


//////////////////////////////////////////

BOOL CPageTwoFormView::isHasDataChangedPageTwo(void)
{
	BOOL bReturn = FALSE;
	if (m_wndReport.isDirty() || m_bIsDirty)
	{
		bReturn = saveTraktDataAndAssToDB();
	}
	m_bIsDirty = FALSE;

	return bReturn;
}

void CPageTwoFormView::populateData(BOOL show_message)
{
	double fM3PerSpcFub = 0.0;
	TCHAR szTmp[127];
	int nDecimals = 2;	// Deafult
	BOOL bDoAdd = FALSE;
	CString sAssortInfo;
	CTraktDataReportRec *pRecord = NULL;
	vecTransactionTraktAss vecTraktAss;
	CTransaction_trakt_ass recTraktAss;

	getTraktDataFromDB(m_vecTraktData);
	getTraktAssFromDB(vecTraktAss);

	//20121206 J� #3509 Uppdatera tr�dslagslistan
	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if(pWnd!=NULL)
	{
		pWnd->updateSpecVec(m_vecTraktData);
	}

	getPlotsFromDB();
	CString S;
	clearAll();

	// Clear data
	m_wndReport.ResetContent();

	if (m_vecTraktData.size() > 0)
	{
		getTrakt();	// OBS! Set m_pTraktRecord in this method; 070614 p�d
		if (m_pTraktRecord == NULL)
			return;
		// Find trakt data based on active trakt; 070315 p�d
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			fM3PerSpcFub = 0.0;
			if (m_vecTraktData[i].getTDataTraktID() == m_pTraktRecord->getTraktID() &&
					m_vecTraktData[i].getTDataType() == STMP_LEN_WITHDRAW)
			{
				m_recTraktDataActive = m_vecTraktData[i];
				// Add Trakt data for Specie to m_wndReport; 070312 p�d
				pRecord = new CTraktDataReportRec(m_recTraktDataActive,
																					m_sLangFN,
																					m_wndReport.GetSafeHwnd());
				m_wndReport.AddRecord(pRecord);

				// Add Assortment information
				if (vecTraktAss.size() > 0)
				{
					for (UINT i = 0;i < vecTraktAss.size();i++)
					{
						recTraktAss = vecTraktAss[i];
						if (recTraktAss.getTAssTraktDataID() == m_recTraktDataActive.getTDataID() &&
							  recTraktAss.getTAssTraktID() == m_recTraktDataActive.getTDataTraktID())
						{
							// Show assortment information. Name and Price; 0703114 p�d
							if (recTraktAss.getM3TO() == 0.0 && recTraktAss.getM3FUB() == 0.0)
							{
								sAssortInfo.Format(_T("%s"),recTraktAss.getTAssName());
								bDoAdd = FALSE;

							}
/*	ALWAYS SHOW VOLUME IN M3FUB; 091117 p�d
							// Change criteria from a value to volume (m3to); 091117 p�d
//							else if (recTraktAss.getValueM3TO() > 0.0)
							else if (recTraktAss.getM3TO() > 0.0)
							{
								// Diffirentiate number of decimals depending
								// on value; 071024 p�d
								if (recTraktAss.getM3TO() > 0.09) nDecimals = 1;
								else if (recTraktAss.getM3TO() > 0.009) nDecimals = 2;
								else if (recTraktAss.getM3TO() > 0.0009) nDecimals = 3;

								_stprintf(szTmp,_T("%s (%.*f %s)"),
																		recTraktAss.getTAssName(),
																		nDecimals,
																		recTraktAss.getM3TO(),
																		m_sM3TO);
								sAssortInfo	= szTmp;
								bDoAdd = TRUE;
							}
*/
							// Change criteria from a value to volume (m3fub); 091117 p�d
//							else if (recTraktAss.getValueM3FUB() > 0.0)
							else if (recTraktAss.getM3FUB() > 0.0)
							{
								// Diffirentiate number of decimals depending
								// on value; 071024 p�d
								if (recTraktAss.getM3FUB() > 0.09) nDecimals = 1;
								else if (recTraktAss.getM3FUB() > 0.009) nDecimals = 2;
								else if (recTraktAss.getM3FUB() > 0.0009) nDecimals = 3;

								_stprintf(szTmp,_T("%s (%.*f %s)"),
																		recTraktAss.getTAssName(),
																		nDecimals,
																		recTraktAss.getM3FUB(),
																		m_sM3FUB);
								sAssortInfo	= szTmp;
								bDoAdd = TRUE;
							}

							if (bDoAdd)
							{
								pRecord->GetChilds()->Add(new CTraktAssReportRec(sAssortInfo,
																															 recTraktAss.getTAssName(),
																															 recTraktAss.getPriceM3FUB(),
																															 recTraktAss.getPriceM3TO(),
																															 recTraktAss.getM3FUB(),
																															 recTraktAss.getM3TO() )); 
						
							}	// if (bDoAdd)
						}	// if (recTAss.getTAssTraktID() == recTraktData.getSpecieID())
					}	// for (UINT i = 0;i < m_vecTraktAss.size();i++)
				}	// if (m_vecTraktAss.size() > 0)	

			}	// if (m_vecTraktData[i].getTDataTraktID() == m_pTraktRecord->getTraktID())
		}	// for (int i = 0;i < m_vecTraktData.size();i++)

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();

		setupAvgAndSumData(show_message);
		
		populateRotpostInfo(TRUE);

	}	// if (m_vecTraktData.size() > 0)
}

void CPageTwoFormView::addTraktData()
{
	vecTransactionTraktData vecTraktData;
	vecTransactionTraktAss vecTraktAss;
	getTraktDataFromDB(vecTraktData);
	getTraktAssFromDB(vecTraktAss);
	showFormView(IDD_FORMVIEW8,m_sLangFN);
	CSpecieDataFormView *pView = (CSpecieDataFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		pView->UpdateWindow();
		pView->clearForm();
		pView->setAction(NEW_ITEM);
		pView->setSpecificLanguage();
		pView->setSpecieID(-1);	// Indicates no specie; 070502 p�d
		m_enumAction = NEW_ITEM;

	}
}

void CPageTwoFormView::updTraktData(CTransaction_trakt_data& trakt_data,short todo)
{
	// Adding a modal dialog, instead of a formview, so user can't go back
	// to Result-tab; 091103 p�d
	if (todo == 1)	// Open settings
	{
		CHandleSettingsDlg *pDlg = new CHandleSettingsDlg(trakt_data.getSpecieName(),trakt_data.getSpecieID(),trakt_data.getTDataTraktID(),m_pDB);
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
				// Set wait cursor; 091104 p�d
				::SetCursor(::LoadCursor(NULL,IDC_WAIT));
				m_enumAction = UPD_ITEM;
				if (runDoCalulationInPageThree())
					updFromSpecieDataFormView();
				// Set back to arrow cursor (default); 091104 p�d
				::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			}
		}
	}
	if (todo == 2)	// Open assortments
	{
		CSpcSettingsDlg *pDlg = new CSpcSettingsDlg(trakt_data.getSpecieName(),trakt_data.getSpecieID(),trakt_data.getTDataTraktID(),m_pDB);
		if (pDlg != NULL)
		{
			pDlg->whatToHandle(todo);
			if (pDlg->DoModal() == IDOK)
			{
				// Set wait cursor; 091104 p�d
				::SetCursor(::LoadCursor(NULL,IDC_WAIT));
				m_enumAction = UPD_ITEM;				
				//�ndrat i funktionsanrop, lagt till att meddelandet ej skall visas om det skall r�knas om eller ej, Bug #2566 20111125 J�
				if (runDoCalulationInPageThree(-1,FALSE,TRUE,FALSE,true,true))
					updFromSpecieDataFormView();
				 //Set back to arrow cursor (default); 091104 p�d
				::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			}	// if (pDlg->DoModal() == IDOK)			

			delete pDlg;
		}
	}
}

void CPageTwoFormView::addFromSpecieDataFormView(void)
{
	populateData();
}

void CPageTwoFormView::updFromSpecieDataFormView(void)
{
	populateData();
}


void CPageTwoFormView::addFromTraktSettingsPropertyGrid(int trakt_id,int spc_id,double h25,double green_crown)
{
	CString S;
	double fH25 = 0.0;
	double fGreenCrown = 0.0;
	BOOL bDoAdd = FALSE;
	CString sAssortInfo;
	CTraktDataReportRec *pRecord;
	vecTransactionSpecies vecSpecies;
	vecTransactionAssort vecAssort;
	CTransaction_species recSpecies;
	CTransaction_assort recAssort;
	CTransaction_trakt_data recTraktData;

	vecTransactionTraktData vecActiveTraktData;
	CTransaction_trakt_data recActiveTraktData;

	getTraktDataFromDB(vecActiveTraktData);

	//---------------------------------------------------------------------------------
	// Get information on pricelist saved in esti_trakt_misc_data_table; 070502 p�d
	getTraktMiscDataFromDB(trakt_id);
	// .. and get information in pricelist (xml-file), saved; 070502 p�d
/*
	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get species in pricelist; 070430 p�d
			pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			//---------------------------------------------------------------------------------
			// Get assortments per specie; 070430 p�d
			pPrlParser->getAssortmentPerSpecie(vecAssort);
		}
		delete pPrlParser;
	}
*/
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get species in pricelist; 070430 p�d
			pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			//---------------------------------------------------------------------------------
			// Get assortments per specie; 070430 p�d
			pPrlParser->getAssortmentPerSpecie(vecAssort);
		}
		delete pPrlParser;
	}

	if (vecSpecies.size() == 0)
		return;	// No need to proceed; 070430 p�d

	m_wndReport.CollapseAll();
	m_wndReport.ClearReport();

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	for (UINT i = 0; i < vecSpecies.size();i++)
	{
		recSpecies = vecSpecies[i];
		// Check if spc_id = -1 it doesn't matter what specie it is. Used om entering
		// trakt manually, else check for specie on Template; 070906 p�d
		if (recSpecies.getSpcID() == spc_id || spc_id == -1)
		{
			// Check in vecActiveTraktData for inormation on H25 values; 090624 p�d
			if (vecActiveTraktData.size() > 0)
			{
				for (UINT i1 = 0;i1 < vecActiveTraktData.size();i1++)
				{
					recActiveTraktData = vecActiveTraktData[i1];
					if (recActiveTraktData.getTDataID() == recSpecies.getSpcID() &&
						  recActiveTraktData.getTDataTraktID() == trakt_id &&
							recActiveTraktData.getTDataType() == STMP_LEN_WITHDRAW)
					{
						fH25 = recActiveTraktData.getH25();
						fGreenCrown = recActiveTraktData.getGreenCrown();
/*
					S.Format(_T("recActiveTraktData.getTDataID() %d\nSpc %d\nrecActiveTraktData.getTDataTraktID() %d\ntrakt_id %d\nrecActiveTraktData.getTDataType() %d\nSTMP_LEN_WITHDRAW %d"),
						recActiveTraktData.getTDataID(),recSpecies.getSpcID(),recActiveTraktData.getTDataTraktID(),trakt_id,recActiveTraktData.getTDataType(),STMP_LEN_WITHDRAW);
					UMMessageBox(S);
*/
						break;
					}
				}
			}


			// Setup record to be added to m_wndReport; 070430 p�d
			recTraktData = CTransaction_trakt_data(recSpecies.getSpcID(),				// OBS! Set trakt data id  = specie id; 070502 p�d
																						 trakt_id,
																						 STMP_LEN_WITHDRAW,
																						 recSpecies.getSpcID(),
																						 recSpecies.getSpcName(),
																						 0.0,
																						 0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 fH25,	// H25
																						 fGreenCrown, //green_crown,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 0.0,
																						 _T(""));

			pRecord = new CTraktDataReportRec(recTraktData,m_sLangFN,m_wndReport.GetSafeHwnd());
			m_wndReport.AddRecord(pRecord);

			// Add assortments for each specie; 070430 p�d
			if (vecAssort.size() > 0)
			{
				for (UINT j = 0;j < vecAssort.size();j++)
				{
					recAssort = vecAssort[j];
					// Check that we add only assortments for Specie; 070430 p�d
					if (recAssort.getSpcID() == recSpecies.getSpcID())
					{

						// Show assortment information. Name and Price; 0703114 p�d
						if (recAssort.getPriceM3to() == 0.0 && recAssort.getPriceM3fub() == 0.0)
						{
							sAssortInfo.Format(_T("%s"),recAssort.getAssortName());
							bDoAdd = TRUE;
						}
						else if (recAssort.getPriceM3to() > 0.0)
						{
							sAssortInfo.Format(_T("%s (%.1f %s)"),
																	recAssort.getAssortName(),
																	recAssort.getPriceM3to(),
																	m_sM3TO);
							bDoAdd = TRUE;
						}
						else if (recAssort.getPriceM3fub() > 0.0)
						{
							sAssortInfo.Format(_T("%s (%.1f %s)"),
																	recAssort.getAssortName(),
																	recAssort.getPriceM3fub(),
																	m_sM3FUB);
							bDoAdd = TRUE;
						}

						if (bDoAdd)
						{
							pRecord->GetChilds()->Add(new CTraktAssReportRec(sAssortInfo,
																														 recAssort.getAssortName(),
																														 recAssort.getPriceM3fub(),
																														 recAssort.getPriceM3to(),
																														 0.0,
																														 0.0 )); 
						}	// if (bDoAdd)
					}	// if (recAssort.getSpcID() == recSpecies.getSpcID())
				}	// for (UINT j = 0;j < vecAssort.size();j++)
			}	// if (vecAssort.size() == 0)
		}	// if (recSpecies.getSpcID() == spc_id || spc_id == -1)
	}	// for (UINT i = 0; i < vecSpecies.size();i++)
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	// Save data; 070502 p�d
	saveTraktDataAndAssToDB(FALSE);	// Don't show Messagebox; tellin' user the data has been saved; 070316 p�d

	setupAvgAndSumData(FALSE);

}

//Added function for retreiving average data on stand 200100224 J�
//Retreive average per stand for variables Da Dg Dgv Hgv AvgHgt
void CPageTwoFormView::retreiveAverages(double *_Da,double *_Dg,double *_Dgv,double *_Hgv,double *_AvgHgt)
{
	double fDCLS	= 0.0;
	double fMid		= 0.0;
	double fSumD	= 0.0;
	double fSumD2	= 0.0;
	double fSumD3	= 0.0;
	double fGY		= 0.0;
	double fDA		= 0.0;
	double fDG		= 0.0;
	double fDGV		= 0.0;
	double fHGV		= 0.0;
	double fAVGHGT	= 0.0;
	double fSumHgt = 0.0;
	double fSumHgtM3sk = 0.0;
	double fSumM3sk = 0.0;
	double fHgt		= 0.0;
	double nNumOf		= 0.0;
	double nNumOfTrees = 0.0;
	double nNumOfHgtTrees = 0.0;


	//Zero pointers to result
	*_Da=fDA;
	*_Dg=fDG;
	*_Dgv=fDGV;
	*_Hgv=fHGV;
	*_AvgHgt=fAVGHGT;

	vecTransactionDCLSTree m_vecDCLSTrees;
	vecTransactionTraktData m_vecTrakt;

	if (m_pDB != NULL)
	{

		//Get HGV per specie and stand, type = 1 = uttag
		//Weighted average Hgv on m3sk
		m_pDB->getTraktData(m_vecTrakt,m_recTraktDataActive.getTDataTraktID(),STMP_LEN_WITHDRAW);
		if (m_vecTrakt.size() > 0)
		{
			for (UINT i = 0;i < m_vecTrakt.size();i++)
			{
				fSumHgtM3sk	+= m_vecTrakt[i].getHGV()*m_vecTrakt[i].getM3SK();
				fSumM3sk		+= m_vecTrakt[i].getM3SK();
			}
			if(fSumM3sk > 0.0)
			{
				fHGV=fSumHgtM3sk/fSumM3sk;
				*_Hgv=fHGV;
			}
		}


		//Get the other variables DA DG DGV AVGHGT
		m_pDB->getDCLSTrees(m_recTraktDataActive.getTDataTraktID(),m_vecDCLSTrees);
		if (m_vecDCLSTrees.size() > 0)
		{
			nNumOfTrees = 0;
			fSumD		= 0.0;
			fSumD2	= 0.0;
			fSumD3	= 0.0;
			fGY		= 0.0;
			fDA		= 0.0;
			fDG		= 0.0;
			fDGV		= 0.0;
			fAVGHGT	= 0.0;
			fSumHgt	= 0.0;
			for (UINT i = 0;i < m_vecDCLSTrees.size();i++)
			{
				fDCLS = (m_vecDCLSTrees[i].getDCLS_to() - m_vecDCLSTrees[i].getDCLS_from());
				fMid = fDCLS / 2.0;
				fDCLS= (m_vecDCLSTrees[i].getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)

				// Adaption for "Intr�ngsv�rdering":
				// Number of trees is a combination of "Tr�d i gata (Uttag) och Kanttr�d"
				// OBS! This only allies to "Intr�ng"; 080307 p�d
				nNumOf = m_vecDCLSTrees[i].getNumOf()+m_vecDCLSTrees[i].getNumOfRandTrees();
				fHgt   = m_vecDCLSTrees[i].getHgt();
				// Calculate Sum baselarea ("Grundyta") for specie; 070524 p�d

				fSumD		+= fDCLS*nNumOf;
				fSumD2	+= pow(fDCLS,2.0)*nNumOf;
				fSumD3	+= pow(fDCLS,3.0)*nNumOf;
				fSumHgt	+= (fHgt*nNumOf)/10.0;	// (dm) => (m)
				nNumOfTrees += nNumOf;
				//Added check 20100302
				//Check if there are any heights, ie do not count 0 heights for average height
				if(fHgt>0)
					nNumOfHgtTrees+=nNumOf;

			} // for (UINT i = 0;i < m_nNumOfSampleTrees;i++)


			if (nNumOfTrees > 0.0)
				fDA = fSumD/nNumOfTrees;
			
			if (nNumOfTrees > 0.0)
				fDG = sqrt(fSumD2/nNumOfTrees);
			
			if (fSumD2 > 0.0 )
				fDGV = fSumD3/fSumD2;

			if (nNumOfHgtTrees > 0.0)
				fAVGHGT=(fSumHgt/nNumOfHgtTrees);

			fDA=fDA/10.0;	
			fDG=fDG/10.0;	
			fDGV=fDGV/10.0; 

			*_Da=fDA;
			*_Dg=fDG;
			*_Dgv=fDGV;
			*_Hgv=fHGV;
			*_AvgHgt=fAVGHGT;

		}	


	}	// if (pDB != NULL)
}


// Setup average and sum data, based on values in
// m_vecTraktData vector; 070313 p�d
void CPageTwoFormView::setupAvgAndSumData(BOOL show_message)
{
	CString S;
	double fSumPercent = 0.0;
	long nSumTrees = 0;
	double fSumM3Sk = 0.0;
	double fSumM3Fub = 0.0;
	double fSumM3Ub = 0.0;
	double fSumGY = 0.0;
	double fAvgDa = 0.0;
	double fAvgDg = 0.0;
	double fAvgDgv = 0.0;
	double fAvgHgv = 0.0;
	double fAvgHgt = 0.0;
	long nNumOfRows = 0;
	long cnt;

	long nSumTrees2 = 0;
	double fSumM3Sk2 = 0.0;
	double fSumM3Fub2 = 0.0;
	double fSumM3Ub2 = 0.0;
	double fSumGY2 = 0.0;

	vecTransactionTraktData vecTraktData;

	getTrakt();	// Set m_pTraktRecord
	CTraktDataReportRec *pRec = NULL;
	CXTPReportRow *pRow = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (cnt = 0;cnt < pRows->GetCount();cnt++)
		{
			pRow = pRows->GetAt(cnt);
			if (pRow != NULL)
			{
				pRec = (CTraktDataReportRec *)pRow->GetRecord();
				// Only calulate avg. data for trees in dcls; 070822 p�d
				if (pRec->getColumnFloat(1) > 0.0)
				{
					fSumPercent += pRec->getColumnFloat(1);
					nSumTrees += pRec->getColumnLong(2);
					fSumM3Sk +=	pRec->getColumnFloat(11);
					fSumM3Fub += pRec->getColumnFloat(12);
					fSumM3Ub += pRec->getColumnFloat(13);
					fSumGY +=	pRec->getColumnFloat(7);
					fAvgDa +=	pRec->getColumnFloat(3);
					fAvgDg += pRec->getColumnFloat(4);
					fAvgDgv	+= pRec->getColumnFloat(5);
					fAvgHgv += pRec->getColumnFloat(6);
					fAvgHgt += pRec->getColumnFloat(8);
					nNumOfRows++;
				}
			}
		}
	}

	nSumTrees2 = 0;
	fSumM3Sk2 = 0.0;
	fSumM3Fub2 = 0.0;
	fSumM3Ub2 = 0.0;
	fSumGY2 = 0.0;
	// Get informetion on "Kvarl�mmnat" for this Trakt; 071121 p�d
	getTraktDataFromDB_to_be_left(vecTraktData);
	if (vecTraktData.size() > 0)
	{
		for (UINT i = 0;i < vecTraktData.size();i++)
		{
			if (vecTraktData[i].getTDataType() == STMP_LEN_TO_BE_LEFT)
			{
				nSumTrees2 += vecTraktData[i].getNumOf();
				fSumM3Sk2 += vecTraktData[i].getM3SK();
				fSumM3Fub2 += vecTraktData[i].getM3FUB();
				fSumM3Ub2 += vecTraktData[i].getM3UB();
				fSumGY2 += vecTraktData[i].getGY();
			}
		}	
		fSumGY2 = fSumGY2/vecTraktData.size();
	}
	if (show_message)
	{
		// Check if there's suspicious values. E.g. Number of trees < 0; 071031 p�d
		if (nSumTrees < 0 || fSumPercent < 0.0 || fSumM3Sk < 0.0 || fSumM3Fub < 0.0 || fSumM3Ub < 0.0 ||
				fSumGY < 0.0 || fAvgDa < 0.0 || fAvgDg < 0.0 || fAvgDgv < 0.0 || fAvgHgt < 0.0)
		{
/*
					S.Format("CPageTwoFormView::setupAvgAndSumData\nnSumTrees %ld",nSumTrees);
					UMMessageBox(S);
*/
			UMMessageBox(this->GetSafeHwnd(),m_sSuspciusCaluclationMsg,m_sMsgCaption,MB_ICONEXCLAMATION | MB_OK);
		}
	}	// if (show_message)

	// Set info in GeneralInfoDlg; 070305 p�d
	// And populate settings; 070507 p�d
	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pWnd != NULL)
	{
		// Set avg. GY depending on number of species; 071003 p�d
		// NOT SURE IF GY SHOULD BE AN AVG. VALUE!?
/*
		if (nNumOfRows > 0)
		{
			fSumGY = fSumGY/nNumOfRows;
		}
*/
		pWnd->setSumInfo(nSumTrees,
										 fSumM3Sk, 
										 fSumM3Fub,
										 fSumM3Ub,
										 fSumGY); 
		pWnd->setSumInfo2(nSumTrees2,
										 fSumM3Sk2, 
										 fSumM3Fub2,
										 fSumM3Ub2,
										 fSumGY2); 
		pWnd->populateSettings();
	}	// if (pWnd != NULL)

	if (nNumOfRows > 0)
	{



		

		//Changed 20010224, not average by columns, average by stand J�
		retreiveAverages(&fAvgDa,&fAvgDg,&fAvgDgv,&fAvgHgv,&fAvgHgt);
/*		m_wndEdit5.setFloat(fAvgDa/nNumOfRows,1);
		m_wndEdit6.setFloat(fAvgDg/nNumOfRows,1);
		m_wndEdit7.setFloat(fAvgDgv/nNumOfRows,1);
		m_wndEdit8.setFloat(fAvgHgv/nNumOfRows,1);
		// Check how large number is and
		// set number of decimals accordingly; 071126 p�d
		if ((fAvgHgt/nNumOfRows) < 1.0)
			m_wndEdit9.setFloat((fAvgHgt/nNumOfRows),3);
		else if ((fAvgHgt/nNumOfRows) > 1.0 && (fAvgHgt/nNumOfRows) < 10.0)
			m_wndEdit9.setFloat((fAvgHgt/nNumOfRows),2);
		else
			m_wndEdit9.setFloat((fAvgHgt/nNumOfRows),1);*/
		

		m_wndEdit5.setFloat(fAvgDa,1);
		m_wndEdit6.setFloat(fAvgDg,1);
		m_wndEdit7.setFloat(fAvgDgv,1);
		m_wndEdit8.setFloat(fAvgHgv,1);
		// Check how large number is and
		// set number of decimals accordingly; 071126 p�d
		if ((fAvgHgt) < 1.0)
			m_wndEdit9.setFloat((fAvgHgt),3);
		else if ((fAvgHgt) > 1.0 && (fAvgHgt) < 10.0)
			m_wndEdit9.setFloat((fAvgHgt),2);
		else
			m_wndEdit9.setFloat((fAvgHgt),1);

	}
	else
	{
		m_wndEdit5.setFloat(0.0,1);
		m_wndEdit6.setFloat(0.0,1);
		m_wndEdit7.setFloat(0.0,1);
		m_wndEdit8.setFloat(0.0,1);
		m_wndEdit9.setFloat(0.0,1);
	}

}

void CPageTwoFormView::clearAll(void)
{
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));

	
}

void CPageTwoFormView::getTrakt(void)
{
	m_pTraktRecord = (CTransaction_trakt *)getActiveTrakt();
}
/* NOT USED 2008-10-28 P�D
// CSpecieDataFormView message handlers
void CPageTwoFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getSpecies(m_vecSpecies);
		}
	}
}
*/
// Get Trakt data in database; 070312 p�d
void CPageTwoFormView::getTraktDataFromDB(vecTransactionTraktData &vec)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(vec,getActiveTrakt()->getTraktID(),STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}
// Get "Kvarl�mmnat"
void CPageTwoFormView::getTraktDataFromDB_to_be_left(vecTransactionTraktData &vec)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(vec,getActiveTrakt()->getTraktID(),STMP_LEN_TO_BE_LEFT);
	}	// if (pDB != NULL)
}


void CPageTwoFormView::getTraktAssFromDB(vecTransactionTraktAss &vec)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktAss(vec);
	}	// if (pDB != NULL)
}

void CPageTwoFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_bConnected = m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

void CPageTwoFormView::getPlotsFromDB(void)
{
	double fRadius = 0.0;
	double fArea = 0.0;
	double fLength = 0.0;
	double fWidth = 0.0;
	if (m_bConnected)	
	{
		CTransaction_trakt *pTrakt = getActiveTrakt();
		if (m_pDB != NULL)
		{
			m_pDB->getPlots(pTrakt->getTraktID(),m_vecTraktPlot);
			m_fNumOfPlotsInTrakt = m_pDB->getNumOfPlotsInTrakt(pTrakt->getTraktID());
		}	// if (m_pDB != NULL)
		// Check that we have plot(s) for this trakt; 070614 p�d
		if (m_vecTraktPlot.size() > 0)
		{
			// If we have plot(s), check if we have data in plot radius.
			// If so, we have a "Cirkelyta/ytor"
			for (UINT i = 0;i < m_vecTraktPlot.size();i++)
			{
				CTransaction_plot plotData = m_vecTraktPlot[i];
				if (plotData.getRadius() > 0.0 && 
						plotData.getLength1() == 0.0 && 
						plotData.getWidth1() == 0.0)
				{
					fRadius += plotData.getRadius();
					fArea += plotData.getArea();
				}
				else if (plotData.getRadius() == 0.0 && 
						plotData.getLength1() > 0.0 && 
						plotData.getWidth1() > 0.0)
				{
					fLength += plotData.getLength1();
					fWidth += plotData.getWidth1();
					fArea += plotData.getArea();
				}

			}	// for (UINT i = 0;i < m_vecTraktPlot.size();i++)
			if (fArea == 0.0)
			{
				m_fNumOfPlotsInTrakt = 1.0;
			}
		}	// if (m_vecTraktPlot.size() > 0)
	}
}

// Remove trakt data in esti_trakt_data_table for this trakt.
// This method's used when adding/changing pricelist in
// settings; 070502 p�d
void CPageTwoFormView::delTraktDataFromDB(int trakt_id)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->removeTraktData(trakt_id);
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

BOOL CPageTwoFormView::getTraktRotpost(int trakt_id)
{

	if (m_pDB != NULL)
	{
		if ( m_pDB->getRotpost(m_vecCTransaction_trakt_rotpost,trakt_id))
		{
			if (m_vecCTransaction_trakt_rotpost.size() == 1)
			{
				m_recTraktRotpost = m_vecCTransaction_trakt_rotpost[0];
				m_nRotpostOrigin = m_recTraktRotpost.getOrigin();

				return TRUE;
			}
		}
	}	// if (pDB != NULL)
	return FALSE;
}


BOOL CPageTwoFormView::saveTraktDataAndAssToDB(BOOL show_msg)
{
	CString S;
	double fSumPercent = 0.0;
	CTransaction_trakt_data recTData;
	CTransaction_trakt_ass recTAss;
	
	CTraktDataReportRec *pRecTData;
	CTraktAssReportRec *pRecTAss;

	CXTPReportRows *pRows;
	CXTPReportRow *pRow;

	CXTPReportRows *pChildRows;
	CXTPReportRow *pChildRow;

	m_wndReport.CollapseAll();
	pRows = m_wndReport.GetRows();

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{

			//==========================================================================
			// Save information on trakt data and assortments associated to trakt; 070314 p�d
			//==========================================================================
			if (pRows != NULL)
			{

				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						pRecTData = (CTraktDataReportRec *)pRow->GetRecord();
						if (pRecTData != NULL)
						{
							recTData = pRecTData->getRecTData();
							// Check if user's changed data directly into
							// the report grid; 070502 p�d
							if (m_wndReport.isDirty())
							{
								recTData = CTransaction_trakt_data(recTData.getTDataID(),
																									 recTData.getTDataTraktID(),
																									 recTData.getTDataType(),
																									 recTData.getSpecieID(),
																									 recTData.getSpecieName(),
																									 pRecTData->getColumnFloat(1),
																									 pRecTData->getColumnLong(2),
																									 pRecTData->getColumnFloat(3),
																									 pRecTData->getColumnFloat(4),
																									 pRecTData->getColumnFloat(5),
																									 pRecTData->getColumnFloat(6),
																									 pRecTData->getColumnFloat(7),
																									 pRecTData->getColumnFloat(8),
																									 pRecTData->getColumnFloat(9),
																									 pRecTData->getColumnFloat(10),
																									 pRecTData->getColumnFloat(11),
																									 pRecTData->getColumnFloat(12),
																									 pRecTData->getColumnFloat(13),
																									 pRecTData->getColumnFloat(14),
																									 pRecTData->getColumnFloat(15),
																									 pRecTData->getColumnFloat(16),
																									 pRecTData->getColumnFloat(17),
																									 _T(""));

							}

							fSumPercent += recTData.getPercent();
							//-----------------------------------------------------
							// Save trakt data for "St�mplingsl�ngd - Uttag"
							if (!m_pDB->addTraktData(recTData))
								m_pDB->updTraktData(recTData);
//*	COMMENTED OUT 2009-03-02 P�D
//		WE SHOULD NOT SAVE DATA FOR "Kvarl�mmnat" AND "Uttag i stickv�g"
//		TO elv_trakt_data_table HERE, BECAUSE THIS DATA IS FOR "Uttag", ONLY; 090302 p�d
							//-----------------------------------------------------
							// 2007-11-21 p�d
							// Also save Trakt data for "St�mplingsl�ngd - Kvarl�mmnat"
							recTData.setTDataType(STMP_LEN_TO_BE_LEFT);
							if (!m_pDB->addTraktData(recTData))
								m_pDB->updTraktData(recTData);
							// Also save Trakt data for "St�mplingsl�ngd - Uttag i Stickv�g"
							recTData.setTDataType(STMP_LEN_WITHDRAW_ROAD);
							if (!m_pDB->addTraktData(recTData))
								m_pDB->updTraktData(recTData);
//*/
							//-----------------------------------------------------

							if (pRow->HasChildren())
							{
								m_pDB->removeTraktAss(recTData.getTDataTraktID(),recTData.getTDataID());
								pChildRows = pRow->GetChilds();
								if (pChildRows != NULL)
								{
									for (int j = 0;j < pChildRows->GetCount();j++)
									{
										pChildRow = pChildRows->GetAt(j);
										if (pChildRow != NULL)
										{
											pRecTAss = (CTraktAssReportRec *)pChildRow->GetRecord();
											//-----------------------------------------------------
											// Save trakt assort
											recTAss = CTransaction_trakt_ass(j+1,
																											 recTData.getTDataTraktID(),
																											 recTData.getTDataID(),
 	 																										 STMP_LEN_WITHDRAW,
																											 pRecTAss->getAssortName(),
																											 pRecTAss->getPriceM3Fub(),
																											 pRecTAss->getPriceM3To(),
																											 pRecTAss->getM3Fub(),
																											 pRecTAss->getM3To(),
																											 // Calculate value of M3FUB; 070612 p�d
																											 pRecTAss->getM3Fub()*pRecTAss->getPriceM3Fub(),
																											 // Calculate value of M3TO; 070612 p�d
																											 pRecTAss->getM3To()*pRecTAss->getPriceM3To(),
																											 _T(""),
																											 0.0);
											// Remove Trakt Assortments if we are goin'
											// to update. Maybe user has removed assortments
											// or added one. We just enter data again; 070315 opd
											if (!m_pDB->addTraktAss(recTAss))
												m_pDB->updTraktAss(recTAss);

											//-----------------------------------------------------

										}	// if (pChildRow != NULL)
									}	// for (int j = 0;j < pChildRows->GetCount();j++)
								}	// if (pChildRows != NULL)
							}	// if (pRow->HasChildren())
						}	// if (pRecTData != NULL)
					} // if (pRow != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)
			}	// if (pRows != NULL)

		}	// if (pDB != NULL)
	}	// if (m_bConnected)

//	populateData();

	m_enumAction = NO_ACTION;

	m_bIsDirty = FALSE;
	m_wndReport.setIsDirty(FALSE);

	return TRUE;
}

void CPageTwoFormView::removeTraktDataSpecieFromDB(void)
{
	CString sMsg;
	CTraktDataReportRec *pRec;
	CTransaction_trakt_data rec;
	CXTPReportRecords *pRecs = NULL;

	pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		m_wndReport.CollapseAll();
		
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CTraktDataReportRec *)pRecs->GetAt(pRow->GetIndex());
			if (pRec != NULL)
			{
				rec = pRec->getRecTData();
			}
			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					// Setup a message for user upon deleting machine; 061010 p�d
					sMsg.Format(_T("%s\n\n%s : %s\n\n%s\n\n%s"),
									m_sRemoveMsg1,
									m_sTraktSpcName,
									rec.getSpecieName(),
									m_sExplRemoveMsg,
									m_sRemoveMsg2);

					if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sRemoveCapMsg,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
					{
						m_pDB->removeTraktDataSpecie(rec);
						pRecs->RemoveAt(pRow->GetIndex());
						populateData();
					}
				}	// if (m_pDB != NULL)
			}	// if (m_bConnected)
		}	// if (pRow != NULL)
	}	// if (pRecs != NULL)
}


void CPageTwoFormView::doShowHideAssortmentInfo(void)
{
	CXTPReportRows *pRows = m_wndReport.GetRows();
	BOOL bExpand = TRUE;
	if (pRows != NULL)
	{
		if (pRows->GetCount() > 0)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				if (pRows->GetAt(i)->IsExpanded())
				{
					bExpand = FALSE;
					m_wndReport.CollapseAll();
				}
			}
		}
	}
	if (bExpand)
	{
		m_wndReport.ExpandAll();
	}
}

void CPageTwoFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PAGE2_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReport.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
/*	
	// Get selected column index into registry; 070219 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);
*/
}

void CPageTwoFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReport.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PAGE2_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
/*
	// Set selected column index into registry; 070219 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);
*/
}


void CPageTwoFormView::OnEnChangeEdit24()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
