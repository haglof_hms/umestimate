#pragma once

#include "Resource.h"


// COnNewTraktDialog dialog

class COnNewTraktDialog : public CXTResizeDialog
{
	DECLARE_DYNAMIC(COnNewTraktDialog)

public:
	COnNewTraktDialog(CWnd* pParent = NULL,CUMEstimateDB *pDB=NULL);   // standard constructor
	virtual ~COnNewTraktDialog();

	virtual INT_PTR DoModal();

	void setTemplates(vecTransactionTemplate &v)
	{
			m_vecTransactionTemplate = v;
			m_bIsTemplates = (m_vecTransactionTemplate.size() > 0);
	}

	CString getXMLFile(void)
	{
		return m_sXMLFile;
	}

	int getCreateTraktSelection(void)
	{
		return m_nCreateTraktSelection;
	}

// Dialog Data
	//{{AFX_DATA(CButtonDlg)
	enum { IDD = IDD_DIALOG1 };

	CXTListCtrl m_wndLCtrl;
	CXTHeaderCtrl   m_Header;

	CButton m_rbNoTemplate;
	CButton m_rbUseTemplate;
	CComboBox m_cbTemplates;
	CButton m_btnOK;
	CButton m_btnCancel;
	CXTResizeGroupBox m_wndGroup1;
	//}}AFX_DATA
protected:

	CUMEstimateDB *m_pDB;
	CString m_sPricelistErrorStatus;
	CString m_sCostErrorStatus;
	CString m_sErrorStatus;
	CString m_sXMLFile;
	BOOL m_bIsTemplates;
	int m_nCreateTraktSelection;
	
	vecTransactionTemplate m_vecTransactionTemplate;
	void setTemplatesInList(void);
//	void setTemplatesInCBox(void);

	void setLanguage(void);
	int CheckStandTemplate(CTransaction_template rec);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedRadio1();
//	afx_msg void OnBnClickedRadio2();
//	afx_msg void OnCbnSelchangeCombo1();

	afx_msg void OnOKBtn();
	afx_msg void OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult);
};
