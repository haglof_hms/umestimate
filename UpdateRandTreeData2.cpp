// UpdateRandTreeData2.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "UpdateRandTreeData2.h"
#include "MDITabbedView.h"

#include "ResLangFileReader.h"

// CUpdateRandTreeData2 dialog

IMPLEMENT_DYNAMIC(CUpdateRandTreeData2, CDialog)

BEGIN_MESSAGE_MAP(CUpdateRandTreeData2, CDialog)
	ON_BN_CLICKED(IDOK, &CUpdateRandTreeData2::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CUpdateRandTreeData2::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CUpdateRandTreeData2::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_RADIO1, &CUpdateRandTreeData2::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CUpdateRandTreeData2::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CUpdateRandTreeData2::OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_BUTTON3, &CUpdateRandTreeData2::OnBnClickedButton3)
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnReportItemValueClick)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

CUpdateRandTreeData2::CUpdateRandTreeData2(vecTransactionSampleTree &vecTraktSampleTrees,
										   CStringArray &sarrTreeTypes,
										   vecTransactionSampleTreeCategory &vecSmpTreeCategories,
										   vecTransactionSpecies &vecSpecies,
										   CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateRandTreeData2::IDD, pParent)
{
	m_vecTraktSampleTrees = vecTraktSampleTrees;
	m_sarrTreeTypes.Copy(sarrTreeTypes);
	m_vecSmpTreeCategories = vecSmpTreeCategories;
	m_vecSpecies = vecSpecies;
}

CUpdateRandTreeData2::~CUpdateRandTreeData2()
{
}

void CUpdateRandTreeData2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);
	DDX_Control(pDX, IDC_RADIO1, m_wndRadio1);
	DDX_Control(pDX, IDC_RADIO2, m_wndRadio2);
	DDX_Control(pDX, IDC_RADIO3, m_wndRadio3);
	DDX_Control(pDX, IDC_BUTTON1, m_wndButton1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndButton2);
	DDX_Control(pDX, IDCANCEL, m_wndButtonCancel);
	DDX_Control(pDX, IDC_EDIT1, m_wndText1);
	DDX_Control(pDX, IDOK, m_wndButtonOK);
	DDX_Control(pDX, IDC_BUTTON3, m_wndButtonRemove);
}

INT_PTR CUpdateRandTreeData2::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

void CUpdateRandTreeData2::OnGetMinMaxInfo(MINMAXINFO * lpMMI)
{
	lpMMI->ptMinTrackSize.x = 600;
	lpMMI->ptMinTrackSize.y = 300;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CUpdateRandTreeData2::myOnSize()
{
		CRect rect;
	GetClientRect(&rect);
	setResize(&m_wndReport,10,30,rect.right-20,rect.bottom-140,TRUE);

	setResize(&m_wndCBox1		,10	,rect.bottom-70,135,12,TRUE);
	setResize(&m_wndCBox2		,170,rect.bottom-70,190,12,TRUE);
	setResize(&m_wndText1		,380,rect.bottom-70,125,18,TRUE);

	setResize(&m_wndRadio1		,12	,rect.bottom-87,76 ,12,TRUE);
	setResize(&m_wndRadio2		,172,rect.bottom-87,76 ,12,TRUE);
	setResize(&m_wndRadio3		,382,rect.bottom-87,76 ,12,TRUE);

	setResize(&m_wndButtonOK	,160,rect.bottom-40,70 ,23,TRUE);
	setResize(&m_wndButtonRemove,250,rect.bottom-40,70 ,23,TRUE);
	setResize(&m_wndButtonCancel,340,rect.bottom-40,70 ,23,TRUE);
	Invalidate();
}

void CUpdateRandTreeData2::OnSize(UINT nType,int cx,int cy)
{
	myOnSize();
	CDialog::OnSize(nType,cx,cy);
}

BOOL CUpdateRandTreeData2::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	RLFReader xml;
	if (fileExists(m_sLangFN))
	{
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING370)));
			m_wndRadio1.SetWindowText(xml.str(IDS_STRING3270));
			m_wndRadio2.SetWindowText(xml.str(IDS_STRING3271));
			m_wndRadio3.SetWindowText(xml.str(IDS_STRING3272));
			m_wndButton1.SetWindowText(xml.str(IDS_STRING3273));
			m_wndButton2.SetWindowText(xml.str(IDS_STRING3274));
			m_wndButtonCancel.SetWindowText(xml.str(IDS_STRING3275));
			m_wndButtonOK.SetWindowText(xml.str(IDS_STRING3276));
			m_wndButtonRemove.SetWindowText(xml.str(IDS_STRING3277));

			m_sRemoveSelected = xml.str(IDS_STRING3278);
		}
	}

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		CXTPReportColumn *pCol = NULL;

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
		m_wndReport.GetReportHeader()->SetAutoColumnSizing( FALSE );
		m_wndReport.EnableScrollBar(SB_HORZ, TRUE );
		m_wndReport.EnableScrollBar(SB_VERT, TRUE );
		m_wndReport.ShowWindow( SW_NORMAL );

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_CHECK, _T(""), 30));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0+1, (xml.str(IDS_STRING262)), 40));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		//pCol->GetEditOptions()->AddExpandButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1+1, (xml.str(IDS_STRING263)), 60));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2+1, (xml.str(IDS_STRING264)), 70));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3+1, (xml.str(IDS_STRING265)), 70));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4+1, (xml.str(IDS_STRING266)), 65));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5+1, (xml.str(IDS_STRING267)), 120));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6+1, (xml.str(IDS_STRING268)), 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7+1, (xml.str(IDS_STRING271)), 120));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_8+1, (xml.str(IDS_STRING273)), 40));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_9+1, (xml.str(IDS_STRING274)), 50));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_10+1, (xml.str(IDS_STRING2720)), 110));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_11+1, (xml.str(IDS_STRING2721)), 90));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_12+1, (xml.str(IDS_STRING269)), 60));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_13+1, (xml.str(IDS_STRING270)), 70));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		
		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_14+1, (xml.str(IDS_STRING2700)), 60));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_15+1, (xml.str(IDS_STRING275)), 140));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_16+1, (xml.str(IDS_STRING276)), 120));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_UPPERCASE; 
		//pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER;

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_17+1, xml.str(IDS_STRING294), 100));	// _T("Kategori")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_18+1, xml.str(IDS_STRING295), 100));	// _T("Fasavstånd")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_19+1, xml.str(IDS_STRING296), 130));	// _T("Toppningsvärde")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_20+1, xml.str(IDS_STRING2961), 100));	// _T("Koordinat")
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
	}

	xml.clean();

	for (UINT i = 0;i < m_vecTraktSampleTrees.size();i++)
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;

		pRec->AddItem(new CTextItem(_T("")))->HasCheckbox(TRUE);
		pRec->AddItem(new CIntItemInplaceBtn(m_vecTraktSampleTrees[i].getPlotID(),m_sLangFN,GetParent()->GetSafeHwnd()));	// Plot (Group) ID
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getSpcID()));								// Specioe id
		pRec->AddItem(new CTextItem((m_vecTraktSampleTrees[i].getSpcName())));					// Name of Specie (Combobox)
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDBH(),sz1dec));					// Diameter at breastheight
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getHgt(),sz1dec));					// Height of tree
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getGCrownPerc(),sz1dec));	// Percent "Grönkrona"
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getBarkThick(),sz1dec));		// Bark thickness mm
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getGROT(),sz1dec));				// GROT kg
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getAge()));									// Age
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getGrowth()));								// "Tillväxt"
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDCLS_from(),sz1dec));		// Diameter class (cm)
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getDCLS_to(),sz1dec));			// Diameter class (cm)
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getM3Sk(),sz3dec));				// M3Sk
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getM3Fub(),sz3dec));				// M3Fub
		pRec->AddItem(new CFloatItem(m_vecTraktSampleTrees[i].getM3Ub(),sz3dec));				// M3Ub
		if (m_vecTraktSampleTrees[i].getTreeType() >= 0 && m_vecTraktSampleTrees[i].getTreeType() < m_sarrTreeTypes.GetCount())
		{
			pRec->AddItem(new CTextItem(m_sarrTreeTypes.GetAt(m_vecTraktSampleTrees[i].getTreeType())));// Type tree (Provträd etc)
		}
		else if (m_sarrTreeTypes.GetCount() > 0)
		{
			pRec->AddItem(new CTextItem(m_sarrTreeTypes.GetAt(0)));// Type tree (Provträd etc)
		}
		pRec->AddItem(new CExTextH100Item((m_vecTraktSampleTrees[i].getBonitet())));					// H100 (m)

		if(m_vecTraktSampleTrees[i].getCategory() != -1)
			pRec->AddItem(new CTextItem( getValueForCategory(&m_vecSmpTreeCategories, m_vecTraktSampleTrees[i].getCategory()) ));					// Category
		else
			pRec->AddItem(new CTextItem(_T("")));

		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getDistCable()));								// Dist cable
		pRec->AddItem(new CIntItem(m_vecTraktSampleTrees[i].getTopCut()));								// Toppningsvärde
		pRec->AddItem(new CTextItem( m_vecTraktSampleTrees[i].getCoord() ));					// Koordinat

		m_wndReport.AddRecord(pRec);
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	for (UINT i = 0;i < m_vecSpecies.size(); i++)
	{
		m_wndCBox1.AddString(m_vecSpecies[i].getSpcName());
	}

	for (UINT i = 0;i < m_sarrTreeTypes.GetCount(); i++)
	{
		m_wndCBox2.AddString(m_sarrTreeTypes[i].GetString());
	}

	m_wndCBox1.EnableWindow(FALSE);
	m_wndCBox2.EnableWindow(FALSE);
	m_wndText1.EnableWindow(FALSE);

	m_wndText1.SetAsNumeric();
	myOnSize();
	OnReportItemValueClick(NULL,NULL);

	return TRUE;
}



// CUpdateRandTreeData2 message handlers

void CUpdateRandTreeData2::OnBnClickedOk()
{
	OnOK();

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			CXTPReportRecords *pRecs = m_wndReport.GetRecords();
			for (UINT i=0; i<pRecs->GetCount(); i++)
			{
				if (pRecs->GetAt(i)->GetItem(COLUMN_CHECK)->IsChecked())
				{
					float dbh;
					int spc, type;
					CString spcname;
					if (m_wndRadio1.GetCheck() && m_wndCBox1.GetCurSel() >= 0)
					{
						for (UINT j=0; j<m_vecSpecies.size(); j++)
						{
							m_wndCBox1.GetLBText(m_wndCBox1.GetCurSel(), spcname);
							if( m_vecSpecies[j].getSpcName() == spcname )
							{
								spc = m_vecSpecies[j].getSpcID();
							}
						}
					}
					else
					{
						spc = m_vecTraktSampleTrees[i].getSpcID();
						spcname = m_vecTraktSampleTrees[i].getSpcName();
					}

					if (m_wndRadio2.GetCheck() && m_wndCBox2.GetCurSel() >= 0)
					{
						type = m_wndCBox2.GetCurSel();
					}
					else
					{
						type = m_vecTraktSampleTrees[i].getTreeType();
					}

					if (m_wndRadio3.GetCheck() && m_wndText1.getFloat() > 0)
					{
						dbh = m_wndText1.getFloat();
					}
					else
					{
						dbh = m_vecTraktSampleTrees[i].getDBH();
					}

					CTransaction_sample_tree data2 = CTransaction_sample_tree(m_vecTraktSampleTrees[i].getTreeID()
						,m_vecTraktSampleTrees[i].getTraktID()
						,m_vecTraktSampleTrees[i].getPlotID()
						,m_vecTraktSampleTrees[i].getSpcID()
						,m_vecTraktSampleTrees[i].getSpcName()
						,dbh
						,m_vecTraktSampleTrees[i].getHgt()
						,m_vecTraktSampleTrees[i].getGCrownPerc()
						,m_vecTraktSampleTrees[i].getBarkThick()
						,m_vecTraktSampleTrees[i].getGROT()
						,m_vecTraktSampleTrees[i].getAge()
						,m_vecTraktSampleTrees[i].getGrowth()
						,m_vecTraktSampleTrees[i].getDCLS_from()
						,m_vecTraktSampleTrees[i].getDCLS_to()
						,m_vecTraktSampleTrees[i].getM3Sk()
						,m_vecTraktSampleTrees[i].getM3Fub()
						,m_vecTraktSampleTrees[i].getM3Ub()
						,type 
						,m_vecTraktSampleTrees[i].getBonitet()
						,m_vecTraktSampleTrees[i].getCreated()
						,m_vecTraktSampleTrees[i].getCoord()
						,m_vecTraktSampleTrees[i].getDistCable()
						,m_vecTraktSampleTrees[i].getCategory()
						,m_vecTraktSampleTrees[i].getTopCut());

					if (m_wndRadio1.GetCheck() && spc != m_vecTraktSampleTrees[i].getSpcID())
						pView->doSpecieHasChanged(data2, spc, spcname);
					else if (m_wndRadio2.GetCheck() && type != m_vecTraktSampleTrees[i].getTreeType())
						pView->doTreeTypeHasChanged(data2, m_vecTraktSampleTrees[i].getTreeType(), type);
					else if (m_wndRadio3.GetCheck() && dbh != m_vecTraktSampleTrees[i].getDBH())
						pView->doDbhHasChanged(data2, m_vecTraktSampleTrees[i].getDBH(), dbh);
				}
			}
		}
	}
}

void CUpdateRandTreeData2::OnBnClickedButton1()
{
	for (UINT i=0; i<m_wndReport.GetRecords()->GetCount(); i++)
	{
		m_wndReport.GetRecords()->GetAt(i)->GetItem(0)->SetChecked(TRUE);
	}
	m_wndReport.Populate();

	OnReportItemValueClick(NULL,NULL);
}

void CUpdateRandTreeData2::OnBnClickedButton2()
{
	for (UINT i=0; i<m_wndReport.GetRecords()->GetCount(); i++)
	{
		m_wndReport.GetRecords()->GetAt(i)->GetItem(0)->SetChecked(FALSE);
	}
	m_wndReport.Populate();

	OnReportItemValueClick(NULL,NULL);
}

void CUpdateRandTreeData2::OnBnClickedRadio1()
{
	m_wndCBox1.EnableWindow(m_wndRadio1.GetCheck());
	m_wndCBox2.EnableWindow(!m_wndRadio1.GetCheck());
	m_wndText1.EnableWindow(!m_wndRadio1.GetCheck());
}

void CUpdateRandTreeData2::OnBnClickedRadio2()
{
	m_wndCBox1.EnableWindow(!m_wndRadio2.GetCheck());
	m_wndCBox2.EnableWindow(m_wndRadio2.GetCheck());
	m_wndText1.EnableWindow(!m_wndRadio2.GetCheck());
}

void CUpdateRandTreeData2::OnBnClickedRadio3()
{
	m_wndCBox1.EnableWindow(!m_wndRadio3.GetCheck());
	m_wndCBox2.EnableWindow(!m_wndRadio3.GetCheck());
	m_wndText1.EnableWindow(m_wndRadio3.GetCheck());
}

void CUpdateRandTreeData2::OnBnClickedButton3()
{
	if (UMMessageBox(m_sRemoveSelected, MB_YESNO|MB_ICONEXCLAMATION) == IDYES)
	{
		OnOK();

		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView != NULL)
		{
			CPageThreeFormView *pView = pTabView->getPageThreeFormView();
			if (pView != NULL)
			{
				CXTPReportRecords *pRecs = m_wndReport.GetRecords();
				for (UINT i=0; i<pRecs->GetCount(); i++)
				{
					if (pRecs->GetAt(i)->GetItem(COLUMN_CHECK)->IsChecked())
					{
						pView->doDeleteTree(m_vecTraktSampleTrees[i]);
					}
				}
			}
		}
	}
}

void CUpdateRandTreeData2::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bEnable = FALSE;

	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	for (UINT i=0; i<pRecs->GetCount(); i++)
	{
		if (pRecs->GetAt(i)->GetItem(COLUMN_CHECK)->IsChecked())
		{
			bEnable = TRUE;
			break;
		}
	}

	m_wndButtonOK.EnableWindow(bEnable);
	m_wndButtonRemove.EnableWindow(bEnable);
}
