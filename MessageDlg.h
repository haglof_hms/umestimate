#pragma once

#include "Resource.h"

#include "XHTMLStatic.h"
// CMessageDialog dialog

class CMessageDialog : public CDialog
{
	DECLARE_DYNAMIC(CMessageDialog)

	CString m_sCaption;
	CString m_sOKBtn;
	CString m_sMsgText;
public:
	CMessageDialog(CWnd* pParent = NULL);   // standard constructor
	CMessageDialog(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR msg,CWnd* pParent = NULL); 
	virtual ~CMessageDialog();

	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

	CXHTMLStatic m_wndHTML;

	CButton m_wndOKBtn;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
