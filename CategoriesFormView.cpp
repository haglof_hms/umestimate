// CategoryFormView.cpp : implementation file
//

#include "stdafx.h"
#include "CategoriesFormView.h"
#include "ResLangFileReader.h"

// CCategoriesFormView

IMPLEMENT_DYNCREATE(CCategoriesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCategoriesFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CCategoriesFormView::CCategoriesFormView()
	: CXTResizeFormView(CCategoriesFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CCategoriesFormView::~CCategoriesFormView()
{
}

void CCategoriesFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecCategories.clear();

	m_wndReport1.ClearReport();

	CXTResizeFormView::OnDestroy();
}

void CCategoriesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CCategoriesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CCategoriesFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
	m_wndReport1.GetWindowRect(&clip);

	ScreenToClient(&clip);
	pDC->ExcludeClipRect(&clip);

	pDC->GetClipBox(&clip);
	pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CCategoriesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL CCategoriesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CCategoriesFormView::doSetNavigationBar()
{
	if (m_vecCategories.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CCategoriesFormView diagnostics

#ifdef _DEBUG
void CCategoriesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CCategoriesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CCategoriesFormView message handlers

BOOL CCategoriesFormView::getCategories(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			m_pDB->GetCategories(m_vecCategories);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CCategoriesFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CCategoriesFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_TRAKT_TYPE_REPORT, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				m_sMsgCap	= xml->str(IDS_STRING151);
				m_sHgtClassID	= xml->str(IDS_STRING176);
				m_sHgtClass	= xml->str(IDS_STRING1598);
				m_sOKBtn	= xml->str(IDS_STRING155);
				m_sCancelBtn	= xml->str(IDS_STRING156);
				m_sText1	= xml->str(IDS_STRING1596);
				m_sText3	= xml->str(IDS_STRING1597);
				m_sDoneSavingMsg = xml->str(IDS_STRING180);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, _T("ID"), 10));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->GetEditOptions()->m_nMaxLength = 4;

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, _T("Kategori"), 20));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->GetEditOptions()->m_nMaxLength = 45;

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, _T("Notering"), 50));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->GetEditOptions()->m_nMaxLength = 255;

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				getCategories();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 1,rect.bottom - 1);

				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CCategoriesFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CCategoriesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addCategory();
			if (saveCategories())
			{
				getCategories();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			removeAllCategories();	// remove all categories from DB first, to avoid ID conflicts
			if (saveCategories())
			{
				getCategories();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			// Reload hgtclasses; 090129 p�d
			getCategories();
			if (removeCategory())
			{
				getCategories();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CCategoriesFormView::populateReport(void)
{
	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecCategories.size() > 0)
	{
		for (UINT i = 0;i < m_vecCategories.size();i++)
		{
			CTransaction_sample_tree_category rec = m_vecCategories[i];
			m_wndReport1.AddRecord(new CCategoriesReportRec(rec.getID(),rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL CCategoriesFormView::addCategory(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	CTransaction_sample_tree_category rec;

	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		int id = 0;
		for (int nLoop=0; nLoop<m_vecCategories.size(); nLoop++)
		{
			rec = m_vecCategories.at(nLoop);
			if (rec.getID() > id) id = rec.getID();
		}
		id++;

		m_wndReport1.AddRecord(new CCategoriesReportRec(id));
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

BOOL CCategoriesFormView::saveCategories(void)
{
	CTransaction_sample_tree_category rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{
				// make sure we don't have duplicate id:s
				for (int i = 0;i < pRecs->GetCount(); i++)
				{
					CCategoriesReportRec *pRecI = (CCategoriesReportRec *)pRecs->GetAt(i);

					for (int j = i+1; j < pRecs->GetCount(); j++)
					{
						CCategoriesReportRec *pRecJ = (CCategoriesReportRec *)pRecs->GetAt(j);
						if (pRecI->getColumnInt(0) == pRecJ->getColumnInt(0) )
						{
							return FALSE;
						}
					}
				}

				// save all records to DB
				for (int i = 0; i < pRecs->GetCount(); i++)
				{
					CCategoriesReportRec *pRec = (CCategoriesReportRec *)pRecs->GetAt(i);

					rec = CTransaction_sample_tree_category(pRec->getColumnInt(0),
 											 	pRec->getColumnText(1),
 											 	pRec->getColumnText(2),
												_T("") );

					if (!m_pDB->addCategory(rec))
					{
						int nOldId = -1;
						nOldId = m_vecCategories.at(i).getID();
						m_pDB->updCategory(nOldId, rec);
					}

				}	// for (int i = 0;i < pRecs->GetCount();i++)

			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CCategoriesFormView::removeAllCategories(void)
{
	if (m_pDB != NULL)
	{
		return m_pDB->removeAllCategories();
	}

	return TRUE;
}

BOOL CCategoriesFormView::removeCategory(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_sample_tree_category data;
	CCategoriesReportRec *pRec = NULL;
	CString sMsg;
	if (m_bConnected)	
	{
		pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CCategoriesReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				data = CTransaction_sample_tree_category(pRec->getID(),pRec->getColumnText(1),pRec->getColumnText(2),_T(""));
				if (m_pDB != NULL)
				{
					sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n\n%s"),
											m_sText1,
											m_sHgtClass,
											data.getCategory(),
											m_sHgtClassNotes,
											data.getNotes(),
											m_sText3);

					if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
					{
						m_pDB->removeCategory(data);
					}
				}
			}
		}
		return TRUE;
	}
	return FALSE;
}

void CCategoriesFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}
