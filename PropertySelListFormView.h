#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"

/////////////////////////////////////////////////////////////////////////////////
// CPropertyReportDataRec

class CPropertyReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_property m_recData;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CPropertyReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
/*
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
*/
	}

	CPropertyReportDataRec(UINT index,CTransaction_property& data)	 
	{
		CString sValue;
		m_nIndex = index;
		m_recData = data;
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
//		AddItem(new CTextItem(_T(data.getParishName())));
		AddItem(new CTextItem((data.getPropertyNum())));
		AddItem(new CTextItem((data.getPropertyName())));
		AddItem(new CTextItem((data.getBlock())));
		AddItem(new CTextItem((data.getUnit())));
/*
		sValue.Format("%.1f",data.getAreal());
		AddItem(new CTextItem(_T(sValue)));
		sValue.Format("%.1f",data.getArealMeasured());
		AddItem(new CTextItem(_T(sValue)));
*/
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
		
	CTransaction_property& getRecord(void)	{ return m_recData; }
	
};

//////////////////////////////////////////////////////////////////////////////
// CPropertySelListFormView form view

class CPropertySelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CPropertySelListFormView)

	int m_nActiveTraktID;
protected:
	CPropertySelListFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertySelListFormView();

	vecTransactionProperty m_vecPropertyData;
	BOOL setupReport(void);
	void populateReport(void);
	void getProperties(void);

	void LoadReportState(void);
	void SaveReportState(void);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
