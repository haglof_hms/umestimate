#if !defined(__INFORMATIONFORMVIEW_H__)
#define __INFORMATIONFORMVIEW_H__

#include "afxhtml.h"
#include "StandEntryDoc.h"

// CMDIInformationView view
class CMDIFrameDoc;

class CMDIInformationView : public CHtmlView
{
	DECLARE_DYNCREATE(CMDIInformationView)

protected:
	CMDIInformationView();           // protected constructor used by dynamic creation
	virtual ~CMDIInformationView();

public:

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
//	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);

	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CMDIFrameDoc* GetDocument();
	void UpdateDocTitle();


protected:
	//{{AFX_MSG(CTabbedViewView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CMDIInformationView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif



#endif