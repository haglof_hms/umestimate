// UpdateRandTreeData.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "UpdateRandTreeData.h"

#include "ResLangFileReader.h"

// CUpdateRandTreeData dialog

IMPLEMENT_DYNAMIC(CUpdateRandTreeData, CDialog)

BEGIN_MESSAGE_MAP(CUpdateRandTreeData, CDialog)
	ON_BN_CLICKED(IDOK, &CUpdateRandTreeData::OnBnClickedOk)
END_MESSAGE_MAP()

CUpdateRandTreeData::CUpdateRandTreeData(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateRandTreeData::IDD, pParent)
{

}

CUpdateRandTreeData::~CUpdateRandTreeData()
{
}

void CUpdateRandTreeData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);
	DDX_Control(pDX, IDC_GROUP2, m_wndGroup2);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);

	DDX_Control(pDX, IDOK, m_wndBtnUpdate);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);

	DDX_Control(pDX, IDC_CHECK1, m_wndCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_wndCheck2);

	//}}AFX_DATA_MAP

}

INT_PTR CUpdateRandTreeData::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL CUpdateRandTreeData::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING370)));

			m_wndGroup1.SetWindowText((xml.str(IDS_STRING3700)));
			m_wndGroup2.SetWindowText((xml.str(IDS_STRING3701)));

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING3702)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING3703)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING3702)));
			m_wndLbl4.SetWindowText((xml.str(IDS_STRING3703)));

			m_wndCheck1.SetWindowText((xml.str(IDS_STRING3706)));
			m_wndCheck2.SetWindowText((xml.str(IDS_STRING3707)));

			m_wndBtnUpdate.SetWindowText((xml.str(IDS_STRING3704)));
			m_wndBtnCancel.SetWindowText((xml.str(IDS_STRING3705)));

			m_sMsgCap = (xml.str(IDS_STRING289));
			m_sMsgCheckBox.Format(_T("%s\n%s\n\n"),(xml.str(IDS_STRING3708)),(xml.str(IDS_STRING3709)));

			m_sMsgAgeMissing = (xml.str(IDS_STRING3710));
			m_sMsgBonitetMissing = (xml.str(IDS_STRING3711));
		}
		xml.clean();
	}

	m_wndEdit1.SetAsNumeric();
	m_wndEdit2.SetAsNumeric();
	// Set Mask for COXMaskedEdit control; 071101 p�d
	m_wndEdit3.SetMask(_T(">##"));
	m_wndEdit3.SetPromptSymbol(' ');
	m_wndEdit4.SetMask(_T(">##"));
	m_wndEdit4.SetPromptSymbol(' ');

	return TRUE;
}

// CUpdateRandTreeData message handlers

void CUpdateRandTreeData::OnBnClickedOk()
{

	// Check if user's seelcted checkbox for Age and/or Bonitet.
	// If not tell user so and DON'T QUIT; 080624 p�d
	if (m_wndCheck1.GetCheck() == BST_UNCHECKED &&
			m_wndCheck2.GetCheck() == BST_UNCHECKED)
	{
		UMMessageBox(this->GetSafeHwnd(),(m_sMsgCheckBox),(m_sMsgCap),MB_ICONINFORMATION | MB_OK);
		return;
	}

	// On Age; check that user, at least, added an age to; 080624 p�d
	if (m_wndCheck1.GetCheck() == BST_CHECKED &&
			m_wndEdit2.getText().IsEmpty())
	{
		UMMessageBox(this->GetSafeHwnd(),(m_sMsgAgeMissing),(m_sMsgCap),MB_ICONINFORMATION | MB_OK);
		return;
	}

	// On Age; check that user, at least, added an age to; 080624 p�d
	if (m_wndCheck2.GetCheck() == BST_CHECKED &&
			m_wndEdit4.GetInputData().IsEmpty())
	{
		UMMessageBox(this->GetSafeHwnd(),(m_sMsgBonitetMissing),(m_sMsgCap),MB_ICONINFORMATION | MB_OK);
		return;
	}

	m_bDoAge = (m_wndCheck1.GetCheck() == BST_CHECKED);
	m_bDoBonitet = (m_wndCheck2.GetCheck() == BST_CHECKED);

	m_nAgeFrom = m_wndEdit1.getInt();
	m_nAgeTo = m_wndEdit2.getInt();

	m_sBonitetFrom = m_wndEdit3.GetInputData();
	m_sBonitetTo = m_wndEdit4.GetInputData();

	OnOK();
}
