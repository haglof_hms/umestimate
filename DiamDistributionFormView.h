#pragma once

#include "ChartViewer.h"

#include "Resource.h"

// CDiamDistributionFormView form view

class CDiamDistributionFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CDiamDistributionFormView)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CString m_sDiagramCaption;
	CString m_sDiagramXLabel;
	CString m_sDiagramYLabel;
	CString m_sPropertyNameLabel;
	CString m_sShowAllCB;

	CString m_sStandNumber;
	CString m_sStandName;
	CString m_sPropertyName;

	CFont m_fntCaption;
	CFont m_fntText;
	CFont m_fntTextBold;
	CFont m_fntTextSmall;

	CMyExtStatic m_wndLbl1;
	CComboBox m_wndCBox1;

	CChartViewer	m_chartViewer;
	int m_nTraktID;
	int m_nShowSpecies;	// -1 = All species
	static double m_fMaxDCLS;
	int m_nMax;

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	enum drawType { DRAW_VIEW, DRAW_PREVIEW };

	void drawChartView(bool redraw = false,drawType dt = DRAW_VIEW,CDC* pDC = NULL);

	char tmp[255];
	char *convStr(LPCTSTR s)	
	{
		sprintf(tmp,"%S",s);
		return tmp;
	}

	static int m_nCurCBSel;

protected:
	CDiamDistributionFormView();           // protected constructor used by dynamic creation
	virtual ~CDiamDistributionFormView();

	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(int trakt_id);

	vecTransactionDCLSTree m_vecDCLS;
	vecTransactionDCLSTree m_vecDCLSTrees;
	void getTraktDCLSFromDB(int trakt_id);

	vecTransactionTraktData m_vecTraktData;
	void getTraktDataFromDB(int trakt_id);

public:
	enum { IDD = IDD_FORMVIEW13 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	inline void doDrawChartView(bool redraw,bool reset) 
	{ 
		if (reset)
		{
			m_nShowSpecies = -1;
			m_nCurCBSel = 0;
		}
		drawChartView(redraw); 	
	}

	inline void doRunPrintPreview(void) { OnPrintPreview(); }

	inline void setPropertyName(LPCTSTR name)	{ m_sPropertyName = name; }

	void doRunPrint();

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageThreeFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageThreeFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPrint(CDC* pDC, CPrintInfo *pInfo);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPrintPreview();
	afx_msg void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnCBoxSpeciesChanged(void);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


