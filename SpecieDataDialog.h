#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"

#include "ResLangFileReader.h"


extern StrMap global_langMap;
/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Origin (Ursprung); 070228 p�d

/////////////////////////////////////////////////////////////////////////////
// CAssortmentReportRec

class CAssortmentReportRec : public CXTPReportRecord
{
	CTransaction_trakt_ass recAss;
	int m_nID;
protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};


	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CAssortmentReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		this->GetItem(COLUMN_0)->SetBackgroundColor(CYAN);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_5)->SetBackgroundColor(RGB(255,193,193));
		this->GetItem(COLUMN_6)->SetBackgroundColor(RGB(255,193,193));
	}

	CAssortmentReportRec(int id,CTransaction_trakt_ass rec)
	{
		m_nID	= id;
		recAss = rec;
		AddItem(new CTextItem(rec.getTAssName() ));
		AddItem(new CFloatItem(rec.getM3FUB(),sz3dec));
		AddItem(new CFloatItem(rec.getM3TO(),sz3dec));
		AddItem(new CFloatItem(rec.getPriceM3FUB(),sz2dec));
		AddItem(new CFloatItem(rec.getPriceM3TO(),sz2dec));
		AddItem(new CFloatItem(rec.getValueM3FUB(),sz2dec));
		AddItem(new CFloatItem(rec.getValueM3TO(),sz2dec));
		AddItem(new CFloatItem(rec.getKrPerM3(),sz0dec));
		this->GetItem(COLUMN_0)->SetBackgroundColor(CYAN);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_5)->SetBackgroundColor(RGB(255,193,193));
		this->GetItem(COLUMN_6)->SetBackgroundColor(RGB(255,193,193));
	}

	int getID(void)
	{
		return m_nID;
	}

	CTransaction_trakt_ass& getRecord(void)
	{
		return recAss;
	}
	
	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CTransferReportRec

class CTransferReportRec : public CXTPReportRecord
{
	CTransaction_trakt_trans recTrans;
	int m_nID;
protected:
	//////////////////////////////////////////////////////////////////////////
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};


	//////////////////////////////////////////////////////////////////////////
	class CCheckFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
		double m_fMaxValue;
		CString m_sMsgCap;
		CString m_sMsgMax;
		CString m_sMsgMin;
	public:
		CCheckFloatItem(double fValue,double max_val,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue),m_fMaxValue(max_val)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
			m_sMsgCap = global_langMap[IDS_STRING2981];
			m_sMsgMax = global_langMap[IDS_STRING2982];
			m_sMsgMin = global_langMap[IDS_STRING2983];
			CString S;
			if (m_fValue > m_fMaxValue)
			{
				S.Format(L"%s\n%s = %.2f %%",m_sMsgCap,m_sMsgMax,m_fMaxValue);
				UMMessageBox(S);
				m_fValue = 0.0;
			}
			else if (m_fValue < 0.0)
			{
				S.Format(L"%s\n%s = %.1f %%",m_sMsgCap,m_sMsgMin,0.0);
				UMMessageBox(S);
				m_fValue = 0.0;
			}
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				CString S;
				if (m_fValue > m_fMaxValue)
				{
					S.Format(L"%s\n%s = %.2f %%",m_sMsgCap,m_sMsgMax,m_fMaxValue);
					UMMessageBox(S);
					m_fValue = 0.0;
				}
				else if (m_fValue < 0.0)
				{
					S.Format(L"%s\n%s = %.1f %%",m_sMsgCap,m_sMsgMin,0.0);
					UMMessageBox(S);
					m_fValue = 0.0;
				}
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			CString S;
			if (m_fValue > m_fMaxValue)
			{
				S.Format(L"%s\n%s = %.2f %%",m_sMsgCap,m_sMsgMax,m_fMaxValue);
				UMMessageBox(S);
				m_fValue = 0.0;
			}
			else if (m_fValue < 0.0)
			{
				S.Format(L"%s\n%s = %.1f %%",m_sMsgCap,m_sMsgMin,0.0);
				UMMessageBox(S);
				m_fValue = 0.0;
			}
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	CString sTmp;
public:

	CTransferReportRec(void)
	{

		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CCheckFloatItem(0.0,MAX_PERCENT,sz1dec));
//		AddItem(new CFloatItem(0.0,sz1dec));
		sTmp.Format(L"%.1f",0.0);
		AddItem(new CTextItem(sTmp));
		this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_4)->SetTextColor(BLUE);
	}

	CTransferReportRec(int id,CTransaction_trakt_trans rec,LPCTSTR m3_in)
	{
		m_nID	= id;
		recTrans = rec;
		AddItem(new CTextItem(rec.getFromName() ));
		AddItem(new CTextItem(rec.getToName() ));
		AddItem(new CFloatItem(rec.getM3FUB(),sz2dec));
		AddItem(new CCheckFloatItem(rec.getPercent(),MAX_PERCENT,sz2dec));
//		AddItem(new CFloatItem(rec.getM3Trans(),sz3dec));
		sTmp.Format(L"%.2f %s",rec.getM3Trans(),m3_in);
		AddItem(new CTextItem(sTmp));
		this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_4)->SetTextColor(BLUE);
	}

	int getID(void)
	{
		return m_nID;
	}

	CTransaction_trakt_trans& getRecord(void)
	{
		return recTrans;
	}
	
	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}
};


// Simple strcture to hold information about Assortment(s) used to
// transfer m3fub and m3to to.
typedef struct _trans_assort_struct
{
	double fm3fub;
	double fm3to;
	double fm3fub_price;
	double fm3to_price;
	CString sFromAssortName;
	CString sToAssortName;

	_trans_assort_struct(double _m3fub,double _m3to,double _m3fub_price,double _m3to_price,LPCTSTR _from_ass_name,LPCTSTR _to_ass_name)
	{
		fm3fub = _m3fub;
		fm3to = _m3to;
		fm3fub_price = _m3fub_price;
		fm3to_price = _m3to_price;
		sFromAssortName = _from_ass_name;
		sToAssortName = _to_ass_name;
	}
} TRANS_ASSORT_STRUCT;

typedef std::vector<TRANS_ASSORT_STRUCT> vecTRANS_ASSORT_STRUCT;
// CSpecieDataFormView form view

class CSpecieDataFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpecieDataFormView)

	CButton m_wndBtnAddAssort;
	CButton m_wndBtnDelAssort;
	CButton m_wndBtnAddToTrakt;
	CButton m_wndBtnAddTransfer;
	CButton m_wndBtnDelTransfer;
	CButton m_wndBtnDelAllTransfers;
	
	CXTResizeGroupBox m_wndGroup1;
	CMyPropertyGrid m_wndPropertyGrid;

	CMyReportCtrl m_wndReport;
	CMyReportCtrl m_wndReport1;	// "�verf�ringar"

	BOOL m_bInitialized;
	BOOL m_bIsDirty;
	BOOL m_bIsFocus;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	enumACTION m_enumAction;

	CTransaction_trakt *m_pTraktRecord;
	void getTrakt(void);

	vecTransactionSpecies m_vecSpecies;

	vecTransactionTraktData m_vecTraktData;
	CTransaction_trakt_data m_recTraktData;
	void getTraktDataRecord(void);
	void getTraktDataFromDB(void);
	void getTraktDataFromDB_specie(int trakt_id,int spc_id);
	void saveTraktDataToDB(void);

	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(int trakt_id);

	vecTransactionTraktAss m_vecTraktAss;
	CTransaction_trakt_ass m_recTraktAss;
	void getTraktAssFromDB(void);
	void getTraktAssFromDB_exclude(void);
	void saveTraktAssToDB(void);
	void delTraktAssFromDB(void);
	void resetTraktAssInDB(void);

	double getLastKrPerM3(CTransaction_trakt_ass rec);

	CTransaction_trakt_trans m_recTraktAssTrans;
	vecTransactionTraktTrans m_vecTraktAssTrans;
	void getTraktAssTrans(int trakt_id);
	void saveTraktAssTrans(void);
	void delTraktAssTrans(void);

	BOOL setupReport(void);
	BOOL setupReport1(void);

	int m_nSpecieID;

	UINT m_nSelectedTraktDataIdx;

	void setLanguage(void);
	
	BOOL isSpcUsed(int id);

	void populateData(void);

	void setupPropertyGrid(void);

	BOOL getIsDirty(void);

	void exitViewAction(void);

	CAssortmentReportRec *m_pSelFromAssortInReport;
	CAssortmentReportRec *m_pSelToAssortInReport;

	int findFromAssortmentID(LPCTSTR assort_name);
	int findToAssortmentID(LPCTSTR assort_name);


	int m_nPriceSetIn;	// M3To or M3Fub: from Pricelist; 090615 p�d
	// Bind to variables for Property grid; 071025 p�d
	CString m_sBindToSpecie;
	double m_fBindToPercentOf;
	long m_nBindToNumOf;
	double m_fBindToGY;
	double m_fBindToAvgHgt;
	double m_fBindToDA;
	double m_fBindToDG;
	double m_fBindToDGV;
	double m_fBindToHGV;
	double m_fBindToVolM3Sk;
	double m_fBindToVolM3Fub;
	double m_fBindToVolM3Ub;
	double m_fBindToAvgVolM3Sk;
	double m_fBindToAvgVolM3Fub;
	double m_fBindToAvgVolM3Ub;
	double m_fBindToH25;
	double m_fBindToGreenCrown;
	double m_fBindToGrot;

	void recalculateTransfers(void);
	void recalculateNoTransfers(void);

	BOOL checkTableDatesOK(void);


protected:
	CSpecieDataFormView();           // protected constructor used by dynamic creation

public:
	void setAction(enumACTION action)	{		m_enumAction = action;	}
	void setSpecieID(int spc_id)	{		m_nSpecieID = spc_id;	}
	void setSpecieName(LPCTSTR spc_name)	{		m_sBindToSpecie = spc_name;	}

	CString getSpcName(void)	{		return m_sBindToSpecie;	}

	void clearForm(void);

	void setSpecificLanguage(void);

	void isDataChanged(void);

	vecTransactionAssort m_vecAssortmentsPerSpc;	// XML pricelist vector
	void getAssortmentsInPricelist(void);

	void addConstraintsToReport(void);

	virtual ~CSpecieDataFormView();

	enum { IDD = IDD_FORMVIEW8 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);

	afx_msg void OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReport1ValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnGyChange(void);
	afx_msg void OnAvgHgtChange(void);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
};


