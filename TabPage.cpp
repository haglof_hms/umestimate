// TabPage.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "TabPage.h"


// CTabPage dialog

IMPLEMENT_DYNAMIC(CTabPage, CPropertyPage)
CTabPage::CTabPage()
	: CPropertyPage(CTabPage::IDD)
{
}

CTabPage::~CTabPage()
{
}

void CTabPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CTabPage, CPropertyPage)
END_MESSAGE_MAP()


// CTabPage message handlers
