#pragma once

#include "XHTMLStatic.h"

#include "Resource.h"

// CGeneralInfoDlg form view

class CGeneralInfoDlg : public CXTResizeDialog
{
	DECLARE_DYNCREATE(CGeneralInfoDlg)

protected:
//	CXTResizeGroupBox m_wndGrp;

	CFont m_fontData;

	CString m_sLblTraktLnr;	// I.e. Trakt id number
	CString m_sLblTraktNum;
	CString m_sLblTraktPNum;
	CString m_sLblTraktName;
	CString m_sLblTraktAreal;

	CString m_sWithDraw;
	CString m_sToBeLeft;

	CString m_sLblSumTrees;
	CString m_sLblSumM3Sk;
	CString m_sLblSumM3Fub;
	//CString m_sLblSumM3Ub;
	CString m_sLblSumGY;

	CString m_sTraktLnr;
	CString m_sTraktNum;
	CString m_sTraktPNum;
	CString m_sTraktName;
	CString m_sTraktAreal;

	CString m_sSumTrees;
	CString m_sSumM3Sk;
	CString m_sSumM3Fub;
	//CString m_sSumM3Ub;
	CString m_sSumGY;

	CString m_sSumTrees2;
	CString m_sSumM3Sk2;
	CString m_sSumM3Fub2;
	//CString m_sSumM3Ub2;
	CString m_sSumGY2;


	//Lagt till per hektar, #4093 20140707 J�
	CString m_sLblSumM3Sk_ha;
	CString m_sLblSumM3Fub_ha;
	CString m_sSumM3Sk_ha;
	CString m_sSumM3Sk2_ha;
	CString m_sSumM3Fub_ha;
	CString m_sSumM3Fub2_ha;
	double m_fAreal;
	double m_fm3sk;
	double m_fm3fub;
	double m_fm3sk2;
	double m_fm3fub2;

	CXHTMLStatic m_wndHTML;

	void setLanguage(void);
public:

	void setInfo(CTransaction_trakt &v)
	{
		if (v.getTraktID() >= 0)
			m_sTraktLnr.Format(_T("%d"),v.getTraktID());
		else
			m_sTraktLnr = _T("");

		m_sTraktNum = v.getTraktNum();
		m_sTraktPNum = v.getTraktPNum();
		m_sTraktName = v.getTraktName();
		
		if (v.getTraktAreal() > 0.0)
		{
			m_sTraktAreal.Format(_T("%.3f"),v.getTraktAreal());
			m_fAreal=v.getTraktAreal();
			m_sSumM3Sk_ha.Format(_T("%.2f"),m_fm3sk/m_fAreal);
			m_sSumM3Fub_ha.Format(_T("%.2f"),m_fm3fub/m_fAreal);
			m_sSumM3Sk2_ha.Format(_T("%.2f"),m_fm3sk2/m_fAreal);
			m_sSumM3Fub2_ha.Format(_T("%.2f"),m_fm3fub2/m_fAreal);
		}
		else
		{
			m_sTraktAreal = _T("");
			m_fAreal=0.0;
			m_sSumM3Sk_ha= _T("");
			m_sSumM3Fub_ha=_T("");
			m_sSumM3Sk2_ha= _T("");
			m_sSumM3Fub2_ha=_T("");
		}
		
//		Invalidate();
		updateHTMLText();
	}

	void setSumInfo(long numof_trees,double m3sk,double m3fub,double m3ub,double gy)
	{
		m_sSumTrees.Format(_T("%ld"),numof_trees);
		m_sSumM3Sk.Format(_T("%.2f"),m3sk);
		m_sSumM3Fub.Format(_T("%.2f"),m3fub);
		//m_sSumM3Ub.Format(_T("%.2f"),m3ub);
		// Check if GY < 1.0 use 3 digits
		// if GY >= 1.0 use 1 digit
		if (gy >= 1.0)
			m_sSumGY.Format(_T("%.1f"),gy);
		else if (gy < 1.0)
			m_sSumGY.Format(_T("%.1f"),gy);

		m_fm3sk=m3sk;
		m_fm3fub=m3fub;
		if(m_fAreal>0.0)
		{
			
			m_sSumM3Sk_ha.Format(_T("%.2f"),m_fm3sk/m_fAreal);
			m_sSumM3Fub_ha.Format(_T("%.2f"),m_fm3fub/m_fAreal);
		}
		else
		{
			m_sSumM3Sk_ha= _T("");
			m_sSumM3Fub_ha=_T("");
		}

		
//		Invalidate();
		updateHTMLText();
	}
	
	void setSumInfo2(long numof_trees,double m3sk,double m3fub,double m3ub,double gy)
	{
		m_sSumTrees2.Format(_T("%ld"),numof_trees);
		m_sSumM3Sk2.Format(_T("%.2f"),m3sk);
		m_sSumM3Fub2.Format(_T("%.2f"),m3fub);
		//m_sSumM3Ub2.Format(_T("%.2f"),m3ub);
		// Check if GY < 1.0 use 3 digits
		// if GY >= 1.0 use 1 digit
		if (gy >= 1.0)
			m_sSumGY2.Format(_T("%.1f"),gy);
		else if (gy < 1.0)
			m_sSumGY2.Format(_T("%.1f"),gy);

		m_fm3sk2=m3sk;
		m_fm3fub2=m3fub;
		if(m_fAreal>0.0)
		{
			m_sSumM3Sk2_ha.Format(_T("%.2f"),m_fm3sk2/m_fAreal);
			m_sSumM3Fub2_ha.Format(_T("%.2f"),m_fm3fub2/m_fAreal);
		}
		else
		{
			m_sSumM3Sk2_ha= _T("");
			m_sSumM3Fub2_ha=_T("");
		}

//		Invalidate();
		updateHTMLText();
	}

	void updateHTMLText(void)
	{
		m_wndHTML.UpdateWindow();
		Invalidate();
	}

	CGeneralInfoDlg();           // protected constructor used by dynamic creation
	virtual ~CGeneralInfoDlg();

	enum { IDD = IDD_DIALOGBAR };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//}}AFX_VIRTUAL

protected:

	DECLARE_MESSAGE_MAP()
};

