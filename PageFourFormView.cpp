// PageFourFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "StandEntryDoc.h"
#include "PageFourFormView.h"

#include "ResLangFileReader.h"
// CPageFourFormView

IMPLEMENT_DYNCREATE(CPageFourFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPageFourFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONTROL1, OnSelectedChanged)
END_MESSAGE_MAP()

CPageFourFormView::CPageFourFormView()
	: CXTResizeFormView(CPageFourFormView::IDD)
{
	m_bInitialized = FALSE;
}

CPageFourFormView::~CPageFourFormView()
{
}

void CPageFourFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CPageFourFormView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();	
}

void CPageFourFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CPageFourFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

void CPageFourFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (!m_bInitialized)
	{

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				AddView(RUNTIME_CLASS(CHeightCurveFormView),xml.str(IDS_STRING2541),0,IDC_HEIGHT_CURVES);	
				AddView(RUNTIME_CLASS(CDiamDistributionFormView),xml.str(IDS_STRING2548),0,IDC_DIAM_DISTRIBUTION);	
				AddView(RUNTIME_CLASS(CVolumeDistributionFormView),xml.str(IDS_STRING2549)+L"/"+xml.str(IDS_STRING3267),0,IDC_VOL_DISTRIBUTION);	
				AddView(RUNTIME_CLASS(CAssortDistributionFormView),xml.str(IDS_STRING2554),0,IDC_ASSORT_DISTRIBUTION);	
#ifdef SHOW_THINNING
				AddView(RUNTIME_CLASS(CThinningDiagramFormView),L"Gallringsmall",0,IDC_THINNING_DIAGRAM);	
#endif
			}
			xml.clean();
		}

		m_bInitialized = TRUE;
	}

}

void CPageFourFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(10, 10, cx - 20, cy - 20);
	}

}

void CPageFourFormView::OnSetFocus(CWnd*)
{
	// When this view gets focus, enbale the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(TRUE,TRUE,TRUE);
	}
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		int nIndex = pItem->GetIndex();
		switch(nIndex)
		{
			case 1 : getDiamDistributionFormView()->doDrawChartView(false,false); break;
			case 2 : getVolumeDistributionFormView()->doDrawChartView(false,false); break;
			case 3 : getAssortDistributionFormView()->doDrawChartView(false,false); break;
#ifdef SHOW_THINNING
			case 4 : getThinningDiagramFormView()->doDrawChartView(false,false); break;
#endif
		};
	}	// if (pItem != NULL)
}


// CPageFourFormView diagnostics

#ifdef _DEBUG
void CPageFourFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPageFourFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPageFourFormView message handlers

void CPageFourFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;

	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		int nIndex = pItem->GetIndex();
		switch(nIndex)
		{
			case 1 : getDiamDistributionFormView()->doDrawChartView(true,false); break;
			case 2 : getVolumeDistributionFormView()->doDrawChartView(true,false); break;
			case 3 : getAssortDistributionFormView()->doDrawChartView(true,false); break;
#ifdef SHOW_THINNING
			case 4 : getThinningDiagramFormView()->doDrawChartView(true,false); break;
#endif
		};
	}	// if (pItem != NULL)

}

void CPageFourFormView::setDiagramOnSelectedTab(void)
{
//	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
//	if (pItem != NULL)
//	{
		//int nIndex = pItem->GetIndex();
		//if (nIndex != 0) m_wndTabControl.SetCurSel(0);
		getHgtCurveFormView()->doDrawChartView(true,false);
//	}
}

void CPageFourFormView::doPreview(void)
{
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		int nIndex = m_tabManager->GetIndex();
		switch (nIndex)
		{
			case 0 : getHgtCurveFormView()->doRunPrintPreview(); break;
			case 1 : getDiamDistributionFormView()->doRunPrintPreview(); break;
			case 2 : getVolumeDistributionFormView()->doRunPrintPreview(); break;
			case 3 : getAssortDistributionFormView()->doRunPrintPreview(); break;
#ifdef SHOW_THINNING
			case 4 : getThinningDiagramFormView()->doRunPrintPreview(); break;
#endif
		};
	}
}

void CPageFourFormView::doPrint(void)
{
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		int nIndex = m_tabManager->GetIndex();
		switch (nIndex)
		{
			case 0 : getHgtCurveFormView()->doRunPrint(); break;
			case 1 : getDiamDistributionFormView()->doRunPrint(); break;
			case 2 : getVolumeDistributionFormView()->doRunPrint(); break;
			case 3 : getAssortDistributionFormView()->doRunPrint(); break;
#ifdef SHOW_THINNING
			case 4 : getThinningDiagramFormView()->doRunPrint(); break;
#endif
		};
	}
}

BOOL CPageFourFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
//		pWnd = (CHeightCurveFormView*)pViewClass->CreateObject();
		pWnd = (CXTResizeFormView*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, id, &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	int nTab = m_wndTabControl.getNumOfTabPages();
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

//	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}
