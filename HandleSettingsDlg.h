#pragma once

#include "Resource.h"

// CHandleSettingsDlg dialog

class CHandleSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CHandleSettingsDlg)


	// Bind to variables for Property grid; 071025 p�d
	int m_nSpecieID;
	CString m_sSpecieName;
	int m_nActiveTraktID;

	// Bind to ...
	CString m_sHgtTypeOfBindStr;	// Which height function type's selcted (e.g. H25,Regression etc)
	CString m_sHgtBindStr;				// Selected function within the TypeOf (e.g. H25 => �vriga tr�dslag)

	CString m_sVolTypeOfBindStr;	// Which volume function type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolBindStr;				// Selected function within the TypeOf (e.g. Brandels => Mindre,Norra)

	CString m_sBarkTypeOfBindStr;	// Which bark function type's selcted (e.g. Jonsson/Ostlin,Skogforsk etc)
	CString m_sBarkBindStr;				// Selected function within the TypeOf (e.g. Jonsson/Ostlin => Tall,Serie 1)

	CString m_sVolUBTypeOfBindStr;	// Which volume function type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolUBBindStr;				// Selected function within the TypeOf (e.g. Brandels => Mindre,Norra)

	CString m_sQDescBindStr;
	CStringArray m_listQDesc;

	CString m_sGrotFuncBindStr;		// Wich GROTfunction selected
	double m_fGrotPercent;				// Uttagsprocent
	double m_fGrotPrice;					// Pris (kr/ton)
	double m_fGrotCost;						// Kostnad (kr/ton)

	double m_fM3SkToM3Ub;
	double m_fM3SkToM3Fub;
	long m_nGreenCrown;
	double m_fM3FubToM3To;
	long m_nDefH25;	// Standardv�rde f�r H25
	double m_fDiamClass;
	long m_nTranspDist1;
	long m_nTranspDist2;

	BOOL m_bIsAtCoast;
	BOOL m_bIsSouthEast;
	BOOL m_bIsRegion5;
	BOOL m_bIsPartOfPlot;
	CString m_sSIH100_Pine;

	CString m_sCapMsg;
	CStringArray m_sarrValidateMsg;

	CString m_sArrNoHgtFunc;
	CString m_sArrNoVolFunc;
	CString m_sArrNoBarkFunc;
	CString m_sArrNoVolUbFunc;
	CString m_sMsgNoSubFuncChosen;

	CString m_sFunctions;
	CString m_sHeight;
	CString m_sCalcHgtAs;
	CString m_sVolume;
	CString m_sCalcVolAs;
	CString m_sBark;
	CString m_sBarkSerie;
	CString m_sVolumeUB;
	CString m_sCalcVolAsUB;
	CString m_sMiscSettings;
	CString m_sSk_Ub;
	CString m_sSk_Fub;
	CString m_sFub_To;
	CString m_sDefH25;
	CString m_sGreenCrown;
	CString m_sTransport;
	CString m_sTranspDist1;	// "Avst�nd till v�g (m)"
	CString m_sTranspDist2;	// "Avst�nd till Indistri (km)"
	CString m_sQualDesc;
	CString m_sCalcQDescAs;
	CString m_sGrotFunctions;
	CString m_sCalcGrotAs;
	CString m_sGrotPercent;
	CString m_sGrotPrice;
	CString m_sGrotCost;
	CString m_sAtCoast;
	CString m_sSouthEast;
	CString m_sRegion5;
	CString m_sPartOfPlot;

	BOOL m_bHgtFuncChanged;
	BOOL m_bVolFuncChanged;
	BOOL m_bBarkFuncChanged;
	BOOL m_bVolUBFuncChanged;
	BOOL m_bQDescFuncChanged;
	BOOL m_bGrotFuncChanged;

	CString m_sLangFN;

	CMyPropertyGrid m_wndPropertyGrid;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	CString m_sMsgQDescMissing;

protected:

	void getIndexForFunctionSet(CXTPPropertyGridItem *pItem,UINT* idx);

	void setupFunctionData(BOOL setup_as_new);
	
	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(void);

	BOOL getActiveTypeOfFunction(int,CString &,UINT*,int*);
	CString getActiveFunction(int);

	vecUCFunctions m_vecHgtFunc;
	vecUCFunctionList m_vecHgtFuncList;
	void setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange);

	vecUCFunctions m_vecVolFunc;
	vecUCFunctionList m_vecVolFuncList;
	void setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange);

	vecUCFunctions m_vecBarkFunc;
	vecUCFunctionList m_vecBarkFuncList;
	void setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange);

	vecUCFunctions m_vecVolUBFunc;
	vecUCFunctionList m_vecVolUBFuncList;
	void setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bonChange);
	
	vecUCFunctions m_vecGROTFunc;
	void setupGrotFunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupGrotPercentInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);

	// Data in esti_trakt_set_spc_table, settings for specie in Trakt; 070504 p�d
	vecTransactionTraktSetSpc m_vecTransactionTraktSetSpc;
	void getTraktSetSpc(void);

	CString getSetSpcDate(void);
	// Qualitydexriptions
	int getQualDescIndex(CXTPPropertyGridItem *pItem,CString& qdesc_name);
	void getQualDescData(UINT *qdesc_index,CStringArray& qdesc_list);
	void setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	// "Omf�ringstal etc"
	BOOL getMiscData(int do_as);
	void setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupMiscSettings2ValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);

	UINT m_nIndexHgtFunc;
	UINT m_nIndexVolFunc;
	UINT m_nIndexBarkFunc;
	UINT m_nIndexVolUBFunc;
	UINT m_nIndexGrotFunc;
	BOOL m_bSetAsFunction;
	UINT m_nIndexQDesc;
	void saveSetSpcHgtFuncToDB();
	void saveSetSpcVolFuncToDB();
	void saveSetSpcBarkFuncToDB();
	void saveSetSpcVolUBFuncToDB();
	void saveSetSpcVolUBFuncToDB_sk_ub();
	BOOL saveSetSpcQDescToDB();
	void saveSetSpcMiscDataToDB();
	void saveSetSpcGrotFuncToDB();

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
public:
	CHandleSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	CHandleSettingsDlg(CString spc_name,int spc_id,int trakt_id,CUMEstimateDB* db = NULL);
	virtual ~CHandleSettingsDlg();

	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG8 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHandleSettingsDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
