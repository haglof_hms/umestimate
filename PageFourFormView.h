#pragma once

#include "Resource.h"

#include "HeightCurveFormView.h"
#include "DiamDistributionFormView.h"
#include "VolumeDistributionFormView.h"
#include "AssortDistributionFormView.h"
#include "ThinningDiagramFormView.h"

// CPageFourFormView form view

class CPageFourFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPageFourFormView)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

protected:
	CPageFourFormView();           // protected constructor used by dynamic creation
	virtual ~CPageFourFormView();

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id);
public:
	enum { IDD = IDD_FORMVIEW12 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	CHeightCurveFormView* getHgtCurveFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CHeightCurveFormView* pView = DYNAMIC_DOWNCAST(CHeightCurveFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CHeightCurveFormView, pView);
			return pView;
		}
		return NULL;
	}

	CDiamDistributionFormView* getDiamDistributionFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CDiamDistributionFormView* pView = DYNAMIC_DOWNCAST(CDiamDistributionFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDiamDistributionFormView, pView);
			return pView;
		}
		return NULL;
	}

	CVolumeDistributionFormView* getVolumeDistributionFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CVolumeDistributionFormView* pView = DYNAMIC_DOWNCAST(CVolumeDistributionFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CVolumeDistributionFormView, pView);
			return pView;
		}
		return NULL;
	}

	CAssortDistributionFormView* getAssortDistributionFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CAssortDistributionFormView* pView = DYNAMIC_DOWNCAST(CAssortDistributionFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CVolumeDistributionFormView, pView);
			return pView;
		}
		return NULL;
	}


	CThinningDiagramFormView* getThinningDiagramFormView(void)
	{
#ifdef SHOW_THINNING
		m_tabManager = m_wndTabControl.getTabPage(4);
		if (m_tabManager)
		{
			CThinningDiagramFormView* pView = DYNAMIC_DOWNCAST(CThinningDiagramFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CThinningDiagramFormView, pView);
			return pView;
		}
#endif
		return NULL;
	}

	void doPreview(void);
	void doPrint(void);

	void setDiagramOnSelectedTab(void);

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageThreeFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageThreeFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


