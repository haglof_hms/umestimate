#pragma once

#include "Resource.h"

#include "SampleTreeReportView.h"
#include "DCLSTreeReportView.h"
#include "DCLS1TreeReportView.h"
#include "DCLS2TreeReportView.h"

// CPageThreeFormView form view

class CPageThreeFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPageThreeFormView)

	BOOL m_bInitialized;

	BOOL m_bDoWeNeedToAddPlots;

	BOOL m_bIsDirty;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sFieldChooser;

	CString m_sChangeTreeType1;
	CString m_sMsg;
	CString m_sMsgGroupID;
	CString m_sRemoveTreesMsg1;
	CString m_sRemoveTreesMsg2;
	CString m_sRemoveTreesMsg3;
	CString m_sRemoveTreesMsg1_1;
	CString m_sRemoveTreesMsg2_1;
	CString m_sRemoveTreesMsg3_1;
	CString m_sRemoveTreesMsg4;
	CString m_sChangeCruseMethod;
	CString m_sLogErrorMsg;
	CString m_sLogErrorMsg2;
	CString m_sLogCap;
	CString m_sCancel;
	CString m_sPrintOut;
	CString m_sSaveToFile;
	CString m_sRecalculate;
	CString m_sNeedToRecalculate;
	CString m_sArealMissing;
	CString m_sDataForGrotMissing;
	CString m_sAgeForBarkSoderbergUnreasonable;
	CString m_sAgeForHgtSoderbergUnreasonable;
	CString m_sAgeForSoderbergUnreasonable;
	CString m_sHeightForH25Unreasonable;
	CString m_sHeightForH25Unreasonable2;
	CString m_sTreeCheckMsg1;
	CString m_sTreeCheckMsg2;
	CString m_sTreeCheckMsg3;
	CString m_sTreeCheckMsg4;
	CString m_sTreeCheckMsg5;
	CString m_sTreeCheckMsg6;
	CString m_sTreeCheckMsg7;
	CString m_sTreeCheckMsg8;
	CString m_sTreeCheckMsg9;
	CString m_sTreeCheckMsg10;

	CString m_sDCLSCap;
	CString m_sDCLS1Cap;
	CString m_sDCLS2Cap;
	CString m_sSampleCap;

	CString m_sOKBtn;

	CString m_sSpcWithoutFunctionsMsg;
	CString m_sNoFunctionsMsg;
	CString m_sNoFunctionsForSpcMsg;

	CString m_sRemoveRandTreesFirstMsg;

	CStringArray m_sarrCalcStatusMsg;

	CXTResizeGroupBox m_wndGroup1;

	CString m_sDiagramXLabel;
	CString m_sDiagramYLabel;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyComboBox m_wndCBox1;
	CMyExtEdit m_wndEdit1;
	CXTButton m_wndBtnOpenPlotList;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	// For "Arealuppr�kning", if we have "Cirkelytor".
	// This value is calculated in method getPlotsFromDB(); 070614 p�d
	double m_fArealCalculationFactor;		

	double m_fPerHa;

	CImageList m_ilIcons;

	CStringArray m_sarrTreeTypes;
	CStringArray m_sarrInventoryMethods;

	char tmp[255];
	char *convStr(LPCTSTR s)	
	{
		sprintf(tmp,"%S",s);

		return tmp;
	}

protected:
	CPageThreeFormView();           // protected constructor used by dynamic creation
	virtual ~CPageThreeFormView();

	void setupInventoryMethodsInCBox(void);

	void setupBasedOnInventoryMethod(void);

	vecTransactionAssort m_vecAssortmentsPerSpc;	// XML pricelist vector
	void getTraktMiscDataFromDB(int trakt_id);

	vecTransactionPlot m_vecTraktPlot;
	void getPlotsFromDB(void);

	vecTransactionSpecies m_vecSpecies;
	void getSpeciesInPricelist(void);

	vecTransactionSampleTreeCategory m_vecSmpTreeCategories;
	void GetSampleTreeCategories(void);

	vecTransactionDCLSTree m_vecDCLSTrees;
	vecTransactionDCLSTree m_vecDCLS;
	vecTransactionTraktData m_vecTraktData;
	void getTraktDataFromDB(int trakt_id);

	void getSampleTreesFromDB(int tree_type = -1);
	void getSampleTreesFromReport(void);
	BOOL saveSampleTreesToDB(void);
	int delSampleTreeInDB(void);

	vecTransactionSampleTree m_vecTraktSampleTrees;
	void getSampleTreesFromDB(int trakt_id,int tree_type = -1,BOOL clear = FALSE);

	vecTransactionSampleTree m_vecTraktNonSampleTrees;
	void getNonSampleTreesFromDB(int tree_type = -1);
	vecTransactionSampleTree m_vecTraktLeftOverSampleTrees;
	void getLeftOverSampleTreesFromDB(int tree_type = -1);
	vecTransactionSampleTree m_vecTraktExtraSampleTrees;
	void getExtraSampleTreesFromDB(int tree_type = -1);

	vecTransactionTraktSetSpc m_vecTransactionTraktSetSpc;
	void getTraktSetSpcFromDB(int trakt_id);

	vecTransactionDCLSTree m_vecTraktDCLSTrees;
	void getDCLSTreesFromDB(void);
	void getDCLSTreesFromReport(void);
	BOOL saveDCLSTreesToDB(void);
	int delDCLSTreeInDB(void);

	vecTransactionDCLSTree m_vecTraktDCLS1Trees;
	void getDCLS1TreesFromDB(void);
	void getDCLS1TreesFromReport(void);
	BOOL saveDCLS1TreesToDB(void);
	int delDCLS1TreeInDB(void);

	vecTransactionDCLSTree m_vecTraktDCLS2Trees;
	void getDCLS2TreesFromDB(void);
	void getDCLS2TreesFromReport(void);
	BOOL saveDCLS2TreesToDB(void);
	int delDCLS2TreeInDB(void);

	void delPlots(void);

	void addPlotType(int id,int trakt_id,int plot_type);
	void updPlotType(int trakt_id,int plot_type);

	void removeTreesAndPlotsInTrakt(void);

	// Method for calculating sum price per Assortments per specie from
	// from: esti_trees_assort_table
	// to: esti_trakt_spc_assort_table
	vecTransactionTraktAss m_vecTransactionTraktAss;			// esti_trees_assort_table
	vecTransactionTreeAssort m_vecTransactionTreeAssort;	// esti_trakt_trees_assort_table
	void calculateAssortFromTreesToSpc(void);

	CTransaction_trakt_misc_data m_recTraktMiscData;
	
	
	void clearAll(int idx = -1);

	BOOL getIsDirty(void);
	void resetIsDirty(void);

	int getIndexForTreeType(LPCTSTR value);
	int getIndexForCategory(LPCTSTR value);

	void addConstraintsToDCLSReport(void);
	void addConstraintsToDCLS1Report(void);
	void addConstraintsToDCLS2Report(void);
	void addConstraintsToSampleReport(void);

	BOOL isSpcAdded(int,int,vecFuncSpc &);
	BOOL checkFunctionsForTrees(vecTransactionTraktSetSpc &);

	BOOL setupReport(void);

	void getDiamClass(double dbh,double dcls,double *dcls_from,double *dcls_to);

	int getNumOfRandTreesInDCLS_forSpc(int spc_id,double dcls_from,double dcls_to);


	BOOL checkGROTInData(void);

	// Selected record item. Used in method setReportRecordGroupID()
	// To use this function, the user must select a row in report; 070516 p�d
	// To reset the method, set this data member to NULL; 070516 p�d
	CXTPReportRecordItem *m_pSelectedDCLSRecordItem;
	CXTPReportRecordItem *m_pSelectedDCLS1RecordItem;
	CXTPReportRecordItem *m_pSelectedDCLS2RecordItem;

	CXTPReportRecordItem *m_pSelectedSampleRecordItem;

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id);

public:

	// Need to be public; 070308 p�d
	void populateData(int idx = -1);

	int doCalculations(int spc_id,BOOL remove_trakt_trans,BOOL check_changes,BOOL show_msg,bool do_transfers,bool do_populate);

	void doRunPrintPreview(void);
	void doRunPrint(void);

	int getSelectedInvMethodIndex(void);
	int getGroupID(void);
	void setGroupID(int id);
	void setReportRecordGroupID(int id);
	void setResetReportRecordSelected(void);
	CString getGroupName(int grp_id);
	void setGroupName(LPCTSTR name);
	BOOL isPlotGroupID(int plot_id);

	//#4380 20150602 J�
	//BOOL Show_Change_Type_Message(int nFromType,int nToType,CTransaction_sample_tree data);
	BOOL doTreeTypeHasChanged(CTransaction_sample_tree data,int nFromType,int nToType);
	BOOL doSpecieHasChanged(CTransaction_sample_tree data,int nToSpec,CString cSpcName);
	BOOL doDbhHasChanged(CTransaction_sample_tree data,double dFromDia,double dToDia);
	BOOL doDeleteTree(CTransaction_sample_tree data);
	BOOL doAddTree(CTransaction_sample_tree data);

	BOOL Add_To_Dcls_Remain(CTransaction_sample_tree data);
	BOOL Add_To_Dcls_Uttag(CTransaction_sample_tree data);
	BOOL Add_To_Dcls_Uttag_NumOfKantTrad(CTransaction_sample_tree data);
	BOOL Remove_From_Dcls_Uttag(CTransaction_sample_tree data);
	BOOL Remove_From_Dcls_Uttag_NumOfKanttrad(CTransaction_sample_tree data);
	BOOL Remove_From_Dcls_Remain(CTransaction_sample_tree data);

	//BOOL Add_To_Dcls_Remain_NumOfKantTrad(CTransaction_sample_tree data);

	BOOL Check_If_Pos(CTransaction_sample_tree data);

	void doMatchRandTrees(void);
	void doUpdateRandTrees(void); // 080624 p�d
	void doUpdateRandTrees2(void);

	void createHeightCurveForDB(int trakt_id = -1);

	CXTPReportControl* getDCLSReport(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CDCLSTreeReportView* pView = DYNAMIC_DOWNCAST(CDCLSTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLSTreeReportView, pView);
			return &pView->GetReportCtrl();
		}
		return NULL;
	}

	CXTPReportControl* getDCLS1Report(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CDCLS1TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS1TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS1TreeReportView, pView);
			return &pView->GetReportCtrl();
		}
		return NULL;
	}

	CXTPReportControl* getDCLS2Report(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CDCLS2TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS2TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS2TreeReportView, pView);
			return &pView->GetReportCtrl();
		}
		return NULL;
	}


	CDCLSTreeReportView* getDCLSTreeReportView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CDCLSTreeReportView* pView = DYNAMIC_DOWNCAST(CDCLSTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLSTreeReportView, pView);
			return pView;
		}
		return NULL;
	}

	CDCLS1TreeReportView* getDCLS1TreeReportView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CDCLS1TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS1TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS1TreeReportView, pView);
			return pView;
		}
		return NULL;
	}

	CDCLS2TreeReportView* getDCLS2TreeReportView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CDCLS2TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS2TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS2TreeReportView, pView);
			return pView;
		}
		return NULL;
	}


	BOOL getIsDCLSReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CDCLSTreeReportView* pView = DYNAMIC_DOWNCAST(CDCLSTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLSTreeReportView, pView);
			return pView->isDirty();
		}
		return FALSE;
	}

	BOOL getIsDCLS1ReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CDCLS1TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS1TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS1TreeReportView, pView);
			return pView->isDirty();
		}
		return FALSE;
	}

	BOOL getIsDCLS2ReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CDCLS2TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS2TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS2TreeReportView, pView);
			return pView->isDirty();
		}
		return FALSE;
	}

	void resetIsDCLSReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CDCLSTreeReportView* pView = DYNAMIC_DOWNCAST(CDCLSTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLSTreeReportView, pView);
			pView->resetIsDirty();
		}
	}

	void resetIsDCLS1ReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CDCLS1TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS1TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS1TreeReportView, pView);
			pView->resetIsDirty();
		}
	}

	void resetIsDCLS2ReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CDCLS2TreeReportView* pView = DYNAMIC_DOWNCAST(CDCLS2TreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CDCLS2TreeReportView, pView);
			pView->resetIsDirty();
		}
	}

	CXTPReportControl* getSampleReport(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CSampleTreeReportView* pView = DYNAMIC_DOWNCAST(CSampleTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CSampleTreeReportView, pView);
			return &pView->GetReportCtrl();
		}
		return NULL;
	}

	CSampleTreeReportView* getSampleTreeReportView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CSampleTreeReportView* pView = DYNAMIC_DOWNCAST(CSampleTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CSampleTreeReportView, pView);
			return pView;
		}
		return NULL;
	}

	BOOL getIsSampleReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CSampleTreeReportView* pView = DYNAMIC_DOWNCAST(CSampleTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CSampleTreeReportView, pView);
			return TRUE; //pView->isDirty();
		}
		return FALSE;
	}

	void resetIsSampleReportDirty(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CSampleTreeReportView* pView = DYNAMIC_DOWNCAST(CSampleTreeReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CSampleTreeReportView, pView);
			pView->resetIsDirty();
		}
	}

	int getTabPageIndex(void)
	{
		m_tabManager = m_wndTabControl.getSelectedTabPage();
		if (m_tabManager)
		{
			return m_tabManager->GetIndex();
		}
		return -1;
	}


	void getTraktMiscData_pageThree(CTransaction_trakt_misc_data &rec)	{ rec = m_recTraktMiscData; }

	enum { IDD = IDD_FORMVIEW9 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Need to be Public
	void addNewTree(void);

	BOOL isHasDataChangedPageThree(void);

	void setFocusOnReport(void);

	void setIsDirtyPageThree(void);
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageThreeFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageThreeFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnCBoxInventoryMethodChange(void);
	//}}AFX_MSG
public:
	afx_msg void OnBnClickedBtnPlotView();

	DECLARE_MESSAGE_MAP()
};


