#pragma once

#include "OXMaskedEdit.h"    // COXMaskedEdit

#include "Resource.h"
#include "afxwin.h"

#define COLUMN_CHECK 0

// CAddDclsDlg dialog

class CAddDclsDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddDclsDlg)

	CString m_sLangFN;
	
	CString m_sAddDclsFailed;
	CString m_sDCLSErrorMsg5;
	CString m_sDCLSErrorMsg6;
	CString m_sErrNoSpecieSet;
	CString m_sErrNoDclsSet;
	CString m_sErrNoNumberSet;

	CStringArray m_sarrInventoryMethods;

	int m_yta;
	double m_from_d_class;
	CString m_tr�dslag;

	CMyReportCtrl m_wndReport;

	vecTransactionSpecies m_vecSpecies;
	vecTransactionPlot m_vecTraktPlot;

	CUMEstimateDB *m_pDB;

	int m_nTraktID;
	int m_nPage;

public:
	CAddDclsDlg(vecTransactionSpecies &vecSpecies, vecTransactionPlot &vecTraktPlot, CUMEstimateDB *pDB, int nTraktID, int nPage, CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddDclsDlg();


	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG11 };

protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CIntItemInplaceBtn : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
		CString m_sLangFN;
		HWND hWndParent;
	public:
		CIntItemInplaceBtn(int nValue,LPCTSTR lang_fn,HWND parent) : CXTPReportRecordItemNumber()
		{
			m_nValue = nValue;
			SetValue(m_nValue);
			m_sLangFN = lang_fn;
			hWndParent = parent;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	protected:
		virtual void OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
		{
			int nInvMethod = getInvMethodIndex();
			if (nInvMethod >= 1)
			{
				::UpdateWindow(hWndParent);
				showFormView(IDD_REPORTVIEW4,m_sLangFN,0);
				// On user selects a Group set PageThree dirty; 070516 p�d
				setIsDirtyPageThree();
			}
		}
	};

	class CExTextH100Item : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		int m_nConstraintIndex;
	public:
		CExTextH100Item(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_sText = szText;
			if (m_sText.FindOneOf(_T("TGB")) > -1 || m_sText.IsEmpty())
			{
				m_sText.Delete(3,m_sText.GetLength());
				SetValue(m_sText);
			}
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}

		void setIsBold(void)		{ SetBold();	}

		void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
		void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	};



	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	void myOnSize();
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()

protected:
	CButton m_wndButton1;
	CButton m_wndButton2;
	CButton m_wndButtonCancel;
	CButton m_wndButtonOk;
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBnClickedButton2();
};
