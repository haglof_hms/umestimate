// MDITabbedView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "MDITabbedView.h"
#include "PageOneFormView.h"
#include "PageTwoFormView.h"
#include "PageThreeFormView.h"
#include "PageFourFormView.h"	// Added 100325 p�d, hold "H�jdkurvor"
#include "ResLangFileReader.h"
#include "Resource.h"

// CMDITabbedView

IMPLEMENT_DYNCREATE(CMDITabbedView, CView)

BEGIN_MESSAGE_MAP(CMDITabbedView, CView)
	//{{AFX_MSG_MAP(CMDITabbedView)
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONTROL, OnSelectedChanged)
	ON_WM_MOUSEACTIVATE()
END_MESSAGE_MAP()

CMDITabbedView::CMDITabbedView()
{
}

CMDITabbedView::~CMDITabbedView()
{
}

int CMDITabbedView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;


	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;

		if (xml->Load(m_sLangFN))
		{
			AddView(RUNTIME_CLASS(CPageOneFormView), xml->str(IDS_STRING114), 3);
			AddView(RUNTIME_CLASS(CPageTwoFormView), xml->str(IDS_STRING115), 3);
			AddView(RUNTIME_CLASS(CPageThreeFormView), xml->str(IDS_STRING254), 3);
			AddView(RUNTIME_CLASS(CPageFourFormView), xml->str(IDS_STRING2540), 0);
		}

		delete xml;
	}

	m_bInitialized = FALSE;
	return 0;
}

BOOL CMDITabbedView::OnEraseBkgnd(CDC*)
{
	return TRUE;
}

void CMDITabbedView::OnPaint()
{
	CPaintDC dc(this);
}

void CMDITabbedView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 0, cx, cy);
	}
}

BOOL CMDITabbedView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;

}

// CMDITabbedView drawing

void CMDITabbedView::OnDraw(CDC* pDC)
{
	
}

void CMDITabbedView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
		UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		UpdateDocTitle();

		CFrameWnd* pFrame = GetParentFrame();
		CView* pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);
		pFrame->SetActiveView(pView);

		int nIndex = pItem->GetIndex();
		if (nIndex > -1)
		{
			long nNumOfStands = 0;
			CPageOneFormView *pPageOneFormView = getPageOneFormView();
			if (pPageOneFormView != NULL)
				nNumOfStands = pPageOneFormView->getNumOfStands();
			int nIndex = pItem->GetIndex();
			if (nIndex > -1)
			{
				if (nIndex == 0)
				{
					if (global_SHOW_ONLY_ONE_STAND)
					{
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);	
					}
					else
					{
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,(nNumOfStands > 0));
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(nNumOfStands > 0));	
					}
				}
				else
				{
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);	
				}
			}

			// Set Toolbar buttons on Shell, depending on selected Tab; 080411 p�d
			//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,nIndex == 0);

			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			// On tabview, do a check if there's any changes made to any of the Tab pages
			// before goin' any further; 070618 p�d
			if (pTabView)
			{
/*	Uncommant this to add check of changed data for
		Page one, on changing tab
				if (nIndex == 0)
				{
					CPageOneFormView *pView1 = pTabView->getPageOneFormView();
					if (pView1 != NULL)
					{
						pView1->isHasDataChanged();
					}
				}	// if (nIndex == 0)
*/
/*	Uncommant this to add check of changed data for
		Page two, on changing tab
				if (nIndex == 1)
				{
					CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
					if (pView2 != NULL)
					{
						pView2->isHasDataChanged();
					}
				}	// if (nIndex == 1)
*/
				if (nIndex == 2)
				{
					CPageThreeFormView *pView3 = pTabView->getPageThreeFormView();
					if (pView3 != NULL)
					{
						// Set focus on report, to avoid accidential
						// change on inventory method; 071001 p�d
						pView3->setFocusOnReport();
					}
				}

				// Added 100326 p�d
				if (nIndex == 3)
				{
					CPageFourFormView *pView4 = NULL;
					if ((pView4 = pTabView->getPageFourFormView()) != NULL)
					{
						pView4->setDiagramOnSelectedTab(); //getHgtCurveFormView()->doDrawChartView(true,false);
						//pView4->getDiamDistributionFormView()->doDrawChartView();
					}
				}

			}	// if (pTabView)
		}	// if (nIndex > -1)
	}	// if (pItem != NULL)
}

int CMDITabbedView::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

// CMDITabbedView diagnostics

#ifdef _DEBUG
void CMDITabbedView::AssertValid() const
{
	CView::AssertValid();
}

void CMDITabbedView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMDILicenseFrameDoc* CTabbedViewView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDILicenseFrameDoc)));
	return (CMDILicenseFrameDoc*)m_pDocument;
}

#endif //_DEBUG


// CMDITabbedView message handlers

BOOL CMDITabbedView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	//pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}


void CMDITabbedView::UpdateDocTitle()
{
	GetDocument()->UpdateFrameCounts();
}
