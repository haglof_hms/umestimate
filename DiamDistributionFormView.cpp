// DiamDistributionFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "PageFourFormView.h"
#include "DiamDistributionFormView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

// CDiamDistributionFormView

int CDiamDistributionFormView::m_nCurCBSel = -1;
double CDiamDistributionFormView::m_fMaxDCLS = 0.0;

IMPLEMENT_DYNCREATE(CDiamDistributionFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDiamDistributionFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELCHANGE(IDC_COMBO13_1, OnCBoxSpeciesChanged)
END_MESSAGE_MAP()

CDiamDistributionFormView::CDiamDistributionFormView()
	: CXTResizeFormView(CDiamDistributionFormView::IDD)
{
	m_bInitialized = FALSE;
	m_nTraktID = -1;
	m_nShowSpecies = -1;
	m_nMax = 0;
	m_fMaxDCLS = 0.0;
}

CDiamDistributionFormView::~CDiamDistributionFormView()
{
}

void CDiamDistributionFormView::OnDestroy()
{
	m_fntCaption.DeleteObject();
	m_fntText.DeleteObject();
	m_fntTextBold.DeleteObject();
	m_fntTextSmall.DeleteObject();

	CXTResizeFormView::OnDestroy();	
}

void CDiamDistributionFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CDiamDistributionFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

BOOL CDiamDistributionFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CDiamDistributionFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHelloworldDlg)
	DDX_Control(pDX, IDC_CHART13_1, m_chartViewer);
	DDX_Control(pDX, IDC_LBL13_1, m_wndLbl1);
	DDX_Control(pDX, IDC_COMBO13_1, m_wndCBox1);
	//}}AFX_DATA_MAP

}

void CDiamDistributionFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sDiagramCaption = xml.str(IDS_STRING2548);
				m_sDiagramYLabel = xml.str(IDS_STRING1350);
				m_sDiagramXLabel = xml.str(IDS_STRING2543);			
				m_sStandNumber = xml.str(IDS_STRING1000);
				m_sStandName = xml.str(IDS_STRING101);
				m_sPropertyNameLabel = xml.str(IDS_STRING104);
				m_sShowAllCB = xml.str(IDS_STRING2545);
				m_wndLbl1.SetWindowText(xml.str(IDS_STRING2544));
			}
			xml.clean();

			m_fntCaption.CreateFont(190, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));

			m_fntText.CreateFont(90, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextBold.CreateFont(90, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextSmall.CreateFont(70, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
		
	
		}

		m_bInitialized = TRUE;
	}

}

void CDiamDistributionFormView::OnSetFocus(CWnd*)
{
}

void CDiamDistributionFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CDiamDistributionFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_chartViewer)
	{
		m_chartViewer.MoveWindow(10,40,cx-20,cy);
		drawChartView();
	}
}

// CDiamDistributionFormView diagnostics

void CDiamDistributionFormView::getTraktDCLSFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getDCLSTrees(trakt_id,m_vecDCLSTrees);
	}	// if (pDB != NULL)
}

void CDiamDistributionFormView::getTraktDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

void CDiamDistributionFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

#ifdef _DEBUG
void CDiamDistributionFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDiamDistributionFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDiamDistributionFormView message handlers

void CDiamDistributionFormView::drawChartView(bool redraw,drawType dt,CDC* pDC)
{
	CString S;
	double fDCLSCounter;
	double fDCLSClass;
	CRect rect;
	GetClientRect(&rect);

	// Create a XYChart
	XYChart *xyChart = new XYChart(rect.right-20, rect.bottom-50);

	if (m_nTraktID != getActiveTrakt()->getTraktID() || redraw)
	{
		getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
		getTraktDataFromDB(getActiveTrakt()->getTraktID());
		getTraktDCLSFromDB(getActiveTrakt()->getTraktID());
		m_vecDCLS.clear();
		//---------------------------------------------------------------------------------
		// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = fDCLSClass;
		if (fDCLSClass > 0)
			m_fMaxDCLS = m_pDB->getMaxDCLS(getActiveTrakt()->getTraktID(),m_nShowSpecies)/fDCLSClass;
		else
			m_fMaxDCLS = 0.0;
		//m_fMaxDCLS = 50; Fast diameterklassindelning
		if (m_vecTraktData.size() > 0)
		{

			m_wndCBox1.ResetContent();

			m_wndCBox1.AddString(m_sShowAllCB);	// "All species"

			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					m_wndCBox1.AddString(recTraktData.getSpecieName());
				}	
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			if (m_nCurCBSel == -1)
				m_wndCBox1.SetCurSel(0);
			else
				m_wndCBox1.SetCurSel(m_nCurCBSel);

			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				fDCLSCounter = fDCLSClass;
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					// Setup the Diameterclass vector adding ALL diameterclasses/species; 100326 p�d
					for (UINT i2 = 0;i2 < (int)m_fMaxDCLS;i2++)
					{
						if ((m_nShowSpecies == recTraktData.getSpecieID() || m_nShowSpecies == -1) && recTraktData.getNumOf() > 0)
						{
							m_vecDCLS.push_back(CTransaction_dcls_tree(i1+1,getActiveTrakt()->getTraktID(),1,recTraktData.getSpecieID(),recTraktData.getSpecieName(),
											 																	fDCLSCounter, //-(fDCLSClass/2.0),
																												(fDCLSCounter+fDCLSClass), //-(fDCLSClass/2.0)),
																												0.0,0,0.0,1.0,0.0,0.0,0.0,0.0,50,0,0,L""));
						}
						fDCLSCounter += fDCLSClass;
		
					}	// for (UINT i = 0;i < 20;i++)
				}
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		}	// if (m_vecTraktData.size() > 0)
		m_nTraktID = getActiveTrakt()->getTraktID();

		// Try to match diamterclasses actually in DB to diameterclasses created
		// and add numb er of; 100407 p�d
		m_nMax = 0;
		std::map<double,int> mapSumNumOfPerDCLS;
		if (m_vecDCLS.size() > 0 && m_vecDCLSTrees.size() > 0)
		{
			for (UINT i2 = 0;i2 < m_vecDCLS.size();i2++)
			{
				CTransaction_dcls_tree dcls1 = m_vecDCLS[i2];
				for (UINT i3 = 0;i3 < m_vecDCLSTrees.size();i3++)
				{
					CTransaction_dcls_tree dcls2 = m_vecDCLSTrees[i3];

					if (dcls1.getDCLS_from() == dcls2.getDCLS_from() &&
							dcls1.getDCLS_to() == dcls2.getDCLS_to() &&
							dcls1.getSpcID() == dcls2.getSpcID())
					{
						mapSumNumOfPerDCLS[dcls1.getDCLS_from()+dcls1.getDCLS_to()] += dcls2.getNumOf() + dcls2.getNumOfRandTrees();
					}
				}

				for (UINT i3 = 0;i3 < m_vecDCLSTrees.size();i3++)
				{
					CTransaction_dcls_tree dcls2 = m_vecDCLSTrees[i3];

					if (dcls1.getDCLS_from() == dcls2.getDCLS_from() &&
							dcls1.getDCLS_to() == dcls2.getDCLS_to() &&
							dcls1.getSpcID() == dcls2.getSpcID())
					{
						m_vecDCLS[i2].setNumOf(mapSumNumOfPerDCLS[dcls1.getDCLS_from()+dcls1.getDCLS_to()]);
						m_nMax = max(m_nMax,mapSumNumOfPerDCLS[dcls1.getDCLS_from()+dcls1.getDCLS_to()]);
					}
				}
				mapSumNumOfPerDCLS.clear();
			}	// for (UINT i2 = 0;i2 < m_vecDCLS.size();i2++)

		}	// if (m_vecDCLS.size() > 0 && m_vecDCLSTrees.size() > 0)
	}


	// Do nothing, empty stand; 100408 p�d
	//if (m_fMaxDCLS == 0.0) return;

//-----------------------------------------------------------------------
  // The data for the line chart
	StrArr sarr((int)m_fMaxDCLS+1);
	vecDbl vDbl,vDblX,vDblY;
	//---------------------------------------------------------------------
	// Setup diameterclass
	fDCLSClass = m_recTraktMiscData.getDiamClass();
	fDCLSCounter = 0.0;
	int nCnt = 0;
	char szTmp[255];
	for (int i3 = 0;i3 < (int)m_fMaxDCLS+1;i3++)
	{
		if (m_fMaxDCLS*fDCLSClass > 80.0)
		{
			if (((i3 % 2) == 0))
				sprintf(szTmp,"%.0f",fDCLSCounter);
			else
				sprintf(szTmp," ");
		}
		else
			sprintf(szTmp,"%.0f",fDCLSCounter);
		
		sarr.add(i3,szTmp);
		
		fDCLSCounter += fDCLSClass;
	}

	vecBarArray vecDCLSDblArr;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
			if (recTraktData.getNumOf() > 0)
			{
				//---------------------------------------------------------------------
				// Setup graf; hights/diameterclass
				vDbl.clear();
				vDbl.push_back(0.0);
				for (UINT i4 = 0;i4 < m_vecDCLS.size();i4++)
				{
					if (m_vecDCLS[i4].getSpcID() == recTraktData.getSpecieID())
					{
						vDbl.push_back(m_vecDCLS[i4].getNumOf() + m_vecDCLS[i4].getNumOfRandTrees());
					}
				}
				vecDCLSDblArr.push_back(CBarArray(recTraktData.getSpecieID(),recTraktData.getSpecieName(),0.0,vDbl));
			}
		}
	}


	if (dt == DRAW_VIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(60, 15, rect.right-240, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);

		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-150, 15); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);
	}
	else if (dt == DRAW_PREVIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 10, rect.right-350, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);
		
		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-270, 10); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);
	}

  // Add a title to the y-axis
  xyChart->yAxis()->setTitle(convStr(m_sDiagramYLabel));

	// Add a title to the x axis
  xyChart->xAxis()->setTitle(convStr(m_sDiagramXLabel));

  // Set the x axis labels
	if (sarr.numof() > 0)
		xyChart->xAxis()->setLabels(StringArray(sarr.get(), sarr.numof()));
	//xyChart->xAxis()->setLinearScale(2, 26, 2, 1);

	BarLayer *layer = xyChart->addBarLayer(Chart::Side,5);
  layer->setBarGap(0.2, Chart::TouchBar);

	if (m_nShowSpecies > -1)
	{
		// Set 2 decimals; 100408 p�d
		layer->setAggregateLabelFormat("{value|0}");
    // Enable bar label for each segment of the stacked bar; 100408 p�d
    layer->setAggregateLabelStyle("arialbd.ttf",8); //,0xffffff);
	}

	if (vecDCLSDblArr.size() > 0)
	{
		for (UINT i6 = 0;i6 < vecDCLSDblArr.size();i6++)
		{
			if (m_nShowSpecies == vecDCLSDblArr[i6].getSpcID() || m_nShowSpecies == -1)
			{
				layer->addDataSet(DoubleArray(vecDCLSDblArr[i6].get(), vecDCLSDblArr[i6].numof()),-1,convStr(vecDCLSDblArr[i6].getSpcName()));
			}
		}
	}

	if (m_nMax <= 40)
		xyChart->yAxis()->setLinearScale(0, m_nMax+1, 1);
	else if (m_nMax > 40 && m_nMax < 100)
		xyChart->yAxis()->setLinearScale(0, m_nMax+1, 2);
	else
		xyChart->yAxis()->setLinearScale(0, m_nMax+1, 10);

	if (dt == DRAW_VIEW)
	{
	  // Output the chart
		m_chartViewer.setChart(xyChart);
	}
	else if (dt == DRAW_PREVIEW)
	{
		// The scaling factor to adjust for printer resolution
		// (pDC = CDC pointer representing the printer device context)
		double xScaleFactor = pDC->GetDeviceCaps(LOGPIXELSX) / 96.0;
		double yScaleFactor = pDC->GetDeviceCaps(LOGPIXELSY) / 96.0;

		// Output the chart as BMP
		MemBlock bmp = xyChart->makeChart(Chart::BMP);

		// The BITMAPINFOHEADER is 14 bytes offset from the beginning
		LPBITMAPINFO header = (LPBITMAPINFO)(bmp.data + 14);

		// The bitmap data
		LPBYTE bitData = (LPBYTE)(bmp.data) + ((LPBITMAPFILEHEADER)(bmp.data))->bfOffBits;

		int nLeft = 20*xScaleFactor;
		int nTop = 20*yScaleFactor;
		int nWidth = 5220; //header->bmiHeader.biWidth*xScaleFactor*0.6;
		int nHeight = 60*yScaleFactor;

		TEXTMETRIC tm;
		CBrush brush;
		brush.CreateSolidBrush(RGB(0,0,255)); 
		//---------------------------------------------------------------------------
		// Setup Caption
		pDC->FillRect(CRect(nLeft,nTop,nWidth*0.9,nHeight),&brush);
		pDC->SelectObject((CFont*)&m_fntCaption);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->DrawText(m_sDiagramCaption,CRect(nLeft,nTop,nWidth*0.9,nHeight),DT_SINGLELINE|DT_CENTER|DT_VCENTER);

		//---------------------------------------------------------------------------
		// Setup date
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextSmall);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nLeft,nHeight,getDateTime());

		//---------------------------------------------------------------------------
		// Setup Name of Stand
		int nTextX = 0;
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextBold);
		pDC->GetTextMetrics(&tm);
		nTextX = max(nTextX,m_sStandNumber.GetLength());
		nTextX = max(nTextX,m_sStandName.GetLength());
		nTextX = max(nTextX,m_sPropertyNameLabel.GetLength());
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*2,m_sStandNumber+L":");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*3,m_sStandName+L":");
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft,nHeight+tm.tmHeight*4,m_sPropertyNameLabel+L":");
		pDC->SelectObject((CFont*)&m_fntText);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*2,getActiveTrakt()->getTraktNum());
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*3,getActiveTrakt()->getTraktName());
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*4,m_sPropertyName);

		//---------------------------------------------------------------------------
		// Output to device context
		StretchDIBits(pDC->m_hDC,
									(int)(10 * xScaleFactor),
									(int)(300 * yScaleFactor),
									(int)(1392 * xScaleFactor * 0.6),
									(int)(583 * yScaleFactor * 0.6),
									0, 0, header->bmiHeader.biWidth, header->bmiHeader.biHeight,
									bitData, header, DIB_RGB_COLORS, SRCCOPY);

	}

	// Clean up
	sarr.free();

	if (vecDCLSDblArr.size() > 0)
		for (UINT i = 0;i < vecDCLSDblArr.size();i++)
			vecDCLSDblArr[i].free();


  //free up resources
	delete xyChart;

}


void CDiamDistributionFormView::OnPrint(CDC* pDC, CPrintInfo *pInfo)
{
	drawChartView(true,DRAW_PREVIEW,pDC);
}

BOOL CDiamDistributionFormView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return CXTResizeFormView::DoPreparePrinting(pInfo);
}

void CDiamDistributionFormView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this, RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}
}

void CDiamDistributionFormView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	drawChartView();
	CXTResizeFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CDiamDistributionFormView::doRunPrint()	
{ 
	this->OnFilePrint();
}

void CDiamDistributionFormView::OnCBoxSpeciesChanged(void)
{
	CString sSpcName;
	m_nCurCBSel = m_wndCBox1.GetCurSel();
	m_wndCBox1.GetWindowText(sSpcName);
	m_nShowSpecies = -1;
	if (m_nCurCBSel != CB_ERR)
	{
		if (m_nCurCBSel > 0)
		{
			if (m_vecTraktData.size() > 0 && m_nCurCBSel-1 < m_vecTraktData.size())
			{		
				for (UINT i = 0;i < m_vecTraktData.size();i++)
				{
					if (m_vecTraktData[i].getSpecieName().Compare(sSpcName) == 0)
					{
						m_nShowSpecies = m_vecTraktData[i].getSpecieID();
						break;
					}
				}
			}	
		}
	}
	drawChartView(true);
}
