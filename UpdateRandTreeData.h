#pragma once

#include "OXMaskedEdit.h"    // COXMaskedEdit

#include "Resource.h"

// CUpdateRandTreeData dialog

class CUpdateRandTreeData : public CDialog
{
	DECLARE_DYNAMIC(CUpdateRandTreeData)

	CString m_sLangFN;

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	COXMaskedEdit m_wndEdit3;
	COXMaskedEdit m_wndEdit4;

	CButton m_wndBtnUpdate;
	CButton m_wndBtnCancel;

	CButton m_wndCheck1;
	CButton m_wndCheck2;

	CString m_sMsgCap;
	CString m_sMsgCheckBox;
	CString m_sMsgAgeMissing;
	CString m_sMsgBonitetMissing;

	int m_nAgeFrom;
	int m_nAgeTo;

	CString m_sBonitetFrom;
	CString m_sBonitetTo;

	BOOL m_bDoAge;
	BOOL m_bDoBonitet;

public:
	CUpdateRandTreeData(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUpdateRandTreeData();

	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG5 };

	BOOL getDoAge(void)			{ return m_bDoAge; }
	BOOL getDoBonitet(void)	{ return m_bDoBonitet; }

	int getAgeFrom(void)		{ return m_nAgeFrom; }
	int getAgeTo(void)			{ return m_nAgeTo; }

	CString getBonitetFrom(void)	{ return m_sBonitetFrom; }
	CString getBonitetTo(void)		{ return m_sBonitetTo; }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
