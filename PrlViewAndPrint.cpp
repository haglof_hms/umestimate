// PrlViewAndPrint.cpp : implementation file
//

// PrlViewAndPrint.cpp : implementation file
//

#include "stdafx.h"
//#include "UMPricelists.h"
#include "PrlViewAndPrint.h"

//#include "PrlViewAndPrintSettingsDlg.h"

#include "ResLangFileReader.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc

IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc construction/destruction

CMDIPrlViewAndPrintDoc::CMDIPrlViewAndPrintDoc()
{
}

CMDIPrlViewAndPrintDoc::~CMDIPrlViewAndPrintDoc()
{
}

BOOL CMDIPrlViewAndPrintDoc::OnNewDocument()
{

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc serialization

void CMDIPrlViewAndPrintDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIPrlViewAndPrintDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIPrlViewAndPrintDoc commands

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintFrame


IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintFrame)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_PRINT_OUT_PRL, OnPrintOut)
//	ON_COMMAND(ID_TBTN_SETTINGS, OnSettings)
//	ON_UPDATE_COMMAND_UI(ID_TBTN_SETTINGS, OnSettingsTBtn)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPrlViewAndPrintFrame::CMDIPrlViewAndPrintFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bIsSettingsTBtn = TRUE;
}

CMDIPrlViewAndPrintFrame::~CMDIPrlViewAndPrintFrame()
{
}

void CMDIPrlViewAndPrintFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PRINT_PRICELIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDIPrlViewAndPrintFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
//			sToolTip1 = xml.str(IDS_STRING1900);
//			sToolTip2 = xml.str(IDS_STRING1903);
		}
		xml.clean();
	}

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();


	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR5)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_PRINT,sToolTip1);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),-1,_T(""),FALSE);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPrlViewAndPrintFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CMDIPrlViewAndPrintFrame diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPrlViewAndPrintFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// PRIVATE
void CMDIPrlViewAndPrintFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIPrlViewAndPrintFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PRINT_PRICELIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPrlViewAndPrintFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIPrlViewAndPrintFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPrlViewAndPrintFrame::OnPrintOut(void)
{
	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW10);
	if (pPrlView != NULL)
	{
		pPrlView->printOut();
		pPrlView = NULL;
	}

}

void CMDIPrlViewAndPrintFrame::OnSettings(void)
{
/*
	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW10);
	if (pPrlView != NULL)
	{
		pPrlView->settingsDlg();
		pPrlView = NULL;
	}
*/
}

void CMDIPrlViewAndPrintFrame::OnSettingsTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsSettingsTBtn );
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPrlViewAndPrintFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDIPrlViewAndPrintFrame::setLanguage()
{
/*
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
*/
}

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint

IMPLEMENT_DYNCREATE(CPrlViewAndPrint, CMyHtmlView)

BEGIN_MESSAGE_MAP(CPrlViewAndPrint, CMyHtmlView)
	//{{AFX_MSG_MAP(CPrlViewAndPrint)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	//}}AFX_MSG_MAP
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CPrlViewAndPrint::CPrlViewAndPrint()
{
	//{{AFX_DATA_INIT(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDB = NULL;
}

CPrlViewAndPrint::~CPrlViewAndPrint()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CPrlViewAndPrint::DoDataExchange(CDataExchange* pDX)
{
	CMyHtmlView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CPrlViewAndPrint::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CMyHtmlView::OnCopyData(pWnd, pData);
}


/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint diagnostics

#ifdef _DEBUG
void CPrlViewAndPrint::AssertValid() const
{
	CMyHtmlView::AssertValid();
}

void CPrlViewAndPrint::Dump(CDumpContext& dc) const
{
	CMyHtmlView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint message handlers

void CPrlViewAndPrint::OnInitialUpdate()
{
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}


LRESULT CPrlViewAndPrint::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		case ID_SHOWVIEW_MSG :	// SendMessageToDecendants()
		{
			m_pPricelistData = (CTransaction_trakt_misc_data*)lParam;
			setHTMFileName(PRL_VIEW_FN);
			setupHTM();
			break;
		}	// case ID_MSG_FROM :
	};

	return 0L;
}

void CPrlViewAndPrint::setupHTM_Pricelist(void)
{

	m_bUseSettings = FALSE;
	m_bShowPriceByQual = TRUE;

	BOOL bIsAvgPrl = FALSE;
	CStringArray sarrQDesc;
	CString sQualityName;
	CTransaction_pricelist rec;
	CELVPrlViewAndPrintRec *pRec = NULL;
	TCHAR szName[128];
	int nPriceIn;
	double fPricePerQual;
	std::map<int,double> mapPricePerQual;

	xmllitePricelistParser *pPrlPars = new xmllitePricelistParser();

	vecTransactionSpecies vecSpc,vecSpcALL;
	CTransaction_species recSpc;
	vecTransactionAssort vecAss;
	CTransaction_assort recAss;
	vecTransactionDiameterclass vecDCLS;
	CTransaction_diameterclass recDCLS;
	vecTransactionPrlData vecPrlData;
	vecTransactionPrlData vecPrlData_perc;

	CStringArray sarrPulpWoodTypes;

	CString sPulpwoodType;

	CString sTraktName;
	CString sTraktNumber;

	CTransaction_trakt recTrakt;
	if(m_pDB != NULL)
	{
		m_pDB->getTrakt(m_pPricelistData->getTSetTraktID(), recTrakt);
		sTraktName = recTrakt.getTraktName();
		sTraktNumber = recTrakt.getTraktNum();
	}

	CTransaction_trakt_misc_data recMisc;

	CString sBuff;
	CString sData;
	CString sDCLSData;

	RLFReader xml;
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN) && pPrlPars)
	{

		if (pPrlPars->loadStream(m_pPricelistData->getXMLPricelist()) && xml.Load(m_sLangFN))
		{
			// Add Pulpwood types to this StringArray; 070608 p�d
			sarrPulpWoodTypes.RemoveAll();
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1552)));
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1550)));
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1551)));
			// Start HTM-file; 090901 p�d
			startHTM();

			pPrlPars->getHeaderName(szName);
			pPrlPars->getHeaderPriceIn(&nPriceIn);
			bIsAvgPrl = (m_pPricelistData->getTypeOf() == 2);

			if (!m_bUseSettings /* = FALSE */)
			{
				pPrlPars->getSpeciesInPricelistFile(vecSpc);
				vecSpcALL = vecSpc;
			}
			pPrlPars->getAssortmentPerSpecie(vecAss);
			
			setHTM_text(getDBDateTime(),HTM_FREE_FMT,1);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING131) + _T(" : "),HTM_FREE_FMT,4);
			setHTM_text(szName,HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			// Add stand-information; 090130 p�d
			setHTM_text(xml.str(IDS_STRING101) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sTraktName,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING1000) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sTraktNumber,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			if (!bIsAvgPrl)
			{
				// Check if pricelist is set to be used; 090108 p�d
				if (nPriceIn == 1)
				{
					setHTM_text(xml.str(IDS_STRING1559) + _T(" : "),HTM_FREE_FMT,2);
					setHTM_text(xml.str(IDS_STRING1560),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				}
				else if (nPriceIn == 2)
				{
					setHTM_text(xml.str(IDS_STRING1559) + _T(" : "),HTM_FREE_FMT,2);
					setHTM_text(xml.str(IDS_STRING1561),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				}
				setHTML_linefeed(1);
			}

			setHTML_line(HTM_HR_NOSHADE,2);

			// OBS! Print by specie; 090107 p�d
			if (vecSpc.size() > 0)
			{

				for (UINT spc = 0;spc < vecSpc.size();spc++)
				{
					recSpc = vecSpc[spc];			
					// Set name of specie; 090902 p�d
					setHTM_text(recSpc.getSpcName(),HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK,true);
					setHTML_linefeed(1);

					//////////////////////////////////////////////////////////////////////////////////////////
					// START --- ADDING ASSORTMENTS/SPECIE; 090902 P�D		
					//////////////////////////////////////////////////////////////////////////////////////////
					if (vecAss.size() > 0)
					{
						// Start by setting up the column widths; 090902 p�d
						setHTM_table();
						setHTM_start_table_column();
						setHTM_table_column(xml.str(IDS_STRING1553),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1554),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1555),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1556),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1557),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_end_table_column();

						for (UINT ass = 0;ass < vecAss.size();ass++)
						{
							recAss = vecAss[ass];
							if (recAss.getSpcID() == recSpc.getSpcID() && !recAss.getAssortName().IsEmpty())
							{
								if (recAss.getPulpType() > -1 && recAss.getPulpType() < sarrPulpWoodTypes.GetCount() && sarrPulpWoodTypes.GetCount() > 0)
									sPulpwoodType = sarrPulpWoodTypes.GetAt(recAss.getPulpType()).Left(20);
								else
									sPulpwoodType = _T("");

								setHTM_start_table_column();
								setHTM_table_column(recAss.getAssortName().Left(20),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
								setHTM_table_column(sPulpwoodType,100,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getMinDiam(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getPriceM3fub(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getPriceM3to(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_end_table_column();
							}
						}	// for (UINT ass = 0;ass < vecAss.size();ass++)
						endHTM_table();
					}	// if (vecAss.size() > 0)
					//////////////////////////////////////////////////////////////////////////////////////////
					// END --- ADDING ASSORTMENTS/SPECIE; 090902 P�D		
					//////////////////////////////////////////////////////////////////////////////////////////

					setHTML_linefeed(1);

					//----------------------------------------------------------------------------------------
					if (!bIsAvgPrl)
					{

						pPrlPars->getDCLSForSpecie(recSpc.getSpcID(),vecDCLS);
						pPrlPars->getPriceForSpecie(recSpc.getSpcID(),vecPrlData);

						//////////////////////////////////////////////////////////////////////////////////////////
						// START --- ADDING DIAMTRERCLASSES AND PRICE/DIAMTERCLASS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
						if (vecDCLS.size() > 0 && vecPrlData.size() > 0)
						{

							setHTM_table();
							setHTM_start_table_column();
							setHTM_table_column(xml.str(IDS_STRING1558),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,true);
							sBuff.Empty();
							for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
							{
								recDCLS = vecDCLS[dcls];
								sBuff.Format(_T("%d-"),recDCLS.getStartDiam());
								setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
								// Initciate map for prices per quality; 090109 p�d

								mapPricePerQual[dcls] = 0.0;
							}	// for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
							setHTM_end_table_column();

							for (UINT prl_data = 0;prl_data < vecPrlData.size();prl_data++)
							{
								CTransaction_prl_data recPrlData = vecPrlData[prl_data];
								if (_tcslen(recPrlData.getQualName()) > 0)
								{
									setHTM_start_table_column();
	
									setHTM_table_column(recPrlData.getQualName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
									sBuff.Empty();
									// Check if there's any prices; 090108 p�d
									if (recPrlData.getInts().size() > 0)
									{
										for (UINT price = 0;price < recPrlData.getInts().size();price++)
										{
											setHTM_table_column(recPrlData.getInts()[price],50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
										}	// for (UINT price = 0;price < recPrlData.getInts().size();price++)
									}	// if (recPrlData.getInts().size() > 0)
									setHTM_end_table_column();
								} // if (_tcslen(recPrlData.getQualName()) > 0)
							}	// for (UINT prl_data = 0;prl_data < vecPrlData.size();prl_data++)

							endHTM_table();
						}	// if (vecDCLS.size() > 0 && vecPrlData.size() > 0)
						//////////////////////////////////////////////////////////////////////////////////////////
						// END --- ADDING DIAMTRERCLASSES AND PRICE/DIAMTERCLASS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////


						//////////////////////////////////////////////////////////////////////////////////////////
						// START --- ADDING QUALITYDESCRIPTIONS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
						pPrlPars->getQualDescNameForSpecie(recSpc.getSpcID(),sarrQDesc);

						if (sarrQDesc.GetCount() > 0)
						{
							setHTM_table();
							for (int i = 0;i < sarrQDesc.GetCount();i++)
							{
								sQualityName = sarrQDesc.GetAt(i);
								
								pPrlPars->getQualDescPercentForQualityAndSpecie(recSpc.getSpcID(),sQualityName,vecPrlData_perc);

								if (vecDCLS.size() > 0)
								{
									setHTM_start_table_column();
									setHTM_table_column(sQualityName,150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_CENTER,true,true);
									setHTM_end_table_column();

									setHTM_start_table_column();
									setHTM_table_column(xml.str(IDS_STRING1558),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,true);
									sBuff.Empty();
									for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
									{
										recDCLS = vecDCLS[dcls];
										sBuff.Format(_T("%d-"),recDCLS.getStartDiam());
										setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
										// Initciate map for prices per quality; 090109 p�d
										mapPricePerQual[dcls] = 0.0;
									}	// for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
									setHTM_end_table_column();
								}

								if (vecPrlData_perc.size() > 0)
								{

									if (vecPrlData_perc[0].getInts().size() > 0)
									{
										//----------------------------------------------------------------------------------------
										for (UINT prl_data1 = 0;prl_data1 < vecPrlData_perc.size();prl_data1++)
										{
											setHTM_start_table_column();
											CTransaction_prl_data recPrlData_perc = vecPrlData_perc[prl_data1];
											setHTM_table_column(recPrlData_perc.getQualName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
											sBuff.Empty();
											for (UINT qual = 0;qual < recPrlData_perc.getInts().size();qual++)
											{
												sBuff.Format(_T("%d"),recPrlData_perc.getInts()[qual]);
												setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
											}	// for (UINT qual = 0;qual < recPrlData_perc.getInts().size(),qual++)
											setHTM_end_table_column();
										}	// for (UINT prl_data1 = 0;prl_data1 < vecPrlData_perc.size();prl_data1++)
									}	// if (vecPrlData_perc[spc].getInts().size() > 0)

								}	// if (spc < vecPrlData_perc.size())


								if (m_bShowPriceByQual)
								{

									// Calculate Price per diameterclass and qualitydescription; 090109 p�d
									if (vecPrlData.size() == vecPrlData_perc.size())
									{

										for (UINT qsum = 0;qsum < vecPrlData_perc.size();qsum++)
										{
											CTransaction_prl_data recPrlData = vecPrlData[qsum];
											CTransaction_prl_data recPrlData_perc = vecPrlData_perc[qsum];
											if (recPrlData.getInts().size() == recPrlData_perc.getInts().size())
											{
												for (UINT qsum_1 = 0;qsum_1 < recPrlData.getInts().size();qsum_1++)
												{
													// OBS! qsum_1 = column (dcls); 090109 p�d
													mapPricePerQual[qsum_1] += recPrlData.getInts()[qsum_1]*recPrlData_perc.getInts()[qsum_1]/100.0;
												}	// for (UINT qsum_1 = 0;qsum_1 < recPrlData.getInts().size();qsum_1++)
											}	// if (recPrlData.getInts().size() == recPrlData_perc.getInts().size())
										}							

										
										if (mapPricePerQual.size() > 0)
										{
											setHTM_start_table_column();
											setHTM_table_column(xml.str(IDS_STRING1902),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,false);
											for (UINT map = 0;map < mapPricePerQual.size();map++)
											{
												fPricePerQual = mapPricePerQual[map];
												sBuff.Format(_T("%.0f"),fPricePerQual);

												setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true,true);
											}	// for (UINT map = 0;map < mapPricePerQual.size();map++)
											setHTM_end_table_column();
										}	// if (mapPricePerQual.size() > 0)

									}

									mapPricePerQual.clear();

								}	// if (m_bShowPriceByQual)

							}	// for (int i = 0;i < sarrQDesc.GetCount();i++)
							endHTM_table();
						}	// if (sarrQDesc.GetCount() > 0)

						//////////////////////////////////////////////////////////////////////////////////////////
						// END --- ADDING QUALITYDESCRIPTIONS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
				
					}	// if (!bIsAvgPrl)	
					// Don't add a line after the last specie; 090902 p�d
					if (spc < vecSpc.size()-1) setHTML_line(HTM_HR_NOSHADE,1);

				}	// for (UINT spc = 0;spc < vecSpc.size();spc++)

			}	// if (vecSpc.size() > 0)
			
			xml.clean();

		}
		delete pPrlPars;
	}

	sarrQDesc.RemoveAll();
	pRec = NULL;
	vecAss.clear();
	vecDCLS.clear();
	vecPrlData.clear();
	vecPrlData_perc.clear();
	mapPricePerQual.clear();

	// End of HTML-file
	endHTM();
	// Save to disk and load into Viewer; 090901 p�d
	showHTM();
}

void CPrlViewAndPrint::setupHTM_Costtemplate(void)
{
	TCHAR szName[127];
	TCHAR szDoneBy[127];
	int nCutCostSetAs;

	vecObjectCostTemplate_table vecTransp1;
	CObjectCostTemplate_table recTransp1;

	vecTransaction_costtempl_transport vecTransp2;
	CTransaction_costtempl_transport recTransp2;

	vecObjectCostTemplate_table vecCuttingDGV;
	vecObjectCostTemplate_table vecCuttingM3FUB;
	CObjectCostTemplate_table recCutting;
	CTransaction_costtempl_cutting recCutting2;

	BOOL bIsThereAnyOtherCosts = FALSE;
	vecObjectCostTemplate_other_cost_table vecOtherCosts;
	CObjectCostTemplate_other_cost_table recOtherCosts;

	CString sDCLS;
	CString sTmp;

	xmlliteCostsParser *pCostParser = new xmlliteCostsParser();

	// Check that Parser instance is ok; 091222 p�d
	if (pCostParser == NULL) return;

	CString sTraktName;
	CString sTraktNumber;

	CTransaction_trakt recTrakt;
	if(m_pDB != NULL)
	{
		m_pDB->getTrakt(m_pPricelistData->getTSetTraktID(), recTrakt);
		sTraktName = recTrakt.getTraktName();
		sTraktNumber = recTrakt.getTraktNum();
	}


	RLFReader xml;
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		if (pCostParser->loadStream(m_pPricelistData->getXMLCosts()) && xml.Load(m_sLangFN))
		{
			// Start HTM-file; 091019 p�d
			startHTM();
		
			//******************************************************************************************
			// Section 1: Header information; 091019 p�d
			pCostParser->getObjCostsTmplName(szName);
			pCostParser->getObjCostsTmplDoneBy(szDoneBy);

			setHTM_text(getDBDateTime(),HTM_FREE_FMT,1);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING50000) + _T(" : "),HTM_FREE_FMT,4);
			setHTM_text(szName,HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			// Add stand-information; 090130 p�d
			setHTM_text(xml.str(IDS_STRING101) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sTraktName,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING1000) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sTraktNumber,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING50010) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(szDoneBy,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);
			setHTML_line(HTM_HR_NOSHADE,2);

			//******************************************************************************************
			// Section 2: Transport - to road and to Industy; 091019 p�d
			pCostParser->getObjCostsTmplTransport(vecTransp1);	// Transport to road
			pCostParser->getObjCostsTmplTransport2(vecTransp2);	// Transport to industry
			//////////////////////////////////////////////////////////////////////////////////////////
			// START --- ADDING TRANSPORT TABLE TO ROAD; 091019 P�D		
			//////////////////////////////////////////////////////////////////////////////////////////
			setHTM_text(xml.str(IDS_STRING50100),HTM_FREE_FMT,3,_T(""),HTM_COLOR_BLUE,true);
			setHTML_linefeed(2);
			if (vecTransp1.size() > 0)
			{
				setHTM_text(xml.str(IDS_STRING50110),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				setHTML_linefeed(1);
				// Start table for column headlines (diamterclasses); 091020 p�d
				setHTM_table();
				setHTM_start_table_column();
				setHTM_table_column(xml.str(IDS_STRING50040),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
				recTransp1 = vecTransp1[0];	// First item holds info. on Diameterclasses; 091020 p�d
				for (UINT dcls1 = 0;dcls1 < recTransp1.getValues_int().size();dcls1++)
				{
					sDCLS.Format(_T("%d-"),recTransp1.getValues_int()[dcls1]);
					setHTM_table_column(sDCLS,80,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
				}
				setHTM_end_table_column();

				for (UINT transp1 = 1;transp1 < vecTransp1.size();transp1++)
				{
					recTransp1 = vecTransp1[transp1];	
					setHTM_start_table_column();
					setHTM_table_column(recTransp1.getSpcName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
					for (UINT dcls1 = 0;dcls1 < recTransp1.getValues_int().size();dcls1++)
					{
						sDCLS.Format(_T("%d"),recTransp1.getValues_int()[dcls1]);
						setHTM_table_column(sDCLS,80,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
					}
					setHTM_end_table_column();			
				}	// for (UINT transp1 = 1;transp1 < vecTransp1.size();transp1++)
				// And of Table here; 091020 p�d
				endHTM_table();
			}	// if (vecTransp1.size() > 0)

			//////////////////////////////////////////////////////////////////////////////////////////
			// END --- ADDING TRANSPORT TABLE TO ROAD; 091019 P�D		
			//////////////////////////////////////////////////////////////////////////////////////////

			//////////////////////////////////////////////////////////////////////////////////////////
			// START --- ADDING TRANSPORT TABLE TO INDUSTRY; 091020 P�D		
			//////////////////////////////////////////////////////////////////////////////////////////
			setHTML_linefeed(1);
			if (vecTransp2.size() > 0)
			{
				setHTM_text(xml.str(IDS_STRING50120),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				setHTML_linefeed(1);
				// Start table with column headlines; 091020 p�d
				setHTM_table();
				setHTM_start_table_column();
				setHTM_table_column(xml.str(IDS_STRING50030),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
				setHTM_table_column(xml.str(IDS_STRING50130),80,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
				setHTM_table_column(xml.str(IDS_STRING50140),80,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
				setHTM_end_table_column();

				for (UINT transp2 = 0;transp2 < vecTransp2.size();transp2++)
				{
					recTransp2 = vecTransp2[transp2];
					setHTM_start_table_column();

					setHTM_table_column(recTransp2.getSpecieName(),100,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
					sTmp.Format(_T("%.2f"),recTransp2.getCost());
					setHTM_table_column(sTmp,80,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
					sTmp.Format(_T("%.1f"),recTransp2.getMaxCost());
					setHTM_table_column(sTmp,80,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);

					setHTM_end_table_column();			
				}	// for (UINT transp2 = 0;transp2 < vecTransp2.size();transp2++)
				// End of Table here; 091020 p�d
				endHTM_table();
			}	// if (vecTransp2.size() > 0)
			setHTML_line(HTM_HR_NOSHADE,2);

			//////////////////////////////////////////////////////////////////////////////////////////
			// END --- ADDING TRANSPORT TABLE TO INDUSTRY; 091020 P�D		
			//////////////////////////////////////////////////////////////////////////////////////////


			//******************************************************************************************
			// Section 3: Information on cutting - kr/m3 or table DGV; 091019 p�d
			pCostParser->getCutCostSetAs(&nCutCostSetAs);
			pCostParser->getObjCostsTmplCutting(vecCuttingDGV,vecCuttingM3FUB);
			pCostParser->getObjCostsTmplCutting2(recCutting2);

			setHTM_text(xml.str(IDS_STRING50200),HTM_FREE_FMT,3,_T(""),HTM_COLOR_BLUE,true);
			setHTML_linefeed(2);
			// Check type of Cutting costs selected; 091020 p�d
			if (nCutCostSetAs == 1)	// Set in kr/m3
			{
				setHTM_text(xml.str(IDS_STRING50210),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				setHTML_linefeed(2);

				// Start table with column headlines; 091020 p�d
				setHTM_table();
				setHTM_start_table_column();
				setHTM_table_column(xml.str(IDS_STRING50211),150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,true);
				setHTM_table_column(xml.str(IDS_STRING50212),150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,true);
				setHTM_table_column(xml.str(IDS_STRING50213),150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,true);
				setHTM_end_table_column();

				setHTM_start_table_column();
				// Avverkning (kr/m3)
				sTmp.Format(_T("%.0f"),recCutting2.getCut1());
				setHTM_table_column(sTmp,150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
				// Sk�rdare (kr/m3)
				sTmp.Format(_T("%.0f"),recCutting2.getCut2());
				setHTM_table_column(sTmp,150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
				// Skotare (kr/m3)
				sTmp.Format(_T("%.0f"),recCutting2.getCut3());
				setHTM_table_column(sTmp,150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
				setHTM_end_table_column();

				endHTM_table();			
			}
			else if (nCutCostSetAs == 2)	// Set as a table dgv
			{
				//////////////////////////////////////////////////////////////////////////////////////////
				// START --- ADDING TABLE FOR CUTTING (DGV); 091020 P�D		
				//////////////////////////////////////////////////////////////////////////////////////////
				if (vecCuttingDGV.size() > 0)
				{
					setHTM_text(xml.str(IDS_STRING50220),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
					setHTML_linefeed(1);
					// Start table for column headlines (diamterclasses); 091020 p�d
					setHTM_table();
					setHTM_start_table_column();
					setHTM_table_column(xml.str(IDS_STRING50041),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
					recCutting = vecCuttingDGV[0];	// First item holds info. on DGV; 091020 p�d
					for (UINT dgv1 = 0;dgv1< recCutting.getValues_int().size();dgv1++)
					{
						sTmp.Format(_T("%d-"),recCutting.getValues_int()[dgv1]);
						setHTM_table_column(sTmp,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
					}
					setHTM_end_table_column();

					for (UINT cut1 = 1;cut1 < vecCuttingDGV.size();cut1++)
					{
						recCutting = vecCuttingDGV[cut1];
						setHTM_start_table_column();
						setHTM_table_column(recCutting.getSpcName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
						for (UINT dgv1 = 0;dgv1 < recCutting.getValues_int().size();dgv1++)
						{
							sTmp.Format(_T("%d"),recCutting.getValues_int()[dgv1]);
							setHTM_table_column(sTmp,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
						}
						setHTM_end_table_column();			
					}	// for (UINT cut1 = 1;cut1 < vecCutting.size();cut1++)
					// And of Table here; 091020 p�d
					endHTM_table();
				}	// if (vecTransp1.size() > 0)

				//////////////////////////////////////////////////////////////////////////////////////////
				// END --- ADDING TABLE FOR CUTTING (DGV); 091020 p�d
				//////////////////////////////////////////////////////////////////////////////////////////

			}
			else if (nCutCostSetAs == 3)	// Set as a table m3fub
			{
				//////////////////////////////////////////////////////////////////////////////////////////
				// START --- ADDING TABLE FOR CUTTING (M3FUB); 091020 P�D		
				//////////////////////////////////////////////////////////////////////////////////////////
				if (vecCuttingM3FUB.size() > 0)
				{
					setHTM_text(xml.str(IDS_STRING50221),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
					setHTML_linefeed(1);
					// Start table for column headlines (diamterclasses); 091020 p�d
					setHTM_table();
					setHTM_start_table_column();
					setHTM_table_column(xml.str(IDS_STRING50042),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
					recCutting = vecCuttingM3FUB[0];	// First item holds info. on DGV; 091020 p�d
					for (UINT m3fub = 0;m3fub < recCutting.getValues_float().size();m3fub++)
					{
						sTmp.Format(_T("%.2f-"),recCutting.getValues_float()[m3fub]);
						setHTM_table_column(sTmp,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
					}
					setHTM_end_table_column();

					for (UINT cut1 = 1;cut1 < vecCuttingM3FUB.size();cut1++)
					{
						recCutting = vecCuttingM3FUB[cut1];
						setHTM_start_table_column();
						setHTM_table_column(recCutting.getSpcName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
						for (UINT m3fub = 0;m3fub < recCutting.getValues_float().size();m3fub++)
						{
							sTmp.Format(_T("%.0f"),recCutting.getValues_float()[m3fub]);
							setHTM_table_column(sTmp,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
						}
						setHTM_end_table_column();			
					}	// for (UINT cut1 = 1;cut1 < vecCutting.size();cut1++)
					// And of Table here; 091020 p�d
					endHTM_table();
				}	// if (vecTransp1.size() > 0)

				//////////////////////////////////////////////////////////////////////////////////////////
				// END --- ADDING TABLE FOR CUTTING (M3FUB); 091020 p�d
				//////////////////////////////////////////////////////////////////////////////////////////

			}

			setHTML_linefeed(1);
			setHTML_line(HTM_HR_NOSHADE,2);

			//******************************************************************************************
			// Section 4: Other costs; 091019 p�d
			pCostParser->getObjCostsTmplOther(vecOtherCosts);

			setHTM_text(xml.str(IDS_STRING50300),HTM_FREE_FMT,3,_T(""),HTM_COLOR_BLUE,true);
			setHTML_linefeed(2);
			if (vecOtherCosts.size() > 0)
			{
				// Check if we have any Other costs. I.e. Price > 0.0; 091020 p�d
				for (UINT otc1 = 0;otc1 < vecOtherCosts.size();otc1++)
				{
					recOtherCosts = vecOtherCosts[otc1];
					if (recOtherCosts.getPrice() > 0.0) 
					{
						bIsThereAnyOtherCosts = TRUE;
						break;
					}
				}
				if (bIsThereAnyOtherCosts)
				{
					// Start table for column headlines (diamterclasses); 091020 p�d
					setHTM_table();

					// Start table with column headlines; 091020 p�d
					setHTM_table();
					setHTM_start_table_column();
					setHTM_table_column(xml.str(IDS_STRING50310),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
					setHTM_table_column(xml.str(IDS_STRING50320),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
					setHTM_table_column(xml.str(IDS_STRING50330), 50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
					setHTM_end_table_column();

					for (UINT otc1 = 0;otc1 < vecOtherCosts.size();otc1++)
					{
						recOtherCosts = vecOtherCosts[otc1];
						setHTM_start_table_column();
						sTmp.Format(_T("%.1f"),recOtherCosts.getPrice());
						setHTM_table_column(sTmp,100,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
						setHTM_table_column(recOtherCosts.getNotes(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
						sTmp.Format(_T("%.1f"),recOtherCosts.getIsVAT());
						setHTM_table_column(sTmp, 50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
						setHTM_end_table_column();
					}	// for (UINT otc1 = 0;otc1 < vecOtherCosts.size();otc1++)

					// And of Table here; 091020 p�d
					endHTM_table();
				}
			}	// if (vecOtherCosts.size() > 0)
			//******************************************************************************************
			// End of HTML-file
			endHTM();
			// Save to disk and load into Viewer; 091019 p�d
			showHTM();

		}	// if (pCostParser->loadStram(m_sXMLFileName))
	}
	delete pCostParser;
}


void CPrlViewAndPrint::setupHTM()
{

	if (m_pPricelistData->getTypeOf() == -1)
		setupHTM_Costtemplate();
	else 
		setupHTM_Pricelist();

}

void CPrlViewAndPrint::printOut()
{
	printOutHTM();
}


