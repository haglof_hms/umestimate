#include "StdAfx.h"

#include "UMEstimateDB.h"

#include <time.h>
//////////////////////////////////////////////////////////////////////////////////
// CUMEstimateDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMEstimateDB::CUMEstimateDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMEstimateDB::CUMEstimateDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMEstimateDB::CUMEstimateDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE
BOOL CUMEstimateDB::traktTypeExists(CTransaction_trakt_type &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_TRAKT_TYPE,rec.getID());

	return exists(sSQL);
}

BOOL CUMEstimateDB::originExists(CTransaction_origin &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_ORIGIN,rec.getID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::inputMethodExists(CTransaction_input_method &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_INPUT_METHOD,rec.getID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::hgtClassExists(CTransaction_hgt_class &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_HGT_CLASS,rec.getID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::cutClassExists(CTransaction_cut_class &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_CUT_CLASS,rec.getID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktExists(int trakt_id)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where trakt_id=%d"),
		TBL_TRAKT,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktExists(CTransaction_trakt &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where trakt_id=%d"),
		TBL_TRAKT,rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDataExists(int trakt_id)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_trakt_id=%d"),
		TBL_TRAKT_DATA,trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDataExists(int trakt_id,int tdata_id,int tdata_data_type)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_trakt_id=%d and tdata_id=%d and tdata_data_type=%d"),
		TBL_TRAKT_DATA,
		trakt_id,
		tdata_id,
		tdata_data_type);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDataExists(CTransaction_trakt_data &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_id=%d and tdata_trakt_id=%d and tdata_data_type=%d"),
		TBL_TRAKT_DATA,
		rec.getTDataID(),
		rec.getTDataTraktID(),
		rec.getTDataType());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktAssExists(CTransaction_trakt_ass &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_assort_id=%d and tass_trakt_id=%d and tass_trakt_data_id=%d and tass_data_type=%d"),
		TBL_TRAKT_SPC_ASS,
		rec.getTAssID(),
		rec.getTAssTraktID(),
		rec.getTAssTraktDataID(),
		rec.getTAssTraktDataType());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktAssExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktAssExists(int trakt_id,int trakt_data_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d and tass_trakt_data_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id,
		trakt_data_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktTransExists(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d and ttrans_from_ass_id=%d and ttrans_to_ass_id=%d and ttrans_to_spc_id=%d and ttrans_data_type=%d"),
		TBL_TRAKT_TRANS,
		rec.getTTransDataID(),
		rec.getTTransTraktID(),
		rec.getTTransFromID(),
		rec.getTTransToID(),
		rec.getSpecieID(),
		rec.getTTransDataType());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktTransExists(int trakt_data_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d"),
		TBL_TRAKT_TRANS,
		trakt_data_id,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktTransExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_id=%d"),
		TBL_TRAKT_TRANS,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktSetSpcExists(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tsetspc_id=%d and tsetspc_tdata_id=%d and tsetspc_tdata_trakt_id=%d and tsetspc_data_type=%d"),
		TBL_TRAKT_SET_SPC,
		rec.getTSetspcID(),
		rec.getTSetspcDataID(),
		rec.getTSetspcTraktID(),
		rec.getTSetspcDataType());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktMiscDataExists(int trakt_id)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tprl_trakt_id=%d"),
		TBL_TRAKT_MISC_DATA,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktMiscDataExists(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tprl_trakt_id=%d"),
		TBL_TRAKT_MISC_DATA,
		rec.getTSetTraktID());
	return exists(sSQL);
}


BOOL CUMEstimateDB::traktPlotExists(CTransaction_plot &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tplot_id=%d and tplot_trakt_id=%d"),
		TBL_TRAKT_PLOT,
		rec.getPlotID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktPlotExists(int plot_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tplot_id=%d and tplot_trakt_id=%d"),
		TBL_TRAKT_PLOT,
		plot_id,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktPlotExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tplot_trakt_id=%d"),
		TBL_TRAKT_PLOT,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktSampleTreeExists(CTransaction_sample_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_id=%d and ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktSampleTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktSampleTreeExists(long tree_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_id=%d and ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		tree_id,
		trakt_id);
	return exists(sSQL);
}


BOOL CUMEstimateDB::traktDCLSTreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_id=%d and tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLSTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLSTreeExists(long tree_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_id=%d and tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		tree_id,
		trakt_id);
	return exists(sSQL);
}


BOOL CUMEstimateDB::traktDCLS1TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLS1TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLS1TreeExists(long tree_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		tree_id,
		trakt_id);
	return exists(sSQL);

}


BOOL CUMEstimateDB::traktDCLS2TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls2_id=%d and tdcls2_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLS2TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls2_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktDCLS2TreeExists(long tree_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls2_id=%d and tdcls2_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		tree_id,
		trakt_id);
	return exists(sSQL);

}


BOOL CUMEstimateDB::traktAssTreeExists(CTransaction_tree_assort &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_id=%d and tassort_tree_trakt_id=%d and tassort_tree_dcls=%.0f"),
		TBL_TRAKT_TREES_ASSORT,
		rec.getTreeID(),
		rec.getTraktID(),
		rec.getDCLS());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktAssTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_trakt_id=%d"),
		TBL_TRAKT_TREES_ASSORT,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktRotpostExists(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where rot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST,
		rec.getRotTraktID());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktRotpostSpcExists(CTransaction_trakt_rotpost_spc &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where spcrot_id=%d and spcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_SPC,
		rec.getRotSpcID_pk(),
		rec.getRotSpcTraktID_pk());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktRotpostSpcExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where spcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_SPC,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktRotpostOtcExists(CTransaction_trakt_rotpost_other &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where otcrot_id=%d and otcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_OTC,
		rec.getOtcRotID_pk(),
		rec.getOtcRotTraktID_pk());
	return exists(sSQL);
}

BOOL CUMEstimateDB::traktRotpostOtcExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where otcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_OTC,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMEstimateDB::categoryExists(CTransaction_sample_tree_category &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_SAMPLE_TREE_CATEGORY, rec.getID());
	return exists(sSQL);
}


// Handle TraktType

// PUBLIC
BOOL CUMEstimateDB::getTraktTypes(vecTransactionTraktType &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_TRAKT_TYPE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_trakt_type(m_saCommand.Field(1).asLong(),
																						(LPCTSTR)m_saCommand.Field(2).asString(),
																						(LPCTSTR)m_saCommand.Field(3).asString(),
																						_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addTraktType(CTransaction_trakt_type &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
//		if (!traktTypeExists(rec))
		if (!traktTypeExists(rec))
		{
			sSQL.Format(_T("insert into %s (type_name,notes) values(:1,:2)"),
									TBL_TRAKT_TYPE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getTraktType();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updTraktType(CTransaction_trakt_type &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		//if (traktTypeExists(rec))
		//{

			sSQL.Format(_T("update %s set type_name=:1,notes=:2 where id=:3"),TBL_TRAKT_TYPE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.getTraktType();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeTraktType(CTransaction_trakt_type &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTypeExists(rec))
		//{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_TRAKT_TYPE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


// Handle Origin (Ursprung)

// PUBLIC
BOOL CUMEstimateDB::getOrigins(vecTransactionOrigin &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_ORIGIN);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_origin(m_saCommand.Field(1).asLong(),
																				(LPCTSTR)m_saCommand.Field(2).asString(),
																				(LPCTSTR)m_saCommand.Field(3).asString(),
																				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addOrigin(CTransaction_origin &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!originExists(rec))
		{
			sSQL.Format(_T("insert into %s (origin_name,notes) values(:1,:2)"),
									TBL_ORIGIN);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getOrigin();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updOrigin(CTransaction_origin &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (originExists(rec))
		//{

			sSQL.Format(_T("update %s set origin_name=:1,notes=:2 where id=:3"),TBL_ORIGIN);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.getOrigin();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeOrigin(CTransaction_origin &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (originExists(rec))
		//{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_ORIGIN);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Handle Input Methods

// PUBLIC
BOOL CUMEstimateDB::getInputMethods(vecTransactionInputMethod &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_INPUT_METHOD);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_input_method(m_saCommand.Field(1).asLong(),
																							(LPCTSTR)m_saCommand.Field(2).asString(),
																							(LPCTSTR)m_saCommand.Field(3).asString(),
																							_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addInputMethod(CTransaction_input_method &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!inputMethodExists(rec))
		{
			sSQL.Format(_T("insert into %s (inpdata_name,notes) values(:1,:2)"),
									TBL_INPUT_METHOD);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getInputMethod();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updInputMethod(CTransaction_input_method &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (inputMethodExists(rec))
		//{

			sSQL.Format(_T("update %s set inpdata_name=:1,notes=:2 where id=:3"),TBL_INPUT_METHOD);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.getInputMethod();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeInputMethod(CTransaction_input_method &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (inputMethodExists(rec))
		//{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_INPUT_METHOD);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


// Handle Height classes

// PUBLIC
BOOL CUMEstimateDB::getHgtClass(vecTransactionHgtClass &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_HGT_CLASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_hgt_class(m_saCommand.Field(1).asLong(),
																					 (LPCTSTR)m_saCommand.Field(2).asString(),
																					 (LPCTSTR)m_saCommand.Field(3).asString(),
																						_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addHgtClass(CTransaction_hgt_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!hgtClassExists(rec))
		{
			sSQL.Format(_T("insert into %s (hgtcls_name,notes) values(:1,:2)"),
									TBL_HGT_CLASS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getHgtClass();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updHgtClass(CTransaction_hgt_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (hgtClassExists(rec))
		//{

			sSQL.Format(_T("update %s set hgtcls_name=:1,notes=:2 where id=:3"),TBL_HGT_CLASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.getHgtClass();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeHgtClass(CTransaction_hgt_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (hgtClassExists(rec))
		//{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_HGT_CLASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


// Handle Cutting classes

// PUBLIC
BOOL CUMEstimateDB::getCutClass(vecTransactionCutClass &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_CUT_CLASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_cut_class(m_saCommand.Field(1).asLong(),
																					(LPCTSTR)m_saCommand.Field(2).asString(),
																					(LPCTSTR)m_saCommand.Field(3).asString(),
																					_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addCutClass(CTransaction_cut_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!cutClassExists(rec))
		{
			sSQL.Format(_T("insert into %s (cutcls_name,notes) values(:1,:2)"),
									TBL_CUT_CLASS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCutClass();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updCutClass(CTransaction_cut_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (cutClassExists(rec))
		//{

			sSQL.Format(_T("update %s set cutcls_name=:1,notes=:2 where id=:3"),TBL_CUT_CLASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.getCutClass();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeCutClass(CTransaction_cut_class &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (cutClassExists(rec))
		//{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_CUT_CLASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

/////////////////////////////////////////////////////////
// Trakt

BOOL CUMEstimateDB::getTraktIndex(vecTraktIndex &vec,BOOL select_all)
{
	CString sSQL,tmp;
	try
	{
		vec.clear();
		// Object select
		if (select_all)
		{
			sSQL.Format(_T("select trakt_id from %s order by trakt_id"),TBL_TRAKT);
		}
		// Stands not in Object select
		else
		{
			// Added 2009-02-12 P�D
			// This select will only add stands not in an Object. I.e. not in the
			// table "elv_cruise_table". Method by PL; 090212 p�d
			sSQL.Format(_T("select a.trakt_id from %s a where a.trakt_id not in (select b.ecru_id from %s b) order by a.trakt_id"),TBL_TRAKT,TBL_ELV_CRUISE);
		}

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back((long)m_saCommand.Field(1).asLong());
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTrakts(vecTransactionTrakt &vec)
{
	CString sSQL,tmp;
	try
	{
		vec.clear();
		
// Commented out 2009-02-12 P�D
// Select to  selecte ALL items; 090212 p�d	
// sSQL.Format(_T("select * from %s order by trakt_id"),TBL_TRAKT);

		// Added 2009-02-12 P�D
		// This select will only add stands not in an Object. I.e. not in the
		// table "elv_cruise_table". Method by PL; 090212 p�d
		sSQL.Format(_T("select a.* from %s a where a.trakt_id not in (select b.ecru_id from %s b) order by a.trakt_id"),TBL_TRAKT,TBL_ELV_CRUISE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt(m_saCommand.Field(1).asLong(),
																			 (LPCTSTR)m_saCommand.Field(2).asString(),
																			 (LPCTSTR)m_saCommand.Field(3).asString(),
																			 (LPCTSTR)m_saCommand.Field(4).asString(),
																			 (LPCTSTR)m_saCommand.Field(5).asString(),
																			 (LPCTSTR)m_saCommand.Field(6).asString(),
																				m_saCommand.Field(7).asLong(),
																				m_saCommand.Field(8).asDouble(),
																				m_saCommand.Field(9).asDouble(),
																				m_saCommand.Field(10).asDouble(),
																				m_saCommand.Field(11).asLong(),
																			 (LPCTSTR)m_saCommand.Field(12).asString(),
																				m_saCommand.Field(13).asDouble(),
																				m_saCommand.Field(14).asLong(),
																				m_saCommand.Field(15).asLong(),
																				m_saCommand.Field(16).asLong(),
																				m_saCommand.Field(17).asLong(),
																			 (LPCTSTR)m_saCommand.Field(18).asString(),
																				m_saCommand.Field(19).asLong(),
																				m_saCommand.Field(20).asLong(),
																			 (LPCTSTR)m_saCommand.Field(21).asString(),
																			 (LPCTSTR)m_saCommand.Field(22).asString(),
																			 (LPCTSTR)m_saCommand.Field(23).asString(),
																			 (LPCTSTR)m_saCommand.Field(24).asString(),
																			 (LPCTSTR)m_saCommand.Field(25).asString(),
																			 (LPCTSTR)m_saCommand.Field(26).asString(),
																				m_saCommand.Field(27).asLong(),
																				m_saCommand.Field("trakt_near_coast").asLong(),
																				m_saCommand.Field("trakt_southeast").asLong(),
																				m_saCommand.Field("trakt_region5").asLong(),
																				m_saCommand.Field("trakt_part_of_plot").asLong(),
																			 (LPCTSTR)m_saCommand.Field("trakt_si_h100_pine").asString(),
																			 (LPCTSTR)m_saCommand.Field("trakt_prop_num").asString(),
																			 (LPCTSTR)m_saCommand.Field("trakt_prop_name").asString(),
																			 (LPCTSTR)m_saCommand.Field("trakt_notes").asLongChar(),
																			 (LPCTSTR)m_saCommand.Field("trakt_created_by").asString(),
																			 (LPCTSTR)convertSADateTime(saDateTime),
																				m_saCommand.Field("trakt_wside").asLong(),
																				m_saCommand.Field("trakt_for_stand_only").asShort(),
																				m_saCommand.Field("trakt_length").asLong(),
																				m_saCommand.Field("trakt_width").asLong(),
																				m_saCommand.Field("trakt_tillfutnyttj").asBool(),
																				m_saCommand.Field(_T("trakt_point")).asString(),
																				m_saCommand.Field(_T("trakt_coordinates")).asString()
																				));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTrakt(int trakt_id,CTransaction_trakt &rec)
{
	CString sSQL,tmp;
	try
	{
		sSQL.Format(_T("select * from %s where trakt_id=:1 order by trakt_id"),TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt(m_saCommand.Field(1).asLong(),
															 (LPCTSTR)m_saCommand.Field(2).asString(),
															 (LPCTSTR)m_saCommand.Field(3).asString(),
															 (LPCTSTR)m_saCommand.Field(4).asString(),
															 (LPCTSTR)m_saCommand.Field(5).asString(),
															 (LPCTSTR)m_saCommand.Field(6).asString(),
																m_saCommand.Field(7).asLong(),
																m_saCommand.Field(8).asDouble(),
																m_saCommand.Field(9).asDouble(),
																m_saCommand.Field(10).asDouble(),
																m_saCommand.Field(11).asLong(),
															 (LPCTSTR)m_saCommand.Field(12).asString(),
																m_saCommand.Field(13).asDouble(),
																m_saCommand.Field(14).asLong(),
																m_saCommand.Field(15).asLong(),
																m_saCommand.Field(16).asLong(),
																m_saCommand.Field(17).asLong(),
															 (LPCTSTR)m_saCommand.Field(18).asString(),
																m_saCommand.Field(19).asLong(),
																m_saCommand.Field(20).asLong(),
															 (LPCTSTR)m_saCommand.Field(21).asString(),
															 (LPCTSTR)m_saCommand.Field(22).asString(),
															 (LPCTSTR)m_saCommand.Field(23).asString(),
															 (LPCTSTR)m_saCommand.Field(24).asString(),
															 (LPCTSTR)m_saCommand.Field(25).asString(),
															 (LPCTSTR)m_saCommand.Field(26).asString(),
																m_saCommand.Field(27).asLong(),
																m_saCommand.Field("trakt_near_coast").asLong(),
																m_saCommand.Field("trakt_southeast").asLong(),
																m_saCommand.Field("trakt_region5").asLong(),
																m_saCommand.Field("trakt_part_of_plot").asLong(),
															 (LPCTSTR)m_saCommand.Field("trakt_si_h100_pine").asString(),
															 (LPCTSTR)m_saCommand.Field("trakt_prop_num").asString(),
															 (LPCTSTR)m_saCommand.Field("trakt_prop_name").asString(),
															 (LPCTSTR)m_saCommand.Field("trakt_notes").asLongChar(),
															 (LPCTSTR)m_saCommand.Field("trakt_created_by").asString(),
															 (LPCTSTR)convertSADateTime(saDateTime),
															 m_saCommand.Field("trakt_wside").asLong(),
															 m_saCommand.Field("trakt_for_stand_only").asShort(),
															 m_saCommand.Field("trakt_length").asLong(),
															 m_saCommand.Field("trakt_width").asLong(),
															 m_saCommand.Field("trakt_tillfutnyttj").asBool(),
															 m_saCommand.Field(_T("trakt_point")).asString(),
															 m_saCommand.Field(_T("trakt_coordinates")).asString()
															 );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::addTrakt(CTransaction_trakt &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktExists(rec))
		{
			sSQL.Format(_T("insert into %s (trakt_num,trakt_pnum,trakt_name,trakt_type,trakt_date,")
									_T("trakt_prop_id,trakt_areal,trakt_areal_consider,trakt_areal_handled,")
									_T("trakt_age,trakt_si_h100,trakt_withdraw,trakt_hgt_over_sea,")
									_T("trakt_ground,trakt_surface,trakt_rake,trakt_mapdata,trakt_latitude,")
									_T("trakt_longitude,trakt_x_coord,trakt_y_coord,trakt_origin,trakt_inp_method,")
									_T("trakt_hgt_class,trakt_cut_class,trakt_sim_data,trakt_near_coast,trakt_southeast,")
									_T("trakt_region5,trakt_part_of_plot,trakt_si_h100_pine,trakt_prop_num,trakt_prop_name,")
									_T("trakt_notes,trakt_created_by,trakt_wside,trakt_for_stand_only,trakt_length,trakt_width,trakt_tillfutnyttj) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,")
									_T(":20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40)"),TBL_TRAKT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getTraktNum();
			m_saCommand.Param(2).setAsString()	= rec.getTraktPNum();
			m_saCommand.Param(3).setAsString()	= rec.getTraktName();
			m_saCommand.Param(4).setAsString()	= rec.getTraktType();
			m_saCommand.Param(5).setAsString()	= rec.getTraktDate();
			m_saCommand.Param(6).setAsLong()		= rec.getTraktPropID();
			m_saCommand.Param(7).setAsDouble()	= rec.getTraktAreal();
			m_saCommand.Param(8).setAsDouble()	= rec.getTraktArealConsiderd();
			m_saCommand.Param(9).setAsDouble()	= rec.getTraktArealHandled();
			m_saCommand.Param(10).setAsLong()	= rec.getTraktAge();
			m_saCommand.Param(11).setAsString()	= rec.getTraktSIH100();
			m_saCommand.Param(12).setAsDouble()	= rec.getTraktWithdraw();
			m_saCommand.Param(13).setAsLong()	= rec.getTraktHgtOverSea();
			m_saCommand.Param(14).setAsLong()	= rec.getTraktGround();
			m_saCommand.Param(15).setAsLong()	= rec.getTraktSurface();
			m_saCommand.Param(16).setAsLong()	= rec.getTraktRake();
			m_saCommand.Param(17).setAsString()	= rec.getTraktMapdata();
			m_saCommand.Param(18).setAsLong()	= rec.getTraktLatitude();
			m_saCommand.Param(19).setAsLong()	= rec.getTraktLongitude();
			m_saCommand.Param(20).setAsString()	= rec.getTraktXCoord();
			m_saCommand.Param(21).setAsString()	= rec.getTraktYCoord();
			m_saCommand.Param(22).setAsString()	= rec.getTraktOrigin();
			m_saCommand.Param(23).setAsString()	= rec.getTraktInpMethod();
			m_saCommand.Param(24).setAsString()	= rec.getTraktHgtClass();
			m_saCommand.Param(25).setAsString()	= rec.getTraktCutClass();
			m_saCommand.Param(26).setAsLong()	= (rec.getTraktSimData() ? 1 : 0);
			m_saCommand.Param(27).setAsLong()	= (rec.getTraktNearCoast() ? 1 : 0);
			m_saCommand.Param(28).setAsLong()	= (rec.getTraktSoutheast() ? 1 : 0);
			m_saCommand.Param(29).setAsLong()	= (rec.getTraktRegion5() ? 1 : 0);
			m_saCommand.Param(30).setAsLong()	= (rec.getTraktPartOfPlot() ? 1 : 0);
			m_saCommand.Param(31).setAsString()	= rec.getTraktSIH100_Pine();
			m_saCommand.Param(32).setAsString()	= rec.getTraktPropNum();
			m_saCommand.Param(33).setAsString()	= rec.getTraktPropName();
			m_saCommand.Param(34).setAsLongChar()	= rec.getTraktNotes();
			m_saCommand.Param(35).setAsString()	= rec.getTraktCreatedBy();
			m_saCommand.Param(36).setAsShort()	= rec.getWSide();	// Added 080922 p�d
			m_saCommand.Param(37).setAsShort()	= rec.getOnlyForStand();	// Added 100626 p�d

			m_saCommand.Param(38).setAsLong()	= rec.getTraktLength();	// #4205 150113 J�
			m_saCommand.Param(39).setAsLong()	= rec.getTraktWidth();	// #4205 150113 J�
			m_saCommand.Param(40).setAsBool()	= rec.getTraktTillfUtnyttj();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CUMEstimateDB::addTrakt_Soderbergs_Specifics(int trakt_id,BOOL at_coast,BOOL south_east,BOOL region_5,BOOL part_of_plot,LPCTSTR si_h100_pine)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktExists(trakt_id))
		{
			sSQL.Format(_T("insert into %s (trakt_near_coast,trakt_southeast,trakt_region5,trakt_part_of_plot,trakt_si_h100_pine) values(:1,:2,:3,:4,:5)"),TBL_TRAKT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsShort()		= (at_coast ? 1 : 0);
			m_saCommand.Param(2).setAsShort()		= (south_east ? 1 : 0);
			m_saCommand.Param(3).setAsShort()		= (region_5 ? 1 : 0);
			m_saCommand.Param(4).setAsShort()		= (part_of_plot ? 1 : 0);
			m_saCommand.Param(5).setAsString()	= si_h100_pine;

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updTrakt(CTransaction_trakt &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("update %s set trakt_num=:1,trakt_pnum=:2,trakt_name=:3,trakt_type=:4,trakt_date=:5,")
									_T("trakt_prop_id=:6,trakt_areal=:7,trakt_areal_consider=:8,trakt_areal_handled=:9,")
									_T("trakt_age=:10,trakt_si_h100=:11,trakt_withdraw=:12,trakt_hgt_over_sea=:13,")
									_T("trakt_ground=:14,trakt_surface=:15,trakt_rake=:16,trakt_mapdata=:17,trakt_latitude=:18,")
									_T("trakt_longitude=:19,trakt_x_coord=:20,trakt_y_coord=:21,trakt_origin=:22,trakt_inp_method=:23,")
									_T("trakt_hgt_class=:24,trakt_cut_class=:25,trakt_sim_data=:26,trakt_near_coast=:27,trakt_southeast=:28,")
									_T("trakt_region5=:29,trakt_part_of_plot=:30,trakt_si_h100_pine=:31,trakt_prop_num=:32,trakt_prop_name=:33,")
									_T("trakt_notes=:34,trakt_created_by=:35,trakt_wside=:36,trakt_for_stand_only=:37,trakt_length=:38,trakt_width=:39,trakt_tillfutnyttj=:40,created=GETDATE() ")
									_T("where trakt_id=:41"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getTraktNum();
			m_saCommand.Param(2).setAsString()	= rec.getTraktPNum();
			m_saCommand.Param(3).setAsString()	= rec.getTraktName();
			m_saCommand.Param(4).setAsString()	= rec.getTraktType();
			m_saCommand.Param(5).setAsString()	= rec.getTraktDate();
			m_saCommand.Param(6).setAsLong()		= rec.getTraktPropID();
			m_saCommand.Param(7).setAsDouble()	= rec.getTraktAreal();
			m_saCommand.Param(8).setAsDouble()	= rec.getTraktArealConsiderd();
			m_saCommand.Param(9).setAsDouble()	= rec.getTraktArealHandled();
			m_saCommand.Param(10).setAsLong()	= rec.getTraktAge();
			m_saCommand.Param(11).setAsString()	= rec.getTraktSIH100();
			m_saCommand.Param(12).setAsDouble()	= rec.getTraktWithdraw();
			m_saCommand.Param(13).setAsLong()	= rec.getTraktHgtOverSea();
			m_saCommand.Param(14).setAsLong()	= rec.getTraktGround();
			m_saCommand.Param(15).setAsLong()	= rec.getTraktSurface();
			m_saCommand.Param(16).setAsLong()	= rec.getTraktRake();
			m_saCommand.Param(17).setAsString()	= rec.getTraktMapdata();
			m_saCommand.Param(18).setAsLong()	= rec.getTraktLatitude();
			m_saCommand.Param(19).setAsLong()	= rec.getTraktLongitude();
			m_saCommand.Param(20).setAsString()	= rec.getTraktXCoord();
			m_saCommand.Param(21).setAsString()	= rec.getTraktYCoord();
			m_saCommand.Param(22).setAsString()	= rec.getTraktOrigin();
			m_saCommand.Param(23).setAsString()	= rec.getTraktInpMethod();
			m_saCommand.Param(24).setAsString()	= rec.getTraktHgtClass();
			m_saCommand.Param(25).setAsString()	= rec.getTraktCutClass();
			m_saCommand.Param(26).setAsLong()	= (rec.getTraktSimData() ? 1 : 0);
			m_saCommand.Param(27).setAsLong()	= (rec.getTraktNearCoast() ? 1 : 0);
			m_saCommand.Param(28).setAsLong()	= (rec.getTraktSoutheast() ? 1 : 0);
			m_saCommand.Param(29).setAsLong()	= (rec.getTraktRegion5() ? 1 : 0);
			m_saCommand.Param(30).setAsLong()	= (rec.getTraktPartOfPlot() ? 1 : 0);
			m_saCommand.Param(31).setAsString()	= rec.getTraktSIH100_Pine();
			m_saCommand.Param(32).setAsString()	= rec.getTraktPropNum();
			m_saCommand.Param(33).setAsString()	= rec.getTraktPropName();
			m_saCommand.Param(34).setAsLongChar()	= rec.getTraktNotes();
			m_saCommand.Param(35).setAsString()	= rec.getTraktCreatedBy();
			m_saCommand.Param(36).setAsShort()	= rec.getWSide();	// Added 080922 p�d
			m_saCommand.Param(37).setAsShort()	= rec.getOnlyForStand();	// Added 100526 p�d
			m_saCommand.Param(38).setAsLong()	= rec.getTraktLength();	// #4205 150113 J�
			m_saCommand.Param(39).setAsLong()	= rec.getTraktWidth();	// #4205 150113 J�
			m_saCommand.Param(40).setAsBool()	= rec.getTraktTillfUtnyttj();	//HMS-44 190327 J�
			m_saCommand.Param(41).setAsLong()		= rec.getTraktID();


			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTrakt_Soderbergs_Specifics(int trakt_id,BOOL at_coast,BOOL south_east,BOOL region_5,BOOL part_of_plot,LPCTSTR si_h100_pine)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("update %s set trakt_near_coast=:1,trakt_southeast=:2,trakt_region5=:3,trakt_part_of_plot=:4,trakt_si_h100_pine=:5,created=GETDATE() where trakt_id=:6"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsShort()		= (at_coast ? 1 : 0);
			m_saCommand.Param(2).setAsShort()		= (south_east ? 1 : 0);
			m_saCommand.Param(3).setAsShort()		= (region_5 ? 1 : 0);
			m_saCommand.Param(4).setAsShort()		= (part_of_plot ? 1 : 0);
			m_saCommand.Param(5).setAsString()	= si_h100_pine;

			m_saCommand.Param(6).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTrakt(CTransaction_trakt &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("delete from %s where trakt_id=:1"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getTraktID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

// OBS! THIS FUNCTION IS USED ONLY ON AN EMPTY TABLE; 070302 p�d
// Resets the IDENTITY field to 0, i.e. first id = 1; 070302 p�d
BOOL CUMEstimateDB::resetTraktIdentityField(void)
{
	CString sSQL;
	try
	{
		if (getConnTypeNumber() == 1)	// SQL Server
		{
			sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 1)"),TBL_TRAKT);
		}
		else if (getConnTypeNumber() == 2)	// MySQL
		{
			sSQL.Format(_T("alter table %s auto_increment=1"),TBL_TRAKT);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

int CUMEstimateDB::getNumOfRecordsInTrakt(void)
{
	return num_of_records(TBL_TRAKT);
}

// Added 070903 p�d
int CUMEstimateDB::getLastTraktIdentity(void)
{
	return last_identity(TBL_TRAKT,_T("trakt_id"));
}

// Information
int CUMEstimateDB::getNumOfPlotsInTrakt(int trakt_id)
{
	std::vector<int> vecInt;
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktPlotExists(trakt_id))
		//{
			sSQL.Format(_T("select distinct ttree_plot_id from %s where ttree_trakt_id=%d"),
									TBL_TRAKT_SAMPLE_TREES,
									trakt_id);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();	

			while(m_saCommand.FetchNext())
			{
				vecInt.push_back(m_saCommand.Field(1).asLong());
			}
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return 1;
	}

	if (vecInt.size() > 0)
		return (int)vecInt.size();
	else
		return 1;
}


BOOL CUMEstimateDB::removePropTraktConnection(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktExists(trakt_id))
		//{

			sSQL.Format(_T("update %s set trakt_prop_id=:1,trakt_prop_num=:2,trakt_prop_name=:3 where trakt_id=:4"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= -1;	// No Property
			m_saCommand.Param(2).setAsString()	= _T("");
			m_saCommand.Param(3).setAsString()	= _T("");

			m_saCommand.Param(4).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}


/////////////////////////////////////////////////////////
// Trakt data
BOOL CUMEstimateDB::getTraktData(vecTransactionTraktData &vec,int trakt_id,int data_type)
{
	CString sSQL,S;
	try
	{
		vec.clear();
		// If trakt_id	> -1, use trakt_id to select
		// a specific trakt data; 070309 p�d
		if (trakt_id == -1)
			sSQL.Format(_T("select * from %s order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA);
		else if (trakt_id > -1 && data_type == -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id);
		else if (trakt_id > -1 && data_type > -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d and tdata_data_type=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id,data_type);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_data(m_saCommand.Field(1).asLong(),
																						m_saCommand.Field(2).asLong(),
																						m_saCommand.Field(3).asShort(),
																						m_saCommand.Field(4).asLong(),
																						(LPCTSTR)m_saCommand.Field(5).asString(),
																						m_saCommand.Field(6).asDouble(),
																						m_saCommand.Field(7).asLong(),
																						m_saCommand.Field(8).asDouble(),
																						m_saCommand.Field(9).asDouble(),
																						m_saCommand.Field(10).asDouble(),
																						m_saCommand.Field(11).asDouble(),
																						m_saCommand.Field(12).asDouble(),
																						m_saCommand.Field(13).asDouble(),
																						m_saCommand.Field(14).asDouble(),
																						m_saCommand.Field(22).asDouble(),
																						m_saCommand.Field(15).asDouble(),
																						m_saCommand.Field(16).asDouble(),
																						m_saCommand.Field(17).asDouble(),
																						m_saCommand.Field(18).asDouble(),
																						m_saCommand.Field(19).asDouble(),
																						m_saCommand.Field(20).asDouble(),
																						m_saCommand.Field(23).asDouble(),
																						(LPCTSTR)convertSADateTime(saDateTime)));;

		}
		doCommit();

//		S.Format(_T("CUMEstimateDB::getTraktData\nvec.size() %d"),vec.size());
//		UMMessageBox(S);
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktData(CTransaction_trakt_data &rec,int trakt_id,int spc_id,int data_type)
{
	CString sSQL;
	try
	{
		// If trakt_id	> -1, use trakt_id to select
		// a specific trakt data; 070309 p�d
		if (trakt_id == -1 || spc_id == -1 || data_type == -1)
			return FALSE;

		sSQL.Format(_T("select * from %s where tdata_trakt_id=%d and tdata_spc_id=%d and tdata_data_type=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,
								trakt_id,
								spc_id,
								data_type);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = (CTransaction_trakt_data(m_saCommand.Field(1).asLong(),
						 												 m_saCommand.Field(2).asLong(),
																		 m_saCommand.Field(3).asShort(),
 																		 m_saCommand.Field(4).asLong(),
																		 (LPCTSTR)m_saCommand.Field(5).asString(),
																		 m_saCommand.Field(6).asDouble(),
																		 m_saCommand.Field(7).asLong(),
																		 m_saCommand.Field(8).asDouble(),
																		 m_saCommand.Field(9).asDouble(),
																		 m_saCommand.Field(10).asDouble(),
																		 m_saCommand.Field(11).asDouble(),
																		 m_saCommand.Field(12).asDouble(),
																		 m_saCommand.Field(13).asDouble(),
																		 m_saCommand.Field(14).asDouble(),
																		 m_saCommand.Field(22).asDouble(),
																		 m_saCommand.Field(15).asDouble(),
																		 m_saCommand.Field(16).asDouble(),
																		 m_saCommand.Field(17).asDouble(),
																		 m_saCommand.Field(18).asDouble(),
																		 m_saCommand.Field(19).asDouble(),
																		 m_saCommand.Field(20).asDouble(),
																		 m_saCommand.Field(23).asDouble(),
																		(LPCTSTR)convertSADateTime(saDateTime) ));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tdata_id,tdata_trakt_id,tdata_data_type,tdata_spc_id,tdata_spc_name,tdata_percent,tdata_numof,")
									_T("tdata_da,tdata_dg,tdata_dgv,tdata_hgv,tdata_gy,tdata_avg_hgt,")
									_T("tdata_h25,tdata_m3sk_vol,tdata_m3fub_vol,tdata_m3ub_vol,")
									_T("tdata_avg_m3sk_vol,tdata_avg_m3fub_vol,tdata_avg_m3ub_vol,tdata_gcrown,tdata_grot) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22)"),
											TBL_TRAKT_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTDataTraktID();
			m_saCommand.Param(3).setAsShort()		= rec.getTDataType();
			m_saCommand.Param(4).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(5).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(7).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()	= rec.getDA();
			m_saCommand.Param(9).setAsDouble()	= rec.getDG();
			m_saCommand.Param(10).setAsDouble()	= rec.getDGV();
			m_saCommand.Param(11).setAsDouble()	= rec.getHGV();
			m_saCommand.Param(12).setAsDouble()	= rec.getGY();
			m_saCommand.Param(13).setAsDouble()	= rec.getAvgHgt();
			m_saCommand.Param(14).setAsDouble()	= rec.getH25();
			m_saCommand.Param(15).setAsDouble()	= rec.getM3SK();
			m_saCommand.Param(16).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(17).setAsDouble()	= rec.getM3UB();
			m_saCommand.Param(18).setAsDouble()	= rec.getAvgM3SK();
			m_saCommand.Param(19).setAsDouble()	= rec.getAvgM3FUB();
			m_saCommand.Param(20).setAsDouble()	= rec.getAvgM3UB();
			m_saCommand.Param(21).setAsDouble()	= rec.getGreenCrown();
			m_saCommand.Param(22).setAsDouble()	= rec.getGrot();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_data rec1;
	//getTraktData(rec1,rec.getTDataTraktID(),rec.getSpecieID(),rec.getTDataType());	// Get original record, to compare with; 090303 p�d
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktDataExists(rec))
		//{

			sSQL.Format(_T("update %s set tdata_spc_name=:1,tdata_percent=:2,tdata_numof=:3,")
									_T("tdata_da=:4,tdata_dg=:5,tdata_dgv=:6,tdata_hgv=:7,tdata_gy=:8,tdata_avg_hgt=:9,")
									_T("tdata_h25=:10,tdata_m3sk_vol=:11,tdata_m3fub_vol=:12,tdata_m3ub_vol=:13,")
									_T("tdata_avg_m3sk_vol=:14,tdata_avg_m3fub_vol=:15,tdata_avg_m3ub_vol=:16,tdata_gcrown=:17,tdata_grot=:18,created=GETDATE() ")
									_T("where tdata_id=:19 and tdata_trakt_id=:20 and tdata_data_type=:21 and tdata_spc_id=:22"),
											TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(2).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(3).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(4).setAsDouble()	= rec.getDA();
			m_saCommand.Param(5).setAsDouble()	= rec.getDG();
			m_saCommand.Param(6).setAsDouble()	= rec.getDGV();
			m_saCommand.Param(7).setAsDouble()	= rec.getHGV();
			m_saCommand.Param(8).setAsDouble()	= rec.getGY();
			m_saCommand.Param(9).setAsDouble()	= rec.getAvgHgt();
			m_saCommand.Param(10).setAsDouble()	= rec.getH25();
			m_saCommand.Param(11).setAsDouble()	= rec.getM3SK();
			m_saCommand.Param(12).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(13).setAsDouble()	= rec.getM3UB();
			m_saCommand.Param(14).setAsDouble()	= rec.getAvgM3SK();
			m_saCommand.Param(15).setAsDouble()	= rec.getAvgM3FUB();
			m_saCommand.Param(16).setAsDouble()	= rec.getAvgM3UB();
			m_saCommand.Param(17).setAsDouble()	= rec.getGreenCrown();
			m_saCommand.Param(18).setAsDouble()	= rec.getGrot();

			m_saCommand.Param(19).setAsLong()		= rec.getTDataID();
			m_saCommand.Param(20).setAsLong()		= rec.getTDataTraktID();
			m_saCommand.Param(21).setAsShort()	= rec.getTDataType();
			m_saCommand.Param(22).setAsLong()		= rec.getSpecieID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktData_m3fub(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	CTransaction_trakt_data rec1;
	try
	{
		//if (traktDataExists(rec))
		//{

			sSQL.Format(_T("update %s set tdata_m3fub_vol=:1,tdata_avg_m3fub_vol=:2,created=GETDATE() where tdata_id=:3 and tdata_trakt_id=:4 and tdata_data_type=:5 and tdata_spc_id=:6"),
										TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(2).setAsDouble()	= rec.getAvgM3FUB();

			m_saCommand.Param(3).setAsLong()	= rec.getTDataID();
			m_saCommand.Param(4).setAsLong()	= rec.getTDataTraktID();
			m_saCommand.Param(5).setAsShort()	= rec.getTDataType();
			m_saCommand.Param(6).setAsLong()	= rec.getSpecieID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::resetTraktData(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDataExists(trakt_id))
		//{
/*
			sSQL.Format(_T("update %s set tdata_percent=:1,tdata_numof=:2,tdata_da=:3,tdata_dg=:4,tdata_dgv=:5,tdata_hgv=:6,\
																 tdata_gy=:7,tdata_avg_hgt=:8,tdata_h25=:9,\
																 tdata_m3sk_vol=:10,tdata_m3fub_vol=:11,tdata_m3ub_vol=:12,\
																 tdata_avg_m3sk_vol=:13,tdata_avg_m3fub_vol=:14,tdata_avg_m3ub_vol=:15,tdata_gcrown=:16 \
																 where tdata_trakt_id=:17"),
																 TBL_TRAKT_DATA);
*/
			sSQL.Format(_T("update %s set tdata_percent=:1,tdata_numof=:2,tdata_da=:3,tdata_dg=:4,tdata_dgv=:5,tdata_hgv=:6,tdata_gy=:7,tdata_avg_hgt=:8,tdata_m3sk_vol=:9,")
									_T("tdata_m3fub_vol=:10,tdata_m3ub_vol=:11,tdata_avg_m3sk_vol=:12,tdata_avg_m3fub_vol=:13,tdata_avg_m3ub_vol=:14 where tdata_trakt_id=:15"),
										TBL_TRAKT_DATA);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= 0.0;
			m_saCommand.Param(2).setAsLong()		= 0;
			m_saCommand.Param(3).setAsDouble()	= 0.0;
			m_saCommand.Param(4).setAsDouble()	= 0.0;
			m_saCommand.Param(5).setAsDouble()	= 0.0;
			m_saCommand.Param(6).setAsDouble()	= 0.0;
			m_saCommand.Param(7).setAsDouble()	= 0.0;
			m_saCommand.Param(8).setAsDouble()	= 0.0;
			m_saCommand.Param(9).setAsDouble()	= 0.0;
			m_saCommand.Param(10).setAsDouble()	= 0.0;
			m_saCommand.Param(11).setAsDouble()	= 0.0;
			m_saCommand.Param(12).setAsDouble()	= 0.0;
			m_saCommand.Param(13).setAsDouble()	= 0.0;
			m_saCommand.Param(14).setAsDouble()	= 0.0;

			m_saCommand.Param(15).setAsLong()	= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktData(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDataExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdata_trakt_id=:1"),TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDataExists(rec))
		//{

			sSQL.Format(_T("delete from %s where tdata_id=:1 and tdata_trakt_id=:2"),TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getTDataID();
			m_saCommand.Param(2).setAsLong() = rec.getTDataTraktID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktDataSpecie(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDataExists(rec))
		//{

			sSQL.Format(_T("delete from %s where tdata_id=:1 and tdata_trakt_id=:2 and tdata_spc_id=:3"),TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getTDataID();
			m_saCommand.Param(2).setAsLong() = rec.getTDataTraktID();
			m_saCommand.Param(3).setAsLong() = rec.getSpecieID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktDataSpecies(int trakt_id,LPCTSTR sql)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDataExists(trakt_id))
		//{
			sSQL.Format(_T("delete from %s %s"),TBL_TRAKT_DATA,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CUMEstimateDB::getNumOfRecordsInTraktData(void)
{
	return num_of_records(TBL_TRAKT_DATA);
}


/////////////////////////////////////////////////////////
// Trakt assort
BOOL CUMEstimateDB::getTraktAss(vecTransactionTraktAss &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asShort(),
																					(LPCTSTR)m_saCommand.Field(5).asString(),
																					 m_saCommand.Field(6).asDouble(),
																					 m_saCommand.Field(7).asDouble(),
																					 m_saCommand.Field(8).asDouble(),
																					 m_saCommand.Field(9).asDouble(),
																					 m_saCommand.Field(10).asDouble(),
																					 m_saCommand.Field(11).asDouble(),
																					 (LPCTSTR)convertSADateTime(saDateTime),
																					 m_saCommand.Field(13).asDouble()));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktAss(vecTransactionTraktAss &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where tass_trakt_id=:1 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asShort(),
																					(LPCTSTR)m_saCommand.Field(5).asString(),
																					 m_saCommand.Field(6).asDouble(),
																					 m_saCommand.Field(7).asDouble(),
																					 m_saCommand.Field(8).asDouble(),
																					 m_saCommand.Field(9).asDouble(),
																					 m_saCommand.Field(10).asDouble(),
																					 m_saCommand.Field(11).asDouble(),
																					(LPCTSTR)convertSADateTime(saDateTime),
																					 m_saCommand.Field(13).asDouble()));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktAss(CTransaction_trakt_ass &rec,CTransaction_trakt_ass rec1)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tass_assort_id=:1 and tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_data_type=:4 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec1.getTAssID();
		m_saCommand.Param(2).setAsLong()		= rec1.getTAssTraktID();
		m_saCommand.Param(3).setAsLong()		= rec1.getTAssTraktDataID();
		m_saCommand.Param(4).setAsLong()		= rec1.getTAssTraktDataType();

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																	 m_saCommand.Field(2).asLong(),
																	 m_saCommand.Field(3).asLong(),
																	 m_saCommand.Field(4).asShort(),
																	(LPCTSTR)m_saCommand.Field(5).asString(),
																	 m_saCommand.Field(6).asDouble(),
																	 m_saCommand.Field(7).asDouble(),
																	 m_saCommand.Field(8).asDouble(),
																	 m_saCommand.Field(9).asDouble(),
																	 m_saCommand.Field(10).asDouble(),
																	 m_saCommand.Field(11).asDouble(),
																	 (LPCTSTR)convertSADateTime(saDateTime),
						 										 	 m_saCommand.Field(13).asDouble());

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktAss(CTransaction_trakt_ass &rec,int trakt_id,int trakt_data_id,int data_type,int assort_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tass_assort_id=:1 and tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_data_type=:4 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= assort_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Param(3).setAsLong()		= trakt_data_id;
		m_saCommand.Param(4).setAsLong()		= data_type;

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																	 m_saCommand.Field(2).asLong(),
																	 m_saCommand.Field(3).asLong(),
																	 m_saCommand.Field(4).asShort(),
																	(LPCTSTR)m_saCommand.Field(5).asString(),
																	 m_saCommand.Field(6).asDouble(),
																	 m_saCommand.Field(7).asDouble(),
																	 m_saCommand.Field(8).asDouble(),
																	 m_saCommand.Field(9).asDouble(),
																	 m_saCommand.Field(10).asDouble(),
																	 m_saCommand.Field(11).asDouble(),
																	 (LPCTSTR)convertSADateTime(saDateTime),
						 										 	 m_saCommand.Field(13).asDouble());

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktAss_exclude(vecTransactionTraktAss &vec,int spc_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where tass_trakt_data_id <> :1 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),TBL_TRAKT_SPC_ASS);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= spc_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asShort(),
																					(LPCTSTR)m_saCommand.Field(5).asString(),
																					 m_saCommand.Field(6).asDouble(),
																					 m_saCommand.Field(7).asDouble(),
																					 m_saCommand.Field(8).asDouble(),
																					 m_saCommand.Field(9).asDouble(),
																					 m_saCommand.Field(10).asDouble(),
																					 m_saCommand.Field(11).asDouble(),
																					 (LPCTSTR)convertSADateTime(saDateTime),
																					 	m_saCommand.Field(13).asDouble()));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktAssExists(rec))
		{
			sSQL.Format(_T("insert into %s (tass_assort_id,tass_trakt_id,tass_trakt_data_id,tass_data_type,tass_assort_name,tass_price_m3fub,tass_price_m3to,")
									_T("tass_m3fub,tass_m3to,tass_m3fub_value,tass_m3to_value,tass_kr_m3_value) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),
									TBL_TRAKT_SPC_ASS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTAssID();
			m_saCommand.Param(2).setAsLong()		= rec.getTAssTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTAssTraktDataID();
			m_saCommand.Param(4).setAsShort()		= rec.getTAssTraktDataType();
			m_saCommand.Param(5).setAsString()	= rec.getTAssName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPriceM3FUB();
			m_saCommand.Param(7).setAsDouble()	= rec.getPriceM3TO();
			m_saCommand.Param(8).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(9).setAsDouble()	= rec.getM3TO();
			m_saCommand.Param(10).setAsDouble()	= rec.getValueM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getValueM3TO();
			m_saCommand.Param(12).setAsDouble()	= rec.getKrPerM3();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_ass rec1;
	//getTraktAss(rec1,rec);	// Get original record, to compare with; 090303 p�d
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		//if (traktAssExists(rec))
		//{
/*
			S.Format(_T("updTraktAss\nTAssName() %s\nPriceM3FUB() %f\nPriceM3TO() %f\nM3FUB() %f\nM3TO() %f\nValueM3FUB() %f\nValueM3TO() %f\nKrPerM3() %f"),
			rec.getTAssName(),rec.getPriceM3FUB(),rec.getPriceM3TO(),rec.getM3FUB(),rec.getM3TO(),rec.getValueM3FUB(),rec.getValueM3TO(),rec.getKrPerM3());
			UMMessageBox(S);
*/
			sSQL.Format(_T("update %s set tass_assort_name=:1,tass_price_m3fub=:2,tass_price_m3to=:3,tass_m3fub=:4,tass_m3to=:5,tass_m3fub_value=:6,tass_m3to_value=:7,tass_kr_m3_value=:8,created=GETDATE() ")
									_T("where tass_assort_id=:9 and tass_trakt_id=:10 and tass_trakt_data_id=:11 and tass_data_type=:12"),TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getTAssName();
			m_saCommand.Param(2).setAsDouble()	= rec.getPriceM3FUB();
			m_saCommand.Param(3).setAsDouble()	= rec.getPriceM3TO();
			m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(5).setAsDouble()	= rec.getM3TO();
			m_saCommand.Param(6).setAsDouble()	= rec.getValueM3FUB();
			m_saCommand.Param(7).setAsDouble()	= rec.getValueM3TO();
			m_saCommand.Param(8).setAsDouble()	= rec.getKrPerM3();

			m_saCommand.Param(9).setAsLong()		= rec.getTAssID();
			m_saCommand.Param(10).setAsLong()		= rec.getTAssTraktID();
			m_saCommand.Param(11).setAsLong()		= rec.getTAssTraktDataID();
			m_saCommand.Param(12).setAsShort()	= rec.getTAssTraktDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}
/*
BOOL CUMEstimateDB::updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price,int price_in)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	CTransaction_trakt_ass rec1;
	getTraktAss(rec1,trakt_id,trakt_data_id,data_type,assort_id);	// Get original record, to compare with; 090303 p�d
	CTransaction_trakt_ass rec2(rec1);
	if (price_in == 0)
		rec2.setPriceM3TO(exch_price);
	else if (price_in == 3)
		rec2.setPriceM3FUB(exch_price);
	if (rec1 == rec2) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		if (traktAssExists(trakt_id,trakt_data_id))
		{
			if (price_in == 0)	// M3TO
			{
				sSQL.Format(_T("update %s set tass_price_m3to=:1,tass_price_m3fub=:2,created=GETDATE() \
										where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),
										TBL_TRAKT_SPC_ASS);
			}
			else if (price_in == 3)	// M3FUB
			{
				sSQL.Format(_T("update %s set tass_price_m3fub=:1,tass_price_m3to=:2,created=GETDATE() \
										where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),
										TBL_TRAKT_SPC_ASS);
			}
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= exch_price;
			m_saCommand.Param(2).setAsDouble()	= 0.0;

			m_saCommand.Param(3).setAsLong()	= trakt_id;
			m_saCommand.Param(4).setAsLong()	= trakt_data_id;
			m_saCommand.Param(5).setAsLong()	= assort_id;
			m_saCommand.Param(6).setAsLong()	= data_type;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &)
	{
		 // print error message
//		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}
*/

BOOL CUMEstimateDB::updTraktAss_kr_per_m3(int trakt_id,int trakt_data_id,int data_type,int assort_id,
																					double m3fub_value,double m3to_value,double kr_per_m3)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;

	try
	{
		//if (traktAssExists(trakt_id,trakt_data_id))
		//{

//			S.Format(_T("updTraktAss_kr_per_m3\nm3fub_value %f\nm3to_value %f\nkr_per_m3 %f"),m3fub_value,m3to_value,kr_per_m3);
//			UMMessageBox(S);

			sSQL.Format(_T("update %s set tass_m3fub_value=:1,tass_m3to_value=:2,tass_kr_m3_value=:3,created=GETDATE() ")
									_T("where tass_trakt_id=:4 and tass_trakt_data_id=:5 and tass_assort_id=:6 and tass_data_type=:7"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()= m3fub_value;
			m_saCommand.Param(2).setAsDouble()= m3to_value;
			m_saCommand.Param(3).setAsDouble()= kr_per_m3;

			m_saCommand.Param(4).setAsLong()	= trakt_id;
			m_saCommand.Param(5).setAsLong()	= trakt_data_id;
			m_saCommand.Param(6).setAsLong()	= assort_id;
			m_saCommand.Param(7).setAsShort()	= data_type;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

// "R�kna upp/ned v�rdet per sortiment och tr�dslag"; 080611 p�d
BOOL CUMEstimateDB::recalcTraktAss_on_k3_per_m3(int trakt_id)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tass_m3fub_value=tass_m3fub_value+(tass_m3fub*tass_kr_m3_value),tass_m3to_value=tass_m3to_value+(tass_m3to*tass_kr_m3_value) where tass_trakt_id=:1"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::resetTraktAss(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tass_m3fub=:1,tass_m3to=:2,tass_m3fub_value=:3,tass_m3to_value=:4 where tass_trakt_id=:5"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= 0.0;
			m_saCommand.Param(2).setAsDouble()	= 0.0;
			m_saCommand.Param(3).setAsDouble()	= 0.0;
			m_saCommand.Param(4).setAsDouble()	= 0.0;

			m_saCommand.Param(5).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssExists(rec))
		//{
	
			sSQL.Format(_T("delete from %s where tass_assort_id=:1 and tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_data_type=:4"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTAssID();
			m_saCommand.Param(2).setAsLong()		= rec.getTAssTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTAssTraktDataID();
			m_saCommand.Param(4).setAsShort()		= rec.getTAssTraktDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktAss(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tass_trakt_id=:1"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::removeTraktAss(int trakt_id,int trakt_data_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssExists(trakt_id,trakt_data_id))
		//{
			sSQL.Format(_T("delete from %s where tass_trakt_id=:1 and tass_trakt_data_id=:2"),
									TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= trakt_data_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CUMEstimateDB::getNumOfRecordsInTraktAss(void)
{
	return num_of_records(TBL_TRAKT_SPC_ASS);
}


/////////////////////////////////////////////////////////
// Trakt transfers for assortments
BOOL CUMEstimateDB::getTraktTrans(vecTransactionTraktTrans &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		if (trakt_id == -1)
		{
			sSQL.Format(_T("select * from %s order by ttrans_m3fub"),TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttrans_trakt_id=:1 order by ttrans_m3fub"),TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_trans(m_saCommand.Field(1).asLong(),
																						m_saCommand.Field(2).asLong(),
																						m_saCommand.Field(3).asLong(),
																						m_saCommand.Field(4).asLong(),
																						m_saCommand.Field(5).asLong(),
																						m_saCommand.Field(6).asShort(),
																					 (LPCTSTR)m_saCommand.Field(7).asString(),
																					 (LPCTSTR)m_saCommand.Field(8).asString(),
																					 (LPCTSTR)m_saCommand.Field(9).asString(),
																					  m_saCommand.Field(10).asDouble(),
																					  m_saCommand.Field(11).asDouble(),
																					  m_saCommand.Field(13).asDouble(),
																					 (LPCTSTR)convertSADateTime(saDateTime)));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktTrans(CTransaction_trakt_trans &rec1,CTransaction_trakt_trans rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2 and ttrans_from_ass_id=:3 and ttrans_to_ass_id=:4 and ttrans_to_spc_id=:5 and ttrans_data_type=:6 ")
								_T("order by ttrans_m3fub"),TBL_TRAKT_TRANS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
		m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
		m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
		m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
		m_saCommand.Param(5).setAsLong()		= rec.getSpecieID();
		m_saCommand.Param(6).setAsShort()		= rec.getTTransDataType();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_trakt_trans(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asLong(),
																			m_saCommand.Field(4).asLong(),
																			m_saCommand.Field(5).asLong(),
																			m_saCommand.Field(6).asShort(),
																		 (LPCTSTR)m_saCommand.Field(7).asString(),
																		 (LPCTSTR)m_saCommand.Field(8).asString(),
																		 (LPCTSTR)m_saCommand.Field(9).asString(),
																		  m_saCommand.Field(10).asDouble(),
																		  m_saCommand.Field(11).asDouble(),
																		  m_saCommand.Field(13).asDouble(),
																		 (LPCTSTR)convertSADateTime(saDateTime));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktTransExists(rec))
		{
			if (rec.getTTransFromID() == -1 || rec.getTTransToID() == -1) return FALSE;

			sSQL.Format(_T("insert into %s (ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id,ttrans_to_spc_id,ttrans_data_type,ttrans_from_name,ttrans_to_name,ttrans_spc_name,ttrans_m3fub,ttrans_percent,ttrans_trans_m3) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),TBL_TRAKT_TRANS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
			m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
			m_saCommand.Param(5).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(6).setAsShort()		= rec.getTTransDataType();
			m_saCommand.Param(7).setAsString()	= rec.getFromName();
			m_saCommand.Param(8).setAsString()	= rec.getToName();
			m_saCommand.Param(9).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(10).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(12).setAsDouble()	= rec.getM3Trans();

			m_saCommand.Execute();

			doCommit();

//		S.Format(L"addTraktTrans 1 Spc %s\n\nFrom %s\nTill %s",rec.getSpecieName(),rec.getFromName(),rec.getToName());
//		UMMessageBox(S);

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CUMEstimateDB::updTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;

	try
	{
			if (rec.getTTransFromID() == -1 || rec.getTTransToID() == -1) return FALSE;

			sSQL.Format(_T("update %s set ttrans_from_name=:1,ttrans_to_name=:2,ttrans_spc_name=:3,ttrans_m3fub=:4,ttrans_percent=:5,ttrans_trans_m3=:6,created=GETDATE() ")
									_T("where ttrans_trakt_data_id=:7 and ttrans_trakt_id=:8 and ttrans_from_ass_id=:9 and ttrans_to_ass_id=:10 and ttrans_to_spc_id=:11 and ttrans_data_type=:12"),TBL_TRAKT_TRANS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getFromName();
			m_saCommand.Param(2).setAsString()	= rec.getToName();
			m_saCommand.Param(3).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(5).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(6).setAsDouble()	= rec.getM3Trans();

			m_saCommand.Param(7).setAsLong()		= rec.getTTransDataID();
			m_saCommand.Param(8).setAsLong()		= rec.getTTransTraktID();
			m_saCommand.Param(9).setAsLong()		= rec.getTTransFromID();
			m_saCommand.Param(10).setAsLong()		= rec.getTTransToID();
			m_saCommand.Param(11).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(12).setAsShort()	= rec.getTTransDataType();

			m_saCommand.Execute();

			doCommit();

//	S.Format(L"updTraktTrans 2 Spc %s\n\nFrom %s\nTill %s",rec.getSpecieName(),rec.getFromName(),rec.getToName());
//	UMMessageBox(S);

			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTransExists(rec))
		//{

			sSQL.Format(_T("delete from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2 and ttrans_from_ass_id=:3 and ttrans_to_ass_id=:4 and ttrans_to_spc_id=:5 and ttrans_data_type=:6"),
									TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
			m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
			m_saCommand.Param(5).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(6).setAsShort()		= rec.getTTransDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::removeTraktTrans(int trakt_data_id,int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTransExists(trakt_data_id,trakt_id))
		//{

			sSQL.Format(_T("delete from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2"),
									TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_data_id;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktTrans(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTransExists(trakt_id))
		//{

		sSQL.Format(_T("delete from %s where ttrans_trakt_id=:1"),
									TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::removeTraktTrans2(int trakt_id,int spc_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTransExists(trakt_id))
		//{

		sSQL.Format(_T("delete from %s where ttrans_trakt_id=:1 and ttrans_to_spc_id=:2"),
									TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= spc_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////
// Trakt set specie(s), functions (volume,height,bark etc)
BOOL CUMEstimateDB::getTraktSetSpc(vecTransactionTraktSetSpc &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
			
		// Check value of trakt_is. If trakt_id = -1, select ALL data otherwise, select
		// only data for trakt_id; 070504 p�d
		if (trakt_id == -1)
		{
			sSQL.Format(_T("select * from %s order by tsetspc_tdata_trakt_id,tsetspc_tdata_id,tsetspc_id"),
										TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
		}
		else
		{
			sSQL.Format(_T("select * from %s where tsetspc_tdata_trakt_id=:1 order by tsetspc_tdata_trakt_id,tsetspc_tdata_id,tsetspc_id"),
										TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_set_spc(m_saCommand.Field(1).asLong(),
																							 m_saCommand.Field(2).asLong(),
																							 m_saCommand.Field(3).asLong(),
																							 m_saCommand.Field(4).asShort(),
																							 m_saCommand.Field(5).asLong(),
																							 m_saCommand.Field(6).asLong(),
																							 m_saCommand.Field(7).asLong(),
																							 m_saCommand.Field(8).asLong(),
																							 m_saCommand.Field(9).asLong(),
																							 m_saCommand.Field(10).asLong(),
																							 m_saCommand.Field(11).asLong(),
																							 m_saCommand.Field(12).asLong(),
																							 m_saCommand.Field(13).asLong(),
																							 m_saCommand.Field(14).asLong(),
																							 m_saCommand.Field(15).asLong(),
																							 m_saCommand.Field(16).asLong(),
																							 m_saCommand.Field(17).asLong(),
 																							 m_saCommand.Field(18).asDouble(),
 																							 m_saCommand.Field(19).asDouble(),
 																							 m_saCommand.Field(20).asDouble(),
																							 m_saCommand.Field(21).asDouble(),
																							 m_saCommand.Field(_T("tsetspc_qdesc_index")).asLong(),
																							 (LPCTSTR)m_saCommand.Field(_T("tsetspc_extra_info")).asString(),
																							 m_saCommand.Field(_T("tsetspc_transp_dist")).asLong(),
																							 m_saCommand.Field(_T("tsetspc_transp_dist2")).asLong(),
																							 (LPCTSTR)convertSADateTime(saDateTime),
																							 (LPCTSTR)m_saCommand.Field(_T("tsetspc_qdesc_name")).asString(),
									  													 m_saCommand.Field(_T("tsetspc_grot_func_id")).asLong(),
									  													 m_saCommand.Field(_T("tsetspc_grot_percent")).asDouble(),
									  													 m_saCommand.Field(_T("tsetspc_grot_price")).asDouble(),
																							 m_saCommand.Field(_T("tsetspc_grot_cost")).asDouble(),
																							 m_saCommand.Field(_T("tsetspc_default_h25")).asDouble(),
																							 m_saCommand.Field(_T("tsetspc_default_greencrown")).asDouble()
																							 ));
		}
		doCommit();
		
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getTraktSetSpc(CTransaction_trakt_set_spc &rec1,CTransaction_trakt_set_spc rec)
{
	CString sSQL;
	try
	{
		
		sSQL.Format(_T("select * from %s where tsetspc_id=:1 and tsetspc_tdata_id=:2 and tsetspc_tdata_trakt_id=:3 and tsetspc_data_type=:4 order by tsetspc_tdata_trakt_id,tsetspc_tdata_id,tsetspc_id"),
										TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTSetspcID();
		m_saCommand.Param(2).setAsLong()		= rec.getTSetspcDataID();
		m_saCommand.Param(3).setAsLong()		= rec.getTSetspcTraktID();
		m_saCommand.Param(4).setAsShort()		= rec.getTSetspcDataType();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_trakt_set_spc(m_saCommand.Field(1).asLong(),
																			 m_saCommand.Field(2).asLong(),
																			 m_saCommand.Field(3).asLong(),
																			 m_saCommand.Field(4).asShort(),
																			 m_saCommand.Field(5).asLong(),
																			 m_saCommand.Field(6).asLong(),
																			 m_saCommand.Field(7).asLong(),
																			 m_saCommand.Field(8).asLong(),
																			 m_saCommand.Field(9).asLong(),
																			 m_saCommand.Field(10).asLong(),
																			 m_saCommand.Field(11).asLong(),
																			 m_saCommand.Field(12).asLong(),
																			 m_saCommand.Field(13).asLong(),
																			 m_saCommand.Field(14).asLong(),
																			 m_saCommand.Field(15).asLong(),
																			 m_saCommand.Field(16).asLong(),
																			 m_saCommand.Field(17).asLong(),
																			 m_saCommand.Field(18).asDouble(),
																			 m_saCommand.Field(19).asDouble(),
																			 m_saCommand.Field(20).asDouble(),
																			 m_saCommand.Field(21).asDouble(),
																			 m_saCommand.Field(_T("tsetspc_qdesc_index")).asLong(),
																			 (LPCTSTR)m_saCommand.Field(_T("tsetspc_extra_info")).asString(),
																			 m_saCommand.Field(_T("tsetspc_transp_dist")).asLong(),
																			 m_saCommand.Field(_T("tsetspc_transp_dist2")).asLong(),
																			 (LPCTSTR)convertSADateTime(saDateTime),
																			 (LPCTSTR)m_saCommand.Field(_T("tsetspc_qdesc_name")).asString(),
					  													 m_saCommand.Field(_T("tsetspc_grot_func_id")).asLong(),
					  													 m_saCommand.Field(_T("tsetspc_grot_percent")).asDouble(),
					  													 m_saCommand.Field(_T("tsetspc_grot_price")).asDouble(),
																			 m_saCommand.Field(_T("tsetspc_grot_cost")).asDouble(),
																			 m_saCommand.Field(_T("tsetspc_default_h25")).asDouble(),
																			 m_saCommand.Field(_T("tsetspc_default_greencrown")).asDouble()
																			 );

		}
		doCommit();
		
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::addTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_hgt_func_id,tsetspc_hgt_specie_id,tsetspc_hgt_func_index) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()		= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()		= rec.getHgtFuncID();
			m_saCommand.Param(7).setAsLong()		= rec.getHgtFuncSpcID();
			m_saCommand.Param(8).setAsLong()		= rec.getHgtFuncIndex();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_volfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_vol_func_id,tsetspc_vol_specie_id,tsetspc_vol_func_index) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getVolFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getVolFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getVolFuncIndex();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_bark_func_id,tsetspc_bark_specie_id,tsetspc_bark_func_index) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getBarkFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getBarkFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getBarkFuncIndex();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_vol_ub_func_id,tsetspc_vol_ub_specie_id,tsetspc_vol_ub_func_index,tsetspc_m3sk_m3ub) ")
								  _T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getVolUBFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getVolUBFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getVolUBFuncIndex();
			m_saCommand.Param(9).setAsDouble()	= rec.getM3SkToM3Ub();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CUMEstimateDB::addTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_m3sk_m3ub) values(:1,:2,:3,:4,:5,:6)"),
								 TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()		= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getM3SkToM3Ub();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpcMiscData(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_m3sk_m3fub,tsetspc_m3fub_m3to,tsetspc_extra_info,tsetspc_transp_dist,tsetspc_transp_dist2,tsetspc_default_greencrown) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()		= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getM3SkToM3Fub();
			m_saCommand.Param(7).setAsDouble()	= rec.getM3FubToM3To();
			m_saCommand.Param(8).setAsString()	= rec.getExtraInfo();
			m_saCommand.Param(9).setAsLong()		= rec.getTranspDist1();	// Added 2008-03-06 P�D
			m_saCommand.Param(10).setAsLong()	= rec.getTranspDist2();	// Added 2008-03-14 P�D
			m_saCommand.Param(11).setAsLong()	= rec.getDefaultGreenCrown();	// 4555: Standard gr�nkronegr�ns

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_grot(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_grot_func_id,tsetspc_grot_percent,tsetspc_grot_price,tsetspc_grot_cost) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getGrotFuncID();
			m_saCommand.Param(7).setAsDouble()= rec.getGrotPercent();
			m_saCommand.Param(8).setAsDouble()= rec.getGrotPrice();
			m_saCommand.Param(9).setAsDouble()= rec.getGrotCost();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_default_h25(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_default_h25) ")
				_T("values(:1,:2,:3,:4,:5,:6)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getDefaultH25();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_default_greencrown(CTransaction_trakt_set_spc &rec)	// #4555: Standard gr�nkroneandel
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_default_greencrown) ")
				_T("values(:1,:2,:3,:4,:5,:6)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getDefaultGreenCrown();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::addTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_dcls,tsetspc_qdesc_index,tsetspc_qdesc_name) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()		= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getDiamClass();
			m_saCommand.Param(7).setAsLong()		= rec.getSelQDescIndex();
			m_saCommand.Param(8).setAsString()	= rec.getQDescName();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_hgt_func_id=:1,tsetspc_hgt_specie_id=:2,tsetspc_hgt_func_index=:3,created=GETDATE() ")
									_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),
									TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getHgtFuncID();
			m_saCommand.Param(2).setAsLong()	= rec.getHgtFuncSpcID();
			m_saCommand.Param(3).setAsLong()	= rec.getHgtFuncIndex();
			m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_volfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	////---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_vol_func_id=:1,tsetspc_vol_specie_id=:2,tsetspc_vol_func_index=:3,created=GETDATE() ")
									_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),
									TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getVolFuncID();
			m_saCommand.Param(2).setAsLong()	= rec.getVolFuncSpcID();
			m_saCommand.Param(3).setAsLong()	= rec.getVolFuncIndex();
			m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::updTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_bark_func_id=:1,tsetspc_bark_specie_id=:2,tsetspc_bark_func_index=:3,created=GETDATE() ")
									_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getBarkFuncID();
			m_saCommand.Param(2).setAsLong()	= rec.getBarkFuncSpcID();
			m_saCommand.Param(3).setAsLong()	= rec.getBarkFuncIndex();
			m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_vol_ub_func_id=:1,tsetspc_vol_ub_specie_id=:2,tsetspc_vol_ub_func_index=:3,tsetspc_m3sk_m3ub=:4,created=GETDATE() ")
									_T("where tsetspc_specie_id=:5 and tsetspc_id=:6 and tsetspc_tdata_id=:7 and tsetspc_tdata_trakt_id=:8 and tsetspc_data_type=:9"),
									TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getVolUBFuncID();
			m_saCommand.Param(2).setAsLong()	= rec.getVolUBFuncSpcID();
			m_saCommand.Param(3).setAsLong()	= rec.getVolUBFuncIndex();
			m_saCommand.Param(4).setAsDouble()	= rec.getM3SkToM3Ub();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(7).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(8).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(9).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::updTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_m3sk_m3ub=:1,created=GETDATE() where tsetspc_specie_id=:2 and tsetspc_id=:3 and tsetspc_tdata_id=:4 and tsetspc_tdata_trakt_id=:5 and tsetspc_data_type=:6"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= rec.getM3SkToM3Ub();
			m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(3).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(4).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(5).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(6).setAsShort()		= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpcMiscData(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

		sSQL.Format(_T("update %s set tsetspc_m3sk_m3fub=:1,tsetspc_m3fub_m3to=:2,tsetspc_extra_info=:3,tsetspc_transp_dist=:4,tsetspc_transp_dist2=:5,created=GETDATE(),tsetspc_default_greencrown=:6,tsetspc_default_h25=:7 ")
									_T("where tsetspc_specie_id=:8 and tsetspc_id=:9 and tsetspc_tdata_id=:10 and tsetspc_tdata_trakt_id=:11 and tsetspc_data_type=:12"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= rec.getM3SkToM3Fub();
			m_saCommand.Param(2).setAsDouble()	= rec.getM3FubToM3To();
			m_saCommand.Param(3).setAsString()	= rec.getExtraInfo();
			m_saCommand.Param(4).setAsLong()		= rec.getTranspDist1();
			m_saCommand.Param(5).setAsLong()		= rec.getTranspDist2();
			m_saCommand.Param(6).setAsLong()		= rec.getDefaultGreenCrown();
			m_saCommand.Param(7).setAsLong()		= rec.getDefaultH25();
			m_saCommand.Param(8).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(9).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(10).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(11).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(12).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_grot(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSetSpcExists(rec))
		//{

		sSQL.Format(_T("update %s set tsetspc_grot_func_id=:1,tsetspc_grot_percent=:2,tsetspc_grot_price=:3,tsetspc_grot_cost=:4,created=GETDATE() ")
									_T("where tsetspc_specie_id=:5 and tsetspc_id=:6 and tsetspc_tdata_id=:7 and tsetspc_tdata_trakt_id=:8 and tsetspc_data_type=:9"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getGrotFuncID();
			m_saCommand.Param(2).setAsDouble()= rec.getGrotPercent();
			m_saCommand.Param(3).setAsDouble()= rec.getGrotPrice();
			m_saCommand.Param(4).setAsDouble()= rec.getGrotCost();
			
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(7).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(8).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(9).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_default_h25(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSetSpcExists(rec))
		//{

		sSQL.Format(_T("update %s set tsetspc_default_h25=:1,created=GETDATE() ")
									_T("where tsetspc_specie_id=:2 and tsetspc_id=:3 and tsetspc_tdata_id=:4 and tsetspc_tdata_trakt_id=:5 and tsetspc_data_type=:6"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getDefaultH25();
			
			m_saCommand.Param(2).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(4).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(5).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(6).setAsShort()	= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_default_greencrown(CTransaction_trakt_set_spc &rec)	// #4555: Standard gr�nkroneandel
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_default_greencrown=:1,created=GETDATE() ")
									_T("where tsetspc_specie_id=:2 and tsetspc_id=:3 and tsetspc_tdata_id=:4 and tsetspc_tdata_trakt_id=:5 and tsetspc_data_type=:6"),
									 TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= rec.getDefaultGreenCrown();
		
		m_saCommand.Param(2).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(4).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(6).setAsShort()	= rec.getTSetspcDataType();

		m_saCommand.Execute();	

		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_set_spc rec1;
	//getTraktSetSpc(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		//if (traktSetSpcExists(rec))
		//{

			sSQL.Format(_T("update %s set tsetspc_dcls=:1,tsetspc_qdesc_index=:2,tsetspc_qdesc_name=:3,created=GETDATE() ")
									_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= rec.getDiamClass();
			m_saCommand.Param(2).setAsLong()		= rec.getSelQDescIndex();
			m_saCommand.Param(3).setAsString()	= rec.getQDescName();

			m_saCommand.Param(4).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(5).setAsLong()		= rec.getTSetspcID();
			m_saCommand.Param(6).setAsLong()		= rec.getTSetspcDataID();
			m_saCommand.Param(7).setAsLong()		= rec.getTSetspcTraktID();
			m_saCommand.Param(8).setAsShort()		= rec.getTSetspcDataType();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updTraktSetSpc_qdesc_all(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSetSpcExists(rec))
		//{

		sSQL.Format(_T("update %s set tsetspc_qdesc_index=:1,tsetspc_qdesc_name=:2  ")
					_T("where tsetspc_tdata_trakt_id=:3 and tsetspc_id=:4"),
									 TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getSelQDescIndex();
			m_saCommand.Param(2).setAsString()= rec.getQDescName();
			
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getSpcID();

			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::removeTraktSetSpc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSetSpcExists(rec))
		//{
	
			sSQL.Format(_T("delete from %s where tass_tsetspc_id=:1,tsetspc_tdata_id=:2,tsetspc_tdata_trakt_id=:3 and tsetspc_data_type=:4"),
									TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();


			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CUMEstimateDB::getNumOfRecordsInTraktSetSpc(void)
{
	return num_of_records(TBL_TRAKT_SET_SPC);
}


///////////////////////////////////////////////////////////////////////////////
// Handle pricelist set for trakt; 070430 p�d

BOOL CUMEstimateDB::getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tprl_trakt_id=:1"),
								TBL_TRAKT_MISC_DATA);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt_misc_data(m_saCommand.Field(1).asLong(),
																				(LPCTSTR)m_saCommand.Field(2).asString(),
																				 m_saCommand.Field(3).asLong(),
																				(LPCTSTR)m_saCommand.Field(4).asLongChar(),
																				(LPCTSTR)m_saCommand.Field(5).asString(),
																				 m_saCommand.Field(6).asLong(),
																				(LPCTSTR)m_saCommand.Field(7).asLongChar(),
																				m_saCommand.Field(8).asDouble(),
																				(LPCTSTR)convertSADateTime(saDateTime));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addTraktMiscData_prl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktMiscDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tprl_trakt_id,tprl_name,tprl_typeof,tprl_pricelist) values(:1,:2,:3,:4)"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetTraktID();
			m_saCommand.Param(2).setAsString()	= rec.getName();
			m_saCommand.Param(3).setAsLong()		= rec.getTypeOf();
			m_saCommand.Param(4).setAsLongChar()	= rec.getXMLPricelist();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::addTraktMiscData_costtmpl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktMiscDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tprl_trakt_id,tprl_costtmpl_name,tprl_costtmpl_typeof,tprl_costtmpl) values(:1,:2,:3,:4)"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetTraktID();
			m_saCommand.Param(2).setAsString()	= rec.getCostsName();
			m_saCommand.Param(3).setAsLong()		= rec.getCostsTypeOf();
			m_saCommand.Param(4).setAsLongChar()	= rec.getXMLCosts();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::addTraktMiscData_dcls(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktMiscDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tprl_trakt_id,tprl_dcls) values(:1,:2)"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetTraktID();
			m_saCommand.Param(2).setAsDouble()	= rec.getDiamClass();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}


BOOL CUMEstimateDB::updTraktMiscData_prl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_misc_data rec1;
	//getTraktMiscData(rec.getTSetTraktID(),rec1);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktMiscDataExists(rec))
		//{
			sSQL.Format(_T("update %s set tprl_name=:1,tprl_typeof=:2,tprl_pricelist=:3,created=GETDATE() where tprl_trakt_id=:4"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getName();
			m_saCommand.Param(2).setAsLong()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getXMLPricelist();

			m_saCommand.Param(4).setAsLong()		= rec.getTSetTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}


BOOL CUMEstimateDB::updTraktMiscData_costtmpl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_misc_data rec1;
	//getTraktMiscData(rec.getTSetTraktID(),rec1);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktMiscDataExists(rec))
		//{
			sSQL.Format(_T("update %s set tprl_costtmpl_name=:1,tprl_costtmpl_typeof=:2,tprl_costtmpl=:3,created=GETDATE() where tprl_trakt_id=:4"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCostsName();
			m_saCommand.Param(2).setAsLong()		= rec.getCostsTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getXMLCosts();
			m_saCommand.Param(4).setAsLong()		= rec.getTSetTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updTraktMiscData_dcls(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_trakt_misc_data rec1;
	//getTraktMiscData(rec.getTSetTraktID(),rec1);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktMiscDataExists(rec))
		//{
			sSQL.Format(_T("update %s set tprl_dcls=:1,created=GETDATE() where tprl_trakt_id=:2"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()	= rec.getDiamClass();
			m_saCommand.Param(2).setAsLong()		= rec.getTSetTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}


BOOL CUMEstimateDB::delTraktMiscData(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		//if (traktMiscDataExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tprl_trakt_id=:1"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delTraktMiscData(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktMiscDataExists(rec))
		//{

			sSQL.Format(_T("delete from %s where tprl_trakt_id=:1"),TBL_TRAKT_MISC_DATA);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTSetTraktID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


///////////////////////////////////////////////////////////////////////////////
// Handle Plots (Groups); 070510 p�d

BOOL CUMEstimateDB::getPlots(int trakt_id,vecTransactionPlot &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tplot_trakt_id=:1"),TBL_TRAKT_PLOT);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_plot(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			(LPCTSTR)m_saCommand.Field(3).asString(),
																			m_saCommand.Field(4).asLong(),
																			m_saCommand.Field(5).asDouble(),
																			m_saCommand.Field(6).asDouble(),
																			m_saCommand.Field(7).asDouble(),
																			m_saCommand.Field(8).asDouble(),
																			(LPCTSTR)m_saCommand.Field(9).asString(),
																			m_saCommand.Field(10).asLong(),
																			(LPCTSTR)convertSADateTime(saDateTime)));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getPlot(int trakt_id,int plot_id,CTransaction_plot &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tplot_trakt_id=:1 and tplot_id=:2"),TBL_TRAKT_PLOT);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec =	CTransaction_plot(m_saCommand.Field(1).asLong(),
															m_saCommand.Field(2).asLong(),
															(LPCTSTR)m_saCommand.Field(3).asString(),
															m_saCommand.Field(4).asLong(),
														  m_saCommand.Field(5).asDouble(),
														  m_saCommand.Field(6).asDouble(),
														  m_saCommand.Field(7).asDouble(),
														  m_saCommand.Field(8).asDouble(),
															(LPCTSTR)m_saCommand.Field(9).asString(),
															m_saCommand.Field(10).asLong(),
															(LPCTSTR)convertSADateTime(saDateTime));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addPlot(CTransaction_plot &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktPlotExists(rec))
		{
			sSQL.Format(_T("insert into %s (tplot_id,tplot_trakt_id,tplot_name,tplot_plot_type,tplot_radius,tplot_length1,tplot_width1,tplot_area,tplot_coord,tplot_numof_trees) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)"),TBL_TRAKT_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
			m_saCommand.Param(3).setAsString()		= rec.getPlotName();
			m_saCommand.Param(4).setAsLong()			= rec.getPlotType();
			m_saCommand.Param(5).setAsDouble()		= rec.getRadius();
			m_saCommand.Param(6).setAsDouble()		= rec.getLength1();
			m_saCommand.Param(7).setAsDouble()		= rec.getWidth1();
			m_saCommand.Param(8).setAsDouble()		= rec.getArea();
			m_saCommand.Param(9).setAsString()		= rec.getCoord();
			m_saCommand.Param(10).setAsLong()		= rec.getNumOfTrees();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updPlot(CTransaction_plot &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_plot rec1;
	//getPlot(rec.getTraktID(),rec.getPlotID(),rec1);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktPlotExists(rec))
		//{
			sSQL.Format(_T("update %s set tplot_name=:1,tplot_plot_type=:2,tplot_radius=:3,tplot_length1=:4,tplot_width1=:5,tplot_area=:6,tplot_coord=:7,tplot_numof_trees=:8,created=GETDATE() ")
									_T("where tplot_id=:9 and tplot_trakt_id=:10"),TBL_TRAKT_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()		= rec.getPlotName();
			m_saCommand.Param(2).setAsLong()			= rec.getPlotType();
			m_saCommand.Param(3).setAsDouble()		= rec.getRadius();
			m_saCommand.Param(4).setAsDouble()		= rec.getLength1();
			m_saCommand.Param(5).setAsDouble()		= rec.getWidth1();
			m_saCommand.Param(6).setAsDouble()		= rec.getArea();
			m_saCommand.Param(7).setAsString()		= rec.getCoord();
			m_saCommand.Param(8).setAsLong()			= rec.getNumOfTrees();

			m_saCommand.Param(9).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(10).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

// Updates data on ALL plots for TraktID; 070927 p�d
BOOL CUMEstimateDB::updPlotEx(CTransaction_plot &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktPlotExists(rec))
		//{
			sSQL.Format(_T("update %s set tplot_name=:1,tplot_plot_type=:2,tplot_radius=:3,tplot_length1=:4,tplot_width1=:5,tplot_area=:6,tplot_coord=:7,tplot_numof_trees=:8,created=GETDATE() ")
									_T("where tplot_trakt_id=:9"),TBL_TRAKT_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()		= rec.getPlotName();
			m_saCommand.Param(2).setAsLong()			= rec.getPlotType();
			m_saCommand.Param(3).setAsDouble()		= rec.getRadius();
			m_saCommand.Param(4).setAsDouble()		= rec.getLength1();
			m_saCommand.Param(5).setAsDouble()		= rec.getWidth1();
			m_saCommand.Param(6).setAsDouble()		= rec.getArea();
			m_saCommand.Param(7).setAsString()		= rec.getCoord();
			m_saCommand.Param(8).setAsLong()			= rec.getNumOfTrees();

			m_saCommand.Param(9).setAsLong()			= rec.getTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CUMEstimateDB::addPlotType(int id,int trakt_id,int plot_type)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktPlotExists(trakt_id))
		{
			sSQL.Format(_T("insert into %s (tplot_id,tplot_trakt_id,tplot_plot_type) values(:1,:2,:3)"),TBL_TRAKT_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			=  id;
			m_saCommand.Param(2).setAsLong()			=  trakt_id;
			m_saCommand.Param(3).setAsLong()			=  plot_type;

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCTSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updPlotType(int trakt_id,int plot_type)
{
	BOOL bFound = TRUE;
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
			sSQL.Format(_T("update %s set tplot_plot_type=:1,created=GETDATE() where tplot_trakt_id=:2"),TBL_TRAKT_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	=  plot_type;
			m_saCommand.Param(2).setAsLong()	= trakt_id;

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}


BOOL CUMEstimateDB::delPlot(CTransaction_plot &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktPlotExists(rec))
		//{
			sSQL.Format(_T("delete from %s where tplot_id=%d and tplot_trakt_id=%d"),TBL_TRAKT_PLOT,rec.getPlotID(),rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delPlot(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktPlotExists(trakt_id))
		//{
			sSQL.Format(_T("delete from %s where tplot_trakt_id=%d"),TBL_TRAKT_PLOT,trakt_id);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();	

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


///////////////////////////////////////////////////////////////////////////////
// Handle Sampletrees; 070510 p�d
BOOL CUMEstimateDB::getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &vec,BOOL clear_vec)
{
	CString sSQL,S;
	try
	{
		if (clear_vec)
			vec.clear();
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (tree_type == SAMPLE_TREE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE_NO_SAMP)	// "Kanttr�d (Ber�knad h�jd)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else if (tree_type == TREE_SAMPLE_REMAINIG)	// "Provtr�d (Kvarl�mmnat)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_SAMPLE_REMAINIG;
		}
		else if (tree_type == TREE_SAMPLE_EXTRA)	// "Provtr�d (Extra)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
		}
		
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_sample_tree(m_saCommand.Field("ttree_id").asLong(),
																			m_saCommand.Field("ttree_trakt_id").asLong(),
																			m_saCommand.Field("ttree_plot_id").asLong(),
																			m_saCommand.Field("ttree_spc_id").asLong(),
																		  (LPCTSTR)m_saCommand.Field("ttree_spc_name").asString(),
																		  m_saCommand.Field("ttree_dbh").asDouble(),
																		  m_saCommand.Field("ttree_hgt").asDouble(),
																		  m_saCommand.Field("ttree_gcrown").asDouble(),
																			m_saCommand.Field("ttree_bark_thick").asDouble(),
																			m_saCommand.Field("ttree_grot").asDouble(),
																			m_saCommand.Field("ttree_age").asLong(),
																			m_saCommand.Field("ttree_growth").asLong(),
																			m_saCommand.Field("ttree_dcls_from").asDouble(),
																			m_saCommand.Field("ttree_dcls_to").asDouble(),
																			m_saCommand.Field("ttree_m3sk").asDouble(),
																			m_saCommand.Field("ttree_m3fub").asDouble(),
																			m_saCommand.Field("ttree_m3ub").asDouble(),
																			m_saCommand.Field("ttree_tree_type").asLong(),
																			(LPCTSTR)m_saCommand.Field("ttree_bonitet").asString(),
																			(LPCTSTR)convertSADateTime(saDateTime),
																			(LPCTSTR)m_saCommand.Field("ttree_coord").asString(),
																			m_saCommand.Field("ttree_distcable").asLong(),
																			m_saCommand.Field("ttree_category").asLong(),
																			m_saCommand.Field("ttree_topcut").asLong()
																			));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getSampleTree(CTransaction_sample_tree &rec1,CTransaction_sample_tree rec)
{
	CString sSQL;
	try
	{
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (rec.getTreeType() == SAMPLE_TREE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3 or ttree_tree_type=:4)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(4).setAsLong()		= TREE_OUTSIDE;
		}
		if (rec.getTreeType() == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (rec.getTreeType() == TREE_OUTSIDE_NO_SAMP)	// "Kanttr�d (Ber�knad h�jd)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_REMAINIG)	// "Provtr�d (Kvarl�mmnat)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_REMAINIG;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_EXTRA)	// "Provtr�d (Extra)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		}
		
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_sample_tree(m_saCommand.Field("ttree_id").asLong(),
																			m_saCommand.Field("ttree_trakt_id").asLong(),
																			m_saCommand.Field("ttree_plot_id").asLong(),
																			m_saCommand.Field("ttree_spc_id").asLong(),
																		  (LPCTSTR)m_saCommand.Field("ttree_spc_name").asString(),
																		  m_saCommand.Field("ttree_dbh").asDouble(),
																		  m_saCommand.Field("ttree_hgt").asDouble(),
																		  m_saCommand.Field("ttree_gcrown").asDouble(),
																			m_saCommand.Field("ttree_bark_thick").asDouble(),
																			m_saCommand.Field("ttree_grot").asDouble(),
																			m_saCommand.Field("ttree_age").asLong(),
																			m_saCommand.Field("ttree_growth").asLong(),
																			m_saCommand.Field("ttree_tree_type").asDouble(),
																			m_saCommand.Field("ttree_dcls_from").asDouble(),
																			m_saCommand.Field("ttree_dcls_to").asDouble(),
																			m_saCommand.Field("ttree_m3sk").asDouble(),
																			m_saCommand.Field("ttree_m3fub").asDouble(),
																			m_saCommand.Field("ttree_m3ub").asLong(),
																			(LPCTSTR)m_saCommand.Field("ttree_bonitet").asString(),
																			(LPCTSTR)convertSADateTime(saDateTime),
																			m_saCommand.Field("ttree_coord").asString(),
																			m_saCommand.Field("ttree_distcable").asLong(),
																			m_saCommand.Field("ttree_category").asLong(),
																			m_saCommand.Field("ttree_topcut").asLong()
																			);

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::getSampleTree_date(CString &date,CTransaction_sample_tree rec)
{
	CString sSQL;
	try
	{
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (rec.getTreeType() == SAMPLE_TREE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3 or ttree_tree_type=:4)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(4).setAsLong()		= TREE_OUTSIDE;
		}
		if (rec.getTreeType() == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (rec.getTreeType() == TREE_OUTSIDE_NO_SAMP)	// "Kanttr�d (Ber�knad h�jd)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_REMAINIG)	// "Provtr�d (Kvarl�mmnat)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_REMAINIG;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_EXTRA)	// "Provtr�d (Extra)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		}
		
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			date = (LPCTSTR)convertSADateTime(saDateTime);
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::addSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	CString S;

	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktSampleTreeExists(rec))
		{

			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				if (rec.getCategory() > -1)
					sSQL.Format(_T("insert into %s (ttree_id,ttree_trakt_id,ttree_plot_id,ttree_spc_id,ttree_spc_name,ttree_dbh,ttree_hgt,ttree_gcrown,ttree_bark_thick,ttree_grot,ttree_age,ttree_growth,ttree_dcls_from,ttree_dcls_to,")
						_T("ttree_m3sk,ttree_m3fub,ttree_m3ub,ttree_tree_type,ttree_bonitet,ttree_distcable,ttree_topcut,ttree_category) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22)"),TBL_TRAKT_SAMPLE_TREES);
				else
					sSQL.Format(_T("insert into %s (ttree_id,ttree_trakt_id,ttree_plot_id,ttree_spc_id,ttree_spc_name,ttree_dbh,ttree_hgt,ttree_gcrown,ttree_bark_thick,ttree_grot,ttree_age,ttree_growth,ttree_dcls_from,ttree_dcls_to,")
						_T("ttree_m3sk,ttree_m3fub,ttree_m3ub,ttree_tree_type,ttree_bonitet,ttree_distcable,ttree_topcut) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21)"),TBL_TRAKT_SAMPLE_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDBH();
				m_saCommand.Param(7).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(11).setAsLong()		= rec.getAge();
				m_saCommand.Param(12).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(13).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(14).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(15).setAsDouble()		= rec.getM3Sk(); //fM3Sk_rounded;
				m_saCommand.Param(16).setAsDouble()		= rec.getM3Fub(); //fM3Fub_rounded;
				m_saCommand.Param(17).setAsDouble()		= rec.getM3Ub(); //fM3Ub_rounded;
				m_saCommand.Param(18).setAsLong()		= rec.getTreeType();
				m_saCommand.Param(19).setAsString()		= rec.getBonitet();
				m_saCommand.Param(20).setAsLong()		= rec.getDistCable();
				m_saCommand.Param(21).setAsLong()		= rec.getTopCut();
				if (rec.getCategory() > -1)	// Giltig kategori?
					m_saCommand.Param(22).setAsLong()		= rec.getCategory();

				m_saCommand.Execute();

			}
			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

//#4308 20150603 J�
//Kontrollera om diameterklassen finns i st�mplingsl�ngd uttag
BOOL CUMEstimateDB::traktDCLS1TreeExistsInClass(CTransaction_sample_tree &data)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_trakt_id=%d and tdcls1_plot_id=%d and tdcls1_spc_id=%d and tdcls1_dcls_from<=%.0f and tdcls1_dcls_to>=%.0f" ),
		TBL_TRAKT_DCLS1_TREES,
		data.getTraktID(),
		data.getPlotID(),
		data.getSpcID(),
		data.getDCLS_from(),
		data.getDCLS_to());
	return exists(sSQL);
}


//#4308 20150603 J�
//Kontrollera om diameterklassen finns i st�mplingsl�ngd uttag
BOOL CUMEstimateDB::traktDCLSTreeExistsInClass(CTransaction_sample_tree &data)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d and tdcls_plot_id=%d and tdcls_spc_id=%d and tdcls_dcls_from<=%.0f and tdcls_dcls_to>=%.0f" ),
		TBL_TRAKT_DCLS_TREES,
		data.getTraktID(),
		data.getPlotID(),
		data.getSpcID(),
		data.getDCLS_from(),
		data.getDCLS_to());
	return exists(sSQL);
}

//#4308 20150603 J�
//Minska antalet i en diameterklass kvarvarande
BOOL CUMEstimateDB::updRemoveFromDclsRemain(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls1_numof=tdcls1_numof-1 where tdcls1_numof>=1 and tdcls1_trakt_id=:1 and tdcls1_plot_id=:2 and tdcls1_spc_id=:3 and tdcls1_dcls_from<=:4 and tdcls1_dcls_to>=:5" ),
			TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4308 20150605 J�
//Minska antalet i antal kanttr�d uttag
BOOL CUMEstimateDB::updRemoveNumKanttradFromDclsUttag(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof_randtrees=tdcls_numof_randtrees-1 where tdcls_numof_randtrees>=1 and tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from<=:4 and tdcls_dcls_to>=:5" ),
			TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4308 20150603 J�
//Minska antalet i en diameterklass uttag
BOOL CUMEstimateDB::updRemoveFromDclsUttag(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof=tdcls_numof-1 where tdcls_numof>=1 and tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from<=:4 and tdcls_dcls_to>=:5" ),
			TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4308 20150612 J�
//Om inga antal eller antal kanttr�d kvar, ta bort diameterklass.
BOOL CUMEstimateDB::delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsRemain(CTransaction_sample_tree &data)
{
	BOOL bRet=FALSE;
	CString sSQL;

	try
	{
		sSQL.Format(_T("delete from %s where tdcls1_numof<=0 and tdcls1_trakt_id=:1 and tdcls1_plot_id=:2 and tdcls1_spc_id=:3 and tdcls1_dcls_from =:4 and tdcls1_dcls_to=:5"),TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();	
		doCommit();
		bRet = TRUE;
	}
	catch(SAException &e)
	{
		// print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bRet;
}

//#4308 20150612 J�
//Om inga antal eller antal kanttr�d kvar, ta bort diameterklass.
BOOL CUMEstimateDB::delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsUttag(CTransaction_sample_tree &data)
{
	BOOL bRet=FALSE;
	CString sSQL;



	try
	{
		sSQL.Format(_T("delete from %s where tdcls_numof<=0 and tdcls_numof_randtrees<=0 and tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from =:4 and tdcls_dcls_to=:5"),TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();	
		doCommit();
		bRet = TRUE;
	}
	catch(SAException &e)
	{
		// print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bRet;
}

//#4308 20150603 J�
//�ka antalet kanttr�d i en diameterklass kvarvarande
/*BOOL CUMEstimateDB::updAddNumKanttradToDclsRemain(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof_randtrees=tdcls_numof_randtrees+1 where tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from<=:4 and tdcls_dcls_to>=:5" ),
			TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}*/


//#4308 20150605 J�
//�ka antalet tr�d i en diameterklass kvarvarande
BOOL CUMEstimateDB::updAddNumOfToDclsRemain(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls1_numof=tdcls1_numof+1 where tdcls1_trakt_id=:1 and tdcls1_plot_id=:2 and tdcls1_spc_id=:3 and tdcls1_dcls_from<=:4 and tdcls1_dcls_to>=:5" ),
			TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4308 20150605 J�
//�ka antalet tr�d i en diameterklass uttag
BOOL CUMEstimateDB::updAddNumOfToDclsUttag(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof=tdcls_numof+1 where tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from<=:4 and tdcls_dcls_to>=:5" ),
			TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4465 ut�ka antal tr�d i en diamterklass uttag
BOOL CUMEstimateDB::updIncNumOfToDclsUttag(CTransaction_dcls_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof=tdcls_numof+%d where tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from<=:4 and tdcls_dcls_to>=:5" ),
			TBL_TRAKT_DCLS_TREES,data.getNumOf());
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4465 ut�ka antal tr�d i en diamterklass kvarl�mnat
BOOL CUMEstimateDB::updIncNumOfToDclsRemain(CTransaction_dcls_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls1_numof=tdcls1_numof+%d where tdcls1_trakt_id=:1 and tdcls1_plot_id=:2 and tdcls1_spc_id=:3 and tdcls1_dcls_from<=:4 and tdcls1_dcls_to>=:5" ),
			TBL_TRAKT_DCLS1_TREES,data.getNumOf());
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

//#4308 20150603 J�
//�ka antalet kanttr�d i en diameterklass uttag
BOOL CUMEstimateDB::updAddNumKanttradToDclsUttag(CTransaction_sample_tree &data)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tdcls_numof_randtrees=tdcls_numof_randtrees+1 where tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_spc_id=:3 and tdcls_dcls_from=:4 and tdcls_dcls_to=:5" ),
			TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()			= data.getTraktID();
		m_saCommand.Param(2).setAsLong()			= data.getPlotID();
		m_saCommand.Param(3).setAsLong()			= data.getSpcID();
		m_saCommand.Param(4).setAsDouble()			= data.getDCLS_from();
		m_saCommand.Param(5).setAsDouble()			= data.getDCLS_to();
		m_saCommand.Execute();
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CUMEstimateDB::updSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;

	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_sample_tree rec1;
	//getSampleTree(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktSampleTreeExists(rec))
		//{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				if (rec.getCategory() > -1)
					sSQL.Format(_T("update %s set ttree_plot_id=:1,ttree_spc_id=:2,ttree_spc_name=:3,ttree_dbh=:4,ttree_hgt=:5,ttree_gcrown=:6,ttree_bark_thick=:7,ttree_grot=:8,ttree_age=:9,ttree_growth=:10,ttree_dcls_from=:11,ttree_dcls_to=:12,")
						_T("ttree_m3sk=:13,ttree_m3fub=:14,ttree_m3ub=:15,ttree_tree_type=:16,ttree_bonitet=:17,ttree_category=:22,ttree_distcable=:18,ttree_topcut=:19,created=GETDATE() where ttree_id=:20 and ttree_trakt_id=:21"),TBL_TRAKT_SAMPLE_TREES);
				else
					sSQL.Format(_T("update %s set ttree_plot_id=:1,ttree_spc_id=:2,ttree_spc_name=:3,ttree_dbh=:4,ttree_hgt=:5,ttree_gcrown=:6,ttree_bark_thick=:7,ttree_grot=:8,ttree_age=:9,ttree_growth=:10,ttree_dcls_from=:11,ttree_dcls_to=:12,")
						_T("ttree_m3sk=:13,ttree_m3fub=:14,ttree_m3ub=:15,ttree_tree_type=:16,ttree_bonitet=:17,ttree_distcable=:18,ttree_topcut=:19,created=GETDATE() where ttree_id=:20 and ttree_trakt_id=:21"),TBL_TRAKT_SAMPLE_TREES);


				m_saCommand.setCommandText((SAString)sSQL);

				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDBH();
				m_saCommand.Param(5).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(6).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(7).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(8).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(9).setAsLong()			= rec.getAge();
				m_saCommand.Param(10).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(11).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(12).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(13).setAsDouble()		= rec.getM3Sk(); //fM3Sk_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3Fub(); //fM3Fub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getM3Ub(); //fM3Ub_rounded;
				m_saCommand.Param(16).setAsLong()		= rec.getTreeType();
				m_saCommand.Param(17).setAsString()		= rec.getBonitet();
				m_saCommand.Param(18).setAsLong()		= rec.getDistCable();
				m_saCommand.Param(19).setAsLong()		= rec.getTopCut();

				m_saCommand.Param(20).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(21).setAsLong()		= rec.getTraktID();

				if (rec.getCategory() > -1)
					m_saCommand.Param(22).setAsLong()		= rec.getCategory();

				m_saCommand.Execute();

				doCommit();
			}
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::resetSampleTrees(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSampleTreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set ttree_m3sk=:1,ttree_m3fub=:2,ttree_m3ub=:3 where ttree_trakt_id=:4"),TBL_TRAKT_SAMPLE_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()		= 0.0;
			m_saCommand.Param(2).setAsDouble()		= 0.0;
			m_saCommand.Param(3).setAsDouble()		= 0.0;
			m_saCommand.Param(4).setAsLong()			= trakt_id;

			m_saCommand.Execute();
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delSampleTreesInTrakt(int trakt_id,int tree_type)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSampleTreeExists(trakt_id))
		//{
			if (tree_type == SAMPLE_TREE ||
				  tree_type == TREE_OUTSIDE)
			{
				sSQL.Format(_T("delete from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
										TBL_TRAKT_SAMPLE_TREES);
					m_saCommand.setCommandText((SAString)sSQL);
					m_saCommand.Param(1).setAsLong()		= trakt_id;
					m_saCommand.Param(2).setAsLong()		= SAMPLE_TREE;
					m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
			}
			else
			{
				sSQL.Format(_T("delete from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
										TBL_TRAKT_SAMPLE_TREES);
					m_saCommand.setCommandText((SAString)sSQL);
					m_saCommand.Param(1).setAsLong()		= trakt_id;
					m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
			}

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delSampleTree(int tree_id,int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktSampleTreeExists(tree_id,trakt_id))
		//{

			sSQL.Format(_T("delete from %s where ttree_id=:1 and ttree_trakt_id=:2"),
									TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= tree_id;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CUMEstimateDB::getMaxSampleTreeID(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	int nMaxID = -1;
	try
	{
		//if (traktSampleTreeExists(trakt_id))
		//{

			sSQL.Format(_T("select max(ttree_id) as 'max_id' from %s where ttree_trakt_id=:1"),
									TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				nMaxID = m_saCommand.Field(_T("max_id")).asLong();
			}	
			
			doCommit();

		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return nMaxID;
	}

	return nMaxID;
}

BOOL CUMEstimateDB::updSampleTrees_age(int trakt_id,int from_age,int to_age)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	// No need to update; 090317 p�d
	if (from_age == to_age) return FALSE;
	try
	{
		//if (traktSampleTreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set ttree_age=:1,created=GETDATE() where ttree_trakt_id=:2 and ttree_age=:3 and (ttree_tree_type=:4 or ttree_tree_type=:5)"),
							TBL_TRAKT_SAMPLE_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= to_age;
			m_saCommand.Param(2).setAsLong()		= trakt_id;
			m_saCommand.Param(3).setAsLong()		= from_age;
			m_saCommand.Param(4).setAsLong()		= TREE_OUTSIDE;
			m_saCommand.Param(5).setAsLong()		= TREE_OUTSIDE_NO_SAMP;

			m_saCommand.Execute();
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updSampleTrees_bonitet(int trakt_id,LPCTSTR from_bonitet,LPCTSTR to_bonitet)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	// No need to update; 090317 p�d
	if (from_bonitet == to_bonitet) return FALSE;
	try
	{
		//if (traktSampleTreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set ttree_bonitet=:1,created=GETDATE() where ttree_trakt_id=:2 and ttree_bonitet=:3 and (ttree_tree_type=:4 or ttree_tree_type=:5)"),
							TBL_TRAKT_SAMPLE_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()		= to_bonitet;
			m_saCommand.Param(2).setAsLong()			= trakt_id;
			m_saCommand.Param(3).setAsString()		= from_bonitet;
			m_saCommand.Param(4).setAsLong()			= TREE_OUTSIDE;
			m_saCommand.Param(5).setAsLong()			= TREE_OUTSIDE_NO_SAMP;

			m_saCommand.Execute();
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


///////////////////////////////////////////////////////////////////////////////
// Handle DCLSrees; 070820 p�d
BOOL CUMEstimateDB::getDCLSTrees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL,S;
	int ant = 0;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1"),TBL_TRAKT_DCLS_TREES);	
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(18).asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field("tdcls_id").asLong(),
																			m_saCommand.Field("tdcls_trakt_id").asLong(),
																			m_saCommand.Field("tdcls_plot_id").asLong(),
																			m_saCommand.Field("tdcls_spc_id").asLong(),
																		  (LPCTSTR)m_saCommand.Field("tdcls_spc_name").asString(),
																		  m_saCommand.Field("tdcls_dcls_from").asDouble(),
																		  m_saCommand.Field("tdcls_dcls_to").asDouble(),
																		  m_saCommand.Field("tdcls_hgt").asDouble(),
																		  m_saCommand.Field("tdcls_numof").asLong(),
																			m_saCommand.Field("tdcls_gcrown").asDouble(),
																			m_saCommand.Field("tdcls_bark_thick").asDouble(),
																			m_saCommand.Field("tdcls_m3sk").asDouble(),
																			m_saCommand.Field("tdcls_m3fub").asDouble(),
																			m_saCommand.Field("tdcls_m3ub").asDouble(),
																			m_saCommand.Field("tdcls_grot").asDouble(),
																			m_saCommand.Field("tdcls_age").asLong(),
																			m_saCommand.Field("tdcls_growth").asLong(),
																			m_saCommand.Field("tdcls_numof_randtrees").asLong(),
																			(LPCTSTR)convertSADateTime(saDateTime)
																			));


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getDCLSTree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1 and tdcls_id=:2"),
				TBL_TRAKT_DCLS_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(18).asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																		m_saCommand.Field(2).asLong(),
																		m_saCommand.Field(3).asLong(),
																		m_saCommand.Field(4).asLong(),
																		(LPCTSTR)m_saCommand.Field(5).asString(),
																		m_saCommand.Field(6).asDouble(),
																		m_saCommand.Field(7).asDouble(),
																		m_saCommand.Field(8).asDouble(),
																		m_saCommand.Field(9).asLong(),
																		m_saCommand.Field(10).asDouble(),
																		m_saCommand.Field(11).asDouble(),
																		m_saCommand.Field(12).asDouble(),
																		m_saCommand.Field(13).asDouble(),
																		m_saCommand.Field(14).asDouble(),
																		m_saCommand.Field(15).asDouble(),
																		m_saCommand.Field(16).asLong(),
																		m_saCommand.Field(17).asLong(),
																		m_saCommand.Field(19).asLong(),
																		(LPCTSTR)convertSADateTime(saDateTime));


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	//TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLSTreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			// Only add trees that have a "number of" trees > 0"; 080915 p�d
			if (rec.getSpcID() > 0 && rec.getNumOf() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth,tdcls_numof_randtrees) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)"),
									 TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(18).setAsLong()		= rec.getNumOfRandTrees();	// Added 080307 p�d

				m_saCommand.Execute();
			
				doCommit();
			}
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::addDCLSTreesWithoutNumOfCheck(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	//TCHAR tmp[127];
	try
	{

			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			// Only add trees that have a "number of" trees > 0"; 080915 p�d
			if (rec.getSpcID() > 0) // && rec.getNumOf() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth,tdcls_numof_randtrees) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)"),
									 TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(18).setAsLong()		= rec.getNumOfRandTrees();	// Added 080307 p�d

				m_saCommand.Execute();
			
				doCommit();
			}
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL,S;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	BOOL bReturn = FALSE;
	//TCHAR tmp[127];
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_dcls_tree rec1;
	//getDCLSTree(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktDCLSTreeExists(rec))
		//{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			// Only add trees that have a "number of" trees > 0; 080915 p�d
			if (rec.getSpcID() > 0 && (rec.getNumOf() > 0 || rec.getNumOfRandTrees() > 0))
			{

				sSQL.Format(_T("update %s set tdcls_plot_id=:1,tdcls_spc_id=:2,tdcls_spc_name=:3,tdcls_dcls_from=:4,tdcls_dcls_to=:5,tdcls_hgt=:6,tdcls_numof=:7,tdcls_gcrown=:8,tdcls_bark_thick=:9,tdcls_m3sk=:10,tdcls_m3fub=:11,tdcls_m3ub=:12,")
									  _T("tdcls_grot=:13,tdcls_age=:14,tdcls_growth=:15,tdcls_numof_randtrees=:16,created=GETDATE() where tdcls_id=:17 and tdcls_trakt_id=:18"),TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(14).setAsLong()		= rec.getAge();
				m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(16).setAsLong()		= rec.getNumOfRandTrees();	// Added 080307 p�d

				m_saCommand.Param(17).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(18).setAsLong()		= rec.getTraktID();

				m_saCommand.Execute();

				doCommit();
			}
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}


BOOL CUMEstimateDB::updDCLSRandTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_dcls_tree rec1;
	//getDCLSTree(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktDCLSTreeExists(rec))
		//{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{

				sSQL.Format(_T("update %s set tdcls_numof_randtrees=:1,created=GETDATE() where tdcls_trakt_id=:2"),TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				m_saCommand.Param(1).setAsLong()			= rec.getNumOfRandTrees();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();

				m_saCommand.Execute();

				doCommit();
			}
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}



BOOL CUMEstimateDB::resetDCLSTrees(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLSTreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tdcls_gcrown=:1,tdcls_bark_thick=:2,tdcls_grot=:3,tdcls_dcls_from=:4,tdcls_dcls_to=:5,tdcls_age=:6,tdcls_growth=:7,tdcls_numof_randtrees=:8 where tdcls_trakt_id=:9"),
									TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()		= 0.0;
			m_saCommand.Param(2).setAsDouble()		= 0.0;
			m_saCommand.Param(3).setAsDouble()		= 0.0;
			m_saCommand.Param(4).setAsDouble()		= 0.0;
			m_saCommand.Param(5).setAsDouble()		= 0.0;
			m_saCommand.Param(6).setAsLong()			= 0;
			m_saCommand.Param(7).setAsLong()			= 0;
			m_saCommand.Param(8).setAsLong()			= 0;

			m_saCommand.Param(9).setAsLong()		= trakt_id;

			m_saCommand.Execute();

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::resetDCLSTrees_tree_type(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLSTreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tdcls_numof_randtrees=:1 where tdcls_trakt_id=:2"),
							TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()	= 0;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLSTreesInTrakt(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLSTreeExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls_trakt_id=:1"),
							TBL_TRAKT_DCLS_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLSTreesInTrakt_plot(int trakt_id,int plot_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls_trakt_id=:1 and tdcls_plot_id=:2"),TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLSTree(int tree_id,int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLSTreeExists(tree_id,trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls_id=:1 and tdcls_trakt_id=:2"),
							TBL_TRAKT_DCLS_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= tree_id;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delSampleTreesInDCLS(double dcls_from, double dcls_to, int trakt_id, int plot_id, int spc_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Delete any 'Provtr�d (Uttag)' and 'Pos.tr�d (Uttag)' in diameter class
		sSQL.Format(_T("delete from %s where (ttree_tree_type=0 or ttree_tree_type=7) and (ttree_dcls_from=:1 and ttree_dcls_to=:2 and ttree_trakt_id=:3 and ttree_plot_id=:4 and ttree_spc_id=:5)"),
						TBL_TRAKT_SAMPLE_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()		= dcls_from;
		m_saCommand.Param(2).setAsDouble()		= dcls_to;
		m_saCommand.Param(3).setAsLong()		= trakt_id;
		m_saCommand.Param(4).setAsLong()		= plot_id;
		m_saCommand.Param(5).setAsLong()		= spc_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CUMEstimateDB::getNumSampleTreesInDCLS(double dcls_from, double dcls_to, int trakt_id, int plot_id, int spc_id)
{
	CString sSQL;
	int nCount = -1;
	try
	{
		// Count number of 'Provtr�d (Uttag)' and 'Pos.tr�d (Uttag)' in diameter class
		sSQL.Format(_T("select count(*) from %s where (ttree_tree_type=0 or ttree_tree_type=7) and (ttree_dcls_from=:1 and ttree_dcls_to=:2 and ttree_trakt_id=:3 and ttree_plot_id=:4 and ttree_spc_id=:5)"),
						TBL_TRAKT_SAMPLE_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()		= dcls_from;
		m_saCommand.Param(2).setAsDouble()		= dcls_to;
		m_saCommand.Param(3).setAsLong()		= trakt_id;
		m_saCommand.Param(4).setAsLong()		= plot_id;
		m_saCommand.Param(5).setAsLong()		= spc_id;

		m_saCommand.Execute();	
		m_saCommand.FetchNext();
		nCount = m_saCommand.Field(1).asLong();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return nCount;
	}

	return nCount;
}

int CUMEstimateDB::getMaxDCLSTreeID(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	int nMaxID = -1;
	try
	{
		//if (traktDCLSTreeExists(trakt_id))
		//{

			sSQL.Format(_T("select max(tdcls_id) as 'max_id' from %s where tdcls_trakt_id=:1"),
							TBL_TRAKT_DCLS_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				nMaxID = m_saCommand.Field(_T("max_id")).asLong();
			}	
			
			doCommit();

		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return nMaxID;
	}

	return nMaxID;
}

double CUMEstimateDB::getMaxDCLS(int trakt_id,int spc_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fMaxDcls = 0.0;
	double fMaxDclsSample = 0.0;
	try
	{
		//if (traktDCLSTreeExists(trakt_id))
		//{
			if (spc_id == -1)
			{
				sSQL.Format(_T("select max(tdcls_dcls_to) as 'max_dcls' from %s where tdcls_trakt_id=:1 and tdcls_hgt>0.0"),TBL_TRAKT_DCLS_TREES);
				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
			}
			else
			{
				sSQL.Format(_T("select max(tdcls_dcls_to) as 'max_dcls' from %s where tdcls_trakt_id=:1 and tdcls_hgt>0.0 and tdcls_spc_id=:2"),TBL_TRAKT_DCLS_TREES);
				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
				m_saCommand.Param(2).setAsLong()		= spc_id;
			}

			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				fMaxDcls = m_saCommand.Field(_T("max_dcls")).asLong();
			}	

			m_saCommand.Close();

			if (spc_id == -1)
			{
				sSQL.Format(_T("select max(ttree_dcls_to) as 'max_dcls_sample' from %s where ttree_trakt_id=:1 and ttree_hgt>0.0"),TBL_TRAKT_SAMPLE_TREES);
				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
			}
			else
			{
				sSQL.Format(_T("select max(ttree_dcls_to) as 'max_dcls_sample' from %s where ttree_trakt_id=:1 and ttree_hgt>0.0 and ttree_spc_id=:2"),TBL_TRAKT_SAMPLE_TREES);
				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
				m_saCommand.Param(2).setAsLong()		= spc_id;
			}

			m_saCommand.Execute();


			while(m_saCommand.FetchNext())
			{
				fMaxDclsSample = m_saCommand.Field(_T("max_dcls_sample")).asLong();
			}	
			
			doCommit();

		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return fMaxDcls;
	}

	return max(fMaxDcls,fMaxDclsSample);
}

double CUMEstimateDB::getDCLSMaxTreeHeightForSpc(int trakt_id,int spc_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fMaxHgt = 0.0;
	try
	{
		sSQL.Format(_T("select max(tdcls_hgt) as 'max_hgt' from %s where tdcls_trakt_id=:1 and tdcls_spc_id=:2"),TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= spc_id;

		m_saCommand.Execute();
			
		while(m_saCommand.FetchNext())
		{
			fMaxHgt = m_saCommand.Field(_T("max_hgt")).asLong();
		}	
			
		doCommit();

	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return fMaxHgt;
	}

	return fMaxHgt;
}


///////////////////////////////////////////////////////////////////////////////
// Handle DCLS1Trees; 071120 p�d
BOOL CUMEstimateDB::getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls1_trakt_id=:1"),
						TBL_TRAKT_DCLS1_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asLong(),
																			m_saCommand.Field(4).asLong(),
																		  (LPCTSTR)m_saCommand.Field(5).asString(),
																		  m_saCommand.Field(6).asDouble(),
																		  m_saCommand.Field(7).asDouble(),
																		  m_saCommand.Field(8).asDouble(),
																		  m_saCommand.Field(9).asLong(),
																			m_saCommand.Field(10).asDouble(),
																			m_saCommand.Field(11).asDouble(),
																			m_saCommand.Field(12).asDouble(),
																			m_saCommand.Field(13).asDouble(),
																			m_saCommand.Field(14).asDouble(),
																			m_saCommand.Field(15).asDouble(),
																			m_saCommand.Field(16).asLong(),
																			m_saCommand.Field(17).asLong(),
																			0,
																			(LPCTSTR)convertSADateTime(saDateTime) ));


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getDCLS1Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls1_trakt_id=:1 and tdcls1_id=:2"),
				TBL_TRAKT_DCLS1_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																		m_saCommand.Field(2).asLong(),
																		m_saCommand.Field(3).asLong(),
																		m_saCommand.Field(4).asLong(),
																		(LPCTSTR)m_saCommand.Field(5).asString(),
																		m_saCommand.Field(6).asDouble(),
																		m_saCommand.Field(7).asDouble(),
																		m_saCommand.Field(8).asDouble(),
																		m_saCommand.Field(9).asLong(),
																		m_saCommand.Field(10).asDouble(),
																		m_saCommand.Field(11).asDouble(),
																		m_saCommand.Field(12).asDouble(),
																		m_saCommand.Field(13).asDouble(),
																		m_saCommand.Field(14).asDouble(),
																		m_saCommand.Field(15).asDouble(),
																		m_saCommand.Field(16).asLong(),
																		m_saCommand.Field(17).asLong(),
																		m_saCommand.Field(18).asLong(),
																		(LPCTSTR)convertSADateTime(saDateTime) );


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::addDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	//TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS1TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls1_id,tdcls1_trakt_id,tdcls1_plot_id,tdcls1_spc_id,tdcls1_spc_name,tdcls1_dcls_from,tdcls1_dcls_to,tdcls1_hgt,tdcls1_numof,tdcls1_gcrown,tdcls1_bark_thick,")
										_T("tdcls1_m3sk,tdcls1_m3fub,tdcls1_m3ub,tdcls1_grot,tdcls1_age,tdcls1_growth) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),
										TBL_TRAKT_DCLS1_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();

				m_saCommand.Execute();
			
				doCommit();
			}
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	BOOL bReturn = FALSE;
	//TCHAR tmp[127];
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_dcls_tree rec1;
	//getDCLS1Tree(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktDCLS1TreeExists(rec))
		//{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{

				sSQL.Format(_T("update %s set tdcls1_plot_id=:1,tdcls1_spc_id=:2,tdcls1_spc_name=:3,tdcls1_dcls_from=:4,tdcls1_dcls_to=:5,tdcls1_hgt=:6,tdcls1_numof=:7,tdcls1_gcrown=:8,")
										_T("tdcls1_bark_thick=:9,tdcls1_m3sk=:10,tdcls1_m3fub=:11,tdcls1_m3ub=:12,tdcls1_grot=:13,tdcls1_age=:14,tdcls1_growth=:15,created=GETDATE() where tdcls1_id=:16 and tdcls1_trakt_id=:17"),
										 TBL_TRAKT_DCLS1_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(14).setAsLong()		= rec.getAge();
				m_saCommand.Param(15).setAsLong()		= rec.getGrowth();

				m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(17).setAsLong()		= rec.getTraktID();

				m_saCommand.Execute();

				doCommit();
			}
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::resetDCLS1Trees(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS1TreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tdcls1_gcrown=:1,tdcls1_bark_thick=:2,tdcls1_grot=:3,tdcls1_dcls_from=:4,tdcls1_dcls_to=:5,tdcls1_age=:6,tdcls1_growth=:7 where tdcls1_trakt_id=:8"),
									TBL_TRAKT_DCLS1_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()		= 0.0;
			m_saCommand.Param(2).setAsDouble()		= 0.0;
			m_saCommand.Param(3).setAsDouble()		= 0.0;
			m_saCommand.Param(4).setAsDouble()		= 0.0;
			m_saCommand.Param(5).setAsDouble()		= 0.0;
			m_saCommand.Param(6).setAsLong()			= 0;
			m_saCommand.Param(7).setAsLong()			= 0;

			m_saCommand.Param(8).setAsLong()		= trakt_id;

			m_saCommand.Execute();

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS1TreesInTrakt(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS1TreeExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls1_trakt_id=:1"),
							TBL_TRAKT_DCLS1_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS1TreesInTrakt_plot(int trakt_id,int plot_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls1_trakt_id=:1 and tdcls1_plot_id=:2"),TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS1Tree(int tree_id,int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS1TreeExists(tree_id,trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls1_id=:1 and tdcls1_trakt_id=:2"),
							TBL_TRAKT_DCLS1_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= tree_id;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CUMEstimateDB::getMaxDCLS1TreeID(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	int nMaxID = -1;
	try
	{
		//if (traktDCLS1TreeExists(trakt_id))
		//{

			sSQL.Format(_T("select max(tdcls1_id) as 'max_id' from %s where tdcls1_trakt_id=:1"),
							TBL_TRAKT_DCLS1_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				nMaxID = m_saCommand.Field(_T("max_id")).asLong();
			}	
			
			doCommit();

		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return nMaxID;
	}

	return nMaxID;
}


///////////////////////////////////////////////////////////////////////////////
// Handle DCLS2Trees; 071120 p�d
BOOL CUMEstimateDB::getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls2_trakt_id=:1"),
					TBL_TRAKT_DCLS2_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asLong(),
																			m_saCommand.Field(4).asLong(),
																		  (LPCTSTR)m_saCommand.Field(5).asString(),
																		  m_saCommand.Field(6).asDouble(),
																		  m_saCommand.Field(7).asDouble(),
																		  m_saCommand.Field(8).asDouble(),
																		  m_saCommand.Field(9).asLong(),
																			m_saCommand.Field(10).asDouble(),
																			m_saCommand.Field(11).asDouble(),
																			m_saCommand.Field(12).asDouble(),
																			m_saCommand.Field(13).asDouble(),
																			m_saCommand.Field(14).asDouble(),
																			m_saCommand.Field(15).asDouble(),
																			m_saCommand.Field(16).asLong(),
																			m_saCommand.Field(17).asLong(),
																			0,
																			(LPCTSTR)convertSADateTime(saDateTime) ));


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getDCLS2Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls2_trakt_id=:1 and tdcls2_id=:2"),
				TBL_TRAKT_DCLS2_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																		m_saCommand.Field(2).asLong(),
																		m_saCommand.Field(3).asLong(),
																		m_saCommand.Field(4).asLong(),
																		(LPCTSTR)m_saCommand.Field(5).asString(),
																		m_saCommand.Field(6).asDouble(),
																		m_saCommand.Field(7).asDouble(),
																		m_saCommand.Field(8).asDouble(),
																		m_saCommand.Field(9).asLong(),
																		m_saCommand.Field(10).asDouble(),
																		m_saCommand.Field(11).asDouble(),
																		m_saCommand.Field(12).asDouble(),
																		m_saCommand.Field(13).asDouble(),
																		m_saCommand.Field(14).asDouble(),
																		m_saCommand.Field(15).asDouble(),
																		m_saCommand.Field(16).asLong(),
																		m_saCommand.Field(17).asLong(),
																		0,
																		(LPCTSTR)convertSADateTime(saDateTime) );


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	//TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS2TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls2_id,tdcls2_trakt_id,tdcls2_plot_id,tdcls2_spc_id,tdcls2_spc_name,tdcls2_dcls_from,tdcls2_dcls_to,tdcls2_hgt,tdcls2_numof,tdcls2_gcrown,tdcls2_bark_thick,")
										_T("tdcls2_m3sk,tdcls2_m3fub,tdcls2_m3ub,tdcls2_grot,tdcls2_age,tdcls2_growth) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),
										TBL_TRAKT_DCLS2_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub();//fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();

				m_saCommand.Execute();
			
				doCommit();
			}
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	BOOL bReturn = FALSE;
	//TCHAR tmp[127];
	//---------------------------------------------------------------------
	// Check if there's any changes to record. If not just QUIT; 090303 p�d
	//CTransaction_dcls_tree rec1;
	//getDCLS2Tree(rec1,rec);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktDCLS2TreeExists(rec))
		//{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{

				sSQL.Format(_T("update %s set tdcls2_plot_id=:1,tdcls2_spc_id=:2,tdcls2_spc_name=:3,tdcls2_dcls_from=:4,tdcls2_dcls_to=:5,tdcls2_hgt=:6,tdcls2_numof=:7,tdcls2_gcrown=:8,")
										_T("tdcls2_bark_thick=:9,tdcls2_m3sk=:10,tdcls2_m3fub=:11,tdcls2_m3ub=:12,tdcls2_grot=:13,tdcls2_age=:14,tdcls2_growth=:15,created=GETDATE() where tdcls2_id=:16 and tdcls2_trakt_id=:17"),
										TBL_TRAKT_DCLS2_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(14).setAsLong()		= rec.getAge();
				m_saCommand.Param(15).setAsLong()		= rec.getGrowth();

				m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(17).setAsLong()		= rec.getTraktID();

				m_saCommand.Execute();

				doCommit();
			}
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::resetDCLS2Trees(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS2TreeExists(trakt_id))
		//{
			sSQL.Format(_T("update %s set tdcls2_gcrown=:1,tdcls2_bark_thick=:2,tdcls2_grot=:3,tdcls2_dcls_from=:4,tdcls2_dcls_to=:5,tdcls2_age=:6,tdcls2_growth=:7 where tdcls2_trakt_id=:8"),
									TBL_TRAKT_DCLS2_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()		= 0.0;
			m_saCommand.Param(2).setAsDouble()		= 0.0;
			m_saCommand.Param(3).setAsDouble()		= 0.0;
			m_saCommand.Param(4).setAsDouble()		= 0.0;
			m_saCommand.Param(5).setAsDouble()		= 0.0;
			m_saCommand.Param(6).setAsLong()			= 0;
			m_saCommand.Param(7).setAsLong()			= 0;

			m_saCommand.Param(8).setAsLong()		= trakt_id;

			m_saCommand.Execute();

			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS2TreesInTrakt(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS2TreeExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls2_trakt_id=:1"),TBL_TRAKT_DCLS2_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS2TreesInTrakt_plot(int trakt_id,int plot_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls2_trakt_id=:1 and tdcls2_plot_id=:2"),TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::delDCLS2Tree(int tree_id,int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktDCLS2TreeExists(tree_id,trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tdcls2_id=:1 and tdcls2_trakt_id=:2"),
							TBL_TRAKT_DCLS2_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= tree_id;
			m_saCommand.Param(2).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CUMEstimateDB::getMaxDCLS2TreeID(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	int nMaxID = -1;
	try
	{
		//if (traktDCLS2TreeExists(trakt_id))
		//{

			sSQL.Format(_T("select max(tdcls2_id) as 'max_id' from %s where tdcls2_trakt_id=:1"),
							TBL_TRAKT_DCLS2_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				nMaxID = m_saCommand.Field(_T("max_id")).asLong();
			}	
			
			doCommit();

		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return nMaxID;
	}

	return nMaxID;
}

BOOL CUMEstimateDB::delSampleTreesInTrakt_plot(int trakt_id,int plot_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where ttree_trakt_id=:1 and ttree_plot_id=:2"),TBL_TRAKT_SAMPLE_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


int CUMEstimateDB::getSumOfDCLSTrees_plot(int trakt_id,int plot_id)
{
	CString sSQL,S;
	int ant = 0;
	try
	{
		sSQL.Format(_T("select sum(tdcls_numof)+sum(tdcls_numof_randtrees) as 'counted' from %s where tdcls_trakt_id=:1 and tdcls_plot_id=:2"),
								TBL_TRAKT_DCLS_TREES); 

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
			ant += m_saCommand.Field(_T("counted")).asLong();

		m_saCommand.Close();
		sSQL.Format(_T("select sum(tdcls1_numof) as 'counted' from %s where tdcls1_trakt_id=:1 and tdcls1_plot_id=:2"),
								TBL_TRAKT_DCLS1_TREES); 

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= plot_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
			ant += m_saCommand.Field(_T("counted")).asLong();

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return 0;
	}

	return ant;
}


// Intermidiate function for Smaple- and DCLS-trees; 080310 p�d
// This function'll count rand trees that matches a diameterclass
// int esti_dcls_trees_table; 080310 p�d
BOOL CUMEstimateDB::getNumOfRandTreesPerSpcAndDCLS(int trakt_id,vecRandTreeCounter &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.ttree_spc_id,a.ttree_plot_id,a.ttree_plot_id,a.ttree_dcls_from,a.ttree_dcls_to,")
								_T("count(a.ttree_spc_id) as 'numof' from %s a where a.ttree_trakt_id=:1 and (a.ttree_tree_type=:2 or a.ttree_tree_type=:3) and a.ttree_dcls_from IN ")
								_T("(select b.tdcls_dcls_from from %s b where b.tdcls_trakt_id=a.ttree_trakt_id and b.tdcls_dcls_to=a.ttree_dcls_to and b.tdcls_spc_id=a.ttree_spc_id) ")
								_T("group by a.ttree_spc_id,a.ttree_plot_id,a.ttree_plot_id,a.ttree_dcls_from,a.ttree_dcls_to"),
								TBL_TRAKT_SAMPLE_TREES,
								TBL_TRAKT_DCLS_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE;
		m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(RAND_TREE_COUNTER(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			(LPCTSTR)m_saCommand.Field(3).asString(),
																			m_saCommand.Field(4).asDouble(),
																			m_saCommand.Field(5).asDouble(),
																		  m_saCommand.Field(6).asLong()));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::updNumOfRandTreesPerSpcAndDCLS(int trakt_id,vecRandTreeCounter &vec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Get "original" data from database, to compare with; 090317 p�d
	//vecRandTreeCounter vec1;
	//getNumOfRandTreesPerSpcAndDCLS(trakt_id,vec1);
	//---------------------------------------------------------------------
	try
	{
		// Make sure the "trakt" exists and that there's data to update; 080310 p�d
		if (traktDCLSTreeExists(trakt_id)) // && vec.size() > 0 && vec1.size() == vec.size())
		{
			sSQL.Format(_T("update %s set tdcls_numof_randtrees=:1,created=GETDATE() where tdcls_trakt_id=:2 and tdcls_plot_id=:3 and tdcls_spc_id=:4 and tdcls_dcls_from=:5 and tdcls_dcls_to=:6"),
							TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			for (UINT i = 0;i < vec.size();i++)
			{
				m_saCommand.Param(1).setAsLong()		= vec[i].nNumOf;
				m_saCommand.Param(2).setAsLong()		= trakt_id;
				m_saCommand.Param(3).setAsLong()		= vec[i].nPlotID;
				m_saCommand.Param(4).setAsLong()		= vec[i].nSpcID;
				m_saCommand.Param(5).setAsDouble()	= vec[i].fDCLS_From;
				m_saCommand.Param(6).setAsDouble()	= vec[i].fDCLS_To;

				m_saCommand.Execute();	
			}
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;

}

// Intermidiate function for Smaple- and DCLS-trees; 080310 p�d
// This function'll count rand trees that DOES NOT matche a diameterclass
// int esti_dcls_trees_table; 080310 p�d
BOOL CUMEstimateDB::getNoNumOfRandTreesPerSpcAndDCLS(int trakt_id,double dcls,vecRandTreeCounter &vec)
{
	CString sSQL;
	double fDCLS_From,fDCLS_To;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.ttree_spc_id,a.ttree_plot_id,a.ttree_spc_name,a.ttree_dcls_from,a.ttree_dcls_to,")
								_T("count(a.ttree_spc_id) as 'numof' from %s a where a.ttree_trakt_id=:1 and (a.ttree_tree_type=:2 or a.ttree_tree_type=:3) and a.ttree_dcls_from NOT IN ")
								_T("(select b.tdcls_dcls_from from %s b where b.tdcls_trakt_id=a.ttree_trakt_id and b.tdcls_plot_id = a.ttree_plot_id and b.tdcls_dcls_to=a.ttree_dcls_to and b.tdcls_spc_id=a.ttree_spc_id) ")
								_T("group by a.ttree_spc_id,a.ttree_plot_id,a.ttree_spc_name,a.ttree_dcls_from,a.ttree_dcls_to"),
								TBL_TRAKT_SAMPLE_TREES,
								TBL_TRAKT_DCLS_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE;
		m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			fDCLS_From = m_saCommand.Field(4).asDouble();
			fDCLS_To = m_saCommand.Field(5).asDouble();

			vec.push_back(RAND_TREE_COUNTER(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			(LPCTSTR)m_saCommand.Field(3).asString(),
																			fDCLS_From,
																			fDCLS_To,
																		  m_saCommand.Field(6).asLong()));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}
//Gjort om s� denna funktion kollar om diameterklassen redan finns i s� fall k�r en update, annars en insert, 
//var bara insert f�rut d�rav dubbla diameterklasser 20121128 J� #3452
BOOL CUMEstimateDB::addNoNumOfRandTreesPerSpcAndDCLS(int trakt_id,vecRandTreeCounter &vec)
{
	CString sSQL,sSQL2,S;
	BOOL bReturn = FALSE;
	int nID = -1,doInsert=0;

	vecDiamClExists vecDiameterClassExists;

	_rand_tree_match strRandTreeMatch;

	try
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			//Run a check on each randtree in the vecRandTreeCounter to set if the diameterclass exists in the trakt_dcls_tree table or not and rettreive the number of randtrees for that class				
			sSQL.Format(_T("select tdcls_id,tdcls_numof_randtrees from %s where tdcls_trakt_id=:1 and tdcls_plot_id=:2 and tdcls_dcls_from=:3 and tdcls_dcls_to=:4 and tdcls_spc_id=:5"),
				TBL_TRAKT_DCLS_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= trakt_id;
			m_saCommand.Param(2).setAsLong()	= vec[i].nPlotID;
			m_saCommand.Param(3).setAsDouble()	= vec[i].fDCLS_From;
			m_saCommand.Param(4).setAsDouble()	= vec[i].fDCLS_To;
			m_saCommand.Param(5).setAsLong()	= vec[i].nSpcID;
			m_saCommand.Execute();
			while(m_saCommand.FetchNext())
			{
				strRandTreeMatch.num_of_rand_trees=m_saCommand.Field(2).asLong();
				if(m_saCommand.Field(1).asLong()>0)
					strRandTreeMatch.matched_diam_class=1;
				else
					strRandTreeMatch.matched_diam_class=0;
				vecDiameterClassExists.push_back(strRandTreeMatch);
			}
		}
		doCommit();
			
		if (vec.size() > 0)
		{

				nID = getMaxDCLSTreeID(trakt_id);
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth,tdcls_numof_randtrees) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)"),
										TBL_TRAKT_DCLS_TREES);

				sSQL2.Format(_T("update %s set tdcls_numof_randtrees=:1,created=GETDATE() where tdcls_trakt_id=:2 and tdcls_plot_id=:3 and tdcls_spc_id=:4 and tdcls_dcls_from=:5 and tdcls_dcls_to=:6"),
							TBL_TRAKT_DCLS_TREES);

				for (UINT i = 0;i < vec.size();i++)
				{					
					if(vecDiameterClassExists.size()<1)
					{
						doInsert=1;
					}
					else
					{
						if(vecDiameterClassExists[i].matched_diam_class<=0)
							doInsert=1;
						else
							doInsert=0;
					}
					if(doInsert)
					{
						//If diameterclass does not exists run an insert
						m_saCommand.setCommandText((SAString)sSQL);

						m_saCommand.Param(1).setAsLong()	= nID + i + 1;
						m_saCommand.Param(2).setAsLong()	= trakt_id;
						m_saCommand.Param(3).setAsLong()	= vec[i].nPlotID;
						m_saCommand.Param(4).setAsLong()	= vec[i].nSpcID;
						m_saCommand.Param(5).setAsString()	= vec[i].sSpcname;
						m_saCommand.Param(6).setAsDouble()	= vec[i].fDCLS_From;
						m_saCommand.Param(7).setAsDouble()	= vec[i].fDCLS_To;
						m_saCommand.Param(8).setAsDouble()	= 0.0;
						m_saCommand.Param(9).setAsLong()	= 0;
						m_saCommand.Param(10).setAsDouble()	= 0.0;
						m_saCommand.Param(11).setAsDouble()	= 0.0;
						m_saCommand.Param(12).setAsDouble()	= 0.0;
						m_saCommand.Param(13).setAsDouble()	= 0.0;
						m_saCommand.Param(14).setAsDouble()	= 0.0;
						m_saCommand.Param(15).setAsDouble()	= 0.0;
						m_saCommand.Param(16).setAsLong()	= 0;
						m_saCommand.Param(17).setAsLong()	= 0;
						m_saCommand.Param(18).setAsLong()	= vec[i].nNumOf;

						m_saCommand.Execute();
					}
					else 
					{
						//If diameterclass does exists run an update on the number of randttrees for that class					
						m_saCommand.setCommandText((SAString)sSQL2);

						for (UINT i = 0;i < vec.size();i++)
						{
							m_saCommand.Param(1).setAsLong()	= vecDiameterClassExists[i].num_of_rand_trees+1;
							m_saCommand.Param(2).setAsLong()	= trakt_id;
							m_saCommand.Param(3).setAsLong()	= vec[i].nPlotID;
							m_saCommand.Param(4).setAsLong()	= vec[i].nSpcID;
							m_saCommand.Param(5).setAsDouble()	= vec[i].fDCLS_From;
							m_saCommand.Param(6).setAsDouble()	= vec[i].fDCLS_To;

							m_saCommand.Execute();	
						}
					}
				}
				doCommit();
			}
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}
/*
BOOL CUMEstimateDB::addNoNumOfRandTreesPerSpcAndDCLS(int trakt_id,vecRandTreeCounter &vec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	int nID = -1;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
//		if (traktDCLSTreeExists(trakt_id) && vec.size() > 0)
		// Changed to check only if there's data in vector, not if there's data in Trakt; 100223 p�d
		if (vec.size() > 0)
		{
				nID = getMaxDCLSTreeID(trakt_id);
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth,tdcls_numof_randtrees) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)"),
										TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL);

				for (UINT i = 0;i < vec.size();i++)
				{
					m_saCommand.Param(1).setAsLong()			= nID + i + 1;
					m_saCommand.Param(2).setAsLong()			= trakt_id;
					m_saCommand.Param(3).setAsLong()			= vec[i].nPlotID;
					m_saCommand.Param(4).setAsLong()			= vec[i].nSpcID;
					m_saCommand.Param(5).setAsString()		= vec[i].sSpcname;
					m_saCommand.Param(6).setAsDouble()		= vec[i].fDCLS_From;
					m_saCommand.Param(7).setAsDouble()		= vec[i].fDCLS_To;
					m_saCommand.Param(8).setAsDouble()		= 0.0;
					m_saCommand.Param(9).setAsLong()			= 0;
					m_saCommand.Param(10).setAsDouble()		= 0.0;
					m_saCommand.Param(11).setAsDouble()		= 0.0;
					m_saCommand.Param(12).setAsDouble()		= 0.0;
					m_saCommand.Param(13).setAsDouble()		= 0.0;
					m_saCommand.Param(14).setAsDouble()		= 0.0;
					m_saCommand.Param(15).setAsDouble()		= 0.0;
					m_saCommand.Param(16).setAsLong()		= 0;
					m_saCommand.Param(17).setAsLong()		= 0;
					m_saCommand.Param(18).setAsLong()		= vec[i].nNumOf;

					m_saCommand.Execute();
				}
				doCommit();
			}
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}*/


///////////////////////////////////////////////////////////////////////////////
// Handle Assortnent for Tree; 070510 p�d
BOOL CUMEstimateDB::getAssTrees(int trakt_id,vecTransactionTreeAssort &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tassort_trakt_id=:1"),
					TBL_TRAKT_TREES_ASSORT);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			//SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_tree_assort(m_saCommand.Field(1).asLong(),
																						m_saCommand.Field(2).asLong(),
																						m_saCommand.Field(3).asDouble(),
																						m_saCommand.Field(4).asLong(),
																						(LPCTSTR)m_saCommand.Field(5).asString(),
																						(LPCTSTR)m_saCommand.Field(6).asString(),
																						m_saCommand.Field(7).asDouble(),
																						m_saCommand.Field(8).asDouble(),
																						m_saCommand.Field(9).asLong()));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getAssTree(CTransaction_tree_assort rec,CTransaction_tree_assort &rec1)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where where tassort_tree_id=:1 and tassort_tree_trakt_id=:2 and tassort_tree_dcls=:3"),
					TBL_TRAKT_TREES_ASSORT);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTreeID();
		m_saCommand.Param(2).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(3).setAsDouble()	= rec.getDCLS();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			//SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_tree_assort(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asDouble(),
																			m_saCommand.Field(4).asLong(),
																			(LPCTSTR)m_saCommand.Field(5).asString(),
																			(LPCTSTR)m_saCommand.Field(6).asString(),
																			m_saCommand.Field(7).asDouble(),
																			m_saCommand.Field(8).asDouble(),
																			m_saCommand.Field(9).asLong());

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::addAssTree(CTransaction_tree_assort &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktAssTreeExists(rec))
		{
			sSQL.Format(_T("insert into %s (tassort_tree_trakt_id,tassort_tree_id,tassort_tree_dcls,tassort_spc_id,tassort_spc_name,tassort_assort_name,")
									_T("tassort_exch_volume,tassort_exch_price,tassort_price_in) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),
									TBL_TRAKT_TREES_ASSORT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()			= rec.getTreeID();
			m_saCommand.Param(3).setAsDouble()		= rec.getDCLS();
			m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(5).setAsString()		= rec.getSpcName();
			m_saCommand.Param(6).setAsString()		= rec.getAssortName();
			m_saCommand.Param(7).setAsDouble()		= rec.getExchVolume();
			m_saCommand.Param(8).setAsDouble()		= rec.getExchPrice();
			m_saCommand.Param(9).setAsLong()			= rec.getPriceIn();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updAssTree(CTransaction_tree_assort &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Get "original" data from database, to compare with; 090317 p�d
	//CTransaction_tree_assort rec1;
	//getAssTree(rec,rec1);
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------
	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktAssTreeExists(rec))
		//{
			sSQL.Format(_T("update %s set tassort_spc_id=:1,tassort_spc_name=:2,tassort_assort_name=:3,tassort_exch_volume=:4,tassort_exch_price=:5,tassort_price_in=:6,created=GETDATE() ")
									_T("where tassort_tree_id=:7 and tassort_tree_trakt_id=:8 and tassort_tree_dcls=:9"),TBL_TRAKT_TREES_ASSORT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(2).setAsString()		= rec.getSpcName();
			m_saCommand.Param(3).setAsString()		= rec.getAssortName();
			m_saCommand.Param(4).setAsDouble()		= rec.getExchVolume();
			m_saCommand.Param(5).setAsDouble()		= rec.getExchPrice();
			m_saCommand.Param(6).setAsLong()			= rec.getPriceIn();

			m_saCommand.Param(7).setAsLong()			= rec.getTreeID();
			m_saCommand.Param(8).setAsLong()			= rec.getTraktID();
			m_saCommand.Param(9).setAsDouble()		= rec.getDCLS();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::delAssTree(CTransaction_tree_assort &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssTreeExists(rec))
		//{

			sSQL.Format(_T("delete from %s where tassort_tree_id=:1 and tassort_tree_trakt_id=:2"),
							TBL_TRAKT_TREES_ASSORT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(2).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}



BOOL CUMEstimateDB::delAssTree(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktAssTreeExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where tassort_tree_trakt_id=:1"),
							TBL_TRAKT_TREES_ASSORT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// "Rotpost"; 071011 p�d
BOOL CUMEstimateDB::getRotpost(vecCTransaction_trakt_rotpost &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),
					TBL_TRAKT_ROTPOST);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost(m_saCommand.Field(1).asLong(),
																							 m_saCommand.Field(2).asDouble(),
																							 m_saCommand.Field(3).asDouble(),
																							 m_saCommand.Field(4).asDouble(),
																							 m_saCommand.Field(5).asDouble(),
																							 m_saCommand.Field(6).asDouble(),
																							 m_saCommand.Field(7).asDouble(),
																							 m_saCommand.Field(8).asDouble(),
																							 m_saCommand.Field(9).asDouble(),
																							 m_saCommand.Field(10).asDouble(),
																							 m_saCommand.Field(11).asDouble(),
																							 m_saCommand.Field(12).asDouble(),
																							 m_saCommand.Field(13).asDouble(),
																							 m_saCommand.Field(14).asShort(),
																							 (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpost(vecCTransaction_trakt_rotpost &vec,int trakt_id)
{
	CString sSQL,S;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where rot_trakt_id=:1"),TBL_TRAKT_ROTPOST);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost(m_saCommand.Field(1).asLong(),
																							 m_saCommand.Field(2).asDouble(),
																							 m_saCommand.Field(3).asDouble(),
																							 m_saCommand.Field(4).asDouble(),
																							 m_saCommand.Field(5).asDouble(),
																							 m_saCommand.Field(6).asDouble(),
																							 m_saCommand.Field(7).asDouble(),
																							 m_saCommand.Field(8).asDouble(),
																							 m_saCommand.Field(9).asDouble(),
																							 m_saCommand.Field(10).asDouble(),
																							 m_saCommand.Field(11).asDouble(),
																							 m_saCommand.Field(12).asDouble(),
																							 m_saCommand.Field(13).asDouble(),
																							 m_saCommand.Field(14).asShort(),
																							 (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpost(CTransaction_trakt_rotpost &rec,int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where rot_trakt_id=:1"),TBL_TRAKT_ROTPOST);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				rec = CTransaction_trakt_rotpost(m_saCommand.Field(1).asLong(),
																				 m_saCommand.Field(2).asDouble(),
																				 m_saCommand.Field(3).asDouble(),
																				 m_saCommand.Field(4).asDouble(),
																				 m_saCommand.Field(5).asDouble(),
																				 m_saCommand.Field(6).asDouble(),
																				 m_saCommand.Field(7).asDouble(),
																				 m_saCommand.Field(8).asDouble(),
																				 m_saCommand.Field(9).asDouble(),
																				 m_saCommand.Field(10).asDouble(),
																				 m_saCommand.Field(11).asDouble(),
																				 m_saCommand.Field(12).asDouble(),
																				 m_saCommand.Field(13).asDouble(),
																				 m_saCommand.Field(14).asShort(),
																				 (LPCTSTR)convertSADateTime(saDateTime));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::addRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktRotpostExists(rec))
		{
			sSQL.Format(_T("insert into %s (rot_trakt_id,rot_post_volume,rot_post_value,rot_post_cost1,rot_post_cost2,rot_post_cost3,rot_post_cost4,")
									_T("rot_post_cost5,rot_post_cost6,rot_post_cost7,rot_post_cost8,rot_post_cost9,rot_post_netto,rot_post_origin) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)"),
									TBL_TRAKT_ROTPOST);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getRotTraktID();
			m_saCommand.Param(2).setAsDouble()		= rec.getVolume();
			m_saCommand.Param(3).setAsDouble()		= rec.getValue();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(5).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(6).setAsDouble()		= rec.getCost3();
			m_saCommand.Param(7).setAsDouble()		= rec.getCost4();
			m_saCommand.Param(8).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(9).setAsDouble()		= rec.getCost6();
			m_saCommand.Param(10).setAsDouble()		= rec.getCost7();
			m_saCommand.Param(11).setAsDouble()		= rec.getCost8();
			m_saCommand.Param(12).setAsDouble()		= rec.getCost9();
			m_saCommand.Param(13).setAsDouble()		= rec.getNetto();
			m_saCommand.Param(14).setAsShort()		= rec.getOrigin();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	//---------------------------------------------------------------------
	// Get "original" data from database, to compare with; 090317 p�d
	//CTransaction_trakt_rotpost rec1;
	//getRotpost(rec1,rec.getRotTraktID());
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktRotpostExists(rec))
		//{
			sSQL.Format(_T("update %s set rot_post_volume=:1,rot_post_value=:2,rot_post_cost1=:3,rot_post_cost2=:4,rot_post_cost3=:5,rot_post_cost4=:6,rot_post_cost5=:7,rot_post_cost6=:8,rot_post_cost7=:9,rot_post_cost8=:10,")
									_T("rot_post_cost9=:11,rot_post_netto=:12,rot_post_origin=:13,created=GETDATE() where rot_trakt_id=:14"),
									TBL_TRAKT_ROTPOST);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()		= rec.getVolume();
			m_saCommand.Param(2).setAsDouble()		= rec.getValue();
			m_saCommand.Param(3).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(5).setAsDouble()		= rec.getCost3();
			m_saCommand.Param(6).setAsDouble()		= rec.getCost4();
			m_saCommand.Param(7).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(8).setAsDouble()		= rec.getCost6();
			m_saCommand.Param(9).setAsDouble()		= rec.getCost7();
			m_saCommand.Param(10).setAsDouble()		= rec.getCost8();
			m_saCommand.Param(11).setAsDouble()		= rec.getCost9();
			m_saCommand.Param(12).setAsDouble()		= rec.getNetto();
			m_saCommand.Param(13).setAsShort()		= rec.getOrigin();

			m_saCommand.Param(14).setAsLong()		= rec.getRotTraktID();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;

}
BOOL CUMEstimateDB::delRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktRotpostExists(rec))
		//{

			sSQL.Format(_T("delete from %s where rot_trakt_id=:1"),
							TBL_TRAKT_ROTPOST);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getRotTraktID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


/////////////////////////////////////////////////////////////////////////
// "Rotpost Transport och Huggningskostnader per tr�dslag"; 071011 p�d
BOOL CUMEstimateDB::getRotpostSpc(vecTransaction_trakt_rotpost_spc &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),
					TBL_TRAKT_ROTPOST_SPC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost_spc(m_saCommand.Field(1).asLong(),
																										 m_saCommand.Field(2).asLong(),
																										 (LPCTSTR)m_saCommand.Field(3).asString(),
																										 m_saCommand.Field(4).asDouble(),
																										 m_saCommand.Field(5).asDouble(),
																										 m_saCommand.Field(6).asDouble(),
																										 m_saCommand.Field(7).asDouble(),
																										 m_saCommand.Field(8).asDouble(),
																										 m_saCommand.Field(9).asDouble(),
																										 m_saCommand.Field(10).asShort(),
																										 (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpostSpc(vecTransaction_trakt_rotpost_spc &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where spcrot_trakt_id=:1"),
						TBL_TRAKT_ROTPOST_SPC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost_spc(m_saCommand.Field(1).asLong(),
																										 m_saCommand.Field(2).asLong(),
																										 (LPCTSTR)m_saCommand.Field(3).asString(),
																										 m_saCommand.Field(4).asDouble(),
																										 m_saCommand.Field(5).asDouble(),
																										 m_saCommand.Field(6).asDouble(),
																										 m_saCommand.Field(7).asDouble(),
																										 m_saCommand.Field(8).asDouble(),
																										 m_saCommand.Field(9).asDouble(),
																										 m_saCommand.Field(10).asShort(),
																										 (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpostSpc(CTransaction_trakt_rotpost_spc &rec,int trakt_id,int rot_id )
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where where spcrot_id=:1 and spcrot_trakt_id=:2"),
						TBL_TRAKT_ROTPOST_SPC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rot_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				rec = CTransaction_trakt_rotpost_spc(m_saCommand.Field(1).asLong(),
																						 m_saCommand.Field(2).asLong(),
																						 (LPCTSTR)m_saCommand.Field(3).asString(),
																						 m_saCommand.Field(4).asDouble(),
																						 m_saCommand.Field(5).asDouble(),
																						 m_saCommand.Field(6).asDouble(),
																						 m_saCommand.Field(7).asDouble(),
																						 m_saCommand.Field(8).asDouble(),
																						 m_saCommand.Field(9).asDouble(),
																						 m_saCommand.Field(10).asShort(),
																						 (LPCTSTR)convertSADateTime(saDateTime));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::addRotpostSpc(CTransaction_trakt_rotpost_spc &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktRotpostSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (spcrot_id,spcrot_trakt_id,spcrot_post_spcname,spcrot_post_cost1,spcrot_post_cost2,spcrot_post_cost3,spcrot_post_cost4,")
									_T("spcrot_post_cost5,spcrot_post_cost6,spcrot_post_origin) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)"),TBL_TRAKT_ROTPOST_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getRotSpcID_pk();
			m_saCommand.Param(2).setAsLong()			= rec.getRotSpcTraktID_pk();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(5).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(6).setAsDouble()		= rec.getCost3();
			m_saCommand.Param(7).setAsDouble()		= rec.getCost4();
			m_saCommand.Param(8).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(9).setAsDouble()		= rec.getCost6();
			m_saCommand.Param(10).setAsShort()		= rec.getOrigin();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updRotpostSpc(CTransaction_trakt_rotpost_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	//---------------------------------------------------------------------
	// Get "original" data from database, to compare with; 090317 p�d
	//CTransaction_trakt_rotpost_spc rec1;
	//getRotpostSpc(rec1,rec.getRotSpcTraktID_pk(),rec.getRotSpcID_pk());
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktRotpostSpcExists(rec))
		//{
			sSQL.Format(_T("update %s set spcrot_post_spcname=:1,spcrot_post_cost1=:2,spcrot_post_cost2=:3,spcrot_post_cost3=:4,spcrot_post_cost4=:5,spcrot_post_cost5=:6,spcrot_post_cost6=:7,")
									_T("spcrot_post_origin=:8,created=GETDATE() where spcrot_id=:9 and spcrot_trakt_id=:10"),TBL_TRAKT_ROTPOST_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()		= rec.getSpcName();
			m_saCommand.Param(2).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(3).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost3();
			m_saCommand.Param(5).setAsDouble()		= rec.getCost4();
			m_saCommand.Param(6).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(7).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(8).setAsShort()			= rec.getOrigin();

			m_saCommand.Param(9).setAsLong()			= rec.getRotSpcID_pk();
			m_saCommand.Param(10).setAsLong()		= rec.getRotSpcTraktID_pk();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;

}
BOOL CUMEstimateDB::delRotpostSpc(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktRotpostSpcExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where spcrot_trakt_id=:1"),
							TBL_TRAKT_ROTPOST_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


/////////////////////////////////////////////////////////////////////////
// "Rotpost �vriga kostnader"; 080312
BOOL CUMEstimateDB::getRotpostOtc(vecTransaction_trakt_rotpost_other &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),
						TBL_TRAKT_ROTPOST_OTC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost_other(m_saCommand.Field(1).asLong(),
																											m_saCommand.Field(2).asLong(),
																											m_saCommand.Field(3).asDouble(),
																											m_saCommand.Field(4).asDouble(),
																											(LPCTSTR)m_saCommand.Field(5).asString(),
																											m_saCommand.Field(6).asDouble(),
																											m_saCommand.Field(7).asShort(),
																										  (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpostOtc(vecTransaction_trakt_rotpost_other &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where otcrot_trakt_id=:1"),TBL_TRAKT_ROTPOST_OTC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_trakt_rotpost_other(m_saCommand.Field(1).asLong(),
																										   m_saCommand.Field(2).asLong(),
																											 m_saCommand.Field(3).asDouble(),
																											 m_saCommand.Field(4).asDouble(),
																											 (LPCTSTR)m_saCommand.Field(5).asString(),
																											 m_saCommand.Field(6).asDouble(),
																											 m_saCommand.Field(7).asShort(),
																										   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::getRotpostOtc(CTransaction_trakt_rotpost_other &rec,int trakt_id,int otc_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where otcrot_id=:1 and otcrot_trakt_id=:2"),TBL_TRAKT_ROTPOST_OTC);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= otc_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				rec = CTransaction_trakt_rotpost_other(m_saCommand.Field(1).asLong(),
																						   m_saCommand.Field(2).asLong(),
																							 m_saCommand.Field(3).asDouble(),
																							 m_saCommand.Field(4).asDouble(),
																							 (LPCTSTR)m_saCommand.Field(5).asString(),
																							 m_saCommand.Field(6).asDouble(),
																							 m_saCommand.Field(7).asShort(),
																						   (LPCTSTR)convertSADateTime(saDateTime));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;

}

BOOL CUMEstimateDB::addRotpostOtc(CTransaction_trakt_rotpost_other &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktRotpostOtcExists(rec))
		{
			sSQL.Format(_T("insert into %s (otcrot_id,otcrot_trakt_id,otcrot_value,otcrot_sum_value,otcrot_text,otcrot_percent,otcrot_type) values(:1,:2,:3,:4,:5,:6,:7)"),
									TBL_TRAKT_ROTPOST_OTC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()			= rec.getOtcRotID_pk();
			m_saCommand.Param(2).setAsLong()			= rec.getOtcRotTraktID_pk();
			m_saCommand.Param(3).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(5).setAsString()		= rec.getText();
			m_saCommand.Param(6).setAsDouble()		= rec.getPercent();
			m_saCommand.Param(7).setAsShort()			= rec.getType();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;
}

BOOL CUMEstimateDB::updRotpostOtc(CTransaction_trakt_rotpost_other &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//---------------------------------------------------------------------
	// Get "original" data from database, to compare with; 090317 p�d
	//CTransaction_trakt_rotpost_other rec1;
	//getRotpostOtc(rec1,rec.getOtcRotTraktID_pk(),rec.getOtcRotID_pk());
	//if (rec1 == rec) return FALSE;
	//---------------------------------------------------------------------

	try
	{
		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		//if (traktRotpostOtcExists(rec))
		//{
			sSQL.Format(_T("update %s set otcrot_value=:1,otcrot_sum_value=:2,otcrot_text=:3,otcrot_text=:4,otcrot_type=:5,created=GETDATE() where otcrot_id=:6 and otcrot_trakt_id=:7"),
									TBL_TRAKT_ROTPOST_OTC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(2).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(3).setAsString()		= rec.getText();
			m_saCommand.Param(4).setAsDouble()		= rec.getPercent();
			m_saCommand.Param(5).setAsShort()			= rec.getType();

			m_saCommand.Param(6).setAsLong()			= rec.getOtcRotID_pk();
			m_saCommand.Param(7).setAsLong()			= rec.getOtcRotTraktID_pk();

			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}


	return bReturn;

}
BOOL CUMEstimateDB::delRotpostOtc(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktRotpostOtcExists(trakt_id))
		//{

			sSQL.Format(_T("delete from %s where otcrot_trakt_id=:1"),
									TBL_TRAKT_ROTPOST_OTC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


///////////////////////////////////////////////////////////////////////////////
// External database items.
// Properties is set in Forrest SUITE; 070305 p�d

BOOL CUMEstimateDB::getProperties(vecTransactionProperty &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),
						TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asLong(),
																				 (LPCTSTR)m_saCommand.Field(2).asString(),
																				 (LPCTSTR)m_saCommand.Field(3).asString(),
																				 (LPCTSTR)m_saCommand.Field(4).asString(),
																				 (LPCTSTR)m_saCommand.Field(5).asString(),
																				 (LPCTSTR)m_saCommand.Field(6).asString(),
																				 (LPCTSTR)m_saCommand.Field(7).asString(),
																				 (LPCTSTR)m_saCommand.Field(8).asString(),
																				 (LPCTSTR)m_saCommand.Field(9).asString(),
																				 (LPCTSTR)m_saCommand.Field(10).asString(),
																				 (LPCTSTR)m_saCommand.Field(11).asString(),
																				 m_saCommand.Field(12).asDouble(),
																				 m_saCommand.Field(13).asDouble(),
																				 (LPCTSTR)m_saCommand.Field(14).asString(),
																				 (LPCTSTR)convertSADateTime(saDateTime),
																				 (LPCTSTR)m_saCommand.Field(16).asString(),
																				 (LPCTSTR)m_saCommand.Field(17).asString(),
																				 (LPCTSTR)m_saCommand.Field(18).asString() ));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getProperties(LPCTSTR sql,vecTransactionProperty &vec)
{
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sql);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asLong(),
																				 (LPCTSTR)m_saCommand.Field(2).asString(),
																				 (LPCTSTR)m_saCommand.Field(3).asString(),
																				 (LPCTSTR)m_saCommand.Field(4).asString(),
																				 (LPCTSTR)m_saCommand.Field(5).asString(),
																				 (LPCTSTR)m_saCommand.Field(6).asString(),
																				 (LPCTSTR)m_saCommand.Field(7).asString(),
																				 (LPCTSTR)m_saCommand.Field(8).asString(),
																				 (LPCTSTR)m_saCommand.Field(9).asString(),
																				 (LPCTSTR)m_saCommand.Field(10).asString(),
																				 (LPCTSTR)m_saCommand.Field(11).asString(),
																				 m_saCommand.Field(12).asDouble(),
																				 m_saCommand.Field(13).asDouble(),
																				 (LPCTSTR)m_saCommand.Field(14).asString(),
																				 (LPCTSTR)m_saCommand.Field(15).asString(),
																				 (LPCTSTR)m_saCommand.Field(16).asString(),
																				 (LPCTSTR)m_saCommand.Field(17).asString(),
																				 (LPCTSTR)m_saCommand.Field(18).asString() ));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::getSpecies(vecTransactionSpecies &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),
						TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_species(-1,
				m_saCommand.Field(1).asLong(),
				m_saCommand.Field(_T("p30_spec")).asShort(),
				(LPCTSTR)m_saCommand.Field(2).asString(),
				(LPCTSTR)m_saCommand.Field(3).asString(),
				(LPCTSTR)convertSADateTime(saDateTime)));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::getPricelists(vecTransactionPricelist &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),
						TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_pricelist(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																		m_saCommand.Field(3).asLong(),
																	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::getTemplates(vecTransactionTemplate &vec,int tmpl_type)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where tmpl_type_of=:1"),
						TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= tmpl_type;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::getCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where cost_type_of=:1 or cost_type_of=:2"),
						TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= COST_TYPE_1;	// "Kostnadsmall f�r Rotpost"
		m_saCommand.Param(2).setAsLong()		= COST_TYPE_2;	// "Kostnadsmall f�r Intr�ngsv�rdering"
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

//Lagt till function f�r att plocka alla kostnader fr�n kostnadstabellen oavsett typ
// Bug #2368 20101011 J�
BOOL CUMEstimateDB::getAllCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


// Special function combining 2 or more tables; 071024 p�d
// Changed; added b.tassort_price_in=3 because of change
// int Pricelist. Set timberprice in m3to=0 or m3fub=3; 080424 p�d
BOOL CUMEstimateDB::getExchPriceCalulatedInExchangeModule(int trakt_id,vecExchCalcInfo &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select a.tass_trakt_id,a.tass_trakt_data_id,a.tass_assort_id,b.tassort_exch_price,b.tassort_price_in from %s a,%s b where b.tassort_tree_trakt_id=a.tass_trakt_id and a.tass_trakt_id=:1 ")
								_T("and (b.tassort_price_in=0 or b.tassort_price_in=3) and b.tassort_assort_name=a.tass_assort_name and b.tassort_spc_id=a.tass_trakt_data_id"),
			TBL_TRAKT_SPC_ASS,
			TBL_TRAKT_TREES_ASSORT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_exch_calc_info_struct(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asDouble(),
																					 m_saCommand.Field(5).asLong()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMEstimateDB::isStandInObjectCruise(int ecru_id)
{
	CString sSQL;
	short nCounter = 0;
	try
	{
	
		sSQL.Format(_T("select * from %s where ecru_id=:1"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= ecru_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())	nCounter++;

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((const TCHAR*)e.ErrText());
		doRollback();
		return FALSE;
	}
	// If nCounter > 0 => There's a match for ecru_id (I.e. StandID); 090331 p�d
	return (nCounter > 0);
}

int CUMEstimateDB::isHeightCurveInDB(int trakt_id)
{
	CString sSQL,S;
	int nSize = 0;
	try
	{
	
		sSQL.Format(_T("select length=datalength(trakt_hgtcurve) from %s where trakt_id=:1"),TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())	
			nSize = m_saCommand.Field(L"length").asLong();

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((const TCHAR*)e.ErrText());
		doRollback();
		return 0;
	}
	// If nCounter > 0 => There's a match for ecru_id (I.e. StandID); 090331 p�d
	return nSize;
}

BOOL CUMEstimateDB::isTraktConnectedToShape(int trakt_id)
{
	BOOL ret = FALSE;
	CString sSQL;
	CStringList layers;
	POSITION pos;

	try
	{
		// Look for layers of stand type
		m_saCommand.setCommandText(_T("IF OBJECT_ID ('gis_layers', 'U') IS NOT NULL SELECT name FROM gis_layers WHERE [type] = 1"));
		m_saCommand.Execute();
		if (m_saCommand.isResultSet())
		{
			while(m_saCommand.FetchNext())
			{
				layers.AddTail(m_saCommand.Field(1).asString());
			}
		}
		doCommit();

		pos = layers.GetHeadPosition();
		while( pos )
		{
			// Look for stand relation in each table
			sSQL.Format(_T("SELECT 1 FROM gis_%s_FEA WHERE RELATIONID = %d AND SUBRELATIONID IS NULL"), layers.GetNext(pos), trakt_id);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			while(m_saCommand.FetchNext())
			{
				ret = TRUE;
				// Intentionally no break here as we want to loop through every record (SQLApi bug??)
			}
			doCommit();
		}
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((const TCHAR*)e.ErrText());
		return FALSE;
	}

	return ret;
}


//---------------------------------------------------------------------------
//	DATASECURITY HANDLING; 090317 p�d
// Client/Server specifics; 090317 p�d

// Added 0900317 p�d
short CUMEstimateDB::checkTraktDate(CTransaction_trakt rec,CString& changed_by,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK

	CTransaction_trakt rec1;
	try
	{

		//if (traktExists(rec))
		//{
			getTrakt(rec.getTraktID(),rec1);

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',trakt_created_by as \'created_by\',created from %s where trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT,
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				changed_by = m_saCommand.Field(_T("created_by")).asString();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

// Added 0900317 p�d
short CUMEstimateDB::checkTraktDataDate(CTransaction_trakt_data rec,int data_type,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CTransaction_trakt_data rec1;
	try
	{

		//if (traktDataExists(rec))
		//{
			getTraktData(rec1,rec.getTDataTraktID(),rec.getSpecieID(),data_type); //rec.getTDataType());	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tdata_trakt_id=%d and tdata_spc_id=%d and tdata_data_type=%d"),
				rec.getCreated(),
				TBL_TRAKT_DATA,
				rec.getTDataTraktID(),
				rec.getSpecieID(),
				data_type);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

// Added 0900317 p�d
short CUMEstimateDB::checkTraktAssDate(CTransaction_trakt_ass rec,int data_type,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CTransaction_trakt_ass rec1;
	try
	{

		//if (traktAssExists(rec))
		//{
			getTraktAss(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tass_assort_id=%d and tass_trakt_id=%d and tass_trakt_data_id=%d and tass_data_type=%d"),
				rec.getCreated(),
				TBL_TRAKT_SPC_ASS,
				rec.getTAssID(),
				rec.getTAssTraktID(),
				rec.getTAssTraktDataID(),
				data_type);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

// Added 0900317 p�d
short CUMEstimateDB::checkTraktTransDate(CTransaction_trakt_trans rec,int data_type,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CTransaction_trakt_trans rec1;
	try
	{

		//if (traktTransExists(rec))
		//{
			getTraktTrans(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d and ttrans_from_ass_id=%d and ttrans_to_ass_id=%d and ttrans_to_spc_id=%d and ttrans_data_type=%d"),
				rec.getCreated(),
				TBL_TRAKT_TRANS,
				rec.getTTransDataID(),
				rec.getTTransTraktID(),
				rec.getTTransFromID(),
				rec.getTTransToID(),
				rec.getSpecieID(),
				data_type);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

short CUMEstimateDB::checkTraktSetSpcDate(CTransaction_trakt_set_spc rec,int data_type,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CString sDate;
	CTransaction_trakt_set_spc rec1;
	try
	{

		//if (traktSetSpcExists(rec))
		//{
			getTraktSetSpc(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tsetspc_id=%d and tsetspc_tdata_id=%d and tsetspc_tdata_trakt_id=%d and tsetspc_data_type=%d"),
				rec.getCreated(),
				TBL_TRAKT_SET_SPC,
				rec.getTSetspcID(),
				rec.getTSetspcDataID(),
				rec.getTSetspcTraktID(),
				data_type);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}


short CUMEstimateDB::checkTraktMiscDataDate(CTransaction_trakt_misc_data rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CString sDate;
	CTransaction_trakt_misc_data rec1;
	try
	{

		//if (traktMiscDataExists(rec))
		//{
			getTraktMiscData(rec.getTSetTraktID(),rec1);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tprl_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_MISC_DATA,
				rec.getTSetTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}


short CUMEstimateDB::checkTraktPlotDate(CTransaction_plot rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,S;
	short nReturn = 1;	// 1 = OK
	CString sDate;
	CTransaction_plot rec1;
	try
	{

		//if (traktPlotExists(rec))
		//{
			getPlot(rec.getTraktID(),rec.getPlotID(),rec1);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tplot_id=%d and tplot_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_PLOT,
				rec.getPlotID(),
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}


short CUMEstimateDB::checkTraktSampleTreesDate(CTransaction_sample_tree rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,sDate;
	short nReturn = 1;	// 1 = OK
	CTransaction_sample_tree rec1;
	try
	{

		//if (traktSampleTreeExists(rec))
		//{
			getSampleTree(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where ttree_id=%d and ttree_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_SAMPLE_TREES,
				rec.getTreeID(),
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

short CUMEstimateDB::checkTraktDCLSTreesDate(CTransaction_dcls_tree rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,sDate;
	short nReturn = 1;	// 1 = OK
	CTransaction_dcls_tree rec1;
	try
	{

		//if (traktDCLSTreeExists(rec))
		//{
			getDCLSTree(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tdcls_id=%d and tdcls_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_DCLS_TREES,
				rec.getTreeID(),
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

short CUMEstimateDB::checkTraktDCLS1TreesDate(CTransaction_dcls_tree rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,sDate;
	short nReturn = 1;	// 1 = OK
	CTransaction_dcls_tree rec1;
	try
	{

		//if (traktDCLS1TreeExists(rec))
		//{
			getDCLS1Tree(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_DCLS1_TREES,
				rec.getTreeID(),
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}

short CUMEstimateDB::checkTraktDCLS2TreesDate(CTransaction_dcls_tree rec,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL,sDate;
	short nReturn = 1;	// 1 = OK
	CTransaction_dcls_tree rec1;
	try
	{

		//if (traktDCLS2TreeExists(rec))
		//{
			getDCLS2Tree(rec1,rec);	// Get original record, to compare with; 090303 p�d

			if (rec.getCreated().IsEmpty() || rec1.getCreated().IsEmpty()) return 1;

			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',created from %s where tdcls2_id=%d and tdcls2_trakt_id=%d"),
				rec.getCreated(),
				TBL_TRAKT_DCLS2_TREES,
				rec.getTreeID(),
				rec.getTraktID());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = (LPCTSTR)convertSADateTime(saDateTime);
			}

			doCommit();
			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return -1;	// NOT OK
	}

	return nReturn;
}




unsigned int FromFileWriter(SAPieceType_t &ePieceType, void *pBuf, unsigned int nLen, void *pAddlData);
SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename);
void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData);


SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename)
{
	SAString s;
	//char sBuf[32*1024];
	TCHAR sBuf[32*1024];
	FILE *pFile = _tfopen(sFilename, _T("rb"));

	if(!pFile)
		SAException::throwUserException(-1, 
		(SAString)_T("Error opening file '%s'\n"), sFilename);
	do
	{
		unsigned int nRead = (unsigned int)fread(sBuf, 1*sizeof(TCHAR), sizeof(sBuf)/sizeof(TCHAR), pFile);
		s += SAString(sBuf, nRead);
	}
	while(!feof(pFile));
	
	fclose(pFile);

	return s;
}


static FILE *pFile = NULL;
static int nTotalBound;
static int nTotalRead;

unsigned int FromFileWriter(SAPieceType_t &ePieceType,	void *pBuf, unsigned int nLen, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece)
	{
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("rb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s'"), sFilename);
		
		nTotalBound = 0;
	}
	
	unsigned int nRead = (unsigned int)fread(pBuf, 1, nLen, pFile);		//TODO: might need to change this if using Unicode and TCHAR
	nTotalBound += nRead;
	// show progress
	//printf("%d bytes of file bound\n", nTotalBound);
	
	if(feof(pFile))
	{
		ePieceType = SA_LastPiece;
		fclose(pFile);
		pFile = NULL;
	}
	
	return nRead;
}

void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece || ePieceType == SA_OnePiece)
	{
		nTotalRead = 0;
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("wb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s' for writing"), sFilename);
	}

	CString csMsg;

	fwrite(pBuf, 1, nLen, pFile);
	nTotalRead += nLen;

//	csMsg.Format(_T("nLen %d\nnTotalRead %d"),nLen, nTotalRead);
//	UMMessageBox(csMsg);

	// show progress
	//printf("%d bytes of %d read\n", nTotalRead, nBlobSize);

  if(ePieceType == SA_LastPiece || ePieceType == SA_OnePiece)
	{
		fclose(pFile);
		pFile = NULL;
	}
}

BOOL CUMEstimateDB::saveImage(CString path, int id)
{
	CString csSQL;
	
	try
	{
		csSQL.Format(_T("update %s set trakt_hgtcurve=:1 where trakt_id = %d"), TBL_TRAKT, id);
		m_saCommand.setCommandText((SAString)csSQL);
		//m_saCommand.Param(1).setAsBLob() = ReadWholeFile(path);
		m_saCommand.Param(1).setAsBLob(FromFileWriter, 10*1024, (void*)(LPCTSTR)path);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		UMMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::loadImage(CString path, int id)
{
	CString csSQL,S;
	BOOL bRet = TRUE;
	short nIsNullCounter = 0;

	try
	{
		csSQL.Format(_T("select trakt_hgtcurve from %s where trakt_id=%d"), TBL_TRAKT, id);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		m_saCommand.Field(1).setLongOrLobReaderMode(SA_LongOrLobReaderManual);
		while(m_saCommand.FetchNext())
		{
				if (m_saCommand.Field(1).isNull()) 
				{
					bRet = FALSE;
				}
				else
				{
					m_saCommand.Field(1).ReadLongOrLob(IntoFileReader,	// our callback to read BLob content into file
																						 32*1024,		// our desired piece size
																						 (void*)/*(const char*)*/(LPCTSTR)path	// additional data, filename in our example
																						 );
					nIsNullCounter++;
				}
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		UMMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nIsNullCounter > 0);
}

////////////////////////////////////////////////////////////////////////
// Tree categories
//
BOOL CUMEstimateDB::GetCategories(vecTransactionSampleTreeCategory &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_sample_tree_category(m_saCommand.Field("id").asLong(),
				(LPCTSTR)(m_saCommand.Field("category").asString()),
				(LPCTSTR)(m_saCommand.Field("notes").asString()),
				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox( e.ErrText().GetWideChars() );
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMEstimateDB::addCategory(CTransaction_sample_tree_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!categoryExists(rec))
		{
			sSQL.Format(_T("insert into %s (id,category,notes) values(:1,:2,:3)"), TBL_SAMPLE_TREE_CATEGORY);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();
			m_saCommand.Param(2).setAsString() = rec.getCategory();
			m_saCommand.Param(3).setAsString() = rec.getNotes();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::updCategory(int nOldID, CTransaction_sample_tree_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set id=:1,category=:2,notes=:3 where id=:4"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getID();
		m_saCommand.Param(2).setAsString() = rec.getCategory();
		m_saCommand.Param(3).setAsString() = rec.getNotes();
		m_saCommand.Param(4).setAsLong() = nOldID;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeCategory(CTransaction_sample_tree_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where id=:1"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getID();

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMEstimateDB::removeAllCategories()
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}
//#5026 PH 20160616
BOOL CUMEstimateDB::updateTransportationToVagForRotPost(int rId,int distance) {
	CString sSQL;
	try
	{
		sSQL.Format(_T("UPDATE %s SET tsetspc_transp_dist=:1 WHERE tsetspc_tdata_trakt_id=:2"), TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = distance;
		m_saCommand.Param(2).setAsLong() = rId;

		m_saCommand.Execute();	
		doCommit();

		return TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
}
//#5026 PH 20160616
BOOL CUMEstimateDB::updateTransportationToIndustriForRotPost(int rId,int distance) {
	CString sSQL;
	try
	{
		sSQL.Format(_T("UPDATE %s SET tsetspc_transp_dist2=:1 WHERE tsetspc_tdata_trakt_id=:2"), TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = distance;
		m_saCommand.Param(2).setAsLong() = rId;

		m_saCommand.Execute();	
		doCommit();

		return TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
}


