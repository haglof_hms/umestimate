#pragma once

#include "Resource.h"
#include "UMEstimateDB.h"



/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Origin (Ursprung); 070228 p�d

/////////////////////////////////////////////////////////////////////////////
// CHgtClassReportRec

class CHgtClassReportRec : public CXTPReportRecord
{
	int m_nID;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CHgtClassReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CHgtClassReportRec(int id,CTransaction_hgt_class rec)
	{
		m_nID	= id;
		AddItem(new CIntItem(rec.getID()));
		AddItem(new CTextItem(rec.getHgtClass()));
		AddItem(new CTextItem(rec.getNotes()));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		if (item == 0)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;

	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

// CHgtClassFormView form view

class CHgtClassFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CHgtClassFormView)

// private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sHgtClassID;
	CString m_sHgtClass;
	CString m_sHgtClassNotes;
	CString m_sText1;
	CString m_sText2;
	CString m_sText3;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sMsgCap;
	CString m_sDoneSavingMsg;

	BOOL getHgtClass(void);
	vecTransactionHgtClass m_vecHgtClassData;
	BOOL isHgtClassUsed(CTransaction_hgt_class &);

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CHgtClassFormView();           // protected constructor used by dynamic creation
	virtual ~CHgtClassFormView();

	// My data members
	CMyReportCtrl m_wndReport1;

	// My methods
	void setResize(CWnd *,int x,int y,int w,int h,BOOL use_winpos = FALSE);
	BOOL setupReport1(void);
	
	BOOL populateReport(void);
	BOOL clearReport(void);

	BOOL addHgtClass(void);
	BOOL removeHgtClass(void);
	void setReportFocus(void);

public:
	// Need to be PUBLIC
	BOOL saveHgtClass(void);

	BOOL getIsDirty(void)
	{
		if (m_wndReport1.GetSafeHwnd() != NULL)
			return m_wndReport1.isDirty();
		return FALSE;
	}

	void doSetNavigationBar();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


