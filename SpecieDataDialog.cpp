// SpecieDataFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "MDITabbedView.h"
#include "PageTwoFormView.h"
#include "SpecieDataDialog.h"

#include "ResLangFileReader.h"

// CSpecieDataFormView

IMPLEMENT_DYNCREATE(CSpecieDataFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSpecieDataFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnReportValueChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT1, OnReport1ValueChanged)

	ON_BN_CLICKED(IDC_BUTTON3, &CSpecieDataFormView::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CSpecieDataFormView::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CSpecieDataFormView::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CSpecieDataFormView::OnBnClickedButton6)

//	ON_EN_CHANGE(IDC_EDIT27, OnGyChange)
//	ON_EN_CHANGE(IDC_EDIT37, OnAvgHgtChange)
ON_BN_CLICKED(IDC_BUTTON7, &CSpecieDataFormView::OnBnClickedButton7)
END_MESSAGE_MAP()

// Protected contructor
CSpecieDataFormView::CSpecieDataFormView()
	: CXTResizeFormView(CSpecieDataFormView::IDD)
{
	m_enumAction = NO_ACTION;
	m_bInitialized = FALSE;
	m_pDB = NULL;
	m_nSpecieID = -1;
	m_nSelectedTraktDataIdx = -1;
}

CSpecieDataFormView::~CSpecieDataFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

// OnClose added 07-06-27 p�d
// In this function'll call on Page three doCalculations
// based on new data entered; 070627 p�d
void CSpecieDataFormView::OnDestroy()
{
	if (getIsDirty())
	{
		runDoCalulationInPageThree();
	}

	m_vecTraktAss.clear();
	m_vecTraktData.clear();
	m_vecTraktAssTrans.clear();

	CXTResizeFormView::OnDestroy();
}

void CSpecieDataFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1_1, m_wndGroup1);

	DDX_Control(pDX, IDC_BUTTON3, m_wndBtnAddAssort);
	DDX_Control(pDX, IDC_BUTTON4, m_wndBtnDelAssort);
	DDX_Control(pDX, IDC_BUTTON5, m_wndBtnAddTransfer);
	DDX_Control(pDX, IDC_BUTTON6, m_wndBtnDelTransfer);
	DDX_Control(pDX, IDC_BUTTON7, m_wndBtnDelAllTransfers);
	//}}AFX_DATA_MAP

}

BOOL CSpecieDataFormView::PreCreateWindow(CREATESTRUCT& cs)
{
/*
	if (!CXTResizeFormView::PreCreateWindow(cs))
	{
		return FALSE;
	}

	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
*/

	return CXTResizeFormView::PreCreateWindow(cs);
}

int CSpecieDataFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	if (!m_wndPropertyGrid.m_hWnd)
	{
		m_wndPropertyGrid.Create(CRect(0, 0, 0, 0), this, 1000);
		m_wndPropertyGrid.SetOwner(this);
		m_wndPropertyGrid.ShowHelp( FALSE );
		m_wndPropertyGrid.SetViewDivider(0.5);
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.ShowWindow(SW_HIDE);
	}
*/
	if (m_wndReport.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport1.Create(this, IDC_REPORT1, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}
	return 0;
}

void CSpecieDataFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_wndBtnAddAssort.ShowWindow( SW_HIDE );
		m_wndBtnDelAssort.ShowWindow( SW_HIDE );

		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setLanguage();
	
		setupReport();
		setupReport1();

		getTraktDataFromDB();
		getTraktAssFromDB();

		m_bInitialized = TRUE;

		m_bIsDirty = FALSE;
	}
}

// CSpecieDataFormView message handlers

BOOL CSpecieDataFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CSpecieDataFormView::OnSize(UINT nType,int cx,int cy)
{
/*
	if (m_wndPropertyGrid.m_hWnd)
	{
		setResize(&m_wndPropertyGrid,20,20,160,cy-40);
	}
*/
	if (m_wndReport.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
//		setResize(&m_wndReport,190,45,cx-210,140);
		// Changed 081128 p�d
		setResize(&m_wndReport,20,15,cx-30,210);
	}

	if (m_wndReport1.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
//		setResize(&m_wndReport1,190,220,cx-210,120);
		// Changed 081128 p�d
		setResize(&m_wndReport1,20,290,cx-30,180);
	}

	if (m_wndGroup1.GetSafeHwnd())
	{
		setResize(&m_wndGroup1,2,2,cx-4,cy-4);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

// These two methods is used to calculate the volume in m3sk, based
// on data entered. Volume is calculated in:
// function double getM3SkVolFromGyAndAvgHgt(double gy,double avg_hgt) PAD_HMSFuncLib; 070402 p�d
void CSpecieDataFormView::OnGyChange(void)
{
	double fm3sk;
	double fGy = m_fBindToGY;
	double fAvgHgt = m_fBindToAvgHgt;
	if (fGy > 0.0 && fAvgHgt > 0.0)
	{
		fm3sk = getM3SkVolFromGyAndAvgHgt(fGy,fAvgHgt);
		m_fBindToVolM3Sk = fm3sk;	
	}	// if (fGy > 0.0 && fAvgHgt > 0.0)
}

void CSpecieDataFormView::OnAvgHgtChange(void)
{
	double fm3sk;
	double fGy = m_fBindToGY;
	double fAvgHgt = m_fBindToAvgHgt;
	if (fGy > 0.0 && fAvgHgt > 0.0)
	{
		fm3sk = getM3SkVolFromGyAndAvgHgt(fGy,fAvgHgt);
		m_fBindToVolM3Sk = fm3sk;	
	}	// if (fGy > 0.0 && fAvgHgt > 0.0)
}

void CSpecieDataFormView::OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * result)
{
	CString S;
	double fM3Fub;
	double fM3To;
	double fValueM3Fub_original;
	double fValueM3Fub_new;
	double fValueM3To_original;
	double fValueM3To_new;
	double fKrPerM3_last;
	double fKrPerM3_entered;
	CXTPReportColumn *pCol = NULL;
	CAssortmentReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		m_nPriceSetIn	= 2;	// Price set as defualt, in m3to; 090615 p�d
		//---------------------------------------------------------------------------------
		// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
		getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
		// .. and get information in pricelist (xml-file), saved; 070502 p�d
/*
		PricelistParser *pPrlParser = new PricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get assortments species in pricelist; 070601 p�d
				pPrlParser->getHeaderPriceIn(&m_nPriceSetIn);
			}
			delete pPrlParser;
		}
*/
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get assortments species in pricelist; 070601 p�d
				pPrlParser->getHeaderPriceIn(&m_nPriceSetIn);
			}
			delete pPrlParser;
		}

			pCol = pItemNotify->pColumn;
			if (pCol != NULL)
			{
				if (pCol->GetItemIndex() == COLUMN_7)
				{
					pRec = (CAssortmentReportRec*)pItemNotify->pRow->GetRecord();
					if (pRec != NULL)
					{
						CTransaction_trakt_ass rec = pRec->getRecord();

						// Caluclate added kr/m3 (+/-); 080305 p�d
						fValueM3Fub_new = 0.0;
						fValueM3To_new = 0.0;
						fM3Fub = rec.getM3FUB();
						fM3To = rec.getM3TO();
						fKrPerM3_last = getLastKrPerM3(rec) * (-1.0);	// To revese calculation; 080305 p�d
						fKrPerM3_entered = pRec->getColumnFloat(COLUMN_7);

						if (rec.getValueM3FUB() > 0.0)
//						if (m_nPriceSetIn == 1)
						{
							// Try to get back to original value, from calculated value; 080304 p�d
							fValueM3Fub_original = rec.getValueM3FUB()+(fM3Fub*fKrPerM3_last);
							// Calculate new value for M3Fub; 080304 p�d
							fValueM3Fub_new = fValueM3Fub_original+(fM3Fub*fKrPerM3_entered);
							pRec->setColumnFloat(COLUMN_5,fValueM3Fub_new);

							// Calculate new value for M3Fub; 080304 p�d
							fValueM3To_new = 0.0;
							pRec->setColumnFloat(COLUMN_6,fValueM3To_new);

						}
						if (rec.getValueM3TO() > 0.0)
//						else if (m_nPriceSetIn == 2)
						{
							// Try to get back to original value, from calculated value; 080304 p�d
							fValueM3To_original = rec.getValueM3TO()+(fM3To*fKrPerM3_last);
							// Calculate new value for M3Fub; 080304 p�d
							fValueM3To_new = fValueM3To_original+(fM3To*fKrPerM3_entered);
							pRec->setColumnFloat(COLUMN_6,fValueM3To_new);

							// Calculate new value for M3Fub; 080304 p�d
							fValueM3Fub_new = 0.0;
							pRec->setColumnFloat(COLUMN_5,fValueM3Fub_new);
			
						}
						// Update database; adding "kronor per kubikmeter); 080305 p�d
						if (m_pDB != NULL)
						{
							m_pDB->updTraktAss_kr_per_m3(rec.getTAssTraktID(),
																					 rec.getTAssTraktDataID(),
																					 rec.getTAssTraktDataType(),
																					 rec.getTAssID(),
																					 fValueM3Fub_new,
																					 fValueM3To_new,
																					 fKrPerM3_entered);
						}
					}	// if (pRec != NULL)
				}	// if (pCol->GetItemIndex() == COLUMN_7)
			}	// if (pCol != NULL)
	}	// if (pItemNotify != NULL)
}

void CSpecieDataFormView::OnReport1ValueChanged(NMHDR * pNotifyStruct, LRESULT * result)
{
	recalculateTransfers();
}


// CSpecieDataFormView diagnostics

#ifdef _DEBUG
void CSpecieDataFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSpecieDataFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG



void CSpecieDataFormView::clearForm(void)
{
	
}

BOOL CSpecieDataFormView::isSpcUsed(int id)
{
	if (m_vecTraktData.size() > 0)
	{
		getTrakt();
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			if (m_pTraktRecord->getTraktID() == m_vecTraktData[i].getTDataTraktID() &&
 				  m_vecTraktData[i].getTDataID() == id)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

void CSpecieDataFormView::populateData(void)
{
	CString sTmp;
	if (m_vecTraktData.size() > 0)
	{
		getTrakt();
		// Find which trakt is selected and populate data
		// for that trakt; 070315 p�d
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			m_recTraktData = m_vecTraktData[i];
			if (m_pTraktRecord->getTraktID() == m_recTraktData.getTDataTraktID() &&
 				  m_recTraktData.getTDataID() == m_nSpecieID)
			{
				m_nSelectedTraktDataIdx = i;	// Index of selected data

				m_wndReport.ClearReport();
				m_wndReport.SetRedraw( FALSE );
				if (m_vecTraktAss.size() > 0)
				{
					// Add assortments to m_wndReport; 070612 p�d
					for (UINT j = 0;j < m_vecTraktAss.size();j++)
					{
						m_recTraktAss = m_vecTraktAss[j];
						if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
								m_recTraktData.getTDataID() == m_nSpecieID &&		
								m_recTraktData.getTDataTraktID() == m_recTraktAss.getTAssTraktID())
						{
/*
							fM3Fub = m_recTraktAss.getM3FUB();
							fM3To = m_recTraktAss.getM3TO();
							fM3FubValue = m_recTraktAss.getValueM3FUB();
							fM3ToValue = m_recTraktAss.getValueM3TO();
							fKrPerM3 = m_recTraktAss.getKrPerM3();
							if (fM3FubValue > 0.0)
								m_recTraktAss.setValueM3FUB((fM3Fub*fKrPerM3)+fM3FubValue);
							if (fM3ToValue > 0.0)
								m_recTraktAss.setValueM3TO((fM3To*fKrPerM3)+fM3ToValue);
*/
							m_wndReport.AddRecord(new CAssortmentReportRec(j+1,m_recTraktAss));				
						}	// if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
					}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)

				}	// if (m_vecTraktAss.size() > 0)
				m_wndReport.SetRedraw( TRUE );
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();

				// Add assortments transfers to m_wndReport1; 070611 p�d
				m_wndReport1.ResetContent();
				m_wndReport1.SetRedraw( FALSE );
				// Get trakt transfers; 070611 p�d
				getTraktAssTrans(m_recTraktData.getTDataTraktID());
				if (m_vecTraktAssTrans.size() > 0)
				{
					for (UINT j = 0;j < m_vecTraktAssTrans.size();j++)
					{
						m_recTraktAssTrans = m_vecTraktAssTrans[j];
						CString S;
						if (m_recTraktAssTrans.getSpecieID() == m_nSpecieID )
						{
							// Add assortments to m_wndReport; 070612 p�d
							for (UINT j = 0;j < m_vecTraktAss.size();j++)
							{
								m_recTraktAss = m_vecTraktAss[j];
								if (m_recTraktAss.getTAssName().CompareNoCase(m_recTraktAssTrans.getFromName()) == 0)
								{
									if (m_recTraktAss.getM3FUB() > 0.0)
										sTmp = L"fub";
									else
									if (m_recTraktAss.getM3FUB() == 0.0 && m_recTraktAss.getM3TO() > 0.0)
										sTmp = L"to";
									break;
								}
							}
						
							
							addConstraintsToReport();	// Need this here; 070925 p�d
							m_wndReport1.AddRecord(new CTransferReportRec(i+1,m_recTraktAssTrans,sTmp));
						}	// if (m_recTraktData.getTDataID() == m_recTraktAss.getTAssTraktDataID() && 
					}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)
				}	// if (m_vecTraktAss.size() > 0)
				m_wndReport1.SetRedraw( TRUE );
				m_wndReport1.Populate();
				m_wndReport1.UpdateWindow();
				break;
			}	// if (m_pTraktRecord->getTraktID() == m_recTraktData.getTDataTraktID())
		}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
	}	// if (m_vecTraktData.size() > 0)

	CMDISpecieDataFrame *pFrame = (CMDISpecieDataFrame*)getFormViewParentByID(IDD_FORMVIEW8);
	if (pFrame != NULL)
	{
		pFrame->doSetupFunctionData();
	}

}

// Set language for items NOT changed, depending on m_enumAction; 070313 p�d
void CSpecieDataFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndBtnAddAssort.SetWindowText((xml->str(IDS_STRING2090)));
			m_wndBtnDelAssort.SetWindowText((xml->str(IDS_STRING2091)));
			m_wndBtnAddTransfer.SetWindowText((xml->str(IDS_STRING297)));
			m_wndBtnDelTransfer.SetWindowText((xml->str(IDS_STRING298)));
			m_wndBtnDelAllTransfers.SetWindowText((xml->str(IDS_STRING2980)));
		}
		delete xml;
	}
}

// Set language for items changed, depending on m_enumAction; 070313 p�d
void CSpecieDataFormView::setSpecificLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			if (m_enumAction == NEW_ITEM)
			{
				SetWindowText((xml->str(IDS_STRING2100)));
			}
			else if (m_enumAction == UPD_ITEM)
			{
				SetWindowText((xml->str(IDS_STRING2101)));
			}
		}
	}
}

void CSpecieDataFormView::isDataChanged(void)
{
}

BOOL CSpecieDataFormView::getIsDirty(void)
{
//	BOOL bIsTableDatesOK = checkTableDatesOK();
//	if (!bIsTableDatesOK) return FALSE;

	BOOL bIsFrameDirty = isSpecieDialogFrameDirty();
	BOOL bIsDialogDirty = (m_wndReport.isDirty() || m_wndReport1.isDirty());
	BOOL bIsPropertyGridDirty = m_wndPropertyGrid.isDirty();
	return bIsFrameDirty || bIsDialogDirty || bIsPropertyGridDirty;
}


BOOL CSpecieDataFormView::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport.GetSafeHwnd() != NULL)
				{
				
					m_wndReport.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING204)), 80));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING249)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING250)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING251)), 60));
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetEditable( TRUE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING252)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING303)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING304)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING2520)), 60));
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					m_wndReport.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport.FocusSubItems(TRUE);
					m_wndReport.AllowEdit(TRUE);
					m_wndReport.SetFocus();

				}	// if (m_wndReport.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}
/*
	CString m_sBindToSpecie;
	int m_nSpecieID;
	double m_fBindToPercentOf;
	int m_nBindToNumOf;
	double m_fBindToGY;
	double m_fBindToAvgHgt;
	double m_fBindToDA;
	double m_fBindToDG;
	double m_fBindToDGV;
	double m_fBindToHGV;
	double m_fBindToVolM3Sk;
	double m_fBindToVolM3Fub;
	double m_fBindToVolM3Ub;
	double m_fBindToAvgVolM3Sk;
	double m_fBindToAvgVolM3Fub;
	double m_fBindToAvgVolM3Ub;
	double m_fBindToH25;
*/
void CSpecieDataFormView::setupPropertyGrid(void)
{
/*
	CXTPPropertyGridItem* pSettings  = NULL;
	CXTPPropertyGridItem *pItem = NULL;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndPropertyGrid.ResetContent();
			//===============================================================================
			// Settings
			pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING318)));
			if (pSettings != NULL)
			{
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem((xml->str(IDS_STRING133)),_T(""),&m_sBindToSpecie))) != NULL)
					{
						pItem->SetReadOnly();
						pItem->SetID(m_nSpecieID);
					}
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING134)),0.0,_T("%.0f"),&m_fBindToPercentOf))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemNumber((xml->str(IDS_STRING135)),0,&m_nBindToNumOf))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING140)),0.0,_T("%.1f"),&m_fBindToGY))) != NULL)
					{
						pItem->SetReadOnly();
					}
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING150)),0.0,_T("%.1f"),&m_fBindToAvgHgt))) != NULL)
					{
						pItem->SetReadOnly();
					}
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING136)),0.0,_T("%.1f"),&m_fBindToDA))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING137)),0.0,_T("%.1f"),&m_fBindToDG))) != NULL)
					{
						pItem->SetReadOnly();
					}
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING138)),0.0,_T("%.1f"),&m_fBindToDGV))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING139)),0.0,_T("%.1f"),&m_fBindToHGV))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING141)),0.0,_T("%.2f"),&m_fBindToVolM3Sk))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING142)),0.0,_T("%.2f"),&m_fBindToVolM3Fub))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING319)),0.0,_T("%.2f"),&m_fBindToVolM3Ub))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING143)),0.0,_T("%.3f"),&m_fBindToAvgVolM3Sk))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING144)),0.0,_T("%.3f"),&m_fBindToAvgVolM3Fub))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING320)),0.0,_T("%.3f"),&m_fBindToAvgVolM3Ub))) != NULL)
					{
						pItem->SetReadOnly();
					}

					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING149)),0.0,_T("%.1f"),&m_fBindToH25))) != NULL)
					{
						pItem->SetReadOnly();
					}
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItemDouble((xml->str(IDS_STRING267)),0.0,_T("%.0f"),&m_fBindToGreenCrown))) != NULL)
					{
						pItem->SetReadOnly();
					}

					// Set values in Property grid; 071025 p�d
					CTransaction_trakt *pTrakt = getActiveTrakt();
					if (pTrakt != NULL)
					{
						getTraktDataFromDB_specie(pTrakt->getTraktID(),m_nSpecieID);
						m_sBindToSpecie = m_recTraktData.getSpecieName();
						m_fBindToPercentOf = m_recTraktData.getPercent();
						m_nBindToNumOf = m_recTraktData.getNumOf();
						m_fBindToGY = m_recTraktData.getGY();
						m_fBindToAvgHgt = m_recTraktData.getAvgHgt();
						m_fBindToDA = m_recTraktData.getDA();
						m_fBindToDG = m_recTraktData.getDG();
						m_fBindToDGV = m_recTraktData.getDGV();
						m_fBindToHGV = m_recTraktData.getHGV();
						m_fBindToVolM3Sk = m_recTraktData.getM3SK();
						m_fBindToVolM3Fub = m_recTraktData.getM3FUB();
						m_fBindToVolM3Ub = m_recTraktData.getM3UB();
						m_fBindToAvgVolM3Sk = m_recTraktData.getAvgM3SK();
						m_fBindToAvgVolM3Fub = m_recTraktData.getAvgM3FUB();
						m_fBindToAvgVolM3Ub = m_recTraktData.getAvgM3UB();
						m_fBindToH25 = m_recTraktData.getH25();
						m_fBindToGreenCrown = m_recTraktData.getGreenCrown();

					}

					// Display data
					pSettings->Expand();
					m_wndPropertyGrid.Refresh();
			}

		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
*/
}

BOOL CSpecieDataFormView::setupReport1(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport1.GetSafeHwnd() != NULL)
				{
				
					m_wndReport1.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING299)), 90));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING300)), 90));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING301)), 60));
					pCol->SetEditable( TRUE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetVisible(FALSE);	// hide possibility to do transfer by m3fub; 091021 p�d

					pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING302)), 60));
					pCol->SetEditable( TRUE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );


					m_wndReport1.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport1.SetMultipleSelection( FALSE );
					m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport1.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport1.FocusSubItems(TRUE);
					m_wndReport1.AllowEdit(TRUE);
					m_wndReport1.SetFocus();

					// Need to set size of Report control; 051219 p�d
					setResize(&m_wndReport1,340,150,300,140);

				}	// if (m_wndReport.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CSpecieDataFormView::getTrakt(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView)
	{
		CXTPTabManagerItem *pManager = pTabView->getTabCtrl().getTabPage(1);
		if (pManager)
		{
			m_pTraktRecord = (CTransaction_trakt *)pManager->GetData();
		}
	}
}


// Collect TraktData data; 070309 p�d
/*
IDC_EDIT25, m_wndEdit1 1	
IDC_EDIT26, m_wndEdit2 2
IDC_EDIT27, m_wndEdit3 7
IDC_EDIT28, m_wndEdit8 3
IDC_EDIT29, m_wndEdit9 4
IDC_EDIT30, m_wndEdit10 5
IDC_EDIT31, m_wndEdit11 6
IDC_EDIT33, m_wndEdit4 10
IDC_EDIT32, m_wndEdit7 13
IDC_EDIT34, m_wndEdit5 11
IDC_EDIT35, m_wndEdit6 12
IDC_EDIT36, m_wndEdit12 9
IDC_EDIT37, m_wndEdit13 8
*/
void CSpecieDataFormView::getTraktDataRecord(void)
{
	int nTDataTraktID;
	int nTDataID;
	int nSpcID;

	nTDataTraktID = getActiveTrakt()->getTraktID();
	nTDataID = m_nSpecieID;
	nSpcID = m_nSpecieID;

	m_recTraktData = CTransaction_trakt_data(nTDataID,			// TraktData id
																					 nTDataTraktID,	// TraktData trakt id
																					 STMP_LEN_WITHDRAW,
																					 nSpcID,				// Specie id
																					 m_sBindToSpecie,
																					 m_fBindToPercentOf,
																					 m_nBindToNumOf,
																					 m_fBindToGY,
																					 m_fBindToAvgHgt,
																					 m_fBindToDA,
																					 m_fBindToDG,
																					 m_fBindToDGV,
																					 m_fBindToHGV,
																					 m_fBindToH25,
																					 m_fBindToGreenCrown,
																					 m_fBindToVolM3Sk,
																					 m_fBindToVolM3Fub,
																					 m_fBindToVolM3Ub,
																					 m_fBindToAvgVolM3Sk,
																					 m_fBindToAvgVolM3Fub,
																					 m_fBindToAvgVolM3Ub,
																					 m_fBindToGrot,
																					 _T(""));

	// Set changed data in m_vecTraktData for index m_nSelectedTraktDataIdx
	// of selected TraktData; 070315 p�d
	if (m_nSelectedTraktDataIdx >= 0 && m_nSelectedTraktDataIdx < m_vecTraktData.size())
	{
		m_vecTraktData[m_nSelectedTraktDataIdx] = m_recTraktData;
	}	// if (m_nSelectedTraktDataIdx >= 0 && m_nSelectedTraktDataIdx < m_vecTraktData.size())
}	
// Added 2007-07-03 p�d
// Save traktdata information to db; 070703 p�d
void CSpecieDataFormView::saveTraktDataToDB(void)
{

	//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�f
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CSpecieDataFormView::saveTraktDataToDB 1"));

CString sDateDone,sMsg;
if (m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_WITHDRAW,sDateDone) == 0 ||			// "Uttag"
		m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
		m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
{
	SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
	return;
}
#endif

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (!m_pDB->addTraktData(m_recTraktData))
				m_pDB->updTraktData(m_recTraktData);
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}

// Collect Trakt assort transfer data; 070611 p�d
void CSpecieDataFormView::saveTraktAssTrans(void)
{
	int nTraktDataID = -1;
	int nTraktID = -1;
	CTransferReportRec *pRec = NULL;
	CTransaction_trakt_trans data;
	CXTPReportRecords *pRecs = NULL;
	
	getTrakt();

	m_wndReport1.Populate();
	pRecs = m_wndReport1.GetRecords();
	if (pRecs == NULL) return;

	if (m_enumAction == UPD_ITEM)
	{
		nTraktID	= m_recTraktData.getTDataTraktID();
		nTraktDataID = m_recTraktData.getTDataID();
	}


//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�f
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CSpecieDataFormView::saveTraktAssTrans 1"));

CString sDateDone,sMsg;
if (m_enumAction == UPD_ITEM)
{
	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (CTransferReportRec *)pRecs->GetAt(i);
		data = CTransaction_trakt_trans(nTraktDataID,
																		nTraktID,
																		findFromAssortmentID(pRec->getColumnText(COLUMN_0)),
																		findToAssortmentID(pRec->getColumnText(COLUMN_1)),
																		nTraktDataID,
																		STMP_LEN_WITHDRAW,
																		pRec->getColumnText(COLUMN_0),
																		pRec->getColumnText(COLUMN_1),
																		m_recTraktData.getSpecieName(),
																		pRec->getColumnFloat(COLUMN_2),
																		pRec->getColumnFloat(COLUMN_3),
																		pRec->getRecord().getCreated());

		if (m_pDB->checkTraktTransDate(data,STMP_LEN_WITHDRAW,sDateDone) == 0)
		//		m_pDB->checkTraktTransDate(m_recTraktData,STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
		//		m_pDB->checkTraktTransDate(m_recTraktData,STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
		{
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			return;
		}
	}	// for (int i = 0;i < pRecs->GetCount();i++)
}	// if (m_enumAction == UPD_ITEM)
#endif

	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (CTransferReportRec *)pRecs->GetAt(i);
		data = CTransaction_trakt_trans(nTraktDataID,
																		nTraktID,
																		findFromAssortmentID(pRec->getColumnText(COLUMN_0)),
																		findToAssortmentID(pRec->getColumnText(COLUMN_1)),
																		nTraktDataID,
																		STMP_LEN_WITHDRAW,
																		pRec->getColumnText(COLUMN_0),
																		pRec->getColumnText(COLUMN_1),
																		m_recTraktData.getSpecieName(),
																		pRec->getColumnFloat(COLUMN_2),
																		pRec->getColumnFloat(COLUMN_3),
																		pRec->getColumnFloat(COLUMN_4),
																		_T(""));

		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				if (!m_pDB->addTraktTrans(data))
					m_pDB->updTraktTrans(data);
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
	}
}

void CSpecieDataFormView::delTraktAssTrans(void)
{
	int nTraktDataID;
	int nTraktID;
	CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
	if (pRecs != NULL)
	{
		getTrakt();

		if (m_enumAction == UPD_ITEM)
		{
			nTraktID	= m_recTraktData.getTDataTraktID();
			nTraktDataID = m_recTraktData.getTDataID();
		}

		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				m_pDB->removeTraktTrans(nTraktDataID,nTraktID);
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
	}	// if (pRecs != NULL)
}

void CSpecieDataFormView::getTraktAssFromDB(void)
{
	m_vecTraktAss.clear();
	getTrakt();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktAss(m_vecTraktAss,m_pTraktRecord->getTraktID());
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}

double CSpecieDataFormView::getLastKrPerM3(CTransaction_trakt_ass rec)
{
	if (m_vecTraktAss.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktAss.size();i++)
		{
			CTransaction_trakt_ass rec1 = m_vecTraktAss[i];
			if (rec1.getTAssID() == rec.getTAssID() &&
					rec1.getTAssTraktDataID() == rec.getTAssTraktDataID() &&
					rec1.getTAssTraktDataType() == rec.getTAssTraktDataType() &&
					rec1.getTAssTraktID() == rec.getTAssTraktID())
					return rec1.getKrPerM3();
		}	// for (UINT i = 0;i < m_vecTraktAss.size();i++)
	}	// if (m_vecTraktAss.size() > 0)

	return 0.0;
}

void CSpecieDataFormView::saveTraktAssToDB(void)
{

	if (m_vecTraktAss.size() > 0)
	{


//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�f
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
CString sDateDone,sMsg;
//UMMessageBox(_T("CSpecieDataFormView::saveTraktAssToDB 1"));

// Check ALL Assortments for changes made by other user; 090318 p�d
for (UINT i = 0;i < m_vecTraktAss.size();i++)
{

	if (m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_WITHDRAW,sDateDone) == 0 ||			// "Uttag"
			m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
			m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
	{
		SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
		return;
	}
}
#endif

		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{

				// Readd Assortment information in m_vecTraktAss vector; 070613 p�d
				for (UINT i = 0;i < m_vecTraktAss.size();i++)
				{
					if (!m_pDB->addTraktAss(m_vecTraktAss[i]))
						m_pDB->updTraktAss(m_vecTraktAss[i]);
				}
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
	}
}

void CSpecieDataFormView::resetTraktAssInDB(void)
{
	if (m_vecTraktAss.size() > 0)
	{
		// Start by removing ALL Exchangeinformation from esti_trakt_spc_assort_table; 070613 p�d
		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				// Readd Assortment information in m_vecTraktAss vector; 070613 p�d
				for (UINT i = 0;i < m_vecTraktAss.size();i++)
				{
					CTransaction_trakt_ass data = m_vecTraktAss[i];
					if (m_recTraktData.getTDataID() == data.getTAssTraktDataID() && 
							m_recTraktData.getTDataID() == m_nSpecieID &&		
							m_recTraktData.getTDataTraktID() == data.getTAssTraktID())
					{

						data.setM3FUB(0.0);
						data.setM3TO(0.0);
						data.setValueM3FUB(0.0);
						data.setValueM3TO(0.0);
						data.setKrPerM3(0.0);
						if (!m_pDB->addTraktAss(data))
							m_pDB->updTraktAss(data);
					}
				}
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
	}

}

void CSpecieDataFormView::delTraktAssFromDB(void)
{
	if (m_vecTraktAss.size() > 0)
	{
		// Start by removing ALL Exchangeinformation from esti_trakt_spc_assort_table; 070613 p�d
		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				m_pDB->removeTraktAss(m_recTraktData.getTDataTraktID(),m_recTraktData.getTDataID());
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
	}
}

// Get Trakt data in database; 070312 p�d
void CSpecieDataFormView::getTraktDataFromDB(void)
{
	m_vecTraktData.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktData(m_vecTraktData);
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

// Get Trakt data in database for specie; 070312 p�d
void CSpecieDataFormView::getTraktDataFromDB_specie(int trakt_id,int spc_id)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktData(m_recTraktData,trakt_id,spc_id,STMP_LEN_WITHDRAW);
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

// Special function; get Assort data from database-table, into
// m_vecTraktAss, excluding the specie set in spc_id.
// This specie'll value are set in getTraktAss() method; 070315 p�d
void CSpecieDataFormView::getTraktAssFromDB_exclude(void)
{
	m_vecTraktAss.clear();

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktAss_exclude(m_vecTraktAss,m_nSpecieID);
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}

// Get transfers made for this trakt; 070611 p�d
void CSpecieDataFormView::getTraktAssTrans(int trakt_id)
{
	m_vecTraktAssTrans.clear();

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktTrans(m_vecTraktAssTrans,trakt_id);
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}


// Get misc. trakt information. I.e. selected pricelist and it's species; 070509 p�d
void CSpecieDataFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CSpecieDataFormView::getAssortmentsInPricelist(void)
{
	//---------------------------------------------------------------------------------
	// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
	getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
	// .. and get information in pricelist (xml-file), saved; 070502 p�d
/*
	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070601 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssortmentsPerSpc);
		}
		delete pPrlParser;
	}
*/
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070601 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssortmentsPerSpc);
		}
		delete pPrlParser;
	}

}


void CSpecieDataFormView::addConstraintsToReport(void)
{
	int nCounter;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport1.GetColumns();
	CXTPReportRows *pRows = m_wndReport1.GetRows();
	if (pColumns != NULL)
	{
		// Get assortments pricelist for this trakt.
		// Get info. from esti_trakt_misc_data_table; 070509 p�d
		getAssortmentsInPricelist();

		// Add species in Pricelist to Combobox in Column 1; 070509 p�d
		CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_0);
		if (pFromCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pFromCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecAssortmentsPerSpc.size() > 0)
			{
				nCounter = 0;
				for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				{
					if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)
					{
						pFromCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
						nCounter++;
					}
				}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
			}	// if (m_vecAssortmentsPerSpc.size() > 0)
		}	// if (pFromCol != NULL)


		// Add species in Pricelist to Combobox in Column 1; 070509 p�d
		CXTPReportColumn *pToCol = pColumns->Find(COLUMN_1);
		if (pToCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pToCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecAssortmentsPerSpc.size() > 0)
			{
				nCounter = 0;
				for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				{
					if (m_vecAssortmentsPerSpc[i].getSpcID() == m_nSpecieID)
					{
						pToCol->GetEditOptions()->AddConstraint((m_vecAssortmentsPerSpc[i].getAssortName()),nCounter);
						nCounter++;
					}
				}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
			}	// if (m_vecAssortmentsPerSpc.size() > 0)
		}	// if (pFromCol != NULL)

	}	// if (pColumns != NULL)

}


void CSpecieDataFormView::exitViewAction(void)
{
	if (getIsDirty())
	{
		// Collect data entered, into:
		// m_recTraktData (CTransaction_trakt_data)
		// m_vecTraktAss	(vecTransactionTraktAss)
		getTraktDataRecord();

		// Save changes to database; 070703 p�d
		// First save as "St�mplingsl�ngd - Uttag"
		saveTraktDataToDB();

		getTraktAssFromDB_exclude();
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
		if (pTabView)
		{
			// Identify the window to be displayed here; 070306 p�d
			CPageTwoFormView* pView = pTabView->getPageTwoFormView();
			{
				if (m_enumAction == NEW_ITEM)
				{
					// Add data to PageTwoFormView m_wndReport and Avg. and Sum. data; 070613 p�d		
					pView->addFromSpecieDataFormView();
				}
				if (m_enumAction == UPD_ITEM)
				{
					// Saves data
					// Update data to PageTwoFormView m_wndReport and Avg. and Sum. data; 070613 p�d		
					pView->updFromSpecieDataFormView();
				}
			}	// CPageTwoFormView* pView = DYNAMIC_DOWNCAST(CPageTwoFormView, 
		}	// if (pTabView)
	}	//	if (getIsDirty())
}


int CSpecieDataFormView::findFromAssortmentID(LPCTSTR assort_name)
{
	CString S;
	CXTPReportColumns *pCols = m_wndReport1.GetColumns();
	if (pCols != NULL)
	{
		CXTPReportColumn *pCol = pCols->Find(COLUMN_0);
		if (pCol != NULL)
		{
			CXTPReportRecordItemConstraint* pConst = pCol->GetEditOptions()->FindConstraint(assort_name);
			if (pConst != NULL)
			{
				return (int)pConst->m_dwData;
			}	// if (pConst != NULL)
		}	// if (pCol != NULL)
	}	// if (pCols != NULL)
	return -1;
}

int CSpecieDataFormView::findToAssortmentID(LPCTSTR assort_name)
{

	CXTPReportColumns *pCols = m_wndReport1.GetColumns();
	if (pCols != NULL)
	{
		CXTPReportColumn *pCol = pCols->Find(COLUMN_1);
		if (pCol != NULL)
		{
			CXTPReportRecordItemConstraint* pConst = pCol->GetEditOptions()->FindConstraint(assort_name);
			if (pConst != NULL)
			{
				return (int)pConst->m_dwData;
			}	// if (pConst != NULL)
		}	// if (pCol != NULL)
	}	// if (pCols != NULL)
	return -1;
}

void CSpecieDataFormView::recalculateTransfers(void)
{
	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport1.GetFocusedColumn();
	vecTransactionTraktTrans vecTransfers;
	int nFocusedRowIndex = -1;
	int nPriceSetIn = 2;

	// Set wait cursor; 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	if (pRow != NULL && pCol != NULL)
	{
		nFocusedRowIndex = pRow->GetIndex();

		if (pCol->GetIndex() < COLUMN_2) return;

		// .. and get information in pricelist (xml-file), saved; 070502 p�d
/*
		PricelistParser pPrlParser;
		if (pPrlParser.LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			pPrlParser.getHeaderPriceIn(&nPriceSetIn);
		}
*/
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				pPrlParser->getHeaderPriceIn(&nPriceSetIn);
			}
			delete pPrlParser;
		}

		// Save transfers made; 070612 p�d
		saveTraktAssTrans();

		// After saving, reload transfer-data; 081016 p�d
		if (m_bConnected)
		{
			getTrakt();	// Active trakt; 081016 p�d
			if (m_pDB != NULL)
			{
				// Load
				m_bConnected = m_pDB->getTraktTrans(vecTransfers,m_pTraktRecord->getTraktID());
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)

		// Recalculate, so we're back at the start, no transfers; 081016 p�d
		resetTraktAssInDB();
		runDoCalulationInPageThree(m_nSpecieID,TRUE /* Clear transfers */,TRUE,FALSE /* Don't show message */);
		getTraktAssFromDB();

		// Add vecTransfers to m_wndReport1
		m_wndReport1.ResetContent();
		// Do we have Transfers; 081016 p�d
		if (vecTransfers.size() > 0)
		{
			for (UINT i = 0;i < vecTransfers.size();i++)
			{
				// We need to check specie also. Because assortments can have the same
				// name in different species; 090922 p�d
				if (vecTransfers[i].getSpecieID() == m_nSpecieID)
				{
					// Only reset values for Percent or M3Fub, on row that's been changed; 081017 p�d
					if (i == nFocusedRowIndex)
					{
						if (pCol->GetIndex() == COLUMN_2)	vecTransfers[i].setPercent(0.0);
						else if (pCol->GetIndex() == COLUMN_3)vecTransfers[i].setM3FUB(0.0);
					}	// if (i == nFocusedRowIndex)
					m_wndReport1.AddRecord(new CTransferReportRec(i+1,vecTransfers[i],L"fub"));
				}	// if (vecTransfers[i].getSpcID() == m_nSpecieID)
			}	// for (UINT i = 0;i < vecTransfers.size();i++)
		}
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		// Save transfers made; 070612 p�d
		saveTraktAssTrans();
		CXTPReportRows *pRows = m_wndReport1.GetRows();
		if (pRows != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				CTransferReportRec *pRec1 = (CTransferReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec1 != NULL)
				{
/*
					// Function in HMSFuncLib.h; 081017 p�d
					calcTransfersByPercent(m_nSpecieID,
						 										 pRec1->getColumnText(COLUMN_0),
																 pRec1->getColumnText(COLUMN_1),
																 pRec1->getColumnFloat(COLUMN_3),
																 m_recTraktData,
																 m_vecTraktAss,
																 nPriceSetIn);
*/
					saveTraktAssToDB();
					getTraktAssFromDB();
				}	// if (pRec1 != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows != NULL)
		populateData();

		vecTransfers.clear();
	}	// if (pRow != NULL && pCol != NULL)
	else
		recalculateNoTransfers();

	// Set back to arrow cursor (default); 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

}

void CSpecieDataFormView::recalculateNoTransfers(void)
{
	// Set wait cursor; 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	m_wndReport1.ResetContent();
	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();
	resetTraktAssInDB();
	if (runDoCalulationInPageThree(m_nSpecieID /* Used only when TRUE = Remove transfers for specie */, 
																 TRUE /* REMOVE TRAKT TRANSFERS */,TRUE,
																 FALSE /* Don't show message */))
	{
		// Reload Trakt assortment data; 070613 p�d
		getTraktAssFromDB();
		populateData();
	}

	// Set back to arrow cursor (default); 081017 p�d
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}

// Add assortment
void CSpecieDataFormView::OnBnClickedButton3()
{
	// Add a CAssortmentReportRec; 070309 p�d
	m_wndReport.AddRecord(new CAssortmentReportRec());
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

}

// Remove assortment
void CSpecieDataFormView::OnBnClickedButton4()
{
	CAssortmentReportRec *pRec;
	CTransaction_trakt_ass rec;
	int nIndex = -1;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CAssortmentReportRec *)pRecs->GetAt(pRow->GetIndex());
			if (pRec != NULL)
			{
				rec = pRec->getRecord();
			}
			nIndex = pRow->GetIndex();
			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					m_pDB->removeTraktAss(rec);
				}	// if (m_pDB != NULL)
				pRecs->RemoveAt(pRow->GetIndex());
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();

				// Reenter data for m_vecTraktAss; 070316 p�d
				getTraktAssFromDB_exclude();
			}	// if (m_bConnected)
		}	// if (pRow != NULL)
	}	// if (pRecs != NULL)
}

// Add Transfer
void CSpecieDataFormView::OnBnClickedButton5()
{
	// Add a CAssortmentReportRec; 070309 p�d
	m_wndReport1.AddRecord(new CTransferReportRec());
	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	addConstraintsToReport();

}
// Remove transfer
void CSpecieDataFormView::OnBnClickedButton6()
{
	CTransaction_trakt_trans rec;
	CTransferReportRec *pRec = NULL;
	int nIndex = -1;
	CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
	if (pRecs != NULL)
	{
		CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CTransferReportRec *)pRecs->GetAt(pRow->GetIndex());
			if (pRec != NULL)
			{
				rec = pRec->getRecord();
				nIndex = pRow->GetIndex();
				if (m_bConnected)
				{
					if (m_pDB != NULL)
					{
						m_pDB->removeTraktTrans(rec);
					}	// if (m_pDB != NULL)
					pRecs->RemoveAt(pRow->GetIndex());
					m_wndReport1.Populate();
					m_wndReport1.UpdateWindow();
					recalculateTransfers();
				}	// if (m_bConnected)
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
	}	// if (pRecs != NULL)
	else
		recalculateNoTransfers();
}

void CSpecieDataFormView::OnBnClickedButton7()
{
	recalculateNoTransfers();
}


BOOL CSpecieDataFormView::checkTableDatesOK(void)
{
/*
//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�d
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
	BOOL bTableDateMissMatch = FALSE;
	CString sDateDone,sMsg;
	CXTPReportRecords *pRecs = m_wndReport1.GetRecords();

	//---------------------------------------------------------------------------------------------------
	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	if (pRow != NULL)
	{
		CAssortmentReportRec *pRec = (CAssortmentReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			CTransaction_trakt_ass rec = pRec->getRecord();
			if (m_pDB->checkTraktAssDate(rec,rec.getTAssTraktDataType(),sDateDone) == 0)
			{	
				bTableDateMissMatch = TRUE;
			}	// if (m_pDB->checkTraktAssDate(rec,rec.getTAssTraktDataType(),sDateDone) == 0)
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)

	//---------------------------------------------------------------------------------------------------
	if (m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_WITHDRAW,sDateDone) == 0 ||			// "Uttag"
			m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
			m_pDB->checkTraktDataDate(m_recTraktData,STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
	{
		bTableDateMissMatch = TRUE;
	}

	//---------------------------------------------------------------------------------------------------
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CTransferReportRec *pRec = (CTransferReportRec *)pRecs->GetAt(i);
			CTransaction_trakt_trans data = CTransaction_trakt_trans(m_recTraktData.getTDataTraktID(),
																															 m_recTraktData.getTDataID(),
																															 findFromAssortmentID(pRec->getColumnText(COLUMN_0)),
																															 findToAssortmentID(pRec->getColumnText(COLUMN_1)),
																															 m_recTraktData.getTDataTraktID(),
																															 STMP_LEN_WITHDRAW,
																															 pRec->getColumnText(COLUMN_0),
																															 pRec->getColumnText(COLUMN_1),
																															 m_recTraktData.getSpecieName(),
																															 pRec->getColumnFloat(COLUMN_2),
																															 pRec->getColumnFloat(COLUMN_3),
																															 pRec->getRecord().getCreated());

			if (m_pDB->checkTraktTransDate(data,STMP_LEN_WITHDRAW,sDateDone) == 0)
			//		m_pDB->checkTraktTransDate(m_recTraktData,STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
			//		m_pDB->checkTraktTransDate(m_recTraktData,STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
			{
				bTableDateMissMatch = TRUE;
				break;
			}	// if (m_pDB->checkTraktTransDate(data,STMP_LEN_WITHDRAW,sDateDone) == 0)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)

	//---------------------------------------------------------------------------------------------------
	// Check ALL Assortments for changes made by other user; 090318 p�d
	for (UINT i = 0;i < m_vecTraktAss.size();i++)
	{

		if (m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_WITHDRAW,sDateDone) == 0 ||			// "Uttag"
				m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_TO_BE_LEFT,sDateDone) == 0||		// "Kvarl�mmnat"
				m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_WITHDRAW_ROAD,sDateDone) == 0)	// "Uttag i stickv�g"
		{
			bTableDateMissMatch = TRUE;
		}	// if (m_pDB->checkTraktAssDate(m_vecTraktAss[i],STMP_LEN_WITHDRAW,sDateDone) == 0 ||			// "Uttag"
	}	// for (UINT i = 0;i < m_vecTraktAss.size();i++)

	if (bTableDateMissMatch)
		SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);

	return bTableDateMissMatch;
#else
	return TRUE;
#endif
*/
	return TRUE;

}
