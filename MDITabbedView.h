#if !defined(__MDITABBEDVIEW_H__)
#define __MDITABBEDVIEW_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StandEntryDoc.h"

#include "PageOneFormView.h"
#include "PageTwoFormView.h"
#include "PageThreeFormView.h"
#include "PageFourFormView.h"


// CMDITabbedView view
class CMDILicenseFrameDoc;

class CMDITabbedView : public CView
{
	DECLARE_DYNCREATE(CMDITabbedView)

	CString	m_sLangFN;

	BOOL m_bInitialized;

protected:
	CMDITabbedView();           // protected constructor used by dynamic creation
	virtual ~CMDITabbedView();

public:
	CMyTabControl &getTabCtrl(void)
	{
		return m_wndTabControl;
	}

	CPageOneFormView *getPageOneFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CPageOneFormView, pView);
			return pView;
		}
		return NULL;
	}

	CPageTwoFormView *getPageTwoFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CPageTwoFormView* pView = DYNAMIC_DOWNCAST(CPageTwoFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CPageTwoFormView, pView);
			return pView;
		}
		return NULL;
	}

	CPageThreeFormView *getPageThreeFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CPageThreeFormView* pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CPageThreeFormView, pView);
			return pView;
		}
		return NULL;
	}

	CPageFourFormView *getPageFourFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(3);
		if (m_tabManager)
		{
			CPageFourFormView* pView = DYNAMIC_DOWNCAST(CPageFourFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(getPageFourFormView, pView);
			return pView;
		}
		return NULL;
	}

	void enablePage(int idx,BOOL enable)
	{
		if (idx < m_wndTabControl.getNumOfTabPages())
		{
			m_tabManager = m_wndTabControl.getTabPage(idx);
			if (m_tabManager)
			{
				m_tabManager->SetEnabled(enable);
			}	// if (m_tabManager)
		}	// if (idx < m_wndTabControl.getNumOfTabPages())
	}

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CMDILicenseFrameDoc* GetDocument();

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	void UpdateDocTitle();


protected:
	//{{AFX_MSG(CTabbedViewView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDILicenseFrameDoc* CMDITabbedView::GetDocument()
	{ return (CMDILicenseFrameDoc*)m_pDocument; }
#endif


#endif