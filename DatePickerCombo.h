// DatePickerCombo.h : header file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATEPICKERCOMBO_H__E9EBF9BB_09DB_480A_8161_718BB52156E9__INCLUDED_)
#define AFX_DATEPICKERCOMBO_H__E9EBF9BB_09DB_480A_8161_718BB52156E9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#define _USE_SHORT_YEAR	// YY instead of YYYY
//#define _USE_LOCALE_USERDEFAULT
/////////////////////////////////////////////////////////////////////////////
// CMyDatePickerCombo window

//////////////////////////////////////////////////////////////////////////
//
// This combobox class overrides standard DropDown functionality, allowing
// to pickup a specific date or a range of dates with CXTPDatePickerControl
//
class CMyDatePickerCombo : public CComboBox
{
//private:

	CString m_sDateStr;
  // Background brush
  HBRUSH m_brush;
	
	CXTMaskEdit*	m_pEdit;
	CString m_sBtnText;
// Construction
	BOOL m_bIsYYMMDD;

	BOOL m_bIsEnabled;
public:
	CMyDatePickerCombo();

// Attributes
public:

// Operations
public:
	void setDateInComboBox(void);
	void setDateInComboBox(LPCTSTR date);

	void setButtonText(LPCTSTR txt)	{	m_sBtnText = txt;	}

	virtual void setMaskEdit(CXTMaskEdit *edit);
	virtual void setMaskText(LPCTSTR text)	{ if (m_pEdit != NULL) m_pEdit->SetMaskedText(text); }
	BOOL isDate()	const { return !m_pEdit->GetMaskedText().IsEmpty(); }

	CString getDate();

	CString getYear();
	CString getMonth();
	CString getDay();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDatePickerCombo)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyDatePickerCombo();

	LRESULT OnCtlColorListBox(WPARAM wParam, LPARAM lParam) ;

	void EnableWindow(BOOL bEnable = TRUE);

	BOOL isEnabled()	const { return m_bIsEnabled; }
	// Generated message map functions
protected:
	//{{AFX_MSG(CMyDatePickerCombo)
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);	
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd,UINT nCtlColor);	
	afx_msg void OnDropDown();
	afx_msg void OnEnable(BOOL bEnable);
	afx_msg void OnSelchange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATEPICKERCOMBO_H__E9EBF9BB_09DB_480A_8161_718BB52156E9__INCLUDED_)
