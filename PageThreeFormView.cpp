// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"
#include "PageThreeFormView.h"
#include "SpecieDataDialog.h"
#include "PlotSelListFormView.h"
#include "ResLangFileReader.h"
#include "UpdateRandTreeData.h"
#include "UpdateRandTreeData2.h"
#include "AddRandTree.h"
#include "AddDclsDlg.h"
#include "PlotSetupDialog.h"

#include "LoggMessageDlg.h"
#include "MessageDlg.h"

// CPageThreeFormView

extern StrMap global_langMap;

IMPLEMENT_DYNCREATE(CPageThreeFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPageThreeFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()

	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, IDC_REPORT, OnReportColumnRClick)
	ON_CBN_SELCHANGE(IDC_CB_INVENTORY_METHOD, OnCBoxInventoryMethodChange)
	ON_BN_CLICKED(IDC_BTN_PLOTLIST, &CPageThreeFormView::OnBnClickedBtnPlotView)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONTROL1, OnSelectedChanged)
END_MESSAGE_MAP()

CPageThreeFormView::CPageThreeFormView()
	: CXTResizeFormView(CPageThreeFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
	m_bDoWeNeedToAddPlots = FALSE;
	m_pSelectedDCLSRecordItem = NULL;
	m_pSelectedDCLS1RecordItem = NULL;
	m_pSelectedDCLS2RecordItem = NULL;
	m_pSelectedSampleRecordItem = NULL;
	m_bIsDirty = FALSE;
}

CPageThreeFormView::~CPageThreeFormView()
{
}

void CPageThreeFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PLOT_GRP, m_wndGroup1);

	DDX_Control(pDX, IDC_LBL_INVENTORY_METHOD, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_PLOTGROUP, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_GROUPNAME, m_wndLbl3);

	DDX_Control(pDX, IDC_CB_INVENTORY_METHOD, m_wndCBox1);

	DDX_Control(pDX, IDC_ED_PLOTGROUP, m_wndEdit1);
	DDX_Control(pDX, IDC_BTN_PLOTLIST, m_wndBtnOpenPlotList);

	//}}AFX_DATA_MAP
}

void CPageThreeFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecAssortmentsPerSpc.clear();
	m_vecTraktPlot.clear();
	m_vecSpecies.clear();
	m_vecTraktSampleTrees.clear();
	m_vecTraktNonSampleTrees.clear();
	m_vecTraktLeftOverSampleTrees.clear();
	m_vecTraktExtraSampleTrees.clear();
	m_vecTraktDCLSTrees.clear();
	m_vecTraktDCLS1Trees.clear();
	m_vecTraktDCLS2Trees.clear();

	if (m_wndTabControl.GetSafeHwnd())
		m_wndTabControl.DestroyWindow();

	CXTResizeFormView::OnDestroy();	
}

void CPageThreeFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CPageThreeFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

void CPageThreeFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_wndEdit1.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1.SetAsNumeric();

		m_wndCBox1.SetEnabledColor(BLACK,WHITE);
		m_wndCBox1.SetDisabledColor(BLACK,INFOBK);

		m_wndBtnOpenPlotList.SetBitmap(CSize(18,14),IDB_OPENWND);
		m_wndBtnOpenPlotList.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndLbl3.SetLblFont(14,FW_NORMAL);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

		setupInventoryMethodsInCBox();

		m_bInitialized = TRUE;
	}
}

BOOL CPageThreeFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CPageThreeFormView::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		// This is for running the OnSetFocus(), setting HMSShell Toolbars; 070315 p�d
		CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
																							CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(0)->GetHandle()));
		{
			pView->SetFocus();
		}
	}

	// When this view gets focus, enbale the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(TRUE,TRUE,TRUE);
	}
}

void CPageThreeFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
}

// CPageThreeFormView diagnostics

#ifdef _DEBUG
void CPageThreeFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPageThreeFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CPageThreeFormView message handlers

void CPageThreeFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);
	if (m_wndGroup1.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndGroup1,7,7,rect.right - 14,45);
	}

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(10, 65, rect.right - 20, rect.bottom - 70);
	}

}

BOOL CPageThreeFormView::setupReport(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				// Also setup text strings; 070509 p�d					
				m_sFieldChooser = (xml->str(IDS_STRING147));
				// Strings for Plot information; 070509 p�d
				m_wndLbl1.SetWindowText((xml->str(IDS_STRING277)));
				m_wndLbl2.SetWindowText((xml->str(IDS_STRING278)));

				m_sOKBtn = xml->str(IDS_STRING155);

				m_sMsg = (xml->str(IDS_STRING289));
				m_sMsgGroupID.Format(_T("%s\n%s"),
					(xml->str(IDS_STRING2890)),
					(xml->str(IDS_STRING2891)));

				m_sChangeTreeType1.Format(_T("%s\n\n"),(xml->str(IDS_STRING6100)));

				m_sRemoveTreesMsg1.Format(_T("%s\n\n%s\n"),
					(xml->str(IDS_STRING3170)),
					(xml->str(IDS_STRING3174)));
				m_sRemoveTreesMsg2.Format(_T("%s\n\n%s\n"),
					(xml->str(IDS_STRING3171)),
					(xml->str(IDS_STRING3174)));
				m_sRemoveTreesMsg3.Format(_T("%s\n\n%s\n"),
					(xml->str(IDS_STRING3172)),
					(xml->str(IDS_STRING3174)));
				m_sRemoveTreesMsg4 = (xml->str(IDS_STRING3173));

				m_sRemoveTreesMsg1_1.Format(_T("%s\n\n%s\n\n%s"),
					(xml->str(IDS_STRING3170)),
					(xml->str(IDS_STRING3175)),
					(xml->str(IDS_STRING3174)));
				m_sRemoveTreesMsg2_1.Format(_T("%s\n\n%s\n\n%s"),
					(xml->str(IDS_STRING3171)),
					(xml->str(IDS_STRING3175)),
					(xml->str(IDS_STRING3174)));
				m_sRemoveTreesMsg3_1.Format(_T("%s\n\n%s\n\n%s"),
					(xml->str(IDS_STRING3172)),
					(xml->str(IDS_STRING3175)),
					(xml->str(IDS_STRING3174)));

				m_sChangeCruseMethod.Format(_T("%s\n\n%s\n%s\n\n%s"),
					(xml->str(IDS_STRING3210)),
					(xml->str(IDS_STRING3211)),
					(xml->str(IDS_STRING3212)),
					(xml->str(IDS_STRING3213)));

				m_sLogErrorMsg.Format(_T("%s\n%s\n\n%s\n\n"),
					(xml->str(IDS_STRING3440)),
					(xml->str(IDS_STRING3441)),
					(xml->str(IDS_STRING3442)));

				m_sLogErrorMsg2.Format(_T("%s\r\n\r\n%s %s\r\n\r\n%s\r\n%s\r\n%s\r\n\r\n"),
					(xml->str(IDS_STRING3443)),
					(xml->str(IDS_STRING3444)),
					getDateTime(),
					(xml->str(IDS_STRING3445)),
					(xml->str(IDS_STRING3446)),
					(xml->str(IDS_STRING3447)));

				m_sLogCap = (xml->str(IDS_STRING3448));
				m_sCancel = (xml->str(IDS_STRING3449));
				m_sPrintOut = (xml->str(IDS_STRING3450));
				m_sSaveToFile = (xml->str(IDS_STRING3451));
				m_sRecalculate = (xml->str(IDS_STRING350));
				m_sArealMissing = (xml->str(IDS_STRING351));
				m_sDataForGrotMissing = (xml->str(IDS_STRING352));
				m_sAgeForBarkSoderbergUnreasonable= (xml->str(IDS_STRING353));
				m_sAgeForHgtSoderbergUnreasonable= (xml->str(IDS_STRING354));
				m_sAgeForSoderbergUnreasonable = (xml->str(IDS_STRING355));
				m_sHeightForH25Unreasonable = xml->str(IDS_STRING356);
				m_sHeightForH25Unreasonable2 = xml->str(IDS_STRING357);
				
					m_sDiagramXLabel = (xml->str(IDS_STRING2543));
				m_sDiagramYLabel = (xml->str(IDS_STRING2542));

	
				m_sNoFunctionsMsg.Format(_T("%s<BR><BR>%s<BR><BR><B><I>%s</I></B>"),
					(xml->str(IDS_STRING3070)),
					(xml->str(IDS_STRING3073)),
					(xml->str(IDS_STRING3074)));

				m_sSpcWithoutFunctionsMsg.Format(_T("%s"),(xml->str(IDS_STRING3071)));

				m_sNoFunctionsForSpcMsg.Format(_T("%s<BR><BR>%s<BR><BR><B><I>%s</I></B>"),
					(xml->str(IDS_STRING3072)),
					(xml->str(IDS_STRING3073)),
					(xml->str(IDS_STRING3074)));

				m_sRemoveRandTreesFirstMsg = (xml->str(IDS_STRING3279));

				// CStringArray, holds statusinformation for
				// doCalculation method; 070627 p�d
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3080)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3081)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3082)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3083)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3084)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3085)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3086)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3087)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3088)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3089)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3090)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3091)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3092)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3093)));
				m_sarrCalcStatusMsg.Add((xml->str(IDS_STRING3094)));

				m_sarrTreeTypes.Add(xml->str(IDS_STRING3102));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3103));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3104));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3105));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3106));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3107));	// Kanttr�d l�mnas (Provtr�d)
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3108));	// Kanttr�d l�mnas (Ber�knad h�jd)
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3109));	// Pos.tr�d (Uttag)
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3115));	// Tr�d i gatan l�mnas (Provtr�d)
				m_sarrTreeTypes.Add(xml->str(IDS_STRING3116));	// Tr�d i gatan l�mnas (Ber�knad h�jd)

				m_sarrInventoryMethods.Add(_T("<") + xml->str(IDS_STRING3110) + _T(">"));
				m_sarrInventoryMethods.Add(xml->str(IDS_STRING3111));
				m_sarrInventoryMethods.Add(xml->str(IDS_STRING3112));
				m_sarrInventoryMethods.Add(xml->str(IDS_STRING3113));
				m_sarrInventoryMethods.Add(xml->str(IDS_STRING3114));
				m_sarrInventoryMethods.Add(xml->str(IDS_STRING31141));

				m_sDCLSCap = (xml->str(IDS_STRING3140));
				m_sDCLS1Cap = (xml->str(IDS_STRING3141));
				m_sDCLS2Cap = (xml->str(IDS_STRING3142));
				m_sSampleCap = (xml->str(IDS_STRING315));

				m_sTreeCheckMsg1 = xml->str(IDS_STRING4100);
				m_sTreeCheckMsg2 = xml->str(IDS_STRING4101);
				m_sTreeCheckMsg3 = xml->str(IDS_STRING4102);
				m_sTreeCheckMsg4 = xml->str(IDS_STRING4103);
				m_sTreeCheckMsg5 = xml->str(IDS_STRING4104);
				m_sTreeCheckMsg6 = xml->str(IDS_STRING4105);
				m_sTreeCheckMsg7 = xml->str(IDS_STRING4120);
				m_sTreeCheckMsg8 = xml->str(IDS_STRING4121);
				m_sTreeCheckMsg9 = xml->str(IDS_STRING4106);
				m_sTreeCheckMsg10 = xml->str(IDS_STRING4107);
		}	// if (xml->Load(m_sLangFN))
			delete xml;
	}	// if (fileExists(m_sLangFN))
	
	AddView(RUNTIME_CLASS(CDCLSTreeReportView), m_sDCLSCap ,3,IDC_DCLS_REPORT);	
	AddView(RUNTIME_CLASS(CDCLS1TreeReportView), m_sDCLS1Cap ,3,IDC_DCLS1_REPORT);	
	AddView(RUNTIME_CLASS(CDCLS2TreeReportView), m_sDCLS2Cap ,3,IDC_DCLS2_REPORT);	
	AddView(RUNTIME_CLASS(CSampleTreeReportView), m_sSampleCap ,3,IDC_SAMPLE_REPORT);	


	m_tabManager = m_wndTabControl.getTabPage(2);
	if (m_tabManager) m_tabManager->SetVisible(FALSE);

	return TRUE;
}


void CPageThreeFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER2, m_sFieldChooser);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER:
//			OnShowFieldChooser();	
		break;
	}
	menu.DestroyMenu();
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPageThreeFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	int nIndex = -1; 
	CXTPTabManagerItem *pManager;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			nIndex = pManager->GetIndex();
		}	// if (pManager != NULL)
	}	// if (pTabView)

	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			if (nIndex == 2)
			{
				if (pTabView != NULL)
				{
					CPageOneFormView *pPageOne = pTabView->getPageOneFormView();
					if (pPageOne != NULL)
					{
						pPageOne->addTrakt();
					}	// if (pPageOne != NULL)
				}	// if (pTabView != NULL)
				clearAll();
			}	// if (nIndex == 2)
			break;
		}	// case ID_SAVE_ITEM :

		case ID_SAVE_ITEM :
		{
			if (nIndex == 2)
			{
				if (saveDCLSTreesToDB())
				{
					populateData(0);	// Uttag
					resetIsDirty();
			}
				if (saveDCLS1TreesToDB())
				{
					populateData(1);	// Kvarl�mmant
					resetIsDirty();
				}
				if (saveDCLS2TreesToDB())
				{
					populateData(2);	// Uttag i gatan
					resetIsDirty();
			}
				if (saveSampleTreesToDB())
				{
					populateData(3);	// Provtr�d
					resetIsDirty();
				}
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			if (nIndex == 2)
			{
				int nIndex1 = getTabPageIndex();
				if (nIndex1 > -1)
				{
					removeTreesAndPlotsInTrakt();
				}	// if (nIndex1 > -1)
			}
			break;
		}	// case ID_DELETE_ITEM :
	}	// switch (wParam)
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////

// Add Inventory methods declared in StdAfx.h; 070509 p�d
void CPageThreeFormView::setupInventoryMethodsInCBox(void)
{
	int nIndex = 0;
	m_wndCBox1.ResetContent();
	for (int i = 0;i < m_sarrInventoryMethods.GetCount();i++)
	{
		m_wndCBox1.AddString(m_sarrInventoryMethods.GetAt(i));
	}

	// Set curent select to whatever method's selected
	// for this Plot (Group); 070509 p�d
	m_wndCBox1.SetCurSel(nIndex);
	// Only need to add plot information if selection is > 0; 070509 p�d
	m_bDoWeNeedToAddPlots = (nIndex >= 0);
/*	Commented out 2010-04-16 P�D
	// Check if inventory method equals "Ej angiven"
	// If so, disable GroupID and Button; 070509 p�d
	if (!m_bDoWeNeedToAddPlots)
	{
		m_wndEdit1.EnableWindow( FALSE );
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit1.setInt(1);
		m_wndEdit1.SetReadOnly();
		m_wndBtnOpenPlotList.EnableWindow( FALSE );
	}	// 
*/
}

void CPageThreeFormView::setupBasedOnInventoryMethod(void)
{
	CTransaction_plot data;
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex > -1 && nIndex < m_sarrInventoryMethods.GetCount())
	{
		if (m_vecTraktPlot.size() > 0)
		{
			data = m_vecTraktPlot[0];
		}

		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		// On changing inventory method, clear report
		// and any other related data; 070509 p�d
// Not sure !!
//		clearAll();

		if (nIndex == 0)	// "Ej angiven", disable GroupID and Button
		{
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
			m_wndEdit1.SetReadOnly(FALSE);
			m_wndEdit1.setInt(1);
			m_wndEdit1.SetReadOnly();
			m_wndBtnOpenPlotList.EnableWindow( FALSE );
			m_wndLbl3.SetWindowText(_T(""));
			// Enbale ADD new trees button if user selectects this Inv-method.
			// I.e. no Inv-method's selected; 070511 p�d
/* COMMENTED OUT 2007-06-15 P�D
	Toolbar button disabled, only enabled on Page three; 070615 p�d
			if (pFrame != NULL)
			{
				pFrame->setToolbarItems(TRUE,TRUE);
			}
*/
		}
		// "Totaltaxering" and "Plats (Trave)"
		else if (nIndex == 1 || nIndex == 4)	
		{
			m_wndEdit1.EnableWindow( TRUE );
			m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
			m_wndEdit1.SetReadOnly(FALSE);
			m_wndEdit1.setInt(1);
			m_wndEdit1.SetReadOnly(TRUE);
			m_wndBtnOpenPlotList.EnableWindow( TRUE );
			m_wndLbl3.SetWindowText(_T("[") + data.getPlotName() + _T("]"));
			// Enbale ADD new trees button if user selectects this Inv-method(s); 070511 p�d
/* COMMENTED OUT 2007-06-15 P�D
	Toolbar button disabled, only enabled on Page three; 070615 p�d
			if (pFrame != NULL)
			{
				pFrame->setToolbarItems(TRUE,TRUE);
			}
*/
		}	// if (nIndex == 1)
		// "Cirkelytor","Rektangul�ra ytor" and "Snabbtaxering"
		else if (nIndex == 2 || nIndex == 3 || nIndex == 5)	
		{
			m_wndEdit1.EnableWindow( TRUE );
			m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
			m_wndEdit1.SetReadOnly(FALSE);
			m_wndEdit1.setInt(1);
			m_wndEdit1.SetReadOnly(TRUE);
			m_wndBtnOpenPlotList.EnableWindow( TRUE );
			m_wndLbl3.SetWindowText(_T("[") + data.getPlotName() + _T("]"));
			// Disable ADD new trees button if user selectects on of these Inv-methods.
			// User needs to crete a new Plot (Group) or select one from already defined
			// plots (groups); 070511 p�d
			// OBS! Enable button on selection/creation of Plot (Group); 070511 p�d
/* COMMENTED OUT 2007-06-15 P�D
	Toolbar button disabled, only enabled on Page three; 070615 p�d
			if (pFrame != NULL)
			{
				pFrame->setToolbarItems(TRUE,TRUE);
			}
*/
		}	// if (nIndex == 1)
/*
		// Only need to add plot information if selection is >= 0; 070509 p�d
		if (nIndex >= 0)
		{
			m_bDoWeNeedToAddPlots = TRUE;
			if (m_pDB != NULL)
			{
				data.setPlotType(nIndex);
				// Update ALL plots for this Trakt; 070927 p�d
				m_pDB->updPlotEx(data);
			}	// if (m_pDB != NULL)
		}	// if (nIndex >= 0)
*/
	}	// if (nIndex > -1 && nIndex < NUMOF_INVENTORY_METHODS)
}

// Add a new line to insert tree data into; 070509 p�d
void CPageThreeFormView::addNewTree(void)
{

	CTransaction_trakt_data *pTraktData = getActiveTraktData();
	CXTPReportRecord *pRec = NULL;

	if (pTraktData == NULL)
		return;
	CString S;
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (nIndex == 0)	// DCLS Report
		{
	
			CAddDclsDlg *pDlg = new CAddDclsDlg(m_vecSpecies, m_vecTraktPlot, m_pDB, getActiveTrakt()->getTraktID(), nIndex);
			if (pDlg != NULL)
			{
				if (pDlg->DoModal() == IDOK)
				{
					populateData(0);
				}
				delete pDlg;
			}
		}

		if (nIndex == 1)	// DCLS Report
		{
			CAddDclsDlg *pDlg = new CAddDclsDlg(m_vecSpecies, m_vecTraktPlot, m_pDB, getActiveTrakt()->getTraktID(), nIndex);
			if (pDlg != NULL)
			{
				if (pDlg->DoModal() == IDOK)
				{
					populateData(1);
				}
				delete pDlg;
			}
		}

		if (nIndex == 2)	// DCLS Report
		{
			getDCLS2Report()->AddRecord(new CTraktDCLS2TreeReportRec(m_wndEdit1.getInt(),0,m_sLangFN,GetParent()->GetSafeHwnd()));
			getDCLS2Report()->Populate();
			getDCLS2Report()->UpdateWindow();
		}

		if (nIndex == 3)	// Sample Report
		{
			CAddRandTree *pDlg = new CAddRandTree(getActiveTrakt()->getTraktID(),m_wndCBox1.GetCurSel(),m_sarrInventoryMethods, m_sarrTreeTypes, m_vecSmpTreeCategories, m_vecSpecies,m_recTraktMiscData);
			if (pDlg != NULL)
			{
				if (pDlg->DoModal() == IDOK)
				{
					populateData(0);	// Uttag
					populateData(1);	// Kvarl�mnat
					populateData(3 /* 3 = SAMPLE TREES */);
				}	// if (pDlg->DoModal() == IDOK)
				delete pDlg;
			}

			/*
			getSampleReport()->AddRecord(new CTraktSampleTreeReportRec(m_wndEdit1.getInt(),0,m_sLangFN,GetParent()->GetSafeHwnd()));
			getSampleReport()->Populate();
			getSampleReport()->UpdateWindow();*/
		}

		m_bIsDirty = TRUE;
	}

}

BOOL CPageThreeFormView::isHasDataChangedPageThree(void)
{
	if (!saveSampleTreesToDB()) return FALSE;
	if (!saveDCLSTreesToDB()) return FALSE;
	if (!saveDCLS1TreesToDB()) return FALSE;
	//saveDCLS2TreesToDB();
	resetIsDirty();
	return TRUE;
}

void CPageThreeFormView::setFocusOnReport(void)
{
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (nIndex == 0)	// DCLS Report
		{
			getDCLSReport()->SetFocus();
		}

		if (nIndex == 1)	// DCLS Report
		{
			getDCLS1Report()->SetFocus();
		}

		if (nIndex == 2)	// DCLS Report
		{
			getDCLS2Report()->SetFocus();
		}

		if (nIndex == 3)	// Sample Report
		{
			getSampleReport()->SetFocus();
		}
	}
}

// Get misc. trakt information. I.e. selected pricelist and it's species; 070509 p�d
void CPageThreeFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

void CPageThreeFormView::getSpeciesInPricelist(void)
{
	//---------------------------------------------------------------------------------
	// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
	getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
	// .. and get information in pricelist (xml-file), saved; 070502 p�d
/*
	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get species in pricelist; 070430 p�d
			pPrlParser->getSpeciesInPricelistFile(m_vecSpecies);
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070516 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssortmentsPerSpc);
		}
		delete pPrlParser;
	}
*/
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get species in pricelist; 070430 p�d
			pPrlParser->getSpeciesInPricelistFile(m_vecSpecies);
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070516 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssortmentsPerSpc);
		}
		delete pPrlParser;
	}


}

void CPageThreeFormView::GetSampleTreeCategories()
{
	if (m_pDB != NULL)
	{
		m_pDB->GetCategories(m_vecSmpTreeCategories);
	}
}

int CPageThreeFormView::getSelectedInvMethodIndex(void)
{
	return m_wndCBox1.GetCurSel();
}

int CPageThreeFormView::getGroupID(void)
{
	return m_wndEdit1.getInt();
}

void CPageThreeFormView::setGroupID(int id)
{
	m_wndEdit1.setInt(id);
}

void CPageThreeFormView::setReportRecordGroupID(int id)
{
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (nIndex == 0)	// DCLS trees Report "St�mplingsl�ngd - Uttag"
		{
			if (m_pSelectedDCLSRecordItem == NULL)
			{
				m_pSelectedDCLSRecordItem = getDCLSReport()->GetActiveItem();
			}	// if (m_pSelectedSampleRecordItem == NULL)
			
			if (m_pSelectedDCLSRecordItem != NULL) 
			{
				CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec*)m_pSelectedDCLSRecordItem->GetRecord();
				if (pRec != NULL)
				{
					pRec->setColumnInt(0,id);
					
					getDCLSReport()->Populate();
					getDCLSReport()->UpdateWindow();

				}	// if (pRec != NULL)
			}	// if (pItem != NULL) 
		}	// if (nIndex == 1)	// DCLS Trees "St�mplingsl�ngd - Uttag"

		if (nIndex == 1)	// DCLS trees Report "St�mplingsl�ngd - Kvarst�ende"
		{
			if (m_pSelectedDCLS1RecordItem == NULL)
			{
				m_pSelectedDCLS1RecordItem = getDCLS1Report()->GetActiveItem();
			}	// if (m_pSelectedSampleRecordItem == NULL)
			
			if (m_pSelectedDCLS1RecordItem != NULL) 
			{
				CTraktDCLS1TreeReportRec *pRec = (CTraktDCLS1TreeReportRec*)m_pSelectedDCLS1RecordItem->GetRecord();
				if (pRec != NULL)
				{
					pRec->setColumnInt(0,id);
					
					getDCLS1Report()->Populate();
					getDCLS1Report()->UpdateWindow();

				}	// if (pRec != NULL)
			}	// if (pItem != NULL) 
		}	// if (nIndex == 1)	// DCLS Trees "St�mplingsl�ngd - Kvarst�ende"

		if (nIndex == 2)	// DCLS trees Report "St�mplingsl�ngd - Uttag i Stickv�g"
		{
			if (m_pSelectedDCLS2RecordItem == NULL)
			{
				m_pSelectedDCLS2RecordItem = getDCLS2Report()->GetActiveItem();
			}	// if (m_pSelectedSampleRecordItem == NULL)
			
			if (m_pSelectedDCLS2RecordItem != NULL) 
			{
				CTraktDCLS2TreeReportRec *pRec = (CTraktDCLS2TreeReportRec*)m_pSelectedDCLS2RecordItem->GetRecord();
				if (pRec != NULL)
				{
					pRec->setColumnInt(0,id);
					
					getDCLS2Report()->Populate();
					getDCLS2Report()->UpdateWindow();

				}	// if (pRec != NULL)
			}	// if (pItem != NULL) 
		}	// if (nIndex == 1)	// DCLS Trees "St�mplingsl�ngd - Uttag i Stickv�g"


		if (nIndex == 3)	// Sample trees Report
		{
			if (m_pSelectedSampleRecordItem == NULL)
			{
				m_pSelectedSampleRecordItem = getSampleReport()->GetActiveItem();
			}	// if (m_pSelectedSampleRecordItem == NULL)
			
			if (m_pSelectedSampleRecordItem != NULL) 
			{
				CTraktSampleTreeReportRec *pRec = (CTraktSampleTreeReportRec*)m_pSelectedSampleRecordItem->GetRecord();
				if (pRec != NULL)
				{
					pRec->setColumnInt(0,id);
					
					getSampleReport()->Populate();
					getSampleReport()->UpdateWindow();

				}	// if (pRec != NULL)
			}	// if (pItem != NULL) 
		}	// if (nIndex == 1)	// Sample trees Report
	
	}	// if (nIndex > -1)
}

void CPageThreeFormView::setResetReportRecordSelected(void)
{
	// This means that there's no longer a row selected.
	// This selection is done in method: setReportRecordGroupID(); 070516 p�d
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (nIndex == 0)	// DCLS trees Report
		{
			m_pSelectedDCLSRecordItem = NULL;
		}
		if (nIndex == 1)	// DCLS trees Report
		{
			m_pSelectedDCLS1RecordItem = NULL;
		}
		if (nIndex == 2)	// DCLS trees Report
		{
			m_pSelectedDCLS2RecordItem = NULL;
		}
		if (nIndex == 3)	// Sample trees Report
		{
			m_pSelectedSampleRecordItem = NULL;
		}
	}
}

void CPageThreeFormView::setGroupName(LPCTSTR name)
{
	CString sTmp;
	sTmp.Format(_T("[%s]"),name);
	m_wndLbl3.SetWindowText(sTmp);
}

int CPageThreeFormView::getIndexForTreeType(LPCTSTR value)
{
	// If there's species, add to Specie column in report; 070509 p�d
	for (int i = 0;i < m_sarrTreeTypes.GetCount();i++)
	{
		if (_tcscmp(m_sarrTreeTypes.GetAt(i),value) == 0)
			return i;
	}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	return -1;
}

int CPageThreeFormView::getIndexForCategory(LPCTSTR value)
{
	// If there's species, add to Specie column in report; 070509 p�d
	for (int i = 0;i < m_vecSmpTreeCategories.size();i++)
	{
		if (_tcscmp(m_vecSmpTreeCategories[i].getCategory(),value) == 0)
			return m_vecSmpTreeCategories[i].getID();
	}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	return -1;
}

void CPageThreeFormView::addConstraintsToDCLSReport(void)
{
	int nInventoryMetodIndex = getInvMethodIndex();

	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = getDCLSReport()->GetColumns();
	CXTPReportRows *pRows = getDCLSReport()->GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 2; 070509 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_2);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get species in pricelist for this trakt.
			// Get info. from esti_trakt_misc_data_table; 070509 p�d
			getSpeciesInPricelist();
			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)
}


void CPageThreeFormView::addConstraintsToDCLS1Report(void)
{
	int nInventoryMetodIndex = getInvMethodIndex();

	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = getDCLS1Report()->GetColumns();
	CXTPReportRows *pRows = getDCLS1Report()->GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 2; 070509 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_2);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get species in pricelist for this trakt.
			// Get info. from esti_trakt_misc_data_table; 070509 p�d
			getSpeciesInPricelist();
			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

}

void CPageThreeFormView::addConstraintsToDCLS2Report(void)
{
	int nInventoryMetodIndex = getInvMethodIndex();

	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = getDCLS2Report()->GetColumns();
	CXTPReportRows *pRows = getDCLS2Report()->GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 2; 070509 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_2);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get species in pricelist for this trakt.
			// Get info. from esti_trakt_misc_data_table; 070509 p�d
			getSpeciesInPricelist();
			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

}

void CPageThreeFormView::addConstraintsToSampleReport(void)
{
	int nInventoryMetodIndex = getInvMethodIndex();

	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = getSampleReport()->GetColumns();
	CXTPReportRows *pRows = getSampleReport()->GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 2; 070509 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_2);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get species in pricelist for this trakt.
			// Get info. from esti_trakt_misc_data_table; 070509 p�d
			getSpeciesInPricelist();
			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)

		// Add tree types to combobox in Column_15; 070510 p�d
		CXTPReportColumn *pTreeTypesCol = pColumns->Find(COLUMN_15);
		if (pTreeTypesCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pTreeTypesCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			for (int i = 0;i < m_sarrTreeTypes.GetCount();i++)
			{
				pTreeTypesCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(i),i);
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
		}	// if (pSpcCol != NULL)

/*	Don't use a list of Jonsson bonitet. Instead just set a number
		E.g. 1,12,2,23 etc; 071025 p�d
		// Add tree types to combobox in Column_13; 070510 p�d
		CXTPReportColumn *pJBonitetCol = pColumns->Find(COLUMN_15);
		if (pJBonitetCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pJBonitetCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)
			// If there's species, add to Specie column in report; 070509 p�d
			for (int i = 0;i < NUMOF_JONSSON_BONITET;i++)
			{
				pJBonitetCol->GetEditOptions()->AddConstraint(_T(JONSSON_BONITETS[i]),i);
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
		}	// if (pSpcCol != NULL)
*/

		// Add categories to Combobox in Column 17
		CXTPReportColumn *pCatCol = pColumns->Find(COLUMN_17);
		if (pCatCol != NULL)
		{
			// Get constraints for Category column and remove all items
			pCons = pCatCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get tree categories from table 'esti_trakt_sample_trees_table'
			GetSampleTreeCategories();

			if (m_vecSmpTreeCategories.size() > 0)
			{
				pCatCol->GetEditOptions()->AddConstraint(_T(""), 0);
				for (UINT i = 0;i < m_vecSmpTreeCategories.size();i++)
				{
					pCatCol->GetEditOptions()->AddConstraint(m_vecSmpTreeCategories[i].getCategory(), m_vecSmpTreeCategories[i].getID());
				}
			}
		}
	}	// if (pColumns != NULL)
}

CString CPageThreeFormView::getGroupName(int grp_id)
{
	int nTraktID = getActiveTrakt()->getTraktID();
	getPlotsFromDB();
	if (m_vecTraktPlot.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktPlot.size();i++)
		{
			CTransaction_plot data = m_vecTraktPlot[i];
			if (data.getTraktID() == nTraktID && data.getPlotID() == grp_id)
			{
				return data.getPlotName();
			}
		}
	}
	return _T("");
}

// Added 2007-06-26 p�d
// This method compares spc_id to spc in vec
// If not in vec return FALSE, else TRUE; 070626 p�d
BOOL CPageThreeFormView::isSpcAdded(int trakt_id,int spc_id,vecFuncSpc &vec)
{
	CString S;

	if (vec.size() == 0)
		return FALSE;

	for (UINT i = 0;i < vec.size();i++)
	{
		if (vec[i].nSpcID == spc_id)
		{
			return TRUE;
		}
	}

	return FALSE;

}

// Added 2007-06-26 p�d
// This method checks if species in m_vecTraktSampleTrees also has functions
// like volume,bark and height defined.
// This is done by comparing speices in m_vecTraktSampleTrees to does
// in esti_trakt_set_spc_table for trakt; 070626 p�d
BOOL CPageThreeFormView::checkFunctionsForTrees(vecTransactionTraktSetSpc &vec)
{
	BOOL bReturn = TRUE;
	BOOL bSpecieFound = FALSE;
	CTransaction_trakt_set_spc recSetSpc;
	CTransaction_dcls_tree recTree;
	// Get active trakt (trakt id etc); 070626 p�d
	CTransaction_trakt recTrakt = *getActiveTrakt();
	vecFuncSpc vecFSpc;

	CString sSpcMsg;

	CMessageDialog *dlg;

	if (vec.size() == 0)
	{
		dlg = new CMessageDialog(m_sMsg,m_sOKBtn,m_sNoFunctionsMsg);
		dlg->DoModal();
		delete dlg;
		return FALSE;
	}

	if (m_vecTraktDCLSTrees.size() > 0 && vec.size() > 0)
	{
		for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = m_vecTraktDCLSTrees[ii];
			for (UINT i = 0;i < vec.size();i++)
			{
				recSetSpc = vec[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
					  recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0 &&
						recSetSpc.getM3SkToM3Fub() > 0.0)
				{
/*
				S.Format("recSetSpc.getSpcID() %d\nrecTree.getSpcID() %d\nrecSetSpc.getTSetspcTraktID() %d\nrecTrakt.getTraktID() %d\ngetHgtFuncID() %d\ngetVolFuncID() %d\ngetBarkFuncID() %d\ngetM3FubToM3To() %f\ngetM3SkToM3Fub() %f",
						recSetSpc.getSpcID(),recTree.getSpcID(),
					  recSetSpc.getTSetspcTraktID(),recTrakt.getTraktID(),
						recSetSpc.getHgtFuncID(),
						recSetSpc.getVolFuncID(),
						recSetSpc.getBarkFuncID(),
						recSetSpc.getM3FubToM3To(),
						recSetSpc.getM3SkToM3Fub());
				UMMessageBox(S);
*/
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)

			if (!bSpecieFound)
			{
				if (!isSpcAdded(recTree.getTraktID(),recTree.getSpcID(),vecFSpc))
				{
					vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
				}	// if (!isSpcAdded(recTree.getTraktID()recTree.getSpcID(),vecFSpc))
			}	// if (!bSpecieFound)
		}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)
	}	// if (m_vecTraktDCLSTrees.size() > 0 && vec.size() > 0)
	
	if (m_vecTraktDCLS1Trees.size() > 0 && vec.size() > 0)
	{
		for (UINT ii = 0;ii < m_vecTraktDCLS1Trees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = m_vecTraktDCLS1Trees[ii];
			for (UINT i = 0;i < vec.size();i++)
			{
				recSetSpc = vec[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
					  recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0 &&
						recSetSpc.getM3SkToM3Fub() > 0.0)
				{
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)

			if (!bSpecieFound)
			{
				if (!isSpcAdded(recTree.getTraktID(),recTree.getSpcID(),vecFSpc))
				{
					vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
				}	// if (!isSpcAdded(recTree.getTraktID()recTree.getSpcID(),vecFSpc))
			}	// if (!bSpecieFound)
		}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)
	}	// if (m_vecTraktDCLSTrees.size() > 0 && vec.size() > 0)


	if (m_vecTraktDCLS2Trees.size() > 0 && vec.size() > 0)
	{
		for (UINT ii = 0;ii < m_vecTraktDCLS2Trees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = m_vecTraktDCLS2Trees[ii];
			for (UINT i = 0;i < vec.size();i++)
			{
				recSetSpc = vec[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
					  recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0 &&
						recSetSpc.getM3SkToM3Fub() > 0.0)
				{
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)

			if (!bSpecieFound)
			{
				if (!isSpcAdded(recTree.getTraktID(),recTree.getSpcID(),vecFSpc))
				{
					vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
				}	// if (!isSpcAdded(recTree.getTraktID()recTree.getSpcID(),vecFSpc))
			}	// if (!bSpecieFound)
		}	// for (UINT ii = 0;ii < m_vecTraktDCLSTrees.size();ii++)
	}	// if (m_vecTraktDCLSTrees.size() > 0 && vec.size() > 0)

	// Check if there's any species not havin' functions added.
	// If so, tell user; 070626 p�d
	if (vecFSpc.size() > 0)
	{
		sSpcMsg.Empty();
		sSpcMsg = m_sSpcWithoutFunctionsMsg;
		for (UINT iii = 0;iii < vecFSpc.size();iii++)
		{
			sSpcMsg += _T("<BR><b>") + vecFSpc[iii].sSpcName + _T("</b>");
		}
		sSpcMsg += _T("<BR><BR>") + m_sNoFunctionsForSpcMsg;
		dlg = new CMessageDialog(m_sMsg,m_sOKBtn,sSpcMsg);
		dlg->DoModal();
		delete dlg;
		
	}
	bReturn = !(vecFSpc.size() == vec.size());	
	vecFSpc.clear();

	// Return; if species NOT havin' functions equals
	// number of species TOTAL there's NO FUNTIONS AT ALL; 070926 p�d
	return bReturn;	

}


// Get selected Inventory Method BY USER; 070509 p�d
void CPageThreeFormView::OnCBoxInventoryMethodChange(void)
{
	CTransaction_plot data;
	int nPlotIndex = m_wndCBox1.GetCurSel();
	CTransaction_trakt recTrakt = CTransaction_trakt();
	if (nPlotIndex > -1)
	{
		if (UMMessageBox(this->GetSafeHwnd(),m_sChangeCruseMethod,m_sMsg,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
		{
			// Get access to Page two, and check if somethig's changed; 070524 p�d
			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			if (pTabView != NULL)
			{
				CPageOneFormView *pPageOne = pTabView->getPageOneFormView();
				if (pPageOne != NULL)
				{
					recTrakt = pPageOne->getActiveTraktRecord();
					pPageOne = NULL;
				}
					pTabView = NULL;
			}
			// There's a plot, update it; 090127 p�d
			if (m_vecTraktPlot.size() > 0)
			{

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CPageThreeFormView::OnCBoxInventoryMethodChange 1"));

	CString sDateDone;
	for (UINT i = 0;i < m_vecTraktPlot.size();i++)
	{
		if (m_pDB->checkTraktPlotDate(m_vecTraktPlot[i],sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			return;
		}
	}
#endif

				// Update plottype based on trakt id and plot id; 080303 p�d
				updPlotType(recTrakt.getTraktID(),nPlotIndex);
				// Get plots (groups) for this Trakt; 070521 p�d
				getPlotsFromDB();	
				// Setup Inventory method handling, according to method selected; 070515 p�d
				setupBasedOnInventoryMethod();
				m_bIsDirty = TRUE	;
			}	// if (pTrakt != NULL)
			// There's NO plot, add it; 090127 p�d
			else
			{
				// Add plottype based on trakt id and plot id; 080303 p�d
				addPlotType(1,recTrakt.getTraktID(),nPlotIndex);
				// Get plots (groups) for this Trakt; 070521 p�d
				getPlotsFromDB();	
				// Setup Inventory method handling, according to method selected; 070515 p�d
				setupBasedOnInventoryMethod();
				m_bIsDirty = TRUE	;
			}
		}
		else
		{
			if (m_vecTraktPlot.size() > 0)
			{
				CTransaction_plot data = m_vecTraktPlot[0];
				// Get Inventory method, get first value in m_vecTraktPlot; 070515 p�d
				m_wndCBox1.SetCurSel(data.getPlotType());
			}
			else
			{
				m_wndCBox1.SetCurSel(0);	// No Inventory method selected
			}
		}
	}

}

// Open Form list view on Plots (Groups) for this Trakt; 070511 p�d
void CPageThreeFormView::OnBnClickedBtnPlotView()
{

	m_pSelectedSampleRecordItem = NULL;
	m_pSelectedDCLSRecordItem = NULL;
	m_pSelectedDCLS1RecordItem = NULL;
	m_pSelectedDCLS2RecordItem = NULL;
	m_bIsDirty = TRUE;

	CPlotSetupDialog *pDlg = new CPlotSetupDialog(m_pDB);
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			// Disable cancel button on recalc. dialog if
			// a plot's been deleted; 100427 p�d
			populateData(-1);
			// Do a recalculation; 091118 p�d
			runDoCalulationInPageThree();
		}	// if (pDlg->DoModal() == IDOK)
		else
		{
			populateData(-1);
		}
		delete pDlg;
	}	// if (pDlg != NULL)

}

BOOL CPageThreeFormView::OnPreparePrinting(CPrintInfo* pInfo)
{
 
	return CXTResizeFormView::DoPreparePrinting(pInfo);
}

void CPageThreeFormView::populateData(int idx)
{
	CString S;

	// Add Treetypes,Bonitet etc. to Report; 070516 p�d
	addConstraintsToDCLSReport();
	addConstraintsToDCLS1Report();
	addConstraintsToDCLS2Report();
	addConstraintsToSampleReport();

	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
//			clearAll(idx);
			if (idx == -1 || idx == 0)
			{
				// "St�mplingsl�ngd - Uttag"; 071120 p�d
				// Get diameterclass trees for this Trakt; 070821 p�d
				getDCLSTreesFromDB();
				if (m_vecTraktDCLSTrees.size() > 0)
				{
					getDCLSReport()->ResetContent();
					for (UINT i = 0;i < m_vecTraktDCLSTrees.size();i++)
					{
						CTransaction_dcls_tree data = m_vecTraktDCLSTrees[i];
						getDCLSReport()->AddRecord(new CTraktDCLSTreeReportRec(data,m_sLangFN,GetParent()->GetSafeHwnd()));
					}
					getDCLSReport()->GetRows()->ReserveSize(m_vecTraktDCLSTrees.size());
					getDCLSReport()->Populate();
					getDCLSReport()->UpdateWindow();

					m_wndCBox1.EnableWindow(TRUE);
				}
				else
				{
					getDCLSReport()->ResetContent();
					getDCLSReport()->Populate();
					getDCLSReport()->UpdateWindow();
					m_wndCBox1.EnableWindow(TRUE);
				}
			}	// if (idx == -1 || idx == 0)

				// "St�mplingsl�ngd - Kvarst�ende"; 071120 p�d
			if (idx == -1 || idx == 1)
			{
				// Get diameterclass trees for this Trakt; 070821 p�d
				getDCLS1TreesFromDB();
				if (m_vecTraktDCLS1Trees.size() > 0)
				{
					getDCLS1Report()->ResetContent();
					for (UINT i = 0;i < m_vecTraktDCLS1Trees.size();i++)
					{
						CTransaction_dcls_tree data = m_vecTraktDCLS1Trees[i];
						getDCLS1Report()->AddRecord(new CTraktDCLS1TreeReportRec(data,m_sLangFN,GetParent()->GetSafeHwnd()));
					}
					getDCLS1Report()->GetRows()->ReserveSize(m_vecTraktDCLS1Trees.size());
					getDCLS1Report()->Populate();
					getDCLS1Report()->UpdateWindow();
				}
				else
				{
					getDCLS1Report()->ResetContent();
					getDCLS1Report()->Populate();
					getDCLS1Report()->UpdateWindow();
				}
			}	// if (idx == -1 || idx == 1)

			// "St�mplingsl�ngd - Uttag i stickv�g"; 071120 p�d
			if (idx == -1 || idx == 2)
			{
				// Get diameterclass trees for this Trakt; 070821 p�d
				getDCLS2TreesFromDB();
				if (m_vecTraktDCLS2Trees.size() > 0)
				{
					getDCLS2Report()->ResetContent();
					for (UINT i = 0;i < m_vecTraktDCLS2Trees.size();i++)
					{
						CTransaction_dcls_tree data = m_vecTraktDCLS2Trees[i];
						getDCLS2Report()->AddRecord(new CTraktDCLS2TreeReportRec(data,m_sLangFN,GetParent()->GetSafeHwnd()));
					}
					getDCLS2Report()->GetRows()->ReserveSize(m_vecTraktDCLS2Trees.size());
					getDCLS2Report()->Populate();
					getDCLS2Report()->UpdateWindow();
				}
				else
				{
					getDCLS2Report()->ResetContent();
					getDCLS2Report()->Populate();
					getDCLS2Report()->UpdateWindow();
				}
			}	// if (idx == -1 || idx == 2)


			if (idx == -1 || idx == 3)
			{
				// Get sample trees for this Trakt; 070821 p�d
				getSampleTreesFromDB();
				if (m_vecTraktSampleTrees.size() > 0)
				{
					getSampleReport()->ResetContent();
					for (UINT i = 0;i < m_vecTraktSampleTrees.size();i++)
					{
						CTransaction_sample_tree data = m_vecTraktSampleTrees[i];
						CXTPReportRecord *pRec = getSampleReport()->AddRecord(new CTraktSampleTreeReportRec(data,m_sLangFN,GetParent()->GetSafeHwnd(),m_sarrTreeTypes, m_vecSmpTreeCategories));
						
						// Disable editing of height when calculated height is used
						if( data.getTreeType() == TREE_OUTSIDE_NO_SAMP ||
							data.getTreeType() == TREE_OUTSIDE_LEFT_NO_SAMP ||
							data.getTreeType() == TREE_POS_NO_SAMP ||
							data.getTreeType() == TREE_INSIDE_LEFT_NO_SAMP)
						{
							pRec->GetItem(COLUMN_4)->SetEditable(FALSE);
						}
					}
					getSampleReport()->GetRows()->ReserveSize(m_vecTraktSampleTrees.size());
					getSampleReport()->Populate();
					getSampleReport()->UpdateWindow();
				}
				else
				{
					getSampleReport()->ResetContent();
					getSampleReport()->Populate();
					getSampleReport()->UpdateWindow();
				}
			}	// if (idx == -1 || idx == 1)
	}	// if (nIndex > -1)

	// Get plots (groups) for this Trakt; 070515 p�d
	getPlotsFromDB();	
	if (m_vecTraktPlot.size() > 0)
	{
		CTransaction_plot data = m_vecTraktPlot[0];
		// Get Inventory method, get first value in m_vecTraktPlot; 070515 p�d
		m_wndCBox1.SetCurSel(data.getPlotType());
	}
	else
	{
		m_wndCBox1.SetCurSel(0);	// No Inventory method selected
	}
	CXTPReportColumns *pCols = NULL;
	// Uttag i  report
	pCols = getDCLSReport()->GetColumns();
	if (pCols != NULL)
	{
		pCols->GetAt(COLUMN_0)->SetCaption(m_wndCBox1.getText());
	}	// if (pCols != NULL)
	// Uttag i  report
	pCols = getDCLS1Report()->GetColumns();
	if (pCols != NULL)
	{
		pCols->GetAt(COLUMN_0)->SetCaption(m_wndCBox1.getText());
	}	// if (pCols != NULL)

	// Uttag i  report
	pCols = getDCLS2Report()->GetColumns();
	if (pCols != NULL)
	{
		pCols->GetAt(COLUMN_0)->SetCaption(m_wndCBox1.getText());
	}	// if (pCols != NULL)
	// Sample report
	pCols = getSampleReport()->GetColumns();
	if (pCols != NULL)
	{
		pCols->GetAt(COLUMN_0)->SetCaption(m_wndCBox1.getText());
	}	// if (pCols != NULL)
	// Setup Inventory method handling, according to method selected; 070515 p�d
	setupBasedOnInventoryMethod();
}

void CPageThreeFormView::getDiamClass(double dbh,double dcls,double *dcls_from,double *dcls_to)
{
	double fDBH = dbh/10.0;	// mm -> cm
	double fDCLS = 0.0;
	double fMaxDiam = 1000.0;	// 1000 cm (10.0 m)
	CString S;
	//---------------------------------------------------------
	// Try to find out the Diamterclass for each tree, and

	// Check if we have a diamterclass to work with.
	// If not set diamgerclass = 0.0; 070615 p�d
	if (dcls > 0.0 && fDBH > 0.0)
	{
		for (double fDCLS = 0.0;fDCLS < (fMaxDiam + dcls);fDCLS += dcls)
		{
			if (fDBH >= fDCLS && fDBH < (fDCLS + dcls))
			{
				*dcls_from = fDCLS;
				*dcls_to = fDCLS + dcls;
				return;
			}	// if (fDBH >= fDCLS && fDBH < fDCLS + m_recMiscData.getDiamClass())
		}	// for (double fDCLS = m_recMiscData.getDiamClass(); fDCLS < fMaxDia;fDCLS += m_recMiscData.getDiamClass())
	}
}

int CPageThreeFormView::getNumOfRandTreesInDCLS_forSpc(int spc_id,double dcls_from,double dcls_to)
{
	int nCounter = 0;
	CTransaction_sample_tree recSampleTree;
	// This vector include "Kanttr�d", marked as Sampletree; 080306 p�d
	if (m_vecTraktSampleTrees.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktSampleTrees.size();i1++)
		{
			recSampleTree = m_vecTraktSampleTrees[i1];
			if (recSampleTree.getSpcID() == spc_id &&
					recSampleTree.getDCLS_from() == dcls_from &&
					recSampleTree.getDCLS_to() == dcls_to)
			{
				nCounter++;
			}		  
		}
	}
	// This vector holds "Kanttr�d", marked as Non Sampletree; 080306 p�d
	if (m_vecTraktNonSampleTrees.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktNonSampleTrees.size();i1++)
		{
			recSampleTree = m_vecTraktNonSampleTrees[i1];
			if (recSampleTree.getSpcID() == spc_id &&
					recSampleTree.getDCLS_from() == dcls_from &&
					recSampleTree.getDCLS_to() == dcls_to)
			{
				nCounter++;
			}		  
		}
	}

	return nCounter;
}

BOOL CPageThreeFormView::checkGROTInData(void)
{
	CTransaction_trakt_set_spc recTraktSetSpc;
	CTraktDCLSTreeReportRec *pRec = NULL;
	CXTPReportRows *pRows = getDCLSReport()->GetRows();
	if (pRows)
	{
		if (pRows->GetCount() == 0)  return FALSE;

		BOOL bFound = FALSE;
		getTraktSetSpcFromDB(m_recTraktMiscData.getTSetTraktID());
		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{			
				pRec = (CTraktDCLSTreeReportRec*)pRows->GetAt(i)->GetRecord();
				// Only check species that has trees; 100618 p�d
				if (pRec && pRec->getColumnInt(COLUMN_6) > 0)
				{
					for (UINT i1 = 0;i1 < m_vecTransactionTraktSetSpc.size();i1++)
					{
						recTraktSetSpc = m_vecTransactionTraktSetSpc[i1];
						if (recTraktSetSpc.getSpcID() == pRec->getColumnInt(COLUMN_1) && 
								recTraktSetSpc.getGrotFuncID() > 0 && 
								recTraktSetSpc.getGrotPercent() > 0.0)
						{
							bFound = TRUE;
							break;
						}
					}
					if (bFound)	break;
				}
			}
		}
		return bFound;
	}
	return FALSE;
}

int CPageThreeFormView::doCalculations(int spc_id,BOOL remove_trakt_trans,BOOL check_changes,BOOL show_msg,bool do_transfers,bool do_populate)
{
	CStringArray errorLog;
	int nCalcRet = 1,nReturnValue=1;
	int nBarkSod=0,nHgtSod=0;
	CString sErrMsg,sMsgCap;

	if (!checkPricelist(m_recTraktMiscData.getXMLPricelist()))
	{
		sErrMsg.Format(L"%s\n%s\n%s\n",
				(global_langMap[IDS_STRING3315]),
				(global_langMap[IDS_STRING3316]),
				(global_langMap[IDS_STRING3317]));
		sMsgCap =  global_langMap[IDS_STRING289];

		UMMessageBox(this->GetSafeHwnd(),sErrMsg,sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}

	setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(1));

	if (check_changes)
	{

		setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(12));

		if (!saveSampleTreesToDB())
		{
			setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
			setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
			return FALSE;
		}
		if (!saveDCLSTreesToDB())
		{
			setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
			setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
			return FALSE;
		}
		if (!saveDCLS1TreesToDB())
		{
			setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
			setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
			return FALSE;
		}
		if (!saveDCLS2TreesToDB())
		{
			setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
			setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
			return FALSE;
		}
	}
	setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(3));



	//#4600 Kontrollera best�nd�lder om s�derbergs anv�nds som barkfunktion 20151020 J�
	CStringArray arrLogBark,arrLogHgt,arrTraktSoderbergErrorLog;
	arrLogBark.RemoveAll();
	arrLogHgt.RemoveAll();
	arrTraktSoderbergErrorLog.RemoveAll();
	CTransaction_trakt recTrakt=*getActiveTrakt();
	CString msg=_T("");
	nReturnValue=doSoderbergHgtandVolCheckonTrakt(m_dbConnectionData,recTrakt,arrLogBark,arrLogHgt);								
	if(nReturnValue!=1)
	{
		arrTraktSoderbergErrorLog.Add(formatData(_T("%s - %s"),recTrakt.getTraktNum(),recTrakt.getTraktName()));
		if (arrLogBark.GetCount() > 0) //Barkfunktion anv�nds med best�nd�lder >500
		{
			arrTraktSoderbergErrorLog.Add(m_sAgeForBarkSoderbergUnreasonable);									
			arrTraktSoderbergErrorLog.Append(arrLogBark);
		}
		if (arrLogHgt.GetCount() > 0) //H�jd anv�nds med best�nd�lder >500
		{
			arrTraktSoderbergErrorLog.Add(m_sAgeForHgtSoderbergUnreasonable);									
			arrTraktSoderbergErrorLog.Append(arrLogHgt);
		}
		for (int i = 0;i < arrTraktSoderbergErrorLog.GetCount();i++)
			msg += arrTraktSoderbergErrorLog.GetAt(i) + _T("\r\n");
		UMMessageBox(this->GetSafeHwnd(),msg,m_sAgeForSoderbergUnreasonable,MB_ICONEXCLAMATION | MB_OK);
	}


	// #4618 Kontrollera om provtr�ds h�jder ligger utanf�r H25s omr�de
	nReturnValue = CheckH25MinMax(m_dbConnectionData, recTrakt, arrLogHgt);
	if (nReturnValue == FALSE)
	{
		// visa varning om att det finns provtr�d utanf�r H25s omr�de
		msg = _T("");

		CString msg2;
		msg2.Format(m_sHeightForH25Unreasonable2, (int)H25_MIN_VALUE*10, (int)H25_MAX_VALUE*10);
		msg += msg2;
		msg += _T("\r\n");

		for (int i = 0; i < arrLogHgt.GetCount(); i++)
			msg += arrLogHgt.GetAt(i) + _T("\r\n");

		UMMessageBox(this->GetSafeHwnd(), msg, m_sHeightForH25Unreasonable, MB_ICONEXCLAMATION | MB_OK);
	}


#ifdef CALCULATE_GROT

	// Check if data for Grotcalculations missing, if so tell user; 100324 p�d
	if (checkGROTInData())
	{
		if (getActiveTrakt()->getTraktLatitude() == 0 || getActiveTrakt()->getTraktLongitude() == 0)
			UMMessageBox(this->GetSafeHwnd(),m_sDataForGrotMissing,m_sMsg,MB_ICONEXCLAMATION | MB_OK);
	}

#endif
	// Check if areal=0.0, if so tell user; 100309 p�d
	if (getActiveTrakt()->getTraktAreal() == 0.0)
	{
		UMMessageBox(this->GetSafeHwnd(),m_sArealMissing,m_sMsg,MB_ICONEXCLAMATION | MB_OK);

		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		// Handle OnSuiteMessage only if THIS tab is selected; 100416 p�d
		if (pTabView)
		{
			// Identify the window, and send SetFocus() to that window.
			CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView,CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(0)->GetHandle()));
			{
				pView->setArealOnForm(1.0);
				pView->saveTrakt(2);
			}
		}
	}
	if (show_msg)
	{
		// Ask user if want to recalculate the Stand; 080521 p�d
		if (UMMessageBox(this->GetSafeHwnd(),m_sRecalculate,m_sMsg,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDNO)
			return IDNO;
	}	// if (show_msg)

	// Clear errorLog; 080505 p�d
	errorLog.RemoveAll();

	if (m_pDB)
	{
		m_pDB->resetTraktData(getActiveTrakt()->getTraktID());
		// Reset calculated data, i.e. volumes; 101208 p�d
		m_pDB->resetSampleTrees(getActiveTrakt()->getTraktID());
	}

	// Calculate "Rotpost"; 080505 p�d
	// Funtion declared in "calculation_methods.h" in HMSFuncLib; 080505 p�d
	nCalcRet = doCruisingCalculations(spc_id,remove_trakt_trans,errorLog,m_bConnected,m_dbConnectionData,*getActiveTrakt(),do_transfers);
	//***********************************************************************
	clearAll();
	populateData();

	resetIsDirty();

	//***********************************************************************
	// Get access to Page two, and check if somethig's changed; 070524 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (do_populate)
	{
		if (pTabView != NULL)
		{
			CPageOneFormView *pView1 = pTabView->getPageOneFormView();
			if (pView1 != NULL)
			{
				pView1->populateData(-1);
			}	// if (pView != NULL)
			CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
			if (pView2 != NULL)
			{
				pView2->populateData(TRUE);
			}	// if (pView != NULL)
		}	// if (pTabView != NULL)
	}
	//************************************************************************************
	// START: Try to create the HightCurve diagram and save to disk and to DB; 100627 p�d
	//************************************************************************************
	createHeightCurveForDB();
	//************************************************************************************
	// END: Try to create the HightCurve diagram and save to disk and to DB; 100627 p�d
	//************************************************************************************

	// Handle OnSuiteMessage only if THIS tab is selected; 100416 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		CPageFourFormView* pView = DYNAMIC_DOWNCAST(CPageFourFormView,CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(3)->GetHandle()));
		{
			pView->setDiagramOnSelectedTab();
		}
	}

	setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
	setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
	//***********************************************************************
	// Check if errorLog holds any information. If so show it to USER; 080505 p�d
	if (errorLog.GetCount() > 0 || nCalcRet == -2)
	{
		CString sMsg;
		if (UMMessageBox(this->GetSafeHwnd(),(m_sLogErrorMsg),(m_sMsg),MB_DEFBUTTON2 | MB_YESNO | MB_ICONASTERISK) == IDYES)
		{
			sMsg = m_sLogErrorMsg2;
			for (int i = 0;i < errorLog.GetCount();i++)
				sMsg += errorLog.GetAt(i) + _T("\r\n");

			CLoggMessageDlg *dlg = new CLoggMessageDlg();
			if (dlg != NULL)
			{
				dlg->setMessage(sMsg);
				dlg->setupLanguage(m_sLogCap,m_sCancel,m_sPrintOut,m_sSaveToFile);
				dlg->DoModal();
		
				delete dlg;
			}	// if (dlg != NULL)
		}	// if (UMMessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_DEFBUTTON2 | MB_YESNO | MB_ICONASTERISK) == IDYES)
	}	// if (arrLog.GetCount() > 0)
	return TRUE;

}

void CPageThreeFormView::createHeightCurveForDB(int trakt_id)
{
	int nSizeInDB = 0;
	int nScatterSpcIndex = -1;
	double fDCLSCounter;
	double fDCLSClass;
	double fMaxDCLS = 0.0;
	//CTransaction_dcls_tree
	vecLineLayers lineLayers;

	if (m_pDB)
	{
		if (trakt_id == -1)
		{
			nSizeInDB = m_pDB->isHeightCurveInDB(getActiveTrakt()->getTraktID());
		}
		else
		{
			nSizeInDB = m_pDB->isHeightCurveInDB(trakt_id);
		}
	}

	// Create a XYChart
	XYChart *xyChart = new XYChart(900-20, 550-50);

	getTraktDataFromDB(getActiveTrakt()->getTraktID());
	getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
	m_vecTraktSampleTrees.clear();
	getSampleTreesFromDB(getActiveTrakt()->getTraktID(),SAMPLE_TREE);
	getSampleTreesFromDB(getActiveTrakt()->getTraktID(),TREE_SAMPLE_EXTRA);

	m_vecDCLSTrees.clear();
	//---------------------------------------------------------------------------------
	// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
	fDCLSClass = m_recTraktMiscData.getDiamClass();
	fDCLSCounter = fDCLSClass;
	if (fDCLSClass > 0.0)
		fMaxDCLS = m_pDB->getMaxDCLS(getActiveTrakt()->getTraktID(),-1)/fDCLSClass;
	else
		fMaxDCLS = 0.0;
	if (fMaxDCLS > 0.0)
	{

		getTraktSetSpcFromDB(getActiveTrakt()->getTraktID());
		
		if (m_vecTraktData.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				fDCLSCounter = fDCLSClass;
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					// Setup the Diameterclass vector adding ALL diameterclasses/species; 100326 p�d
					for (UINT i2 = 0;i2 < (int)fMaxDCLS;i2++)
					{
						m_vecDCLSTrees.push_back(CTransaction_dcls_tree(i1+1,getActiveTrakt()->getTraktID(),1,recTraktData.getSpecieID(),recTraktData.getSpecieName(),
																														fDCLSCounter-(fDCLSClass/2.0),
																														(fDCLSCounter+fDCLSClass-(fDCLSClass/2.0)),
																														0.0,1,0.0,1.0,0.0,0.0,0.0,0.0,50,0,0,L""));

						fDCLSCounter += fDCLSClass;
		
					}	// for (UINT i = 0;i < 20;i++)
				}
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)

			doUCCalculate(*getActiveTrakt(),
										m_recTraktMiscData,
										m_vecTraktData,
										m_vecTraktSampleTrees,
										m_vecDCLSTrees,
										m_vecTransactionTraktSetSpc,
										true,	// doCalculations
										false	// Don't calc grot
										);
		}
	}

	//---------------------------------------------------------------------
	// The data for the line chart
	StrArr sarr((int)fMaxDCLS);
	vecDbl vDbl,vDblX,vDblY;
	std::vector<DblArr> vecDCLSDblArr;
	std::vector<DblArr> vecSampDblArrX,vecSampDblArrY;
	// Do nothing, empty stand; 100408 p�d
	if (fMaxDCLS > 0.0)
	{

		//---------------------------------------------------------------------
		// Setup diameterclass
		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = 0.0;
		int nCnt = 0;
		char szTmp[255];
		for (int i3 = 0;i3 < (int)fMaxDCLS;i3++)
		{
			if (fMaxDCLS*fDCLSClass > 100.0)
			{
				if (((i3 % 2) == 0))
					sprintf(szTmp,"%.0f",fDCLSCounter);
				else
					sprintf(szTmp," ");
			}
			else
				sprintf(szTmp,"%.0f",fDCLSCounter);
			
			sarr.add(i3,szTmp);
			
			fDCLSCounter += fDCLSClass;
		}

		std::vector<DblArr> vecDCLSDblArr;
		std::vector<DblArr> vecSampDblArrX,vecSampDblArrY;
		int cnt = 0;
		if (m_vecTraktData.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];

				//---------------------------------------------------------------------
				// Setup graf; hights/diameterclass
				vDbl.clear();
				vDbl.push_back(0.0);
				for (UINT i4 = 0;i4 < m_vecDCLSTrees.size();i4++)
				{
					if (m_vecDCLSTrees[i4].getSpcID() == recTraktData.getSpecieID())
					{
						vDbl.push_back(m_vecDCLSTrees[i4].getHgt()/10.0);
					}
				}
				vecDCLSDblArr.push_back(DblArr(vDbl));

				//---------------------------------------------------------------------
				// Add Sampletrees
				vDblX.clear();
				vDblY.clear();
				for (UINT i5 = 0;i5 < m_vecTraktSampleTrees.size();i5++)
				{
					CTransaction_sample_tree sampTree = m_vecTraktSampleTrees[i5];
					if (sampTree.getSpcID() == recTraktData.getSpecieID())
					{
						vDblX.push_back((sampTree.getDBH()/20.0));
						vDblY.push_back(sampTree.getHgt()/10.0);
						nScatterSpcIndex = cnt;
					}
				}
				vecSampDblArrX.push_back(DblArr(vDblX));
				vecSampDblArrY.push_back(DblArr(vDblY));

				cnt++;
			}
		}
		nCnt = 0;
		//------------------------------------------------------------------
		// Add lineLayers/species

		if (m_vecTraktData.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					xyChart->addLineLayer()->addDataSet(DoubleArray(vecDCLSDblArr[nCnt].get(), vecDCLSDblArr[nCnt].numof()),-1,convStr(recTraktData.getSpecieName()));
				}
				nCnt++;
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		}	// if (m_vecTraktData.size() > 0)
		// Set the axes line width to 2 pixels
		xyChart->xAxis()->setWidth(2);
		xyChart->yAxis()->setWidth(2);

		// Add a title to the y axis
		xyChart->yAxis()->setTitle(convStr(m_sDiagramYLabel));

		// Add a title to the x axis
		xyChart->xAxis()->setTitle(convStr(m_sDiagramXLabel));

		// Set the labels on the x axis; 100330 p�d
		xyChart->xAxis()->setLabels(StringArray(sarr.get(), sarr.numof()));

		xyChart->yAxis()->setAutoScale(0, 0, 0);

		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 15, 900-220, 550-110, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);

		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(900-150, 15); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);

		char szImagePath[MAX_PATH];
		sprintf(szImagePath,"%S%S%d.png",getTempDir(),L"hgt_curve",getActiveTrakt()->getTraktID());
		xyChart->makeChart(szImagePath);
		CString sPath(szImagePath);
		int nFileSize = getFileSize(sPath);
		if (nFileSize > 0) // && nFileSize != nSizeInDB)
		{
			if (m_pDB)
				m_pDB->saveImage(sPath, getActiveTrakt()->getTraktID());
		}
		removeFile(sPath);

	
		if (sarr.numof() > 0)	sarr.free();
		for (UINT i = 0;i < vecDCLSDblArr.size();i++)
			vecDCLSDblArr[i].free();
		for (UINT i = 0;i < vecSampDblArrX.size();i++)
			vecSampDblArrX[i].free();
		for (UINT i = 0;i < vecSampDblArrY.size();i++)
			vecSampDblArrY[i].free();

	}
  //free up resources
	delete xyChart;
}

void CPageThreeFormView::getTraktDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

void CPageThreeFormView::getSampleTreesFromDB(int trakt_id,int tree_type,BOOL clear)
{
	if (m_pDB != NULL)
	{
		m_pDB->getSampleTrees(trakt_id,tree_type,m_vecTraktSampleTrees,clear);
	}	// if (m_pDB != NULL)
}

void CPageThreeFormView::getTraktSetSpcFromDB(int trakt_id)
{
	if (m_pDB)
	{
		m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,trakt_id);
	}	// if (m_pDB != NULL)
}

void CPageThreeFormView::doRunPrintPreview(void)
{
	int nIndex = -1;
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		nIndex = m_tabManager->GetIndex();
		if (nIndex == 0)
		{
			getDCLSTreeReportView()->doRunPrintPreview();
		}	// if (nIndex == 0)
		if (nIndex == 1)
		{
			getDCLS1TreeReportView()->doRunPrintPreview();
		}	// if (nIndex == 0)
		if (nIndex == 2)
		{
			getDCLS2TreeReportView()->doRunPrintPreview();
		}	// if (nIndex == 0)
		if (nIndex == 3)
		{
			getSampleTreeReportView()->doRunPrintPreview();
		}	// if (nIndex == 1)
	}	// if (m_tabManager)
}

void CPageThreeFormView::doRunPrint(void)
{
	int nIndex = -1;
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		nIndex = m_tabManager->GetIndex();
		if (nIndex == 0)
		{
			getDCLSTreeReportView()->SendMessage(WM_COMMAND,ID_FILE_PRINT);
		}	// if (nIndex == 0)
		if (nIndex == 1)
		{
			getDCLS1TreeReportView()->SendMessage(WM_COMMAND,ID_FILE_PRINT);
		}	// if (nIndex == 0)
		if (nIndex == 2)
		{
			getDCLS2TreeReportView()->SendMessage(WM_COMMAND,ID_FILE_PRINT);
		}	// if (nIndex == 0)
		if (nIndex == 3)
		{
			getSampleTreeReportView()->SendMessage(WM_COMMAND,ID_FILE_PRINT);
		}	// if (nIndex == 1)
	}	// if (m_tabManager)
}

void CPageThreeFormView::clearAll(int idx)
{
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (idx == -1 || idx == 0)
		{
			getDCLSReport()->ResetContent();
		}

		if (idx == -1 || idx == 1)
		{
			getDCLS1Report()->ResetContent();
		}

		if (idx == -1 || idx == 2)
		{
			getDCLS2Report()->ResetContent();
		}

		if (idx == -1 || idx == 3)
		{
			getSampleReport()->ResetContent();
		}
	}	// if (nIndex > -1)
}

BOOL CPageThreeFormView::getIsDirty(void)
{
	int nIndex = getTabPageIndex();
	if (m_wndEdit1.isDirty() || m_bIsDirty)
		return TRUE;
	if (nIndex > -1)
	{
		if (nIndex == 0) // DCLS Trees "St�mplingsl�gd - Uttag"
		{
			if (getIsDCLSReportDirty())
			return TRUE;
		}
		if (nIndex == 1) // DCLS1 Trees "St�mplingsl�gd - Kvarst�ende"
		{
			if (getIsDCLS1ReportDirty())
			return TRUE;
		}
		if (nIndex == 2) // DCLS Trees "St�mplingsl�gd - Uttag i stickv�g"
		{
			if (getIsDCLS2ReportDirty())
			return TRUE;
		}
		if (nIndex == 1) // Sample Trees
		{
			if (getIsSampleReportDirty())
			return TRUE;
		}
	}

	return FALSE;
}

void CPageThreeFormView::resetIsDirty(void)
{
	int nIndex = getTabPageIndex();
	if (nIndex > -1)
	{
		if (nIndex == 0) // DCLS Trees "St�mplingsl�gd - Uttag"
		{
			resetIsDCLSReportDirty();
		}
		if (nIndex == 1) // DCLS Trees "St�mplingsl�gd - Kvarst�ende"
		{
			resetIsDCLS1ReportDirty();
		}
		if (nIndex == 2) // DCLS Trees "St�mplingsl�gd - Uttag i Stickv�g"
		{
			resetIsDCLS2ReportDirty();
		}
		if (nIndex == 1) // Sample Trees
		{
			resetIsSampleReportDirty();
		}
	}
	m_wndEdit1.resetIsDirty();
	m_bIsDirty = FALSE;
}

// Force m_bIsDirty; 070516 p�d
void CPageThreeFormView::setIsDirtyPageThree()
{
	m_bIsDirty = TRUE;
}

void CPageThreeFormView::getPlotsFromDB(void)
{
	double fRadius = 0.0;
	double fArea = 0.0;
	double fLength = 0.0;
	double fWidth = 0.0;
	CTransaction_trakt *pTrakt = getActiveTrakt();

	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return;

	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			m_pDB->getPlots(pTrakt->getTraktID(),m_vecTraktPlot);
		}	// if (m_pDB != NULL)
		// Check that we have plot(s) for this trakt; 070614 p�d
		if (m_vecTraktPlot.size() > 0)
		{
			// If we have plot(s), check if we have data in plot radius.
			// If so, we have a "Cirkelyta/ytor" or "Snabbtaxering"
			for (UINT i = 0;i < m_vecTraktPlot.size();i++)
			{
				CTransaction_plot plotData = m_vecTraktPlot[i];
				if (plotData.getPlotType() == 2 ||	// "Cirkelytor"
						plotData.getPlotType() == 3 ||	// "Rektangul�ra ytor"
						plotData.getPlotType() == 4 ||	// "Snabbtaxering"
						plotData.getPlotType() == 5)	// "Laserscanning"
				{
					fArea += plotData.getArea();
				}		

			}	// for (UINT i = 0;i < m_vecTraktPlot.size();i++)

			if (fArea > 0.0)
			{
				m_fArealCalculationFactor = (10000.0/fArea)*pTrakt->getTraktAreal();
				m_fPerHa = (10000.0/fArea);
			}	// if (fArea > 0.0)
			else
			{
				m_fArealCalculationFactor = 1.0;	// On calculting with this factor, do nothing, not yet anyway; 070614 p�d
				if (pTrakt->getTraktAreal() > 0.0)
					m_fPerHa = 1.0/pTrakt->getTraktAreal();
				else
					m_fPerHa = 1.0;	// Addded 080117 p�d
			}
		}	// if (m_vecTraktPlot.size() > 0)
	}

}

void CPageThreeFormView::getSampleTreesFromDB(int tree_type)
{
	if (m_pDB != NULL)
	{
		CTransaction_trakt *pTrakt = getActiveTrakt();
		if (pTrakt != NULL)
			 m_pDB->getSampleTrees(pTrakt->getTraktID(),tree_type,m_vecTraktSampleTrees);

		for (UINT i=0; i<m_vecTraktSampleTrees.size(); i++)
		{
			m_vecTraktSampleTrees[i].setDCLS_from(int(m_vecTraktSampleTrees[i].getDBH() / 10) - ((int(m_vecTraktSampleTrees[i].getDBH()) / 10) % int(m_recTraktMiscData.getDiamClass())));
			m_vecTraktSampleTrees[i].setDCLS_to(m_vecTraktSampleTrees[i].getDCLS_from() + m_recTraktMiscData.getDiamClass());
			m_pDB->updSampleTrees(m_vecTraktSampleTrees[i]);
		}
	}	// if (m_pDB != NULL)
}

void CPageThreeFormView::getNonSampleTreesFromDB(int tree_type)
{
	if (m_pDB != NULL)
	{
		CTransaction_trakt *pTrakt = getActiveTrakt();
		if (pTrakt != NULL)
				m_pDB->getSampleTrees(pTrakt->getTraktID(),tree_type,m_vecTraktNonSampleTrees);
	}	// if (m_pDB != NULL)
}

void CPageThreeFormView::getLeftOverSampleTreesFromDB(int tree_type)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
			}
			else
				m_bConnected = m_pDB->getSampleTrees(pTrakt->getTraktID(),tree_type,m_vecTraktLeftOverSampleTrees);
		}	// if (m_pDB != NULL)
	}
}

void CPageThreeFormView::getExtraSampleTreesFromDB(int tree_type)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
			}
			else
				m_bConnected = m_pDB->getSampleTrees(pTrakt->getTraktID(),tree_type,m_vecTraktExtraSampleTrees);
		}	// if (m_pDB != NULL)
	}
}

// Read trees in m_wndReport, and use these to do calculations
// from, instead of first saving to DB and then read back trees; 070531 p�d
void CPageThreeFormView::getSampleTreesFromReport(void)
{
	CXTPReportRows *pRows = getSampleReport()->GetRows();
	CXTPReportRow *pRow = NULL;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	if (pTrakt == NULL)
		return;

	// Make suew there's something to save; 070515 p�d
	if (pRows != NULL)
	{
		getSampleReport()->Populate();
		m_vecTraktSampleTrees.clear();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CTraktSampleTreeReportRec *pRecord = (CTraktSampleTreeReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					// Setup the CTransaction_trees from 
					CTransaction_sample_tree data = CTransaction_sample_tree(i+1,
																										 pTrakt->getTraktID(),
																										 pRecord->getColumnInt(COLUMN_0),
																										 pRecord->getColumnInt(COLUMN_1),
																										 pRecord->getColumnText(COLUMN_2),
																										 pRecord->getColumnFloat(COLUMN_3),	
																										 pRecord->getColumnFloat(COLUMN_4),	
																										 pRecord->getColumnFloat(COLUMN_5),	
																										 pRecord->getColumnFloat(COLUMN_6),	
																										 pRecord->getColumnFloat(COLUMN_7),	
																										 pRecord->getColumnInt(COLUMN_8),	
																										 pRecord->getColumnInt(COLUMN_9),	
																										 pRecord->getColumnFloat(COLUMN_10),
																										 pRecord->getColumnFloat(COLUMN_11),
																										 pRecord->getColumnFloat(COLUMN_12),	
																										 pRecord->getColumnFloat(COLUMN_13),	
																										 pRecord->getColumnFloat(COLUMN_14),	
																										 getIndexForTreeType(pRecord->getColumnText(COLUMN_15)),
																										 pRecord->getColumnText(COLUMN_16),
																										 _T(""));
					m_vecTraktSampleTrees.push_back(data);
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

// Save trees and assortments per tree; 070516 p�d
BOOL CPageThreeFormView::saveSampleTreesToDB(void)
{
	CStringArray sarrErrors;
	CString sErrMsg;
	BOOL bReturnAndError = FALSE;
	BOOL bReturn = FALSE;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	int nRowCounter = 0;
	int nMaxTreeID = 0;
	CTraktSampleTreeReportRec *pRecord = NULL;

	CString S;

	CTransaction_trakt *pTrakt = getActiveTrakt();

	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return FALSE;


	if (m_bConnected)	
	{
		getSampleReport()->Populate();
		pRows = getSampleReport()->GetRows();

		// Make suew there's something to save; 070515 p�d
		if (pRows != NULL && m_pDB != NULL && pTrakt != NULL)
		{
			if (pRows->GetCount() == 0) bReturn = TRUE;

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CPageThreeFormView::saveSampleTreesToDB 1"));
	CString sDateDone;

	for (int i = 0;i < pRows->GetCount();i++)
	{
		pRow = pRows->GetAt(i);
		if (pRow != NULL)
		{
			pRecord = (CTraktSampleTreeReportRec *)pRow->GetRecord();
			if (pRecord != NULL)
			{
					if (pRecord->getTreeRec().getTreeID() > 0)
					{
						if (m_pDB->checkTraktSampleTreesDate(pRecord->getTreeRec(),sDateDone) == 0)
						{	
							SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
							populateData(3);	// Provtr�d
							return FALSE;
						}	// if (pDB->checkTraktSampleTreesDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
					}	// if (pRecord->getTreeRec().getTreeID() > 0)
			}	// if (pRecord != NULL)				
		}	// if (pRow != NULL)			
	}	// for (int i = 0;i < pRows->GetCount();i++)

#endif

			// Do a check of each tree, to see if any one has an error.
			// E.g. missing number of trees etc; 090202 p�d
			sarrErrors.RemoveAll();
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRecord = (CTraktSampleTreeReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRecord != NULL)
				{
					int nTreeTypeIndex = getIndexForTreeType(pRecord->getColumnText(COLUMN_15));

					sErrMsg.Empty();
					// If tree's undefined, don't save it; 080407 p�d						
					if ( pRecord->getColumnText(COLUMN_2).IsEmpty() ||
							pRecord->getColumnFloat(COLUMN_3) == 0.0 ||
							(pRecord->getColumnFloat(COLUMN_4) == 0.0 && (nTreeTypeIndex == SAMPLE_TREE || nTreeTypeIndex == TREE_OUTSIDE || nTreeTypeIndex == TREE_SAMPLE_REMAINIG || nTreeTypeIndex == TREE_SAMPLE_EXTRA || nTreeTypeIndex == TREE_OUTSIDE_LEFT || nTreeTypeIndex == TREE_INSIDE_LEFT)) )
					{
						S.Format(_T(" --- %s ---"),m_sSampleCap);
						sarrErrors.Add(S);
						sarrErrors.Add(_T(""));
						S.Format(m_sTreeCheckMsg1);
						sarrErrors.Add(S);
						//S.Format(_T("%s : %d"),m_sTreeCheckMsg2,pRecord->getColumnInt(COLUMN_1));
						//sarrErrors.Add(S);
						// Check if Name of species is empty; 100223 p�d
						if (pRecord->getColumnText(COLUMN_2).IsEmpty())
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg3); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_3) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg9); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_4) == 0.0 && (nTreeTypeIndex == SAMPLE_TREE || nTreeTypeIndex == TREE_OUTSIDE || nTreeTypeIndex == TREE_SAMPLE_REMAINIG || nTreeTypeIndex == TREE_SAMPLE_EXTRA || nTreeTypeIndex == TREE_OUTSIDE_LEFT || nTreeTypeIndex == TREE_INSIDE_LEFT) )
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg10); 
							sarrErrors.Add(S);
						}
						// Check if thre's any error
						bReturnAndError = TRUE;
					}
				}	// if (pRecord != NULL)
			}

			if (!bReturnAndError)
			{
				bReturn = TRUE;
				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						pRecord = (CTraktSampleTreeReportRec *)pRow->GetRecord();
						if (pRecord != NULL)
						{
								if (pRecord->getTreeRec().getTreeID() > 0)
									nRowCounter = pRecord->getTreeRec().getTreeID();
								else
								{
									nMaxTreeID = m_pDB->getMaxSampleTreeID(pTrakt->getTraktID());
									// Added 2008-04-07 p�d
									// If there's no items in list nMaxTreeID = -1
									// Then nRowCounter = 0 (ERROR)
									// With change nRowCounter = 1 for first entry; 080407 p�d
									if (nMaxTreeID == -1) nMaxTreeID = 0;

									nRowCounter = nMaxTreeID + 1;
								}

								int dcls_from = (int(pRecord->getColumnFloat(COLUMN_3)) / 10) - ((int(pRecord->getColumnFloat(COLUMN_3)) / 10) % int(m_recTraktMiscData.getDiamClass()));
								int dcls_to = dcls_from + m_recTraktMiscData.getDiamClass();
								
								// Setup the CTransaction_trees from 
								CTransaction_sample_tree data = CTransaction_sample_tree(nRowCounter,
																																				 pTrakt->getTraktID(),
																																				 pRecord->getColumnInt(COLUMN_0),	// plot
																																				 pRecord->getColumnInt(COLUMN_1),	// spc_id
																																				 pRecord->getColumnText(COLUMN_2),	// spc_name
																																				 pRecord->getColumnFloat(COLUMN_3),		// dbh
																																				 pRecord->getColumnFloat(COLUMN_4),		// hgt
																																				 pRecord->getColumnFloat(COLUMN_5),		// gcrown_perc
																																				 pRecord->getColumnFloat(COLUMN_6),		// bark_thick
																																				 pRecord->getColumnFloat(COLUMN_7),		// grot
																																				 pRecord->getColumnInt(COLUMN_8),	// age
																																				 pRecord->getColumnInt(COLUMN_9),		// growth
																																				 dcls_from,	// dcls_from
																																				 dcls_to,	// dcls_to
																																				 pRecord->getColumnFloat(COLUMN_12),	// m3sk
																																				 pRecord->getColumnFloat(COLUMN_13),	// m3fub
																																				 pRecord->getColumnFloat(COLUMN_14),	// m3ub
																																				 getIndexForTreeType(pRecord->getColumnText(COLUMN_15)),	// tree_type
																																				 pRecord->getColumnText(COLUMN_16),	// bonitet
																																				 _T(""),	// created
																																				 _T(""),	// coord
																																				 pRecord->getColumnInt(COLUMN_18),	// dist cable
																																				 getIndexForCategory(pRecord->getColumnText(COLUMN_17)),	// category
																																				 pRecord->getColumnInt(COLUMN_19)
																																				 );
								if (!m_pDB->addSampleTrees(data))
									m_pDB->updSampleTrees(data);

						}	// if (pRecord != NULL)				
					}	// if (pRow != NULL)			
				}	// for (int i = 0;i < pRows->GetCount();i++)
				populateData(3);	//@b: beh�ver man l�sa tillbaka n�got man nyss sparade?
			}	// if (!bReturnAndError)
			else
			{
				// Tell user: there's errors in trees; 090202 p�d
				for (int i = 0;i < sarrErrors.GetCount();i++)
					sErrMsg += sarrErrors.GetAt(i) + _T("\n");
				sErrMsg += _T("\n") + m_sTreeCheckMsg7;
				sErrMsg += _T("\n") + m_sTreeCheckMsg8;

				UMMessageBox(this->GetSafeHwnd(),sErrMsg,m_sMsg,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}	// if (pRows != NULL && m_pDB != NULL)
	}

	return bReturn && !bReturnAndError;
}

// Delete selected tree and repopulate; 070516 p�d
int CPageThreeFormView::delSampleTreeInDB(void)
{
	CString S;
	CXTPReportRow* pRow = getSampleReport()->GetFocusedRow();
	if (m_bConnected)	
	{
		if (pRow != NULL)
		{	
			CTraktSampleTreeReportRec *pRec = (CTraktSampleTreeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{

				if (m_pDB != NULL)
				{
					if (m_pDB->delSampleTree(pRec->getTreeRec().getTreeID(),
																	 pRec->getTreeRec().getTraktID()))
					{
						m_bIsDirty = FALSE;
					}	// if (m_pDB->delTree(pRec->getTreeRec().getTreeID(),

				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}

		CXTPReportRecords *pRecs = getSampleReport()->GetRecords();
		if (pRecs != NULL)
		{
			populateData(3);
			return pRecs->GetCount();
		}
	}

	return 0;
}


//-----------------------------------------------------------------------------
// Save DCLS trees "St�mplingsl�ngd - Uttag"; 070821 p�d
BOOL CPageThreeFormView::saveDCLSTreesToDB(void)
{
	CString sErrMsg,S;
	BOOL bReturn = FALSE;
	BOOL bReturnAndError = FALSE;
	CStringArray sarrErrors;
	int nNumOfTrees = 0;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	int nRowCounter = 0;
	int nMaxTreeID = 0;
	CTraktDCLSTreeReportRec *pRecord = NULL;
	
	CTransaction_trakt *pTrakt = getActiveTrakt();
	
	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return FALSE;

		getDCLSReport()->Populate();
		//getDCLSReport()->UpdateWindow();

		pRows = getDCLSReport()->GetRows();


		// Make suew there's something to save; 070515 p�d
		if (pRows != NULL && m_pDB != NULL)
		{
			if (pRows->GetCount() == 0) bReturn = TRUE;

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
	CString sDateDone;
//UMMessageBox(_T("CPageThreeFormView::saveDCLSTreesToDB 1"));

	for (int i = 0;i < pRows->GetCount();i++)
	{
		pRow = pRows->GetAt(i);
		if (pRow != NULL)
		{
			pRecord = (CTraktDCLSTreeReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRecord != NULL)
			{
					if (pRecord->getTreeRec().getTreeID() > 0)
					{
						// Setup the CTransaction_trees from 
						if (m_pDB->checkTraktDCLSTreesDate(pRecord->getTreeRec(),sDateDone) == 0)
						{	
							SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
							populateData(0);	// Uttag
							return FALSE;
						}	// if (pDB->checkTraktSampleTreesDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
					}	// if (pRecord->getTreeRec().getTreeID() > 0)
			}	// if (pRecord != NULL)				
		}	// if (pRow != NULL)			
	}	// for (int i = 0;i < pRows->GetCount();i++)
#endif

			// Do a check of each tree, to see if any one has an error.
			// E.g. missing number of trees etc; 090202 p�d
			sarrErrors.RemoveAll();
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRecord = (CTraktDCLSTreeReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRecord != NULL)
				{
					sErrMsg.Empty();
					nNumOfTrees = pRecord->getColumnInt(COLUMN_6) + pRecord->getColumnInt(COLUMN_15);
					// If tree's undefined, don't save it; 080407 p�d						
					if (pRecord->getColumnText(COLUMN_2).IsEmpty() ||
							pRecord->getColumnFloat(COLUMN_3) < 0.0 ||	// Added 2009-05-05 p�d, possible to have startdcls from 0.0 cm
							//pRecord->getColumnFloat(COLUMN_3) == 0.0 || Commented out 2009-05-05 p�d
							pRecord->getColumnFloat(COLUMN_4) == 0.0 ||
							nNumOfTrees == 0)
							//pRecord->getColumnInt(COLUMN_6) > 0)
					{
						if (sarrErrors.GetCount() > 0)
							sarrErrors.Add(L"");
						S.Format(_T(" --- %s ---"),m_sDCLSCap);
						sarrErrors.Add(S);
						sarrErrors.Add(_T(""));
						S.Format(m_sTreeCheckMsg1);
						sarrErrors.Add(S);
						//S.Format(_T("%s : %d"),m_sTreeCheckMsg2,pRecord->getColumnInt(COLUMN_1));
						//sarrErrors.Add(S);
						if (pRecord->getColumnText(COLUMN_2).IsEmpty())
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg3); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_3) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg4); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_4) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg5);
							sarrErrors.Add(S);

						}
						if (nNumOfTrees == 0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg6);
							sarrErrors.Add(S);
						}
					
						// Check if thre's any error
						bReturnAndError = TRUE;
					}
				}	// if (pRecord != NULL)
			}
			if (!bReturnAndError)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						pRecord = (CTraktDCLSTreeReportRec *)pRow->GetRecord();
						if (pRecord != NULL)
						{
							bReturn = TRUE;

							if (pRecord->getTreeRec().getTreeID() > 0)
								nRowCounter = pRecord->getTreeRec().getTreeID();
							else
							{
								nMaxTreeID = m_pDB->getMaxDCLSTreeID(pTrakt->getTraktID());
								// Added 2008-04-07 p�d
								// If there's no items in list nMaxTreeID = -1
								// Then nRowCounter = 0 (ERROR)
								// With change nRowCounter = 1 for first entry; 080407 p�d
								if (nMaxTreeID == -1) nMaxTreeID = 0;

								nRowCounter = nMaxTreeID + 1;
							}

								CTransaction_dcls_tree data = CTransaction_dcls_tree(nRowCounter,
																														 pTrakt->getTraktID(),
																														 pRecord->getColumnInt(COLUMN_0),
																														 pRecord->getColumnInt(COLUMN_1),
																														 pRecord->getColumnText(COLUMN_2),
																														 pRecord->getColumnFloat(COLUMN_3),	
																														 pRecord->getColumnFloat(COLUMN_4),	
																														 pRecord->getColumnFloat(COLUMN_5),
																														 pRecord->getColumnInt(COLUMN_6),
																														 pRecord->getColumnFloat(COLUMN_7),
																														 pRecord->getColumnFloat(COLUMN_8),	
																														 pRecord->getColumnFloat(COLUMN_9),	
																														 pRecord->getColumnFloat(COLUMN_10),
																														 pRecord->getColumnFloat(COLUMN_11),
																														 pRecord->getColumnFloat(COLUMN_12),
																														 pRecord->getColumnInt(COLUMN_13),	
																														 pRecord->getColumnInt(COLUMN_14),
																														 pRecord->getColumnInt(COLUMN_15),
																														 _T("") );

								if (!m_pDB->addDCLSTrees(data))
										m_pDB->updDCLSTrees(data);
						}	// if (pRecord != NULL)
					}	// if (pRow != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)
				populateData(0);	//@b: beh�ver man verkligen ladda om data som redan finns inl�st?
			}	// if (!bReturnAndError)
			else
			{
				// Tell user: there's errors in trees; 090202 p�d
				for (int i = 0;i < sarrErrors.GetCount();i++)
					sErrMsg += sarrErrors.GetAt(i) + _T("\n");
				sErrMsg += _T("\n") + m_sTreeCheckMsg7;
				sErrMsg += _T("\n") + m_sTreeCheckMsg8;

				UMMessageBox(this->GetSafeHwnd(),sErrMsg,m_sMsg,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}	// if (pRows != NULL && m_pDB != NULL)

	return bReturn && !bReturnAndError;
}

// Delete selected DCLS tree and repopulate; 070516 p�d
int CPageThreeFormView::delDCLSTreeInDB(void)
{
	CString S;
	if (m_bConnected)	
	{
		CXTPReportRow* pRow = getDCLSReport()->GetFocusedRow();
		if (pRow != NULL)
		{	
			CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{

				if (m_pDB != NULL)
				{
					CTransaction_dcls_tree tree = pRec->getTreeRec();
					if (m_pDB->delSampleTreesInDCLS(tree.getDCLS_from(),tree.getDCLS_to(),tree.getTraktID(),tree.getPlotID(),tree.getSpcID()))
					{
						if (m_pDB->delDCLSTree(tree.getTreeID(),tree.getTraktID()))
						{
							m_pDB->updTraktData(CTransaction_trakt_data(tree.getTreeID(),tree.getTraktID(),STMP_LEN_WITHDRAW,tree.getSpcID(),tree.getSpcName(),
																	0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
							clearAll();
							populateData();
							doMatchRandTrees();
							m_bIsDirty = FALSE;
						}	// if (m_pDB->delTree(pRec->getTreeRec().getTreeID(),
					}

				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}

		CXTPReportRecords *pRecs = getDCLSReport()->GetRecords();
		if (pRecs != NULL)
		{
			populateData(0);
			return pRecs->GetCount();
		}
	}
	return 0;
}


// Read trees in m_wndReport, and use these to do calculations
// from, instead of first saving to DB and then read back trees; 070531 p�d
void CPageThreeFormView::getDCLSTreesFromReport(void)
{
	CXTPReportRows *pRows = getDCLSReport()->GetRows();
	CXTPReportRow *pRow = NULL;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	if (pTrakt == NULL)
		return;

	// Make suew there's something to save; 070515 p�d
	if (pRows != NULL )
	{
		m_vecTraktDCLSTrees.clear();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CTraktDCLSTreeReportRec *pRecord = (CTraktDCLSTreeReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					// Setup the CTransaction_trees from 
					CTransaction_dcls_tree data = CTransaction_dcls_tree(i+1,
																										 pTrakt->getTraktID(),
																										 pRecord->getColumnInt(COLUMN_0),
																										 pRecord->getColumnInt(COLUMN_1),
																										 pRecord->getColumnText(COLUMN_2),
																										 pRecord->getColumnFloat(COLUMN_3),
																										 pRecord->getColumnFloat(COLUMN_4),	
																										 pRecord->getColumnFloat(COLUMN_5),	
																										 pRecord->getColumnInt(COLUMN_6),	
																										 pRecord->getColumnFloat(COLUMN_7),	
																										 pRecord->getColumnFloat(COLUMN_8),	
																										 pRecord->getColumnFloat(COLUMN_9),	
																										 pRecord->getColumnFloat(COLUMN_10),
																										 pRecord->getColumnFloat(COLUMN_11),
																										 pRecord->getColumnFloat(COLUMN_12),
																										 pRecord->getColumnInt(COLUMN_13),	
																										 pRecord->getColumnInt(COLUMN_14),
																										 0,
																										 _T("") );
					m_vecTraktDCLSTrees.push_back(data);
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULLL)
}

void CPageThreeFormView::getDCLSTreesFromDB(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
				return;
			}
			m_bConnected = m_pDB->getDCLSTrees(pTrakt->getTraktID(),m_vecTraktDCLSTrees);
		}	// if (m_pDB != NULL)
	}
}

//-----------------------------------------------------------------------------
// Save DCLS1 trees "St�mplingsl�ngd - Kvarst�ende"; 070821 p�d
BOOL CPageThreeFormView::saveDCLS1TreesToDB(void)
{
	CString sErrMsg,S;
	BOOL bReturn = FALSE;
	BOOL bReturnAndError = FALSE;
	CStringArray sarrErrors;
	int nNumOfTrees = 0;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	int nRowCounter = 0;
	int nMaxTreeID = 0;
	CTraktDCLS1TreeReportRec *pRecord = NULL;
	
	CTransaction_trakt *pTrakt = getActiveTrakt();
	
	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return FALSE;

	if (m_bConnected)	
	{
		getDCLS1Report()->Populate();
		pRows = getDCLS1Report()->GetRows();

		// Make suew there's something to save; 070515 p�d
		if (pRows != NULL && m_pDB != NULL)
		{
			if (pRows->GetCount() == 0) bReturn = TRUE;

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
	CString sDateDone;
//UMMessageBox(_T("CPageThreeFormView::saveDCLS1TreesToDB 1"));

	for (int i = 0;i < pRows->GetCount();i++)
	{
		pRow = pRows->GetAt(i);
		if (pRow != NULL)
		{
			pRecord = (CTraktDCLS1TreeReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRecord != NULL)
			{
					if (pRecord->getTreeRec().getTreeID() > 0)
					{
						if (m_pDB->checkTraktDCLS1TreesDate(pRecord->getTreeRec(),sDateDone) == 0)
						{	
							SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
							populateData(1);	// Kvarl�mmnat
							return FALSE;
						}	// if (pDB->checkTraktSampleTreesDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
					}	// if (pRecord->getTreeRec().getTreeID() > 0)
			}	// if (pRecord != NULL)				
		}	// if (pRow != NULL)			
	}	// for (int i = 0;i < pRows->GetCount();i++)
#endif

			// Do a check of each tree, to see if any one has an error.
			// E.g. missing number of trees etc; 090202 p�d
			sarrErrors.RemoveAll();
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRecord = (CTraktDCLS1TreeReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRecord != NULL)
				{
					sErrMsg.Empty();
					nNumOfTrees = pRecord->getColumnInt(COLUMN_6);
					// If tree's undefined, don't save it; 080407 p�d						
					if (pRecord->getColumnText(COLUMN_2).IsEmpty() ||
							pRecord->getColumnFloat(COLUMN_3) == 0.0 ||
							pRecord->getColumnFloat(COLUMN_4) == 0.0 ||
							nNumOfTrees == 0)
							//pRecord->getColumnInt(COLUMN_6) > 0)
					{
						S.Format(_T(" --- %s ---"),m_sDCLS1Cap);
						sarrErrors.Add(S);
						sarrErrors.Add(_T(""));
						S.Format(m_sTreeCheckMsg1);
						sarrErrors.Add(S);
						//S.Format(_T("%s : %d"),m_sTreeCheckMsg2,i+1);
						//sarrErrors.Add(S);
						if (pRecord->getColumnText(COLUMN_2).IsEmpty())
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg3); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_3) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg4); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_4) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg5);
							sarrErrors.Add(S);

						}
						if (nNumOfTrees == 0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg6);
							sarrErrors.Add(S);
						}
					
						// Check if thre's any error
						bReturnAndError = TRUE;
					}
				}	// if (pRecord != NULL)
			}
			if (!bReturnAndError)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						pRecord = (CTraktDCLS1TreeReportRec *)pRow->GetRecord();
						if (pRecord != NULL)
						{
							bReturn = TRUE;

							if (pRecord->getTreeRec().getTreeID() > 0)
								nRowCounter = pRecord->getTreeRec().getTreeID();
							else
							{
								nMaxTreeID= m_pDB->getMaxDCLS1TreeID(pTrakt->getTraktID());
								// Added 2008-04-07 p�d
								// If there's no items in list nMaxTreeID = -1
								// Then nRowCounter = 0 (ERROR)
								// With change nRowCounter = 1 for first entry; 080407 p�d
								if (nMaxTreeID == -1) nMaxTreeID = 0;

								nRowCounter = nMaxTreeID + 1;
							}

								CTransaction_dcls_tree data = CTransaction_dcls_tree(nRowCounter,
																														 pTrakt->getTraktID(),
																														 pRecord->getColumnInt(COLUMN_0),
																														 pRecord->getColumnInt(COLUMN_1),
																														 pRecord->getColumnText(COLUMN_2),
																														 pRecord->getColumnFloat(COLUMN_3),	
																														 pRecord->getColumnFloat(COLUMN_4),	
																														 pRecord->getColumnFloat(COLUMN_5),
																														 pRecord->getColumnInt(COLUMN_6),
																														 pRecord->getColumnFloat(COLUMN_7),
																														 pRecord->getColumnFloat(COLUMN_8),	
																														 pRecord->getColumnFloat(COLUMN_9),	
																														 pRecord->getColumnFloat(COLUMN_10),
																														 pRecord->getColumnFloat(COLUMN_11),
																														 pRecord->getColumnFloat(COLUMN_12),
																														 pRecord->getColumnInt(COLUMN_13),	
																														 pRecord->getColumnInt(COLUMN_14),
																														 0,
																														 _T("") );

								if (!m_pDB->addDCLS1Trees(data))
										m_pDB->updDCLS1Trees(data);
						}	// if (pRecord != NULL)
					}	// if (pRow != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)
				populateData(1);	//@b: ladda om n�r data redan finns?
			}	// if (!bReturnAndError)
			else
			{
				// Tell user: there's errors in trees; 090202 p�d
				for (int i = 0;i < sarrErrors.GetCount();i++)
					sErrMsg += sarrErrors.GetAt(i) + _T("\n");
				sErrMsg += _T("\n") + m_sTreeCheckMsg7;
				sErrMsg += _T("\n") + m_sTreeCheckMsg8;

				UMMessageBox(this->GetSafeHwnd(),sErrMsg,m_sMsg,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}	// if (pRows != NULL && m_pDB != NULL)
	}
	return bReturn && !bReturnAndError;
}

// Delete selected DCLS tree and repopulate; 070516 p�d
int CPageThreeFormView::delDCLS1TreeInDB(void)
{
	CString S;
	if (m_bConnected)	
	{
		CXTPReportRow* pRow = getDCLS1Report()->GetFocusedRow();
		if (pRow != NULL)
		{	
			CTraktDCLS1TreeReportRec *pRec = (CTraktDCLS1TreeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{

				if (m_pDB != NULL)
				{
	/*
					S.Format("CPageThreeFormView::delTree\npRec->getTreeRec().getTreeID() %d\npRec->getTreeRec().getTraktID() %d",
									pRec->getTreeRec().getTreeID(),pRec->getTreeRec().getTraktID());
					UMMessageBox(S);
	*/
					CTransaction_dcls_tree tree = pRec->getTreeRec();
					if (m_pDB->delDCLS1Tree(tree.getTreeID(),tree.getTraktID()))
					{
						m_pDB->updTraktData(CTransaction_trakt_data(tree.getTreeID(),tree.getTraktID(),STMP_LEN_TO_BE_LEFT,tree.getSpcID(),tree.getSpcName(),
																0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
						m_bIsDirty = FALSE;
					}	// if (m_pDB->delTree(pRec->getTreeRec().getTreeID(),

				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}

		CXTPReportRecords *pRecs = getDCLS1Report()->GetRecords();
		if (pRecs != NULL)
		{
			populateData(1);
			return pRecs->GetCount();
		}
	}
	return 0;
}


// Read trees in m_wndReport, and use these to do calculations
// from, instead of first saving to DB and then read back trees; 070531 p�d
void CPageThreeFormView::getDCLS1TreesFromReport(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	CTransaction_trakt *pTrakt = getActiveTrakt();

	if (pTrakt == NULL)
		return;

	getDCLS1Report()->Populate();
	pRows = getDCLS1Report()->GetRows();

	// Make suew there's something to save; 070515 p�d
	if (pRows != NULL)
	{
		m_vecTraktDCLS1Trees.clear();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CTraktDCLS1TreeReportRec *pRecord = (CTraktDCLS1TreeReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					// Setup the CTransaction_trees from 
					CTransaction_dcls_tree data = CTransaction_dcls_tree(i+1,
																										 pTrakt->getTraktID(),
																										 pRecord->getColumnInt(COLUMN_0),
																										 pRecord->getColumnInt(COLUMN_1),
																										 pRecord->getColumnText(COLUMN_2),
																										 pRecord->getColumnFloat(COLUMN_3),	
																										 pRecord->getColumnFloat(COLUMN_4),	
																										 pRecord->getColumnFloat(COLUMN_5),	
																										 pRecord->getColumnInt(COLUMN_6),	
																										 pRecord->getColumnFloat(COLUMN_7),	
																										 pRecord->getColumnFloat(COLUMN_8),	
																										 pRecord->getColumnFloat(COLUMN_9),	
																										 pRecord->getColumnFloat(COLUMN_10),
																										 pRecord->getColumnFloat(COLUMN_11),
																										 pRecord->getColumnFloat(COLUMN_12),
																										 pRecord->getColumnInt(COLUMN_13),	
																										 pRecord->getColumnInt(COLUMN_14),
																										 0,
																										 _T("") );
					m_vecTraktDCLS1Trees.push_back(data);
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

void CPageThreeFormView::getDCLS1TreesFromDB(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
			}
			else
				m_bConnected = m_pDB->getDCLS1Trees(pTrakt->getTraktID(),m_vecTraktDCLS1Trees);
		}	// if (m_pDB != NULL)
	}
}

//-----------------------------------------------------------------------------
// Save DCLS1 trees "St�mplingsl�ngd - Uttag i Stickv�g"; 070821 p�d
BOOL CPageThreeFormView::saveDCLS2TreesToDB(void)
{
	CString sErrMsg,S;
	BOOL bReturn = FALSE;
	BOOL bReturnAndError = FALSE;
	CStringArray sarrErrors;
	int nNumOfTrees = 0;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	int nRowCounter = 0;
	int nMaxTreeID = 0;
	CTraktDCLS2TreeReportRec *pRecord = NULL;
	
	CTransaction_trakt *pTrakt = getActiveTrakt();
	
	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return FALSE;

	if (m_bConnected)	
	{
		getDCLS2Report()->Populate();
		pRows = getDCLS2Report()->GetRows();

		// Make suew there's something to save; 070515 p�d
		if (pRows != NULL && m_pDB != NULL)
		{
			if (pRows->GetCount() == 0) bReturn = TRUE;
			
//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
	CString sDateDone;
//UMMessageBox(_T("CPageThreeFormView::saveDCLS2TreesToDB 1"));

	for (int i = 0;i < pRows->GetCount();i++)
	{
		pRow = pRows->GetAt(i);
		if (pRow != NULL)
		{
			pRecord = (CTraktDCLS2TreeReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRecord != NULL)
			{
					if (pRecord->getTreeRec().getTreeID() > 0)
					{
						if (m_pDB->checkTraktDCLS2TreesDate(pRecord->getTreeRec(),sDateDone) == 0)
						{	
							SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
							populateData(2);	// Uttag i stickv�g
							return FALSE;
						}	// if (pDB->checkTraktSampleTreesDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
					}	// if (pRecord->getTreeRec().getTreeID() > 0)
			}	// if (pRecord != NULL)				
		}	// if (pRow != NULL)			
	}	// for (int i = 0;i < pRows->GetCount();i++)
#endif
		
			
			// Do a check of each tree, to see if any one has an error.
			// E.g. missing number of trees etc; 090202 p�d
			sarrErrors.RemoveAll();
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRecord = (CTraktDCLS2TreeReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRecord != NULL)
				{
					sErrMsg.Empty();
					nNumOfTrees = pRecord->getColumnInt(COLUMN_6);
					// If tree's undefined, don't save it; 080407 p�d						
					if (pRecord->getColumnText(COLUMN_2).IsEmpty() ||
							pRecord->getColumnFloat(COLUMN_3) == 0.0 ||
							pRecord->getColumnFloat(COLUMN_4) == 0.0 ||
							nNumOfTrees == 0)
							//pRecord->getColumnInt(COLUMN_6) > 0)
					{
						S.Format(_T(" --- %s ---"),m_sDCLS2Cap);
						sarrErrors.Add(S);
						sarrErrors.Add(_T(""));
						S.Format(m_sTreeCheckMsg1);
						sarrErrors.Add(S);
						//S.Format(_T("%s : %d"),m_sTreeCheckMsg2,i+1);
						//sarrErrors.Add(S);
						if (pRecord->getColumnText(COLUMN_2).IsEmpty())
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg3); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_3) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg4); 
							sarrErrors.Add(S);
						}
						if (pRecord->getColumnFloat(COLUMN_4) == 0.0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg5);
							sarrErrors.Add(S);

						}
						if (nNumOfTrees == 0)
						{
							S.Format(_T("- %s"),m_sTreeCheckMsg6);
							sarrErrors.Add(S);
						}
					
						// Check if thre's any error
						bReturnAndError = TRUE;
					}
				}	// if (pRecord != NULL)
			}
			if (!bReturnAndError)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						pRecord = (CTraktDCLS2TreeReportRec *)pRow->GetRecord();
						if (pRecord != NULL)
						{
							bReturn = TRUE;

							if (pRecord->getTreeRec().getTreeID() > 0)
								nRowCounter = pRecord->getTreeRec().getTreeID();
							else
							{
								nMaxTreeID= m_pDB->getMaxDCLS2TreeID(pTrakt->getTraktID());
								// Added 2008-04-07 p�d
								// If there's no items in list nMaxTreeID = -1
								// Then nRowCounter = 0 (ERROR)
								// With change nRowCounter = 1 for first entry; 080407 p�d
								if (nMaxTreeID == -1) nMaxTreeID = 0;

								nRowCounter = nMaxTreeID + 1;
							}
								CTransaction_dcls_tree data = CTransaction_dcls_tree(nRowCounter,
																														 pTrakt->getTraktID(),
																														 pRecord->getColumnInt(COLUMN_0),
																														 pRecord->getColumnInt(COLUMN_1),
																														 pRecord->getColumnText(COLUMN_2),
																														 pRecord->getColumnFloat(COLUMN_3),	
																														 pRecord->getColumnFloat(COLUMN_4),	
																														 pRecord->getColumnFloat(COLUMN_5),
																														 pRecord->getColumnInt(COLUMN_6),
																														 pRecord->getColumnFloat(COLUMN_7),
																														 pRecord->getColumnFloat(COLUMN_8),	
																														 pRecord->getColumnFloat(COLUMN_9),	
																														 pRecord->getColumnFloat(COLUMN_10),
																														 pRecord->getColumnFloat(COLUMN_11),
																														 pRecord->getColumnFloat(COLUMN_12),
																														 pRecord->getColumnInt(COLUMN_13),	
																														 pRecord->getColumnInt(COLUMN_14),
																														 0,
																														 _T("") );

								if (!m_pDB->addDCLS2Trees(data))
										m_pDB->updDCLS2Trees(data);
						}	// if (pRecord != NULL)
					}	// if (pRow != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)
				populateData(2);
			}	// if (!bReturnAndError)
			else
			{
				// Tell user: there's errors in trees; 090202 p�d
				for (int i = 0;i < sarrErrors.GetCount();i++)
					sErrMsg += sarrErrors.GetAt(i) + _T("\n");
				sErrMsg += _T("\n") + m_sTreeCheckMsg7;
				sErrMsg += _T("\n") + m_sTreeCheckMsg8;

				UMMessageBox(this->GetSafeHwnd(),sErrMsg,m_sMsg,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}	// if (pRows != NULL && m_pDB != NULL)
	}
	return bReturn && !bReturnAndError;
}

// Delete selected DCLS tree and repopulate; 070516 p�d
int CPageThreeFormView::delDCLS2TreeInDB(void)
{
	CString S;
	if (m_bConnected)	
	{
		CXTPReportRow* pRow = getDCLS2Report()->GetFocusedRow();
		if (pRow != NULL)
		{	
			CTraktDCLS2TreeReportRec *pRec = (CTraktDCLS2TreeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{

				if (m_pDB != NULL)
				{
	/*
					S.Format("CPageThreeFormView::delTree\npRec->getTreeRec().getTreeID() %d\npRec->getTreeRec().getTraktID() %d",
									pRec->getTreeRec().getTreeID(),pRec->getTreeRec().getTraktID());
					UMMessageBox(S);
	*/
					CTransaction_dcls_tree tree = pRec->getTreeRec();
					if (m_pDB->delDCLS2Tree(tree.getTreeID(),tree.getTraktID()))
					{
						m_pDB->updTraktData(CTransaction_trakt_data(tree.getTreeID(),tree.getTraktID(),STMP_LEN_WITHDRAW_ROAD,tree.getSpcID(),tree.getSpcName(),
																0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
						m_bIsDirty = FALSE;
					}	// if (m_pDB->delTree(pRec->getTreeRec().getTreeID(),

				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}

		CXTPReportRecords *pRecs = getDCLS2Report()->GetRecords();
		if (pRecs != NULL)
		{
			populateData(2);
			return pRecs->GetCount();
		}
	}

	return 0;
}


// Read trees in m_wndReport, and use these to do calculations
// from, instead of first saving to DB and then read back trees; 070531 p�d
void CPageThreeFormView::getDCLS2TreesFromReport(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	CTransaction_trakt *pTrakt = getActiveTrakt();

	if (pTrakt == NULL)
		return;

	getDCLS2Report()->Populate();
	pRows = getDCLS2Report()->GetRows();

	// Make suew there's something to save; 070515 p�d
	if (pRows != NULL)
	{
		m_vecTraktDCLS2Trees.clear();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CTraktDCLS1TreeReportRec *pRecord = (CTraktDCLS1TreeReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					// Setup the CTransaction_trees from 
					CTransaction_dcls_tree data = CTransaction_dcls_tree(i+1,
																										 pTrakt->getTraktID(),
																										 pRecord->getColumnInt(COLUMN_0),
																										 pRecord->getColumnInt(COLUMN_1),
																										 pRecord->getColumnText(COLUMN_2),
																										 pRecord->getColumnFloat(COLUMN_3),	
																										 pRecord->getColumnFloat(COLUMN_4),	
																										 pRecord->getColumnFloat(COLUMN_5),	
																										 pRecord->getColumnInt(COLUMN_6),	
																										 pRecord->getColumnFloat(COLUMN_7),	
																										 pRecord->getColumnFloat(COLUMN_8),	
																										 pRecord->getColumnFloat(COLUMN_9),	
																										 pRecord->getColumnFloat(COLUMN_10),
																										 pRecord->getColumnFloat(COLUMN_11),
																										 pRecord->getColumnFloat(COLUMN_12),
																										 pRecord->getColumnInt(COLUMN_13),	
																										 pRecord->getColumnInt(COLUMN_14),
																										 0,
																										 _T("") );
					m_vecTraktDCLS2Trees.push_back(data);
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

void CPageThreeFormView::getDCLS2TreesFromDB(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
			}
			else
				m_bConnected = m_pDB->getDCLS2Trees(pTrakt->getTraktID(),m_vecTraktDCLS2Trees);
		}	// if (m_pDB != NULL)
	}
}


// Delete selected tree and repopulate; 070516 p�d
void CPageThreeFormView::delPlots(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			CTransaction_trakt *pTrakt = getActiveTrakt();
			if (pTrakt == NULL)
			{
				m_bConnected = FALSE;
			}
			else
				m_pDB->delPlot(pTrakt->getTraktID());
		}	// if (m_pDB != NULL)
		populateData();
	}
}
// Added 2009-01-27 P�D
void CPageThreeFormView::addPlotType(int id,int trakt_id,int plot_type)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			m_pDB->addPlotType(id,trakt_id,plot_type);
		}	// if (m_pDB != NULL)
		populateData();
	}
}

// Added 2008-03-03 P�D
void CPageThreeFormView::updPlotType(int trakt_id,int plot_type)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			m_pDB->updPlotType(trakt_id,plot_type);
		}	// if (m_pDB != NULL)
		populateData();
	}
}

void CPageThreeFormView::removeTreesAndPlotsInTrakt(void)
{
	int nIndex = -1;
	// Check which page were on, DCLS or Samples; 071001 p�d
	nIndex = getTabPageIndex();
	if (nIndex == 0)	// DCLS "St�mplingsl�ngd - Uttag"
	{
		CXTPReportRow* pRow = getDCLSReport()->GetFocusedRow();
		if (pRow != NULL)
		{	
			CTraktDCLSTreeReportRec *pRec = (CTraktDCLSTreeReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getTreeRec().getNumOf() == 0 && pRec->getTreeRec().getNumOfRandTrees() > 0)
				{
					UMMessageBox(m_sRemoveRandTreesFirstMsg, MB_ICONEXCLAMATION|MB_OK);
					return;
				}
			}
		}
		if (UMMessageBox(this->GetSafeHwnd(),m_sRemoveTreesMsg1,m_sMsg,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			// Delete selected trees in Sample or DCLS; 071001 p�d
			delDCLSTreeInDB();
			// Do a recalculation, also; 071001 p�d
			//runDoCalulationInPageThree(-1,FALSE,FALSE);
		}
		// Also set Diameterclass settings in COnNewTraktDialog on Prop.pane; 070824 p�d
		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		if (pFrame != NULL)
		{
			pFrame->setReadOnlyInSettingsProperty(FALSE);
		}
	}
	if (nIndex == 1)	// DCLS "St�mplingsl�ngd - Kvarl�mmnat"
	{
		if (UMMessageBox(this->GetSafeHwnd(),m_sRemoveTreesMsg2,m_sMsg,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			// Delete selected trees in Sample or DCLS; 071001 p�d
			delDCLS1TreeInDB();
			// Do a recalculation, also; 071001 p�d
			runDoCalulationInPageThree(-1,FALSE,FALSE);
		}
		// Also set Diameterclass settings in COnNewTraktDialog on Prop.pane; 070824 p�d
		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		if (pFrame != NULL)
		{
			pFrame->setReadOnlyInSettingsProperty(FALSE);
		}
	}
	if (nIndex == 2)	// DCLS "St�mplingsl�ngd - Uttag i Stickv�g"
	{
		if (UMMessageBox(this->GetSafeHwnd(),m_sRemoveTreesMsg3,m_sMsg,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			// Delete selected trees in Sample or DCLS; 071001 p�d
			delDCLS2TreeInDB();
			// Do a recalculation, also; 071001 p�d
			runDoCalulationInPageThree(-1,FALSE,FALSE);
		}
		// Also set Diameterclass settings in COnNewTraktDialog on Prop.pane; 070824 p�d
		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		if (pFrame != NULL)
		{
			pFrame->setReadOnlyInSettingsProperty(FALSE);
		}
	}
	else if (nIndex == 3)	// Samples trees
	{
		if (getSampleReport()->GetRecords()->GetCount())
			doUpdateRandTrees2();
		/*if (UMMessageBox(this->GetSafeHwnd(),m_sRemoveTreesMsg4,m_sMsg,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			// Delete selected trees in Sample or DCLS; 071001 p�d
			delSampleTreeInDB();
			// Do a recalculation, also; 071001 p�d
			runDoCalulationInPageThree(-1,FALSE,FALSE);
		}*/
		// Also set Diameterclass settings in COnNewTraktDialog on Prop.pane; 070824 p�d
		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		if (pFrame != NULL)
		{
			pFrame->setReadOnlyInSettingsProperty(FALSE);
		}
	}
}

// Method for calculating sum price per Assortments per specie from; 070531 p�d
// from: esti_trees_assort_table
// to: esti_trakt_spc_assort_table
void CPageThreeFormView::calculateAssortFromTreesToSpc(void)
{
	vecTransactionTraktSetSpc vecSetSpc;
	double fM3FubToM3To = 0.0;
	double fPriceFub = 0.0;
	double fPriceTo = 0.0;
	double fCalcPriceFub = 0.0;
	double fCalcPriceTo = 0.0;
	double fVolumeFub = 0.0;
	double fVolumeTo = 0.0;
	double fKrPerM3 = 0.0;
					CString S;

	CTransaction_trakt *pTrakt = getActiveTrakt();

	if (pTrakt == NULL || !m_bConnected || m_pDB == NULL)
		return;


	if (m_bConnected)	
	{
		// Load data from: esti_trakt_spc_assort_table
		// and from: esti_trakt_set_spc_table
		if (m_pDB != NULL)
		{
			// Get information on functins set in trakt per specie; 070705 p�d
			m_pDB->getTraktSetSpc(vecSetSpc,pTrakt->getTraktID());
			// Reset values in table "esti_trakt_spc_assort_table", before adding
			// new values for Trakt; 070705 p�d
			// This is becuse we need to remove assortments data for specie(s), that maybe have
			// been removed from tree-list on PageThree; 070705 p�d
			m_pDB->resetTraktAss(pTrakt->getTraktID());
			// Get resetted data in table "esti_trakt_spc_assort_table"; 070705 p�d
			m_pDB->getTraktAss(m_vecTransactionTraktAss,pTrakt->getTraktID());
		}
		if (m_vecTransactionTraktAss.size() > 0 && m_vecTransactionTreeAssort.size() > 0)
		{
			// Use m_vecTransactionTraktAss vector to check for Specie (tass_trakt_data_id)
			for (UINT i = 0;i < m_vecTransactionTraktAss.size();i++)
			{
				fPriceFub = 0.0;
				fPriceTo = 0.0;
				fVolumeFub = 0.0;
				fVolumeTo = 0.0;
				fCalcPriceFub = 0.0;
				fCalcPriceTo = 0.0;
				CTransaction_trakt_ass tdata = m_vecTransactionTraktAss[i];
				// Get info on "�verf�ringstal" for specie; 070531 p�d
				for (UINT iii = 0;iii < vecSetSpc.size();iii++)
				{
					CTransaction_trakt_set_spc tsetspc = vecSetSpc[iii];
					if (tsetspc.getSpcID() == tdata.getTAssTraktDataID())
					{
						fM3FubToM3To = tsetspc.getM3FubToM3To();
						fPriceFub = tdata.getPriceM3FUB();
						fPriceTo = tdata.getPriceM3TO();
						fKrPerM3 = tdata.getKrPerM3();
						break;
					}	// if (tsetspc.getSpcID() == tdata.getTAssTraktDataID())
				}	// for (UINT iii = 0;iii < vecSetSpc.size();iii++)


				for (UINT ii = 0;ii < m_vecTransactionTreeAssort.size();ii++)
				{
					CTransaction_tree_assort tass = m_vecTransactionTreeAssort[ii];
					if (tass.getSpcID() == tdata.getTAssTraktDataID() && tass.getAssortName() == tdata.getTAssName())
					{
		
						fVolumeFub = tass.getExchVolume();
						fCalcPriceFub = tass.getExchVolume()*fPriceFub; //tass.getExchPrice();
						fCalcPriceTo = 0.0;
						// Calculate To price for assortment, based on setting of PriceIn; 070531 p�d
						if (tass.getPriceIn() == ASSORTMENT_TIMBER_IN_M3TO)
						{
							fVolumeTo = fVolumeFub * fM3FubToM3To;
							fCalcPriceTo = fVolumeTo*fPriceTo;
	
						}	// if (tass.getPriceIn() == ASSORTMENT_TIMBER_IN_M3TO)
						else if (tass.getPriceIn() == ASSORTMENT_TIMBER_IN_M3FUB)
						{
							fVolumeTo = 0.0; //fVolumeFub * fM3FubToM3To;
							fCalcPriceFub = fVolumeFub*fPriceFub;
						}	// if (tass.getPriceIn() == ASSORTMENT_TIMBER_IN_M3FUB)

						m_vecTransactionTraktAss[i].setM3FUB(fVolumeFub*m_fArealCalculationFactor);	// Do "Arealsuppr�kning"
						m_vecTransactionTraktAss[i].setM3TO(fVolumeTo*m_fArealCalculationFactor);	// Do "Arealsuppr�kning"
						// Added calculation for "Kronor per kubikmeter"; 080305 p�d
						if (fCalcPriceFub > 0.0)
							m_vecTransactionTraktAss[i].setValueM3FUB((fCalcPriceFub+(fKrPerM3*fVolumeFub))*m_fArealCalculationFactor);	// Do "Arealsuppr�kning"
						else
							m_vecTransactionTraktAss[i].setValueM3FUB(0.0);
						if (fCalcPriceTo > 0.0)
							m_vecTransactionTraktAss[i].setValueM3TO((fCalcPriceTo+(fKrPerM3*fVolumeTo))*m_fArealCalculationFactor);	// Do "Arealsuppr�kning"
						else
							m_vecTransactionTraktAss[i].setValueM3TO(0.0);

						// Save to: esti_trakt_spc_assort_table
						if (m_pDB != NULL)
						{
							if (!m_pDB->addTraktAss(m_vecTransactionTraktAss[i]))
								m_pDB->updTraktAss(m_vecTransactionTraktAss[i]);
						}	// if (m_pDB != NULL)

					}	// if (tass.getSpcID() == tdata.getTAssTraktDataID() && tass.getAssortName() == tdata.getTAssName())

				}	// for (UINT ii = 0;ii < m_vecTransactionTreeAssort.size();i++)

			}	// for (UINT i = 0;i < m_vecTransactionTraktAss.size();i++)
		}	// if (m_vecTransactionTraktAss.size() > 0 && m_vecTransactionTreeAssort.size() > 0)
	}
	vecSetSpc.clear();
}


//#4380 20150604 J�
//Ta bort en ur antal i uttagsdiameterklasstabellen
BOOL CPageThreeFormView::Remove_From_Dcls_Uttag_NumOfKanttrad(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	if(m_pDB->traktDCLSTreeExistsInClass(data))
	{
		bRet=m_pDB->updRemoveNumKanttradFromDclsUttag(data);
		if(bRet)
			m_pDB->delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsUttag(data);
	}
	return bRet;
}


//#4380 20150604 J�
//Ta bort en ur antal i uttagsdiameterklasstabellen
BOOL CPageThreeFormView::Remove_From_Dcls_Remain(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	if(m_pDB->traktDCLS1TreeExistsInClass(data))
	{
		bRet=m_pDB->updRemoveFromDclsRemain(data);
		m_pDB->delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsRemain(data);
	}
	return bRet;
}

//#4380 20150603 J�
//Ta bort en ur antal i uttagsdiameterklasstabellen
BOOL CPageThreeFormView::Remove_From_Dcls_Uttag(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	if(m_pDB->traktDCLSTreeExistsInClass(data))
	{
		bRet=m_pDB->updRemoveFromDclsUttag(data);
		if(bRet)
			m_pDB->delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsUttag(data);
	}
	return bRet;
}

BOOL CPageThreeFormView::Add_To_Dcls_Remain(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nRowCounter=-1,nMaxTreeID=-1;
	if(!m_pDB->traktDCLS1TreeExistsInClass(data))
	{
			nMaxTreeID = m_pDB->getMaxDCLS1TreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;

		CTransaction_dcls_tree dcls_data = CTransaction_dcls_tree(
			nRowCounter,
			data.getTraktID(),
			data.getPlotID(),
			data.getSpcID(),
			data.getSpcName(),
			data.getDCLS_from(),
			data.getDCLS_to(),
			data.getHgt(),
			1,
			data.getGCrownPerc(),
			data.getBarkThick(),
			data.getM3Sk(),
			data.getM3Fub(),
			data.getM3Ub(),
			data.getGROT(),
			data.getAge(),
			data.getGrowth(),
			0,
			_T(""));

		if (!m_pDB->addDCLS1Trees(dcls_data))
			bRet=FALSE;

		//populateData(0);	// Uttag
		//resetIsDirty();
		bRet=TRUE;
	}
	else
		bRet=m_pDB->updAddNumOfToDclsRemain(data);
	return bRet;

}

BOOL CPageThreeFormView::Add_To_Dcls_Uttag(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nRowCounter=-1,nMaxTreeID=-1;
	if(!m_pDB->traktDCLSTreeExistsInClass(data))
	{
			nMaxTreeID = m_pDB->getMaxDCLSTreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;

		CTransaction_dcls_tree dcls_data = CTransaction_dcls_tree(
			nRowCounter,
			data.getTraktID(),
			data.getPlotID(),
			data.getSpcID(),
			data.getSpcName(),
			data.getDCLS_from(),
			data.getDCLS_to(),
			data.getHgt(),
			1,
			data.getGCrownPerc(),
			data.getBarkThick(),
			data.getM3Sk(),
			data.getM3Fub(),
			data.getM3Ub(),
			data.getGROT(),
			data.getAge(),
			data.getGrowth(),
			0,
			_T(""));

		if (!m_pDB->addDCLSTreesWithoutNumOfCheck(dcls_data))
			bRet=FALSE;

		//populateData(0);	// Uttag
		//resetIsDirty();
		bRet=TRUE;
	}
	else
		bRet=m_pDB->updAddNumOfToDclsUttag(data);
	return bRet;
}

//#4380 20150603 J�
//L�gg till ett kanttr�d i dcls tabellen f�r kvarl�mnat, finns den inte skapa klassen
/*
BOOL CPageThreeFormView::Add_To_Dcls_Remain_NumOfKantTrad(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nRowCounter=-1,nMaxTreeID=-1;
	//Om inte diameterklassen finns i kvarvarande s� skapa den
	if(!m_pDB->traktDCLS1TreeExistsInClass(data))
	{
		if (data.getTreeID() > 0)
			nRowCounter = data.getTreeID();
		else
		{
			nMaxTreeID = m_pDB->getMaxDCLS1TreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;
		}

		CTransaction_dcls_tree dcls_data = CTransaction_dcls_tree(
			nRowCounter,
			data.getTraktID(),
			data.getPlotID(),
			data.getSpcID(),
			data.getSpcName(),
			data.getDCLS_from(),
			data.getDCLS_to(),
			data.getHgt(),
			1,
			data.getGCrownPerc(),
			data.getBarkThick(),
			data.getM3Sk(),
			data.getM3Fub(),
			data.getM3Ub(),
			data.getGROT(),
			data.getAge(),
			data.getGrowth(),
			0,
			_T(""));

		if (!m_pDB->addDCLS1Trees(dcls_data))
			bRet=FALSE;

		populateData(1);	// Uttag
		resetIsDirty();
		bRet=TRUE;
	}
	else
		bRet=m_pDB->updAddNumKanttradToDclsRemain(data);
	return bRet;
}*/


//#4380 20150603 J�
//L�gg till ett kanttr�d i dcls tabellen f�r uttag, finns den inte skapa klassen
BOOL CPageThreeFormView::Add_To_Dcls_Uttag_NumOfKantTrad(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nRowCounter=-1,nMaxTreeID=-1;
	if(!m_pDB->traktDCLSTreeExistsInClass(data))
	{

			nMaxTreeID = m_pDB->getMaxDCLSTreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;
		

		CTransaction_dcls_tree dcls_data = CTransaction_dcls_tree(
			nRowCounter,
			data.getTraktID(),
			data.getPlotID(),
			data.getSpcID(),
			data.getSpcName(),
			data.getDCLS_from(),
			data.getDCLS_to(),
			data.getHgt(),
			0,
			data.getGCrownPerc(),
			data.getBarkThick(),
			data.getM3Sk(),
			data.getM3Fub(),
			data.getM3Ub(),
			data.getGROT(),
			data.getAge(),
			data.getGrowth(),
			1,
			_T(""));

		if (!m_pDB->addDCLSTreesWithoutNumOfCheck(dcls_data))
			bRet=FALSE;

		//populateData(0);	// Uttag
		//resetIsDirty();
		bRet=TRUE;
	}
	else
		bRet=m_pDB->updAddNumKanttradToDclsUttag(data);
	return bRet;
}

/*
BOOL CPageThreeFormView::Show_Change_Type_Message(int nFromType,int nToType,CTransaction_sample_tree data)
{
	CString cMsg=_T("");
	BOOL bRet=FALSE;

	switch(nFromType)
	{
	case SAMPLE_TREE:				//	0		// A sampletree. I.e. it has a heigth etc
		switch(nToType)
		{
		case SAMPLE_TREE:
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);
			//Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag, uppdatera antal kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);
			//Add_To_Dcls_Remain_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_EXTRA:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);					
			break;
		case TREE_OUTSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);					
			break;
		case TREE_OUTSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);					
			break;
		case TREE_INSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);					
			break;
		case TREE_INSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Uttag(data);					
			break;
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		}
		break;
	case TREE_SAMPLE_REMAINIG:		//	3		// "Provtr�d (Kvarl�mmnat)"
		switch(nToType)
		{
		case SAMPLE_TREE:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			//Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			//Add_To_Dcls_Remain_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			break;
		case TREE_SAMPLE_EXTRA:
			cMsg.Format(_T("Byt till %S inl�st, ta bort fr�n st�mplingsl�ngd kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			break;
		case TREE_OUTSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			break;
		case TREE_OUTSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			break;
		case TREE_INSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			break;
		case TREE_INSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//Remove_From_Dcls_Remain(data);
			break;
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S"),m_sarrTreeTypes.GetAt(nToType));
			break;
		}
		break;
	case TREE_OUTSIDE:				//	1		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
	case TREE_OUTSIDE_NO_SAMP:		//	2		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
		switch(nToType)
		{
		case SAMPLE_TREE:		
			cMsg.Format(_T("Byt till %s, ta bort fr�n antal kanttr�d uttag, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//if(Remove_From_Dcls_Uttag_NumOfKanttrad(data))
			//	bRet=Add_To_Dcls_Uttag(data);
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			//bRet=TRUE;
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, ta bort fr�n antal kanttr�d uttag, uppdatera antal tr�d kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//if(Remove_From_Dcls_Uttag_NumOfKanttrad(data))
			//	bRet=Add_To_Dcls_Remain(data);
			break;
		case TREE_SAMPLE_EXTRA:
			cMsg.Format(_T("Byt till %S, ta bort fr�n antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
			//bRet=Remove_From_Dcls_Uttag_NumOfKanttrad(data);
			break;
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S, ta bort fr�n antal kanttr�d uttag, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//if(Check_If_Pos(data))
			//{
			//	if(Remove_From_Dcls_Uttag_NumOfKanttrad(data))
			//		bRet=Add_To_Dcls_Uttag(data);
			//}
			break;
		}
		break;
	case TREE_SAMPLE_EXTRA:			//	4		// "Provtr�d (Extra inl�sta)"
		switch(nToType)
		{
		case SAMPLE_TREE:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag(data);
			break;
		case TREE_OUTSIDE:
			cMsg.Format(_T("Byt till %S, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Remain(data);
			break;
		case TREE_SAMPLE_EXTRA:
			break;
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			//bRet=TRUE;
			break;
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//if(Check_If_Pos(data))
			//	bRet=Add_To_Dcls_Uttag(data);
			break;
		}
		break;
	case TREE_OUTSIDE_LEFT:			//	5	// Kanttr�d l�mnas (Provtr�d)
	case TREE_OUTSIDE_LEFT_NO_SAMP:	//	6	// Kanttr�d l�mnas  (Ber�knad h�jd)
		switch(nToType)
		{
		case SAMPLE_TREE:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag(data);
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Remain(data);
			break;
		case TREE_SAMPLE_EXTRA:
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			//bRet=TRUE;
			break;						
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag(data);
			break;
		}
		break;
	case TREE_POS_NO_SAMP:			//	7	// Pos.tr�d (uttag)							
		switch(nToType)
		{
		case SAMPLE_TREE:
			//bRet=TRUE;
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//if(Remove_From_Dcls_Uttag(data))
			//	bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag, uppdatera antal tr�d kvarvarande"),m_sarrTreeTypes.GetAt(nToType));
			//if(Remove_From_Dcls_Uttag(data))
			//	bRet=Add_To_Dcls_Remain(data);
			break;
		case TREE_SAMPLE_EXTRA:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_OUTSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_OUTSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_INSIDE_LEFT:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;
		case TREE_INSIDE_LEFT_NO_SAMP:
			cMsg.Format(_T("Byt till %S, ta bort fr�n st�mplingsl�ngd uttag"),m_sarrTreeTypes.GetAt(nToType));
			break;			
			//bRet=Remove_From_Dcls_Uttag(data);
			break;
		case TREE_POS_NO_SAMP:
			//bRet=TRUE;
			break;
		}
		break;
	case TREE_INSIDE_LEFT:			//	8	// Tr�d i gatan l�mnas (Provtr�d)
	case TREE_INSIDE_LEFT_NO_SAMP:	//	9	// Tr�d i gatan l�mnas (Ber�knad h�jd)
		switch(nToType)
		{
		case SAMPLE_TREE:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag(data);
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			cMsg.Format(_T("Byt till %S, uppdatera antal kanttr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
			break;
		case TREE_SAMPLE_REMAINIG:
			cMsg.Format(_T("Byt till %S, uppdatera antal tr�d kvarl�mnat"),m_sarrTreeTypes.GetAt(nToType));
			//bRet=Add_To_Dcls_Remain(data);
			break;
		case TREE_SAMPLE_EXTRA:
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			//bRet=TRUE;
			break;
		case TREE_POS_NO_SAMP:
			if(Check_If_Pos(data))
				cMsg.Format(_T("Byt till %S, uppdatera antal tr�d uttag"),m_sarrTreeTypes.GetAt(nToType));
				//bRet=Add_To_Dcls_Uttag(data);
			break;
		}
		break;
	}

	if(cMsg.GetLength()>1)
		if (UMMessageBox(this->GetSafeHwnd(),cMsg,m_sChangeTreeType1,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			bRet=TRUE;
		}
	return bRet;

}*/

BOOL CPageThreeFormView::Check_If_Pos(CTransaction_sample_tree data)
{
	CString cCoord=_T("");
	cCoord.Format(_T("%s"),data.getCoord());
	if(cCoord.GetLength()>5)
		return TRUE;
	return FALSE;
}

// #4380 20150612 J� Byta tr�dslag
BOOL CPageThreeFormView::doSpecieHasChanged(CTransaction_sample_tree data,int nToSpec,CString cSpcName)
{
	BOOL bRet=FALSE;
	CTransaction_sample_tree data2  = CTransaction_sample_tree(data.getTreeID()
		,data.getTraktID()
		,data.getPlotID()
		,nToSpec
		,cSpcName
		,data.getDBH()
		,data.getHgt()
		,data.getGCrownPerc()
		,data.getBarkThick()
		,data.getGROT()
		,data.getAge()
		,data.getGrowth()
		,data.getDCLS_from()
		,data.getDCLS_to()
		,data.getM3Sk()
		,data.getM3Fub()
		,data.getM3Ub()
		,data.getTreeType() 
		,data.getBonitet()
		,data.getCreated()
		,data.getCoord()
		,data.getDistCable()
		,data.getCategory()
		,data.getTopCut());

	if(nToSpec!=-1)
	{
		//Kontrollera vilken tabell det g�ller
		switch(data2.getTreeType())
		{
			//Uttagstabell
		case SAMPLE_TREE:
		case TREE_POS_NO_SAMP:
			//Ta bort det gamla tr�dslaget fr�n diameterklasstabellen l�gg till det nya
			Remove_From_Dcls_Uttag(data);
			bRet=Add_To_Dcls_Uttag(data2);
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			//Ta bort det gamla tr�dslaget fr�n diameterklasstabellen l�gg till det nya
			Remove_From_Dcls_Uttag_NumOfKanttrad(data);
			bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data2);
			break;
			//Kvarvarande tabell
		case TREE_SAMPLE_REMAINIG:
			//Ta bort det gamla tr�dslaget fr�n diameterklasstabellen l�gg till det nya
			Remove_From_Dcls_Remain(data);
			bRet=Add_To_Dcls_Remain(data2);
			break;
			//Bara i provtr�dstabell
		case TREE_SAMPLE_EXTRA:
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			//G�r inget utan uppdatera bara provtr�dstabellen med det nya tr�dslaget
			bRet=TRUE;
			break;
		}
	}
	if(bRet)
	{
		m_pDB->updSampleTrees(data2);
	}
	return bRet;
}



// #4380 20150615 J� Radera provtr�d
BOOL CPageThreeFormView::doDeleteTree(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nType=data.getTreeType();
	switch(nType)
	{
	case SAMPLE_TREE:
		Remove_From_Dcls_Uttag(data);
		bRet=TRUE;
		break;
	case TREE_SAMPLE_REMAINIG:
		Remove_From_Dcls_Remain(data);
		bRet=TRUE;
		break;
	case TREE_OUTSIDE:
	case TREE_OUTSIDE_NO_SAMP:
		Remove_From_Dcls_Uttag_NumOfKanttrad(data);
		bRet=TRUE;
		break;
	case TREE_SAMPLE_EXTRA:
	case TREE_OUTSIDE_LEFT:
	case TREE_OUTSIDE_LEFT_NO_SAMP:
	case TREE_INSIDE_LEFT:
	case TREE_INSIDE_LEFT_NO_SAMP:
		bRet=TRUE;					
		break;
	case TREE_POS_NO_SAMP:
		Remove_From_Dcls_Uttag(data);
		bRet=TRUE;
		break;
	}
	if(bRet)
	{
		bRet=m_pDB->delSampleTree(data.getTreeID(),data.getTraktID());
	}
	return bRet;
}

// #4380 20150615 J� L�gg till provtr�d
BOOL CPageThreeFormView::doAddTree(CTransaction_sample_tree data)
{
	BOOL bRet=FALSE;
	int nType=data.getTreeType();
	int nMaxTreeID=-1,nRowCounter=-1;
	switch(nType)
	{
	case SAMPLE_TREE:
		bRet=Add_To_Dcls_Uttag(data);
		break;
	case TREE_SAMPLE_REMAINIG:
		bRet=Add_To_Dcls_Remain(data);
		break;
	case TREE_OUTSIDE:
	case TREE_OUTSIDE_NO_SAMP:
		bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
		break;
	case TREE_SAMPLE_EXTRA:
	case TREE_OUTSIDE_LEFT:
	case TREE_OUTSIDE_LEFT_NO_SAMP:
	case TREE_INSIDE_LEFT:
	case TREE_INSIDE_LEFT_NO_SAMP:
		bRet=TRUE;					
		break;
	case TREE_POS_NO_SAMP:
		bRet=Add_To_Dcls_Uttag(data);
		break;
	}

	if(bRet)
	{

			nMaxTreeID = m_pDB->getMaxSampleTreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;


		// Setup the CTransaction_trees from 
		CTransaction_sample_tree data2 = CTransaction_sample_tree(nRowCounter
			,data.getTraktID()
			,data.getPlotID()
			,data.getSpcID()
			,data.getSpcName()
			,data.getDBH()
			,data.getHgt()
			,data.getGCrownPerc()
			,data.getBarkThick()
			,data.getGROT()
			,data.getAge()
			,data.getGrowth()
			,data.getDCLS_from()
			,data.getDCLS_to()
			,data.getM3Sk()
			,data.getM3Fub()
			,data.getM3Ub()
			,data.getTreeType() 
			,data.getBonitet()
			,data.getCreated()
			,data.getCoord()
			,data.getDistCable()
			,data.getCategory()
			,data.getTopCut());
		bRet=m_pDB->addSampleTrees(data2);
	}
	return bRet;
}

// #4380 20150610 J� Byta diameter, CTransaction_sample_tree data=gamla datat
BOOL CPageThreeFormView::doDbhHasChanged(CTransaction_sample_tree data,double dFromDia,double dToDia)
{
	BOOL bRet=FALSE;
	int nDcls_from_old = (int(dFromDia) / 10) - ((int(dFromDia) / 10) % int(m_recTraktMiscData.getDiamClass()));
	int nDcls_from_new = (int(dToDia) / 10) - ((int(dToDia) / 10) % int(m_recTraktMiscData.getDiamClass()));
	int nDcls_to_new = nDcls_from_new + m_recTraktMiscData.getDiamClass();

	int nType=data.getTreeType();
	int nMaxTreeID=-1,nRowCounter=-1;
	if(nDcls_from_new!=nDcls_from_old)//Ej samma diameterklass
	{	
		//Ta bort befintligt tr�d
		switch(nType)
		{
		case SAMPLE_TREE:
			Remove_From_Dcls_Uttag(data);
			bRet=TRUE;
			break;
		case TREE_SAMPLE_REMAINIG:
			Remove_From_Dcls_Remain(data);
			bRet=TRUE;
			break;
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_NO_SAMP:
			Remove_From_Dcls_Uttag_NumOfKanttrad(data);
			bRet=TRUE;
			break;
		case TREE_SAMPLE_EXTRA:
		case TREE_OUTSIDE_LEFT:
		case TREE_OUTSIDE_LEFT_NO_SAMP:
		case TREE_INSIDE_LEFT:
		case TREE_INSIDE_LEFT_NO_SAMP:
			bRet=TRUE;					
			break;
		case TREE_POS_NO_SAMP:
			Remove_From_Dcls_Uttag(data);
			bRet=TRUE;
			break;
		}	
		if(bRet)
		{
			// G�r det nya CTransaction_sample_tree efter att det �r borttaget ur databas annar slbir not nRowCounter fel
			nMaxTreeID = m_pDB->getMaxSampleTreeID(data.getTraktID());
			// Added 2008-04-07 p�d
			// If there's no items in list nMaxTreeID = -1
			// Then nRowCounter = 0 (ERROR)
			// With change nRowCounter = 1 for first entry; 080407 p�d
			if (nMaxTreeID == -1) nMaxTreeID = 0;

			nRowCounter = nMaxTreeID + 1;
			CTransaction_sample_tree data2 = CTransaction_sample_tree(nRowCounter
				,data.getTraktID()
				,data.getPlotID()
				,data.getSpcID()
				,data.getSpcName()
				,dToDia
				,data.getHgt()
				,data.getGCrownPerc()
				,data.getBarkThick()
				,data.getGROT()
				,data.getAge()
				,data.getGrowth()
				,nDcls_from_new
				,nDcls_to_new
				,data.getM3Sk()
				,data.getM3Fub()
				,data.getM3Ub()
				,data.getTreeType() 
				,data.getBonitet()
				,data.getCreated()
				,data.getCoord()
				,data.getDistCable()
				,data.getCategory()
				,data.getTopCut());
			//L�gg till det nya tr�det i respektive tabell
			switch(nType)
			{
			case SAMPLE_TREE:
				bRet=Add_To_Dcls_Uttag(data2);
				break;
			case TREE_SAMPLE_REMAINIG:
				bRet=Add_To_Dcls_Remain(data2);
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data2);				
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				bRet=TRUE;					
				break;
			case TREE_POS_NO_SAMP:
				bRet=Add_To_Dcls_Uttag(data2);
				break;
			}	
		}
	}
	else	//Samma diameterklass #4599 20151027 J� Uppdatera bara sj�lva kanttr�det
	{
		bRet=TRUE;	
	}
	//Uppdatera sj�lva kanttr�det i kanttr�dstabellen med ny diameter och diameterklass
	if(bRet)
	{
		// Setup the CTransaction_trees from 
		CTransaction_sample_tree data3 = CTransaction_sample_tree(data.getTreeID()
			,data.getTraktID()
			,data.getPlotID()
			,data.getSpcID()
			,data.getSpcName()
			,dToDia
			,data.getHgt()
			,data.getGCrownPerc()
			,data.getBarkThick()
			,data.getGROT()
			,data.getAge()
			,data.getGrowth()
			,nDcls_from_new
			,nDcls_to_new
			,data.getM3Sk()
			,data.getM3Fub()
			,data.getM3Ub()
			,data.getTreeType() 
			,data.getBonitet()
			,data.getCreated()
			,data.getCoord()
			,data.getDistCable()
			,data.getCategory()
			,data.getTopCut());
		bRet=m_pDB->updSampleTrees(data3);
	}
	return bRet;
}

// #4380 20150610 J� Byta tr�dtyp
BOOL CPageThreeFormView::doTreeTypeHasChanged(CTransaction_sample_tree data,int nFromType,int nToType)
{
	BOOL bRet=FALSE;

	if(nFromType!=-1 && nToType!=-1)
	{

		switch(nFromType)
		{
		case SAMPLE_TREE:				//	0		// A sampletree. I.e. it has a heigth etc
			switch(nToType)
			{
			case SAMPLE_TREE:
				bRet=TRUE;
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				Remove_From_Dcls_Uttag(data);
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				Remove_From_Dcls_Uttag(data);
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				Remove_From_Dcls_Uttag(data);					
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				bRet=Check_If_Pos(data);
				break;
			}
			break;
		case TREE_SAMPLE_REMAINIG:		//	3		// "Provtr�d (Kvarl�mmnat)"
			switch(nToType)
			{
			case SAMPLE_TREE:
				Remove_From_Dcls_Remain(data);
				bRet=Add_To_Dcls_Uttag(data);
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				Remove_From_Dcls_Remain(data);
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				bRet=TRUE;
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				Remove_From_Dcls_Remain(data);
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				bRet=Check_If_Pos(data);
				break;
			}
			break;
		case TREE_OUTSIDE:				//	1		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
		case TREE_OUTSIDE_NO_SAMP:		//	2		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
			switch(nToType)
			{
			case SAMPLE_TREE:							
				Remove_From_Dcls_Uttag_NumOfKanttrad(data);
				bRet=Add_To_Dcls_Uttag(data);
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				bRet=TRUE;
				break;
			case TREE_SAMPLE_REMAINIG:
				Remove_From_Dcls_Uttag_NumOfKanttrad(data);
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				Remove_From_Dcls_Uttag_NumOfKanttrad(data);
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				if(Check_If_Pos(data))
				{
					Remove_From_Dcls_Uttag_NumOfKanttrad(data);
					bRet=Add_To_Dcls_Uttag(data);
				}
				break;
			}
			break;
		case TREE_SAMPLE_EXTRA:			//	4		// "Provtr�d (Extra inl�sta)"
			switch(nToType)
			{
			case SAMPLE_TREE:
				bRet=Add_To_Dcls_Uttag(data);
				break;
			case TREE_OUTSIDE:
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_OUTSIDE_NO_SAMP:
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
				break;
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				if(Check_If_Pos(data))
					bRet=Add_To_Dcls_Uttag(data);
				break;
			}
			break;
		case TREE_OUTSIDE_LEFT:			//	5	// Kanttr�d l�mnas (Provtr�d)
		case TREE_OUTSIDE_LEFT_NO_SAMP:	//	6	// Kanttr�d l�mnas  (Ber�knad h�jd)
			switch(nToType)
			{
			case SAMPLE_TREE:
				bRet=Add_To_Dcls_Uttag(data);
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				bRet=TRUE;
				break;						
			case TREE_POS_NO_SAMP:
				if(Check_If_Pos(data))
					bRet=Add_To_Dcls_Uttag(data);
				break;
			}
			break;
		case TREE_POS_NO_SAMP:			//	7	// Pos.tr�d (uttag)							
			switch(nToType)
			{
			case SAMPLE_TREE:
				bRet=TRUE;
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				Remove_From_Dcls_Uttag(data);
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				Remove_From_Dcls_Uttag(data);
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				Remove_From_Dcls_Uttag(data);
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				bRet=TRUE;
				break;
			}
			break;
		case TREE_INSIDE_LEFT:			//	8	// Tr�d i gatan l�mnas (Provtr�d)
		case TREE_INSIDE_LEFT_NO_SAMP:	//	9	// Tr�d i gatan l�mnas (Ber�knad h�jd)
			switch(nToType)
			{
			case SAMPLE_TREE:
				bRet=Add_To_Dcls_Uttag(data);
				break;
			case TREE_OUTSIDE:
			case TREE_OUTSIDE_NO_SAMP:
				bRet=Add_To_Dcls_Uttag_NumOfKantTrad(data);
				break;
			case TREE_SAMPLE_REMAINIG:
				bRet=Add_To_Dcls_Remain(data);
				break;
			case TREE_SAMPLE_EXTRA:
			case TREE_OUTSIDE_LEFT:
			case TREE_OUTSIDE_LEFT_NO_SAMP:
			case TREE_INSIDE_LEFT:
			case TREE_INSIDE_LEFT_NO_SAMP:
				bRet=TRUE;
				break;
			case TREE_POS_NO_SAMP:
				if(Check_If_Pos(data))
					bRet=Add_To_Dcls_Uttag(data);
				break;
			}
			break;
		}

	}
	//Uppdatera typen f�r provtr�det i provtr�dstabellen
	if(bRet)
	{
		CTransaction_sample_tree data2 = CTransaction_sample_tree(data.getTreeID()
			,data.getTraktID()
			,data.getPlotID()
			,data.getSpcID()
			,data.getSpcName()
			,data.getDBH()
			,data.getHgt()
			,data.getGCrownPerc()
			,data.getBarkThick()
			,data.getGROT()
			,data.getAge()
			,data.getGrowth()
			,data.getDCLS_from()
			,data.getDCLS_to()
			,data.getM3Sk()
			,data.getM3Fub()
			,data.getM3Ub()
			,nToType 
			,data.getBonitet()
			,data.getCreated()
			,data.getCoord()
			,data.getDistCable()
			,data.getCategory()
			,data.getTopCut());
		bRet=m_pDB->updSampleTrees(data2);
	}
	return bRet;
}


// Compare plot_id to id's in m_vecTraktPlot, to see
// if user entered a PlotID that isn't defined; 070515 p�d
BOOL CPageThreeFormView::isPlotGroupID(int plot_id)
{
	if (m_vecTraktPlot.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktPlot.size();i++)
		{
			if (m_vecTraktPlot[i].getPlotID() == plot_id)
				return TRUE;
		}	// for (UINT i = 0;i < m_vecTraktPlot.size();i++)
	}	// if (m_vecTraktPlot.size() > 0)

	// Tell user that the value entered isn't a valid PlotID.
	// Ask if user want's to enter this PlotID also; 070515 p�d
	if (UMMessageBox(this->GetSafeHwnd(),m_sMsgGroupID,m_sMsg,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		OnBnClickedBtnPlotView();
	}

	return FALSE;
}

void CPageThreeFormView::doMatchRandTrees(void)
{
	BOOL bIsRandTrees = FALSE;
	BOOL bIsNoRandTrees = FALSE;
	CString sStatusbarText,S;
	vecRandTreeCounter m_vecRandTreeCounter;
	vecRandTreeCounter m_vecNoRandTreeCounter;
	CTransaction_trakt* pTrakt = getActiveTrakt();

	setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(13));
	setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(12));

	if (m_pDB != NULL)
	{
		setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(14));
		// Save sampletrees to DB; 080307 p�d
		// This methods also includes Data security handling; 090319 p�d
		if (!saveSampleTreesToDB()) return;
		// Save dcls-etrees to DB; 080307 p�d
		// This methods also includes Data security handling; 090319 p�d
		if (!saveDCLSTreesToDB()) return;

		m_pDB->getNumOfRandTreesPerSpcAndDCLS(pTrakt->getTraktID(),m_vecRandTreeCounter);
		m_pDB->getNoNumOfRandTreesPerSpcAndDCLS(pTrakt->getTraktID(),m_recTraktMiscData.getDiamClass(),m_vecNoRandTreeCounter);

		// Check if thre's any RandTrees to be entered into dcls_numof_randtrees; 080310 p�d
		// in "esti_trakt_dcls_trees_table"; 080307 p�d
		if (m_vecRandTreeCounter.size() > 0)
		{
			bIsRandTrees = TRUE;
			// First we reset dcls_numof_randtrees to zero; 080505 p�d
			m_pDB->resetDCLSTrees_tree_type(pTrakt->getTraktID());
			// Second we set dcls_numof_randtrees to number of per dcls and specie; 080505 p�d
			m_pDB->updNumOfRandTreesPerSpcAndDCLS(pTrakt->getTraktID(),m_vecRandTreeCounter);
		}	// if (vecDCLSTrees.size() > 0 && vecRandTreeCounter.size() > 0)
		// If there's no "RandTrees", just set tree_tree_type = 0 in table
		// esti_trakt_dcls_trees_table; 080505 p�d
		else
		{
			bIsNoRandTrees = TRUE;
			m_pDB->resetDCLSTrees_tree_type(pTrakt->getTraktID());
		}
		// Check if thre's any RandTrees to be entered into dcls numof_randtrees; 080310 p�d
		// in "esti_trakt_dcls_trees_table"; 080307 p�d
		if (m_vecNoRandTreeCounter.size() > 0)
		{
			bIsNoRandTrees = TRUE;
			m_pDB->addNoNumOfRandTreesPerSpcAndDCLS(pTrakt->getTraktID(),m_vecNoRandTreeCounter);
		}	// if (m_vecNoRandTreeCounter.size() > 0)

		if (bIsRandTrees ||	bIsNoRandTrees)
		{
			// We also need to recalculate; 080310 p�d
			if (!runDoCalulationInPageThree(-1,FALSE,FALSE))
			{
				setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
				setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
			}
		}	// if (bIsRandTrees ||	bIsNoRandTrees)
		else
		{
			setStatusBarText(0,m_sarrCalcStatusMsg.GetAt(0));
			setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(2));
		}
	}

	m_vecRandTreeCounter.clear();
	m_vecNoRandTreeCounter.clear();

	populateData(0);

}

void CPageThreeFormView::doUpdateRandTrees(void)
{

	int nTraktID = -1;
	CTransaction_trakt* pTrakt = getActiveTrakt();
	if (pTrakt != NULL && m_pDB != NULL)
	{
		nTraktID = pTrakt->getTraktID();
		pTrakt = NULL;

		CUpdateRandTreeData *pDlg = new CUpdateRandTreeData();
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
				// Check if user's goin' to change age of RandTrees; 080624 p�d
				if (pDlg->getDoAge())
				{
					m_pDB->updSampleTrees_age(nTraktID,pDlg->getAgeFrom(),pDlg->getAgeTo());
				}	// if (pDlg->getDoAge())

				// Check if user's goin' to change bonitet of RandTrees; 080624 p�d
				if (pDlg->getDoBonitet())
				{
					m_pDB->updSampleTrees_bonitet(nTraktID,pDlg->getBonitetFrom(),pDlg->getBonitetTo());
				}	// if (pDlg->getDoAge())

			}	// if (pDlg->DoModal() == IDOK)
			delete pDlg;
		}
		populateData(3 /* 3 = SAMPLE TREES */);
	}
}

void CPageThreeFormView::doUpdateRandTrees2(void)
{

	int nTraktID = -1;
	CTransaction_trakt* pTrakt = getActiveTrakt();
	if (pTrakt != NULL && m_pDB != NULL)
	{
		nTraktID = pTrakt->getTraktID();
		pTrakt = NULL;

		saveDCLSTreesToDB();
		saveDCLS1TreesToDB();
		saveDCLS2TreesToDB();
		saveSampleTreesToDB();

		getSampleTreesFromDB();
		CUpdateRandTreeData2 *pDlg = new CUpdateRandTreeData2(m_vecTraktSampleTrees, m_sarrTreeTypes, m_vecSmpTreeCategories, m_vecSpecies);
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
			}	// if (pDlg->DoModal() == IDOK)
			delete pDlg;
		}
		populateData(0);	// Uttag
		populateData(1);	// Kvarl�mnat
		populateData(3 /* 3 = SAMPLE TREES */);
	}
}

BOOL CPageThreeFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CSpecieDataFormView*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, id, &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	int nTab = m_wndTabControl.getNumOfTabPages();
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

//	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

