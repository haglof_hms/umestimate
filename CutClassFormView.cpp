// CategoryFormView.cpp : implementation file
//

#include "stdafx.h"
#include "CutClassFormView.h"

#include "ResLangFileReader.h"

// CCutClassFormView

IMPLEMENT_DYNCREATE(CCutClassFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCutClassFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CCutClassFormView::CCutClassFormView()
	: CXTResizeFormView(CCutClassFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CCutClassFormView::~CCutClassFormView()
{
}

void CCutClassFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecCutClassData.clear();
	m_wndReport1.ClearReport();

	CXTResizeFormView::OnDestroy();
}
	
void CCutClassFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CCutClassFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CCutClassFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CCutClassFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
		//m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL CCutClassFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CCutClassFormView diagnostics

#ifdef _DEBUG
void CCutClassFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CCutClassFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

void CCutClassFormView::doSetNavigationBar()
{
	if (m_vecCutClassData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CCutClassFormView message handlers

BOOL CCutClassFormView::getCutClass(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCutClass(m_vecCutClassData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CCutClassFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CCutClassFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_TRAKT_TYPE_REPORT, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				m_sMsgCap	= xml->str(IDS_STRING151);
				m_sCutClassID	= xml->str(IDS_STRING182);
				m_sCutClass	= xml->str(IDS_STRING183);
				m_sCutClassNotes	= xml->str(IDS_STRING184);
				m_sOKBtn	= xml->str(IDS_STRING155);
				m_sCancelBtn	= xml->str(IDS_STRING156);
				m_sText1	= xml->str(IDS_STRING1574);
				m_sText2	= xml->str(IDS_STRING1584);
				m_sText3	= xml->str(IDS_STRING1594);
				m_sDoneSavingMsg = xml->str(IDS_STRING186);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (m_sCutClassID), 30));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->SetVisible( FALSE );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (m_sCutClass), 60));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, (m_sCutClassNotes), 120));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getCutClass();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 1,rect.bottom - 1);

				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CCutClassFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CCutClassFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addCutClass();
			if (saveCutClass())
			{
				getCutClass();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveCutClass())
			{
				getCutClass();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getCutClass();
			if (removeCutClass())
			{
				getCutClass();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CCutClassFormView::populateReport(void)
{
	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecCutClassData.size() > 0)
	{
		for (UINT i = 0;i < m_vecCutClassData.size();i++)
		{
			CTransaction_cut_class rec = m_vecCutClassData[i];
			m_wndReport1.AddRecord(new CCutClassReportRec(rec.getID(),rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL CCutClassFormView::addCutClass(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CCutClassReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

BOOL CCutClassFormView::saveCutClass(void)
{
	CTransaction_cut_class rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{

				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CCutClassReportRec *pRec = (CCutClassReportRec *)pRecs->GetAt(i);

					rec = CTransaction_cut_class(pRec->getColumnInt(0),
 											 								 pRec->getColumnText(1),
																			 pRec->getColumnText(2),
																			 _T(""));

					if (!m_pDB->addCutClass(rec))
						m_pDB->updCutClass(rec);
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			//populateReport();
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CCutClassFormView::removeCutClass(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_cut_class data;
	CCutClassReportRec *pRec = NULL;
	CString sMsg;
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CCutClassReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				data = CTransaction_cut_class(pRec->getID(),pRec->getColumnText(1),pRec->getColumnText(2),_T(""));
				if (m_pDB != NULL)
				{
					// Check if used in database; 061002 p�d	
					if (!isCutClassUsed(data))
					{
						// Setup a message for user upon deleting machine; 061010 p�d
						sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n\n%s\n\n%s"),
												m_sText1,
												//m_sCutClassID,
												//data.getID(),
												m_sCutClass,
												data.getCutClass(),
												m_sCutClassNotes,
												data.getNotes(),
												m_sText2,
												m_sText3);

						if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						{
							// Delete Machine and ALL underlying 
							//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
							m_pDB->removeCutClass(data);
							bReturn = TRUE;
						}	// if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
					}	// if (!isCutClassUsed(data))
				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
	}

	return bReturn;
}

void CCutClassFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}

BOOL CCutClassFormView::isCutClassUsed(CTransaction_cut_class &)
{
	return FALSE;
}
