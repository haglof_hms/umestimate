// AssortDistributionFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "PageFourFormView.h"
#include "AssortDistributionFormView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

#include <algorithm>


// CAssortDistributionFormView

int CAssortDistributionFormView::m_nCurCBSel = -1;

IMPLEMENT_DYNCREATE(CAssortDistributionFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CAssortDistributionFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELCHANGE(IDC_COMBO15_1, OnCBoxSpeciesChanged)
	ON_CBN_SELCHANGE(IDC_COMBO15_2, OnCBoxDistributionChanged)
	ON_BN_CLICKED(IDC_CHECKBOX15_1, OnBnClickedCheckBox1)
END_MESSAGE_MAP()

CAssortDistributionFormView::CAssortDistributionFormView()
	: CXTResizeFormView(CAssortDistributionFormView::IDD)
{
	m_bInitialized = FALSE;
	m_nTraktID = -1;
	m_nShowSpecies = -1;
	m_nShowDistribution = 0;
	m_nMax = 0;
	m_bShowVolumeLabels = TRUE;
	m_fMaxDCLS = 0.0;
}

CAssortDistributionFormView::~CAssortDistributionFormView()
{
}

void CAssortDistributionFormView::OnDestroy()
{
	m_fntCaption.DeleteObject();
	m_fntText.DeleteObject();
	m_fntTextBold.DeleteObject();
	m_fntTextSmall.DeleteObject();

	if (m_vecDCLSDblArr.size() > 0)
		for (UINT i = 0;i < m_vecDCLSDblArr.size();i++)
			m_vecDCLSDblArr[i].free();

	// Clean up
	m_sarr.free();
	m_sarrY.free();

	CXTResizeFormView::OnDestroy();	
}

void CAssortDistributionFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CAssortDistributionFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

BOOL CAssortDistributionFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CAssortDistributionFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHelloworldDlg)
	DDX_Control(pDX, IDC_CHART15_1, m_chartViewer);
	DDX_Control(pDX, IDC_LBL15_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL15_2, m_wndLbl2);
	DDX_Control(pDX, IDC_COMBO15_1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO15_2, m_wndCBox2);
	DDX_Control(pDX, IDC_CHECKBOX15_1, m_wndCheckBox1);
	//}}AFX_DATA_MAP

}

void CAssortDistributionFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sDiagramCaption = xml.str(IDS_STRING2554);
				m_sDiagramYLabel = xml.str(IDS_STRING2551);
				m_sDiagramXLabel = xml.str(IDS_STRING2543);			
				m_sStandNumber = xml.str(IDS_STRING1000);
				m_sStandName = xml.str(IDS_STRING101);
				m_sPropertyNameLabel = xml.str(IDS_STRING104);
				m_sShowAllCB = xml.str(IDS_STRING2545);
				m_sSpecies = xml.str(IDS_STRING133);
				m_sExcludeTranfers = xml.str(IDS_STRING2559);

				m_wndLbl1.SetWindowText(xml.str(IDS_STRING2544));
				m_wndLbl2.SetWindowText(xml.str(IDS_STRING2555));
				m_wndCBox2.ResetContent();
				m_wndCBox2.AddString(xml.str(IDS_STRING2556));
				m_wndCBox2.AddString(xml.str(IDS_STRING2557));
				m_wndCheckBox1.SetWindowText(xml.str(IDS_STRING2558));
			}
			xml.clean();

			m_fntCaption.CreateFont(190, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));

			m_fntText.CreateFont(90, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextBold.CreateFont(90, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextSmall.CreateFont(70, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
		
	
		}

		m_wndCheckBox1.SetCheck(TRUE);
		m_wndCBox2.SetCurSel(m_nShowDistribution);

		m_sarrY.set(101);
		char szTmp[127];
		for (int i3 = 0;i3 <= 100;i3++)
		{
			if (((i3 % 10) == 0))
				sprintf(szTmp,"%d%s",i3,"%");
			else
				sprintf(szTmp,"");
			
			m_sarrY.add(i3,szTmp);
		}

		m_bInitialized = TRUE;
	}

}

void CAssortDistributionFormView::OnSetFocus(CWnd*)
{
}

void CAssortDistributionFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CAssortDistributionFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_chartViewer)
	{
		m_chartViewer.MoveWindow(10,40,cx-20,cy);
		drawChartView();
	}
}

// CAssortDistributionFormView diagnostics

void CAssortDistributionFormView::getTraktDCLSFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getDCLSTrees(trakt_id,m_vecDCLSTrees);
	}	// if (pDB != NULL)
}

void CAssortDistributionFormView::getTraktDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

void CAssortDistributionFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

void CAssortDistributionFormView::getTraktSetSpcFromDB(int trakt_id)
{
	if (m_pDB)
	{
		m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,trakt_id);
	}	// if (m_pDB != NULL)
}

void CAssortDistributionFormView::getTraktPlotsFromDB(int trakt_id)
{
	if (m_pDB)
	{
		m_pDB->getPlots(trakt_id,m_vecTraktPlot);
	}	// if (m_pDB != NULL)
}

#ifdef _DEBUG
void CAssortDistributionFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAssortDistributionFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


bool MyDataSort_ass(_ass& d1,_ass& d2)
{
  return (d1.m_fMinDiam > d2.m_fMinDiam);
}

bool MyDataSort(CTransaction_exchange_simple& d1,CTransaction_exchange_simple& d2)
{
//  return (d1.getDCLSFrom() < d2.getDCLSFrom()) && (d1.getExchVolume() < d2.getExchVolume());
  return (d1.getExchVolume() < d2.getExchVolume());
}

bool MyDataSort2(CTransaction_exchange_simple& d1,CTransaction_exchange_simple& d2)
{
  return d1.getMinDiam() < d2.getMinDiam();
}

// CAssortDistributionFormView message handlers


void CAssortDistributionFormView::drawChartView(bool redraw,drawType dt,CDC* pDC)
{
	CString S;
	double fDCLSCounter;
	double fDCLSClass;
	double fDeltaVolume = 0.0;
	CRect rect;
	GetClientRect(&rect);
	vecDbl vDbl;
	CTransaction_exchange_simple rec1;
	double fArea = 0.0;
	double fPerHa = 0.0;
	double fArealCalculationFactor = 0.0;

	double fMax = 0.0;

	vecTransaction_exchange_simple vecExchSimple;

	CTransaction_tree_assort recTreeAssort;

	
	// Create a XYChart
	XYChart *xyChart = new XYChart(rect.right-20, rect.bottom-50);

	if (m_nTraktID != getActiveTrakt()->getTraktID() || redraw)
	{
		if (m_vecDCLSDblArr.size() > 0)
		{
			for (UINT i = 0;i < m_vecDCLSDblArr.size();i++)
			{
				m_vecDCLSDblArr[i].free();
			}
		}
		m_vecDCLSDblArr.clear();
		// Clean up
		m_sarr.free();
		getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
		getTraktDataFromDB(getActiveTrakt()->getTraktID());
		getTraktDCLSFromDB(getActiveTrakt()->getTraktID());
		getTraktSetSpcFromDB(getActiveTrakt()->getTraktID());
		
		if (m_nShowDistribution == 0)
			getTraktPlotsFromDB(getActiveTrakt()->getTraktID());

		if (m_vecDCLSTrees.size() > 0 && m_vecTransactionTraktSetSpc.size() > 0)
		{
			doUMExchangeSimple(m_recTraktMiscData,
												 m_vecDCLSTrees,
												 m_vecTransactionTraktSetSpc,
												 m_vecTreeAssort,
												 vecExchSimple);
		}

		if (m_vecTraktPlot.size() > 0 && m_nShowDistribution == 0)
		{
			// If we have plot(s), check if we have data in plot radius.
			// If so, we have a "Cirkelyta/ytor"
			for (UINT i = 0;i < m_vecTraktPlot.size();i++)
			{
				CTransaction_plot plotData = m_vecTraktPlot[i];
				if (plotData.getPlotType() == 2 ||	// "Cirkelytor"
						plotData.getPlotType() == 3 ||	// "Rektangul�ra ytor"
						plotData.getPlotType() == 4)		// "Snabbtaxering"
				{
					fArea += plotData.getArea();
				}		

			}	// for (UINT i = 0;i < m_vecTraktPlot.size();i++)
		}	// if (vecTraktPlot.size() > 0)
		if (fArea > 0.0)
		{
			fPerHa = (10000.0/fArea);
		}	// if (fArea > 0.0)
		else
		{
			//if (getActiveTrakt()->getTraktAreal() > 0.0)	// Add check for division by zero; 080117 p�d
			fPerHa = 1.0;
		}
		if (m_vecTraktData.size() > 0)
		{
			m_wndCBox1.ResetContent();

			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					m_wndCBox1.AddString(recTraktData.getSpecieName());
					if (m_nShowSpecies == -1)
					{
						m_nShowSpecies = recTraktData.getSpecieID();
					}
				}	
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			if (m_nCurCBSel == -1)
				m_wndCBox1.SetCurSel(0);
			else
				m_wndCBox1.SetCurSel(m_nCurCBSel);
		}	// if (m_vecTraktData.size() > 0)

		//---------------------------------------------------------------------------------
		// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = fDCLSClass;
		if (fDCLSClass > 0.0)
			m_fMaxDCLS = m_pDB->getMaxDCLS(getActiveTrakt()->getTraktID(),m_nShowSpecies)/fDCLSClass + 1;
		else
			m_fMaxDCLS = 0.0;
		//m_fMaxDCLS = 50; Fast diameterklassindelning

		m_nTraktID = getActiveTrakt()->getTraktID();

		m_vecTreeAssortSpc.clear();
		// Setup vecTreesAssortSpc; 100412 p�d
		if (m_vecTreeAssort.size() > 0)
		{
			for (UINT i = 0;i < m_vecTreeAssort.size();i++)
			{
				recTreeAssort = m_vecTreeAssort[i];
				if (recTreeAssort.getSpcID() == m_nShowSpecies)
				{
					m_vecTreeAssortSpc.push_back(m_vecTreeAssort[i]);
				}
			}
		}

		//std::sort(vecExchSimple.begin(),vecExchSimple.end(),MyDataSort);
		fDeltaVolume = 0.0;
		double fVolume = 0.0;
		double fMaxVolume = 0.0;
		m_vecAss.clear();
		for (UINT i33 = 0;i33 < m_vecTreeAssortSpc.size();i33++)
		{
			fDCLSClass = m_recTraktMiscData.getDiamClass();
			fDCLSCounter = 0.0;
			recTreeAssort = m_vecTreeAssortSpc[i33];
			for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++)
			{
				for (UINT i2 = 0;i2 < vecExchSimple.size();i2++)
				{
					rec1 = vecExchSimple[i2];
					if (fDCLSCounter == rec1.getDCLSFrom()&& 
							fDCLSCounter+fDCLSClass == rec1.getDCLSTo() && 
							rec1.getSpcID() == m_nShowSpecies && 
							recTreeAssort.getSpcID() == rec1.getSpcID() &&
							recTreeAssort.getDCLS() == rec1.getMinDiam() &&
							rec1.getExchVolume() > 0.0)
					{
						fVolume += rec1.getExchVolume();
					}
				}
				if (recTreeAssort.getSpcID() == m_nShowSpecies)
					m_vecAss.push_back(_ass(m_nShowSpecies,recTreeAssort.getDCLS(),fDCLSCounter,fDCLSCounter+fDCLSClass,fVolume));
				fVolume = 0.0;
				fDCLSCounter += fDCLSClass;
			}
		}

		std::sort(m_vecAss.begin(),m_vecAss.end(),MyDataSort_ass);

		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = 0.0;
		_ass recAss;
		for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++)
		{
			for (UINT i2 = 0;i2 < m_vecAss.size();i2++)
			{
				recAss = m_vecAss[i2];
				if (m_nShowDistribution == 0)		// Percent
				{
						if (fDCLSCounter == recAss.m_fDclsFrom && 
								fDCLSCounter+fDCLSClass == recAss.m_fDclsTo && 
								recAss.m_nSpcID == m_nShowSpecies)
						{
							fMaxVolume = max(fMaxVolume,recAss.m_fSumExchVolume);
						}
				}	// if (m_nShowDistribution == 0)
			}

			for (UINT i2 = 0;i2 < m_vecAss.size();i2++)
			{

				recAss = m_vecAss[i2];
				if (fDCLSCounter == recAss.m_fDclsFrom && 
						fDCLSCounter+fDCLSClass == recAss.m_fDclsTo && 
						recAss.m_nSpcID == m_nShowSpecies)
				{
					fVolume = (recAss.m_fSumExchVolume - fDeltaVolume);
					
					if (fVolume < 0.0) 
					{
						fVolume *= (-1.0);
					}
					if (m_nShowDistribution == 0)		// Percvent
					{
						if (fVolume > 0.0)
							m_vecAss[i2].m_fDeltaExchVolume = (fVolume/fMaxVolume)*100.0;
						else
							m_vecAss[i2].m_fDeltaExchVolume = 0.0;
					}
					else if (m_nShowDistribution == 1)	// Volume
						m_vecAss[i2].m_fDeltaExchVolume = (fVolume*fPerHa);

					fDeltaVolume = recAss.m_fSumExchVolume;
				}
			}

			fDCLSCounter += fDCLSClass;
			fDeltaVolume = 0.0;
			fMaxVolume = 0.0;
		}	// for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++)

		BOOL bNewDCLSVector = TRUE;
		//std::sort(vecExchSimple.begin(),vecExchSimple.end(),MyDataSort2);
		for (UINT iii = 0;iii < m_vecTreeAssort.size();iii++)
		{
			recTreeAssort = m_vecTreeAssort[iii];
			// Setup a new Diameterclass vector; 100413 p�d
			if (bNewDCLSVector && recTreeAssort.getSpcID() == m_nShowSpecies)
			{
				fDCLSClass = m_recTraktMiscData.getDiamClass();
				fDCLSCounter = 0.0;
				vDbl.clear();
				for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++) vDbl.push_back(0.0);	// Empty 
				bNewDCLSVector = FALSE;
			}
			for (UINT i2 = 0;i2 < m_vecAss.size();i2++)
			{
				recAss = m_vecAss[i2];
				if (recAss.m_nSpcID == m_nShowSpecies && 
						recTreeAssort.getDCLS() == recAss.m_fMinDiam && 
						recTreeAssort.getSpcID() == m_nShowSpecies)
				{
					// Findout were to place the data in the vector; 100413 p�d
					fDCLSClass = m_recTraktMiscData.getDiamClass();
					fDCLSCounter = 0.0;
					for (int iDCLS = 0;iDCLS < (int)m_fMaxDCLS;iDCLS++)
					{
						if (recAss.m_fDclsFrom == fDCLSCounter && recAss.m_fDclsTo == fDCLSCounter+fDCLSClass)
						{
							vDbl[iDCLS] = recAss.m_fDeltaExchVolume;
							break;
						}
						
						fDCLSCounter += fDCLSClass;
					}
				}
			}
			bNewDCLSVector = TRUE;
			if (recTreeAssort.getSpcID() == m_nShowSpecies)
				m_vecDCLSDblArr.push_back(CBarArray(m_nShowSpecies,recTreeAssort.getAssortName(),recTreeAssort.getDCLS(),vDbl));
		}
		//---------------------------------------------------------------------
		// Setup diameterclass

		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = 0.0;
		m_sarr.set((int)m_fMaxDCLS);
		char szTmp[255];
		for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++)
		{
			if (m_fMaxDCLS*fDCLSClass > 80.0)
			{
				if (((i3 % 2) == 0))
					sprintf(szTmp,"%.0f",fDCLSCounter);
				else
					sprintf(szTmp," ");
			}
			else
				sprintf(szTmp,"%.0f",fDCLSCounter);
			
			m_sarr.add(i3,szTmp);
			
			fDCLSCounter += fDCLSClass;
		}
	}	// if (m_nTraktID != getActiveTrakt()->getTraktID() || redraw)

	if (dt == DRAW_VIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(60, 50, rect.right-250, rect.bottom-140, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);

		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-150, 50); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);
	}
	else if (dt == DRAW_PREVIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 25, rect.right-380, rect.bottom-140, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);
		
		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-310, 25); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);
	}

	// Add a title to the x axis
  xyChart->xAxis()->setTitle(convStr(m_sDiagramXLabel));

  // Set the x axis labels
	xyChart->xAxis()->setLabels(StringArray(m_sarr.get(), m_sarr.numof()));
	
	BarLayer *layer = NULL;
	if (m_nShowDistribution == 0)
	{
		layer = xyChart->addBarLayer(Chart::Stack);
	}
	else if (m_nShowDistribution == 1)
	{
		layer = xyChart->addBarLayer(Chart::Stack,4);
	}
  // Set bar shape to circular (cylinder)
  layer->setBarShape(Chart::CircleShapeNoShading);
	
	// For a vertical stacked chart with positive data only, the last data set is
	// always on top. However, in a vertical legend box, the last data set is at the
	// bottom. This can be reversed by using the setLegend method.
	layer->setLegend(Chart::ReverseLegend);

	if (m_vecDCLSDblArr.size() > 0)
	{
		for (UINT i7 = 0;i7 < m_vecDCLSDblArr.size();i7++)
		{
			layer->addDataSet(DoubleArray(m_vecDCLSDblArr[i7].get(), m_vecDCLSDblArr[i7].numof()),-1,convStr(m_vecDCLSDblArr[i7].getSpcName()));
		}
	}

	if (m_nShowDistribution == 0)
	{
		// Set the y axis labels
		xyChart->yAxis()->setLabels(StringArray(m_sarrY.get(), m_sarrY.numof()));
		// 	
		xyChart->addText(20,10,convStr(m_sExcludeTranfers),"Verdana",8);
	}
	else if (m_nShowDistribution == 1)
	{
		// Add a title to the y-axis
		xyChart->yAxis()->setTitle(convStr(m_sDiagramYLabel));
	}

	if (m_bShowVolumeLabels)
	{
		if (m_nShowDistribution == 0)
		{
			// Enable bar label for each segment of the stacked bar; 100408 p�d
			layer->setDataLabelFormat("{value|0}%"); //"arialbd.ttf",8); //,0xffffff);
			// Enable bar label for each segment of the stacked bar; 100408 p�d
			layer->setDataLabelStyle()->setAlignment(Chart::Center); 
		}
		else if (m_nShowDistribution == 1)
		{
			// Set 2 decimals; 100408 p�d
			layer->setAggregateLabelFormat("{value|2}");
			// Enable bar label for each segment of the stacked bar; 100408 p�d
			layer->setAggregateLabelStyle("arialbd.ttf",8); //,0xffffff);
			// Enable bar label for each segment of the stacked bar; 100408 p�d
			layer->setDataLabelFormat("{value|2}"); //"arialbd.ttf",8); //,0xffffff);
			// Enable bar label for each segment of the stacked bar; 100408 p�d
			layer->setDataLabelStyle(); //"arialbd.ttf",8); //,0xffffff);
		}
	}

	if (dt == DRAW_VIEW)
	{
	  // Output the chart
		m_chartViewer.setChart(xyChart);
	}
	else if (dt == DRAW_PREVIEW)
	{
		// The scaling factor to adjust for printer resolution
		// (pDC = CDC pointer representing the printer device context)
		double xScaleFactor = pDC->GetDeviceCaps(LOGPIXELSX) / 96.0;
		double yScaleFactor = pDC->GetDeviceCaps(LOGPIXELSY) / 96.0;

		// Output the chart as BMP
		MemBlock bmp = xyChart->makeChart(Chart::BMP);

		// The BITMAPINFOHEADER is 14 bytes offset from the beginning
		LPBITMAPINFO header = (LPBITMAPINFO)(bmp.data + 14);

		// The bitmap data
		LPBYTE bitData = (LPBYTE)(bmp.data) + ((LPBITMAPFILEHEADER)(bmp.data))->bfOffBits;

		int nLeft = 20*xScaleFactor;
		int nTop = 20*yScaleFactor;
		int nWidth = 5220; //header->bmiHeader.biWidth*xScaleFactor*0.6;
		int nHeight = 60*yScaleFactor;

		TEXTMETRIC tm;
		CBrush brush;
		brush.CreateSolidBrush(RGB(0,0,255)); 
		//---------------------------------------------------------------------------
		// Setup Caption
		pDC->FillRect(CRect(nLeft,nTop,nWidth*0.9,nHeight),&brush);
		pDC->SelectObject((CFont*)&m_fntCaption);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->DrawText(m_sDiagramCaption,CRect(nLeft,nTop,nWidth*0.9,nHeight),DT_SINGLELINE|DT_CENTER|DT_VCENTER);

		//---------------------------------------------------------------------------
		// Setup date
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextSmall);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nLeft,nHeight,getDateTime());

		//---------------------------------------------------------------------------
		// Setup Name of Stand
		int nTextX = 0;
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextBold);
		pDC->GetTextMetrics(&tm);
		nTextX = max(nTextX,m_sStandNumber.GetLength());
		nTextX = max(nTextX,m_sStandName.GetLength());
		nTextX = max(nTextX,m_sPropertyNameLabel.GetLength());
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*2,m_sStandNumber+L":");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*3,m_sStandName+L":");
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft,nHeight+tm.tmHeight*4,m_sPropertyNameLabel+L":");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*5,m_sSpecies+L":");

		pDC->SelectObject((CFont*)&m_fntText);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*2,getActiveTrakt()->getTraktNum());
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*3,getActiveTrakt()->getTraktName());
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*4,m_sPropertyName);
		CString sSpecies;
		m_wndCBox1.GetWindowText(sSpecies);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*5,sSpecies);

		//---------------------------------------------------------------------------
		// Output to device context

		StretchDIBits(pDC->m_hDC,
									(int)(10 * xScaleFactor),
									(int)(300 * yScaleFactor),
									(int)(1392 * xScaleFactor * 0.6),
									(int)(583 * yScaleFactor * 0.6),
									0, 0, header->bmiHeader.biWidth, header->bmiHeader.biHeight,
									bitData, header, DIB_RGB_COLORS, SRCCOPY);

}

  //free up resources
	delete xyChart;

}


void CAssortDistributionFormView::OnPrint(CDC* pDC, CPrintInfo *pInfo)
{
	drawChartView(true,DRAW_PREVIEW,pDC);
}

BOOL CAssortDistributionFormView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return CXTResizeFormView::DoPreparePrinting(pInfo);
}

void CAssortDistributionFormView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this, RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}
}

void CAssortDistributionFormView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	drawChartView(true);
	CXTResizeFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CAssortDistributionFormView::doRunPrint()	
{ 
	this->OnFilePrint();
}

void CAssortDistributionFormView::OnCBoxSpeciesChanged(void)
{
	CString sSpcName;
	m_nCurCBSel = m_wndCBox1.GetCurSel();
	m_wndCBox1.GetWindowText(sSpcName);
	m_nShowSpecies = -1;
	if (m_nCurCBSel != CB_ERR)
	{
		if (m_nCurCBSel > 0)
		{
			if (m_vecTraktData.size() > 0 && m_nCurCBSel-1 < m_vecTraktData.size())
			{		
				for (UINT i = 0;i < m_vecTraktData.size();i++)
				{
					if (m_vecTraktData[i].getSpecieName().Compare(sSpcName) == 0)
					{
						m_nShowSpecies = m_vecTraktData[i].getSpecieID();
						break;
					}
				}
			}	
		}
	}
	drawChartView(true);
}

void CAssortDistributionFormView::OnBnClickedCheckBox1()
{
	m_bShowVolumeLabels = m_wndCheckBox1.GetCheck();
	drawChartView(true);
}

void CAssortDistributionFormView::OnCBoxDistributionChanged(void)
{
	m_nShowDistribution = m_wndCBox2.GetCurSel();
	drawChartView(true);
}
