// SearchPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "SearchPropDlg.h"


// CSearchPropDlg dialog

IMPLEMENT_DYNAMIC(CSearchPropDlg, CDialog)

CSearchPropDlg::CSearchPropDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchPropDlg::IDD, pParent)
{

}

CSearchPropDlg::~CSearchPropDlg()
{
}

void CSearchPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSearchPropDlg, CDialog)
END_MESSAGE_MAP()


// CSearchPropDlg message handlers
