#pragma once

#include "Resource.h"

// CPlotSetupDialog dialog

class CPlotSetupDialog : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CPlotSetupDialog)

	vecTransactionPlot m_vecTraktPlot;
	void getPlots(void);
	void savePlots(void);

	BOOL setupReport(void);
	void populateReport(void);

	int m_nInvMethodIndex;

	CXTResizeGroupBox m_wndGroup1;
	CMyReportCtrl m_wndReport;

	CButton m_wndBtnAdd;
	CButton m_wndBtnRemove;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	BOOL m_bIsDirty;

	CUMEstimateDB *m_pDB;

	CString m_sMsgCap;
	CString m_sMsgRemove;

	CString m_sPlot;

	BOOL m_bPlotDeleted;
	int m_nSelectedTraktID_delete;
	int m_nSelectedPlotID_delete;
public:
	CPlotSetupDialog(CWnd* pParent = NULL);   // standard constructor
	CPlotSetupDialog(CUMEstimateDB *db);
	virtual ~CPlotSetupDialog();

	virtual INT_PTR DoModal();

	// Need to be Public
	void delPlot(void);

	void addGroup(CTransaction_plot &rec);

	inline BOOL isPlotDeleted(void) { return 	m_bPlotDeleted;	}

// Dialog Data
	enum { IDD = IDD_DIALOG7 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPlotSetupDialog)
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedRemove();
	afx_msg void OnBnClickedOk();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
