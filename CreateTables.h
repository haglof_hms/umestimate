#if !defined(AFX_CREATETABLES_H)
#define AFX_CREATETABLES_H

#include "StdAfx.h"

// CreateTables in Database; 080627 p�d

// "Ers�tter sql-script"
//***************************************************************************************************
LPCTSTR table_EstiTrakt = _T("CREATE TABLE dbo.%s (\
														 trakt_id int NOT NULL IDENTITY(1,1),\
														trakt_num nvarchar(20),			/* Traktnummer */\
														trakt_pnum nvarchar(10),			/* Delnummer */\
														trakt_name nvarchar(31),			/* Namn p� trakten */\
														trakt_type nvarchar(15),			/* Trakttyp */\
														trakt_date nvarchar(15),			/* Datum */\
														trakt_prop_id int,					/* Id f�r fastighet */\
														trakt_areal float,					/* Traktens areal totala (ha) */\
														trakt_areal_consider float,	/* Areal h�nsyn (ha) */\
														trakt_areal_handled float,	/* Areal bearbetad (ha) */\
														trakt_age int,							/* �lder */\
														trakt_si_h100 nvarchar(10),	/* SI H100 */\
														trakt_withdraw float,				/* Uttag i procent */\
														trakt_hgt_over_sea int,			/* H�jd �ver havet i meter */\
														trakt_ground int,						/* Grundf�rh�llanden */\
														trakt_surface int,					/* Ytf�rh�llnaden */\
														trakt_rake int,							/* Lutning */\
														trakt_mapdata nvarchar(40),	/* Kartinformation */\
														trakt_latitude int,					/* Latitude (Breddgrad) */\
														trakt_longitude int,				/* L�ngitude */\
														trakt_x_coord nvarchar(25),	/* X-koordinat */\
														trakt_y_coord nvarchar(25),	/* Y-koordinat */\
														trakt_origin nvarchar(30),		/* Ursprung */\
														trakt_inp_method nvarchar(30),	/* Indata metod */\
														trakt_hgt_class nvarchar(30),	/* H�jdklass */\
														trakt_cut_class nvarchar(30),	/* Huggningsklass */\
														trakt_sim_data int,						/* Ja eller Nej 1/0 */\
														trakt_near_coast int,					/* Ja eller Nej 1/0 */\
														trakt_southeast int,					/* Ja eller Nej 1/0 */\
														trakt_region5 int,						/* Ja eller Nej 1/0 */\
														trakt_part_of_plot int,				/* Ja eller Nej 1/0 */\
														trakt_si_h100_pine nvarchar(10),	/* SI (H100) f�r Tall (S�derbergs) */\
														trakt_prop_num nvarchar(50),			/* Fastighetsnummer */\
														trakt_prop_name nvarchar(50),		/* Namn p� fastighet */\
														trakt_notes ntext,							/* Noteringar */\
														trakt_created_by nvarchar(20),		/* Skapad av */\
														created datetime NOT NULL default CURRENT_TIMESTAMP,\
														trakt_wside tinyint,\
														trakt_img varbinary(MAX),\
														trakt_areal_factor float,	/* Inneh�ller uppr�kningsfaktor f�r areal (Cirkelytor etcc.) */\
														trakt_hgtcurve varbinary(MAX),\
														trakt_coordinates nvarchar(MAX),\	/* Inneh�ller koordinater; hateras av GIS/InData */\
														trakt_for_stand_only smallint, /* 1 = Ing�r ej i intr�ng, 0 = Ing�r i intr�ng */\
														trakt_point nvarchar(80),		/*Stand point coordinate */\
														PRIMARY KEY (trakt_id));");
//***************************************************************************************************
LPCTSTR tableEstiTraktMisc = _T("CREATE TABLE dbo.%s (\
														tprl_trakt_id int NOT NULL,		/* Trakt id */\
														tprl_name nvarchar(50),			/* Name of Pricelist */\
														tprl_typeof int,				/* Type of i.g. 1 = Pricelist for R. Ollas */\
														tprl_pricelist ntext,			/* Acual pricelist in xml-format */\
														tprl_costtmpl_name nvarchar(50),	/* Name of cost template */\
														tprl_costtmpl_typeof int,		/* Type of i.g. 1 = Trakt costs, 2 = LandValue */\
														tprl_costtmpl ntext,			/* Acual cost template in xml-format */\
														tprl_dcls float,				/* Diameterclass for trakt */\
														created datetime NOT NULL default CURRENT_TIMESTAMP,\
														PRIMARY KEY (tprl_trakt_id),\
														CONSTRAINT fk_tprl\
														FOREIGN KEY (tprl_trakt_id)\
														REFERENCES esti_trakt_table (trakt_id)\
														ON DELETE CASCADE\
														ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktData = _T("CREATE TABLE dbo.%s (\
																tdata_id int NOT NULL,				/* Equals specie id-number */\
																tdata_trakt_id int NOT NULL,\
																tdata_data_type smallint NOT NULL,	/* Typ av data 1=Uttag,2=Kvarl�mmnat,3=Uttag i Stickv�g etc. */\
																tdata_spc_id int,				/* Tr�dslagets id */\
																tdata_spc_name nvarchar(51),		/* Tr�dslagets namn */\
																tdata_percent float,					/* Procentuell andel av tr�dslag */\
																tdata_numof int,							/* Antal tr�d */\
																tdata_da float,								/* DA */\
																tdata_dg float,								/* DG */\
																tdata_dgv float,							/* DGV */\
																tdata_hgv float,							/* HGV */\
																tdata_gy float,								/* Grundyta */\
																tdata_avg_hgt float,					/* Medelh�jd */\
																tdata_h25 float,							/* H25 v�rde */\
																tdata_m3sk_vol float,					/* Volym M3SK */\
																tdata_m3fub_vol float,				/* Volym M3FUB */\
																tdata_m3ub_vol float,					/* Volym M3UB */\
																tdata_avg_m3sk_vol float,			/* Medelvolym M3SK */\
																tdata_avg_m3fub_vol float,		/* Medelvolym M3FUB */\
																tdata_avg_m3ub_vol float,			/* Medelvolym M3UB */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																tdata_gcrown float,						/* Krongr�ns procent v�rde */\
																tdata_grot float,							/* GROT */\
																PRIMARY KEY (tdata_id,tdata_trakt_id,tdata_data_type),\
																CONSTRAINT fk_tdata\
																FOREIGN KEY (tdata_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktSpcAssort = _T("CREATE TABLE dbo.%s (\
																tass_assort_id int NOT NULL,	/* Trakt assort id */\
																tass_trakt_id int NOT NULL,		/* Trakt id */\
																tass_trakt_data_id int NOT NULL,/* Trakt data id = specie id*/\
																tass_data_type smallint NOT NULL,	/* Typ av data 1=Uttag,2=Kvarl�mmnat,3=Uttag i Stickv�g etc. */\
																tass_assort_name nvarchar(31),	/* Name of assortment */\
																tass_price_m3fub float,			/* Pris i m3fub f�r utbytesvolym */\
																tass_price_m3to float,			/* Pris i m3to f�r utbytesvolym */\
																tass_m3fub float,				/* Utbytesvolym m3fub */\
																tass_m3to float,				/* Utbytesvolym m3to */\
																tass_m3fub_value float,			/* V�rde i ex. kronor */\
																tass_m3to_value float,			/* V�rde i ex. kronor */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																tass_kr_m3_value float,			/* Upp/nedr�kning i kronor per m3 */\
																PRIMARY KEY (tass_assort_id,tass_trakt_id,tass_trakt_data_id,tass_data_type),\
																CONSTRAINT fk_tass\
																FOREIGN KEY (tass_trakt_data_id,tass_trakt_id,tass_data_type)\
																REFERENCES esti_trakt_data_table (tdata_id,tdata_trakt_id,tdata_data_type)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktTransAssort = _T("CREATE TABLE dbo.%s (\
																ttrans_trakt_data_id int NOT NULL,	/* Trakt data id */\
																ttrans_trakt_id int NOT NULL,		/* Trakt id */\
																ttrans_from_ass_id int NOT NULL,	/* ID of FROM assortment */\
																ttrans_to_ass_id int NOT NULL,		/* ID of TO assortment */\
																ttrans_to_spc_id int NOT NULL,		/* ID of Specie */\
																ttrans_data_type smallint NOT NULL,	/* Typ av data 1=Uttag,2=Kvarl�mmnat,3=Uttag i Stickv�g etc. */\
																ttrans_from_name nvarchar(31),		/* Name of FROM assortment */\
																ttrans_to_name nvarchar(31),			/* Name of TO assortment */\
																ttrans_spc_name nvarchar(31),		/* Name of Specie */\
																ttrans_m3fub float,					/* Volume transfered */\
																ttrans_percent float,				/* Percent transfered */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																ttrans_trans_m3 float,			/* Volume transfered */\
																PRIMARY KEY (ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id,ttrans_to_spc_id,ttrans_data_type),\
																CONSTRAINT fk_ttrans\
																FOREIGN KEY (ttrans_trakt_data_id,ttrans_trakt_id,ttrans_data_type)\
																REFERENCES esti_trakt_data_table(tdata_id,tdata_trakt_id,tdata_data_type)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktSetSpc = _T("CREATE TABLE dbo.%s (\
																tsetspc_id int NOT NULL,\
																tsetspc_tdata_id int NOT NULL,			/* Trakt data table identifer */\
																tsetspc_tdata_trakt_id int NOT NULL,	/* Trakt table identifer */\
																tsetspc_data_type smallint NOT NULL,	/* Typ av data 1=Uttag,2=Kvarl�mmnat,3=Uttag i Stickv�g etc. */\
																tsetspc_specie_id int,					/* ID of specie function is set to */\
																tsetspc_hgt_func_id	int,				/* ID of selected height-function */\
																tsetspc_hgt_specie_id int,				/* ID of specie function to use */\
																tsetspc_hgt_func_index	int,			/* Index of selected height-function */\
																tsetspc_vol_func_id	int,				/* ID of selected volume-function */\
																tsetspc_vol_specie_id int,				/* ID of specie function to use */\
																tsetspc_vol_func_index	int,			/* Index of selected volume-function */\
																tsetspc_bark_func_id int,				/* ID of selected bark-function */\
																tsetspc_bark_specie_id int,				/* ID of specie function to use */\
																tsetspc_bark_func_index int,			/* Index of selected bark-function */\
																tsetspc_vol_ub_func_id	int,				/* ID of selected volume-function */\
																tsetspc_vol_ub_specie_id int,				/* ID of specie function to use */\
																tsetspc_vol_ub_func_index int,			/* Index of selected volume-function */\
																tsetspc_m3sk_m3ub float,				/* From m3sk to m3ub under bark */\
																tsetspc_m3sk_m3fub float,				/* From m3sk to m3fub */\
																tsetspc_m3fub_m3to float,				/* From m3fub to m3to */\
																tsetspc_dcls float,						/* Diameterclass in cm */\
																tsetspc_qdesc_index int,				/* Index for selected qualitydesc. for spc and pricelist */\
																tsetspc_extra_info nvarchar(50),			/* Can hold any function specific data. e.g. S�derbergs */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																tsetspc_transp_dist int,				/* Transportavst�n till v�g (m) */\
																tsetspc_transp_dist2 int,				/* Transportavst�n till Industri (km) */\
																tsetspc_qdesc_name nvarchar(50),	/*Namn p� vald kvalitetsbeskrivning */\
																tsetspc_grot_func_id int,		/* ID of selcted GROT-function */\
																tsetspc_grot_percent float,		/* Uttagsprocent */\
																tsetspc_grot_price float,			/* Pris (kr/ton) */\
																tsetspc_grot_cost float,			/* Kostnad (kr/ton) */\
																PRIMARY KEY (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type),\
																CONSTRAINT fk_tsetspc1\
																FOREIGN KEY (tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type)\
																REFERENCES esti_trakt_data_table (tdata_id,tdata_trakt_id,tdata_data_type)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktPlot = _T("CREATE TABLE dbo.%s (\
																tplot_id int NOT NULL,			/* Ytans id */\
																tplot_trakt_id int NOT NULL,	/* Traktid */\
																tplot_name nvarchar(40),			/* Traktnummer */\
																tplot_plot_type int,			/* Typ av plot 0 = Totaltax. etc. */\
																tplot_radius float,				/* Radie */\
																tplot_length1 float,			/* Rektangul�r yta */\
																tplot_width1 float,				/* Rektangul�r yta */\
																tplot_area float,				/* Ytans area m2 */\
																tplot_coord nvarchar(80),		/* Koordinat */\
																tplot_numof_trees int,			/* Antal tr�d */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																PRIMARY KEY (tplot_id,tplot_trakt_id),\
																CONSTRAINT fk_tplot\
																FOREIGN KEY (tplot_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiTraktSampleTrees = _T("CREATE TABLE dbo.%s (\
																ttree_id int NOT NULL,\
																ttree_trakt_id int NOT NULL,	/* Trakt id */\
																ttree_plot_id int,				/* Plot (Group) id */\
																ttree_spc_id int,				/* Tr�dslagets id */\
																ttree_spc_name nvarchar(20),		/* Tr�dslagets namn */\
																ttree_dbh float,				/* Br�sth�jdsdiameter i mm */\
																ttree_hgt float,				/* Tr�dets h�jd i dm */\
																ttree_gcrown float,				/* Gr�nkrona i procent (procent) */\
																ttree_bark_thick float,			/* Barktjocklek (dubbla) i mm */\
																ttree_grot float,				/* Biomassa i kg */\
																ttree_age int,					/* Tr�dets �lder */\
																ttree_growth int,				/* Tilv�xtfaktor */\
																ttree_tree_type int,			/* 0 = Provtr�d, 1 = Provtr�d i gatan, 2 = Kanttr�d */\
																ttree_dcls_from float,			/* Fr�n diameterklass */\
																ttree_dcls_to float,			/* Till diameterklass */\
																ttree_m3sk float,				/* Volume m3sk */\
																ttree_m3fub float,				/* Volume m3fub */\
																ttree_m3ub float,				/* Volume m3ub */\
																ttree_bonitet nvarchar(10),		/* Jonsson bonitet inr 1,12,2,23 etc */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																ttree_coord nvarchar(80),		/* Border tree coordinates */\
																ttree_distcable int, \
																ttree_category int, \
																ttree_topcut int, \
																PRIMARY KEY (ttree_id,ttree_trakt_id),\
																CONSTRAINT fk_ttree\
																FOREIGN KEY (ttree_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiDclsTrees = _T("CREATE TABLE dbo.%s (\
																tdcls_id int NOT NULL,\
																tdcls_trakt_id int NOT NULL,	/* Trakt id */\
																tdcls_plot_id int,				/* Plot (Group) id */\
																tdcls_spc_id int,				/* Tr�dslagets id */\
																tdcls_spc_name nvarchar(20),		/* Tr�dslagets namn */\
																tdcls_dcls_from float,			/* Fr�n diameterklass */\
																tdcls_dcls_to float,			/* Till diameterklass */\
																tdcls_hgt float,				/* H�jd i dm f�r klassmitt */\
																tdcls_numof float,				/* Antal tr�d i diamterklass */\
																tdcls_gcrown float,				/* Gr�nkrona i procent (procent) */\
																tdcls_bark_thick float,			/* Barktjocklek (dubbla) i mm */\
																tdcls_m3sk float,				/* Volume m3sk */\
																tdcls_m3fub float,				/* Volume m3fub */\
																tdcls_m3ub float,				/* Volume m3ub */\
																tdcls_grot float,				/* Biomassa i kg */\
																tdcls_age int,					/* Tr�dets �lder */\
																tdcls_growth int,				/* Tilv�xtfaktor */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																tdcls_numof_randtrees int,		/* Antal kanttr�d (intr�ng) */\
																PRIMARY KEY (tdcls_id,tdcls_trakt_id),\
																CONSTRAINT fk_tdcls\
																FOREIGN KEY (tdcls_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiDcls1Trees = _T("CREATE TABLE dbo.%s (\
																tdcls1_id int NOT NULL,\
																tdcls1_trakt_id int NOT NULL,	/* Trakt id */\
																tdcls1_plot_id int,				/* Plot (Group) id */\
																tdcls1_spc_id int,				/* Tr�dslagets id */\
																tdcls1_spc_name nvarchar(20),	/* Tr�dslagets namn */\
																tdcls1_dcls_from float,			/* Fr�n diameterklass */\
																tdcls1_dcls_to float,			/* Till diameterklass */\
																tdcls1_hgt float,				/* H�jd i dm f�r klassmitt */\
																tdcls1_numof float,				/* Antal tr�d i diamterklass */\
																tdcls1_gcrown float,			/* Gr�nkrona i procent (procent) */\
																tdcls1_bark_thick float,		/* Barktjocklek (dubbla) i mm */\
																tdcls1_m3sk float,				/* Volume m3sk */\
																tdcls1_m3fub float,				/* Volume m3fub */\
																tdcls1_m3ub float,				/* Volume m3ub */\
																tdcls1_grot float,				/* Biomassa i kg */\
																tdcls1_age int,					/* Tr�dets �lder */\
																tdcls1_growth int,				/* Tilv�xtfaktor */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																PRIMARY KEY (tdcls1_id,tdcls1_trakt_id),\
																CONSTRAINT fk_tdcls1\
																FOREIGN KEY (tdcls1_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_EstiDcls2Trees = _T("CREATE TABLE dbo.%s (\
																tdcls2_id int NOT NULL,\
																tdcls2_trakt_id int NOT NULL,	/* Trakt id */\
																tdcls2_plot_id int,				/* Plot (Group) id */\
																tdcls2_spc_id int,				/* Tr�dslagets id */\
																tdcls2_spc_name nvarchar(20),	/* Tr�dslagets namn */\
																tdcls2_dcls_from float,			/* Fr�n diameterklass */\
																tdcls2_dcls_to float,			/* Till diameterklass */\
																tdcls2_hgt float,				/* H�jd i dm f�r klassmitt */\
																tdcls2_numof float,				/* Antal tr�d i diamterklass */\
																tdcls2_gcrown float,			/* Gr�nkrona i procent (procent) */\
																tdcls2_bark_thick float,		/* Barktjocklek (dubbla) i mm */\
																tdcls2_m3sk float,				/* Volume m3sk */\
																tdcls2_m3fub float,				/* Volume m3fub */\
																tdcls2_m3ub float,				/* Volume m3ub */\
																tdcls2_grot float,				/* Biomassa i kg */\
																tdcls2_age int,					/* Tr�dets �lder */\
																tdcls2_growth int,				/* Tilv�xtfaktor */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																PRIMARY KEY (tdcls2_id,tdcls2_trakt_id),\
																CONSTRAINT fk_tdcls2\
																FOREIGN KEY (tdcls2_trakt_id)\
																REFERENCES esti_trakt_table (trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_TraktTreesAssort = _T("CREATE TABLE dbo.%s (\
																tassort_tree_trakt_id int NOT NULL,	/* Trakt id */\
																tassort_tree_id int NOT NULL,		/* Tree id */\
																tassort_tree_dcls float NOT NULL,	/* Diamterklass */\
																tassort_spc_id int,					/* Specie id */\
																tassort_spc_name nvarchar(30),		/* Namn of specie */\
																tassort_assort_name nvarchar(30),	/* Namn of assortment */\
																tassort_exch_volume float,			/* Utbytesber�knad volym */\
																tassort_exch_price float,			/* Utbytesber�knat pris f�r Sortiment och Tr�dslag Per Diamterklass */\
																tassort_price_in int,				/* 0 = m3fub 1 = m3to etc, */\
																created datetime NOT NULL default CURRENT_TIMESTAMP,\
																PRIMARY KEY (tassort_tree_dcls,tassort_tree_id,tassort_tree_trakt_id),\
																CONSTRAINT fk_tassort\
																FOREIGN KEY (tassort_tree_id,tassort_tree_trakt_id)\
																REFERENCES esti_trakt_dcls_trees_table (tdcls_id,tdcls_trakt_id)\
																ON DELETE CASCADE\
																ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_TraktType = _T("CREATE TABLE dbo.%s (\
															 id int NOT NULL IDENTITY(1,1),\
															 type_name nvarchar(45),\
															 notes nvarchar(255),\
															 created datetime NOT NULL default CURRENT_TIMESTAMP,\
															 PRIMARY KEY (id));");
//***************************************************************************************************
LPCTSTR table_Origin = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL IDENTITY(1,1),\
															origin_name nvarchar(30) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP);");
//***************************************************************************************************
LPCTSTR table_InputData = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL IDENTITY(1,1),\
															inpdata_name nvarchar(30) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP);");
//***************************************************************************************************
LPCTSTR table_HeightClass = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL IDENTITY(1,1),\
															hgtcls_name nvarchar(30) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP);");
//***************************************************************************************************
LPCTSTR table_CutClass = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL IDENTITY(1,1),\
															cutcls_name nvarchar(30) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP);");
//***************************************************************************************************
LPCTSTR table_TraktRotpost = _T("CREATE TABLE dbo.%s (\
															 rot_trakt_id int NOT NULL,\
															 rot_post_volume float,				/* Gangvirke */\
															 rot_post_value float,				/* Virkesv�rde */\
															 rot_post_cost1 float,				/* Kostnad 1; Huggning */\
															 rot_post_cost2 float,				/* Kostnad 2; Sk�rdare */\
															 rot_post_cost3 float,				/* Kostnad 3; Skotare */\
															 rot_post_cost4 float,				/* Kostnad 4; �vriga */\
															 rot_post_cost5 float,				/* Kostnad 5; Transport */\
															 rot_post_cost6 float,				/* Kostnad 6; Fast kostnad */\
															 rot_post_cost7 float,				/* Kostnad 7; TOM */\
															 rot_post_cost8 float,				/* Kostnad 8; TOM */\
															 rot_post_cost9 float,				/* Kostnad 9; TOM */\
															 rot_post_netto float,				/* Rotnetto */\
															 rot_post_origin smallint,			/* Origin 1 = Vanlig rotpost 2 = Intr�ngsers�ttning */\
															 created datetime NOT NULL default CURRENT_TIMESTAMP,\
															 PRIMARY KEY (rot_trakt_id),\
															 CONSTRAINT fk_rot\
															 FOREIGN KEY (rot_trakt_id)\
															 REFERENCES esti_trakt_table (trakt_id)\
															 ON DELETE CASCADE\
															 ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_RotpostSpc = _T("CREATE TABLE dbo.%s (\
															 spcrot_id int NOT NULL,				/* Equals specieid */\
															 spcrot_trakt_id int NOT NULL,\
															 spcrot_post_spcname nvarchar(20),		/* Namn p� Tr�dslag */\
															 spcrot_post_cost1 float,				/* Kostnad Transport kr/m3*/\
															 spcrot_post_cost2 float,				/* Summa Kostnad Transport kr */\
															 spcrot_post_cost3 float,				/* Kostnad Huggning kr/m3*/\
															 spcrot_post_cost4 float,				/* Summa Kostnad Huggning kr */\
															 spcrot_post_cost5 float,				/* Kostnad Transport till Industri kr/m3*/\
															 spcrot_post_cost6 float,				/* Summa Kostnad Transport till Industri kr*/\
															 spcrot_post_origin smallint,			/* Origin always 2 = Intr�ngsers�ttning */\
															 created datetime NOT NULL default CURRENT_TIMESTAMP,\
															 PRIMARY KEY (spcrot_id,spcrot_trakt_id),\
															 CONSTRAINT fk_spcrot\
															 FOREIGN KEY (spcrot_trakt_id)\
															 REFERENCES esti_trakt_table (trakt_id)\
															 ON DELETE CASCADE\
															 ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_RotpostOther = _T("CREATE TABLE dbo.%s (\
															 otcrot_id int NOT NULL,				/* Equals a inc number */\
															 otcrot_trakt_id int NOT NULL,\
															 otcrot_value float,					/* Kostnad i �vrig ers�ttning */\
															 otcrot_sum_value float,				/* Summa �vrig ers�ttning */\
															 otcrot_text nvarchar(255),				/* F�rklarande text */\
															 otcrot_percent float,					/* Om kostnad �r baserad p� ett procenttal (ex. MOMS)*/\
															 otcrot_type smallint,					/* Type 1 = �vrig intr. ers. 2 = �vriga fasta kostnader */\
															 created datetime NOT NULL default CURRENT_TIMESTAMP,\
															 PRIMARY KEY (otcrot_id,otcrot_trakt_id),\
															 CONSTRAINT fk_otcrot\
															 FOREIGN KEY (otcrot_trakt_id)\
															 REFERENCES esti_trakt_table (trakt_id)\
															 ON DELETE CASCADE\
															 ON UPDATE CASCADE);");
//***************************************************************************************************
LPCTSTR table_SampleTreesCategory_SVE = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL,\
															category nvarchar(45) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP,\
															PRIMARY KEY (id) );")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(1, 'Kategori 1');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(2, 'Kategori 2');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(3, 'Kategori 3');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(4, 'Kategori 4');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(5, 'Kategori 5');");

LPCTSTR table_SampleTreesCategory_ENU = _T("CREATE TABLE dbo.%s (\
															id int NOT NULL,\
															category nvarchar(45) NOT NULL default '',\
															notes nvarchar(255),\
															create_date datetime NOT NULL default CURRENT_TIMESTAMP,\
															PRIMARY KEY (id) );")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(1, 'Category 1');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(2, 'Category 2');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(3, 'Category 3');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(4, 'Category 4');")
_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(5, 'Category 5');");

//***************************************************************************************************
/* Alter Table scripts */
//***************************************************************************************************
// This script also does a check to see if column already exists; 090423 p�d
LPCTSTR alter_EstiTraktTable = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																	 where table_name = '%s' and column_name = 'trakt_img')  \
																	 alter table %s add trakt_img varbinary(MAX)");

LPCTSTR alter_EstiTraktTable090630 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'trakt_name')  \
																				 alter table %s alter column trakt_name nvarchar(50)");

LPCTSTR alter_EstiTraktTable090916 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'tsetspc_qdesc_name')  \
																				 alter table %s add tsetspc_qdesc_name nvarchar(50)");

LPCTSTR alter_EstiTraktTable091102 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'trakt_areal_factor')  \
																				 alter table %s add trakt_areal_factor float");


LPCTSTR alter_EstiSetSpcTable100311_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_grot_func_id')  \
																						alter table %s add tsetspc_grot_func_id int");

LPCTSTR alter_EstiSetSpcTable100311_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_grot_percent')  \
																						alter table %s add tsetspc_grot_percent float");

LPCTSTR alter_EstiSetSpcTable100315_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_grot_price')  \
																						alter table %s add tsetspc_grot_price float");

LPCTSTR alter_EstiSetSpcTable100316_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_grot_cost')  \
																						alter table %s add tsetspc_grot_cost float");

LPCTSTR alter_EstiSetSpcTable150929_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_default_h25')  \
																						alter table %s add tsetspc_default_h25 float");
// #4555: Default gr�nkroneandel
LPCTSTR alter_EstiSetSpcTable151006_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																						where table_name = '%s' and column_name = 'tsetspc_default_greencrown')  \
																						alter table %s add tsetspc_default_greencrown float");

LPCTSTR alter_EstiTraktDataTable110315_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							 where table_name = '%s' and column_name = 'tdata_grot')  \
																							 alter table %s add tdata_grot float");

LPCTSTR alter_EstiTraktTable100306 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'trakt_hgtcurve')  \
																				 alter table %s add trakt_hgtcurve varbinary(MAX)");

LPCTSTR alter_EstiTraktTable100419 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'trakt_coordinates')  \
																				 alter table %s add trakt_coordinates text");

LPCTSTR alter_EstiTraktTable100526 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				where table_name = '%s' and column_name = 'trakt_for_stand_only')  \
																				alter table %s add trakt_for_stand_only smallint");


LPCTSTR alter_EstiTraktTable150112_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				where table_name = '%s' and column_name = 'trakt_length')  \
																				alter table %s add trakt_length int");
LPCTSTR alter_EstiTraktTable150112_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				where table_name = '%s' and column_name = 'trakt_width')  \
																				alter table %s add trakt_width int");

LPCTSTR alter_EstiTraktTable190327 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'trakt_tillfutnyttj')  \
																									 alter table %s add trakt_tillfutnyttj bit");


LPCTSTR alter_EstiTraktTable151210_1 = _T("ALTER TABLE esti_trakt_table ALTER COLUMN trakt_img varbinary(MAX)");
LPCTSTR alter_EstiTraktTable151210_2 = _T("ALTER TABLE esti_trakt_table ALTER COLUMN trakt_hgtcurve varbinary(MAX)");

LPCTSTR alter_EstiTraktTransTable101110 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ttrans_trans_m3')  \
																							alter table %s add ttrans_trans_m3 float");

LPCTSTR alter_EstiTraktSampleTreesTable121024 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ttree_coord')  \
																							alter table %s add ttree_coord nvarchar(80)");
LPCTSTR alter_EstiTraktTable121024 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'trakt_point')  \
																							alter table %s add trakt_point nvarchar(80)");

LPCTSTR alter_EstiSampeTreesTable1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ttree_distcable')  \
																							alter table %s add ttree_distcable int");
LPCTSTR alter_EstiSampeTreesTable2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ttree_category')  \
																							alter table %s add ttree_category int");

LPCTSTR alter_EstiSampeTreesTable4205= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ttree_topcut')  \
																							alter table %s add ttree_topcut int");

LPCTSTR alter_EstiSampleTreesCategory150811_SVE = _T("IF (SELECT COUNT(*) FROM esti_sample_trees_category_table) = 0 BEGIN ")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(1, 'Kategori 1');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(2, 'Kategori 2');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(3, 'Kategori 3');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(4, 'Kategori 4');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(5, 'Kategori 5');")
													_T("END");

LPCTSTR alter_EstiSampleTreesCategory150811_ENU = _T("IF (SELECT COUNT(*) FROM esti_sample_trees_category_table) = 0 BEGIN ")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(1, 'Category 1');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(2, 'Category 2');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(3, 'Category 3');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(4, 'Category 4');")
													_T("INSERT INTO esti_sample_trees_category_table (id, category) VALUES(5, 'Category 5');")
													_T("END");


LPCTSTR alter_EstiTraktTable151009 = _T("ALTER TABLE %s ")
										_T("ALTER COLUMN trakt_coordinates nvarchar(MAX) NULL");

#endif
