// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "StandEntryDoc.h"
#include "MDITabbedView.h"
#include "SampleTreeReportView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CSampleTreeReportView

IMPLEMENT_DYNCREATE(CSampleTreeReportView,  CMyReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSampleTreeReportView,  CMyReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)

	//#4380 20150602 J� Testar
	ON_NOTIFY(XTP_NM_REPORT_REQUESTEDIT , XTP_ID_REPORT_CONTROL, OnReportReqEdit)
END_MESSAGE_MAP()

CSampleTreeReportView::CSampleTreeReportView()
	: CMyReportView()
{
	m_pDB = NULL;
	m_bInitialized = FALSE;
}

CSampleTreeReportView::~CSampleTreeReportView()
{
}

void CSampleTreeReportView::OnInitialUpdate()
{
	CMyReportView::OnInitialUpdate();

	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST2, &pWnd->m_wndFieldChooserDlg2);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (!m_bInitialized)
	{

//		VERIFY(CXTPReportControl::UseReportCustomHeap());
//		CXTPReportControl::UseRowBatchAllocation();

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		setupReport();

		LoadReportState();

		m_bInitialized = TRUE;

	}

}

BOOL CSampleTreeReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CMyReportView::OnCopyData(pWnd, pData);
}

void CSampleTreeReportView::OnDestroy()
{
	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	CMyReportView::OnDestroy();	
}

BOOL CSampleTreeReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CMyReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CSampleTreeReportView diagnostics

#ifdef _DEBUG
void CSampleTreeReportView::AssertValid() const
{
	CMyReportView::AssertValid();
}

void CSampleTreeReportView::Dump(CDumpContext& dc) const
{
	CMyReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CSampleTreeReportView message handlers

// CSampleTreeReportView message handlers
void CSampleTreeReportView::OnSize(UINT nType,int cx,int cy)
{
	CMyReportView::OnSize(nType,cx,cy);
}

void CSampleTreeReportView::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

// Create and add Assortment settings reportwindow
BOOL CSampleTreeReportView::setupReport(void)
{

	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				// Also setup text strings; 070509 p�d					
				m_sFieldChooser = (xml->str(IDS_STRING147));
				m_sPrintPreview = (xml->str(IDS_STRING316));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					GetReportCtrl().ShowWindow( SW_NORMAL );
					GetReportCtrl().ShowGroupBy( TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->AddExpandButton();

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING263)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING264)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING265)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING266)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING267)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING268)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING271)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING273)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING274)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING2720)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING2721)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING269)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, (xml->str(IDS_STRING270)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml->str(IDS_STRING2700)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_15, (xml->str(IDS_STRING275)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_16, (xml->str(IDS_STRING276)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_UPPERCASE; 
					//pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER;

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_17, xml->str(IDS_STRING294), 100));	// _T("Kategori")
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_18, xml->str(IDS_STRING295), 100));	// _T("Fasavst�nd")
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_19, xml->str(IDS_STRING296), 100));	// _T("Toppningsv�rde")
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_20, xml->str(IDS_STRING2961), 100));	// _T("Koordinat")
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(TRUE);
					GetReportCtrl().GetColumns()->Find(COLUMN_0)->SetTreeColumn(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}


void CSampleTreeReportView::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();
	if (pRow != NULL)
	{
		CTraktSampleTreeReportRec *pRec = (CTraktSampleTreeReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{		
			int nGrpID = pRec->getColumnInt(COLUMN_0);
			setGroupID(nGrpID);
			setGroupName(nGrpID);
			m_bIsDirty = TRUE;	// Notify that something's happend on the grid; 070530 p�d		
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)

}

//#4380 20150602 J�
void CSampleTreeReportView::OnReportReqEdit(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
		if (pRecItem != NULL)
		{
			CTraktSampleTreeReportRec *pRec = (CTraktSampleTreeReportRec*)pRecItem->GetRecord();
			if (pRec != NULL)
			{
				int nTypeTree=-1;
				CString cTmp=_T("");
				cTmp		= pRec->getColumnText(COLUMN_15);
			}
		}
	}
}

void CSampleTreeReportView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	int nIndex = -1, nTmp=-1, nPrevType=-1,nNewType=-1;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
			CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
		if (pRecItem != NULL)
		{
			CTraktSampleTreeReportRec *pRec = (CTraktSampleTreeReportRec*)pRecItem->GetRecord();
			if (pRec != NULL)
			{

				//#4380 20150528 J�
				/*
				CXTPReportColumn *pColumn_15 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_15);
				if (pColumn_15 != NULL)
				{
					int nTypeTree=-1;
					CString cTmp=_T(""),cToType=_T("");
					cToType		= pRec->getColumnText(COLUMN_15);
					//cFromType	= ??;
					
					if (pItemNotify->pColumn->GetIndex() == pColumn_15->GetIndex())
					{

						CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
						if (pTabView != NULL)
						{
							CPageThreeFormView *pView = pTabView->getPageThreeFormView();
							if (pView != NULL)
							{
								if(!pView->doTreeTypeHasChanged(pRec,m_cFromType,cToType))
								{
									//Byt tillbaks till gamla tr�dtypen
									pRec->setColumnText(COLUMN_15,m_cFromType);
								}
							}
						}

					}
				}*/
			

				// Set specie id in Tr�dID column; 070515 p�d		
				CXTPReportColumn *pColumn2 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_2);
				if (pColumn2 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn2);
						if (pEdOptions != NULL)
						{
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pColumn2));
							if (pConst != NULL)
							{
								pRec->setColumnInt(COLUMN_1,(int)pConst->m_dwData);
								// User changed specie; 070521 p�d
								m_bIsDirty = TRUE;
							}	// if (pConst != NULL)
						}	// if (pEdOptions != NULL)
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn2->GetIndex())
				}	// if (pColumn2 != NULL)

				// Get column for Group ID and compare entered value
				// to Plot data (Groups), for this Trakt; 070515 p�d
				CXTPReportColumn *pColumn0 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_0);
				if (pColumn0 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn0->GetIndex())
					{
						CPageThreeFormView *pView = (CPageThreeFormView *)getFormViewByID(IDD_FORMVIEW9);
						if (pView != NULL)
						{
							pView->isPlotGroupID(pRec->getColumnInt(COLUMN_0));
						}
					}	// if (pItemNotify->pColumn->GetIndex() == pColumn0->GetIndex())
				}	// if (pColumn0 != NULL)

				// Enable editing height (if not a sample tree with calculated height)
				CXTPReportColumn *pColumn15 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_15);
				if (pColumn15 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn15->GetIndex())
					{
						BOOL editable = TRUE;
						int ct = pColumn15->GetEditOptions()->GetConstraints()->GetCount();
						for( int i=0; i < pColumn15->GetEditOptions()->GetConstraints()->GetCount(); i++ )
						{
							CString constraint = pColumn15->GetEditOptions()->GetConstraints()->GetAt(i)->m_strConstraint;
							if( _tcscmp(pColumn15->GetEditOptions()->GetConstraints()->GetAt(i)->m_strConstraint, pRec->getColumnText(COLUMN_15)) == 0 )
							{
								if( i == TREE_OUTSIDE_NO_SAMP ||
									i == TREE_POS_NO_SAMP ||
									i == TREE_OUTSIDE_LEFT_NO_SAMP ||
									i == TREE_INSIDE_LEFT_NO_SAMP)
								{
									editable = FALSE;
								}
								break;
							}
						}
						pRec->GetItem(COLUMN_4)->SetEditable(editable);
					}
				}


				// 
				/*CXTPReportColumn *pColumn17 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_17);
				if (pColumn17 != NULL)
				{
					if (pItemNotify->pColumn->GetIndex() == pColumn17->GetIndex())
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pRecItem->GetEditOptions(pColumn17);
						if (pEdOptions != NULL)
						{
							CString csTemp = pRecItem->GetCaption(pColumn17);
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pColumn17));
							if (pConst != NULL)
							{
								pRec->setColumnInt(COLUMN_17,(int)pConst->m_dwData);
								// User changed specie; 070521 p�d
								m_bIsDirty = TRUE;
							}
						}
					}
				}*/

			}	// if (pRec != NULL)
		}	// if (pRecItem != NULL)
	}	// if (pItemNotify != NULL)

}

void CSampleTreeReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
/* Commented out 2008-01-14 p�d
	EditItem not used insted
	-------------------------------------------------------
	pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
	-------------------------------------------------------
	have been added to editable columns.
	if (pItemNotify->pRow && pItemNotify->pColumn)
	{
			if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0 || 
					pItemNotify->pColumn->GetItemIndex() == COLUMN_2 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_3 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_4 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_5 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_6 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_7 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_8 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_9 ||
//					pItemNotify->pColumn->GetItemIndex() == COLUMN_14 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_15 ||
					pItemNotify->pColumn->GetItemIndex() == COLUMN_16)
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&GetReportCtrl(), pItemNotify->pRow, pItemNotify->pColumn);
				GetReportCtrl().EditItem(&itemArgs);
			}
	}
*/
}

void CSampleTreeReportView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
/* Commented out 2008-01-14 p�d
	EditItem not used insted
	-------------------------------------------------------
	pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
	-------------------------------------------------------
	have been added to editable columns.

	if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
	{
		CXTPReportColumn *pColumn = GetReportCtrl().GetFocusedColumn();
		CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
		if (pColumn != NULL && pRow != NULL)
		{
			if (pColumn->GetItemIndex() == COLUMN_0 || 
					pColumn->GetItemIndex() == COLUMN_2 ||
					pColumn->GetItemIndex() == COLUMN_3 ||
					pColumn->GetItemIndex() == COLUMN_4 ||
					pColumn->GetItemIndex() == COLUMN_5 ||
					pColumn->GetItemIndex() == COLUMN_6 ||
					pColumn->GetItemIndex() == COLUMN_7 ||
					pColumn->GetItemIndex() == COLUMN_8 ||
					pColumn->GetItemIndex() == COLUMN_9 ||
//					pColumn->GetItemIndex() == COLUMN_14 ||
					pColumn->GetItemIndex() == COLUMN_15 ||
					pColumn->GetItemIndex() == COLUMN_16)
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&GetReportCtrl(), pRow, pColumn);
				GetReportCtrl().EditItem(&itemArgs);
			}
		}	// if (pColumn != NULL && pRow != NULL)
	}	// if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
*/
}

void CSampleTreeReportView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenuW(MF_STRING, ID_SHOW_FIELDCHOOSER2, m_sFieldChooser);
	menu.AppendMenuW(MF_STRING, ID_PREVIEW2, m_sPrintPreview);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER2 :
		{
			CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
			if (pWnd != NULL)
			{
				BOOL bShow = !pWnd->m_wndFieldChooserDlg2.IsVisible();
				pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg2, bShow, FALSE);
			}
		}
		break;

		case ID_PREVIEW2 :
			OnPrintPreview();
		break;
	}
	menu.DestroyMenu();
}

void CSampleTreeReportView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now
	}
	// Disable print preview button on Toolbar. This is because if user clicks the
	// preview button there'll be multiple instances on Prieview.
	// We doesn't wan't that; 071218 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,TRUE,FALSE);
	}

}

void CSampleTreeReportView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	// Enable print preview button on Toolbar on ending preview; 071218 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,TRUE,TRUE);
	}

	CMyReportView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CSampleTreeReportView::doRunPrintPreview(void)
{
	OnPrintPreview();
}

void CSampleTreeReportView::OnFilePrint()
{
	CMyReportView::OnFilePrint();
}

void CSampleTreeReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_SAMPLE_TREE_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CSampleTreeReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_SAMPLE_TREE_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

