// stdafx.cpp : source file that includes just the standard includes
// EvalueStand.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "MDITabbedView.h"
#include "ResLangFileReader.h"


extern HINSTANCE hInst;

//////////////////////////////////////////////////////////////////////////////////////////
/*
CTransaction_trakt_misc_data CCDC::CCommonDataContainer::m_recTraktMiscData = CTransaction_trakt_misc_data();
vecTransactionSampleTree CCDC::CCommonDataContainer::m_vecTraktSampleTrees;
vecTransactionTraktData CCDC::CCommonDataContainer::m_vecTraktData;

CCDC::CCommonDataContainer::CCommonDataContainer(void)
{
}

CCDC::CCommonDataContainer::~CCommonDataContainer(void)
{
	m_vecTraktSampleTrees.clear();
	m_vecTraktData.clear();
}

*/
//////////////////////////////////////////////////////////////////////////////////////////


CString getResStr(UINT id)
{
	CString sText=_T("");
	sText.LoadString(id);

	return sText;
}

CString getToolBarResourceFN(void)
{
	CString sPath=_T("");
	CString sToolBarResPath=_T("");
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr=_T("");
	CString sDocName=_T("");
	CString sCaption=_T("");
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle=_T("");
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}

CView *getFormViewByID(int idd)
{
	CString sResStr=_T("");
	CString sDocName=_T("");
	CString sCaption=_T("");
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr=_T("");
	CString sDocName=_T("");
	CString sCaption=_T("");
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE]=_T("");	// 10 kb buffer

	TCHAR sDB_PATH[127]=_T("");
	TCHAR sUserName[127]=_T("");
	TCHAR sPSW[127]=_T("");
	TCHAR sDSN[127]=_T("");
	TCHAR sLocation[127]=_T("");
	TCHAR sDBName[127]=_T("");
	int nAuthentication=0;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		UMMessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}
/*
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!db_name.IsEmpty())
					_tcscpy_s(sDBName,127,db_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		UMMessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}
*/
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table,CString database_name)
{
	TCHAR sDB_PATH[127]=_T("");
	TCHAR sUserName[127]=_T("");
	TCHAR sPSW[127]=_T("");
	TCHAR sDSN[127]=_T("");
	TCHAR sLocation[127]=_T("");
	TCHAR sDBName[127]=_T("");
	int nAuthentication=0;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript=_T("");

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!database_name.IsEmpty())
					_tcscpy_s(sDBName,127,database_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (do_create_table)
								{
									if (!pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
								else if (!do_create_table)
								{
									if (pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		UMMessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action)
{
	TCHAR sDB_PATH[127]=_T("");
	TCHAR sUserName[127]=_T("");
	TCHAR sPSW[127]=_T("");
	TCHAR sDSN[127]=_T("");
	TCHAR sLocation[127]=_T("");
	TCHAR sDBName[127]=_T("");
	int nAuthentication=0;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	Scripts scripts;

	CString sScript=_T("");

	try
	{

		if (vec.size() > 0)
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							for (UINT i = 0;i < vec.size();i++)
							{
								scripts = vec[i];

								if (!scripts.getDBName().IsEmpty())
											_tcscpy_s(sDBName,127,scripts.getDBName());

								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

								if (pDB->existsDatabase(sDBName))
								{
									if (action == Scripts::TBL_CREATE)
									{
										if (!pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (!pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// if (scripts._CreateTable)
									else if (action == Scripts::TBL_ALTER)
									{
										if (pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// else if (!scripts._CreateTable)
								}	// if (pDB->existsDatabase(sDBName))
							} // for (UINT i = 0;i < vec.size();i++)
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		UMMessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec)
{
	vec.clear();
	if (fileExists(fn))
	{
		XMLShellData *pParser = new XMLShellData();
		if (pParser != NULL)
		{
			if (pParser->load(fn))
			{
				pParser->getReports(add_to,index,vec,getLangSet());
			}
			delete pParser;
		}
	}
	return (vec.size() > 0);
}

//=====================================================================================
// Generic functions (multiused), for getting data from
// FormView classes; 070515 p�d

// Get Selected inventory method index; 070514 p�d
int getInvMethodIndex(void)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			nIndex = pView->getSelectedInvMethodIndex();
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)

	return nIndex;
}

// Get active group, set in CPageThreeFormView::getSelectedInvMethodIndex(void) ( m_wndCBox1.GetCurSel()); 070514 p�d
int getGroupID(void)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			nIndex = pView->getGroupID();
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)

	return nIndex;
}

// Get active group, set in CPageThreeFormView::getSelectedInvMethodIndex(void) ( m_wndCBox1.GetCurSel()); 070514 p�d
void setGroupID(int id)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->setGroupID(id);
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}

void setReportRecordGroupID(int id)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->setReportRecordGroupID(id);
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}

void setResetReportRecordSelected(void)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->setResetReportRecordSelected();
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}

void setGroupName(int grp_id)
{
	CString S=_T("");
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			S = pView->getGroupName(grp_id);
			pView->setGroupName(S);
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}

void setGroupName(LPCTSTR name)
{
	int nIndex = 0;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->setGroupName(name);
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}

// Enable/Disable Tabs in TabbedView; 070515 p�d
void enableTabPage(int idx,BOOL enable)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		pTabView->enablePage(idx,enable);
	}	// if (pTabView != NULL)
}

// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
CTransaction_trakt* getActiveTrakt(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		// This is for running the OnSetFocus(), setting HMSShell Toolbars; 070315 p�d
		CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
																							CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(0)->GetHandle()));
		{
			return &pView->getActiveTraktRecord();
		}	// CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
	}	// if (pTabView)

	return NULL;
}

// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
CTransaction_trakt_data* getActiveTraktDataOnClick(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		// This is for running the OnSetFocus(), setting HMSShell Toolbars; 070315 p�d
		CPageTwoFormView* pView = DYNAMIC_DOWNCAST(CPageTwoFormView, 
																							CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(1)->GetHandle()));
		{
			return &pView->getOnClickActiveTraktDataRecord();
		}	// CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView,
	}	// if (pTabView)

	return NULL;
}

// Get information on active trakt data, set in CPageTwoFormView (a formview in TabbedView); 070515 p�d
CTransaction_trakt_data* getActiveTraktData(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageTwoFormView *pView1 = DYNAMIC_DOWNCAST(CPageTwoFormView, 
																								CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(1)->GetHandle()));
		if (pView1 != NULL)
		{
			// Get active trakt record; holds information on trakt-id; 070430 p�d
			return &pView1->getActiveTraktDataRecord();
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)

	return NULL;
}

// Get information on active trakt data, set in CPageTwoFormView (a formview in TabbedView); 070515 p�d
CTransaction_trakt_misc_data* getActiveTraktMiscData(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		// TODO:
		CPageTwoFormView *pView1 = DYNAMIC_DOWNCAST(CPageTwoFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(1)->GetHandle()));
		if (pView1 != NULL)
		{
			// Get active trakt record; holds information on trakt-id; 070430 p�d
			return &pView1->getActiveTraktMiscDataRecord();
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)

	return NULL;
}


void populatePageThree(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->populateData();
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}


void setIsDirtyPageThree(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView != NULL)
		{
			pView->setIsDirtyPageThree();
		}	// if (pView3 != NULL)
	}	// if (pTabView != NULL)
}


enumACTION getPageOneAction(void)
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		// Identify the window, and send SetFocus() to that window.
		// This is for running the OnSetFocus(), setting HMSShell Toolbars; 070315 p�d
		CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
																							CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(0)->GetHandle()));
		{
			return pView->getAction();
		}	// CPageOneFormView* pView = DYNAMIC_DOWNCAST(CPageOneFormView, 
	}	// if (pTabView)

	return NO_ACTION;

}

void setStatusBarText(int index,LPCTSTR text)
{
	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setStatusBarText(index,text);
	}
}

BOOL populateDataInPageOne(void)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView = pTabView->getPageOneFormView();
		if (pView != NULL)
		{
			pView->doPopulateData();
			return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

BOOL populateDataInPageOne(int trakt_id)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView = pTabView->getPageOneFormView();
		if (pView != NULL)
		{
			pView->doPopulateData(trakt_id);
			return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

BOOL populateDataInPageThree(void)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			pView->populateData();
			return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

BOOL isSpecieDialogFrameDirty(void)
{
	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDISpecieDataFrame *pFrame = (CMDISpecieDataFrame *)getFormViewParentByID(IDD_FORMVIEW8);
	if (pFrame != NULL)
	{
		return pFrame->getIsDirty();
	}
	return FALSE;
}

BOOL isCruiseInObject(int ecru_id)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView = pTabView->getPageOneFormView();
		if (pView != NULL)
		{
			pView->isCruiseInObject(ecru_id);
			return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

int SplitString(const CString& input, 
  const CString& delimiter, CStringArray& results)
{
	CString tmp=_T("");
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find(delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos >= iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2); //+1);
  }

  for( int i = 0; i < positions.GetSize(); i++ )
  {
    CString s=_T("");
    if( i == 0 )
		{
      s = input.Mid( i, positions[i] );
		}
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if ( i == positions.GetSize() )
				{
          s = input.Mid(offset);
				}
        else if ( i > 0 )
				{
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
				}
      }
    }
    if( s.GetLength() >= 0 )
      results.Add(s);
  }
  return numFound;
}


void getTemplateMainSettings(TemplateParser *pars,int *hgt_over_sea,int *latitude,
																						 LPTSTR growth_area,
																						 LPTSTR si_h100,
																						 double *corr_fac,
																						 int *costs_id,
																						 LPTSTR costs_tmpl_name,
																						 bool *is_at_coast,
																						 bool *is_south_east,
																						 bool *is_region5,
																						 bool *is_part_of_plot,
																						 LPTSTR si_h100_pine)
{
	int nValue=0;
	if (pars != NULL)
	{
		pars->getTemplateHgtOverSea(hgt_over_sea);
		pars->getTemplateLatitude(latitude);
		pars->getTemplateGrowthArea(growth_area);
		pars->getTemplateSI_H100(si_h100);
		pars->getTemplateCostsTmpl(costs_id,costs_tmpl_name);
		pars->getIsAtCoast(&nValue);
		*is_at_coast = (nValue == 1 ? TRUE : FALSE);

		pars->getIsSouthEast(&nValue);
		*is_south_east = (nValue == 1 ? TRUE : FALSE);

		pars->getIsRegion5(&nValue);
		*is_region5 = (nValue == 1 ? TRUE : FALSE);

		pars->getPartOfPlot(&nValue);
		*is_part_of_plot = (nValue == 1 ? TRUE : FALSE);

		pars->getSIH100_Pine(si_h100_pine);
	}
}

void getTemplateLongitudeSettings(TemplateParser *pars,int *lat)
{
	if (pars != NULL)
	{
		pars->getTemplateLongitude(lat);
	}
}

void getTemplateSettings(TemplateParser *pars,int *exch_id,LPTSTR exch,int *prl_id,
												 int *prl_type_of,LPTSTR prl_name,double *dcls)
{
	if (pars != NULL)
	{
		pars->getTemplateExchange(exch_id,exch);
		pars->getTemplatePricelist(prl_id,prl_name);
		pars->getTemplatePricelistTypeOf(prl_type_of);
		pars->getTemplateDCLS(dcls);
	}
}

//----------------------------------------------------------------------------
// OBS! These methods also in UMTemplates; 070903 p�d
void getTemplateSpecies(TemplateParser *pars,vecTransactionSpecies &vec)
{
	if (pars != NULL)
	{
		pars->getSpeciesInTemplateFile(vec);
	}
}

void getTemplateFunctionsPerSpcecie(TemplateParser *pars,int spc_id,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const)
{
	if (pars != NULL)
	{
		pars->getFunctionsForSpecie(spc_id,vec,m3fub_m3to,m3sk_m3ub_const);
	}
}

void getTemplateMiscFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *qdesc_index,LPTSTR qdesc,
																				double *sk_fub,double *fub_to,double *h25,int *transp_dist1,
																				int *transp_dist2,double *gcrown)
{
	if (pars != NULL)
	{
		pars->getMiscDataForSpecie(spc_id,qdesc_index,qdesc,sk_fub,fub_to,h25,transp_dist1,transp_dist2,gcrown);
	}
}

void getTemplateGrotFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *grot_index,double *percent,double *price,double *cost)
{
	if (pars != NULL)
	{
		pars->getGrotDataForSpecie(spc_id,grot_index,percent,price,cost);
	}
}

void getTemplateTransfersPerSpcecie(TemplateParser *pars,int spc_id,vecTransactionTemplateTransfers &vec)
{
	if (pars != NULL)
	{
		pars->getTransfersForSpecie(spc_id,vec);
	}
}

typedef struct _tagLICENSE
{
	CString csModID; // module id
	CString csModName; // module name
	int nType; // 1 = User, 2 = Computer, 3 = Counter, 4 = Yes/No, 5 = Item, 6 = Concurrent User (only possible if the necessary licence exists)
	int nLicenses; // Number of users or computers etc. In a Yes/No-Module 1 for yes and 0 for no.
	BOOL bDemo; // demo?
	DATE dtExpires; // Date when this module will expire, 0 for unlimited
	int nNoOfDays; // Number of days this module will run after the first usage
	DATE dtMaxDate; // Max date when this module will no longer work, even if there are open days, 0 for unlimited
	CString csTag; // The tag value of that module
	BOOL bAllowDeactivate; // Can users, computers or items be deactivated In that module? 
	int nWebActivation; // WebActivation State of the new module (0-3). See Web Activation Documentation. 
	BOOL bYesNo; // TRUE = yes, FALSE = no
	int nRemDays; // amount of days left
	int nRemLic; // amount of licenses left
} LICENSE;


BOOL License(void)
{
	CString sLangFN=_T("");
	CString sHeadline=_T("");
	CString sNoLicenseMsg=_T("");

	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			sHeadline = xml.str(IDS_STRING10);
		}
		xml.clean();
	}

	TCHAR szBuf[MAX_PATH]=_T(""), szBuf2[MAX_PATH]=_T("");
	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szBuf2, NULL);

/*	Commented out 2009-04-02 P�D, NOT USED
	sLangFN = getLanguageFN(getLanguageDir(),SHELL_PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			sNoLicenseMsg = xml.str(14011);	// 14011 set in HMSShellxxx.xml language-file; 090119 p�d
		}
		xml.clean();
	}
*/
	// check if we have a valid license.
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H2201"), sHeadline, (bShowLicenseDialog ? _T("1") : _T("0")),szBuf2);
	int nRet = AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	// Check if there's a license-dll present; 090119 p�d
	if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)
	{
		//	Commented out 2009-04-02 P�D, NOT USED
		//UMMessageBox(sNoLicenseMsg);
		return FALSE;
	}	// if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)

	if (bShowLicenseDialog)
		bShowLicenseDialog = FALSE;

	return (_tcscmp(msg.getFunc(), _T("1")) == 0);	// License ok; 081210 p�d
}

/////////////////////////////////////////////////////////////////////////////////////
// Calculate Height,Volyme,Exchange etc; 080116 p�d

// This function'll calulate, usin' data both in database and in PageThree; 080116 p�d
BOOL runDoCalulationInPageThree(int spc_id,BOOL remove_trakt_trans,BOOL check_changes,BOOL show_msg,bool do_transfers,bool do_populate)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			if (pView->doCalculations(spc_id,remove_trakt_trans,check_changes,show_msg,do_transfers,do_populate) == IDNO)
				return FALSE;
			else
				return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

// This function'll calulate, usin' data both in database and in PageThree; 080116 p�d
BOOL runDoHeightCurveToDBInPageThree(int trakt_id)
{
	// Get access to Page three; 070627 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			pView->createHeightCurveForDB(trakt_id);
			return TRUE;
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	return FALSE;
}

CString formatData(LPCTSTR fmt,...)
{
	CString sFmt=_T("");
	va_list ap;
	va_start(ap,fmt);
	sFmt.FormatV(fmt,ap);
	va_end(ap);
	return (sFmt);
}

int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText, ...)
{
    va_list argList;
    va_start(argList, pText);

    ASSERT(nNoOfCols >= 1); // use the 'normal' Insert function with 1 argument
    int nIndex = ctrl.InsertItem(LVIF_TEXT|LVIF_STATE, nPos, pText,0,LVIS_SELECTED,0,0);
    ASSERT(nIndex != -1);
    if (nIndex < 0) return(nIndex);
    for (int i = 1; i < nNoOfCols; i++) 
		{
        LPCTSTR p = va_arg(argList,LPCTSTR);
        if (p != NULL) 
				{
					ctrl.SetItemText(nIndex, i, p);    
        }
    }
    va_end(argList);
    return(nIndex);
}

int GetSelectedItem(CListCtrl &ctrl)
{
	int nItem = -1;
  POSITION nPos = ctrl.GetFirstSelectedItemPosition();
  if (nPos)
  {
		nItem = ctrl.GetNextSelectedItem(nPos);
  }
  return nItem;
}


void SecurityMessage(HWND hwnd,LPCTSTR lang_fn,LPCTSTR date)
{
	CString sMsg=_T(""),sMsgCap=_T(""),sDate(date);
	RLFReader xml;
	if (xml.Load(lang_fn))
	{
		sMsgCap = xml.str(IDS_STRING289);
		sMsg.Format(_T("%s\n%s\n\n%s : %s\n\n%s"),
			xml.str(IDS_STRING4200),
			xml.str(IDS_STRING4201),
			xml.str(IDS_STRING4202),
			sDate.Left(sDate.Find('.')),
			xml.str(IDS_STRING4203));
	}	// if (xml.Load(lang_fn))
	if (hwnd == NULL)
		UMMessageBox(0,sMsg,sMsgCap,MB_ICONASTERISK | MB_OK);
	else if (hwnd != NULL)
		UMMessageBox(hwnd,sMsg,sMsgCap,MB_ICONASTERISK | MB_OK);
}

void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}

BOOL checkPricelist(LPCTSTR prl_xml)
{
	vecTransactionSpecies vecSpc;
	CTransaction_species recSpc;
	vecTransactionAssort vecAss;
	CTransaction_assort recAss;
	std::map<short,short> mapCnt;
	BOOL bReturn = TRUE;

	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	// Check pricelist, to see that there's only one pulpassortmant for exchangecalculations; 101018 p�d
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(prl_xml))
		{
			pPrlParser->getAssortmentPerSpecie(vecAss);
			pPrlParser->getSpeciesInPricelistFile(vecSpc);

			if (vecAss.size() > 0 && vecSpc.size() > 0)
			{
				for (UINT i1 = 0;i1 < vecSpc.size();i1++)
				{
					recSpc = vecSpc[i1];
					mapCnt[recSpc.getSpcID()] = 0;
					for (UINT i2 = 0;i2 < vecAss.size();i2++)
					{
						recAss = vecAss[i2];
						if (recAss.getMinDiam() > 0.0 && recAss.getPulpType() > 0 && recAss.getSpcID() == recSpc.getSpcID())
						{
							mapCnt[recSpc.getSpcID()]++;
						}
					}	// for (UINT i2 = 0;i2 < vecAss.size();i2++)
				}	// for (UINT i1 = 0;i1 < vecSpc.size();i1++)

				//-------------------------------------------------------------------------
				for (UINT i3 = 0;i3 < vecSpc.size();i3++)
				{
					recSpc = vecSpc[i3];
					if (mapCnt[recSpc.getSpcID()] > 1)
					{
						bReturn = FALSE;
						break;
					}
				}
				//-------------------------------------------------------------------------
			}	// if (vecAss.size() > 0 && vecSpc.size() > 0)
		}
		delete pPrlParser;
	}

	return bReturn;
}
