// HandleSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "HandleSettingsDlg.h"

#include "ResLangFileReader.h"

// CHandleSettingsDlg dialog

IMPLEMENT_DYNAMIC(CHandleSettingsDlg, CDialog)

BEGIN_MESSAGE_MAP(CHandleSettingsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHandleSettingsDlg::OnBnClickedOk)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
	ON_BN_CLICKED(IDCANCEL, &CHandleSettingsDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


CHandleSettingsDlg::CHandleSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHandleSettingsDlg::IDD, pParent)
{

}

CHandleSettingsDlg::CHandleSettingsDlg(CString spc_name,int spc_id,int trakt_id,CUMEstimateDB* db)
	: CDialog(CHandleSettingsDlg::IDD, NULL),
		m_pDB(db),
		m_sSpecieName(spc_name),
		m_nSpecieID(spc_id),
		m_nActiveTraktID(trakt_id)
{
	m_bSetAsFunction = TRUE;	
}

CHandleSettingsDlg::~CHandleSettingsDlg()
{
}

void CHandleSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHandleSettingsDlg)
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);

	//}}AFX_DATA_MAP
}

INT_PTR CHandleSettingsDlg::DoModal()
{
	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL CHandleSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CRect rect;
	GetClientRect(&rect);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			CString sTmpCap;
			sTmpCap.Format(_T("%s - %s"),xml.str(IDS_STRING11),m_sSpecieName);
			SetWindowText(sTmpCap);

			m_wndBtnOK.SetWindowText(xml.str(IDS_STRING155));
			m_wndBtnCancel.SetWindowText(xml.str(IDS_STRING156));

			m_sFunctions	= (xml.str(IDS_STRING258));
			m_sHeight	= (xml.str(IDS_STRING2580));
			m_sCalcHgtAs	= (xml.str(IDS_STRING2581));
			m_sVolume = (xml.str(IDS_STRING2582));
			m_sCalcVolAs = (xml.str(IDS_STRING2583));
			m_sBark = (xml.str(IDS_STRING2584));
			m_sBarkSerie = (xml.str(IDS_STRING2585));
			m_sVolumeUB = (xml.str(IDS_STRING2586));
			m_sCalcVolAsUB = (xml.str(IDS_STRING2587));
			m_sMiscSettings = (xml.str(IDS_STRING259));
			m_sSk_Ub = (xml.str(IDS_STRING2596));
			m_sSk_Fub = (xml.str(IDS_STRING2590));
			m_sFub_To = (xml.str(IDS_STRING2591));
			m_sGreenCrown = (xml.str(IDS_STRING267));
			m_sDefH25 = (xml.str(IDS_STRING4204));
			m_sQualDesc = (xml.str(IDS_STRING292));
			m_sTransport = (xml.str(IDS_STRING2597));
			m_sTranspDist1 = (xml.str(IDS_STRING2598));
			m_sTranspDist2 = (xml.str(IDS_STRING2599));
			m_sCalcQDescAs = (xml.str(IDS_STRING2920));
			m_sGrotFunctions = xml.str(IDS_STRING305);
			m_sCalcGrotAs = xml.str(IDS_STRING3050);
			m_sGrotPercent = xml.str(IDS_STRING3051);
			m_sGrotPrice = xml.str(IDS_STRING3052);
			m_sGrotCost = xml.str(IDS_STRING3053);

			m_sMsgQDescMissing = xml.str(IDS_STRING3054);
			m_sCapMsg = xml.str(IDS_STRING289);
			m_sarrValidateMsg.Add(xml.str(IDS_STRING2410));
			m_sarrValidateMsg.Add(xml.str(IDS_STRING2411));
			m_sarrValidateMsg.Add(xml.str(IDS_STRING2412));
			m_sarrValidateMsg.Add(xml.str(IDS_STRING2413));


			m_sArrNoHgtFunc	= (xml.str(IDS_STRING2610));
			m_sArrNoVolFunc	= (xml.str(IDS_STRING2611));
			m_sArrNoBarkFunc	= (xml.str(IDS_STRING2612));
			m_sArrNoVolUbFunc	= (xml.str(IDS_STRING2613));
			m_sMsgNoSubFuncChosen= (xml.str(IDS_STRING2614));


		}
		xml.clean();
	}

	m_bHgtFuncChanged = FALSE;
	m_bVolFuncChanged = FALSE;
	m_bBarkFuncChanged = FALSE;
	m_bVolUBFuncChanged = FALSE;
	m_bQDescFuncChanged = FALSE;
	m_bGrotFuncChanged = FALSE;

	if (!m_wndPropertyGrid.m_hWnd)
	{
		m_wndPropertyGrid.Create(CRect(5, 5, rect.right-5, rect.bottom-40), this, ID_SPECIE_PROPGRID);
		m_wndPropertyGrid.SetOwner(this);
		m_wndPropertyGrid.ShowHelp( FALSE );
		m_wndPropertyGrid.SetViewDivider(0.45);
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
	}

	// Get information of selected data (e.g. Volume-,Height-,Bark-functoions etc); 070504 p�d
	getTraktSetSpc();

	setupFunctionData(TRUE);

	return TRUE;
}
//*********************************************************
// CHandleSettingsDlg message handlers
//*********************************************************

LRESULT CHandleSettingsDlg::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
		if (pItem->GetID() == ID_HGT_TYPEOF_FUNC_CB)
		{
			setupHeightfunctionsInPropertyGrid(pItem,TRUE);
			//#4665 20151118 J�
			//m_bHgtFuncChanged = TRUE;
		}
		if (pItem->GetID() == ID_HGT_FUNC_CB)
		{
			getIndexForFunctionSet(pItem,&m_nIndexHgtFunc);
			m_bHgtFuncChanged = TRUE;
		}

		if (pItem->GetID() == ID_VOL_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsInPropertyGrid(pItem,TRUE);
			//#4665 20151118 J�
			//m_bVolFuncChanged = TRUE;
		}
		if (pItem->GetID() == ID_VOL_FUNC_CB)
		{
			getIndexForFunctionSet(pItem,&m_nIndexVolFunc);
			m_bVolFuncChanged = TRUE;
		}

		if (pItem->GetID() == ID_BARK_TYPEOF_FUNC_CB)
		{
			setupBarkseriesInPropertyGrid(pItem,TRUE);
			//#4665 20151118 J�
			//m_bBarkFuncChanged = TRUE;
		}
		
		if (pItem->GetID() == ID_BARK_FUNC_CB)
		{
			getIndexForFunctionSet(pItem,&m_nIndexBarkFunc);
			m_bBarkFuncChanged = TRUE;
		}

		if (pItem->GetID() == ID_VOL_UB_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsUBInPropertyGrid(pItem,TRUE);
			//#4665 20151118 J�
			//m_bVolUBFuncChanged = TRUE;
			//m_bSetAsFunction = TRUE;
		}
		if (pItem->GetID() == ID_VOL_UB_FUNC_CB)
		{
			getIndexForFunctionSet(pItem,&m_nIndexVolUBFunc);
			m_bVolUBFuncChanged = TRUE;
			m_bSetAsFunction = TRUE;	
		}
		if (pItem->GetID() == ID_GROT_TYPEOF_FUNC_CB)
		{
			getIndexForFunctionSet(pItem,&m_nIndexGrotFunc);
			//m_bGrotFuncChanged = TRUE;
			//m_bSetAsFunction = TRUE;	
		}
		if (pItem->GetID() == ID_M3SK_TO_M3UB)
		{
			m_nIndexVolUBFunc = m_vecVolUBFuncList.size() - 1;
			m_bVolUBFuncChanged = TRUE;
			m_bSetAsFunction = FALSE;	
		}
		if (pItem->GetID() == ID_QUALDESC)
		{
			getIndexForFunctionSet(pItem,&m_nIndexQDesc);
			m_bQDescFuncChanged = TRUE;
		}
	}
	if (wParam == XTP_PGN_RCLICK)
	{
			// Show a Popupmenu on rightclick; 070502 p�d
			CPoint curPos;
			CMenu menu;
			CString sMenu1;
			if (fileExists(m_sLangFN))
			{
				RLFReader *xml = new RLFReader;
				if (xml->Load(m_sLangFN))
				{
					sMenu1 = (xml->str(IDS_STRING261));
				}	// if (xml->Load(m_sLangFN))
				delete xml;
			}
			VERIFY(menu.CreatePopupMenu());
			GetCursorPos(&curPos);
			// create main menu items
			menu.AppendMenu(MF_STRING, ID_POPUPMENU_EXPAND_COLLAPSE, (sMenu1));
			// track menu
			int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
									curPos.x, curPos.y,this, NULL);
			// other general items
			switch (nMenuResult)
			{
				case ID_POPUPMENU_EXPAND_COLLAPSE :
				{
					if (pItem->IsExpanded())
						pItem->Collapse();
					else
						pItem->Expand();
					break;
				}
			}	// switch (nMenuResult)
	}
	return FALSE;
}

void CHandleSettingsDlg::getIndexForFunctionSet(CXTPPropertyGridItem *pItem,UINT* idx)
{
	// Check, to make sure we have something to work with; 070503 p�d
	if (pItem != NULL)
	{
		CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	

		if (pConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
			if (pConstraint != NULL)
			{
				*idx = pConstraint->m_dwData;
			}	// if (pConstraint != NULL)
		}	// if (pConstraints != NULL)
	}	// if (pItem != NULL)
}

// Get settings for species in this Trakt; 070504 p�d
void CHandleSettingsDlg::getTraktSetSpc(void)
{
	if (m_pDB != NULL)
	{
		m_vecTransactionTraktSetSpc.clear();
		// Only select data for This trakt; 070504 p�d
		 m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,getActiveTraktData()->getTDataTraktID());
	}	// if (m_pDB != NULL)
}

// Get data from m_vecTransactionTraktSetSpc; 070504 p�d
BOOL CHandleSettingsDlg::getActiveTypeOfFunction(int which,CString &s,UINT* index,int* identifer)
{
	BOOL bReturn = FALSE;
	int nFuncID;
	UINT j;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *recTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == recTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == recTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					for (j = 0;j < m_vecHgtFunc.size();j++)
					{
						UCFunctions funcs = m_vecHgtFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecHgtFunc.size();j++)
				}
				else if (which == ID_VOL_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					for (j = 0;j < m_vecVolFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
				else if (which == ID_BARK_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					for (j = 0;j < m_vecBarkFunc.size();j++)
					{
						UCFunctions funcs = m_vecBarkFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecBarkFunc.size();j++)
				}
				else if (which == ID_VOL_UB_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					for (j = 0;j < m_vecVolUBFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolUBFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
				else if (which == ID_GROT_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getGrotFuncID();
					for (j = 0;j < m_vecGROTFunc.size();j++)
					{
						UCFunctions funcs = m_vecGROTFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
			}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
		} // for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
	}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)

	return bReturn;

}

CString CHandleSettingsDlg::getActiveFunction(int which)
{
	CString sInfo,sTmp;
	UINT j;
	int nSpcID;
	int nFuncID;
	int nFuncIndex;
	BOOL bFound = FALSE;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		// getActiveTraktDataOnClick() = Get data from record m_recTraktDataOnClick set
		// in CPageTwoFormView on OnReportItemClick.
		// If this fails, we try to get data deom m_recTraktDataActive, set on
		// populateData in CPageTwoFormView; 081128 p�d
		CTransaction_trakt_data *pTData = getActiveTraktDataOnClick();
		if (pTData == NULL)
			pTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == pTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					nSpcID	= recSetSpc.getHgtFuncSpcID();
					nFuncIndex = recSetSpc.getHgtFuncIndex();
					for (j = 0;j < m_vecHgtFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecHgtFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s , %s"),flist.getSpcName(),flist.getFuncArea());
							return sInfo;
						}	// if (fflist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncflist.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					nSpcID	= recSetSpc.getVolFuncSpcID();
					nFuncIndex = recSetSpc.getVolFuncIndex();
					for (j = 0;j < m_vecVolFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecVolFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_BARK_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					nSpcID	= recSetSpc.getBarkFuncSpcID();
					nFuncIndex = recSetSpc.getBarkFuncIndex();
					for (j = 0;j < m_vecBarkFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecBarkFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s, %s"),flist.getSpcName(),flist.getFuncArea());
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_UB_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					nSpcID	= recSetSpc.getVolUBFuncSpcID();
					nFuncIndex = recSetSpc.getVolUBFuncIndex();
					for (j = 0;j < m_vecVolUBFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecVolUBFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
			}	// if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
		}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
	
	}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	return _T("");
}

void CHandleSettingsDlg::setupFunctionData(BOOL setup_as_new)
{
		CXTPPropertyGridItem* pSettings  = NULL;
		//===============================================================================
		// CATEGORY; Functions
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory(m_sFunctions);
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sFunctions);

		if (pSettings != NULL)
		{
			// Item:Heights
			setupTypeOfHeightfunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item:Volumes m3sk
			setupTypeofVolumefunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item: Bark
			setupTypeOfBarkfunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item:Volumes m3ub
			setupTypeofVolumefunctionsUBInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; GROT 2010-03-12 P�D
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory(m_sGrotFunctions);
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sGrotFunctions);

		if (pSettings != NULL)
		{
			// Item: Grot select functions
			setupGrotFunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item: Grot "uttagsprocent"
			setupGrotPercentInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "Kvalitetsbeskrivning"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sQualDesc));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sQualDesc);

		if (pSettings != NULL)
		{
			// Item: "Kvalitetsbeskrivning(ar)"
			setupQualityDescInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "�vriga inst�llningar"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sMiscSettings));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sMiscSettings);

		if (pSettings != NULL)
		{
			// Item: "Omf�ringstal"
			setupMiscSettingsValuesInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "�vriga inst�llningar"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sTransport));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sTransport);

		if (pSettings != NULL)
		{
			setupMiscSettings2ValuesInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}
}


void CHandleSettingsDlg::setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	CString sInfo;
	int nActiveFuncIndex;
	// get volume functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getHeightFunctions(m_vecHgtFunc,m_vecHgtFuncList);
	if (m_vecHgtFunc.size() > 0)
	{
		CXTPPropertyGridItem *pHgtItem = NULL;
		if (setup_as_new)
			pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sHeight)));
		else
			pHgtItem = pItem->GetChilds()->FindItem(m_sHeight);
		if (pHgtItem != NULL)
		{
			pHgtItem->BindToString(&m_sHgtTypeOfBindStr);

			for (UINT j = 0;j < m_vecHgtFunc.size();j++)
			{
				UCFunctions flist = m_vecHgtFunc[j];
				sInfo.Format(_T("%s"),flist.getName());
				pHgtItem->GetConstraints()->AddConstraint((sInfo));
			}	// for (UINT j = 0;j < func_list.size();j++)
			pHgtItem->SetFlags(xtpGridItemHasComboButton);
			pHgtItem->SetConstraintEdit( FALSE );
			pHgtItem->SetID(ID_HGT_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

			if (getActiveTypeOfFunction(ID_HGT_TYPEOF_FUNC_CB,m_sHgtTypeOfBindStr,&m_nIndexHgtFunc,&nActiveFuncIndex))
			{
				pHgtItem->GetConstraints()->SetCurrent(m_nIndexHgtFunc);
				setupHeightfunctionsInPropertyGrid(pHgtItem,FALSE);
			}	// if (getActiveTypeOfFunction(ID_HGT_TYPEOF_FUNC_CB,m_sHgtTypeOfBindStr,&nIndex))
		}	// if (pHgtItem != NULL)
	}	// if (m_vecHgtFunc.size() > 0)

}

// This function is called from OnDockingPaneNotify
void CHandleSettingsDlg::setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		CString sInfo;
		int nActiveFuncIndex;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions(m_vecVolFunc,m_vecVolFuncList);
		if (m_vecVolFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolItem = NULL;
			if (setup_as_new)
				pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sVolume)));
			else
				pVolItem = pItem->GetChilds()->FindItem(m_sVolume);

			if (pVolItem != NULL)
			{
				pVolItem->BindToString(&m_sVolTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolFunc.size();j++)
				{
					UCFunctions flist = m_vecVolFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolItem->SetFlags(xtpGridItemHasComboButton);
				pVolItem->SetConstraintEdit( FALSE );
				pVolItem->SetID(ID_VOL_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&m_nIndexVolFunc,&nActiveFuncIndex))
				{
					pVolItem->GetConstraints()->SetCurrent(m_nIndexVolFunc);
					setupVolumefunctionsInPropertyGrid(pVolItem,FALSE);
				}	// if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&nIndex))
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify
void CHandleSettingsDlg::setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		int nActiveFuncIndex;
		CString sInfo;
		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getBarkFunctions(m_vecBarkFunc,m_vecBarkFuncList);
		if (m_vecBarkFunc.size() > 0)
		{
			CXTPPropertyGridItem *pBarkFuncItem = NULL;
			if (setup_as_new)
				pBarkFuncItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sBark)));
			else
				pBarkFuncItem = pItem->GetChilds()->FindItem(m_sBark);
			if (pBarkFuncItem != NULL)
			{
				pBarkFuncItem->BindToString(&m_sBarkTypeOfBindStr);

				for (UINT j = 0;j < m_vecBarkFunc.size();j++)
				{
					UCFunctions flist = m_vecBarkFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pBarkFuncItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pBarkFuncItem->SetFlags(xtpGridItemHasComboButton);
				pBarkFuncItem->SetConstraintEdit( FALSE );
				pBarkFuncItem->SetID(ID_BARK_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_BARK_TYPEOF_FUNC_CB,m_sBarkTypeOfBindStr,&m_nIndexBarkFunc,&nActiveFuncIndex))
				{
					pBarkFuncItem->GetConstraints()->SetCurrent(m_nIndexBarkFunc);
					setupBarkseriesInPropertyGrid(pBarkFuncItem,FALSE);
				}	// if (getActiveTypeOfFunction(ID_BARK_TYPEOF_FUNC_CB,m_sBarkTypeOfBindStr,&nIndex))
			}	// if (pBarkFuncItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify
void CHandleSettingsDlg::setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		CString sInfo;
		int nActiveFuncIndex;
		UCFunctions flist;
		UCFunctions flist_active_func;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions_ub(m_vecVolUBFunc,m_vecVolUBFuncList);
		if (m_vecVolUBFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolUBItem = NULL;
			if (setup_as_new)
				pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sVolumeUB)));
			else
				pVolUBItem = pItem->GetChilds()->FindItem(m_sVolumeUB);
			if (pVolUBItem != NULL)
			{
				pVolUBItem->BindToString(&m_sVolUBTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolUBFunc.size();j++)
				{
					flist = m_vecVolUBFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolUBItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolUBItem->SetFlags(xtpGridItemHasComboButton);
				pVolUBItem->SetConstraintEdit( FALSE );
				pVolUBItem->SetID(ID_VOL_UB_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_VOL_UB_TYPEOF_FUNC_CB,m_sVolUBTypeOfBindStr,&m_nIndexVolUBFunc,&nActiveFuncIndex))
				{
					pVolUBItem->GetConstraints()->SetCurrent(m_nIndexVolUBFunc);
					setupVolumefunctionsUBInPropertyGrid(pVolUBItem,FALSE);
				}	// if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&nIndex))
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}


void CHandleSettingsDlg::setupGrotFunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		CString sInfo;
		int nActiveFuncIndex;
		UCFunctions flist;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getGROTFunctions(m_vecGROTFunc);
		if (m_vecGROTFunc.size() > 0)
		{
			CXTPPropertyGridItem *pGrotItem = NULL;
			if (setup_as_new)
				pGrotItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcGrotAs)));
			else
				pGrotItem = pItem->GetChilds()->FindItem(m_sCalcGrotAs);
			if (pGrotItem != NULL)
			{
				pGrotItem->BindToString(&m_sGrotFuncBindStr);

				for (UINT j = 0;j < m_vecGROTFunc.size();j++)
				{
					flist = m_vecGROTFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pGrotItem->GetConstraints()->AddConstraint(sInfo,j);
				}	// for (UINT j = 0;j < func_list.size();j++)
				pGrotItem->SetFlags(xtpGridItemHasComboButton);
				pGrotItem->SetConstraintEdit( FALSE );
				pGrotItem->SetID(ID_GROT_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_GROT_TYPEOF_FUNC_CB,m_sGrotFuncBindStr,&m_nIndexGrotFunc,&nActiveFuncIndex))
				{
					pGrotItem->GetConstraints()->SetCurrent(m_nIndexGrotFunc);
				}	// if (getActiveTypeOfFunction(ID_GROT_TYPEOF_FUNC_CB,m_sGrotTypeOfBindStr,&m_nIndexGrotFunc,&nActiveFuncIndex))
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}

void CHandleSettingsDlg::setupGrotPercentInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	if (setup_as_new)
	{
		m_fGrotPercent = 0.0;
		CXTPPropertyGridItemDouble *pGrotPerc = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotPercent),0.0,_T("%.0f")));
		pGrotPerc->SetID(ID_GROT_PERCENT);
		pGrotPerc->BindToDouble(&m_fGrotPercent);

		m_fGrotPrice = 0.0;
		CXTPPropertyGridItemDouble *pGrotPrice = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotPrice),0.0,_T("%.0f")));
		pGrotPrice->SetID(ID_GROT_PRICE);
		pGrotPrice->BindToDouble(&m_fGrotPrice);

		m_fGrotCost = 0.0;
		CXTPPropertyGridItemDouble *pGrotCost = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotCost),0.0,_T("%.0f")));
		pGrotCost->SetID(ID_GROT_COST);
		pGrotCost->BindToDouble(&m_fGrotCost);
	}

	getMiscData(2);
}

// This function is called from OnDockingPaneNotify

void CHandleSettingsDlg::setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	getQualDescData(&m_nIndexQDesc,m_listQDesc);
	if (m_listQDesc.GetCount() > 0)
	{
		CXTPPropertyGridItem *pQDescItem = NULL;
		if (setup_as_new)
			pQDescItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcQDescAs)));
		else
			pQDescItem = pItem->GetChilds()->FindItem(m_sCalcQDescAs);
		if (pQDescItem != NULL)
		{
			pQDescItem->BindToString(&m_sQDescBindStr);

			for (int i = 0;i < m_listQDesc.GetCount();i++)
			{
					pQDescItem->GetConstraints()->AddConstraint((m_listQDesc[i]),i);
			}	// for (UINT j = 0;j < func_list.size();j++)
			pQDescItem->SetFlags(xtpGridItemHasComboButton);
			pQDescItem->SetConstraintEdit( FALSE );
			pQDescItem->SetID(ID_QUALDESC);	// Set ID in PropertyGrid; 070521 p�d
		}
		// Make sure we're inside the array; 070521 p�d
		if (m_nIndexQDesc >= 0 && m_nIndexQDesc < m_listQDesc.GetCount())
		{
			m_sQDescBindStr = m_listQDesc[m_nIndexQDesc];
		}
	}
}

// "Omf�ringstal"
void CHandleSettingsDlg::setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	if (setup_as_new)
	{
/*	Commented out 2009-06-02 P�D
		NOT USED, "Omf�ringstal f�r m3sk till m3fub", use "gagnvrkesvolym m3fub"; 090602 p�d
		CXTPPropertyGridItemDouble *pM3Sk_M3Fub = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sSk_Fub),0.0,_T("%.2f")));
		pM3Sk_M3Fub->SetID(ID_M3SK_TO_M3FUB);
		pM3Sk_M3Fub->BindToDouble(&m_fM3SkToM3Fub);
*/
		m_fM3SkToM3Fub = 0.0;
		CXTPPropertyGridItemDouble *pM3Fub_M3To = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sFub_To),0.0,_T("%.2f")));
		pM3Fub_M3To->SetID(ID_M3FUB_TO_M3TO);
		pM3Fub_M3To->BindToDouble(&m_fM3FubToM3To);

		// #4513: Default H25
		m_nDefH25 = 0;
		CXTPPropertyGridItemNumber *pDefH25 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sDefH25)));
		pDefH25->SetID(ID_DEF_H25);
		pDefH25->BindToNumber(&m_nDefH25);

		m_nGreenCrown = 0.0;
		CXTPPropertyGridItemNumber *pGreenCrown = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sGreenCrown)));
		pGreenCrown->SetID(ID_MISC_GREEN_CROWN);
		pGreenCrown->BindToNumber(&m_nGreenCrown);
	}

	getMiscData(2);
}

void CHandleSettingsDlg::setupMiscSettings2ValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	if (setup_as_new)
	{
		CXTPPropertyGridItemNumber *pTranspDist1 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTranspDist1)));
		pTranspDist1->SetID(ID_TRANSP_DIST1);
		pTranspDist1->BindToNumber(&m_nTranspDist1);

		CXTPPropertyGridItemNumber *pTranspDist2 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTranspDist2)));
		pTranspDist2->SetID(ID_TRANSP_DIST2);
		pTranspDist2->BindToNumber(&m_nTranspDist2);
	}

	getMiscData(2);
}

// This function is called from OnGridNotify
void CHandleSettingsDlg::setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pHgtItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer;
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcHgtAs)));
		pHgtItem->BindToString(&m_sHgtBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pHgtItem = pGridChilds->GetAt(0);
			m_sHgtBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pHgtItem != NULL)
	{
		pItem->Expand();
		pHgtItem->GetConstraints()->RemoveAll();
		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecHgtFunc.size())
		{
			nFunctionIdentifer = m_vecHgtFunc[nIndex].getIdentifer();
		}

		if (m_vecHgtFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecHgtFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecHgtFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
					sInfo.Format(_T("%s , %s"),list.getSpcName(),list.getFuncArea());
					pHgtItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pHgtItem->SetFlags(xtpGridItemHasComboButton);
		pHgtItem->SetConstraintEdit( FALSE );
		pHgtItem->SetID(ID_HGT_FUNC_CB);
		//#4636 20151110 J�
		if(bOnchange)
		{
			//#4668 20151123 J�
			m_sHgtBindStr=_T("");
			m_nIndexHgtFunc = -1;
			pItem->GetGrid()->Refresh();

			/*
			m_sHgtBindStr=pHgtItem->GetConstraints()->GetAt(0);
			pHgtItem->GetConstraints()->SetCurrent(0);
			pItem->GetGrid()->Refresh();
			//#4665 20151118 J�
			m_nIndexHgtFunc = pHgtItem->GetConstraints()->GetConstraintAt(pHgtItem->GetConstraints()->GetCurrent())->m_dwData;	
			*/
		}
		else
			m_sHgtBindStr = getActiveFunction(ID_HGT_FUNC_CB);	

	} // 	if (pHgtItem != NULL)

}

// This function is called from OnGridNotify
void CHandleSettingsDlg::setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pVolItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcVolAs)));
		pVolItem->BindToString(&m_sVolBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pVolItem = pGridChilds->GetAt(0);
			m_sVolBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pVolItem != NULL)
	{
		pItem->Expand();
		pVolItem->GetConstraints()->RemoveAll();
		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecVolFunc.size())
		{
			nFunctionIdentifer = m_vecVolFunc[nIndex].getIdentifer();
		}

		if (m_vecVolFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecVolFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
					pVolItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pVolItem->SetFlags(xtpGridItemHasComboButton);
		pVolItem->SetConstraintEdit( FALSE );
		pVolItem->SetID(ID_VOL_FUNC_CB);
		//#4636 20151110 J�
		if(bOnchange)
		{
			//#4668 20151123 J�
			m_sVolBindStr=_T("");
			m_nIndexVolFunc=-1;
			pItem->GetGrid()->Refresh();

			/*m_sVolBindStr=pVolItem->GetConstraints()->GetAt(0);
			pVolItem->GetConstraints()->SetCurrent(0);
			pItem->GetGrid()->Refresh();
			//#4665 20151118 J�
			m_nIndexVolFunc = pVolItem->GetConstraints()->GetConstraintAt(pVolItem->GetConstraints()->GetCurrent())->m_dwData;	*/
		}
		else
			m_sVolBindStr = getActiveFunction(ID_VOL_FUNC_CB); 


	} // 	if (pBarkItem != NULL)

}

// This function is called from OnGridNotify
void CHandleSettingsDlg::setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bOnchange)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pBarkItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)

	if (!pItem->HasChilds())
	{
		pBarkItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sBarkSerie)));
		pBarkItem->BindToString(&m_sBarkBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pBarkItem = pGridChilds->GetAt(0);
			m_sBarkBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pBarkItem != NULL)
	{
		pItem->Expand();
		pBarkItem->GetConstraints()->RemoveAll();

		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecBarkFunc.size() )
		{
			nFunctionIdentifer = m_vecBarkFunc[nIndex].getIdentifer();
		}
		if (m_vecBarkFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecBarkFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecBarkFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
					sInfo.Format(_T("%s, %s"),
											list.getSpcName(),
											list.getFuncArea());
					pBarkItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pBarkItem->SetFlags(xtpGridItemHasComboButton);
		pBarkItem->SetConstraintEdit( FALSE );
		pBarkItem->SetID(ID_BARK_FUNC_CB);
		//#4636 20151110 J�
		if(bOnchange)
		{
			//#4668 20151123 J�
			m_sBarkBindStr=_T("");
			pItem->GetGrid()->Refresh();
			m_nIndexBarkFunc = -1;


			/*m_sBarkBindStr=pBarkItem->GetConstraints()->GetAt(0);
			pBarkItem->GetConstraints()->SetCurrent(0);
			pItem->GetGrid()->Refresh();
			//#4665 20151118 J�
			m_nIndexBarkFunc = pBarkItem->GetConstraints()->GetConstraintAt(pBarkItem->GetConstraints()->GetCurrent())->m_dwData;	*/
		}
		else
			m_sBarkBindStr = getActiveFunction(ID_BARK_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

// This function is called from OnGridNotify
void CHandleSettingsDlg::setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL bonChange)
{
	BOOL bFuncChanged = FALSE;
	CString sInfo,sTmp,S;
	CString sActiveFuncName;
	UINT nActiveIndex;
	int nActiveIdentifer;
	UINT nIndex;
	CXTPPropertyGridItem *pVolUBItem = NULL;
	CMyPropGridItemDouble_validate *pM3Sk_M3Fub = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	if (pItem == NULL) return;

	if (!pItem->HasChilds())
	{
			pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcVolAsUB)));
			pVolUBItem->SetID(ID_VOL_UB_FUNC_CB);
			pVolUBItem->BindToString(&m_sVolUBBindStr);
			pVolUBItem->SetFlags(xtpGridItemHasComboButton);
			pVolUBItem->SetConstraintEdit( FALSE );
			
			pM3Sk_M3Fub = (CMyPropGridItemDouble_validate*)pItem->AddChildItem(new CMyPropGridItemDouble_validate((m_sSk_Ub),_T("%.2f"),m_sCapMsg,m_sarrValidateMsg,1.0,VALUE_LE));
			pM3Sk_M3Fub->SetID(ID_M3SK_TO_M3UB);
			pM3Sk_M3Fub->BindToDouble(&m_fM3SkToM3Ub);
	}

	// Get Volume function set; 081204 p�d
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL) nIndex = pItemConstraints->GetCurrent();
	
	// Get identifer of Function selected; 070416 p�d
	if (nIndex >= 0 && nIndex < m_vecVolUBFunc.size())
	{
		nFunctionIdentifer = m_vecVolUBFunc[nIndex].getIdentifer();

		getActiveTypeOfFunction(ID_VOL_UB_TYPEOF_FUNC_CB,sActiveFuncName,&nActiveIndex,&nActiveIdentifer);
		bFuncChanged = (nActiveIdentifer != nFunctionIdentifer);

		CXTPPropertyGridItems *pChilds = pItem->GetChilds();
		if (pChilds != NULL)
		{
			if (nFunctionIdentifer == ID_BRANDELS_UB || 
				  nFunctionIdentifer == ID_NASLUNDS_UB ||
				  nFunctionIdentifer == ID_HAGBERG_MATERN_UB)
			{
				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL)
				{
					pVolFuncUBFound->GetConstraints()->RemoveAll();
					if (m_vecVolUBFuncList.size() > 0)
					{
						for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
						{
							sInfo.Empty();
							UCFunctionList list = m_vecVolUBFuncList[i];
							// Check the Function identifer; 070416 p�d
							if (list.getID() == nFunctionIdentifer)
							{
								if (_tcslen(list.getSpcName()) > 0)
								{
									sTmp.Format(_T("%s"),list.getSpcName());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncType()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncType());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncArea()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncArea());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncDesc()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncDesc());					
									sInfo += sTmp;
								}

								pVolFuncUBFound->GetConstraints()->AddConstraint((sInfo),i);			
							}	// if (list.getID() == nFunctionIdentifer)
						}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
					}	// if (m_vecVolFuncList.size() > 0)


					if(bonChange)
					{
						//#4668 20151123 J�
						m_sVolUBBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
						m_nIndexVolUBFunc = -1;
						pItem->GetGrid()->Refresh();
						/*
												//#4636 20151110 J�
						m_sVolUBBindStr=pVolFuncUBFound->GetConstraints()->GetAt(0);
						pVolFuncUBFound->GetConstraints()->SetCurrent(0);
						pItem->GetGrid()->Refresh();
						//#4665 20151118 J�
						m_nIndexVolUBFunc = pVolFuncUBFound->GetConstraints()->GetConstraintAt(pVolFuncUBFound->GetConstraints()->GetCurrent())->m_dwData;	
						*/
					}
					else
					{
					/*if (bFuncChanged)
					{
						m_sVolUBBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
						pItem->GetGrid()->Refresh();
					}
					else*/
						m_sVolUBBindStr = getActiveFunction(ID_VOL_UB_FUNC_CB);
					}

					pVolFuncUBFound->SetHidden(FALSE);
				}	// if (pVolFuncUBFound != NULL)

				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(TRUE);
			}
			else if (nFunctionIdentifer == ID_OMF_FACTOR_UB)
			{
				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(FALSE);

				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL) pVolFuncUBFound->SetHidden(TRUE);
			}
		}

		pItem->Expand();
		getMiscData(1);
	}
}


int CHandleSettingsDlg::getQualDescIndex(CXTPPropertyGridItem *pItem,CString& qdesc_name)
{
	CString S;
	CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	

	if (pConstraints != NULL)
	{
		CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
		if (pConstraint != NULL)
		{
			int nIndex = (int)pConstraint->m_dwData;
			if (nIndex >= 0 && nIndex < m_listQDesc.GetCount())
			{
				qdesc_name = m_listQDesc.GetAt(nIndex);
				return nIndex;
			}	// if (nIndex >= 0 && nIndex < m_listQDesc.GetCount())
		}	// if (pConstraint != NULL)
	}	// if (pConstraints != NULL)

	return -1;
}

// Use this instead (no diamtercalss/specie); 070507 p�d
void CHandleSettingsDlg::getQualDescData(UINT *qdesc_index,CStringArray& qdesc_list)
{
	CTransaction_trakt_set_spc recSetSpc;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *pTData = getActiveTraktData();
	if (pTData != NULL)
	{
		// Retrive information from selected pricelist, for active trakt.
		// In thiscase, we'll get info. on Qualitydescription(s); 070521 p�d
		getTraktMiscDataFromDB();

		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			// First find id's and indexes; 070504 p�d
			for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
			{
				recSetSpc = m_vecTransactionTraktSetSpc[i];
				// Find Settings info for This trakt; 070504 p�d
				if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
						recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
						recSetSpc.getTSetspcID() == pTData->getSpecieID())
				{
					*qdesc_index = recSetSpc.getSelQDescIndex();
				}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
			}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
		}	// if (m_vecTransactionTraktSetSpc.size() > 0)
/*
		PricelistParser *pPrlParser = new PricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get qualitydescriptions for pricelist; 070521 p�d
				pPrlParser->getQualDescNameForSpecie(pTData->getSpecieID(),m_listQDesc);
			}
			delete pPrlParser;
		}
*/
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get qualitydescriptions for pricelist; 070521 p�d
				pPrlParser->getQualDescNameForSpecie(pTData->getSpecieID(),m_listQDesc);
			}
			delete pPrlParser;
		}

	}
}

// Get pricelist for trakt in esti_trakt_pricelist_table; 070430 p�d
void CHandleSettingsDlg::getTraktMiscDataFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTraktMiscData(rec->getTraktID(),m_recTraktMiscData);
	}	// if (m_pDB != NULL)
}
/*
void CHandleSettingsDlg::getTraktFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTrakt(rec->getTraktID(),m_recTrakt);
	}	// if (m_pDB != NULL)
}
*/

// Use this instead (no diamtercalss/specie); 070507 p�d
BOOL CHandleSettingsDlg::getMiscData(int do_as)
{
	BOOL bReturn = FALSE;
	CString sExtraInfo;
	CStringArray args;
	
	CTransaction_trakt_set_spc recSetSpc;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt *pTrakt = getActiveTrakt();
	CTransaction_trakt_data *pTData = NULL;
	
	// getActiveTraktDataOnClick() = Get data from record m_recTraktDataOnClick set
	// in CPageTwoFormView on OnReportItemClick.
	// If this fails, we try to get data deom m_recTraktDataActive, set on
	// populateData in CPageTwoFormView; 081128 p�d
	pTData = getActiveTraktDataOnClick();
	if (pTData == NULL)
		pTData = getActiveTraktData();


	if (pTrakt != NULL)
	{
		m_bIsAtCoast = (pTrakt->getTraktNearCoast() ? TRUE : FALSE);
		m_bIsSouthEast = (pTrakt->getTraktSoutheast() ? TRUE : FALSE);
		m_bIsRegion5 = (pTrakt->getTraktRegion5() ? TRUE : FALSE);
		m_bIsPartOfPlot = (pTrakt->getTraktPartOfPlot() ? TRUE : FALSE);
		m_sSIH100_Pine = pTrakt->getTraktSIH100_Pine();
	}
	else
	{
		m_bIsAtCoast = FALSE;
		m_bIsSouthEast = FALSE;
		m_bIsRegion5 = FALSE;
		m_bIsPartOfPlot = FALSE;
		m_sSIH100_Pine =  _T("");
	}

	if (pTData != NULL)
	{
		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			// First find id's and indexes; 070504 p�d
			for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
			{
				recSetSpc = m_vecTransactionTraktSetSpc[i];
				// Find Settings info for This trakt; 070504 p�d
				if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
						recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
						recSetSpc.getTSetspcID() == pTData->getSpecieID())
				{	
					if (do_as == 0 || do_as == 1)
					{
						m_fM3SkToM3Ub = recSetSpc.getM3SkToM3Ub();
					}
					if (do_as == 0 || do_as == 2)
					{
						m_fM3SkToM3Fub = recSetSpc.getM3SkToM3Fub();
						m_nDefH25 = recSetSpc.getDefaultH25();
						m_fM3FubToM3To = recSetSpc.getM3FubToM3To();
						m_nGreenCrown = recSetSpc.getDefaultGreenCrown();
						m_fGrotPercent = recSetSpc.getGrotPercent();
						m_fGrotPrice	 = recSetSpc.getGrotPrice();
						m_fGrotCost		 = recSetSpc.getGrotCost();
						m_nTranspDist1 = recSetSpc.getTranspDist1();
						m_nTranspDist2 = recSetSpc.getTranspDist2();
					}
					break;
				}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
			}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
		}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	}
	return bReturn;
}


void CHandleSettingsDlg::saveSetSpcHgtFuncToDB()
{
	UCFunctionList funcList;
	CString S;
	if (m_vecHgtFuncList.size() > 0 && m_nIndexHgtFunc >= 0 && m_nIndexHgtFunc < m_vecHgtFuncList.size())
	{
		funcList = m_vecHgtFuncList[m_nIndexHgtFunc];
/*
		S.Format(_T("saveSetSpcHgtFuncToDB\nm_nIndexHgtFunc %d\n %s %s %s"),
			m_nIndexHgtFunc,funcList.getFuncArea(),funcList.getFuncDesc(),funcList.getFuncType());
		UMMessageBox(S);
*/
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *pTData = getActiveTraktData();
		// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
		if (m_pDB != NULL && pTData != NULL)
		{

				// Setup record to hold save information for height function; 070503 p�d
				CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																		pTData->getTDataID(),
																																		pTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		pTData->getSpecieID(),
																																		funcList.getID(),
																																		funcList.getSpcID(),
																																		funcList.getIndex(),
																																		0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0,0.0);
			if (!m_pDB->addTraktSetSpc_hgtfunc(rec))
				m_pDB->updTraktSetSpc_hgtfunc(rec);
		}	// if (m_pDB != NULL && pTData != NULL)
	}	// if (m_vecHgtFuncList.size() > 0 && m_nIndexHgtFunc >= 0 && m_nIndexHgtFunc < m_vecHgtFuncList.size())
}

void CHandleSettingsDlg::saveSetSpcVolFuncToDB()
{
	UCFunctionList funcList;
	if (m_vecVolFuncList.size() > 0 && m_nIndexVolFunc >= 0 && m_nIndexVolFunc < m_vecVolFuncList.size())
	{
		funcList = m_vecVolFuncList[m_nIndexVolFunc];
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *pTData = getActiveTraktData();

		// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
		if (m_pDB != NULL && pTData != NULL)
		{
				// Setup record to hold save information for height function; 070503 p�d
				CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																		pTData->getTDataID(),
																																		pTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		pTData->getSpecieID(),
																																		0,0,0,
																																		funcList.getID(),
																																		funcList.getSpcID(),
																																		funcList.getIndex(),
																																		0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0,0.0);
				if (!m_pDB->addTraktSetSpc_volfunc(rec))
					m_pDB->updTraktSetSpc_volfunc(rec);
		}
	}
}

void CHandleSettingsDlg::saveSetSpcBarkFuncToDB()
{
	UCFunctionList funcList;
	if (m_vecBarkFuncList.size() > 0 && m_nIndexBarkFunc >= 0 && m_nIndexBarkFunc < m_vecBarkFuncList.size())
	{
		funcList = m_vecBarkFuncList[m_nIndexBarkFunc];
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *pTData = getActiveTraktData();
		// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
		if (m_pDB != NULL && pTData != NULL)
		{
							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																					pTData->getTDataID(),
																																					pTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					pTData->getSpecieID(),
																																					0,0,0,0,0,0,
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0,0,0,0.0,
																																					0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0,0.0);
				if (!m_pDB->addTraktSetSpc_barkfunc(rec))
					m_pDB->updTraktSetSpc_barkfunc(rec);
		}
	}
}

void CHandleSettingsDlg::saveSetSpcVolUBFuncToDB()
{
	UCFunctionList funcList;
	if (m_vecVolUBFuncList.size() > 0 && m_nIndexVolUBFunc >= 0 && m_nIndexVolUBFunc < m_vecVolUBFuncList.size())
	{
		funcList = m_vecVolUBFuncList[m_nIndexVolUBFunc];
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *pTData = getActiveTraktData();
		// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
		if (m_pDB != NULL && pTData != NULL)
		{
							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																					pTData->getTDataID(),
																																					pTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					pTData->getSpecieID(),
																																					0,0,0,0,0,0,0,0,0,
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0,0.0);
				if (!m_pDB->addTraktSetSpc_volfunc_ub(rec))
					m_pDB->updTraktSetSpc_volfunc_ub(rec);
		}
	}
}

void CHandleSettingsDlg::saveSetSpcVolUBFuncToDB_sk_ub()
{
	BOOL bFound = FALSE;
	UCFunctionList funcList;
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecVolUBFuncList.size() > 0)
	{
		// Try to find idenifer for function "Omf. tal" sk to ub; 071022 p�d
		for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
		{
			funcList = m_vecVolUBFuncList[i];	
			if (funcList.getID() == ID_OMF_FACTOR_UB)
			{
				bFound = TRUE;
				break;
			}	// if (funcList.getID() == ID_OMF_FACTOR_UB)
		}	// for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
		if (bFound)
		{
			// Get active trakt record; holds information on trakt-id; 070430 p�d
			CTransaction_trakt_data *pTData = getActiveTraktData();

			// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
			if (m_pDB != NULL && pTData != NULL)
			{

					// Setup record to hold save information for height function; 070503 p�d
					CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																			pTData->getTDataID(),
																																			pTData->getTDataTraktID(),
																																			STMP_LEN_WITHDRAW,
																																			pTData->getSpecieID(),
																																			0,0,0,0,0,0,0,0,0,
																																			funcList.getID(),
																																			funcList.getSpcID(),
																																			funcList.getIndex(),
																																			m_fM3SkToM3Ub,
																																			0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0,0.0);
					if (!m_pDB->addTraktSetSpc_volfunc_ub(rec))
						m_pDB->updTraktSetSpc_volfunc_ub(rec);
			}
		}
	}	// if (m_vecVolFuncList.size() > 0)
}



void CHandleSettingsDlg::saveSetSpcGrotFuncToDB()
{
	BOOL bFound = FALSE;
	UCFunctions funcList;
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecGROTFunc.size() > 0 && m_nIndexGrotFunc >= 0 && m_nIndexGrotFunc < m_vecGROTFunc.size())
	{
		funcList = m_vecGROTFunc[m_nIndexGrotFunc];
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *pTData = getActiveTraktData();

		// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
		if (m_pDB != NULL && pTData != NULL)
		{

				// Setup record to hold save information for height function; 070503 p�d
				CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																		pTData->getTDataID(),
																																		pTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		pTData->getSpecieID(),
																																		0,0,0,0,0,0,0,0,0,0,0,0,0.0,
																																		0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),
																																		funcList.getIdentifer(),
																																		m_fGrotPercent,
																																		m_fGrotPrice,
																																		m_fGrotCost,
																																		0.0,
																																		0.0);
				if (!m_pDB->addTraktSetSpc_grot(rec))
					m_pDB->updTraktSetSpc_grot(rec);
		}
	}	// if (m_vecGROTFunc.size() > 0)
}


BOOL CHandleSettingsDlg::saveSetSpcQDescToDB()
{
	CString sQDescName,S;

	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *pTData = getActiveTraktData();

	// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
	if (m_pDB != NULL && pTData != NULL)
	{

			// Save misc data, depending on selected item in Property; 070531
			// If ID_QUALDESC is selected, we'll also save diamterclass; 070531
			// Get Index of selected Qualitydesc.; 070521 p�d
			// We'll also add the name of the selected Qualitydescription in the
			// "esti_trakt_set_spc_table; tsetspc_qdesc_name"; 090918 p�d
			if (m_nIndexQDesc >= 0 && m_nIndexQDesc < m_listQDesc.GetCount())
			{
				sQDescName = m_listQDesc.GetAt(m_nIndexQDesc);
			}	// if (nIndex >= 0 && nIndex < m_listQDesc.GetCount())
			else
			{
				sQDescName = _T("");
			}

			// Check Qualitydescription is OK; 101012 p�d
			if (sQDescName.IsEmpty() && m_listQDesc.GetCount() > 0)
			{
				UMMessageBox(this->GetSafeHwnd(),m_sMsgQDescMissing,m_sCapMsg,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}

			// Setup record to hold save information for misc data function; 070504 p�d
			CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																	pTData->getTDataID(),
																																	pTData->getTDataTraktID(),
																																	STMP_LEN_WITHDRAW,
																																	pTData->getSpecieID(),
																																	0,0,0,0,0,0,0,0,0,0,0,0.0,0,0.0,0.0,
																																	m_recTraktMiscData.getDiamClass(),
																																	m_nIndexQDesc,_T(""),0,0,_T(""),sQDescName,0,0.0,0.0,0.0,0.0,0.0);
				if (!m_pDB->addTraktSetSpc_rest_of_misc_data(rec))
					m_pDB->updTraktSetSpc_rest_of_misc_data(rec);

	}	// if (m_pDB != NULL)

	return TRUE;
}

void CHandleSettingsDlg::saveSetSpcMiscDataToDB()
{
	CString sExtraInfo;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *pTData = getActiveTraktData();

	// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
	if (m_pDB != NULL && pTData != NULL)
	{
			//---------------------------------------------------------------------------------------------------
			// Setup the "Extra information"; 070627 p�d
			sExtraInfo.Format(_T("%d;%d;%d;%d;%s;"),
						m_bIsAtCoast,
						m_bIsSouthEast,
						m_bIsRegion5,
						m_bIsPartOfPlot,
						m_sSIH100_Pine);
			// Setup record to hold save information for misc data function; 070504 p�d
			CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(pTData->getSpecieID(),
																																	pTData->getTDataID(),
																																	pTData->getTDataTraktID(),
																																	STMP_LEN_WITHDRAW,
																																	pTData->getSpecieID(),
																																	0,0,0,0,0,0,0,0,0,0,0,0,
																																	0.0,
																																	m_fM3SkToM3Fub,
																																	m_fM3FubToM3To,
																																	0.0,0,
																																	sExtraInfo,
																																	m_nTranspDist1,
																																	m_nTranspDist2,
																																	_T(""),_T(""),0,0.0,0.0,0.0,
																																	m_nDefH25,
																																	m_nGreenCrown);
			if (!m_pDB->addTraktSetSpcMiscData(rec))
				m_pDB->updTraktSetSpcMiscData(rec);
			//---------------------------------------------------------------------------------------------------
	}	// if (m_pDB != NULL)
}

void CHandleSettingsDlg::OnBnClickedOk()
{
	BOOL bChanged = FALSE,bdoSave=FALSE;
	CString sErrMsg=_T("");
	if (m_wndPropertyGrid.m_hWnd)
	{
		if (m_wndPropertyGrid.isDirty())
		{
			bChanged = TRUE;
			//#4668 20151123 J� Kontroll om underfunktion vald
			if(m_nIndexVolFunc==-1 || m_nIndexHgtFunc==-1 || m_nIndexBarkFunc==-1 || m_nIndexVolUBFunc==-1)
			{
				if(m_nIndexHgtFunc==-1)
				{
					sErrMsg+=m_sArrNoHgtFunc + _T("\n");
				}

				if(m_nIndexVolFunc==-1)
				{
					sErrMsg+=m_sArrNoVolFunc + _T("\n");
				}

				if(m_nIndexBarkFunc==-1)
				{
					sErrMsg+=m_sArrNoBarkFunc + _T("\n");
				}
				if(m_nIndexVolUBFunc==-1)
				{
					sErrMsg+=m_sArrNoVolUbFunc + _T("\n");
				}
				UMMessageBox(this->GetSafeHwnd(),sErrMsg,m_sMsgNoSubFuncChosen,MB_ICONASTERISK | MB_OK);
			}
			else
			{
				bdoSave=TRUE;
				if (m_bHgtFuncChanged)
					saveSetSpcHgtFuncToDB();
				if (m_bVolFuncChanged)
					saveSetSpcVolFuncToDB();
				if (m_bBarkFuncChanged)
					saveSetSpcBarkFuncToDB();
				if (m_bVolUBFuncChanged)
				{
					if (m_bSetAsFunction)
						saveSetSpcVolUBFuncToDB();
					else
						saveSetSpcVolUBFuncToDB_sk_ub();
				}
				saveSetSpcGrotFuncToDB();

				//if (m_bQDescFuncChanged)
				saveSetSpcQDescToDB();

				saveSetSpcMiscDataToDB();
			}
		}
		else
		{
			OnCancel();
		}
	}
	// TODO: Add your control notification handler code here
	if (bdoSave)	
		OnOK();

}

void CHandleSettingsDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}
