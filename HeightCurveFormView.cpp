// PageFourFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "HeightCurveFormView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

// CHeightCurveFormView

int CHeightCurveFormView::m_nCurCBSel = -1;

IMPLEMENT_DYNCREATE(CHeightCurveFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CHeightCurveFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELCHANGE(IDC_COMBO11_1, OnCBoxSpeciesChanged)
	ON_BN_CLICKED(IDC_CHECK11_1, OnBnClickedCheck1)
END_MESSAGE_MAP()

CHeightCurveFormView::CHeightCurveFormView()
	: CXTResizeFormView(CHeightCurveFormView::IDD)
{
	m_bInitialized = FALSE;
	m_nTraktID = -1;
	m_nShowSpecies = -1;
	m_bShowSampleTrees = FALSE;
	m_fMaxDCLS = 0.0;
}

CHeightCurveFormView::~CHeightCurveFormView()
{
}

void CHeightCurveFormView::OnDestroy()
{
	m_fntCaption.DeleteObject();
	m_fntText.DeleteObject();
	m_fntTextBold.DeleteObject();
	m_fntTextSmall.DeleteObject();

	CXTResizeFormView::OnDestroy();	
}

void CHeightCurveFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CHeightCurveFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

BOOL CHeightCurveFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CHeightCurveFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHelloworldDlg)
	DDX_Control(pDX, IDC_CHART_HGTCURVES, m_chartViewer);
	DDX_Control(pDX, IDC_LBL11_1, m_wndLbl1);
	DDX_Control(pDX, IDC_COMBO11_1, m_wndCBox1);
	DDX_Control(pDX, IDC_CHECK11_1, m_wndCheck1);
	//}}AFX_DATA_MAP
}

void CHeightCurveFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sDiagramCaption = xml.str(IDS_STRING2541);
				m_sDiagramYLabel = xml.str(IDS_STRING2542);
				m_sDiagramXLabel = xml.str(IDS_STRING2543);			
				m_sShowAllCB = xml.str(IDS_STRING2545);
				m_sStandNumber = xml.str(IDS_STRING1000);
				m_sStandName = xml.str(IDS_STRING101);
				m_sPropertyNameLabel = xml.str(IDS_STRING104);
				m_sNoSampleTrees = xml.str(IDS_STRING2547);
				m_sMsgCap = xml.str(IDS_STRING151);

				m_wndLbl1.SetWindowText(xml.str(IDS_STRING2544));
				m_wndCheck1.SetWindowText(xml.str(IDS_STRING2546));
			}
			xml.clean();

			m_fntCaption.CreateFont(190, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));

			m_fntText.CreateFont(90, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextBold.CreateFont(90, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextSmall.CreateFont(70, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
		
	
		}

		m_wndCheck1.EnableWindow(FALSE);
		m_bInitialized = TRUE;
	}

}

void CHeightCurveFormView::OnSetFocus(CWnd*)
{
}

void CHeightCurveFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CHeightCurveFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_chartViewer)
	{
		m_chartViewer.MoveWindow(10,40,cx-20,cy);
		drawChartView();
	}
}

// CHeightCurveFormView diagnostics

#ifdef _DEBUG
void CHeightCurveFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CHeightCurveFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CHeightCurveFormView::getTraktSetSpcFromDB(int trakt_id)
{
	if (m_pDB)
	{
		m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,trakt_id);
	}	// if (m_pDB != NULL)
}

void CHeightCurveFormView::getTraktDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

void CHeightCurveFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

void CHeightCurveFormView::getSampleTreesFromDB(int trakt_id,int tree_type,BOOL clear)
{
	if (m_pDB != NULL)
	{
		m_pDB->getSampleTrees(trakt_id,tree_type,m_vecTraktSampleTrees,clear);
	}	// if (m_pDB != NULL)
}

// CHeightCurveFormView message handlers

void CHeightCurveFormView::drawChartView(bool redraw,bool save_to_db,drawType dt,CDC* pDC)
{
	int nScatterSpcIndex = -1;
	double fDCLSCounter;
	double fDCLSClass;
	CRect rect;
	GetClientRect(&rect);
	//CTransaction_dcls_tree
	vecLineLayers lineLayers;

	// Create a XYChart
	XYChart *xyChart = new XYChart(rect.right-20, rect.bottom-50);

	if (m_nTraktID != getActiveTrakt()->getTraktID() || redraw)
	{
		getTraktDataFromDB(getActiveTrakt()->getTraktID());
		getTraktMiscDataFromDB(getActiveTrakt()->getTraktID());
		m_vecTraktSampleTrees.clear();
		getSampleTreesFromDB(getActiveTrakt()->getTraktID(),SAMPLE_TREE);
		getSampleTreesFromDB(getActiveTrakt()->getTraktID(),TREE_SAMPLE_EXTRA);
		// Add species to ComboBox; 100406 p�d
		if (m_vecTraktData.size() > 0)
		{
			m_wndCBox1.ResetContent();

			m_wndCBox1.AddString(m_sShowAllCB);	// "All species"

			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0)
				{
					m_wndCBox1.AddString(recTraktData.getSpecieName());
				}	
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			if (m_nCurCBSel == -1)
				m_wndCBox1.SetCurSel(0);
			else
				m_wndCBox1.SetCurSel(m_nCurCBSel);
		}	// if (m_vecTraktData.size() > 0)

		m_vecDCLSTrees.clear();
		//---------------------------------------------------------------------------------
		// Get information on pricelist saved in esti_trakt_pricelist_table; 070502 p�d
		fDCLSClass = m_recTraktMiscData.getDiamClass();
		fDCLSCounter = fDCLSClass;
		if (fDCLSClass > 0.0)
			m_fMaxDCLS = m_pDB->getMaxDCLS(getActiveTrakt()->getTraktID(),m_nShowSpecies)/fDCLSClass;
		else
			m_fMaxDCLS = 0.0;
//		if (m_fMaxDCLS == 0.0) m_fMaxDCLS = fDCLSClass;
		if (m_fMaxDCLS > 0.0)
		{

			getTraktSetSpcFromDB(getActiveTrakt()->getTraktID());
			
			if (m_vecTraktData.size() > 0)
			{
				for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
				{
					fDCLSCounter = fDCLSClass;
					CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
					if (recTraktData.getNumOf() > 0 && (m_nShowSpecies == recTraktData.getSpecieID() || m_nShowSpecies == -1) )
					{
						// Setup the Diameterclass vector adding ALL diameterclasses/species; 100326 p�d
						for (UINT i2 = 0;i2 < (int)m_fMaxDCLS;i2++)
						{
							m_vecDCLSTrees.push_back(CTransaction_dcls_tree(i1+1,getActiveTrakt()->getTraktID(),1,recTraktData.getSpecieID(),recTraktData.getSpecieName(),
																															fDCLSCounter-(fDCLSClass/2.0),
																															(fDCLSCounter+fDCLSClass-(fDCLSClass/2.0)),
																															0.0,1,0.0,1.0,0.0,0.0,0.0,0.0,50,0,0,L""));

							fDCLSCounter += fDCLSClass;
			
						}	// for (UINT i = 0;i < 20;i++)
					}
				}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)

				doUCCalculate(*getActiveTrakt(),
											m_recTraktMiscData,
											m_vecTraktData,
											m_vecTraktSampleTrees,
											m_vecDCLSTrees,
											m_vecTransactionTraktSetSpc,
											true,	// doCalculations
											false	// Don't calc grot
											);
			}
		}
		m_nTraktID = getActiveTrakt()->getTraktID();

	}

	// Do nothing, empty stand; 100408 p�d
	//if (m_fMaxDCLS == 0.0) return;

	//---------------------------------------------------------------------
  // The data for the line chart
	StrArr sarr((int)m_fMaxDCLS);
	vecDbl vDbl,vDblX,vDblY;
	//---------------------------------------------------------------------
	// Setup diameterclass
	fDCLSClass = m_recTraktMiscData.getDiamClass();
	fDCLSCounter = 0.0;
	int nCnt = 0;
	char szTmp[255];
	for (int i3 = 0;i3 < (int)m_fMaxDCLS;i3++)
	{
		if (m_fMaxDCLS*fDCLSClass > 100.0)
		{
			if (((i3 % 2) == 0))
				sprintf(szTmp,"%.0f",fDCLSCounter);
			else
				sprintf(szTmp," ");
		}
		else
			sprintf(szTmp,"%.0f",fDCLSCounter);
		
		sarr.add(i3,szTmp);
		
		fDCLSCounter += fDCLSClass;
	}

	std::vector<DblArr> vecDCLSDblArr;
	std::vector<DblArr> vecSampDblArrX,vecSampDblArrY;
	int cnt = 0;
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			CTransaction_trakt_data recTraktData = m_vecTraktData[i1];

			//---------------------------------------------------------------------
			// Setup graf; hights/diameterclass
			vDbl.clear();
			vDbl.push_back(0.0);
			for (UINT i4 = 0;i4 < m_vecDCLSTrees.size();i4++)
			{
				if (m_vecDCLSTrees[i4].getSpcID() == recTraktData.getSpecieID())
				{
					vDbl.push_back(m_vecDCLSTrees[i4].getHgt()/10.0);
				}
			}
			vecDCLSDblArr.push_back(DblArr(vDbl));

			//---------------------------------------------------------------------
			// Add Sampletrees
			vDblX.clear();
			vDblY.clear();
			for (UINT i5 = 0;i5 < m_vecTraktSampleTrees.size();i5++)
			{
				CTransaction_sample_tree sampTree = m_vecTraktSampleTrees[i5];
				if (sampTree.getSpcID() == recTraktData.getSpecieID() && m_nShowSpecies == recTraktData.getSpecieID())
				{
					vDblX.push_back((sampTree.getDBH()/20.0));
					vDblY.push_back(sampTree.getHgt()/10.0);
					nScatterSpcIndex = cnt;
				}
			}
			vecSampDblArrX.push_back(DblArr(vDblX));
			vecSampDblArrY.push_back(DblArr(vDblY));

			cnt++;
		}
	}
	LineLayer *layer;	// Max 20 species in one diagram; 100627 p�d
	lineLayers.clear();
	//------------------------------------------------------------------
	// Add lineLayers/species
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
			lineLayers.push_back(CChartLayers(recTraktData.getSpecieID(),recTraktData.getSpecieName(),recTraktData.getNumOf() > 0));
		}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
	}	// if (m_vecTraktData.size() > 0)
	if (lineLayers.size() > 0)
	{
		for (UINT l1 = 0;l1 < lineLayers.size();l1++)
		{
			// Add actual hightcurve/species; 100330 p�d
			if (lineLayers[l1].getUse())
			{	
				layer = xyChart->addLineLayer();
				layer->addDataSet(DoubleArray(vecDCLSDblArr[l1].get(), vecDCLSDblArr[l1].numof()),-1,convStr(lineLayers[l1].getSpcName()));
				if (dt == DRAW_VIEW) layer->setLineWidth(1);
				else if (dt == DRAW_PREVIEW) layer->setLineWidth(2);
			}	// if (lineLayers[l1].getUse())
		}	// for (UINT l1 = 0;l1 < lineLayers.size();l1++)
	}	// if (lineLayers.size() > 0)

	// Set the axes line width to 2 pixels
	xyChart->xAxis()->setWidth(2);
	xyChart->yAxis()->setWidth(2);

	// Add a title to the y axis
	xyChart->yAxis()->setTitle(convStr(m_sDiagramYLabel));

	// Add a title to the x axis
	xyChart->xAxis()->setTitle(convStr(m_sDiagramXLabel));

	// Set the labels on the x axis; 100330 p�d
	if (m_fMaxDCLS > 0.0)
		xyChart->xAxis()->setLabels(StringArray(sarr.get(), sarr.numof()));

	xyChart->yAxis()->setAutoScale(0, 0, 0);


	if (m_bShowSampleTrees && nScatterSpcIndex == -1)
	{
		if (m_wndCheck1.GetCheck())
			UMMessageBox(this->GetSafeHwnd(),m_sNoSampleTrees,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

		m_wndCheck1.SetCheck(FALSE);
	}
	else if (m_bShowSampleTrees && vecSampDblArrX.size() > 0 && vecSampDblArrY.size() > 0 && nScatterSpcIndex < vecSampDblArrX.size() &&  nScatterSpcIndex < vecSampDblArrY.size())
	{
	  // Plot sampletrees for specie; 100406 p�d
		xyChart->addScatterLayer(DoubleArray(vecSampDblArrX[nScatterSpcIndex].get(),vecSampDblArrX[nScatterSpcIndex].numof()),
														 DoubleArray(vecSampDblArrY[nScatterSpcIndex].get(),vecSampDblArrY[nScatterSpcIndex].numof()),
														 "",Chart::SquareShape,6);
	}

	if (dt == DRAW_VIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 15, rect.right-220, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);

		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-150, 15); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);
		// Output the chart
		m_chartViewer.setChart(xyChart);
	}
	else if (dt == DRAW_PREVIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 10, rect.right-350, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);
		// Add legend, display name of species; 100330 p�d
		xyChart->addLegend(rect.right-270, 10); //, true, "timesbd.ttf", 10); //->setBackground(0xd0d0d0,0xd0d0d0,1);

		// The scaling factor to adjust for printer resolution
		// (pDC = CDC pointer representing the printer device context)
		double xScaleFactor = pDC->GetDeviceCaps(LOGPIXELSX) / 96.0;
		double yScaleFactor = pDC->GetDeviceCaps(LOGPIXELSY) / 96.0;

		// Output the chart as BMP
		MemBlock bmp = xyChart->makeChart(Chart::BMP);

		// The BITMAPINFOHEADER is 14 bytes offset from the beginning
		LPBITMAPINFO header = (LPBITMAPINFO)(bmp.data + 14);

		// The bitmap data
		LPBYTE bitData = (LPBYTE)(bmp.data) + ((LPBITMAPFILEHEADER)(bmp.data))->bfOffBits;

		int nLeft = 20*xScaleFactor;
		int nTop = 20*yScaleFactor;
		int nWidth = 5220; //header->bmiHeader.biWidth*xScaleFactor*0.6;
		int nHeight = 60*yScaleFactor;

		TEXTMETRIC tm;
		CBrush brush;
		brush.CreateSolidBrush(RGB(0,0,255)); 
		//---------------------------------------------------------------------------
		// Setup Caption
		pDC->FillRect(CRect(nLeft,nTop,nWidth*0.9,nHeight),&brush);
		pDC->SelectObject((CFont*)&m_fntCaption);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->DrawText(m_sDiagramCaption,CRect(nLeft,nTop,nWidth*0.9,nHeight),DT_SINGLELINE|DT_CENTER|DT_VCENTER);

		//---------------------------------------------------------------------------
		// Setup date
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextSmall);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nLeft,nHeight,getDateTime());

		//---------------------------------------------------------------------------
		// Setup Name of Stand
		int nTextX = 0;
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextBold);
		pDC->GetTextMetrics(&tm);
		nTextX = max(nTextX,m_sStandNumber.GetLength());
		nTextX = max(nTextX,m_sStandName.GetLength());
		nTextX = max(nTextX,m_sPropertyNameLabel.GetLength());
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*2,m_sStandNumber+L":");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*3,m_sStandName+L":");
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft,nHeight+tm.tmHeight*4,m_sPropertyNameLabel+L":");
		pDC->SelectObject((CFont*)&m_fntText);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*2,getActiveTrakt()->getTraktNum());
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*3,getActiveTrakt()->getTraktName());
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*4,m_sPropertyName);

		//---------------------------------------------------------------------------
		// Output to device context
		StretchDIBits(pDC->m_hDC,
									(int)(10 * xScaleFactor),
									(int)(300 * yScaleFactor),
									(int)(1392 * xScaleFactor * 0.6),
									(int)(583 * yScaleFactor * 0.6),
									0, 0, header->bmiHeader.biWidth, header->bmiHeader.biHeight,
									bitData, header, DIB_RGB_COLORS, SRCCOPY);

	}


	if (m_fMaxDCLS > 0.0)
		if (sarr.numof() > 0)	sarr.free();
	for (UINT i = 0;i < vecDCLSDblArr.size();i++)
		vecDCLSDblArr[i].free();
	for (UINT i = 0;i < vecSampDblArrX.size();i++)
		vecSampDblArrX[i].free();
	for (UINT i = 0;i < vecSampDblArrY.size();i++)
		vecSampDblArrY[i].free();

  //free up resources
	delete xyChart;
}

void CHeightCurveFormView::OnPrint(CDC* pDC, CPrintInfo *pInfo)
{
	drawChartView(false,false,DRAW_PREVIEW,pDC);
}

BOOL CHeightCurveFormView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return CXTResizeFormView::DoPreparePrinting(pInfo);
}

void CHeightCurveFormView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this, RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}
}

void CHeightCurveFormView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	drawChartView();
	CXTResizeFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CHeightCurveFormView::doRunPrint()	
{ 
	this->OnFilePrint();
}

void CHeightCurveFormView::OnCBoxSpeciesChanged(void)
{
	CString sSpcName;
	m_nCurCBSel = m_wndCBox1.GetCurSel();
	m_wndCBox1.GetWindowText(sSpcName);
	m_nShowSpecies = -1;
	if (m_nCurCBSel != CB_ERR)
	{
		if (m_nCurCBSel > 0)
		{
			if (m_vecTraktData.size() > 0 && m_nCurCBSel-1 < m_vecTraktData.size())
			{		
				for (UINT i = 0;i < m_vecTraktData.size();i++)
				{
					if (m_vecTraktData[i].getSpecieName().Compare(sSpcName) == 0)
					{
						m_nShowSpecies = m_vecTraktData[i].getSpecieID();
						break;
					}
				}
			}	
		}
	}

	m_wndCheck1.EnableWindow(m_nCurCBSel > 0);

	m_bShowSampleTrees = ((m_nCurCBSel > 0) ? TRUE : FALSE) && (m_wndCheck1.GetCheck());
	if (!m_bShowSampleTrees)
		m_wndCheck1.SetCheck(FALSE);

	drawChartView(true);
}

void CHeightCurveFormView::OnBnClickedCheck1()
{
	m_bShowSampleTrees = ((m_nCurCBSel > 0) ? TRUE : FALSE) && (m_wndCheck1.GetCheck());
	drawChartView(true);
}

