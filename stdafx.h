// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500
/*
// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0400		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0400	// Change this to the appropriate value to target Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0400	// Change this to the appropriate value to target IE 5.0 or later.
#endif
*/
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxhtml.h>						// Per tal de poder utilitzar HTML.

#include <SQLAPI.h> // main SQLAPI++ header

#include <vector>
#include <map>

#define _USE_MATH_DEFINES
#include <math.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions
#include <afxdlgs.h>

// ... in PAD_HMSFuncLib
#include "pad_hms_miscfunc.h"
#include "pad_transaction_classes.h"	
#include "pad_xmlshelldatahandler.h"
#include "pricelistparser.h"	
#include "xmllitebase.h"	
#include "templateparser.h"	
#include "coststmplparser.h"	
//#include "coststmplparser.h"	
#include "Calculation_methods.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"
// ...
#include "DatePickerCombo.h"

#include "UMEstimateDB.h"

#include "ChartViewer.h"





//////////////////////////////////////////////////////////////////////
// MY DEFINE FOR ADDING DATA SECUTITY METHODS; 090224 p�d
// UNCOMMENT THIS TO USE IT!
//#define __USE_DATA_SECURITY_METHODS

//////////////////////////////////////////////////////////////////////

// Uncomment this to add GROT calulations
// NB! Also in UCCalculate!
#define CALCULATE_GROT

// Uncomment this to add "Gallringsmall"
//#define SHOW_THINNING


/////////////////////////////////////////////////////////////////////
// My own includes

#define USER_MSG_SHELL_TREE 				(WM_USER + 3)		// This is a identifer used when communication from a

#define USER_MSG_REPORT_OPEN				(WM_USER + 5)		// This is a identifer used when communication from a
																										// suite/user module, to open a Report; 060119 p�d
#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally
#define MSG_IN_SUITE2				 				(WM_USER + 11)		// This identifer's used to send messages internally

#define ID_SHOWVIEW_MSG							0x8116

#define ID_SPECIE_PROPGRID					0x9002

#define IDC_REPORT									0x9010
#define IDC_TRAKT_TYPE_REPORT				0x9011
#define IDC_REPORT1									0x9012
#define IDC_TABCONTROL							0x9013
#define IDC_TABCONTROL1							0x9014
#define IDC_REPORT2									0x9015

#define ID_TBBTN_COLUMNS						0x9016


#define ID_HGT_TYPEOF_FUNC_CB				0x9018		// Type of height functions (e.g. H25,Regression,S�derbergs)
#define ID_HGT_FUNC_CB							0x9019		// Which functions to use witin e.g. H25, Regression, S�derbergs etc

#define ID_VOL_TYPEOF_FUNC_CB				0x9020		// Type of volume functions (e.g. Brandels,N�slunds)
#define ID_VOL_FUNC_CB							0x9021		// Which functions to use witin e.g. Brandels, N�slunds etc

#define ID_VOL_UB_TYPEOF_FUNC_CB		0x9022		// Type of volume functions (e.g. Brandels,N�slunds)
#define ID_VOL_UB_FUNC_CB						0x9023		// Which functions to use witin e.g. Brandels, N�slunds etc

#define ID_BARK_TYPEOF_FUNC_CB			0x9024		// Type of bark functions (e.g. Jonsson/Ostlin, Skogforsk, S�derbergs)
#define ID_BARK_FUNC_CB							0x9025		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc

#define ID_M3SK_TO_M3FUB						0x9026
#define ID_M3FUB_TO_M3TO						0x9027
#define ID_DEF_H25								0x9057	// #4513: Def H25
#define ID_M3SK_TO_M3UB							0x9028
#define ID_DIAM_CLASS								0x9029
#define ID_COST_TMPL								0x9030
#define ID_QUALDESC									0x9031
#define ID_ATCOAST									0x9032
#define ID_SOUTHEAST								0x9033
#define ID_REGION5									0x9034
#define ID_PART_OF_PLOT							0x9035
#define ID_SI_H100_PINE							0x9036
#define ID_TRANSP_DIST1							0x9037
#define ID_TRANSP_DIST2							0x9038

#define ID_EXCH_FUNC_CB							0x9039
#define ID_PRL_FUNC_CB							0x9040

#define ID_MISC_GREEN_CROWN	0x9058	// #4555: Standard gr�nkroneandel

#define ID_POPUPMENU_UPDATE_PRICELIST 0x9041
#define ID_POPUPMENU_UPDATE_COSTTMPL	0x9042
#define ID_POPUPMENU_SHOW_PRICELIST		0x9043
#define ID_POPUPMENU_SHOW_COSTTMPL		0x9044

#define ID_POPUPMENU_EXPAND_COLLAPSE	0x9045

#define ID_INDICATOR_PROG             0x9046

#define ID_REPORT_GROUPS							0x9047

#define ID_GROT_TYPEOF_FUNC_CB				0x9048		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc
#define ID_GROT_PERCENT								0x9049		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc

// ID's for popupmenu in ReportControl; 070109 p�d
#define ID_POPUPMENU_SHOW_HIDE_ASSORTMENTS 0x9050

#define ID_TBBTN_COLUMNS_DLG				0x9051
#define ID_TBBTN10_COLUMNS_DLG			0x9052
#define ID_TBBTN11_COLUMNS_DLG			0x9053
#define ID_TBBTN12_COLUMNS_DLG			0x9054
#define ID_TBBTN2_COLUMNS_DLG				0x9055

#define ID_INVMETHOD_DLGBAR					0x9056

#define ID_GROUP_BYTHIS							0x9100
#define ID_SHOW_GROUPBOX						0x9101
#define ID_SHOW_FIELDCHOOSER				0x9102
#define ID_SHOW_FIELDCHOOSER1				0x9103
#define ID_SHOW_FIELDCHOOSER2				0x9104
#define ID_PREVIEW1									0x9105
#define ID_PREVIEW2									0x9106

#define IDC_DCLS_REPORT							0x9107
#define IDC_DCLS1_REPORT						0x9108
#define IDC_DCLS2_REPORT						0x9109
#define IDC_SAMPLE_REPORT						0x9110
#define IDC_SEL_PROP_REPORT					0x9111

#define IDC_PRINT_PRL								0x9115

#define ID_GROT_PRICE								0x9116		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc
#define ID_GROT_COST								0x9117		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc

#define IDC_HEIGHT_CURVES						0x9118
#define IDC_DIAM_DISTRIBUTION				0x9119
#define IDC_VOL_DISTRIBUTION				0x9120
#define IDC_ASSORT_DISTRIBUTION			0x9121
#define IDC_THINNING_DIAGRAM				0x9122

#define IDC_OPEN_SETTINGS						0x9123
#define IDC_OPEN_SET_ASSORT					0x9124

#define ID_SPEC_SETTINGS_START				0x9125				//20121206 J� #3509
#define ID_SPEC_SETTINGS_END				0x92b5
#define ID_SPEC_ASSORT_START				0x92b6
#define ID_SPEC_ASSORT_END					0x94aa

#define ID_SPEC_SETTINGS					0x94ab
#define ID_SPEC_ASSORT						0x94ac

#define ID_GIS_VIEWSTAND					0x94ad
#define ID_GIS_UPDATESTAND					0x94ae
/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING10								10
#define IDS_STRING11								11
#define IDS_STRING12								12

#define IDS_STRING1000							1000
#define IDS_STRING1001							1001
#define IDS_STRING100								100
#define IDS_STRING101								101
#define IDS_STRING102								102
#define IDS_STRING103								103
#define IDS_STRING104								104
#define IDS_STRING1040							1040
#define IDS_STRING1041							1041
#define IDS_STRING1050							1050
#define IDS_STRING1051							1051
#define IDS_STRING1052							1052
#define IDS_STRING1053							1053
#define IDS_STRING106								106
#define IDS_STRING107								107
#define IDS_STRING108								108
#define IDS_STRING109								109
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113
#define IDS_STRING114								114
#define IDS_STRING115								115
#define IDS_STRING116								116
#define IDS_STRING117								117
#define IDS_STRING118								118
#define IDS_STRING119								119
#define IDS_STRING120								120
#define IDS_STRING121								121
#define IDS_STRING122								122
#define IDS_STRING123								123
#define IDS_STRING124								124
#define IDS_STRING1240							1240
#define IDS_STRING1241							1241
#define IDS_STRING1242							1242
#define IDS_STRING125								125
#define IDS_STRING126								126
#define IDS_STRING1270							1270
#define IDS_STRING1271							1271
#define IDS_STRING128								128
#define IDS_STRING129								129
#define IDS_STRING130								130
#define IDS_STRING1300							1300
#define IDS_STRING1301							1301
#define IDS_STRING1302							1302
#define IDS_STRING131								131
#define IDS_STRING132								132
#define IDS_STRING133								133
#define IDS_STRING134								134
#define IDS_STRING135								135
#define IDS_STRING1350							1350
#define IDS_STRING1351							1351
#define IDS_STRING1352							1352
#define IDS_STRING136								136
#define IDS_STRING137								137
#define IDS_STRING138								138
#define IDS_STRING139								139
#define IDS_STRING140								140
#define IDS_STRING141								141
#define IDS_STRING1410							1410
#define IDS_STRING1411							1411
#define IDS_STRING142								142
#define IDS_STRING1420							1420
#define IDS_STRING1421							1421
#define IDS_STRING143								143
#define IDS_STRING144								144
#define IDS_STRING145								145
#define IDS_STRING146								146
#define IDS_STRING147								147
#define IDS_STRING148								148
#define IDS_STRING149								149
#define IDS_STRING150								150
#define IDS_STRING151								151	// Message
#define IDS_STRING152								152
#define IDS_STRING153								153
#define IDS_STRING154								154
#define IDS_STRING155								155
#define IDS_STRING1550							1550
#define IDS_STRING1551							1551
#define IDS_STRING1552							1552	// 071204 p�d
#define IDS_STRING1553							1553
#define IDS_STRING1554							1554
#define IDS_STRING1555							1555
#define IDS_STRING1556							1556
#define IDS_STRING1557							1557
#define IDS_STRING1558							1558
#define IDS_STRING1559							1559
#define IDS_STRING1560							1560
#define IDS_STRING1561							1561
#define IDS_STRING156								156
#define IDS_STRING157								157
#define IDS_STRING158								158
#define IDS_STRING159								159
#define IDS_STRING1570							1570
#define IDS_STRING1580							1580
#define IDS_STRING1590							1590
#define IDS_STRING1571							1571
#define IDS_STRING1581							1581
#define IDS_STRING1591							1591
#define IDS_STRING1572							1572
#define IDS_STRING1582							1582
#define IDS_STRING1592							1592
#define IDS_STRING1573							1573
#define IDS_STRING1583							1583
#define IDS_STRING1593							1593
#define IDS_STRING1574							1574
#define IDS_STRING1584							1584
#define IDS_STRING1594							1594
#define IDS_STRING1595							1595
#define IDS_STRING1596	1596
#define IDS_STRING1597	1597
#define IDS_STRING1598	1598
#define IDS_STRING160								160
#define IDS_STRING161								161
#define IDS_STRING162								162
#define IDS_STRING163								163
#define IDS_STRING164								164
#define IDS_STRING165								165
#define IDS_STRING166								166
#define IDS_STRING167								167
#define IDS_STRING168								168
#define IDS_STRING169								169
#define IDS_STRING170								170
#define IDS_STRING171								171
#define IDS_STRING172								172
#define IDS_STRING173								173
#define IDS_STRING174								174
#define IDS_STRING175								175
#define IDS_STRING176								176
#define IDS_STRING177								177
#define IDS_STRING178								178
#define IDS_STRING179								179
#define IDS_STRING180								180
#define IDS_STRING181								181
#define IDS_STRING182								182
#define IDS_STRING183								183
#define IDS_STRING184								184
#define IDS_STRING185								185
#define IDS_STRING186								186
#define IDS_STRING187								187
#define IDS_STRING188								188
#define IDS_STRING189								189
#define IDS_STRING190								190
#define IDS_STRING191								191
#define IDS_STRING192								192
#define IDS_STRING193								193
#define IDS_STRING194								194
#define IDS_STRING195								195
#define IDS_STRING196								196
#define IDS_STRING197								197
#define IDS_STRING198								198
#define IDS_STRING199								199
#define IDS_STRING1900							1900
#define IDS_STRING1901							1901
#define IDS_STRING1902							1902
#define IDS_STRING1903							1903
#define IDS_STRING1904							1904
#define IDS_STRING200								200
#define IDS_STRING201								201
#define IDS_STRING202								202
#define IDS_STRING2001							2001
#define IDS_STRING2002							2002
#define IDS_STRING2003							2003
#define IDS_STRING203								203
#define IDS_STRING204								204
#define IDS_STRING205								205
#define IDS_STRING2050							2050
#define IDS_STRING2051							2051
#define IDS_STRING2060							2060
#define IDS_STRING206								206
#define IDS_STRING207								207
#define IDS_STRING208								208
#define IDS_STRING2090							2090
#define IDS_STRING2091							2091
#define IDS_STRING2092							2092
#define IDS_STRING2100							2100
#define IDS_STRING2101							2101
#define IDS_STRING211								211
#define IDS_STRING212								212
#define IDS_STRING213								213
#define IDS_STRING214								214
#define IDS_STRING215								215
#define IDS_STRING216								216
#define IDS_STRING217								217
#define IDS_STRING218								218
#define IDS_STRING219								219
#define IDS_STRING220								220
#define IDS_STRING221								221
#define IDS_STRING222								222
#define IDS_STRING223								223
#define IDS_STRING224								224
#define IDS_STRING225								225
#define IDS_STRING226								226
#define IDS_STRING227								227
#define IDS_STRING228								228
#define IDS_STRING229								229
#define IDS_STRING230								230
#define IDS_STRING231								231
#define IDS_STRING232								232
#define IDS_STRING233								233
#define IDS_STRING234								234
#define IDS_STRING235								235
#define IDS_STRING236								236
#define IDS_STRING237								237
#define IDS_STRING238								238
#define IDS_STRING239								239
#define IDS_STRING240								240
#define IDS_STRING2410							2410
#define IDS_STRING2411							2411
#define IDS_STRING2412							2412
#define IDS_STRING2413							2413
#define IDS_STRING241								241
#define IDS_STRING242								242
#define IDS_STRING243								243
#define IDS_STRING244								244
#define IDS_STRING245								245
#define IDS_STRING246								246
#define IDS_STRING247								247
#define IDS_STRING248								248
#define IDS_STRING249								249
#define IDS_STRING250								250
#define IDS_STRING251								251
#define IDS_STRING252								252
#define IDS_STRING2520							2520
#define IDS_STRING253								253
#define IDS_STRING254								254
#define IDS_STRING2540							2540
#define IDS_STRING2541							2541
#define IDS_STRING2542							2542
#define IDS_STRING2543							2543
#define IDS_STRING2544							2544
#define IDS_STRING2545							2545
#define IDS_STRING2546							2546
#define IDS_STRING2547							2547
#define IDS_STRING2548							2548
#define IDS_STRING2549							2549
#define IDS_STRING2550							2550
#define IDS_STRING2551							2551
#define IDS_STRING2552							2552
#define IDS_STRING2553							2553
#define IDS_STRING2554							2554
#define IDS_STRING2555							2555
#define IDS_STRING2556							2556
#define IDS_STRING2557							2557
#define IDS_STRING2558							2558
#define IDS_STRING2559							2559
#define IDS_STRING2560							2560
#define IDS_STRING255								255
#define IDS_STRING256								256
#define IDS_STRING257								257
#define IDS_STRING258								258
#define IDS_STRING2580							2580
#define IDS_STRING2581							2581
#define IDS_STRING2582							2582
#define IDS_STRING2583							2583
#define IDS_STRING2584							2584
#define IDS_STRING2585							2585
#define IDS_STRING2586							2586
#define IDS_STRING2587							2587
#define IDS_STRING259								259
#define IDS_STRING2590							2590
#define IDS_STRING2591							2591
#define IDS_STRING2592							2592
#define IDS_STRING2594							2594
#define IDS_STRING2595							2595
#define IDS_STRING2596							2596
#define IDS_STRING2597							2597
#define IDS_STRING2598							2598
#define IDS_STRING2599							2599
#define IDS_STRING260								260
#define IDS_STRING2600							2600
#define IDS_STRING2601							2601
#define IDS_STRING2602							2602
#define IDS_STRING2603							2603
#define IDS_STRING2604							2604
#define IDS_STRING2605							2605

//#5026 PH 20160617
#define IDS_STRING26051							26051
#define IDS_STRING26052							26052
#define IDS_STRING26053							26053
#define IDS_STRING26054							26054
#define IDS_STRING26055							26055
#define IDS_STRING26056							26056

#define IDS_STRING2610							2610
#define IDS_STRING2611							2611	
#define IDS_STRING2612							2612
#define IDS_STRING2613							2613
#define IDS_STRING2614							2614

#define IDS_STRING261								261
#define IDS_STRING262								262
#define IDS_STRING263								263
#define IDS_STRING264								264
#define IDS_STRING265								265
#define IDS_STRING266								266
#define IDS_STRING267								267
#define IDS_STRING268								268
#define IDS_STRING269								269
#define IDS_STRING270								270
#define IDS_STRING2700							2700
#define IDS_STRING271								271
#define IDS_STRING272								272
#define IDS_STRING2720							2720
#define IDS_STRING2721							2721
#define IDS_STRING273								273
#define IDS_STRING274								274
#define IDS_STRING275								275
#define IDS_STRING276								276
#define IDS_STRING277								277
#define IDS_STRING278								278
#define IDS_STRING279								279
#define IDS_STRING280								280
#define IDS_STRING281								281
#define IDS_STRING282								282
#define IDS_STRING2820							2820
#define IDS_STRING2821							2821
#define IDS_STRING2822							2822
#define IDS_STRING2823							2823
#define IDS_STRING283								283
#define IDS_STRING284								284
#define IDS_STRING285								285
#define IDS_STRING286								286
#define IDS_STRING287								287
#define IDS_STRING2872								2872
#define IDS_STRING288								288
#define IDS_STRING289								289
#define IDS_STRING2890							2890
#define IDS_STRING2891							2891
#define IDS_STRING290								290
#define IDS_STRING291								291
#define IDS_STRING292								292
#define IDS_STRING2920							2920
#define IDS_STRING293								293
#define IDS_STRING2930							2930
#define IDS_STRING2931							2931
#define IDS_STRING2932							2932
#define IDS_STRING2933							2933
#define IDS_STRING2934							2934
#define IDS_STRING294	294
#define IDS_STRING295	295
#define IDS_STRING296	296
#define IDS_STRING2961	2961
#define IDS_STRING297								297
#define IDS_STRING298								298
#define IDS_STRING2980							2980
#define IDS_STRING2981							2981
#define IDS_STRING2982							2982
#define IDS_STRING2983							2983
#define IDS_STRING299								299
#define IDS_STRING300								300
#define IDS_STRING301								301
#define IDS_STRING302								302
#define IDS_STRING303								303
#define IDS_STRING304								304
#define IDS_STRING305								305
#define IDS_STRING3050							3050
#define IDS_STRING3051							3051
#define IDS_STRING3052							3052
#define IDS_STRING3053							3053
#define IDS_STRING3054							3054
#define IDS_STRING3055							3055
#define IDS_STRING306								306
#define IDS_STRING3060							3060
#define IDS_STRING3061							3061
#define IDS_STRING3070							3070
#define IDS_STRING3071							3071
#define IDS_STRING3072							3072
#define IDS_STRING3073							3073
#define IDS_STRING3074							3074
#define IDS_STRING3080							3080
#define IDS_STRING3081							3081
#define IDS_STRING3082							3082
#define IDS_STRING3083							3083
#define IDS_STRING3084							3084
#define IDS_STRING3085							3085
#define IDS_STRING3086							3086
#define IDS_STRING3087							3087
#define IDS_STRING3088							3088
#define IDS_STRING3089							3089
#define IDS_STRING3090							3090
#define IDS_STRING3091							3091
#define IDS_STRING3092							3092
#define IDS_STRING3093							3093
#define IDS_STRING3094							3094
#define IDS_STRING3095							3095
#define IDS_STRING3100							3100
#define IDS_STRING3101							3101
#define IDS_STRING3102							3102
#define IDS_STRING3103							3103
#define IDS_STRING3104							3104
#define IDS_STRING3105							3105
#define IDS_STRING3106							3106
#define IDS_STRING3107							3107
#define IDS_STRING3108							3108
#define IDS_STRING3109							3109
#define IDS_STRING3110							3110
#define IDS_STRING3111							3111
#define IDS_STRING3112							3112
#define IDS_STRING3113							3113
#define IDS_STRING3114							3114
#define IDS_STRING31141							31141
#define IDS_STRING3115                          3115
#define IDS_STRING3116                          3116
#define IDS_STRING311								311
#define IDS_STRING312								312
#define IDS_STRING313								313
#define IDS_STRING3140							3140
#define IDS_STRING3141							3141
#define IDS_STRING3142							3142
#define IDS_STRING315								315
#define IDS_STRING316								316
#define IDS_STRING317								317
#define IDS_STRING318								318
#define IDS_STRING3170							3170
#define IDS_STRING3171							3171
#define IDS_STRING3172							3172
#define IDS_STRING3173							3173
#define IDS_STRING3174							3174
#define IDS_STRING3175							3175
#define IDS_STRING3180							3180
#define IDS_STRING3181							3181
#define IDS_STRING3182							3182
#define IDS_STRING3183							3183
#define IDS_STRING3184							3184
#define IDS_STRING3185							3185
#define IDS_STRING3186							3186
#define IDS_STRING3187							3187
#define IDS_STRING3188							3188
#define IDS_STRING3189							3189
#define IDS_STRING319								319
#define IDS_STRING3190							3190
#define IDS_STRING3191							3191
#define IDS_STRING320								320
#define IDS_STRING321								321
#define IDS_STRING3210							3210
#define IDS_STRING3211							3211
#define IDS_STRING3212							3212
#define IDS_STRING3213							3213

#define IDS_STRING3220							3220
#define IDS_STRING3221							3221
#define IDS_STRING3222							3222
#define IDS_STRING3223							3223
#define IDS_STRING3224							3224

#define IDS_STRING3230							3230
#define IDS_STRING3231							3231
#define IDS_STRING3232							3232

#define IDS_STRING3240							3240
#define IDS_STRING3241							3241
#define IDS_STRING3242							3242
#define IDS_STRING3243							3243
#define IDS_STRING3244							3244
#define IDS_STRING3245							3245
#define IDS_STRING3246							3246

#define IDS_STRING3250							3250
#define IDS_STRING3251							3251
#define IDS_STRING3252							3252
#define IDS_STRING3253							3253
#define IDS_STRING3254							3254
#define IDS_STRING3255							3255
#define IDS_STRING3256							3256
#define IDS_STRING3257							3257
#define IDS_STRING3258							3258
#define IDS_STRING3259							3259
#define IDS_STRING3260							3260
#define IDS_STRING3261							3261
#define IDS_STRING3262							3262
#define IDS_STRING3263							3263
#define IDS_STRING3264							3264
#define IDS_STRING3265							3265
#define IDS_STRING3266							3266
#define IDS_STRING3267							3267
#define IDS_STRING3268							3268
#define IDS_STRING3269							3269
#define IDS_STRING3270							3270
#define IDS_STRING3271							3271
#define IDS_STRING3272							3272
#define IDS_STRING3273							3273
#define IDS_STRING3274							3274
#define IDS_STRING3275							3275
#define IDS_STRING3276							3276
#define IDS_STRING3277							3277
#define IDS_STRING3278							3278
#define IDS_STRING3279							3279


#define IDS_STRING329								329

#define IDS_STRING330								330
#define IDS_STRING3300							3300
#define IDS_STRING3301							3301
#define IDS_STRING3302							3302

#define IDS_STRING3303							3303
#define IDS_STRING3304							3304
#define IDS_STRING3305							3305

#define IDS_STRING3306							3306
#define IDS_STRING3307							3307
#define IDS_STRING3308							3308

#define IDS_STRING3310							3310
#define IDS_STRING3311							3311
#define IDS_STRING3312							3312
#define IDS_STRING3313							3313
#define IDS_STRING3314							3314

#define IDS_STRING3315							3315
#define IDS_STRING3316							3316
#define IDS_STRING3317							3317
#define IDS_STRING3318							3318
#define IDS_STRING3319							3319
#define IDS_STRING33190							33190

#define IDS_STRING3320							3320
#define IDS_STRING3321							3321

#define IDS_STRING3322							3322
#define IDS_STRING3323							3323
#define IDS_STRING3324							3324
#define IDS_STRING3325							3325
#define IDS_STRING3326							3326


#define IDS_STRING3330							3330
#define IDS_STRING3331							3331

#define IDS_STRING3332							3332


#define IDS_STRING3440							3440
#define IDS_STRING3441							3441
#define IDS_STRING3442							3442
#define IDS_STRING3443							3443
#define IDS_STRING3444							3444
#define IDS_STRING3445							3445
#define IDS_STRING3446							3446
#define IDS_STRING3447							3447
#define IDS_STRING3448							3448
#define IDS_STRING3449							3449
#define IDS_STRING3450							3450
#define IDS_STRING3451							3451

#define IDS_STRING350								350
#define IDS_STRING351								351
#define IDS_STRING352								352
#define IDS_STRING353								353
#define IDS_STRING354								354
#define IDS_STRING355								355
#define	IDS_STRING356								356
#define	IDS_STRING357								357
#define IDS_STRING360								360

#define IDS_STRING370								370
#define IDS_STRING3700							3700
#define IDS_STRING3701							3701
#define IDS_STRING3702							3702
#define IDS_STRING3703							3703
#define IDS_STRING3704							3704
#define IDS_STRING3705							3705
#define IDS_STRING3706							3706
#define IDS_STRING3707							3707
#define IDS_STRING3708							3708
#define IDS_STRING3709							3709
#define IDS_STRING3710							3710
#define IDS_STRING3711							3711

#define IDS_STRING380								380

#define IDS_STRING390								390
#define IDS_STRING391								391

#define IDS_STRING400								400

#define IDS_STRING4100							4100
#define IDS_STRING4101							4101
#define IDS_STRING4102							4102
#define IDS_STRING4103							4103
#define IDS_STRING4104							4104
#define IDS_STRING4105							4105
#define IDS_STRING4106							4106
#define IDS_STRING4107							4107
#define IDS_STRING4120							4120
#define IDS_STRING4121							4121

#define IDS_STRING4200							4200
#define IDS_STRING4201							4201
#define IDS_STRING4202							4202
#define IDS_STRING4203							4203
#define IDS_STRING4204							4204

#define IDS_STRING500								500

#define IDS_STRING600								600
#define IDS_STRING601								601

#define IDS_STRING5000							5000

#define IDS_STRING6000							6000
#define IDS_STRING6001							6001
#define IDS_STRING6002							6002
#define IDS_STRING6003							6003

#define IDS_STRING6010							6010
#define IDS_STRING6011							6011

#define IDS_STRING6100							6100

#define IDS_STRING50000							50000
#define IDS_STRING50010							50010
#define IDS_STRING50020							50020
#define IDS_STRING50030							50030
#define IDS_STRING50040							50040
#define IDS_STRING50041							50041
#define IDS_STRING50042							50042
#define IDS_STRING50043							50043
#define IDS_STRING50100							50100
#define IDS_STRING50110							50110
#define IDS_STRING50120							50120
#define IDS_STRING50130							50130
#define IDS_STRING50140							50140
#define IDS_STRING50200							50200
#define IDS_STRING50210							50210
#define IDS_STRING50211							50211
#define IDS_STRING50212							50212
#define IDS_STRING50213							50213
#define IDS_STRING50220							50220
#define IDS_STRING50221							50221
#define IDS_STRING50300							50300
#define IDS_STRING50310							50310
#define IDS_STRING50320							50320
#define IDS_STRING50330							50330

#define IDS_STRING701							701
#define IDS_STRING702							702
#define IDS_STRING703							703

#define IDS_STRING7000							7000
#define IDS_STRING7001							7001
#define IDS_STRING7002							7002
#define IDS_STRING7003							7003
#define IDS_STRING7004							7004
#define IDS_STRING7005							7005

#define IDS_STRING8000							8000
#define IDS_STRING8001							8001
#define IDS_STRING8002							8002
#define IDS_STRING8003							8003
#define IDS_STRING8004							8004
#define IDS_STRING8005							8005
#define IDS_STRING8006							8006
#define IDS_STRING8007							8007
#define IDS_STRING8008							8008
#define IDS_STRING8009							8009

// Identifer for functions
#define ID_BRANDELS						1000
#define ID_BRANDELS_UB				1100
#define ID_NASLUNDS						1001
#define ID_NASLUNDS_UB				1101
#define ID_HAGBERG_MATERN			1002	// Volumefunctions for "Ek" and "Book"
#define ID_HAGBERG_MATERN_UB	1102	// Volumefunctions for "Ek" and "Book", "Under bark"; 090305 p�d
#define ID_CYLINDER						1003	// Cylinder volume

#define ID_H25							1500
#define ID_REGRESSION				1501
#define ID_SODERBERGS				1502

#define ID_BARK_JO					2000
#define ID_BARK_SF					2001	// Skogforsk Sf_tall och Sf_gran
#define ID_BARK_SO					2002	// S�derbergs barkfunktioner

#define ID_OMF_FACTOR_UB		3000	// Special value; 071016 p�d

/////////////////////////////////////////////////////////////////////////////////////////////////
// enumrated column values used in:
//	PageTwoFormView
//	TraktSelListFormView

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22,
	COLUMN_23,
	COLUMN_24,
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Type of tree; 070510 p�d

const LPCTSTR TREE_TYPE = _T("Provtr�d (Uttag)");

/////////////////////////////////////////////////////////////////////////////////////////////////
// Jonsson Boniteter; 070510 p�d
#define NUMOF_JONSSON_BONITET	15
const LPCTSTR JONSSON_BONITETS[NUMOF_JONSSON_BONITET] = {_T("I"),_T("I/II"),_T("II"),_T("II/III"),_T("III"),
																												 _T("III/IV"),_T("IV"),_T("IV/V"),_T("V"),_T("V/VI"),
																												 _T("VI"),_T("VII"),_T("VIII"),_T("IMP"),_T("")};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated NEW or UPDATE
typedef enum { NO_ACTION,NEW_ITEM, UPD_ITEM } enumACTION;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for treetype same as in UCCalculate; 070703 p�d
#define SAMPLE_TREE						0		// A sampletree. I.e. it has a heigth etc
#define TREE_OUTSIDE					1		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
#define TREE_OUTSIDE_NO_SAMP	2		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
#define TREE_SAMPLE_REMAINIG	3		// "Provtr�d (Kvarl�mmnat)"
#define TREE_SAMPLE_EXTRA			4		// "Provtr�d (Extra inl�sta)"
#define TREE_OUTSIDE_LEFT 5	// Kanttr�d l�mnas (Provtr�d)
#define TREE_OUTSIDE_LEFT_NO_SAMP 6	// Kanttr�d l�mnas  (Ber�knad h�jd)
#define TREE_POS_NO_SAMP 7	// Pos.tr�d (uttag)
#define TREE_INSIDE_LEFT 8	// Tr�d i gatan l�mnas (Provtr�d)
#define TREE_INSIDE_LEFT_NO_SAMP 9	// Tr�d i gatan l�mnas (Ber�knad h�jd)

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines type of "St�mplingsl�ngd"; 071121 p�d
/*
const int STMP_LEN_WITHDRAW				=	1;		// "St�mplingsl�ngd - Uttag"
const int STMP_LEN_TO_BE_LEFT			=	2;		// "St�mplingsl�ngd - Kvarl�mmnat"
const int STMP_LEN_WITHDRAW_ROAD	= 3;		// "St�mplingsl�ngd - Uttag i Stickv�g"
*/
/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated type for determin' kind of assortment; 070531 p�d
#define ASSORTMENT_TIMBER_IN_M3TO				0
#define ASSORTMENT_TIMBER_IN_M3FUB			3
#define ASSORTMENT_NOT_PULP_NOT_TMBER		1
#define ASSORTMENT_PULPWOOD							2

//////////////////////////////////////////////////////////////////////////////////////////
// Identifies type of template; 070903 p�d
// OBS! IDENTIFERS SET IN UMTemplates
// 1 = Trakt	2 = Objekt etc.
#define ID_TEMPLATE_TRAKT			1
#define ID_TEMPLATE_OBJECT		2

//////////////////////////////////////////////////////////////////////////////////////////
// Type of cost template; 071008 p�d
#define COST_TYPE_1									1	// "Kostnader f�r Rotpost"
#define COST_TYPE_2									2	// "Kostnader f�r Rotpost i LandValue"

//////////////////////////////////////////////////////////////////////////////////////////
// Identifies type of template; 070903 p�d
// 1 = Trakt	2 = Objekt etc.
#define ID_TRAKT_USE_TEMPLATE	1
#define ID_TRAKT_NO_TEMPLATE	2

// Identifies that this is a pricelist used for calulation (utbyte)
// using Rune Ollas calculations; 071024 p�d
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_1			1		// Pricelist for R. Ollars
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_2			2		// "Simple" avg. prices per assortment

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for "Rotpost" origin
#define ROTPOST_ORIGIN1			1		// "Vanlig" rotpost
#define ROTPOST_ORIGIN2_1		2		// "Intr�ngsv�rdering" rotpost "Kostnadsmall med huggningskostnad kr/m3"
#define ROTPOST_ORIGIN2_2		3		// "Intr�ngsv�rdering" rotpost "Kostnadsmall med huggningskostnad enl. tabell DGV"

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for "Rotpost" origin
#define OTHER_COST_0				0		// "Kostnader f�r 'Vanlig' rotpost i kostndsmall"
#define OTHER_COST_1				1		// "�vriga intr�ngskostnader"
#define OTHER_COST_2				2		// "�vriga fasta kostnader"

// Identifies that this is a pricelist used for calulation (utbyte)
// using Rune Ollas calculations; 080303 p�d
// OBS! Defines also in UMPricelists; 080303
#define ID_PRICELIST_IN_DEVELOPEMENT_1				-1	// Pricelist isn't ready yet; 071219 p�d
#define ID_PRICELIST_IN_DEVELOPEMENT_2				-2	// Pricelist isn't ready yet; 071219 p�d
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_1			1		// Pricelist for R. Ollars
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_2			2		// "Simple" avg. prices per assortment

// Use this root key in my own registry settings; 060113 p�d
const LPCTSTR REG_ROOT_HMSSHELL				= _T("Software\\HaglofManagmentSystem\\HMSShell");

const LPCTSTR PROGRAM_NAME						= _T("UMEstimate");				// Used for Languagefile, registry entries etc.; 051114 p�d

const LPCTSTR LICENSE_NAME						= _T("License");				// Used for Languagefile for License; 080901 p�d
const LPCTSTR LICENSE_FILE_NAME				= _T("License.dll");		// Used on setting up filename and path; 080901 p�d
const LPCTSTR LICENSE_GET_LIC					= _T("GetLicense");

const LPCTSTR SHELL_PROGRAM_NAME			= _T("HMSShell");				// Used for Languagefile for Main program (HMSShell.exe); 090119 p�d

// STD_REPORTS xml-file, holds Standard reports per Suite/Usermodule; 070326 p�d
const LPCTSTR STD_REPORTS_FILE				= _T("std_reports.xml");

	const LPCTSTR REG_STAND_ENTRY_KEY			= _T("UMEstimate\\StandEntry\\Placement");

	const LPCTSTR REG_WP_PAGE2_REPORT_KEY	= _T("UMEstimate\\StandEntryPage2Frame\\Report");	// 070119 p�d
	const LPCTSTR REG_WP_PAGE3_REPORT_KEY	= _T("UMEstimate\\StandEntryPage3Frame\\Report");	// 070119 p�d

	const LPCTSTR REG_WP_TRAKT_TYPE_KEY		= _T("UMEstimate\\TraktTypeFrame\\Placement");	// 070228 p�d

	const LPCTSTR REG_WP_ORIGIN_KEY				= _T("UMEstimate\\OriginFrame\\Placement");	// 070228 p�d

	const LPCTSTR REG_WP_INPUT_METHOD_KEY	= _T("UMEstimate\\InputMethodFrame\\Placement");	// 070228 p�d

	const LPCTSTR REG_WP_HGT_CLASS_KEY			= _T("UMEstimate\\HgtClassFrame\\Placement");	// 070228 p�d

	const LPCTSTR REG_WP_CUT_CLASS_KEY			= _T("UMEstimate\\CutClassFrame\\Placement");	// 070228 p�d

	const LPCTSTR REG_WP_PROPERTY_SELLEIST_FRAME_KEY	= _T("UMEstimate\\PropertySelListFrame\\Placement");	// 070117 p�d

	const LPCTSTR REG_WP_PROPERTY_SELLEIST_REPORT_KEY	= _T("UMEstimate\\PropertySelListFrame\\Report");	// 070105 p�d

	const LPCTSTR REG_WP_PRICELIST_SELLEIST_FRAME_KEY	= _T("UMEstimate\\PricelistSelListFrame\\Placement");	// 070306 p�d

	const LPCTSTR REG_WP_PRICELIST_SELLEIST_REPORT_KEY	= _T("UMEstimate\\PricelistSelListFrame\\Report");	// 070306 p�d

	const LPCTSTR REG_WP_TRAKT_SELLEIST_FRAME_KEY	= _T("UMEstimate\\TraktSelListFrame\\Placement");	// 070306 p�d

	const LPCTSTR REG_WP_TRAKT_SELLEIST_REPORT_KEY	= _T("UMEstimate\\TraktSelListFrame\\Report");	// 070306 p�d

	const LPCTSTR REG_WP_PLOT_SELLEIST_REPORT_KEY	= _T("UMEstimate\\PlotSelListFrame\\Report");	// 070510 p�d

	const LPCTSTR REG_WP_PLOT_SELLEIST_FRAME_KEY	= _T("UMEstimate\\PlotSelListFrame\\Placement");	// 070510 p�d

	const LPCTSTR REG_SPECIE_DATA_KEY							= _T("UMEstimate\\SpecieData\\Placement");

	const LPCTSTR REG_WP_INFORMATION_FRAME_KEY		= _T("UMEstimate\\InformationFrame\\Placement");	// 070510 p�d

	const LPCTSTR REG_WP_SAMPLE_TREE_REPORT_KEY	= _T("UMEstimate\\SampleTreeFrame\\Report");	// 070306 p�d

	const LPCTSTR REG_WP_DCLS_TREE_REPORT_KEY		= _T("UMEstimate\\DCLSTreeFrame\\Report");	// 070306 p�d
	const LPCTSTR REG_WP_DCLS1_TREE_REPORT_KEY	= _T("UMEstimate\\DCLS1TreeFrame\\Report");	// 070306 p�d
	const LPCTSTR REG_WP_DCLS2_TREE_REPORT_KEY	= _T("UMEstimate\\DCLS2TreeFrame\\Report");	// 070306 p�d

	const LPCTSTR REG_WP_STAND_ENTRY_LAYOUT_KEY	= _T("UMEstimate_Layout_StandEntry");	// 070215 p�d

	const LPCTSTR REG_WP_SPECIE_DATA_LAYOUT_KEY	= _T("UMEstimate_Layout_SpecieData");	// 070402 p�d

	const LPCTSTR REG_PRINT_PRICELIST_KEY		= _T("UMEstimate\\PrintPricelists\\Placement");

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script files, used in GallBas; 061120 p�d
/* NOT USED
const LPCTSTR SQL_SERVER_SQRIPT_DIRECTORY						= _T("Microsoft SQL Server");

const LPCTSTR ESTI_TABLES														= _T("Create_esti_tables.sql");
const LPCTSTR ESTI_MISC_TABLES											= _T("Create_esti_misc_tables.sql");
const LPCTSTR ESTI_TRAKT_PLOT_TREES_ASSORT_TABLES		= _T("Create_esti_trakt_plot_trees_assort_table.sql");
const LPCTSTR ESTI_TRAKT_SETTINGS_TABLES						= _T("Create_esti_trakt_settings_tables.sql");
const LPCTSTR ESTI_TRAKT_ROTPOST_TABLES							= _T("Create_esti_trakt_rotpost_table.sql");
*/
//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d

//const LPCTSTR TOOLBAR_RES_DLL								= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d
const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_FILTER						= 39;
const int RES_TB_FILTER_OFF				= 8;
const int RES_TB_TOOLS						= 47;
const int RES_TB_PRINT						= 36;
const int RES_TB_REFRESH					= 43;
const int RES_TB_EXECUTE					= 46;
const int RES_TB_ADD							= 0;
const int RES_TB_DEL							= 22;
const int RES_TB_INFO							= 20;
const int RES_TB_FOLDER						= 12;
const int RES_TB_PREVIEW					= 31;
const int RES_TB_UPDATE						= 43;
const int RES_TB_IMPORT						= 19;
const int RES_TB_GIS							= 74;

const LPCTSTR RSTR_TB_FILTER							= _T("Tabellfilter");
const LPCTSTR RSTR_TB_FILTER_OFF					= _T("Endfilter");
const LPCTSTR RSTR_TB_TOOLS								= _T("Verktyg");
const LPCTSTR RSTR_TB_PRINT								= _T("Skrivut");
const LPCTSTR RSTR_TB_REFRESH							= _T("Utrop");
const LPCTSTR RSTR_TB_ADD									= _T("Add");			
const LPCTSTR RSTR_TB_DEL									= _T("Minus");		
const LPCTSTR RSTR_TB_INFO								= _T("Info");		
const LPCTSTR RSTR_TB_FOLDER							= _T("Folder");		
const LPCTSTR RSTR_TB_PREVIEW							= _T("Preview");		
const LPCTSTR RSTR_TB_UPDATE							= _T("Uppdatera");		
const LPCTSTR RSTR_TB_IMPORT							= _T("Import");		

//////////////////////////////////////////////////////////////////////////////////////////
// Filename of logfile

const LPCTSTR ROTPOST_LOG_FILE_NAME				= _T("Rotpost_Log");


const LPCTSTR PRL_VIEW_FN									= _T("tmp_prl_view.htm");

//////////////////////////////////////////////////////////////////////////////////////////
// Colors
#define CYAN					RGB(		0,  255,  255)
#define LTCYAN				RGB(  224,  255,  255)

//////////////////////////////////////////////////////////////////////////////////////////
//	

//////////////////////////////////////////////////////////////////////////////////////////
// Database tables
/*
const LPCTSTR TBL_TRAKT										= "esti_trakt_table";

const LPCTSTR TBL_TRAKT_DATA								= "esti_trakt_data_table";
const LPCTSTR TBL_TRAKT_SPC_ASS						= "esti_trakt_spc_assort_table";
const LPCTSTR TBL_TRAKT_TRANS							= "esti_trakt_trans_assort_table";


const LPCTSTR TBL_TRAKT_MISC_DATA					= "esti_trakt_misc_data_table";

const LPCTSTR TBL_TRAKT_PLOT								= "esti_trakt_plot_table";
const LPCTSTR TBL_TRAKT_SAMPLE_TREES				= "esti_trakt_sample_trees_table";
const LPCTSTR TBL_TRAKT_DCLS_TREES					= "esti_trakt_dcls_trees_table";
const LPCTSTR TBL_TRAKT_DCLS1_TREES				= "esti_trakt_dcls1_trees_table";
const LPCTSTR TBL_TRAKT_DCLS2_TREES				= "esti_trakt_dcls2_trees_table";
const LPCTSTR TBL_TRAKT_TREES_ASSORT				= "esti_trakt_trees_assort_table";
const LPCTSTR TBL_TRAKT_ROTPOST						= "esti_trakt_rotpost_table";
const LPCTSTR TBL_TRAKT_ROTPOST_SPC				= "esti_trakt_rotpost_spc_table";
const LPCTSTR TBL_TRAKT_ROTPOST_OTC				= "esti_trakt_rotpost_other_table";
*/
// This table holds userdefined names for Stands 
// e.g. "Avdelning","Trakt","Best�nd" etc; 070228 p�d
/*
const LPCTSTR TBL_TRAKT_TYPE								= "esti_trakt_type_table";
const LPCTSTR TBL_ORIGIN										= "esti_origin_table";
const LPCTSTR TBL_INPUT_METHOD							= "esti_inpdata_table";
const LPCTSTR TBL_HGT_CLASS								= "esti_hgtcls_table";
const LPCTSTR TBL_CUT_CLASS								= "esti_cutcls_table";
*/

const LPCTSTR TBL_PROPERTY									= _T("fst_property_table");
const LPCTSTR TBL_SPECIES										= _T("fst_species_table");

const LPCTSTR TBL_PRICELISTS								= _T("prn_pricelist_table");

const LPCTSTR TBL_TEMPLATE									= _T("tmpl_template_table");
const LPCTSTR TBL_COSTS_TEMPLATE						= _T("cost_template_table");

const LPCTSTR TBL_SAMPLE_TREE_CATEGORY	= _T("esti_sample_trees_category_table");


//////////////////////////////////////////////////////////////////////////////////////////
//
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_X_SIZE										= 50; //400;
const int MIN_Y_SIZE										= 50; //250;

const int MIN_X_SIZE_TRAKT_TYPE					= 50; //400;
const int MIN_Y_SIZE_TRAKT_TYPE					= 50; //250;

const int MIN_X_SIZE_ORIGIN							= 50; //400;
const int MIN_Y_SIZE_ORIGIN							= 50; //250;

const int MIN_X_SIZE_INPUT_METHOD				= 50; //400;
const int MIN_Y_SIZE_INPUT_METHOD				= 50; //250;

const int MIN_X_SIZE_HGT_CLASS					= 50; //400;
const int MIN_Y_SIZE_HGT_CLASS					= 50; //250;

const int MIN_X_SIZE_CUT_CLASS					= 50; //400;
const int MIN_Y_SIZE_CUT_CLASS					= 50; //250;

const int MIN_X_SIZE_PROPERTY_SELLIST		= 50; //400;
const int MIN_Y_SIZE_PROPERTY_SELLIST		= 50; //250;

const int MIN_X_SIZE_PRICELIST_SELLIST	= 50; //400;
const int MIN_Y_SIZE_PRICELIST_SELLIST	= 50; //250;

const int MIN_X_SIZE_SPECIE_DATA				= 50; //400;
const int MIN_Y_SIZE_SPECIE_DATA				= 50; //250;

const int MIN_X_SIZE_TRAKT_SELLIST			= 50; //400;
const int MIN_Y_SIZE_TRAKT_SELLIST			= 50; //250;

const int MIN_X_SIZE_PLOT_SELLIST				= 50; //400;
const int MIN_Y_SIZE_PLOT_SELLIST				= 50; //250;

const int MIN_X_SIZE_INFOVIEW						= 50; //400;
const int MIN_Y_SIZE_INFOVIEW						= 50; //250;

const int MIN_X_SIZE_PRICELISTS_LIST		= 450;
const int MIN_Y_SIZE_PRICELISTS_LIST		= 300;

//////////////////////////////////////////////////////////////////////////////////////////
// Buffer
#define BUFFER_SIZE		1024*100		// 100 kb buffer

//////////////////////////////////////////////////////////////////////////////////////////
// Max value for Diameterclass (in cm)
#define MAX_DIAM_CLASS		500.0

//////////////////////////////////////////////////////////////////////////////////////////
//	Set limits;
//	Areal: max= 999.999 min=0.001
//	Number of trees in Diameterclass = 32000 ~ the size of an integer
#define AREAL_MAX_SIZE		999.999	// ha
#define AREAL_MIN_SIZE		0.001		// ha

#define NUMOF_TREES_IN_DCLS	32000

#define MAX_TO_D_CLASS 2500

//////////////////////////////////////////////////////////////////////////////////////////
// Deafult diameterclass; 080411 p�d
#define DEF_DIAMCLASS					2.0

//////////////////////////////////////////////////////////////////////////////////////////
// Max percent 
#define MAX_PERCENT				100.0

//////////////////////////////////////////////////////////////////////////////////////////
// This structure's used to hold information on specie(s) not havin' any functions
// added. I.e. no volume-,bark- or heightfunction; 070626 p�d
/*
typedef struct _func_spc_struct
{
	int nSpcID;
	CString sSpcName;
	_func_spc_struct(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
	}
	_func_spc_struct(int spc_id,LPCTSTR spc_name)
	{
		nSpcID = spc_id;
		sSpcName = _T(spc_name);
	}
} FUNC_SPC_STRUCT;

typedef std::vector<FUNC_SPC_STRUCT> vecFuncSpc;
*/

//////////////////////////////////////////////////////////////////////////
// CMyPropGridItem_validate derived from CXTPPropertyGridItemDouble; 081126 p�d
typedef	enum { VALUE_LT,	// Value less than
							 VALUE_LE,	// Value less than or equal
							 VALUE_GT,	// Value greater than
							 VALUE_GE,	// Value greater than or equal
							 VALUE_NO		// No validation
						} enumValidateType;

class CMyPropGridItemDouble_validate : public CXTPPropertyGridItemDouble
{
//private:
	double m_fValidateValue;
	CString m_sMsgCap;
	CStringArray m_arrMsg;
	enumValidateType m_enumValidateType;
	short m_nNumOfMsg;
public:
	CMyPropGridItemDouble_validate(LPCTSTR cap,LPCTSTR dec,LPCTSTR msgCap,CStringArray& msg,double validate_value,enumValidateType validateType = VALUE_NO)
		: CXTPPropertyGridItemDouble(cap,0.0,dec),
		  m_fValidateValue(validate_value),
			m_sMsgCap(msgCap),
			m_enumValidateType(validateType)
	{
		m_arrMsg.Append(msg);
		m_nNumOfMsg = m_arrMsg.GetCount();
	}

	virtual ~CMyPropGridItemDouble_validate(void)
	{
		m_arrMsg.RemoveAll();
	}

	virtual void OnValueChanged(CString strValue)
	{
		CString sMsg;
		BOOL bIsValueOK = TRUE;
		// Do the actual validation of entered data.
		// Criteria: if value entered is <= to validate value
		// set this as the new value, otherwise set the old value; 081126 p�d
		double fEnteredValue = _tstof(strValue);
		// Check type of validation
		switch (m_enumValidateType)
		{
			case VALUE_LT :
				if (fEnteredValue >= m_fValidateValue)
				{

					if (m_nNumOfMsg > 0)
						sMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(0),m_fValidateValue);
					else
						sMsg.Format(_T("Value must be LT : %.2f"),m_fValidateValue);

					bIsValueOK = FALSE;
				}
			break;
			case VALUE_LE :
				if (fEnteredValue > m_fValidateValue)
				{
					if (m_nNumOfMsg > 1)
						sMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(1),m_fValidateValue);
					else
						sMsg.Format(_T("Value must be LE : %.2f"),m_fValidateValue);
					bIsValueOK = FALSE;
				}
			break;
			case VALUE_GT :
				if (fEnteredValue <= m_fValidateValue)
				{
					if (m_nNumOfMsg > 2)
						sMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(2),m_fValidateValue);
					else
						sMsg.Format(_T("Value must be GT : %.2f"),m_fValidateValue);
					bIsValueOK = FALSE;
				}
			break;
			case VALUE_GE :
				if (fEnteredValue < m_fValidateValue)
				{
					if (m_nNumOfMsg > 3)
						sMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(3),m_fValidateValue);
					else
						sMsg.Format(_T("Value must be GE : %.2f"),m_fValidateValue);
					bIsValueOK = FALSE;
				}
			break;
		};

		if (bIsValueOK)
			SetDouble(fEnteredValue);
		else
		{
			SetDouble(0.0);
			strValue = _T("0.0");
			UMMessageBox(0,sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}

		CXTPPropertyGridItemDouble::OnValueChanged(strValue);
	}
};

// Added 2009-06-05 P�D
class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};


typedef std::vector<Scripts> vecScriptFiles;

class CChartLayers
{
	int m_nSpcID;
	CString m_sSpcName;
	bool m_bUse;
public:
	CChartLayers(void)
	{
		m_nSpcID = -1;
		m_sSpcName = L"";
		m_bUse = true;
	}
	CChartLayers(int spc_id,LPCTSTR spc_name,bool _use = true)
	{
		m_nSpcID = spc_id;
		m_sSpcName = spc_name;
		m_bUse = _use;
	}
	int getSpcID()				{ return m_nSpcID; }
	CString getSpcName()	{ return m_sSpcName; }
	bool getUse()					{ return m_bUse; }
};
typedef	std::vector<CChartLayers> vecLineLayers;


typedef	std::vector<const char*> vecStr;
class StrArr
{
//private:
	char **_strArr;
	int numOf;
	vecStr vecArr;
protected:
	void _add(int idx,const char *s)	
	{
		_strArr[idx] = (char*)malloc(strlen(s)+1);
		strcpy(_strArr[idx],s);	
	}
public:
	StrArr()
	{
		vecArr.clear();
		numOf = 0;
		_strArr = (char**)malloc(numOf*sizeof(*_strArr));
	}
	StrArr(int num)
	{
		if (num > 0)
		{
			numOf = num;
			_strArr = (char**)malloc(num*sizeof(*_strArr));
		}
	}
	StrArr(const vecStr &vec)
	{
		vecArr = vec;
		numOf = vec.size();
		_strArr = (char**)malloc(numOf*sizeof(*_strArr));
		for (UINT i = 0;i < vec.size();i++)	{ _add(i,vec[i]); }
	}

	void set(int num)
	{
		if (num > 0)
		{
			numOf = num;
			_strArr = (char**)malloc(num*sizeof(*_strArr));
		}
	}
	
	void add(int idx,const char *s)	{ _add(idx,s); }

	char* operator [](UINT i)	{ return _strArr[i]; }

	char** get()	{ return _strArr; }

	void free()	{ ::free(_strArr); }

	int numof()	{ return numOf; }
};


typedef std::vector<double> vecDbl;
class DblArr
{
//private:
	double *_dblArr;
	int numOf;
	vecDbl vecArr;
protected:
	void _add(int idx,double n)	
	{
		_dblArr[idx] = n;
	}
public:
	DblArr()
	{
		vecArr.clear();
		numOf = 0;
		_dblArr = NULL;
	}
	DblArr(int num)
	{
		if (num > 0)
		{
			numOf = num;
			_dblArr = (double*)malloc(num*sizeof(double));
		}
	}
	DblArr(const vecDbl &vec)
	{
		vecArr = vec;
		numOf = vec.size();
		_dblArr = (double*)malloc(numOf*sizeof(double));
		for (UINT i = 0;i < vec.size();i++)	{ _add(i,vec[i]); }
	}

	void add(int idx,double n)	{ _add(idx,n); }

	double operator [](UINT i)	{ return _dblArr[i]; }

	double* get()	{ return _dblArr; }
	double get2()	{ return *_dblArr; }

	void free()	{ ::free(_dblArr); }

	int numof()	{ return numOf; }
};

class CBarArray : public DblArr
{
	int m_nSpcID;
	CString m_sSpcName;
	double m_fMinDiam;
public:
	CBarArray(void)	: DblArr()
	{
		m_nSpcID = -1;
		m_sSpcName = L"";
		m_fMinDiam = 0.0;
	}
	CBarArray(int spc_id,LPCTSTR spc_name,double min_diam,vecDbl _vec) : DblArr(_vec)
	{
		m_nSpcID = spc_id;
		m_sSpcName = spc_name;
		m_fMinDiam = min_diam;
	}

	int getSpcID()				{ return m_nSpcID; }
	CString getSpcName()	{ return m_sSpcName; }
	double getMinDiam()		{ return m_fMinDiam; }
};

typedef	std::vector<CBarArray> vecBarArray;

/*
//////////////////////////////////////////////////////////////////////////////////////////
// Containerclass for Transaction records; 100331 p�d
namespace CCDC
{
	class CCommonDataContainer
	{
		protected:
			static CTransaction_trakt_misc_data m_recTraktMiscData;
			static vecTransactionSampleTree m_vecTraktSampleTrees;
			static vecTransactionTraktData m_vecTraktData;
		public:
			CCommonDataContainer(void);
			virtual ~CCommonDataContainer(void);
	};

}
*/
//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

CString getResStr(UINT id);

CString getToolBarResourceFN(void);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp = -1);
CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
//BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name);
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table,CString database_name);
// Added 2009-06-05 p�d
BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action);

BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec);

// Generic functions (multiused), for getting data from
// FormView classes; 070515 p�d
int getInvMethodIndex(void);
int getGroupID(void);
void setGroupID(int id);
void setReportRecordGroupID(int id);
void setResetReportRecordSelected(void);
void setGroupName(int grp_id);
void setGroupName(LPCTSTR name);
void enableTabPage(int idx,BOOL enable);
CTransaction_trakt* getActiveTrakt(void);
CTransaction_trakt_data* getActiveTraktDataOnClick(void);
CTransaction_trakt_data* getActiveTraktData(void);
CTransaction_trakt_misc_data* getActiveTraktMiscData(void);
void populatePageThree(void);
void setIsDirtyPageThree(void);
enumACTION getPageOneAction(void);
void setStatusBarText(int index,LPCTSTR text);
BOOL populateDataInPageOne(void);
BOOL populateDataInPageOne(int trakt_id);
BOOL populateDataInPageThree(void);
BOOL isSpecieDialogFrameDirty(void);

BOOL isCruiseInObject(int ecru_id);

int SplitString(const CString& input,const CString& delimiter, CStringArray& results);

// OBS! These methods also in UMTemplates; 070903 p�d
void getTemplateMainSettings(TemplateParser *pars,int *,int *,LPTSTR,LPTSTR,double *,int *,LPTSTR,
																						 bool *,bool *,bool *,bool *,LPTSTR);
void getTemplateLongitudeSettings(TemplateParser *pars,int *);
void getTemplateSettings(TemplateParser *pars,int*,LPTSTR,int*,int*,LPTSTR,double*);
void getTemplateSpecies(TemplateParser *pars,vecTransactionSpecies &vec);
void getTemplateFunctionsPerSpcecie(TemplateParser *pars,int spc_id,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const);
void getTemplateMiscFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *qdesc_index,LPTSTR,double *,double *,double *,int *,int *,double *);
void getTemplateGrotFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *,double *,double *,double *);
void getTemplateTransfersPerSpcecie(TemplateParser *pars,int spc_id,vecTransactionTemplateTransfers &);

// Static variable, check if user's opened a window (doesn't matter which).
// Only show "Demo - number of days"-message once.
static BOOL bShowLicenseDialog = TRUE;
BOOL License(void);

BOOL runDoCalulationInPageThree(int spc_id = -1,BOOL remove_trakt_trans = FALSE,BOOL check_changes = TRUE,BOOL show_msg = TRUE,bool do_transfers = true,bool do_populate = true);
int runCalculationsFomDB(CUMEstimateDB *pDB,BOOL is_connected,int spc_id,CTransaction_trakt &recTrakt,BOOL remove_trakt_trans = FALSE);

BOOL runDoHeightCurveToDBInPageThree(int trakt_id = -1);

CString formatData(LPCTSTR fmt,...);

int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText, ...);
int GetSelectedItem(CListCtrl &ctrl);

void SecurityMessage(HWND hwnd,LPCTSTR lang_fn,LPCTSTR date);

void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);

BOOL checkPricelist(LPCTSTR prl_xml);

// Added 2009-10-14 p�d
// USED IN AN EXPORTED FUNCTION (For Peter L.); 091014 p�d
extern "C" BOOL __declspec(dllexport) export_createTraktFromTemplate(CString sTemplateXMLFile,DB_CONNECTION_DATA& conn,int *trakt_id,TCHAR *username = NULL);
extern "C" BOOL __declspec(dllexport) calculateStand(int trakt_id, DB_CONNECTION_DATA &conn);
extern "C" BOOL __declspec(dllexport) changeTemplateFix(int trakt_id, LPCTSTR templateXml, LPCTSTR pricelistXml, DB_CONNECTION_DATA &conn);
