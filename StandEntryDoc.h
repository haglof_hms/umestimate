#if !defined(__STANDENTRYDOC_H__)
#define __STANDENTRYDOC_H__

#include "GeneralInfoDlg.h"

#include "UMEstimateDB.h"

#include "XHTMLStatic.h"

extern bool global_SHOW_ONLY_ONE_STAND;
extern bool global_SHOW_ONLY_ONE_STAND_FROM_GIS;


//////////////////////////////////////////////////////////////////////////
// CMyControlPopup
/*
class CMyControlPopup : public CXTPControlPopup
{
	DECLARE_XTP_CONTROL(CMyControlPopup)

	struct _menu_items
	{
		int m_nID;
		CString m_sText;

		_menu_items(int id,LPCTSTR text)
		{
			m_nID = id;
			m_sText = text;
		}
	};

	std::vector<_menu_items > vecMenuItems;
protected:
	virtual BOOL OnSetPopup(BOOL bPopup);
public:
	CMyControlPopup(void);
	void addMenuIDAndText(int id = -1,LPCTSTR text = _T(""));
};*/
class CMyControlPopup : public CXTPControlPopup
{
	DECLARE_XTP_CONTROL(CMyControlPopup)

	struct _menu_items
	{
		int m_nID;
		CString m_sText;
		int m_nSubMenuID;

		_menu_items(int id,LPCTSTR text,int sub_menu_id = 0)
		{
			m_nID = id;
			m_sText = (text);
			m_nSubMenuID = sub_menu_id;
		}
	};

	std::vector<_menu_items > vecMenuItems;
	std::vector<_menu_items > vecSubMenuItems;

protected:
	virtual BOOL OnSetPopup(BOOL bPopup);
public:
	CMyControlPopup(void);
	virtual ~CMyControlPopup(void);
	void addMenuIDAndText(int id = 0,LPCTSTR text = _T(""),int sub_menu_id = -999);
	void setSubMenuIDAndText(int id = 0,LPCTSTR text = _T(""),int sub_menu_id = -900);
	void clearVectors(void);
};

//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropGridItemBool : public CXTPPropertyGridItemBool
{
public:
	CMyPropGridItemBool(const CString true_text,
											const CString false_text,
											const CString& cap,
											BOOL bValue) : CXTPPropertyGridItemBool(cap,bValue)
	{
		SetTrueFalseText((true_text),(false_text));
	}
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDILicenseFrameDoc

class CMDILicenseFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDILicenseFrameDoc();
	DECLARE_DYNCREATE(CMDILicenseFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void PreCloseFrame(CFrameWnd *);
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDILicenseFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void PreCloseFrame(CFrameWnd *);
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

class CACBox : public CComboBox
{
public:
	CACBox();

	void SetLblFont(int size,int weight,LPCTSTR font_name = _T("Arial"));

	vecSTDReports m_vecReports;
	void setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index);
	void setLanguageFN(LPCTSTR lng_fn);

protected:
	CFont *m_fnt1;
	CString m_sLangFN;

	CString getLangStr(int id);

	//{{AFX_MSG(CACBox)
	afx_msg void OnDestroy();
	afx_msg BOOL OnCBoxChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CMDIStandEntryFormFrame

class CMDIStandEntryFormFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIStandEntryFormFrame)

//private:

	BOOL m_bInitReports;

	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	int m_nSelPrintOut;
	CString m_sShellDataFile;
	int m_nShellDataIndex;

	//BOOL m_bShowOnlyOneStand;

	vecSTDReports m_vecReports;
	CACBox m_cbPrintOuts;
	CMyExtTranspStatic m_lblPrintOuts;

	CString m_sLangFN;

	RECT toolbarRect;

	BOOL m_bIsNewTraktAdded;
	BOOL m_bPricelistChanged;

	CString m_sMessageCap;
	CString m_sUpdatePricelist;
	CString m_sPricelistMissing;
	CString m_sDiameterclassErr;
	CString m_sPricelistPulpError;
	CString m_sPricelistLessSpec;

	CString m_sExchange;
	CString m_sPricelist;
	CString m_sCalculateAs;
	CString m_sMiscSettings;
	CString m_sDiamClass;
	CString m_sCostTmpl;
	CString m_sSoderbergsSettings;
	CString m_sAtCoast;
	CString m_sSouthEast;
	CString m_sRegion5;
	CString m_sPartOfPlot;
	CString m_sSIH100_Pine;

	CString m_sYes;
	CString m_sNo;

	CString m_sNoStandsMsg1;
	CString m_sNoStandsMsg2;

	// Enable/Disable toolbar buttons ...
	BOOL m_bIsInfoTBtn;
	BOOL m_bIsAddTBtn;
	BOOL m_bIsPrintOutTBtn;
	BOOL m_bIsPrintPreviewTBtn;
	BOOL m_bIsCalculationTBtn;
	BOOL m_bIsINVFileTBtn;
	BOOL m_bIsToolsTBtn;
	
	BOOL m_bGISInstalled;

	BOOL m_bClosing;

	void setLanguage(void);

	BOOL m_bFirstOpen;
	// Bind to
	CString m_sExchPricelistStr;
	CString m_sPricelistSelected;

	//Lagt till en variablen f�r att h�lla reda p� vilken typ av prislista som v�ljs, 
	//eftersom det itne r�cker med att kika p� namn d� tv� prislistor med olika typer kan ha samma namn 20111229 J� Bug #2716
	int m_nTypeOfPriceList;
	double m_fDiamClass;	// Added 070507 p�d
	CString m_sCostTmplBindTo;
		
	BOOL m_bBindToIsAtCoast;
	BOOL m_bBindToIsSouthEast;
	BOOL m_bBindToIsRegion5;
	BOOL m_bBindToIsPartOfPlot;
	CString m_sBindToSIH100_Pine;

	vecUCFunctions m_vecExchangeFunc;
	void setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupSoderbergsSettingsInPropertyGrid(CXTPPropertyGridItem *pItem);


	// Pricelist set in prn_pricelist_table
	vecTransactionPricelist m_vecTransactionPricelist;
	void getPricelistsFromDB(void);

	// Cost templates in cost_template_table
	CTransaction_costtempl m_recActiveCostTmpl;
	vecTransaction_costtempl m_vecTransaction_costtempl;
	void getCostTemplateFromDB(void);

	// Get active trakt data from database; 071217 p�d
	//CTransaction_trakt m_recTrakt;
	//void getTraktFromDB(void);

	// Pricelist set in esti_trakt_pricelist_table
	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(void);

	// Added 2007-06-14, get information about
	// Plot(s) set for this Trakt; 070614 p�d
	vecTransactionPlot m_vecTraktPlot;
	void getPlotsFromDB(int trakt_id);

	BOOL saveMiscDataToDB_prl(CXTPPropertyGridItem *pItem,BOOL removeTraktData);
	BOOL saveMiscDataToDB_dcls(CXTPPropertyGridItem *pItem);
	BOOL saveMiscDataToDB_costtmpl(CXTPPropertyGridItem *pItem);

	void saveSoderbergsSpecificsToDB(CXTPPropertyGridItem *pItem);	// Added 071207 p�d

	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	void ViewStandInGis(BOOL updateCoords = FALSE);

	CGeneralInfoDlg m_wndGeneralInfoDlg;
	CXTPPropertyGrid m_wndPropertyGrid;
	
	HICON m_hIcon;
public:
	void updateSpecVec(vecTransactionTraktData vecTraktData); //20121206 J� #3509
	vecTransactionTraktData m_vecTraktData;

	double getDiamClass(void){return m_fDiamClass;};
	CMDIStandEntryFormFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CDialogBar m_wndFieldChooserDlg;		// Sample Field chooser window
	CDialogBar m_wndFieldChooserDlg10;  // "St�mplingsl�ngd - Uttag"
	CDialogBar m_wndFieldChooserDlg11;  // "St�mplingsl�ngd - Kvarst�ende"
	CDialogBar m_wndFieldChooserDlg12;  // "St�mplingsl�ngd - Uttag i STickv�g"
	CDialogBar m_wndFieldChooserDlg2;		// Sample Field chooser window

	void setGeneralInfo(CTransaction_trakt &v)
	{
		if (m_wndGeneralInfoDlg.GetSafeHwnd())
		{
			m_wndGeneralInfoDlg.setInfo(v);
		}
	}

	void setSumInfo(long numof_trees,double m3sk,double m3fub,double m3ub,double gy)
	{
		if (m_wndGeneralInfoDlg.GetSafeHwnd())
		{
			m_wndGeneralInfoDlg.setSumInfo(numof_trees,m3sk,m3fub,m3ub,gy);
		}
	}

	void setSumInfo2(long numof_trees,double m3sk,double m3fub,double m3ub,double gy)
	{
		if (m_wndGeneralInfoDlg.GetSafeHwnd())
		{
			m_wndGeneralInfoDlg.setSumInfo2(numof_trees,m3sk,m3fub,m3ub,gy);
		}
	}

	void updateGeneralInfoDialog(void)
	{
		m_wndGeneralInfoDlg.updateHTMLText();
	}

	void setToolbarItems(BOOL refresh,BOOL add,BOOL preview)
	{
		m_bIsInfoTBtn = refresh;
		m_bIsAddTBtn = add;
		m_bIsPrintPreviewTBtn = preview;
	}

	void setToolbarItemsOnNoData(BOOL enable)
	{
		m_bIsCalculationTBtn = enable;
		m_bIsINVFileTBtn = enable;
		m_bIsToolsTBtn = enable;
		m_bIsPrintPreviewTBtn = enable;
		m_bGISInstalled = enable;

		//m_wndToolBar.EnableWindow( enable );
	}

	void setUpdateDockingPane(void)
	{
		if (GetDockingPaneManager()->IsPaneClosed(IDR_PROPSPANE1))
		{
			GetDockingPaneManager()->ShowPane(IDR_PROPSPANE1);
			GetDockingPaneManager()->HidePane(IDR_PROPSPANE1);
		}
	}

	void setShowDockingPane(BOOL show)
	{
		if (!show)
		{
			GetDockingPaneManager()->ClosePane(IDR_PROPSPANE1);
		}
		else
		{
			if (GetDockingPaneManager()->IsPaneClosed(IDR_PROPSPANE1))
			{
				GetDockingPaneManager()->ShowPane(IDR_PROPSPANE1);
				GetDockingPaneManager()->HidePane(IDR_PROPSPANE1);
			}
		}
	}

	void setStatusBarText(int index,LPCTSTR text)
	{
		if (m_wndStatusBar.GetSafeHwnd() != NULL)
		{
			m_wndStatusBar.SetPaneText(index,text);
		}
	}

	void populateSettings(void);

	void clearPropertyGrid(void);

	void setReadOnlyInSettingsProperty(BOOL ro);

	void setExchange(LPCTSTR value)
	{
		m_sExchPricelistStr = value;
		m_wndPropertyGrid.Refresh();
	}

	void setPricelist(LPCTSTR value)
	{
		m_sPricelistSelected = value;
		m_wndPropertyGrid.Refresh();
	}

	void setDiameterclass(double value)
	{
		m_fDiamClass = value;
		m_wndPropertyGrid.Refresh();
	}

	// Get "S�derbergs" data; 071217 p�d

	BOOL getIsAtCoast(void)
	{
		return m_bBindToIsAtCoast;
	}

	BOOL getIsSouthEast(void)
	{
		return m_bBindToIsSouthEast;
	}

	BOOL getIsRegion5(void)
	{
		return m_bBindToIsRegion5;
	}

	BOOL getIsPartOfPlot(void)
	{
		return m_bBindToIsPartOfPlot;
	}

	CString getSIH100_Pine(void)
	{
		return m_sBindToSIH100_Pine;
	}

	BOOL getShowOnlyOneStand(void)
	{
		return global_SHOW_ONLY_ONE_STAND; //m_bShowOnlyOneStand;
	}

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDIStandEntryFormFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnUpdateAddTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrintPreviewTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCalculationTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGISTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateINVFileTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolsTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRandTreesTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRandTreesToolsTBtn(CCmdUI* pCmdUI);

	afx_msg void OnToolsMatchRandTrees();
	afx_msg void OnToolsHandleRandTrees();
	afx_msg void OnToolsHandleRandTrees2();
	afx_msg void OnToolsUpdatePricelist(void);
	afx_msg void OnToolsUpdateCosttemplate(void);
	afx_msg void OnToolsCosttemplateInfo(void);	// Added 901021 p�d
	
	//20121206 J� #3509
	afx_msg void OnSpecSet(UINT nId);
	afx_msg void OnSpecAssort(UINT nId);

	afx_msg void OnTBBtnAddSpecieToTrakt();

	afx_msg void OnTBBtnPrintOut();

	afx_msg void OnTBBtnDoCalculation();

	afx_msg void OnTBBtnInformation();
	
	afx_msg void OnTBBtnINVFile();

	afx_msg void OnTBBtnPreview();

	afx_msg void OnTBBtnGisViewStand();
	afx_msg void OnTBBtnGisUpdateStand();

	afx_msg void OnFilePrint();

	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CPropertySelectListFrame frame

class CPropertySelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CPropertySelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:

	void setLanguage(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CPropertySelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CPropertySelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CPropertySelectListFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CPricelistSelectListFrame frame

class CPricelistSelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CPricelistSelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:

	void setLanguage(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CPricelistSelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CPricelistSelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// class CMDISpecieDataFrame
class CMDISpecieDataFrame : public CMDIChildWnd //CChildFrameBase
{
	DECLARE_DYNCREATE(CMDISpecieDataFrame)
	
//private:
	CXTPDockingPaneManager m_paneManager;

	CXTPPropertyGrid m_wndPropertyGrid;

	CString m_sLangFN;

	CString m_sPropspane2Cap;

	CString m_sFunctions;
	CString m_sHeight;
	CString m_sCalcHgtAs;
	CString m_sVolume;
	CString m_sCalcVolAs;
	CString m_sBark;
	CString m_sBarkSerie;
	CString m_sVolumeUB;
	CString m_sCalcVolAsUB;
	CString m_sMiscSettings;
	CString m_sSk_Ub;
	CString m_sSk_Fub;
	CString m_sFub_To;
	CString m_sTransport;
	CString m_sTranspDist1;	// "Avst�nd till v�g (m)"
	CString m_sTranspDist2;	// "Avst�nd till Indistri (km)"
	CString m_sQualDesc;
	CString m_sCalcQDescAs;

	CString m_sAtCoast;
	CString m_sSouthEast;
	CString m_sRegion5;
	CString m_sPartOfPlot;

	void setLanguage(void);

	BOOL m_bFirstOpen;
	BOOL m_bIsDirty;
	// Bind to ...
	CString m_sHgtTypeOfBindStr;	// Which height function type's selcted (e.g. H25,Regression etc)
	CString m_sHgtBindStr;				// Selected function within the TypeOf (e.g. H25 => �vriga tr�dslag)

	CString m_sVolTypeOfBindStr;	// Which volume function type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolBindStr;				// Selected function within the TypeOf (e.g. Brandels => Mindre,Norra)

	CString m_sBarkTypeOfBindStr;	// Which bark function type's selcted (e.g. Jonsson/Ostlin,Skogforsk etc)
	CString m_sBarkBindStr;				// Selected function within the TypeOf (e.g. Jonsson/Ostlin => Tall,Serie 1)

	CString m_sVolUBTypeOfBindStr;	// Which volume function type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolUBBindStr;				// Selected function within the TypeOf (e.g. Brandels => Mindre,Norra)

	double m_fM3SkToM3Ub;
	double m_fM3SkToM3Fub;
	double m_fM3FubToM3To;
	double m_fDiamClass;
	long m_nTranspDist1;
	long m_nTranspDist2;

	BOOL m_bIsAtCoast;
	BOOL m_bIsSouthEast;
	BOOL m_bIsRegion5;
	BOOL m_bIsPartOfPlot;
	CString m_sSIH100_Pine;

	CString m_sQDescBindStr;
	CStringArray m_listQDesc;

	CString m_sCapMsg;
	CStringArray m_sarrValidateMsg;

	vecUCFunctions m_vecHgtFunc;
	vecUCFunctionList m_vecHgtFuncList;
	void setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecVolFunc;
	vecUCFunctionList m_vecVolFuncList;
	void setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecBarkFunc;
	vecUCFunctionList m_vecBarkFuncList;
	void setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecVolUBFunc;
	vecUCFunctionList m_vecVolUBFuncList;
	void setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem);
	
	// Data in esti_trakt_set_spc_table, settings for specie in Trakt; 070504 p�d
	vecTransactionTraktSetSpc m_vecTransactionTraktSetSpc;
	void getTraktSetSpc(void);

	BOOL getActiveTypeOfFunction(int,CString &,UINT*,int*);
	CString getActiveFunction(int);
	CString getSetSpcDate(void);
	// Qualitydexriptions
	int getQualDescIndex(CXTPPropertyGridItem *pItem,CString& qdesc_name);
	void getQualDescData(int *qdesc_index,CStringArray& qdesc_list);
	void setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	// "Omf�ringstal etc"
	BOOL getMiscData(int do_as);
	void setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);
	void setupMiscSettings2ValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new = TRUE);

	// DB handling
	void saveSetSpcHgtFuncToDB(CXTPPropertyGridItem *pItem);
	void saveSetSpcVolFuncToDB(CXTPPropertyGridItem *pItem);
	void saveSetSpcBarkFuncToDB(CXTPPropertyGridItem *pItem);
	void saveSetSpcVolUBFuncToDB(CXTPPropertyGridItem *pItem);
	void saveSetSpcVolUBFuncToDB_sk_ub(CXTPPropertyGridItem *pItem);
	void saveSetSpcMiscDataToDB(CXTPPropertyGridItem *pItem);

	CTransaction_trakt_misc_data m_recTraktMiscData;	// Added 070507 p�d
	void getTraktMiscDataFromDB(void);

	void doCalculation();

	void setupFunctionData(BOOL setup_as_new = TRUE);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CMDISpecieDataFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	BOOL getIsDirty(void)
	{
		return m_bIsDirty;
	}

	void doSetupFunctionData(void)
	{
		CXTPDockingPane *pPane = m_paneManager.FindPane(IDR_PROPSPANE2);
		if (pPane != NULL)
		{
			if (pPane->IsValid())
			{
				m_wndPropertyGrid.ResetContent();
				setupFunctionData();
				m_wndPropertyGrid.Refresh();
			}
		}
	}

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDISpecieDataFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDISpecieDataFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
//	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CTraktSelectListFrame frame

class CTraktSelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CTraktSelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipPrintOut;
	CString m_sToolTipRefresh;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CTraktSelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CTraktSelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFieldChooserDlg;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CPlotSelListFrame frame

class CPlotSelListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CPlotSelListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipAdd;
	CString m_sToolTipDel;

	BOOL m_bPossibleToAddGroup;
	BOOL m_bPossibleToDelGroup;

	CDialogBar m_wndInvMethodInfoDialog;
	int	m_nInvMethodIndex;
	CStringArray m_sarrInventoryMethods;
	
	CXHTMLStatic m_wndLblInvMethod;
	
	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	void setOnUpdateAddPlotTBBtnEnable(BOOL enable)
	{
		m_bPossibleToAddGroup = enable;
	}

	void setOnUpdateDelPlotTBBtnEnable(BOOL enable)
	{
		m_bPossibleToDelGroup = enable;
	}

	CPlotSelListFrame();           // protected constructor used by dynamic creation
	virtual ~CPlotSelListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;


// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnAddPlotTBtn();
	afx_msg void OnDelPlotTBtn();
	afx_msg void OnUpdateAddPlotTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDelPlotTBtn(CCmdUI* pCmdUI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CMDIInformationFrame frame

class CMDIInformationFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIInformationFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	
	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:


	CMDIInformationFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIInformationFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;


// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


#endif