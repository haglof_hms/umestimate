#pragma once

#include "Resource.h"

// CDlgPage dialog

class CDlgPage : public CDialog
{
	DECLARE_DYNAMIC(CDlgPage)

public:
	CDlgPage(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgPage();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
