// PageOneFormView.cpp : implementation file
//

/*

	2008-03-25
	* �ndrat till vecTraktIndex och att l�sa fr�n databas traktinfo. per traktid.

*/

#include "stdafx.h"
#include "UMEstimate.h"
#include "MDITabbedView.h"
#include "PageOneFormView.h"
#include "PageTwoFormView.h"
#include "PageFourFormView.h"
#include "StandEntryDoc.h"
#include "OnNewTraktDialog.h"
#include "SearchPropertyDlg.h"
#include "ArealCalcDlg.h"

#include "ResLangFileReader.h"

// CPageOneFormView

IMPLEMENT_DYNCREATE(CPageOneFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPageOneFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_CHECK1, &CPageOneFormView::OnBnClickedCheck1)
	ON_CBN_SELCHANGE(IDC_CBOX_DATEPICKER, &CPageOneFormView::OnCbnSelchangeCboxDatepicker)
	ON_BN_CLICKED(IDC_BTN_PROPWND, &CPageOneFormView::OnBnClickedBtnPropwnd)
	ON_BN_CLICKED(IDC_BTN_PROPWND2, &CPageOneFormView::OnBnClickedBtnPropwnd2)
	ON_EN_CHANGE(IDC_EDIT8, OnChangeHandledAreal)	

	ON_EN_CHANGE(IDC_EDIT_TRAKT_LENGTH, OnChangeTraktLengthOrWidth)	
	ON_EN_CHANGE(IDC_EDIT_TRAKT_WIDTH, OnChangeTraktLengthOrWidth)	

	ON_EN_CHANGE(IDC_EDIT7, OnChangeConsiderdAreal)	
	ON_EN_CHANGE(IDC_EDIT11, OnEnChangeEdit11)
	ON_EN_CHANGE(IDC_EDIT12, OnEnChangeEdit12)
	//ON_BN_CLICKED(IDC_BTN_CALC1_1, &CPageOneFormView::OnBnClickedBtnCalc11)
	ON_BN_CLICKED(IDC_BTN_AREA, &CPageOneFormView::OnBnClickedBtnArea)
	ON_BN_CLICKED(IDC_BTN_TRANSPORT_SPARA, &CPageOneFormView::OnBnClickedBtnTransportSpara)
END_MESSAGE_MAP()

CPageOneFormView::CPageOneFormView()
	: CXTResizeFormView(CPageOneFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDirty = FALSE;
//	m_bShowOnlyOneStand = FALSE;
	m_bSetFocusOnInitDone = TRUE;
	m_pDB = NULL;
	m_nTraktId = 0;
	savedTraktId = -1;
	m_vecTraktIndex.clear();
}

CPageOneFormView::~CPageOneFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecTraktIndex.clear();
	m_vecTrakt.clear();
	m_vecTraktTypeData.clear();
	m_vecOriginData.clear();
	m_vecInputMethodData.clear();
	m_vecHgtClassData.clear();
	m_vecCutClassData.clear();
	m_vecProperty.clear();
	m_vecTemplates.clear();
	m_vecTraktAss.clear();

	// Reset value in Register, after checkin' where the request
	// to open UMEstimate came from; 090217 p�d
	// 1 = From NaigationBar
	// 2 = From UMLandValue Object
	setFromNavBarOrOtherInReg(1);
}

void CPageOneFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL11, m_wndLbl11);
	DDX_Control(pDX, IDC_LBL12, m_wndLbl12);
	DDX_Control(pDX, IDC_LBL13, m_wndLbl13);
	DDX_Control(pDX, IDC_LBL14, m_wndLbl14);
	DDX_Control(pDX, IDC_LBL15, m_wndLbl15);
	DDX_Control(pDX, IDC_LBL16, m_wndLbl16);
	DDX_Control(pDX, IDC_LBL17, m_wndLbl17);
	DDX_Control(pDX, IDC_LBL19, m_wndLbl19);
	DDX_Control(pDX, IDC_LBL20, m_wndLbl20);
	DDX_Control(pDX, IDC_LBL21, m_wndLbl21);
	DDX_Control(pDX, IDC_LBL22, m_wndLbl22);
	DDX_Control(pDX, IDC_LBL23, m_wndLbl23);
	DDX_Control(pDX, IDC_LBL24, m_wndLbl24);
	DDX_Control(pDX, IDC_LBL25, m_wndLbl25);
	DDX_Control(pDX, IDC_LBL27, m_wndLbl27);
	DDX_Control(pDX, IDC_LBL28, m_wndLbl28);
	DDX_Control(pDX, IDC_LBL29, m_wndLbl29);
	DDX_Control(pDX, IDC_LBL37, m_wndLbl30);

	DDX_Control(pDX, IDC_LBL_TRAKT_LENGTH, m_wndLblLength);
    DDX_Control(pDX, IDC_LBL_TRAKT_WIDTH, m_wndLblWidth);
	
	DDX_Control(pDX, IDC_LABEL_TRANSPORT, m_wndLabelTransport);
	DDX_Control(pDX, IDC_LABEL_TRANSPORT_TILL_INDUSTRI, m_wndLabelTillIndustri);
	DDX_Control(pDX, IDC_LABEL_TRANSPORT_TILL_VAG, m_wndLabelTillVag);

	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);
	DDX_Control(pDX, IDC_GROUP2, m_wndGroup2);
	DDX_Control(pDX, IDC_GROUP3, m_wndGroup3);
	DDX_Control(pDX, IDC_GROUP4, m_wndGroup4);
	DDX_Control(pDX, IDC_GROUP5, m_wndGroup5);
	DDX_Control(pDX, IDC_GROUP6, m_wndGroup6);
	DDX_Control(pDX, IDC_GROUP7, m_wndGroup7);
	DDX_Control(pDX, IDC_CHECK_GROUP, m_wndGroup8);
	DDX_Control(pDX, IDC_GROUP8, m_wndGroup9);
	DDX_Control(pDX, IDC_GROUP9, m_wndGroup10);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT18, m_wndEdit18);
	DDX_Control(pDX, IDC_EDIT19, m_wndEdit19);
	DDX_Control(pDX, IDC_EDIT20, m_wndEdit20);
	DDX_Control(pDX, IDC_EDIT21, m_wndEdit21);
	DDX_Control(pDX, IDC_EDIT22, m_wndEdit22);
	DDX_Control(pDX, IDC_EDIT23, m_wndEdit23);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit24);
	DDX_Control(pDX, IDC_EDIT_TRANSPORT_TILL_VAG, m_wndEditTransportTillVag);
	DDX_Control(pDX, IDC_EDIT_TRANSPORT_TILL_INDUSTRI, m_wndEditTransportTillIndustri);

	DDX_Control(pDX, IDC_EDIT_TRAKT_LENGTH, m_wndEditLength);
	DDX_Control(pDX, IDC_EDIT_TRAKT_WIDTH, m_wndEditWidth);

	DDX_Control(pDX, IDC_CHECK1, m_wndCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_wndCheck2);


	DDX_Control(pDX, IDC_CHECK_TILLF_UTNYTTJ, m_wndCheckTillfUtnyttj);	

	DDX_Control(pDX, IDC_CBOX_DATEPICKER, m_wndDateTimeCtrl);
	DDX_Control(pDX, IDC_CBOX_TRAKTTYPE, m_wndCBoxTraktTypeCtrl);
	DDX_Control(pDX, IDC_CBOX_ORIGIN, m_wndCBoxOriginCtrl);
	DDX_Control(pDX, IDC_CBOX_INPUTDATA, m_wndCBoxInputDataCtrl);
	DDX_Control(pDX, IDC_CBOX_HCLASS, m_wndCBoxHeightClassCtrl);
	DDX_Control(pDX, IDC_CBOX_CUTCLASS, m_wndCBoxCutClassCtrl);

	DDX_Control(pDX, IDC_BTN_PROPWND, m_wndBtnOpenPropWnd);
	DDX_Control(pDX, IDC_BTN_PROPWND2, m_wndBtnRemovePropWnd);
	DDX_Control(pDX,IDC_BTN_TRANSPORT_SPARA,m_wndBtnTransportSpara);
	//DDX_Control(pDX, IDC_BTN_CALC1_1, m_wndBtnCalc1);

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_BTN_AREA, m_wndBtnArea);
}

BOOL CPageOneFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CPageOneFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setLanguage();

		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit2.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit5.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit6.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit6.SetReadOnly();
		m_wndEdit7.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit8.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit9.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit10.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit11.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit12.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit13.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit13.SetReadOnly();
		m_wndEdit14.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit16.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit18.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit19.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit20.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit21.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit22.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit22.SetLimitText(30);	// Added 101005 p�d
		m_wndEdit23.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit24.SetDisabledColor(BLACK,COL3DFACE );

		m_wndEditLength.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEditWidth.SetDisabledColor(BLACK,COL3DFACE );

		m_wndBtnOpenPropWnd.SetBitmap(CSize(18,14),IDB_OPENWND);
		m_wndBtnOpenPropWnd.SetXButtonStyle( BS_XT_WINXP_COMPAT );
		m_wndBtnRemovePropWnd.SetBitmap(CSize(18,14),IDB_DELPROP);
		m_wndBtnRemovePropWnd.SetXButtonStyle( BS_XT_WINXP_COMPAT );
		//m_wndBtnCalc1.SetBitmap(CSize(18,14),IDB_BITMAP4);
		//m_wndBtnCalc1.SetXButtonStyle( BS_XT_WINXP_COMPAT );
		m_wndBtnArea.SetBitmap(CSize(18,14),IDB_BITMAP5);
		m_wndBtnArea.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndDateTimeCtrl.setDateInComboBox();

		m_wndCBoxTraktTypeCtrl.SetEnabledColor(BLACK,WHITE );
		m_wndCBoxTraktTypeCtrl.SetDisabledColor(BLACK,COL3DFACE );
		m_wndCBoxTraktTypeCtrl.SetReadOnlyEx();

		m_wndCBoxOriginCtrl.SetEnabledColor(BLACK,WHITE );
		m_wndCBoxOriginCtrl.SetDisabledColor(BLACK,COL3DFACE );
		m_wndCBoxOriginCtrl.SetReadOnlyEx();

		m_wndCBoxInputDataCtrl.SetEnabledColor(BLACK,WHITE );
		m_wndCBoxInputDataCtrl.SetDisabledColor(BLACK,COL3DFACE );
		m_wndCBoxInputDataCtrl.SetReadOnlyEx();

		m_wndCBoxHeightClassCtrl.SetEnabledColor(BLACK,WHITE );
		m_wndCBoxHeightClassCtrl.SetDisabledColor(BLACK,COL3DFACE );
		m_wndCBoxHeightClassCtrl.SetReadOnlyEx();

		m_wndCBoxCutClassCtrl.SetEnabledColor(BLACK,WHITE );
		m_wndCBoxCutClassCtrl.SetDisabledColor(BLACK,COL3DFACE );
		m_wndCBoxCutClassCtrl.SetReadOnlyEx();

		// Set numerc editboxes; 070219 p�d
		m_wndEdit6.SetAsNumeric();
		m_wndEdit7.SetAsNumeric();
		m_wndEdit8.SetAsNumeric();
		m_wndEdit9.SetAsNumeric();
		// Set Mask for COXMaskedEdit control; 071101 p�d
		m_wndEdit15.SetMask(_T(">##"));
		m_wndEdit15.SetPromptSymbol('_');
		m_wndEdit16.SetAsNumeric();
		m_wndEdit18.SetAsNumeric();
		m_wndEdit19.SetAsNumeric();
		m_wndEdit20.SetAsNumeric();
		m_wndEdit21.SetAsNumeric();
		m_wndEdit11.SetAsNumeric();
		m_wndEdit11.SetRange(0,99);
		m_wndEdit12.SetAsNumeric();
		m_wndEdit12.SetRange(0,99);
		m_wndEdit13.SetAsNumeric();
		m_wndEdit13.SetRange(0,99);
		m_wndEdit14.SetAsNumeric();
		m_wndEdit24.SetAsNumeric();
		m_wndEdit24.SetRange(1,2);

		m_wndEditLength.SetAsNumeric();
		m_wndEditWidth.SetAsNumeric();

		m_enumAction = UPD_ITEM;

		getTableData();

		setTableData();

		// Show only one stand, open from UMLandValue
		global_SHOW_ONLY_ONE_STAND = (getFromNavBarOrOtherInReg() > 1);
		setEnabledData(TRUE);

		if (!global_SHOW_ONLY_ONE_STAND)
		{
			getTraktIndex();
			m_nDBIndex = (long)m_vecTraktIndex.size() - 1;
		}
		// Reset value in Register, after checkin' where the request
		// to open UMEstimate came from; 090217 p�d
		// 1 = From NaigationBar
		// 2 = From UMLandValue Object
		//setFromNavBarOrOtherInReg(1);

		getProperties();

// Commented out 2008-01-02 p�d
// We only need to set m_nDBIndex, not populate
// this is done in OnSetFocus(CWnd*)
//		populateData(m_nDBIndex);

		m_bInitialized = TRUE;
		m_bSetFocusOnInitDone = FALSE;	// Run SetFocus

	}

}

void CPageOneFormView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);

}

void CPageOneFormView::OnSetFocus(CWnd*)
{
	// PopulateData is done here, because the TabControl tab
	// for PropOwners is updated with the name of the Property
	// in action; 070118 p�d
	if (! m_bSetFocusOnInitDone)
	{
		// Populate data
		populateData(m_nDBIndex);

		// Set Show/Hide Setiings pane, depending
		// if there's data ont ore more Traks; 070507 p�d
		setShowSettingsPane();
		
		// Show/Hide Tab for page Two (Resultat) 
		// depending on if there's one or more
		// Trakts; 070308 p�d
		setPageTwoTabOnTrakt();

		// Show/Hide Tab for page (Provtr�d och Diameterklasser)
		// depending on if there's one or more
		// Trakts; 070402 p�d
		setPageThreeTabOnTrakt();
	
		// Show/Hide Tab for page (Diagram)
		// depending on if there's one or more
		// Trakts; 100416 p�d
		setPageFourTabOnTrakt();

		m_bSetFocusOnInitDone = TRUE;
	}

	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItems(FALSE,FALSE,FALSE);
		CDocument *pDoc = pFrame->GetActiveDocument();
		if (pDoc != NULL)	
		{
			// Show only one stand, open from UMLandValue
			global_SHOW_ONLY_ONE_STAND = (getFromNavBarOrOtherInReg() > 1);
			if (global_SHOW_ONLY_ONE_STAND)
			{
				CString sTitle1;
				if (!global_SHOW_ONLY_ONE_STAND_FROM_GIS)
					sTitle1.Format(_T("%s (%s)"),m_sMainWindowCaption,m_sInfringe);
				else
					sTitle1.Format(_T("%s"),m_sMainWindowCaption);
				pDoc->SetTitle(sTitle1);
			}
		}	// if (pDoc != NULL)	
	}

	if (!m_vecTraktIndex.empty())
	{
		if (global_SHOW_ONLY_ONE_STAND)
		{
			setNavigationButtons(FALSE,FALSE);
			// Send messages to HMSShell, enable buttons on toolbar; 090227 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
		else
		{
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
			// Send messages to HMSShell, enable buttons on toolbar; 090227 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
		}
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
		// Send messages to HMSShell, enable buttons on toolbar; 090227 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CPageOneFormView message handlers

void CPageOneFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}

BOOL CPageOneFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CPageOneFormView diagnostics

#ifdef _DEBUG
void CPageOneFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPageOneFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// PRIVATE

void CPageOneFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndGroup9.SetWindowText((xml->str(IDS_STRING1301)));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING1000)));
			m_wndLbl16.SetWindowText((xml->str(IDS_STRING1001)));
			m_wndLbl29.SetWindowText((xml->str(IDS_STRING101)));
			m_wndLbl30.SetWindowText((xml->str(IDS_STRING380)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING122)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING102)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING103)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING104)));
			m_wndLbl6.SetWindowText((xml->str(IDS_STRING1050)));
			m_wndLbl7.SetWindowText((xml->str(IDS_STRING1051)));
			m_wndLbl8.SetWindowText((xml->str(IDS_STRING1052)));

			m_wndLbl9.SetWindowText((xml->str(IDS_STRING107)));
			m_wndLbl15.SetWindowText((xml->str(IDS_STRING108)));
			m_wndLbl17.SetWindowText((xml->str(IDS_STRING126)));
			m_wndLbl19.SetWindowText((xml->str(IDS_STRING106)));

			m_wndLbl20.SetWindowText((xml->str(IDS_STRING1240)));
			m_wndLbl21.SetWindowText((xml->str(IDS_STRING1241)));
			m_wndLbl22.SetWindowText((xml->str(IDS_STRING1242)));

			m_wndLbl10.SetWindowText((xml->str(IDS_STRING119)));
			m_wndLbl11.SetWindowText((xml->str(IDS_STRING110)));
			m_wndLbl12.SetWindowText((xml->str(IDS_STRING111)));
			m_wndLbl13.SetWindowText((xml->str(IDS_STRING112)));
			m_wndLbl14.SetWindowText((xml->str(IDS_STRING113)));
			
			m_wndLbl23.SetWindowText((xml->str(IDS_STRING123)));
			m_wndLbl24.SetWindowText((xml->str(IDS_STRING129)));
			m_wndLbl25.SetWindowText((xml->str(IDS_STRING128)));
			m_wndLbl27.SetWindowText((xml->str(IDS_STRING125)));
			m_wndLbl28.SetWindowText((xml->str(IDS_STRING132)));
 
			m_wndLblLength.SetWindowText((xml->str(IDS_STRING285)));
			m_wndLblWidth.SetWindowText((xml->str(IDS_STRING286)));

			m_wndCheck1.SetWindowText((xml->str(IDS_STRING130)));
			m_wndCheck2.SetWindowText((xml->str(IDS_STRING1300)));

			m_wndCheckTillfUtnyttj.SetWindowText((xml->str(IDS_STRING1302)));

			m_sRemoveCapMsg = (xml->str(IDS_STRING215));
			m_sRemoveMsg1 = (xml->str(IDS_STRING211));
			m_sRemoveMsg2 = (xml->str(IDS_STRING214));
			m_sArealLimitsMsg1 = (xml->str(IDS_STRING3230));
			m_sArealLimitsMsg2 = (xml->str(IDS_STRING3231));
			m_sArealLimitsMsg3 = (xml->str(IDS_STRING3232));
			m_sExplRemoveMsg.Format(_T("%s\n%s"),
								(xml->str(IDS_STRING212)),
								(xml->str(IDS_STRING213)));

			m_sCaptionMsg = (xml->str(IDS_STRING151));
			m_sSaveMsg = (xml->str(IDS_STRING216));
			m_sSavedMsg = (xml->str(IDS_STRING217));
			m_sShouldSaveMsg = (xml->str(IDS_STRING218));
			m_sRemovePropSTandConenctionMsg = (xml->str(IDS_STRING360));
			m_sTraktNumOf = (xml->str(IDS_STRING321));

			m_sSaveMsg1.Format(_T("%s\n%s"),
					(xml->str(IDS_STRING3060)),
					(xml->str(IDS_STRING3061)));

			m_sTraktNum	= (xml->str(IDS_STRING1000));
			m_sTraktPNum =	(xml->str(IDS_STRING1001));
			m_sTraktName = (xml->str(IDS_STRING101));

			m_sNoTraktMsg.Format(_T("%s\n%s"),
				(xml->str(IDS_STRING247)),
				(xml->str(IDS_STRING248)));

			m_sCouldNotFindStandMsg = xml->str(IDS_STRING400);
			m_sOK	= (xml->str(IDS_STRING155));
			m_sCancel = (xml->str(IDS_STRING156));

			m_wndDateTimeCtrl.setButtonText((xml->str(IDS_STRING100)));

			m_sInfringe = xml->str(IDS_STRING500);

			m_sMainWindowCaption = xml->str(IDS_STRING5000);

			//#5026 PH 20160617
			m_wndLabelTransport.SetWindowText((xml->str(IDS_STRING26051)));
			m_wndLabelTillVag.SetWindowText((xml->str(IDS_STRING26052)));
			m_wndLabelTillIndustri.SetWindowText((xml->str(IDS_STRING26053)));
			m_wndBtnTransportSpara.SetWindowText((xml->str(IDS_STRING26056)));
		}
		delete xml;
	}

}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CPageOneFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	if (global_SHOW_ONLY_ONE_STAND)
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	else
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPageOneFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	int nIndex = -1; 
	CXTPTabManagerItem *pManager;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
	if (pTabView)
	{
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			nIndex = pManager->GetIndex();
			pManager = NULL;
		}	// if (pManager != NULL)
	}	// if (pTabView)
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			//if (nIndex == 0)
			if (addTrakt())
			{
				// Handle OnSuiteMessage only if THIS tab is selected; 070307 p�d
				if (pTabView)
				{
					pManager = pTabView->getTabCtrl().getTabPage(0);
					if (pManager != NULL)
					{
						pManager->Select();
						pManager = NULL;
					}	// if (pManager != NULL)
				}	// if (pTabView)
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (nIndex == 0)
				saveTrakt(1);
	
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			if (nIndex == 0)
			{
				removeTrakt();
			}
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Check if user have changed
			// active data; 061113 p�d
			isHasDataChangedPageOne();
			m_nDBIndex = 0;
			setNavigationButtons(FALSE,TRUE);
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Check if user have changed
			// active data; 061113 p�d
			isHasDataChangedPageOne();
			m_nDBIndex--;
			if (m_nDBIndex < 0)
				m_nDBIndex = 0;
			if (m_nDBIndex == 0)
			{
				setNavigationButtons(FALSE,TRUE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Check if user have changed
			// active data; 061113 p�d
			isHasDataChangedPageOne();
			m_nDBIndex++;
			if (m_nDBIndex > ((long)m_vecTraktIndex.size() - 1))
				m_nDBIndex = (long)m_vecTraktIndex.size() - 1;
					
			if (m_nDBIndex == (long)m_vecTraktIndex.size() - 1)
			{
				setNavigationButtons(TRUE,FALSE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Check if user have changed
			// active data; 061113 p�d
			isHasDataChangedPageOne();
			m_nDBIndex = (long)m_vecTraktIndex.size()-1;
			setNavigationButtons(TRUE,FALSE);	
			populateData(m_nDBIndex);
			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)
	return 0L;
}

// PROTECTED

// Get data from database on tables:
// 1) Trakttyper
// 2)	Ursprung
// 3) Indata-metod
// 4) H�jd-klasser
// 5) Huggningsklasser (f�rh�llanden)
// 2007-02-28 p�d
/*
	vecTraktTypeData m_vecTraktTypeData;
	vecOriginData m_vecOriginData;
	vecInputMethodData m_vecInputMethodData;
	vecHgtClassData m_vecHgtClassData;
	vecCutClassData m_vecCutClassData;
*/
void CPageOneFormView::getTableData(void)
{
	if (m_pDB != NULL)
	{
		if ( m_pDB->getTraktTypes(m_vecTraktTypeData))
		{
			m_pDB->getOrigins(m_vecOriginData);
			m_pDB->getInputMethods(m_vecInputMethodData);	
			m_pDB->getHgtClass(m_vecHgtClassData);
			m_pDB->getCutClass(m_vecCutClassData);
		}
	}	// if (pDB != NULL)
}

// Add data to comboboxes fron database; 070228 p�d
/*
m_wndCBoxTraktTypeCtrl
m_wndCBoxOriginCtrl
m_wndCBoxInputDataCtrl
m_wndCBoxHeightClassCtrl
m_wndCBoxCutClassCtrl
*/
void CPageOneFormView::setTableData(void)
{
	UINT i;
	// Add Trakttypes
	if (m_vecTraktTypeData.size() > 0)
	{
		m_wndCBoxTraktTypeCtrl.ResetContent();
		for (i = 0;i <  m_vecTraktTypeData.size();i++)
		{
			m_wndCBoxTraktTypeCtrl.AddString(m_vecTraktTypeData[i].getTraktType());
		}	// for (i = 0;i <  m_vecTraktTypeData.size();i++)
	}	// if (m_vecTraktTypeData.size() > 0)

	// Add Origin (Ursprung)
	if (m_vecOriginData.size() > 0)
	{
		m_wndCBoxOriginCtrl.ResetContent();
		for (i = 0;i <  m_vecOriginData.size();i++)
		{
			m_wndCBoxOriginCtrl.AddString(m_vecOriginData[i].getOrigin());
		}	// for (i = 0;i <  m_vecOriginData.size();i++)
	}	// if (m_vecOriginData.size() > 0)

	// Add Input methods
	if (m_vecInputMethodData.size() > 0)
	{
		m_wndCBoxInputDataCtrl.ResetContent();
		for (i = 0;i <  m_vecInputMethodData.size();i++)
		{
			m_wndCBoxInputDataCtrl.AddString(m_vecInputMethodData[i].getInputMethod());
		}	// for (i = 0;i <  m_vecInputMethodData.size();i++)
	}	// if (m_vecInputMethodData.size() > 0)

	// Add Height classes
	if (m_vecHgtClassData.size() > 0)
	{
		m_wndCBoxHeightClassCtrl.ResetContent();
		for (i = 0;i <  m_vecHgtClassData.size();i++)
		{
			m_wndCBoxHeightClassCtrl.AddString(m_vecHgtClassData[i].getHgtClass());
		}	// for (i = 0;i <  m_vecHgtClassData.size();i++)
	}	// if (m_vecHgtClassData.size() > 0)

	// Add Cut classes
	if (m_vecCutClassData.size() > 0)
	{
		m_wndCBoxCutClassCtrl.ResetContent();
		for (i = 0;i <  m_vecCutClassData.size();i++)
		{
			m_wndCBoxCutClassCtrl.AddString(m_vecCutClassData[i].getCutClass());
		}	// for (i = 0;i <  m_vecCutClassData.size();i++)
	}	// if (m_vecCutClassData.size() > 0)

}

void CPageOneFormView::getTrakts(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_pDB->getTrakts(m_vecTrakt))
			{
/*
				// Check if there's any Trakt data.
				// If not tell user; 070320 p�d
				if (m_vecTrakt.size() == 0)
				{
					setupOnNoTraktData(FALSE);
					setEnabledData();
					clearAll();
				}
*/
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

void CPageOneFormView::getTraktIndex()
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_pDB->getTraktIndex(m_vecTraktIndex,FALSE))
			{

				// Check if there's any Trakt data.
				// If not tell user; 090217 p�d
				if (m_vecTraktIndex.size() == 0)
				{
					//UMMessageBox(this->GetSafeHwnd(),m_sNoTraktMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);
					clearAll();
					setEnabledData(FALSE);
				}

			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

void CPageOneFormView::getTraktIndex_ObjectSelect(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_pDB->getTraktIndex(m_vecTraktIndex,TRUE))
			{
				// Check if there's any Trakt data.
				// If not tell user; 090217 p�d
				if (m_vecTraktIndex.size() == 0)
				{
					clearAll();
					setEnabledData(FALSE);
				}
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

int CPageOneFormView::getTraktId()
{
	return m_nTraktId;
}

void CPageOneFormView::getTrakt(int trakt_id,CTransaction_trakt &rec)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTrakt(trakt_id,rec);	
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}


void CPageOneFormView::getProperties(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getProperties(m_vecProperty);
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

// This method tries to find a pricelist, in prl_priselist_table, based
// on information in selected template; 070906 p�d
BOOL CPageOneFormView::getPricelistBasedOnTemplateInformation(int id,int type_of,CTransaction_pricelist& data)
{
	vecTransactionPricelist vecPricelist;

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getPricelists(vecPricelist))
			{
				if (vecPricelist.size() > 0)
				{
					for (UINT i = 0;i < vecPricelist.size();i++)
					{
						data = vecPricelist[i];
						if (data.getID() == id && data.getTypeOf() == type_of)
						{
							return TRUE;
						}
					}
				}
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
	return FALSE;
}

BOOL CPageOneFormView::getCostsTmplBasedOnTemplateInformation(int id,LPCTSTR costs_tmpl_name,CTransaction_costtempl& data)
{
	CString S;
	vecTransaction_costtempl vecCostsTmpl;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCostTmpls(vecCostsTmpl))
			{
				if (vecCostsTmpl.size() > 0)
				{
					for (UINT i = 0;i < vecCostsTmpl.size();i++)
					{
						data = vecCostsTmpl[i];
						// OBS! Check for "Kostnadsmallar f�r b�de Rotpost och Intr�ng"; 080306 p�d
						if (data.getID() == id && 
//							 (data.getTypeOf() == COST_TYPE_1 || data.getTypeOf() == COST_TYPE_2) && 
								data.getTemplateName().Compare(costs_tmpl_name) == 0)
						{
							return TRUE;
						}
					}
				}
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
	return FALSE;
}

void CPageOneFormView::getTemplates(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTemplates(m_vecTemplates,ID_TEMPLATE_TRAKT);
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

CString CPageOneFormView::getNextTraktNumber(void)
{
	CString S;
	TCHAR szTmp[127];
	int nIdentity;
	int nNumOf;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			nIdentity = m_pDB->getLastTraktIdentity();
			nNumOf = m_pDB->getNumOfRecordsInTrakt();
/*
			S.Format(_T("CPageOneFormView::getNextTraktNumber\nnIdentity %d\nnNumOf %d"),
				nIdentity,nNumOf);
			UMMessageBox(S);			
*/
			if (nIdentity == 1 && nNumOf == 0)
				_stprintf(szTmp,_T("T%05d"),nIdentity);
			else
				_stprintf(szTmp,_T("T%05d"),nIdentity+1);
/*
			if (nIdentity == 0 && nNumOf == 0)
			{
				_stprintf(szTmp,_T("T%05d"),nIdentity);
			}
			else if (nIdentity < 1 || nIdentity > 1)
			{
				_stprintf(szTmp,_T("T%05d"),nIdentity+1);
			}
*/
		}	// if (pDB != NULL)
	}	// if (m_bConnected)

	return szTmp;
}

void CPageOneFormView::getTraktAssFromDB(void)
{
	m_vecTraktAss.clear();

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getTraktAss(m_vecTraktAss);
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}

BOOL CPageOneFormView::addTrakt(void)
{
	BOOL ret = FALSE;

	// Save any changes
	saveTrakt(2);

	// Ask user, in a dialog, if he likes to use
	// a template on creating a new Trakt; 070308 p�d
	COnNewTraktDialog *dlg = new COnNewTraktDialog(NULL,m_pDB);
	if (dlg != NULL)
	{
		getTemplates();
		dlg->setTemplates(m_vecTemplates);
		if (dlg->DoModal() == IDOK)
		{
			setEnabledData(TRUE);
			clearAll();
	
			m_enumAction = NEW_ITEM;
			// Find out selection made by user:
			//	1 = Create Trakt from template
			//	2 = Create Trakt no template
			if (dlg->getCreateTraktSelection() == ID_TRAKT_USE_TEMPLATE)
			{
				m_sTemplateXMLFile = dlg->getXMLFile();			
				if (createTraktFromTemplate())
				{
					CMDIStandEntryFormFrame *pForm = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);

					if (pForm != NULL)
					{
						pForm->setShowDockingPane(TRUE);
					}
				}
			}
			//else if (dlg->getCreateTraktSelection() == ID_TRAKT_NO_TEMPLATE)
			//	createTraktNoTemplate();
	
			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			if (pTabView != NULL)
			{
				// Also update owners for selected property; 070126 p�d
				CPageFourFormView* pView4 = DYNAMIC_DOWNCAST(CPageFourFormView, 
																										CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(3)->GetHandle()));
				{
					if (pView4 != NULL)
					{
						// Refresh data; 100329 p�d
						pView4->getHgtCurveFormView()->setPropertyName(L"");
						pView4->getHgtCurveFormView()->doDrawChartView(true,true);
					}	// if (pView4 != NULL)
				}
			}

			ret = TRUE;
		}
		delete dlg;
	}
	return ret;
}

BOOL CPageOneFormView::removeTrakt(void)
{
	BOOL bResetIdentity = TRUE;
	CTransaction_trakt data;
	CString sMsg;
	int nIndex = -1;
	if (m_bConnected)	
	{
		// Check that there's any data in m_vecMachineData vector; 061010 p�d
		if (m_vecTraktIndex.size() > 0)
		{
			if (m_pDB != NULL)
			{
				// Get Regions in database; 061002 p�d	
//				data = m_vecTrakt[m_nDBIndex];
				getTrakt(m_vecTraktIndex[m_nDBIndex],data);
				// Setup a message for user upon deleting machine; 061010 p�d
				sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n%s : %s\n\n%s\n\n%s"),
										m_sRemoveMsg1,
										m_sTraktNum,
										data.getTraktNum(),
										m_sTraktPNum,
										data.getTraktPNum(),
										m_sTraktName,
										data.getTraktName(),
										m_sExplRemoveMsg,
										m_sRemoveMsg2);

				if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sRemoveCapMsg,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
				{
					// Delete Trakt and ALL underlying data, including Pricelist
					// added for trakt; 070507 p�d
					m_pDB->removeTrakt(data);
					
					// Reload information
					// Reload information on Stands not connectecd to any Object.
					// If there's no Stands, not connected, try to load Stands
					// connected to an object; 090313 p�d
					getTraktIndex();

					if (m_vecTraktIndex.size() == 0)
					{
						getTraktIndex_ObjectSelect();
						// Check; if there's stands connected to Object(s)
						// we can't reset the Identity-field; 090313 p�d
						if (m_vecTraktIndex.size() > 0)
						{
							bResetIdentity = FALSE;
							m_vecTraktIndex.clear();
						}
					}

					// Check if there's any trakt data. If not
					// hide Tab for Page two (Resultat); 070308 p�d
					setPageTwoTabOnTrakt();
					// Show/Hide Tab for page (Provtr�d och Diameterklasser)
					// depending on if there's one or more
					// Trakts; 070904 p�d
					setPageThreeTabOnTrakt();
					// Show/Hide Tab for page (Diagram)
					// depending on if there's one or more
					// Trakts; 100416 p�d
					setPageFourTabOnTrakt();

					// Check if there's any trakt data. If not
					// hide Settings pane; 070507 p�d
					setShowSettingsPane();

					// After a delete, set to last item; 060103 p�d
					m_nDBIndex = (long)m_vecTraktIndex.size() - 1;
					populateData(m_nDBIndex);

					if (m_vecTraktIndex.size() == 0)
					{
						setNavigationButtons(FALSE,FALSE);
						resetIsDirty();
						// Only reset Identity-filed if the table is empty.
						// I.e. no stands (alone or connected to an Object); 090313 p�d
						if (bResetIdentity)	
							m_pDB->resetTraktIdentityField();
						// Populate data on Page two and three; 070906 p�d
						CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
						if (pTabView != NULL)
						{
							CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
							if (pView2 != NULL)
							{
								pView2->populateData();
							}	// if (pView != NULL)
							CPageThreeFormView *pView3 = pTabView->getPageThreeFormView();
							if (pView3 != NULL)
							{
								pView3->populateData();
							}	// if (pView != NULL)
						}	// if (pTabView != NULL)

					}
					else
					{
						setNavigationButtons(m_nDBIndex > 0,
  															 m_nDBIndex < (m_vecTraktIndex.size()-1));
					}


				}	// if (messageDlg(m_sCaption,m_sOKBtn,m_sCancelBtn,sMsg))
			}	// if (m_pDB != NULL)

		}	// if (m_vecMachineData.size() > 0)
		return TRUE;
	}
	return FALSE;
}
/*
// Collect Trakt assort transfer data; 070611 p�d
void CPageOneFormView::saveTraktAssTrans(int trakt_id,int trakt_data_id,int spc_id,LPCTSTR spc_name,vecTransactionTemplateTransfers &vec)
{
	CTransaction_trakt_trans data;
	if (vec.size() > 0)
	{

		for (int i = 0;i < vec.size();i++)
		{
			data = CTransaction_trakt_trans(trakt_data_id,
																		  trakt_id,
																		  vec[i].getFromAssortID(),
																		  vec[i].getToAssortID(),
																		  spc_id,
																		  STMP_LEN_WITHDRAW,
																		  vec[i].getFromAssort(),
																		  vec[i].getToAssort(),
																		  spc_name,
																		  vec[i].getM3Fub(),
																		  vec[i].getPercent(),
																		  vec[i].getM3Trans(),
																		  _T(""));

			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					if (!m_pDB->addTraktTrans(data))
						m_pDB->updTraktTrans(data);
				}	// if (m_pDB != NULL)
			}	// if (m_bConnected)
		}
	}

}
*/
// Added 070903 p�d
BOOL CPageOneFormView::createTraktFromTemplate(void)
{

	int nTraktID;
	BOOL bReturn = export_createTraktFromTemplate(m_sTemplateXMLFile,m_dbConnectionData,&nTraktID);

	// If traktid > -1 then we'll add 0 data to esti_trakt_rotpost_table; 100309 p�d
	if (m_pDB) m_pDB->addRotpost(CTransaction_trakt_rotpost(nTraktID,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,ROTPOST_ORIGIN1,L""));
	// Reload data, and set to last item entered; 081114 p�d
	getTraktIndex();
	m_nDBIndex = (long)m_vecTraktIndex.size() - 1;
	populateData(m_nDBIndex);
	// Show/Hide Tab for page Two (Resultat) 
	// depending on if there's one or more
	// Trakts; 070308 p�d
	setPageTwoTabOnTrakt();
	// Show/Hide Tab for page Three (Sampletrees and Diamterclasses) 
	// depending on if there's one or more
	// Trakts; 070904 p�d
	setPageThreeTabOnTrakt();
	// Show/Hide Tab for page (Diagram)
	// depending on if there's one or more
	// Trakts; 100416 p�d
	setPageFourTabOnTrakt();

	return bReturn;
}

BOOL CPageOneFormView::createTraktNoTemplate()
{
	setEnabledData(TRUE);
	clearAll();

	// On create a new trakt, we need to add a "Traktnummer".
	// This number's created from identity-field in esti_trakt_table
	// and incremented by one; 070903 p�d
	m_wndEdit1.SetWindowText(getNextTraktNumber());

	m_enumAction = NEW_ITEM;
	m_bIsDirty = TRUE;

	saveTrakt(1);


	return TRUE;
}

// Check if there's any trakt data. If not
// hide Tab for Page two (Volymer); 070308 p�d
void CPageOneFormView::setPageTwoTabOnTrakt(void)
{
	BOOL bShowTabForPageTwo = (m_vecTraktIndex.size() > 0);
	enableTabPage(1,bShowTabForPageTwo);
}

// Check if there's any trakt data. If not
// hide Tab for Page three; 070329 p�d
void CPageOneFormView::setPageThreeTabOnTrakt(void)
{
	BOOL bShowTabForPageThree = (m_vecTraktIndex.size() > 0);
	enableTabPage(2,bShowTabForPageThree);
}

// Check if there's any trakt data. If not
// hide Tab for Page three; 070329 p�d
void CPageOneFormView::setPageFourTabOnTrakt(void)
{
	BOOL bShowTabForPageFour = (m_vecTraktIndex.size() > 0);
	enableTabPage(3,bShowTabForPageFour);
}

// Check if there's any trakt data. If not
// hide Tab for Page two (Volymer); 070308 p�d
void CPageOneFormView::setShowSettingsPane(void)
{
	BOOL bShowSettingsPane = (m_vecTraktIndex.size() > 0);
	CMDIStandEntryFormFrame *pForm = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pForm != NULL)
	{
		pForm->setShowDockingPane(bShowSettingsPane);
	}
}

// If there's no data in the m_vecTrakt vector
// maybe on first startup or user have deleted
// all data. Tell user that there's no data and
// that he needs to Click on the New button on
// the main toolbar to create a Trakt.
// Also disable hide page CPageTwoFormView; 070320 p�d
void CPageOneFormView::setupOnNoTraktData(BOOL add_trakt)
{
	//UMMessageBox(this->GetSafeHwnd(),m_sNoTraktMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);
}


void CPageOneFormView::populateData(long idx)
{
	CString S;
	if (idx == -1) idx = m_nDBIndex;
	// We need to check for changes in Tab formview, before we move on; 070516 p�d		
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (m_vecTraktIndex.size() > 0 && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
		S.Format(m_sTraktNumOf,idx + 1,m_vecTraktIndex.size());
		setStatusBarText(2,S);		

		if (pTabView)
		{
			CXTPTabManagerItem *pManager = pTabView->getTabCtrl().getTabPage(1);
			if (pManager)
			{
				// Also update owners for selected property; 070126 p�d
				CPageTwoFormView* pView1 = DYNAMIC_DOWNCAST(CPageTwoFormView, 
																										CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(1)->GetHandle()));
				{
					if (pView1 != NULL)
					{
						// Check if data's been changed on Volume- and Assortrment data.
						// If data has changed, ask user to Save changes; 070315 p�d
						pView1->isHasDataChangedPageTwo();
					}	// if (pView1 != NULL)
				}	// CPageTwoFormView* pView1 = DYNAMIC_DOWNCAST(CPageTwoFormView, 
			}	// if (pManager)
		}	// if (pTabView)
		
		//m_recTraktActive = m_vecTrakt[idx];
		getTrakt(m_vecTraktIndex[idx],m_recTraktActive);

		savedTraktId=m_vecTraktIndex[idx];

		m_wndEdit1.SetWindowText(m_recTraktActive.getTraktNum());
		m_wndEdit3.SetWindowText(m_recTraktActive.getTraktPNum());

		m_wndEdit22.SetWindowText(m_recTraktActive.getTraktName());

		m_wndCBoxTraktTypeCtrl.SetCurSel(m_wndCBoxTraktTypeCtrl.FindString(0,m_recTraktActive.getTraktType()));
		m_wndDateTimeCtrl.SetWindowText(m_recTraktActive.getTraktDate());
		m_wndEdit4.SetWindowText(m_recTraktActive.getTraktCreatedBy());

		// #4717: kolla att inte sida �r 0, orsakar stack-krash
		if(m_recTraktActive.getWSide() != 0)
			m_wndEdit24.setInt(m_recTraktActive.getWSide());
		else
			m_wndEdit24.setInt(1);

		m_wndEditLength.setInt(m_recTraktActive.getTraktLength());
		m_wndEditWidth.setInt(m_recTraktActive.getTraktWidth());

/*
		CString S;
		S.Format("CPageOneFormView::populateData(%d)\nm_recTraktActive.getTraktArealHandled() %f",
			idx,m_recTraktActive.getTraktArealHandled());
		UMMessageBox(S);
*/
		m_wndEdit6.setFloat(m_recTraktActive.getTraktAreal(),3);
		m_wndEdit7.setFloat(m_recTraktActive.getTraktArealConsiderd(),3);
		m_wndEdit8.setFloat(m_recTraktActive.getTraktArealHandled(),3);
		m_wndEdit9.setInt(m_recTraktActive.getTraktAge());
		m_wndEdit10.SetWindowText(m_recTraktActive.getTraktMapdata());
		m_wndEdit11.setInt(m_recTraktActive.getTraktLatitude());
		m_wndEdit12.setInt(m_recTraktActive.getTraktLongitude());
		m_wndEdit13.SetWindowText(m_recTraktActive.getTraktXCoord());
		m_wndEdit14.SetWindowText(m_recTraktActive.getTraktYCoord());
		m_wndEdit15.SetWindowText(m_recTraktActive.getTraktSIH100());
		m_wndEdit16.setFloat(m_recTraktActive.getTraktWithdraw(),1);
		m_wndEdit18.setInt(m_recTraktActive.getTraktHgtOverSea());
		m_wndEdit19.setInt(m_recTraktActive.getTraktGround());
		m_wndEdit20.setInt(m_recTraktActive.getTraktSurface());
		m_wndEdit21.setInt(m_recTraktActive.getTraktRake());
		m_wndEdit23.SetWindowText(m_recTraktActive.getTraktNotes());
		m_wndCBoxOriginCtrl.SetCurSel(m_wndCBoxOriginCtrl.FindString(0,m_recTraktActive.getTraktOrigin()));
		m_wndCBoxInputDataCtrl.SetCurSel(m_wndCBoxInputDataCtrl.FindString(0,m_recTraktActive.getTraktInpMethod()));
		m_wndCBoxHeightClassCtrl.SetCurSel(m_wndCBoxHeightClassCtrl.FindString(0,m_recTraktActive.getTraktHgtClass()));
		m_wndCBoxCutClassCtrl.SetCurSel(m_wndCBoxCutClassCtrl.FindString(0,m_recTraktActive.getTraktCutClass()));

		m_wndCheck1.SetCheck((m_recTraktActive.getTraktSimData() ? 1 : 0));
		m_wndCheck2.SetCheck((m_recTraktActive.getOnlyForStand() == 1) ? 1 : 0);

		m_wndCheckTillfUtnyttj.SetCheck((m_recTraktActive.getTillfUtnyttj() == 1) ? 1 : 0);

		m_wndBtnArea.EnableWindow(m_pDB->isTraktConnectedToShape(m_recTraktActive.getTraktID()));

		doPropertyPopulate(m_recTraktActive.getTraktPropID());
		// If user goes to an already entered Trakt, the action = UPD_ITEM; 070618 p�d
		m_enumAction = UPD_ITEM;

		if (pTabView)
		{
			CXTPTabManagerItem *pManager = pTabView->getTabCtrl().getTabPage(1);
			if (pManager)
			{
				pManager->SetData(DWORD_PTR(&m_recTraktActive));

				// Also update owners for selected property; 070126 p�d
				CPageTwoFormView* pView2 = DYNAMIC_DOWNCAST(CPageTwoFormView, 
																										CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(1)->GetHandle()));
				{
					if (pView2 != NULL)
					{
						// Display Volume- and Assortment data for Trakt; 070309 p�d
						pView2->populateData();
					}	// if (pView1 != NULL)
				}	// CPageTwoFormView* pView1 = DYNAMIC_DOWNCAST(CPageTwoFormView, 
			}

				// Also update owners for selected property; 070126 p�d
				CPageThreeFormView* pView3 = DYNAMIC_DOWNCAST(CPageThreeFormView, 
																										CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
				{
					if (pView3 != NULL)
					{
						// Refresh data; 070515 p�d
						pView3->populateData();
					}	// if (pView3 != NULL)
					// Check to see if the Heightcure is added to DB;
					if (!global_SHOW_ONLY_ONE_STAND)
					{
						runDoHeightCurveToDBInPageThree();
					}

				}
				// Also update owners for selected property; 070126 p�d
				CPageFourFormView* pView4 = DYNAMIC_DOWNCAST(CPageFourFormView, 
																										CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(3)->GetHandle()));
				{
					if (pView4 != NULL)
					{
						CString sPropName;
						m_wndEdit5.GetWindowText(sPropName);
						// Refresh data; 100329 p�d
						pView4->getHgtCurveFormView()->setPropertyName(sPropName);
						pView4->getHgtCurveFormView()->doDrawChartView(true,true);
						pView4->getDiamDistributionFormView()->setPropertyName(sPropName);
						pView4->getDiamDistributionFormView()->doDrawChartView(true,true);
						pView4->getVolumeDistributionFormView()->setPropertyName(sPropName);
						pView4->getVolumeDistributionFormView()->doDrawChartView(true,true);
						pView4->getAssortDistributionFormView()->setPropertyName(sPropName);
						pView4->getAssortDistributionFormView()->doDrawChartView(true,true);
#ifdef SHOW_THINNING
						pView4->getThinningDiagramFormView()->setPropertyName(sPropName);
						pView4->getThinningDiagramFormView()->doDrawChartView(true,true);
#endif
					}	// if (pView4 != NULL)
				}	// CPageFourFormView* pView4 = DYNAMIC_DOWNCAST(CPageFourFormView,

		}	// if (pTabView)
	}
	else
	{
		m_recTraktActive = CTransaction_trakt();
	}

	// Set info in GeneralInfoDlg; 070305 p�d
	// And populate settings; 070507 p�d
	CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pWnd != NULL)
	{
		pWnd->setGeneralInfo(m_recTraktActive);
//		pWnd->populateSettings();
	}	// if (pWnd != NULL)

}

void CPageOneFormView::resetIsDirty(void)
{
	m_wndEdit1.resetIsDirty();
	m_wndEdit2.resetIsDirty();
	m_wndEdit3.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndEdit5.resetIsDirty();
	m_wndEdit6.resetIsDirty();
	m_wndEdit7.resetIsDirty();
	m_wndEdit8.resetIsDirty();
	m_wndEdit9.resetIsDirty();
	m_wndEdit10.resetIsDirty();
	m_wndEdit11.resetIsDirty();
	m_wndEdit12.resetIsDirty();
	m_wndEdit13.resetIsDirty();
	m_wndEdit14.resetIsDirty();
//	m_wndEdit15.resetIsDirty();
	m_wndEdit16.resetIsDirty();
	m_wndEdit18.resetIsDirty();
	m_wndEdit19.resetIsDirty();
	m_wndEdit20.resetIsDirty();
	m_wndEdit21.resetIsDirty();
	m_wndEdit22.resetIsDirty();
	m_wndEdit23.resetIsDirty();

	m_wndEditLength.resetIsDirty();
	m_wndEditWidth.resetIsDirty();

	m_wndCBoxTraktTypeCtrl.resetIsDirty();
	m_wndCBoxOriginCtrl.resetIsDirty();
	m_wndCBoxInputDataCtrl.resetIsDirty();
	m_wndCBoxHeightClassCtrl.resetIsDirty();
	m_wndCBoxCutClassCtrl.resetIsDirty();

	m_bIsDirty = FALSE;
}

BOOL CPageOneFormView::getIsDirty(void)
{
	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit1.isDirty() ||
			m_wndEdit2.isDirty() ||
			m_wndEdit3.isDirty() ||
			m_wndEdit4.isDirty() ||
			m_wndEdit5.isDirty() ||
			m_wndEdit6.isDirty() ||
			m_wndEdit7.isDirty() ||
			m_wndEdit8.isDirty() ||
			m_wndEdit9.isDirty() ||
			m_wndEdit10.isDirty() ||
			m_wndEdit11.isDirty() ||
			m_wndEdit12.isDirty() ||
			m_wndEdit13.isDirty() ||
			m_wndEdit14.isDirty() ||
//			m_wndEdit15.isDirty() ||
			m_wndEdit16.isDirty() ||
			m_wndEdit18.isDirty() ||
			m_wndEdit19.isDirty() ||
			m_wndEdit20.isDirty() ||
			m_wndEdit21.isDirty() ||
			m_wndEdit22.isDirty() ||
			m_wndEdit23.isDirty() ||
			m_wndEditLength.isDirty() ||
			m_wndEditWidth.isDirty() ||
			m_wndCBoxTraktTypeCtrl.isDirty() ||
			m_wndCBoxOriginCtrl.isDirty() ||
			m_wndCBoxInputDataCtrl.isDirty() ||
			m_wndCBoxHeightClassCtrl.isDirty() ||
			m_wndCBoxCutClassCtrl.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

// Added 2007-06-28 p�d
// Check if wee need to recalculate data for trakt; 070628 p�d
BOOL CPageOneFormView::getNeedToRecalculate(void)
{
	if (m_wndEdit6.isDirty() ||
			m_wndEdit7.isDirty() ||
			m_wndEdit8.isDirty() ||
			m_wndEdit9.isDirty() ||
			m_wndEdit11.isDirty() ||
			m_wndEdit18.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CPageOneFormView::clearAll()
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.setFloat(0,1);
	m_wndEdit7.setFloat(0,1);
	m_wndEdit8.setFloat(0,1);
	m_wndEdit9.setInt(0);
	m_wndEdit10.SetWindowText(_T(""));
	m_wndEdit11.SetWindowText(_T(""));
	m_wndEdit12.SetWindowText(_T(""));
	m_wndEdit13.SetWindowText(_T(""));
	m_wndEdit14.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.setFloat(0,1);
	m_wndEdit18.setInt(0);
	m_wndEdit19.SetWindowText(_T(""));
	m_wndEdit20.SetWindowText(_T(""));
	m_wndEdit21.SetWindowText(_T(""));
	m_wndEdit22.SetWindowText(_T(""));
	m_wndEdit23.SetWindowText(_T(""));
	m_wndEditLength.SetWindowText(_T(""));
	m_wndEditWidth.SetWindowText(_T(""));
	m_wndCBoxTraktTypeCtrl.SetWindowText(_T(""));
	m_wndCBoxOriginCtrl.SetWindowText(_T(""));
	m_wndCBoxInputDataCtrl.SetWindowText(_T(""));
	m_wndCBoxHeightClassCtrl.SetWindowText(_T(""));
	m_wndCBoxCutClassCtrl.SetWindowText(_T(""));
	m_wndCheck1.SetCheck(0);
	m_wndCheck2.SetCheck(0);	// Default; use in both Stand and Infring; 100526 p�d
	m_wndCheckTillfUtnyttj.SetCheck(0);

	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->clearPropertyGrid();
	}
}

void CPageOneFormView::setEnabledData(BOOL enable)
{
	m_bIsEnabledData = enable;

	m_wndEdit1.EnableWindow( enable );
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow( FALSE );
	m_wndEdit2.SetReadOnly();
	m_wndEdit3.EnableWindow( enable );
	m_wndEdit3.SetReadOnly(!enable);
	m_wndEdit4.EnableWindow( enable );
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit5.EnableWindow( FALSE );
	m_wndEdit5.SetReadOnly();
//	m_wndEdit6.EnableWindow( enable );
//	m_wndEdit6.SetReadOnly(!enable);
	m_wndEdit7.EnableWindow( enable );
	m_wndEdit7.SetReadOnly(!enable);
	m_wndEdit8.EnableWindow( enable );
	m_wndEdit8.SetReadOnly(!enable);
	m_wndEdit9.EnableWindow( enable );
	m_wndEdit9.SetReadOnly(!enable);
	m_wndEdit10.EnableWindow( enable );
	m_wndEdit10.SetReadOnly(!enable);
	m_wndEdit11.EnableWindow( enable );
	m_wndEdit11.SetReadOnly(!enable);
	m_wndEdit12.EnableWindow( enable );
	m_wndEdit12.SetReadOnly(!enable);
	//m_wndEdit13.EnableWindow( enable );
	//m_wndEdit13.SetReadOnly(!enable);
	m_wndEdit14.EnableWindow( enable );
	m_wndEdit14.SetReadOnly(!enable);
	m_wndEdit15.EnableWindow( enable );
	m_wndEdit15.SetReadOnly(!enable);
	m_wndEdit16.EnableWindow( enable );
	m_wndEdit16.SetReadOnly(!enable);
	m_wndEdit18.EnableWindow( enable );
	m_wndEdit18.SetReadOnly(!enable);
	m_wndEdit19.EnableWindow( enable );
	m_wndEdit19.SetReadOnly(!enable);
	m_wndEdit20.EnableWindow( enable );
	m_wndEdit20.SetReadOnly(!enable);
	m_wndEdit21.EnableWindow( enable );
	m_wndEdit21.SetReadOnly(!enable);
	m_wndEdit22.EnableWindow( enable );
	m_wndEdit22.SetReadOnly(!enable);
	m_wndEdit23.EnableWindow( enable );
	m_wndEdit23.SetReadOnly(!enable);
	m_wndEdit24.EnableWindow( enable );
	m_wndEdit24.SetReadOnly(!enable);

	m_wndEditLength.EnableWindow( enable );
	m_wndEditLength.SetReadOnly(!enable);
	m_wndEditWidth.EnableWindow( enable );
	m_wndEditWidth.SetReadOnly(!enable);

	m_wndCBoxTraktTypeCtrl.EnableWindow( enable );
	m_wndCBoxOriginCtrl.EnableWindow( enable );
	m_wndCBoxInputDataCtrl.EnableWindow( enable );
	m_wndCBoxHeightClassCtrl.EnableWindow( enable );
	m_wndCBoxCutClassCtrl.EnableWindow( enable );
	m_wndCheck1.EnableWindow( enable );
	m_wndCheck2.EnableWindow( enable );
	m_wndCheckTillfUtnyttj.EnableWindow(enable);

	m_wndDateTimeCtrl.EnableWindow( enable );

	if (global_SHOW_ONLY_ONE_STAND)
	{
		m_wndBtnOpenPropWnd.EnableWindow( FALSE );
		m_wndBtnRemovePropWnd.EnableWindow( FALSE );
//		m_wndBtnCalc1.EnableWindow( FALSE );
	}
	else
	{
		m_wndBtnOpenPropWnd.EnableWindow( (m_vecTraktIndex.size() > 0) || enable);
		m_wndBtnRemovePropWnd.EnableWindow( (m_vecTraktIndex.size() > 0) || enable);
		//m_wndBtnCalc1.EnableWindow(  (m_vecTraktIndex.size() > 0) || enable );
	}

	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pFrame != NULL)
	{
		pFrame->setToolbarItemsOnNoData(enable);
	}

}

BOOL CPageOneFormView::isCruiseInObject(int ecru_id)
{
	if (m_pDB != NULL)
		return m_pDB->isStandInObjectCruise(ecru_id);

	return FALSE;
}

BOOL CPageOneFormView::getEnteredData(int action)
{
	CString sDate;
	BOOL bIsSimData;
	int nTraktID;
	short nOnlyForStand;

	int nHgtOverSea;
	int nLatitude;
	TCHAR szGrowthArea[127];
	TCHAR szSI_H100[127];
	double fCorrFac;
	int nCostsTmplID;
	TCHAR szCostsTmplName[127];

	bool bIsAtCoast = false;
	bool bIsSouthEast = false;
	bool bIsRegion5 = false;
	bool bIsPartOfPlot = false;
	TCHAR szSI_H100_Pine[127];
	BOOL bTillfUtnyttj=FALSE;

	szGrowthArea[0] = '\0';
	szSI_H100[0] = '\0';
	szCostsTmplName[0] = '\0';
	szSI_H100_Pine[0] = '\0';

	// Check if there's any data entered. E.g. a Traktnumber 
	// If not return FALSE; 070411 p�d
	if (m_wndEdit1.getText() == _T("") && action == 1)
	{
		UMMessageBox(this->GetSafeHwnd(),m_sSaveMsg1,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);	
		return FALSE;
	}
	else if (m_wndEdit1.getText() == _T("") && action == 2)
	{
		return FALSE;
	}

	m_wndDateTimeCtrl.GetWindowText(sDate);

	bIsSimData = (m_wndCheck1.GetCheck() == BST_CHECKED);

	nOnlyForStand = ((m_wndCheck2.GetCheck() == BST_CHECKED) ? 1 : 0);

	if(m_wndCheckTillfUtnyttj.GetCheck()==BST_CHECKED)
		bTillfUtnyttj=TRUE;

	if (m_enumAction == NEW_ITEM)
		nTraktID = -1;
	else
	{
		nTraktID = m_recTraktActive.getTraktID();
		// Get Info on "S�derbergs" settings; 071217 p�d
		// When this view gets focus, disable the view Add toolbar button; 070308 p�d
		CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
		if (pFrame != NULL)
		{
			bIsAtCoast = (pFrame->getIsAtCoast() ? true : false);
			bIsSouthEast = (pFrame->getIsSouthEast() ? true : false);
			bIsRegion5 = (pFrame->getIsRegion5() ? true : false);
			bIsPartOfPlot = (pFrame->getIsPartOfPlot() ? true : false);
			_tcscpy(szSI_H100_Pine,pFrame->getSIH100_Pine());
		}

	}

	// Check if m_wndEdit5 = Property is empty. 
	// If so, set PropID = -1; 070305 p�d
	if (m_wndEdit5.getText().IsEmpty())
		m_wndEdit5.setItemIntData(-1);

	m_recTraktNew = CTransaction_trakt(nTraktID,
																		 m_wndEdit1.getText(),
																		 m_wndEdit3.getText(),
																		 m_wndEdit22.getText(),
																		 m_wndCBoxTraktTypeCtrl.getText(),
																		 sDate,
																		 m_wndEdit5.getItemIntData(),
																		 m_wndEdit6.getFloat(),
																		 m_wndEdit7.getFloat(),
																		 m_wndEdit8.getFloat(),
																		 m_wndEdit9.getInt(),
																		 m_wndEdit15.GetInputData(),
																		 m_wndEdit16.getFloat(),
																		 m_wndEdit18.getInt(),
																		 m_wndEdit19.getInt(),
																		 m_wndEdit20.getInt(),
																		 m_wndEdit21.getInt(),
																		 m_wndEdit10.getText(),
																		 m_wndEdit11.getInt(),
																		 m_wndEdit12.getInt(),
																		 m_wndEdit13.getText(),
																		 m_wndEdit14.getText(),
																		 m_wndCBoxOriginCtrl.getText(),
																		 m_wndCBoxInputDataCtrl.getText(),
																		 m_wndCBoxHeightClassCtrl.getText(),
																		 m_wndCBoxCutClassCtrl.getText(),
																		 bIsSimData,
																		 bIsAtCoast,
																		 bIsSouthEast,
																		 bIsRegion5,
																		 bIsPartOfPlot,
																		 (szSI_H100_Pine),	// SI H100 Tall
																		 m_wndEdit2.getText(),
																		 m_wndEdit5.getText(),
																		 m_wndEdit23.getText(),
																		 m_wndEdit4.getText(),
																		 m_recTraktActive.getCreated(),
																		 m_wndEdit24.getInt(),
																		 nOnlyForStand,
																		 m_wndEditLength.getInt(),
																		 m_wndEditWidth.getInt(),
																		 bTillfUtnyttj,
																		 _T(""),	// HMS-120 trakt point
																		 _T(""));	// HMS-120 trakt  coordinates
	return TRUE;
}

// PUBLIC
BOOL CPageOneFormView::isHasDataChangedPageOne(int action,BOOL show_pane_settings)
{
	if (saveTrakt(action,show_pane_settings))
	{
		resetIsDirty();
		return TRUE;
	}

	return FALSE;
}

BOOL CPageOneFormView::saveTrakt(int action,BOOL show_pane_settings)
{
	BOOL bReturn = FALSE;
	CString S;
	int nNumOf;
	if (m_bConnected)	
	{

		if (m_pDB != NULL)
		{
			// Check number of entries in the contacts table.
			// If there's no entries, reset the identity field to start
			// from 1; 070102 p�d
			nNumOf = m_pDB->getNumOfRecordsInTrakt();
			if (nNumOf < 1)
			{
				m_enumAction = NEW_ITEM;
			}	// if (nNumOf < 1)
		}
		// Save; only if there's anything to be saved; 070411 p�d
		// getEnteredData() also check if User entered a "TraktNummer"; 070903 p�d
		if (getEnteredData(action))
		{
			if (m_pDB != NULL)
			{

				if (!m_pDB->addTrakt(m_recTraktNew))
				{
					m_pDB->updTrakt(m_recTraktNew);
				}	// if (!m_pDB->addTrakt(m_recTraktNew))
				// Reload information
				if (!global_SHOW_ONLY_ONE_STAND)
					getTraktIndex();
				else
					getTraktIndex_ObjectSelect();

//				reloadTrakt(getActiveTrakt()->getTraktID());	//@b: beh�ver vi ladda om trakten?
				m_recTraktActive = m_recTraktNew;	// let current record be the same as the one we saved 9-10 rows above

				if (m_enumAction == NEW_ITEM)
				{
					// Point to last post in DB; 070903 p�d
					m_nDBIndex = (long)m_vecTraktIndex.size() - 1;
				}	// if (m_enumAction == NEW_ITEM)


				// Check for and save data set in Page three (Trees to be cut etc.); 100223 p�d
				CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
				if (pTabView != NULL)
				{
					// Check for and save data set in Page two
					CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
					if (pView2 != NULL)
					{
						bReturn |= pView2->isHasDataChangedPageTwo();
					}

					CPageThreeFormView *pView3 = pTabView->getPageThreeFormView();
					if (pView3 != NULL)
					{
						bReturn |= pView3->isHasDataChangedPageThree();
					}	// if (pView != NULL)
				}	// if (pTabView != NULL)
				
				//@b: beh�ver vi ladda om data?
//				if (bReturn)
//					populateData(m_nDBIndex);

				if (m_vecTraktIndex.empty())
				{
					setNavigationButtons(FALSE,FALSE);
					clearAll();
				}
				else if (m_vecTraktIndex.size() == 1 || global_SHOW_ONLY_ONE_STAND)
				{
					setNavigationButtons(FALSE,FALSE);
				}
				else
				{
					setNavigationButtons(m_nDBIndex > 0,
  														m_nDBIndex < (m_vecTraktIndex.size()-1));
				}
				// Tell user that data have been saved
			}	// if (pDB != NULL)


			// Show/Hide Tab for page Two (Resultat) 
			// depending on if there's one or more
			// Trakts; 070308 p�d
			setPageTwoTabOnTrakt();

			// Show/Hide Tab for page Three (Sampletrees and Diamterclasses) 
			// depending on if there's one or more
			// Trakts; 070904 p�d
			setPageThreeTabOnTrakt();

			// Show/Hide Tab for page (Diagram)
			// depending on if there's one or more
			// Trakts; 100416 p�d
			setPageFourTabOnTrakt();

			if (show_pane_settings)
			{
				setShowSettingsPane();
			}
		
			m_enumAction = UPD_ITEM;
			m_bIsDirty = FALSE;
			resetIsDirty();
		}
	}
	return bReturn;
}

void CPageOneFormView::doPropertyPopulate(UINT idx)
{
	CTransaction_property rec;
	CString sProperty;
	BOOL bFound = FALSE;
	if (m_vecProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecProperty.size();i++)
		{
			rec = m_vecProperty[i];
			if (rec.getID() == idx)
			{
				bFound = TRUE;
				break;
			}	// if (rec.getID() == idx)
		}	// for (UINT i = 0;i < m_vecProperty.size();i++)
	} // if (idx >= 0 && idx < m_vecProperty.size())
	if (bFound)
	{
		m_wndEdit2.SetWindowText(rec.getPropertyNum());
		if (!rec.getUnit().IsEmpty())
		{
			sProperty.Format(_T("%s %s:%s"),
				rec.getPropertyName(),
				rec.getBlock(),
				rec.getUnit());
		}
		else
		{
			sProperty.Format(_T("%s %s"),
				rec.getPropertyName(),
				rec.getBlock());
		}
		m_wndEdit5.SetWindowText(sProperty);
		m_wndEdit5.setItemIntData(rec.getID());
	}
	else
	{
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit5.SetWindowText(_T(""));
		m_wndEdit5.setItemIntData(-1);
	}

}

void CPageOneFormView::doTraktPopulate(UINT index)
{
	// Do a check, if user has changed
	// data for active contact and if so
	// ask user to save; 070111 p�d
	isHasDataChangedPageOne();

	m_nDBIndex = index;
	populateData(m_nDBIndex);
/*
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecTraktData.size()-1));
*/
}

// This method reloads trakt data
// and populates trakt with Last entered
// trakt; 071001 p�d
// Populate active trakt; 080407 p�d
void CPageOneFormView::doPopulateData(void)
{
	getTraktIndex();	
	if (m_vecTraktIndex.size() > 0)
	{
		// Check that we are within limits; 080407 p�d
		if (m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size())
		{
			populateData(m_nDBIndex);
		}	// if (m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size())
	}	// if (m_vecTraktIndex.size() > 0)
}

// This method reloads trakt data
// and populates trakt with the index; 080820 p�d
void CPageOneFormView::doPopulateData(int trakt_id)
{
	// Show only one stand, open from UMLandValue
	// or UMGIS; 090429 p�d
	global_SHOW_ONLY_ONE_STAND = (getFromNavBarOrOtherInReg() > 1);

	BOOL bFound = FALSE;
	getTraktIndex_ObjectSelect();	
	if (m_vecTraktIndex.size() > 0)
	{
		// Try to find the TraktID equal to trakt_id; 080820 p�d
		for (UINT i = 0;i < m_vecTraktIndex.size();i++)
		{
			if (trakt_id == m_vecTraktIndex[i])
			{
				m_nDBIndex = i;
				m_nTraktId = trakt_id;

				populateData(m_nDBIndex);
				// Show/Hide Tab for page Two (Resultat) 
				// depending on if there's one or more
				// Trakts; 070308 p�d
				setPageTwoTabOnTrakt();

				// Show/Hide Tab for page (Provtr�d och Diameterklasser)
				// depending on if there's one or more
				// Trakts; 070402 p�d
				setPageThreeTabOnTrakt();

				// Show/Hide Tab for page (Diagram)
				// depending on if there's one or more
				// Trakts; 100416 p�d
				setPageFourTabOnTrakt();

				// Show settings-pane; 090303 p�d
				setShowSettingsPane();

				bFound = TRUE;
				break;
			}
		}	// for (UINT i = 0;i < m_vecTraktIndex.size();i++)
	}	// if (m_vecTraktIndex.size() > 0)
	if (!bFound)
		UMMessageBox(this->GetSafeHwnd(),m_sCouldNotFindStandMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);

}

CString CPageOneFormView::setPricelistName(CTransaction_pricelist &rec)
{
	return rec.getName();
}

void CPageOneFormView::OnBnClickedCheck1()
{
	m_bIsDirty = TRUE;
}

void CPageOneFormView::OnCbnSelchangeCboxDatepicker()
{
	m_bIsDirty = TRUE;
}

void CPageOneFormView::OnBnClickedBtnPropwnd()
{
	CTransaction_property recSelProp;
	CSearchPropertyDlg *pDlg = new CSearchPropertyDlg();
	if (pDlg != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		if (pDlg->DoModal() == IDOK)
		{
			//Ladda om fastigheterna innan, eftersom man kan ha lagt till en ny fastighet efter best�ndsf�nstret �ppnats och d� �r fastighetsvektorn ej komplett
			//Bug #2584 20111123 J�
			getProperties();
			recSelProp = pDlg->getSelectedProperty();
			doPropertyPopulate(recSelProp.getID());
			m_bIsDirty = TRUE;
		}
		delete pDlg;
	}
}
// Method used to Remove the Property connection to the Stand; 080616 p�d
void CPageOneFormView::OnBnClickedBtnPropwnd2()
{
	if (UMMessageBox(this->GetSafeHwnd(),m_sRemovePropSTandConenctionMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		return;

	if (m_pDB != NULL)
	{
		if (m_pDB->removePropTraktConnection(m_recTraktActive.getTraktID()))
		{
			m_wndEdit2.SetWindowText(_T(""));
			m_wndEdit5.SetWindowText(_T(""));
		}	// if (m_pDB->removePropTraktConnection(m_recTraktActive.getTraktID()))
	}	// if (m_pDB != NULL)
}

void CPageOneFormView::OnChangeTraktLengthOrWidth(void)
{
	double fAreal=0.0;
	double fLength=m_wndEditLength.getInt();
	double fWidth=m_wndEditWidth.getInt();
	if(fLength>0.0 && fWidth>0.0)
	{
		fAreal=(fLength*fWidth)/10000.0;
		m_wndEdit8.setFloat(fAreal,3);
		OnChangeHandledAreal();
	}
}

// Use these two methods to calculate the Total areal,
// Set in m_wndEdit6 (Areal); 070508 p�d
void CPageOneFormView::OnChangeHandledAreal(void)
{
	CString sMsg;
	double fHandled = m_wndEdit8.getFloat();
	double fConsiderd = m_wndEdit7.getFloat();
	if (fHandled > 0.0)
	{
		// Check for max and min size of Areal; 071031 p�d
		if (fHandled > AREAL_MAX_SIZE || fHandled < AREAL_MIN_SIZE)
		{
			sMsg.Format(_T("%s\n%s: %.3f\n%s: %.3f"),
				m_sArealLimitsMsg1,
				m_sArealLimitsMsg2,AREAL_MAX_SIZE,
				m_sArealLimitsMsg3,AREAL_MIN_SIZE);
			UMMessageBox(this->GetSafeHwnd(),sMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);
			// Reset areal to 0.0; 071031 p�d
//			if (fHandled > AREAL_MAX_SIZE)
				fHandled = 1.0;
//			if (fHandled < AREAL_MIN_SIZE)
//				fHandled > AREAL_MIN_SIZE;

			m_wndEdit8.setFloat(fHandled,3);
			m_wndEdit8.SetFocus();
		}
	}	// if (fHandled > 0.0)
	m_wndEdit6.setFloat(fHandled + fConsiderd,3);
}

void CPageOneFormView::OnChangeConsiderdAreal(void)
{
	CString sMsg;
	double fHandled = m_wndEdit8.getFloat();
	double fConsiderd = m_wndEdit7.getFloat();
	
	if (fConsiderd > 0.0)
	{
		// Check for max and min size of Areal; 071031 p�d
		if (fConsiderd > AREAL_MAX_SIZE || fConsiderd < AREAL_MIN_SIZE)
		{
			sMsg.Format(_T("%s\n%s: %.3f\n%s: %.3f"),
				m_sArealLimitsMsg1,
				m_sArealLimitsMsg2,AREAL_MAX_SIZE,
				m_sArealLimitsMsg3,AREAL_MIN_SIZE);
			UMMessageBox(this->GetSafeHwnd(),sMsg,m_sCaptionMsg,MB_ICONEXCLAMATION | MB_OK);
			// Reset areal to 0.0; 071031 p�d
//			if (fConsiderd > AREAL_MAX_SIZE)
				fConsiderd = 1.0;
//			if (fConsiderd < AREAL_MIN_SIZE)
//				fConsiderd = AREAL_MIN_SIZE;

			m_wndEdit7.setFloat(fConsiderd,3);
			m_wndEdit7.SetFocus();
		}
	}

	m_wndEdit6.setFloat(fHandled + fConsiderd,3);
}

// Latitude
void CPageOneFormView::OnEnChangeEdit11()
{
	CLatLongToRT90_data LLtoRT90;
	double fLat = m_wndEdit11.getFloat();
	double fLong = m_wndEdit12.getFloat();
	SweRef99LatLongtoRT90xy(&(LLtoRT90 = CLatLongToRT90_data(fLat*100.0,fLong*100.0)));
	m_wndEdit13.setFloat(LLtoRT90.getX()/100000.0,0);

}
// Longitude
void CPageOneFormView::OnEnChangeEdit12()
{
	CLatLongToRT90_data LLtoRT90;
	double fLat = m_wndEdit11.getFloat();
	double fLong = m_wndEdit12.getFloat();
	SweRef99LatLongtoRT90xy(&(LLtoRT90 = CLatLongToRT90_data(fLat*100.0,fLong*100.0)));
	m_wndEdit13.setFloat(LLtoRT90.getX()/100000.0,0);

}
/* #4205 150114 Tagit bort och lagt l�ngd och bredd under intr�ngsinfo inst�llet
// Calculate Areal (ha) from length and width
void CPageOneFormView::OnBnClickedBtnCalc11()
{
	CArealCalcDlg *pDlg = new CArealCalcDlg();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			// Only add if there's data
			if (pDlg->getAreal() > 0.0)
			{
				m_wndEdit8.setFloat(pDlg->getAreal(),3);
			}
		}	// if (pDlg->DoModal() == IDOK)
		delete pDlg;

	}	// if (pDlg != NULL)
}*/

void CPageOneFormView::OnBnClickedBtnArea()
{
	// Send message to UMGIS telling we want to know the area for this stand
	CString sLangFN = getLanguageFN(getLanguageDir(),_T("UMGIS"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	showFormView(888,sLangFN);
	CView *pViewGIS = getFormViewByID(888);
	if( pViewGIS )
	{
		pViewGIS->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x06, (LPARAM)m_recTraktActive.getTraktID());
	}
}

//#5026 PH 20160616
void CPageOneFormView::OnBnClickedBtnTransportSpara()
{

	bool updated = false;

	if(savedTraktId<0)
		return;

	int tVag=m_wndEditTransportTillVag.getInt();
	int tIndustri=m_wndEditTransportTillIndustri.getInt();

	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN)) {
			if (::UMMessageBox(this->GetSafeHwnd(),xml->str(IDS_STRING26055),xml->str(IDS_STRING26054),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				return;
		}

	}

	if(m_pDB!=NULL) {
		if(tVag>0) {
			m_pDB->updateTransportationToVagForRotPost(savedTraktId,tVag);
			updated=true;
		}
		if(tIndustri>0) {
			m_pDB->updateTransportationToIndustriForRotPost(savedTraktId,tIndustri);
			updated=true;
			
		}
		if(updated) {
			// HMS-54 M�ste kolla om det k�rs via intr�ng i s� fall skicka med traktid till dopopulatedata annars laddas 
			// best�nd som inte �r knutna till objekt och d� hittas inte best�ndet i fr�ga 20200407 J�
			// F�rut k�rdes bara dopopulatedata utan index,dvs fungerade bara f�r om rotpost k�rdes
			if(global_SHOW_ONLY_ONE_STAND)
				doPopulateData(savedTraktId); 
			else
				doPopulateData();
			runDoCalulationInPageThree();
		}
	}

	m_wndEditTransportTillVag.SetWindowTextW(L"");
	m_wndEditTransportTillIndustri.SetWindowTextW(L"");

}
