#pragma once

#include "Resource.h"

// CTabPage dialog

class CTabPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CTabPage)

public:
	CTabPage();
	virtual ~CTabPage();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
