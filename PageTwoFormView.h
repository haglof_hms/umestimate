#pragma once

#include "Resource.h"
/////////////////////////////////////////////////////////////////////////////
// CTraktDataReportRec

class CTraktDataReportRec : public CXTPReportRecord
{
	CTransaction_trakt_data recTData;
	int m_nTDataID;
	int m_nTDataTraktID;
	int m_nTDataDataType;
	int m_nTDataSpcID;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
	};

	class CLongItem : public CXTPReportRecordItemNumber
	{
	//private:
		long m_nValue;
	public:
		CLongItem(long nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstol(szText);
				SetValue(m_nValue);
		}

		long getLongItem(void)	{	return m_nValue; 	}
	};


	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTraktDataReportRec(LPCTSTR lang_fn,HWND parent)
	{
		m_nTDataID = -1;
		m_nTDataTraktID = -1;
		m_nTDataDataType = -1;
		m_nTDataSpcID = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CLongItem(0));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));

		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));

	}

	CTraktDataReportRec(CTransaction_trakt_data& rec,LPCTSTR lang_fn,HWND parent)
	{
		recTData = rec;
		m_nTDataID = rec.getTDataID();
		m_nTDataTraktID = rec.getTDataTraktID();
		m_nTDataDataType = rec.getTDataType();
		m_nTDataSpcID = rec.getSpecieID();
		AddItem(new CTextItem(rec.getSpecieName()));
		AddItem(new CFloatItem(rec.getPercent(),sz1dec));
		AddItem(new CLongItem(rec.getNumOf()));
		AddItem(new CFloatItem(rec.getDA(),sz1dec));
		AddItem(new CFloatItem(rec.getDG(),sz1dec));
		AddItem(new CFloatItem(rec.getDGV(),sz1dec));
		AddItem(new CFloatItem(rec.getHGV(),sz1dec));
		AddItem(new CFloatItem(rec.getGY(),sz3dec));
		AddItem(new CFloatItem(rec.getAvgHgt(),sz1dec));
		AddItem(new CFloatItem(rec.getH25(),sz1dec));
		AddItem(new CFloatItem(rec.getGreenCrown(),sz0dec));
		AddItem(new CFloatItem(rec.getM3SK(),sz3dec));
		AddItem(new CFloatItem(rec.getM3FUB(),sz3dec));
		AddItem(new CFloatItem(rec.getM3UB(),sz3dec));
		AddItem(new CFloatItem(rec.getAvgM3SK(),sz3dec));
		AddItem(new CFloatItem(rec.getAvgM3FUB(),sz3dec));
		AddItem(new CFloatItem(rec.getAvgM3UB(),sz3dec));
		AddItem(new CFloatItem(rec.getGrot()/1000.0  /*ton */,sz3dec));

	}

	CTransaction_trakt_data& getRecTData(void)
	{
		return recTData;
	}

	int getTDataID(void)
	{
		return m_nTDataID;
	}

	int getTDataTraktID(void)
	{
		return m_nTDataTraktID;
	}
	int getTDataSpcID(void)
	{
		return m_nTDataSpcID;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	long getColumnLong(int item)	
	{ 
		return ((CLongItem*)GetItem(item))->getLongItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};


/////////////////////////////////////////////////////////////////////////////
// CTraktAssReportRec

class CTraktAssReportRec : public CXTPReportRecord
{
	CString m_sAssortName;
	double m_fPriceM3Fub;
	double m_fPriceM3To;
	double m_fM3Fub;
	double m_fM3To;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
	};

public:

	CTraktAssReportRec(void)
	{
		m_sAssortName	= "";
		m_fPriceM3Fub	= 0.0;
		m_fPriceM3To	= 0.0;
		m_fM3Fub	= 0.0;
		m_fM3To	= 0.0;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTraktAssReportRec(LPCTSTR text,LPCTSTR assort_name,double price_m3fub,double price_m3to,double m3fub,double m3to)
	{
		m_sAssortName = assort_name;
		m_fPriceM3Fub	= price_m3fub;
		m_fPriceM3To	= price_m3to;
		m_fM3Fub	= m3fub;
		m_fM3To	= m3to;
		AddItem(new CTextItem(text));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CString getAssortName(void)	{		return m_sAssortName;	}
	double getPriceM3Fub(void)	{		return m_fPriceM3Fub;	}
	double getPriceM3To(void)		{		return m_fPriceM3To;	}
	double getM3Fub(void)				{		return m_fM3Fub;	}
	double getM3To(void)				{		return m_fM3To;	}
};

/////////////////////////////////////////////////////////////////////////////
// CTraktRotpostCostsRec

class CTraktRotpostCostsRec : public CXTPReportRecord
{
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		BOOL m_bBold;
	public:
		CTextItem(CString sValue) : 
				CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
			double fValue = _tstof(sValue);
			if (fValue < 0.0)
				SetTextColor(RGB(255,0,0));
		}

		void setIsBold(void)	
		{ 
			SetBold();
		}

		void setTextColor(COLORREF rgb_value)	
		{ 
			SetTextColor(rgb_value);
		}

	};
public:

	CTraktRotpostCostsRec(void)
	{
		AddItem(new CTextItem(_T("")));	// Empty first column

		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));

	}

	CTraktRotpostCostsRec(LPCTSTR empty,LPCTSTR cut_name,LPCTSTR cut_value,LPCTSTR cut_sort,LPCTSTR cut_val_m3fub,LPCTSTR cut_val_sort)
	{
		AddItem(new CTextItem((empty)));					
		AddItem(new CTextItem((cut_name)));
		AddItem(new CTextItem((cut_value)));
		AddItem(new CTextItem((cut_sort)));
		AddItem(new CTextItem((cut_val_m3fub)));
		AddItem(new CTextItem((cut_val_sort)));
	}

	void setColumnBold(int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CTextItem*)GetItem(col))->setIsBold();
		}
		va_end(vl);
	}

	void setColumnTextColor(COLORREF rgb_value,int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CTextItem*)GetItem(col))->setTextColor(rgb_value);
		}
		va_end(vl);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CTraktRotpostRec

class CTraktRotpostRec : public CXTPReportRecord
{
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		CFont m_fnt;
		CFont m_fntBold;
	public:
		CTextItem(CString sValue,bool set_color = false) : 
				CXTPReportRecordItemText(sValue)
		{
			m_fnt.CreateFont(17, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_fntBold.CreateFont(17, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_sText = sValue;
			double fValue = _tstof(sValue);
			if (set_color)
			{
				if (fValue < 0.0)
					SetTextColor(RGB(255,0,0));
				else
					SetTextColor(RGB(0,0,255));
			}
		}
		virtual ~CTextItem(void)
		{
			m_fnt.DeleteObject();
			m_fntBold.DeleteObject();
		}

		void setIsBold(void)	
		{ 
			SetBold();
		}

		void setFont(BOOL bold)	
		{ 
			if (bold)
				SetFont(&m_fntBold);
			else
				SetFont(&m_fnt);
		}

	};
public:

	CTraktRotpostRec()
	{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTraktRotpostRec(LPCTSTR empty,LPCTSTR netto_name,LPCTSTR netto_value,LPCTSTR netto_sort,LPCTSTR grot_value=L"",LPCTSTR grot_sort=L"",bool set_color = false)
	{
		AddItem(new CTextItem((empty)));
		AddItem(new CTextItem((netto_name)));
		AddItem(new CTextItem((netto_value),set_color));
		AddItem(new CTextItem((netto_sort)));
		AddItem(new CTextItem(grot_value,set_color));
		AddItem(new CTextItem((grot_sort)));
	}
	void setColumnBold(int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CTextItem*)GetItem(col))->setIsBold();
		}
		va_end(vl);
	}
	void setColumnFont(int col,BOOL bold)	
	{ 
		((CTextItem*)GetItem(col))->setFont(bold);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CPageTwoFormView form view

class CPageTwoFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPageTwoFormView)

	BOOL m_bInitialized;

	enumACTION m_enumAction;

	CString m_sFieldChooser;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sM3TO;
	CString m_sM3FUB;

	CString m_sPriceM3TO;
	CString m_sPriceM3FUB;

	CString m_sRemoveCapMsg;
	CString m_sRemoveMsg1;
	CString m_sExplRemoveMsg;
	CString m_sRemoveMsg2;
	CString m_sSuspciusCaluclationMsg;

	CString m_sTraktNum;
	CString m_sTraktPNum;
	CString m_sTraktName;
	CString m_sTraktSpcName;

	CString m_sGroup3_cap1;	// "Rotpost"
	CString m_sGroup3_cap2;	// "Rotpost Intr�ngsv�rdering"

	CString m_sMeter;
	CString m_sKiloMeter;

	CString m_sVolume;	// "Gagnvirke"
	CString m_sPulpVol;	// "Massaved"
	CString m_sNETTO;
	CString m_sValueTimber;
	CString m_sCostCap;
	CString m_sCostCutting;
	CString m_sCostHarvester;
	CString m_sCostSkotare;
	CString m_sCostRest;
	CString m_sCostPermanent;
	CString m_sKR_M3FUB;
	CString m_sKR;
	CString m_sSum;
	CString m_sTonne;
	CString m_sTransport;
	CString m_sTransportToRoad;
	CString m_sTransportToIndustry;
	CString m_sGrot;
	CString m_sSumGrotPrice;
	CString m_sSumGrotCost;

	CString m_sMsgCaption;
	CString m_sMsgChanges;
	CString m_sMsgChangesSaved;
	CString m_sMsgSpecieShare;
	CString m_sMsgDataMissing1;
	CString m_sMsgDataMissing2;

	CString m_sHandleSettings;
	CString m_sHandleAssortments;

	CMyReportCtrl m_wndReport;
	CXTPReportSubListControl m_wndSubList;
	CFont *m_pRotpostHeaderFont;
	CMyReportCtrl m_wndRotpost1;
	CMyReportCtrl m_wndRotpost2;

	BOOL m_bIsDirty;

	int m_nRotpostOrigin;

	// This value is calculated in method getPlotsFromDB(); 070614 p�d
	int m_fNumOfPlotsInTrakt;

	CImageList m_ilIcons;
protected:
	CPageTwoFormView();           // protected constructor used by dynamic creation
	virtual ~CPageTwoFormView();

	CXTResizeGroupBox m_wndGroup2;
	CXTResizeGroupBox m_wndGroup3;

	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;

	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;

	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;

	//vecTransactionSpecies m_vecSpecies;
	//void getSpeciesFromDB(void);
	
	CTransaction_trakt_data m_recTraktDataActive;
	CTransaction_trakt_data m_recTraktDataOnClick;
	CTransaction_trakt *m_pTraktRecord;
	void getTrakt(void);

	vecTransactionTraktData m_vecTraktData;
	void getTraktDataFromDB(vecTransactionTraktData &);
	void getTraktDataFromDB_to_be_left(vecTransactionTraktData &);
	void getTraktAssFromDB(vecTransactionTraktAss &);
	void delTraktDataFromDB(int trakt_id);

	CTransaction_trakt_misc_data m_recTraktMiscData;
	void getTraktMiscDataFromDB(int trakt_id);
	
	// Added 2007-06-14, get information about
	// Plot(s) set for this Trakt; 070614 p�d
	vecTransactionPlot m_vecTraktPlot;
	void getPlotsFromDB(void);

	vecCTransaction_trakt_rotpost m_vecCTransaction_trakt_rotpost;
	CTransaction_trakt_rotpost m_recTraktRotpost;
	BOOL getTraktRotpost(int trakt_id);

	void setupAvgAndSumData(BOOL show_message);
	void clearAll(void);

	BOOL setupReport(void);
	void LoadReportState();
	void SaveReportState();

	BOOL saveTraktDataAndAssToDB(BOOL show_msg = TRUE);
	void removeTraktDataSpecieFromDB(void);

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	CMDILicenseFrameDoc* GetDocument();

	void populateRotpostInfo(BOOL is_data);

	// Need to be public; 070308 p�d
	void addTraktData(void);	// Method opens FormView
//	void addFromSpecieDataFormView(CTransaction_trakt_data rec,vecTransactionTraktAss& vecTAss);
//	void updFromSpecieDataFormView(CTransaction_trakt_data rec,vecTransactionTraktAss& vecTAss);

	void addFromSpecieDataFormView(void);
	void updFromSpecieDataFormView(void);

	void addFromTraktSettingsPropertyGrid(int trakt_id,int spc_id,double h25,double green_crown);

	//Added function for retreiving average data on stand 200100224 J�
	void retreiveAverages(double *_Da,double *_Dg,double *_Dgv,double *_Hgv,double *_AvgHgt);

	void updTraktData(CTransaction_trakt_data& trakt_data,short todo);
	void populateData(BOOL show_message = FALSE);
	BOOL isHasDataChangedPageTwo(void);
	void doShowHideAssortmentInfo(void);
	void setIsDirty(void)
	{
		m_bIsDirty = TRUE;
	}

	// This method's called in getActiveTraktData() (set in StdAfx); 070515 p�d
	CTransaction_trakt_data &getOnClickActiveTraktDataRecord(void)
	{
		return m_recTraktDataOnClick;
	}

	// This method's called in getClickActiveTraktData() (set in StdAfx); 070515 p�d
	CTransaction_trakt_data &getActiveTraktDataRecord(void)
	{
		return m_recTraktDataActive;
	}

	// This method's called in getActiveTraktMiscData() (set in StdAfx); 070515 p�d
	CTransaction_trakt_misc_data &getActiveTraktMiscDataRecord(void)
	{
		return m_recTraktMiscData;
	}

	void SetTraktDataActive(CTransaction_trakt_data recTraktData)
	{
	m_recTraktDataActive = recTraktData;
	}

	void SetTraktDataOnClick(CTransaction_trakt_data recTraktData)
	{
	m_recTraktDataOnClick = recTraktData;
	}

	void setupGeneralInfoDlgData(void)
	{
		setupAvgAndSumData(FALSE);
	}

	int showMenu(short todo);

	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageTwoFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageTwoFormView)
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReport1RowExpandChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnShowFieldChooser();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit24();
};

#ifndef _DEBUG  // debug version in ModalFrame2View.cpp
inline CMDILicenseFrameDoc* CPageTwoFormView::GetDocument()
   { return (CMDILicenseFrameDoc*)m_pDocument; }
#endif



