
#include "StdAfx.h"
#include "StandEntryDoc.h"

#include "ResLangFileReader.h"

#include "MDITabbedView.h"
#include "PageOneFormView.h"
#include "PageTwoFormView.h"
#include "PageThreeFormView.h"
#include "PropertySelListFormView.h"
#include "PlotSelListFormView.h"
#include "SpecieDataDialog.h"
#include "PrlViewAndPrint.h"

#include "Resource.h"

extern HINSTANCE hInst;

////////////////////////////////////////////////////////////////////////////////////////////////////////
//
/*IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
}

BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		CMenu menu;
		VERIFY(menu.CreatePopupMenu());
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items
			if (item.m_nID > -1)		
				menu.AppendMenuW(MF_STRING, item.m_nID,(vecMenuItems[i].m_sText));
			else if (item.m_nID == -1)		
				menu.AppendMenuW(MF_SEPARATOR, -1, _T(""));
		}
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}


void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text)
{
	vecMenuItems.push_back(_menu_items(id,text));
}*/
IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
}

CMyControlPopup::~CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
}


BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	CMenu menu;
	HMENU sub_menu;
	short nIdx = -1;
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		menu.CreatePopupMenu();
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items

			//Specialare f�r att uppdatera tr�dslagslistan i submenyn f�r genv�g till inst�llningarna f�r tr�dslag samt sortiment 20121207 J� #3509
			if(item.m_nID == ID_SPEC_SETTINGS || item.m_nID == ID_SPEC_ASSORT)
			{
				CMDIStandEntryFormFrame* pWnd = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
				if(pWnd!=NULL)
				{
					if(pWnd->m_vecTraktData.size()>0)
					{
						sub_menu = CreatePopupMenu();
						for(UINT i=0;i<pWnd->m_vecTraktData.size();i++)
						{
							if(item.m_nID == ID_SPEC_SETTINGS)
								AppendMenu(sub_menu,MF_STRING, pWnd->m_vecTraktData[i].getSpecieID()+ID_SPEC_SETTINGS_START, pWnd->m_vecTraktData[i].getSpecieName());
							else
								AppendMenu(sub_menu,MF_STRING, pWnd->m_vecTraktData[i].getSpecieID()+ID_SPEC_ASSORT_START, pWnd->m_vecTraktData[i].getSpecieName());
						}
						menu.AppendMenu(MF_POPUP, (UINT)sub_menu, item.m_sText);
						menu.EnableMenuItem(nIdx,MF_BYPOSITION | MF_ENABLED);
					}
				}

			}
			else
			{

				if (item.m_nID > 0)		
				{
					menu.AppendMenu(MF_STRING, item.m_nID, item.m_sText);
				}
				else if (item.m_nID == 0)		
				{
					menu.AppendMenu(MF_SEPARATOR, -1, _T(""));
				}
				else if (item.m_nID < 0 && vecSubMenuItems.size() > 0 && item.m_nSubMenuID > 0) // Setup a Sub-menu
				{
					if (vecSubMenuItems.size() > 0)
					{
						sub_menu = CreatePopupMenu();
						for (UINT i1 = 0;i1 < vecSubMenuItems.size();i1++)
						{
							_menu_items sub_item = vecSubMenuItems[i1];
							if (sub_item.m_nSubMenuID == item.m_nSubMenuID)
							{
								// create sub menu item(s)
								AppendMenu(sub_menu,MF_STRING, sub_item.m_nID, sub_item.m_sText);
							}
							else if (sub_item.m_nID == 0)
							{
								AppendMenu(sub_menu,MF_SEPARATOR, -1, _T(""));
							}
						}
						menu.AppendMenu(MF_POPUP, (UINT)sub_menu, item.m_sText);
						menu.EnableMenuItem(nIdx,MF_BYPOSITION | MF_ENABLED);

					}
				}	// for (UINT i = 0;i < vecSubMenuItems.size();i++)
			}
		}
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}

void CMyControlPopup::clearVectors(void)
{	
	vecMenuItems.clear();
	vecSubMenuItems.clear();
}


void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setSubMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecSubMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}


////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CMDILicenseFrameDoc

IMPLEMENT_DYNCREATE(CMDILicenseFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDILicenseFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDILicenseFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDILicenseFrameDoc construction/destruction

CMDILicenseFrameDoc::CMDILicenseFrameDoc()
{
	// TODO: add one-time construction code here
}

CMDILicenseFrameDoc::~CMDILicenseFrameDoc()
{
}

BOOL CMDILicenseFrameDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2008-08-18 P�D
	if (!License())
		return FALSE;

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

 void CMDILicenseFrameDoc::PreCloseFrame(CFrameWnd *frame)
 {
	 CDocument::PreCloseFrame(frame);
 }

/////////////////////////////////////////////////////////////////////////////
// CMDILicenseFrameDoc serialization

void CMDILicenseFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDILicenseFrameDoc diagnostics

#ifdef _DEBUG
void CMDILicenseFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDILicenseFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDILicenseFrameDoc commands

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)


	return TRUE;
}

 void CMDIFrameDoc::PreCloseFrame(CFrameWnd *frame)
 {
	 CDocument::PreCloseFrame(frame);
 }

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands

////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

BEGIN_MESSAGE_MAP(CACBox, CComboBox)
	//{{AFX_MSG_MAP(CMyExtCBox)
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT_EX(CBN_SELCHANGE, OnCBoxChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CACBox::CACBox()
{
	m_fnt1 = new CFont();
}

void CACBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
  
	CComboBox::OnDestroy();
}

void CACBox::setLanguageFN(LPCTSTR lng_fn)
{
	m_sLangFN = lng_fn;
}

void CACBox::setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index)
{
	CString sStr;
	if (getSTDReports(shell_data_file,add_to,index,m_vecReports))
	{
		ResetContent();
		for (UINT i = 0;i < m_vecReports.size();i++)
		{
			sStr = m_vecReports[i].getCaption();
			// Check if there's a Caption for this report
			// item. If not try to read the String in language-
			// file, for this Suite/Module; 070410 p�d
			if (sStr.IsEmpty())
			{
				sStr = getLangStr(m_vecReports[i].getStrID());
			}
			AddString(sStr);

		}	// for (UINT i = 0;i < m_vecReports.size();i++)
	}	// if (getSTDReports(m_vecReports))
}

BOOL CACBox::OnCBoxChange()
{
	CMDIStandEntryFormFrame *pForm = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pForm != NULL)
	{
		pForm->SendMessage(MSG_IN_SUITE,GetCurSel());
	}
	return FALSE;
}

void CACBox::SetLblFont(int size,int weight,LPCTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

CString CACBox::getLangStr(int id)
{
	CString sStr;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sStr = xml->str(id);
		}
		delete xml;
	}
	return sStr;
}

/////////////////////////////////////////////////////////////////////////////
// CMDIStandEntryFormFrame

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
/*
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
*/
};

IMPLEMENT_DYNCREATE(CMDIStandEntryFormFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIStandEntryFormFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIStandEntryFormFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SYSCOMMAND()

	//ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)

	ON_COMMAND(ID_BTN2, OnTBBtnAddSpecieToTrakt)
	ON_UPDATE_COMMAND_UI(ID_BTN2, OnUpdateAddTBtn)

	ON_COMMAND(ID_DO_PRINTOUT, OnTBBtnPrintOut)
	ON_UPDATE_COMMAND_UI(ID_DO_PRINTOUT, OnUpdatePrintOutTBtn)

	ON_COMMAND(ID_CALC_UPDATE, OnTBBtnDoCalculation)
	ON_UPDATE_COMMAND_UI(ID_CALC_UPDATE, OnUpdateCalculationTBtn)

	ON_COMMAND(ID_INV_FILE, OnTBBtnINVFile)
	ON_UPDATE_COMMAND_UI(ID_INV_FILE, OnUpdateINVFileTBtn)

	ON_COMMAND(ID_PREVIEW, OnTBBtnPreview)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW, OnUpdatePrintPreviewTBtn)

	ON_COMMAND(ID_GIS_VIEWSTAND, OnTBBtnGisViewStand)
	ON_COMMAND(ID_GIS_UPDATESTAND, OnTBBtnGisUpdateStand)
	ON_UPDATE_COMMAND_UI(ID_GIS, OnUpdateGISTBtn)

	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)

	ON_UPDATE_COMMAND_UI(ID_TOOLS, OnUpdateToolsTBtn)

	ON_COMMAND(ID_POPUPMENU_UPDATE_PRICELIST,OnToolsUpdatePricelist)
	ON_COMMAND(ID_POPUPMENU_SHOW_PRICELIST,OnTBBtnInformation)
	ON_COMMAND(ID_POPUPMENU_UPDATE_COSTTMPL,OnToolsUpdateCosttemplate)
	ON_COMMAND(ID_POPUPMENU_SHOW_COSTTMPL,OnToolsCosttemplateInfo)
	ON_COMMAND(ID_MATCH_RANDTREES, OnToolsMatchRandTrees)
	ON_COMMAND(ID_HANDLE_RANDTREES, OnToolsHandleRandTrees)
	ON_COMMAND(ID_HANDLE_RANDTREES2, OnToolsHandleRandTrees2)
	ON_COMMAND_RANGE(ID_SPEC_SETTINGS_START,ID_SPEC_SETTINGS_END,OnSpecSet)
    ON_COMMAND_RANGE(ID_SPEC_ASSORT_START,ID_SPEC_ASSORT_END,OnSpecAssort)

	//}}AFX_MSG_MAP
	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()

// CMDIStandEntryFormFrame construction/destruction

XTPDockingPanePaintTheme CMDIStandEntryFormFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDIStandEntryFormFrame::CMDIStandEntryFormFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bConnected = FALSE;
	m_bFirstOpen = TRUE;
	m_pDB = NULL;
//	m_bShowOnlyOneStand = FALSE;

	m_bIsCalculationTBtn = TRUE;
	m_bIsINVFileTBtn = TRUE;
	m_bIsToolsTBtn = TRUE;

	m_bGISInstalled = TRUE;

	m_bClosing = FALSE;

	m_bInitReports = FALSE;
	//m_pToolsPopup = NULL;
}

CMDIStandEntryFormFrame::~CMDIStandEntryFormFrame()
{
}

void CMDIStandEntryFormFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_STAND_ENTRY_KEY);
	SavePlacement(this, csBuf);

	if (m_pDB != NULL)
		delete m_pDB;

	m_vecReports.clear();
	m_vecExchangeFunc.clear();
	m_vecTransactionPricelist.clear();
	m_vecTransaction_costtempl.clear();
	m_vecTraktPlot.clear();
//	if(m_pToolsPopup != NULL)
//		delete m_pToolsPopup;

	CMDIChildWnd::OnDestroy();
}

void CMDIStandEntryFormFrame::OnClose(void)
{

	if (m_bClosing) return;

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView1 = pTabView->getPageOneFormView();
		if (pView1 != NULL)
		{
			pView1->isHasDataChangedPageOne(2);
		}
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((REG_WP_STAND_ENTRY_LAYOUT_KEY));


	// Added 2009-09-17 p�d
	if (global_SHOW_ONLY_ONE_STAND)
	{
		m_bClosing = TRUE;
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,(LPARAM)&_doc_identifer_msg(_T("View5000"),_T("Module5200"),_T(""),5200,0,0));
	}

	CMDIChildWnd::OnClose();
}

int CMDIStandEntryFormFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;
	CString sToolTip3;
	CString sToolTip4;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	EnableDocking(CBRS_ALIGN_ANY);

	// Send message to HMSShell, please send back the DB connection; 070430 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!fileExists(m_sLangFN))
	{
		UMMessageBox(_T("ERROR! Language file missing!"));
		return -1;
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// Check if GIS module is installed (this will be used to enabled/disable toolbar button)
	m_bGISInstalled = fileExists(getSuitesDir() + _T("UMGIS.dll"));

	RECT rect;
	GetWindowRect(&rect);
	m_wndStatusBar.SetPaneWidth(0, 70);
	m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS); //m_wndStatusBar.GetPaneStyle(0) | SBPS_NOBORDERS);
	m_wndStatusBar.SetPaneWidth(1, 550);
	m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(1) | SBPS_STRETCH | SBPS_NORMAL);
	m_wndStatusBar.SetPaneWidth(2, 100);
	m_wndStatusBar.SetPaneStyle(2, SBPS_POPOUT); //m_wndStatusBar.GetPaneStyle(2) | SBPS_POPOUT);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	// control bars objects have been created and docked.
	m_paneManager.InstallDockingPanes(this);

	m_paneManager.SetTheme(m_themeCurrent);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg10.Create(this, IDD_FIELD_CHOOSER1,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN10_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg11.Create(this, IDD_FIELD_CHOOSER3,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN11_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg12.Create(this, IDD_FIELD_CHOOSER4,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN12_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg2.Create(this, IDD_FIELD_CHOOSER2,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN2_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}

	setLanguage();

	CXTPDockingPane *paneProp1 = m_paneManager.CreatePane(IDR_PROPSPANE1, CRect(0, 0,210, 120), xtpPaneDockLeft);
	paneProp1->SetOptions( xtpPaneNoCloseable );

	// Create docking panes.
	CXTPDockingPane *paneProp2 = m_paneManager.CreatePane(IDR_GENERAL_DATA_PANE, CRect(0, 0,310, 110), xtpPaneDockTop);
	paneProp2->SetOptions( xtpPaneNoCloseable );

	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((REG_WP_STAND_ENTRY_LAYOUT_KEY)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	// docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	// docking for field chooser
	m_wndFieldChooserDlg10.EnableDocking(0);
	m_wndFieldChooserDlg11.EnableDocking(0);
	m_wndFieldChooserDlg12.EnableDocking(0);
	// docking for field chooser
	m_wndFieldChooserDlg2.EnableDocking(0);

	// Don't show fieldchooser at this stadge; 070220 p�d
	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// Don't show fieldchooser at this stadge; 070509 p�d
	ShowControlBar(&m_wndFieldChooserDlg10, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg10, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	ShowControlBar(&m_wndFieldChooserDlg11, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg11, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	ShowControlBar(&m_wndFieldChooserDlg12, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg12, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// Don't show fieldchooser at this stadge; 070823 p�d
	ShowControlBar(&m_wndFieldChooserDlg2, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg2, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{

				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{
						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();
						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR1)
						{		
							setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,xml->str(IDS_STRING203));	//
							// Set special button
							pCtrl = p->GetAt(1);
							pCtrl->SetStyle(xtpButtonCaption);
							pCtrl->SetCaption(xml->str(IDS_STRING253));
							setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_PRINT,xml->str(IDS_STRING290));	//
							setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_EXECUTE,xml->str(IDS_STRING291));	//
							setToolbarBtn(sTBResFN,p->GetAt(5),0,_T(""),FALSE);	//
							setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_IMPORT,xml->str(IDS_STRING313));	//
							setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_PREVIEW,xml->str(IDS_STRING317));	//
							setToolbarBtn(sTBResFN,p->GetAt(8),RES_TB_TOOLS,xml->str(IDS_STRING330));	//
							setToolbarBtn(sTBResFN,p->GetAt(9),RES_TB_GIS,xml->str(IDS_STRING329));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())

			}	// if (fileExists(sTBResFN))
		
			/////////////////////////////////////////////////////////////////////////////////////
			// Set caption for Properties pane; 060207 p�d
			CXTPDockingPane* pPaneProp1 = GetDockingPaneManager()->FindPane(IDR_PROPSPANE1);
			ASSERT(pPaneProp1);
			if (pPaneProp1) 
			{
				pPaneProp1->SetTitle((xml->str(IDS_STRING118)));
				//pPaneProp1->Hide();
			}
			CXTPDockingPane* pPaneProp2 = GetDockingPaneManager()->FindPane(IDR_GENERAL_DATA_PANE);
			ASSERT(pPaneProp2);
			if (pPaneProp2) 
			{
				pPaneProp2->SetTitle((xml->str(IDS_STRING120)));
				//pPaneProp2->Hide();
			}
			delete xml;
		}
	}
	m_bIsInfoTBtn = TRUE;
	m_bIsAddTBtn = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bIsPrintPreviewTBtn = FALSE;

	m_bIsNewTraktAdded = FALSE;
	m_bPricelistChanged = FALSE;

	// Get pricelists 
	getPricelistsFromDB();

	return 0; // creation ok
}

void CMDIStandEntryFormFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	CMDIChildWnd::OnSysCommand(nID, lParam);
}


int CMDIStandEntryFormFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_LBL_PRINTOUTS)
	{
		CXTPControlLabel *pLbl = new CXTPControlLabel();
    lpCreateControl->pControl = pLbl;
		return TRUE;
	}

	if (lpCreateControl->nID == ID_TOOLS)
	{
		CMyControlPopup	*m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		m_pToolsPopup->clearVectors();
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_UPDATE_PRICELIST,(xml.str(IDS_STRING2602)),1);
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_SHOW_PRICELIST,(xml.str(IDS_STRING2603)),1);
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_UPDATE_COSTTMPL,(xml.str(IDS_STRING2604)),2);
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_SHOW_COSTTMPL,(xml.str(IDS_STRING2605)),2);
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				//m_pToolsPopup->addMenuIDAndText(ID_MATCH_RANDTREES,(xml.str(IDS_STRING3304)),3);
				m_pToolsPopup->addMenuIDAndText(ID_HANDLE_RANDTREES,(xml.str(IDS_STRING3305)),3);		
				m_pToolsPopup->addMenuIDAndText(ID_HANDLE_RANDTREES2,(xml.str(IDS_STRING3332)),3);		
				m_pToolsPopup->addMenuIDAndText();	// Add separator

				m_pToolsPopup->addMenuIDAndText(ID_SPEC_SETTINGS,xml.str(IDS_STRING3330),4);
				m_pToolsPopup->addMenuIDAndText(ID_SPEC_ASSORT,xml.str(IDS_STRING3331),5);

			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}
		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}
	else if (lpCreateControl->nID == ID_GIS)
	{
		CMyControlPopup	*m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		m_pToolsPopup->clearVectors();
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_GIS_VIEWSTAND,xml.str(IDS_STRING6010),1);
				m_pToolsPopup->addMenuIDAndText(ID_GIS_UPDATESTAND,xml.str(IDS_STRING6011),2);
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}
		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}
	else if (lpCreateControl->nID == ID_COMBO_PRINTOUT)
	{
		CRect rCombo;
		if (!m_cbPrintOuts.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_COMBO_PRINTOUT) )
		{
			UMMessageBox(_T("ERROR:\nCMDIStandEntryFormFrame::OnCreateControl"));
		}
		else
		{
			m_cbPrintOuts.SetOwner(this);
			GetWindowRect(&rCombo);
			m_cbPrintOuts.SetLblFont(15,FW_NORMAL);     
			m_cbPrintOuts.MoveWindow(45, 3, 150, 20);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cbPrintOuts);
//      pCB->SetFlags(xtpFlagManualUpdate);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;
				
		}
		return TRUE;
	}

	return FALSE;
}


void CMDIStandEntryFormFrame::updateSpecVec(vecTransactionTraktData vecTraktData)
{
m_vecTraktData=vecTraktData;
/*
	if(m_pToolsPopup == NULL)
	{
		m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
	}
	if(m_pToolsPopup != NULL)
	{
		m_pToolsPopup->clearVectors();
		//m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_UPDATE_PRICELIST,(xml.str(IDS_STRING2602)),1);
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_SHOW_PRICELIST,(xml.str(IDS_STRING2603)),1);
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_UPDATE_COSTTMPL,(xml.str(IDS_STRING2604)),1);
				m_pToolsPopup->addMenuIDAndText(ID_POPUPMENU_SHOW_COSTTMPL,(xml.str(IDS_STRING2605)),1);
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_MATCH_RANDTREES,(xml.str(IDS_STRING3304)),1);
				m_pToolsPopup->addMenuIDAndText(ID_HANDLE_RANDTREES,(xml.str(IDS_STRING3305)),1);		
				m_pToolsPopup->addMenuIDAndText();	// Add separator

				//L�gg till genv�g till tr�dslagsinst�llningar 20121206 J� #3509
				if(vecTraktData.size()>0)
				{
					for(UINT i=0;i<vecTraktData.size();i++)
					{					
						m_pToolsPopup->setSubMenuIDAndText(ID_SPEC_SETTINGS_START+vecTraktData[i].getSpecieID(),vecTraktData[i].getSpecieName(),1);
					}
					m_pToolsPopup->addMenuIDAndText(-1,xml.str(IDS_STRING3330),1);		

					for(UINT i=0;i<vecTraktData.size();i++)
					{
						m_pToolsPopup->setSubMenuIDAndText(ID_SPEC_ASSORT_START+vecTraktData[i].getSpecieID(),vecTraktData[i].getSpecieName(),2);
					}
					m_pToolsPopup->addMenuIDAndText(-2,xml.str(IDS_STRING3331),2);		
				}

			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}
	}
	*/
}

BOOL CMDIStandEntryFormFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDIStandEntryFormFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDIStandEntryFormFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIStandEntryFormFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {

		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_STAND_ENTRY_KEY);
		LoadPlacement(this, csBuf);

		// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
		// with ID_LPARAM_COMMAND2.
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		//AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

  }
}

void CMDIStandEntryFormFrame::OnSetFocus(CWnd* pWnd)
{
	// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
	// with ID_LPARAM_COMMAND2.
	// The return message holds a _user_msg structure, collected in the
	// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView1 = pTabView->getPageOneFormView();
		if (pView1 != NULL)
		{
			pView1->doSetNavigationButtons();
		}
	}

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIStandEntryFormFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_REPORTVIEW3,m_sLangFN);
	}
	else if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{
			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//m_nShellDataIndex = msg->getIndex();
				m_nShellDataIndex = 5000; // explicit set Identifer, macth id in ShellData file; 090212 p�d
				m_cbPrintOuts.setLanguageFN(m_sLangFN);
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cbPrintOuts.setSTDReportsInCBox(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex);
			}	// if (msg != NULL)

			m_bInitReports = TRUE;
		}
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 2)	// Identifer for GIS module
			{
				global_SHOW_ONLY_ONE_STAND = isCruiseInObject(msg->getValue2());
				global_SHOW_ONLY_ONE_STAND_FROM_GIS = true;
				setFromNavBarOrOtherInReg((global_SHOW_ONLY_ONE_STAND ? 2 : 1));
				populateDataInPageOne(msg->getValue2());
				if (global_SHOW_ONLY_ONE_STAND)
				{
					// If we open a Stand from UMLandValue, we can't go to any other stand; 090331 p�d
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
				}
			}	// if (msg->getValue1() == 2)
			if (msg->getValue1() == 3)	// Identifer for UMLandValue module; 090123 p�d
			{
				global_SHOW_ONLY_ONE_STAND = isCruiseInObject(msg->getValue2());
				global_SHOW_ONLY_ONE_STAND_FROM_GIS = false;
				setFromNavBarOrOtherInReg((global_SHOW_ONLY_ONE_STAND ? 2 : 1));
				populateDataInPageOne(msg->getValue2());
				runDoHeightCurveToDBInPageThree();
				// If we open a Stand from UMLandValue, we can't go to any other stand; 090212 p�d
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

			}	// if (msg->getValue1() == 3)
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x04)
	{
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView != NULL)
		{
			CPageOneFormView *pView1;
			if ((pView1 = pTabView->getPageOneFormView()) != NULL)
			{
				if( pView1->addTrakt() )
				{
					// Set lat/long, area in form and save changes
					int *data = (int*)lParam;
					pView1->SetLatLongArea(data[0],data[1],*(float*)&data[2] / 10000.0f); // Convert area from m2 to hectares
					pView1->saveTrakt(1);

					// Send message back to GIS with id of created stand
					CView *pViewGIS = getFormViewByID(888);	// 888 = Identifer for GIS-module; 090924 p�d
					if( pViewGIS )
					{
						pViewGIS->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x04, (LPARAM)m_pDB->getLastTraktIdentity());
					}
				}
			}
		}
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x06)
	{
		// Refresh current stand data (message from GIS)
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView != NULL)
		{
			CPageOneFormView *pView = pTabView->getPageOneFormView();
			populateDataInPageOne(pView->getTraktId());
		}
	}
	else if (wParam == (ID_EXECUTE_ITEM))
	{
		// Run recalsulation of Stand by F11; 090130 p�d
		OnTBBtnDoCalculation();
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}

LRESULT CMDIStandEntryFormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	m_nSelPrintOut = (int)wParam;

	m_bIsPrintOutTBtn = (m_nSelPrintOut > -1);

	return 0L;
}

void CMDIStandEntryFormFrame::OnUpdateAddTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsAddTBtn );
}

void CMDIStandEntryFormFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintOutTBtn );
}

void CMDIStandEntryFormFrame::OnUpdateCalculationTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsCalculationTBtn );
}

void CMDIStandEntryFormFrame::OnUpdateINVFileTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsINVFileTBtn );
}

void CMDIStandEntryFormFrame::OnUpdatePrintPreviewTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintPreviewTBtn );
}

void CMDIStandEntryFormFrame::OnUpdateGISTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( fileExists(getSuitesDir() + _T("UMGIS.dll")) );
//	pCmdUI->Enable( m_bGISInstalled );
}

void CMDIStandEntryFormFrame::OnUpdateToolsTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsToolsTBtn );
}

// CMDIStandEntryFormFrame diagnostics

#ifdef _DEBUG
void CMDIStandEntryFormFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIStandEntryFormFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIStandEntryFormFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

void CMDIStandEntryFormFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIStandEntryFormFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

LRESULT CMDIStandEntryFormFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{
			if (pPane->GetID() == IDR_GENERAL_DATA_PANE)
			{
				if (!m_wndGeneralInfoDlg.GetSafeHwnd())
				{
					if (m_wndGeneralInfoDlg.Create(CGeneralInfoDlg::IDD, this))
					{
						pPane->Attach(&m_wndGeneralInfoDlg);
					}
				}	// if (m_wndGeneralInfoDlg.Create(CGeneralInfoDlg::IDD, this))
				
				if (m_wndGeneralInfoDlg.GetSafeHwnd())
				{
					// Get active trakt record; holds information on trakt-id; 070430 p�d
					CTransaction_trakt *rec = getActiveTrakt();
					setGeneralInfo(*rec);
					CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
					if (pTabView != NULL)
					{
						CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
						if (pView2 != NULL)
						{
							pView2->setupGeneralInfoDlgData();
						}	// if (pView2 != NULL)
					}	// if (pTabView != NULL)
				}	// if (m_wndGeneralInfoDlg.GetSafeHwnd())
			} // if (pPane->GetID() == IDR_GENERAL_DATA_PANE)
			if (pPane->GetID() == IDR_PROPSPANE1)
			{
				if (!m_wndPropertyGrid.m_hWnd)
				{
					m_wndPropertyGrid.Create(CRect(0, 0, 0, 0), this, 1000);
					m_wndPropertyGrid.SetOwner(this);
					m_wndPropertyGrid.ShowHelp( FALSE );
					m_wndPropertyGrid.SetViewDivider(0.5);
					m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
					pPane->Attach(&m_wndPropertyGrid);
				}

				if (m_wndPropertyGrid.m_hWnd)
				{
					CXTPPropertyGridItem* pSettings  = NULL;
					//===============================================================================
					// CATEGORY; Exchange "Utbytesber�kning"
					pSettings  = m_wndPropertyGrid.AddCategory((m_sExchange));

					// Item: Exchange (Utbyte); 070430 p�d
					setupTypeOfExchangefunctionsInPropertyGrid(pSettings);

					//===============================================================================
					// CATEGORY; "�vriga inst�llningar"
					pSettings  = m_wndPropertyGrid.AddCategory((m_sMiscSettings));

					setupMiscSettingsValuesInPropertyGrid(pSettings);

					//===============================================================================
					// CATEGORY; "Inst�llningar (S�derbergs)"
					pSettings  = m_wndPropertyGrid.AddCategory((m_sSoderbergsSettings));

					setupSoderbergsSettingsInPropertyGrid(pSettings);
				}
			}
		} // if (!pPane->IsValid())

		return TRUE; // handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)
	return FALSE;

}

LRESULT CMDIStandEntryFormFrame::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
/*
	CString S;
	S.Format("wParam = %d\n\nXTP_PGN_ITEMVALUE_CHANGED = %d",wParam,XTP_PGN_ITEMVALUE_CHANGED);
	UMMessageBox(S);
*/
	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
		if (pItem->GetID() == ID_EXCH_FUNC_CB)
		{
			setupExchangefunctionsInPropertyGrid(pItem);
		}
		else if (pItem->GetID() == ID_PRL_FUNC_CB)
		{
			m_bPricelistChanged = TRUE;
			if (saveMiscDataToDB_prl(pItem,TRUE))
			{
				// After pricelist have been updated, we'll do a recalculation; 091109 p�d
				runDoCalulationInPageThree();
			}
			m_bPricelistChanged = FALSE;

		}
		
		if (pItem->GetID() == ID_DIAM_CLASS)
		{
			if (saveMiscDataToDB_dcls(pItem))
			{
				// After diamtercalss have been updated, we'll do a recalculation; 091109 p�d
				runDoCalulationInPageThree();
			}
		}

		if (pItem->GetID() == ID_COST_TMPL)
		{
			if (saveMiscDataToDB_costtmpl(pItem))
			{
				// After cost-template have been updated, we'll do a recalculation; 091109 p�d
				runDoCalulationInPageThree();
			}
		}

		if (pItem->GetID() == ID_ATCOAST ||
			  pItem->GetID() == ID_SOUTHEAST ||
			  pItem->GetID() == ID_REGION5 ||
			  pItem->GetID() == ID_PART_OF_PLOT ||
			  pItem->GetID() == ID_SI_H100_PINE)
		{
			saveSoderbergsSpecificsToDB(pItem);
		}

	}
	return FALSE;
}

// Added 2007-12-18 p�d
// When opening Preview usin' toolbar button;
// On pressing Print button in Preview on e.g. "Uttag" the command is
// directed to this window.
void CMDIStandEntryFormFrame::OnFilePrint(void)
{
	int nIndex = -1;
	SetFocus();
	CXTPTabManagerItem *pManager = NULL;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			if (pManager->GetIndex() == 2)
			{
				CPageThreeFormView *pView = pTabView->getPageThreeFormView();
				if (pView != NULL)
				{
					pView->doRunPrint();
				}	// if (pView != NULL)
			}
			else if (pManager->GetIndex() == 3)
			{
				CPageFourFormView *pView4 = pTabView->getPageFourFormView();
				if (pView4 != NULL)
				{
					pView4->doPrint();
				}	// if (pView != NULL)
			}
		}
	}	// if (pTabView != NULL)
}

BOOL CMDIStandEntryFormFrame::saveMiscDataToDB_prl(CXTPPropertyGridItem *pItem,BOOL removeTraktData)
{
	CString sSQLRemoveSpecies,S,qname;
	CTransaction_pricelist prl;
	BOOL bFound = FALSE,bSpcOK = TRUE;
	vecTransactionSpecies vecSpc;
	vecTransactionDCLSTree vecDCLSTrees;


	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt *rec = getActiveTrakt();
	if (m_vecTransactionPricelist.size() > 0)
	{			
		// Try to match the Selected Exchange type and Pricelist; 070608 p�d
		for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
		{
			prl = m_vecTransactionPricelist[i];			
			//M�ste nog �ven h�r inte bara leta p� namn utan �ven id p� prislista eftersom en medelprislista kan heta samma som en matrisprislista 20111229 J� Bug #2716
			//if(nTypeOfPriceList == prl.getTypeOf())
			if (m_sPricelistSelected.CompareNoCase(prl.getName()) == 0 && m_nTypeOfPriceList==prl.getTypeOf())
			{

					bFound = TRUE;
					break;
			}	// if (m_sPricelistSelected.Compare(prl1.getName()) == 0)
		}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)

		// Check status of pricelist. If Stasus have been changed to
		// "Klar att anv�nda" to "Under utveckling", tell user
		// that this pricelist can not be used; 080303 p�d
		if (bFound)
		{
			if (prl.getTypeOf() == ID_PRICELIST_IN_DEVELOPEMENT_1 ||
				  prl.getTypeOf() == ID_PRICELIST_IN_DEVELOPEMENT_2)
			{
				UMMessageBox(this->GetSafeHwnd(),m_sUpdatePricelist,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
				return FALSE;
			}
		}	// if (bFound)
		else
		{
			UMMessageBox(this->GetSafeHwnd(),m_sPricelistMissing,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		

		// Check pricelist; 101018 p�d
		if (!checkPricelist(prl.getPricelistFile())) 
		{
			UMMessageBox(this->GetSafeHwnd(),m_sPricelistPulpError,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
			// Reset data; pricelist etc; 101018 p�d
			CPageOneFormView *pView1 = NULL;
			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			if (pTabView != NULL)
			{
				if ((pView1 = pTabView->getPageOneFormView()) != NULL)
				{
					pView1->isHasDataChangedPageOne();
				}
			}
			return FALSE;
		}

		// Get trees in stand; use this to compare trees in pricelist; 2012-12-03 P�D #3479
		if (m_pDB != NULL)
		{
			m_pDB->getDCLSTrees(rec->getTraktID(),vecDCLSTrees);
		}

		// Try to setup an SQL-quaestion, that contains specie in Pricelist.
		// use this information to delete specie in table "esti_trakt_data_table" that
		// doesn't match these specieid(s); 080425 p�d
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();

		CStringArray arrQualDescNames;

		// Check pricelist, to see that there's only one pulpassortmant for exchangecalculations; 101018 p�d
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(prl.getPricelistFile()))
			{
				pPrlParser->getSpeciesInPricelistFile(vecSpc);

				//--------------------------------------------------------------------------------------
				// We'll do a check, to see if species in pricelist, at least, matches species in stand.
				// If not, tell user, and give him a chance to quit and select another pricelist or just
				// continue; 2012-12-04 P�D #3479
				std::map<int,int> mapSpc;
				for (int ii = 0;ii < vecDCLSTrees.size();ii++)
				{
					mapSpc[vecDCLSTrees[ii].getSpcID()] = 0;
					for (int iii = 0;iii  < vecSpc.size();iii++)
					{
						if (vecDCLSTrees[ii].getSpcID() == vecSpc[iii].getSpcID())
						{
							mapSpc[vecDCLSTrees[ii].getSpcID()] = 1;
							break;
						}
					}
				}	// for (int ii = 0;ii < vecDCLSTrees.size();ii++)

				for (int iv = 0;iv < vecDCLSTrees.size();iv++)
				{
					if (mapSpc[vecDCLSTrees[iv].getSpcID()] == 0)
					{
						if (UMMessageBox(this->GetSafeHwnd(),m_sPricelistLessSpec,m_sMessageCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
						{
							populateSettings();
							return FALSE;
						}
					}
				}
				//--------------------------------------------------------------------------------------

				sSQLRemoveSpecies = formatData(_T("where tdata_trakt_id=%d and "),rec->getTraktID());
				if (vecSpc.size() > 0)
				{
					for (UINT i = 0;i < vecSpc.size();i++)
					{
						if (vecSpc.size() == 1)
						{
							sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d "),vecSpc[i].getSpcID());
						}
						else if (vecSpc.size() > 1 && i < vecSpc.size() - 1)
						{
							sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d and "),vecSpc[i].getSpcID());
						}
						else 
						{
							sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d "),vecSpc[i].getSpcID());
						}

						// Get name of first quality description for specie
						CStringArray arrTmp;
						pPrlParser->getQualDescNameForSpecie(vecSpc[i].getSpcID(), arrTmp);
						if(arrTmp.GetSize() > 0) arrQualDescNames.Add( arrTmp.GetAt(0) );

					}	// for (UINT i = 0;i < vecSpc.size();i++)
					if (m_pDB != NULL)
					{
						m_pDB->removeTraktDataSpecies(rec->getTraktID(),sSQLRemoveSpecies);
					}
				}	// if (vecSpc.size() > 0)
			}	// if (pPrlParser->loadStream(prl.getPricelistFile()))
			delete pPrlParser;
		}	// if (pPrlParser != NULL)

		if (bFound)
		{
			CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
			if (pTabView != NULL)
			{
				// Pricelist set in esti_trakt_pricelist_table
				// OBS! On saving a pricelist, if there's already a pricelist for this
				// Trakt, ask user if he realy want's to change pricelist, becuse information
				// on already entered pricelist'll be lost; 070507 p�d
				// This is also true if user choose to Update already entered pricelist.
				// The update comes from prn_pricelist_table; 070507 p�d
				// On changing/updating pricelist, all information in table esti_trakt_data_table
				// should be deleted. Because data are, more or less, dependent on pricelist.
				// I.e. which species in pricelist etc; 070507 p�d

				if (m_bConnected)
				{
					if (m_pDB != NULL)
					{
						// Save new pricelist; 070507 p�d
						CTransaction_trakt_misc_data data = CTransaction_trakt_misc_data(rec->getTraktID(),
							prl.getName(),
							prl.getTypeOf(),
							prl.getPricelistFile(),
							_T(""),
							-1,
							_T(""),
							0.0,
							m_recTraktMiscData.getCreated());

						if (!m_pDB->addTraktMiscData_prl(data))
							m_pDB->updTraktMiscData_prl(data);

						// Reset quality desciption id after pricelist change (matrix pricelist only)
						if (m_bPricelistChanged && m_nTypeOfPriceList == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
						{
							if (vecSpc.size() > 0)
							{
								for (UINT i = 0;i < vecSpc.size();i++)
								{
									CTransaction_trakt_set_spc spc(vecSpc[i].getSpcID(), 0, rec->getTraktID(), 0, vecSpc[i].getSpcID(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _T(""), arrQualDescNames[i], 0, 0, 0, 0, 0, 0);
									m_pDB->updTraktSetSpc_qdesc_all(spc);
								}
							}
						}

						// Delete info. in table esti_trakt_data_table, if any ... ; 070507 p�d
						// Check: Only remove traktdata if user have selected another pricelist; 070531 p�d
						if (removeTraktData)
						{
							m_pDB->removeTraktTrans(rec->getTraktID());

							m_pDB->delAssTree(rec->getTraktID());

							m_pDB->removeTraktAss(rec->getTraktID());

							m_pDB->resetTraktData(rec->getTraktID());

							// Reset calculated data, i.e. volumes; 071127 p�d
							m_pDB->resetSampleTrees(rec->getTraktID());

						}	// if (removeTraktData)
					}	// if (m_pDB != NULL)
				}	// if (m_bConnected)
				//	Save pricelist to esti_trakt_pricelist_table. 
				//	Add pricelist data to CPageTwoFormView; 070430 p�d
				CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
				if (pView2 != NULL)
				{
					pView2->addFromTraktSettingsPropertyGrid(rec->getTraktID(),
																									 -1 /* Don't check specie */,
																									 0.0 /* No h25 value on manual entry */,
																									 0.0 /* No GreenCrown on manual entry 080625 P�D*/);
				}	// if (pView != NULL)

				CPageThreeFormView *pView3 = pTabView->getPageThreeFormView();
				if (pView3 != NULL)
				{
					pView3->populateData();
				}	// if (pView != NULL)
			}	// if (pTabView != NULL)
		}
		// I think we can enable Tree tab now. Open tab when user
		// selects and saves a pricelist; 070510 p�d
		enableTabPage(2,TRUE);	// Enable tab; 070510 p�d
	}	// if (m_vecTransactionPricelist.size() > 0)

	return TRUE;

}

BOOL CMDIStandEntryFormFrame::saveMiscDataToDB_dcls(CXTPPropertyGridItem *pItem)
{
	// Check that diameterclass > 0.0
	if (m_fDiamClass == 0.0)
	{
		UMMessageBox(this->GetSafeHwnd(),(m_sDiameterclassErr),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt *rec = getActiveTrakt();

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Save new pricelist; 070507 p�d
			CTransaction_trakt_misc_data data = CTransaction_trakt_misc_data(rec->getTraktID(),
				_T(""),
				0,
				_T(""),
				_T(""),
				-1,
				_T(""),
				m_fDiamClass,
				m_recTraktMiscData.getCreated());
//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�d
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDIStandEntryFormFrame::saveMiscDataToDB_dcls 1"));

CString sDateDone,sMsg;
if (m_pDB->checkTraktMiscDataDate(data,sDateDone) == 0)
{	
	SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
	getTraktMiscDataFromDB();
	m_fDiamClass = m_recTraktMiscData.getDiamClass();
	if (m_wndPropertyGrid.m_hWnd)
		m_wndPropertyGrid.Refresh();
	populateSettings();
	return FALSE;
}
#endif

			if (!m_pDB->addTraktMiscData_dcls(data))
				m_pDB->updTraktMiscData_dcls(data);
			
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)

	return TRUE;

}

BOOL CMDIStandEntryFormFrame::saveMiscDataToDB_costtmpl(CXTPPropertyGridItem *pItem)
{
		CTransaction_costtempl recCostTmpl;
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();

		// Reload cost templates and match name of m_sCostTmplBindTo to the
		// list of cost templates; 071010 p�d
		getCostTemplateFromDB();
		if (m_vecTransaction_costtempl.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
			{
				recCostTmpl = m_vecTransaction_costtempl[i];
				if (recCostTmpl.getTemplateName().Compare(m_sCostTmplBindTo) == 0)
				{
					break;
				}	// if (rec.getTemplateName().Compare(m_sCostTmplBindTo) == 0)
			}	// for (UINT i = =;i < m_vecTransaction_costtempl.size();i++)
		}	// if (m_vecTransaction_costtempl.size() > 0)
		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				// Save new pricelist; 070507 p�d
				CTransaction_trakt_misc_data data = CTransaction_trakt_misc_data(rec->getTraktID(),
																													 _T(""),
																													 0,
																													 _T(""),
																													 recCostTmpl.getTemplateName(),
																													 recCostTmpl.getTypeOf(),
																													 recCostTmpl.getTemplateFile(),
																													 0.0,
																													 m_recTraktMiscData.getCreated());
//************************************************************************************
//	DATASECURITY HANDLING; 090317 p�d
// Client/Server specifics; 090317 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDIStandEntryFormFrame::saveMiscDataToDB_costtmpl 1 ") + m_recTraktMiscData.getCostsName());

CString sDateDone,sMsg;
if (m_pDB->checkTraktMiscDataDate(data,sDateDone) == 0)
{	
	SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
	getTraktMiscDataFromDB();
	m_sCostTmplBindTo = m_recTraktMiscData.getCostsName();
	if (m_wndPropertyGrid.m_hWnd)
		m_wndPropertyGrid.Refresh();
	populateSettings();
	return FALSE;
}
#endif

				if (!m_pDB->addTraktMiscData_costtmpl(data))
					m_pDB->updTraktMiscData_costtmpl(data);
			}	// if (m_pDB != NULL)
		}	// if (m_bConnected)
		return TRUE;
}

// Added 071207 P�D
// Save "S�derbergs" specific data to esti_trakt_table
void CMDIStandEntryFormFrame::saveSoderbergsSpecificsToDB(CXTPPropertyGridItem *pItem)
{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		if (m_bConnected)
		{
			if (m_pDB != NULL)
			{
				// Save "S�derberg" specifis data to DB; 071207 p�d

				if (m_sBindToSIH100_Pine.Find(_T("T")) == -1)
					m_sBindToSIH100_Pine = _T("T");

				if (!m_pDB->addTrakt_Soderbergs_Specifics(rec->getTraktID(),
																								m_bBindToIsAtCoast,
																								m_bBindToIsSouthEast,
																								m_bBindToIsRegion5,
																								m_bBindToIsPartOfPlot,
																								m_sBindToSIH100_Pine))
				{
					m_pDB->updTrakt_Soderbergs_Specifics(rec->getTraktID(),
																								m_bBindToIsAtCoast,
																								m_bBindToIsSouthEast,
																								m_bBindToIsRegion5,
																								m_bBindToIsPartOfPlot,
																								m_sBindToSIH100_Pine);
				}
		}	// if (m_pDB != NULL)
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView != NULL)
		{
			CPageOneFormView *pView1 = pTabView->getPageOneFormView();
			if (pView1 != NULL)
			{
				pView1->reloadTrakt(rec->getTraktID());
			}
		}
		populateSettings();
	}	// if (m_bConnected)
}

void CMDIStandEntryFormFrame::setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString S;
	UCFunctions flist;
	// get exchange functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getExchangeFunctions(m_vecExchangeFunc);
	if (m_vecExchangeFunc.size() > 0)
	{
		CXTPPropertyGridItem *pExchangeItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalculateAs)));
		if (pExchangeItem != NULL)
		{
			pExchangeItem->BindToString(&m_sExchPricelistStr);

			for (UINT j = 0;j < m_vecExchangeFunc.size();j++)
			{
				flist = m_vecExchangeFunc[j];
				pExchangeItem->GetConstraints()->AddConstraint((flist.getName()),j);
			}	// for (UINT j = 0;j < func_list.size();j++)
		}	// if (pExchangeItem != NULL)

		// Retrive information on Pricelist,Diameterclass etc. for Trakt; 070618 p�d
		getTraktMiscDataFromDB();
		// Get Exchange function selected. This value is based on 
		// value in m_recTraktMiscData.getTypeOf() and compared with data
		// in m_vecExchangeFunc (UCFunctions class); 070618 p�d
		if (m_vecExchangeFunc.size() > 0)
		{
				for (UINT j = 0;j < m_vecExchangeFunc.size();j++)
				{
					flist = m_vecExchangeFunc[j];
					// Check if we can find the typeof in m_recTraktMiscData
					// to match the typeof in flist; 070618 p�d
					if (m_recTraktMiscData.getTypeOf() == flist.getIdentifer() && 
							m_recTraktMiscData.getTSetTraktID() == getActiveTrakt()->getTraktID())
					{
						m_sExchPricelistStr = flist.getName();
						break;
					}
				}
		}	// for (UINT j = 0;j < func_list.size();j++)

		pExchangeItem->SetFlags(xtpGridItemHasComboButton);
		pExchangeItem->SetConstraintEdit( FALSE );
		pExchangeItem->SetID(ID_EXCH_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
		setupExchangefunctionsInPropertyGrid(pExchangeItem);
	}	// if (func_list.size() > 0)

	CXTPPropertyGridItem *pCostTmpl = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sCostTmpl)));
	pCostTmpl->SetFlags(xtpGridItemHasComboButton);
	pCostTmpl->SetConstraintEdit( FALSE );
	pCostTmpl->SetID(ID_COST_TMPL);
	pCostTmpl->BindToString(&m_sCostTmplBindTo);
	// Load Cost templates from cost_template_table
	getCostTemplateFromDB();
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
			pCostTmpl->GetConstraints()->AddConstraint((rec.getTemplateName()),rec.getID());
		}
	}
	m_sCostTmplBindTo = m_recTraktMiscData.getCostsName();


}

// This function is called from OnGridNotify
// Actaully adds pricelists for selected exchange functions; 070507 p�d
void CMDIStandEntryFormFrame::setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	UCFunctions list;
	CString sInfo,S;
	UINT nIndex;
	CXTPPropertyGridItem *pExchangeItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	//Lagt till en variablen f�r att h�lla reda p� vilken typ av prislista som v�ljs, 
	//eftersom det itne r�cker med att kika p� namn d� tv� prislistor med olika typer kan ha samma namn 20111229 J� Bug #2716
	m_nTypeOfPriceList=0;
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();

	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)

	if (!pItem->HasChilds())
	{
		pExchangeItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sPricelist)));
		pExchangeItem->BindToString(&m_sPricelistSelected);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pExchangeItem = pGridChilds->GetAt(0);
			m_sPricelistSelected = "";
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pExchangeItem != NULL)
	{
		pItem->Expand();
		pExchangeItem->GetConstraints()->RemoveAll();

		if (nIndex >= 0  && nIndex < m_vecExchangeFunc.size())
		{
			list = m_vecExchangeFunc[nIndex];
		}

		// Add pricelists for Exchange calculation model selected; 070430 p�d
		if (m_vecTransactionPricelist.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
			{
				CTransaction_pricelist prl = m_vecTransactionPricelist[i];
				if (list.getIdentifer() == -1)
				{
					// Check the Function identifer; 070416 p�d
					if (m_recTraktMiscData.getTypeOf() == prl.getTypeOf() && prl.getTypeOf() > 0)
					{
						sInfo.Format(_T("%s"),prl.getName());
						pExchangeItem->GetConstraints()->AddConstraint((prl.getName()),prl.getID());
						m_nTypeOfPriceList=m_recTraktMiscData.getTypeOf();
					}	// if (list.getID() == nFunctionIdentifer)
				}
				else if (list.getIdentifer() > 0)
				{
					// Check the Function identifer; 070416 p�d
					if (list.getIdentifer() == prl.getTypeOf() && prl.getTypeOf() > 0)
					{
						sInfo.Format(_T("%s"),prl.getName());
						pExchangeItem->GetConstraints()->AddConstraint((prl.getName()),prl.getID());
						m_nTypeOfPriceList=list.getIdentifer();
					}	// if (list.getID() == nFunctionIdentifer)
				}
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pExchangeItem->SetFlags(xtpGridItemHasComboButton);
		pExchangeItem->SetConstraintEdit( FALSE );
		pExchangeItem->SetID(ID_PRL_FUNC_CB);
		m_sPricelistSelected = m_recTraktMiscData.getName();
		//m_nPricelistSelectedType = m_recTraktMiscData.getTypeOf();
	} // 	if (pExchangeItem != NULL)

}

// This function is called from OnDockingPaneNotify
// "Omf�ringstal"
void CMDIStandEntryFormFrame::setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemDouble *pDiamClass = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sDiamClass),0.0,_T("%.1f")));
	pDiamClass->SetID(ID_DIAM_CLASS);
	pDiamClass->BindToDouble(&m_fDiamClass);
	setReadOnlyInSettingsProperty(TRUE); //m_recTraktMiscData.getDiamClass() > 0.0);

	m_fDiamClass = m_recTraktMiscData.getDiamClass();
/*
	CXTPPropertyGridItem *pCostTmpl = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem(_T(m_sCostTmpl)));
	pCostTmpl->SetFlags(xtpGridItemHasComboButton);
	pCostTmpl->SetConstraintEdit( FALSE );
	pCostTmpl->SetID(ID_COST_TMPL);
	pCostTmpl->BindToString(&m_sCostTmplBindTo);
	// Load Cost templates from cost_template_table
	getCostTemplateFromDB();
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
			pCostTmpl->GetConstraints()->AddConstraint(_T(rec.getTemplateName()),rec.getID());
		}
	}
	m_sCostTmplBindTo = m_recTraktMiscData.getCostsName();
*/
}

void CMDIStandEntryFormFrame::setupSoderbergsSettingsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemBool *pExtraInfo1 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sAtCoast,TRUE));
	pExtraInfo1->SetID(ID_ATCOAST);
	pExtraInfo1->BindToBool(&m_bBindToIsAtCoast);

	CXTPPropertyGridItemBool *pExtraInfo2 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sSouthEast,TRUE));
	pExtraInfo2->SetID(ID_SOUTHEAST);
	pExtraInfo2->BindToBool(&m_bBindToIsSouthEast);

	CXTPPropertyGridItemBool *pExtraInfo3 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sRegion5,TRUE));
	pExtraInfo3->SetID(ID_REGION5);
	pExtraInfo3->BindToBool(&m_bBindToIsRegion5);

	CXTPPropertyGridItemBool *pExtraInfo4 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sPartOfPlot,TRUE));
	pExtraInfo4->SetID(ID_PART_OF_PLOT);
	pExtraInfo4->BindToBool(&m_bBindToIsPartOfPlot);

	CXTPPropertyGridItem *pSIH100_pine = (CXTPPropertyGridItem *)pItem->AddChildItem(new CXTPPropertyGridItem(m_sSIH100_Pine));
	pSIH100_pine->SetID(ID_SI_H100_PINE);
	pSIH100_pine->BindToString(&m_sBindToSIH100_Pine);
	pSIH100_pine->SetMask(_T("T00"),_T("T__"));

	CTransaction_trakt *pTrakt = getActiveTrakt();
	if (pTrakt != NULL)
	{
		m_bBindToIsAtCoast = pTrakt->getTraktNearCoast();
		m_bBindToIsSouthEast = pTrakt->getTraktSoutheast();
		m_bBindToIsRegion5 = pTrakt->getTraktRegion5();
		m_bBindToIsPartOfPlot = pTrakt->getTraktPartOfPlot();
		m_sBindToSIH100_Pine = pTrakt->getTraktSIH100_Pine();
	}	// if (pTrakt != NULL)

}


// Pricelist set in prn_pricelist_table
void CMDIStandEntryFormFrame::getPricelistsFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_vecTransactionPricelist.clear();
		m_pDB->getPricelists(m_vecTransactionPricelist);
	}
}

void CMDIStandEntryFormFrame::getCostTemplateFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_vecTransaction_costtempl.clear();
		m_pDB->getCostTmpls(m_vecTransaction_costtempl);
	}
}

// Get pricelist for trakt in esti_trakt_pricelist_table; 070430 p�d
void CMDIStandEntryFormFrame::getTraktMiscDataFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTraktMiscData(rec->getTraktID(),m_recTraktMiscData);
	}	// if (m_pDB != NULL)
}

/*void CMDIStandEntryFormFrame::getTraktFromDB(void)
{
	if (m_pDB != NULL)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt *rec = getActiveTrakt();
		m_pDB->getTrakt(rec->getTraktID(),m_recTrakt);
	}	// if (m_pDB != NULL)
}*/

void CMDIStandEntryFormFrame::populateSettings(void)
{
	CString S;
	int nIndex = -1;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	UCFunctions flist;
//@b	getTraktFromDB();
	// Retrive information on Pricelist,Diameterclass etc. for Trakt; 070618 p�d
	getTraktMiscDataFromDB();
//	if (pTrakt != NULL)
//	{
//		if (m_recTraktMiscData.getTSetTraktID() == pTrakt->getTraktID())
//		{
			// Get Exchange function selected. This value is based on 
			// value in m_recTraktMiscData.getTypeOf() and compared with data
			// in m_vecExchangeFunc (UCFunctions class); 070618 p�d
			if (m_vecExchangeFunc.size() > 0)
			{
					for (UINT j = 0;j < m_vecExchangeFunc.size();j++)
					{
						flist = m_vecExchangeFunc[j];
						// Check if we can find the typeof in m_recTraktMiscData
						// to match the typeof in flist; 070618 p�d
						if (m_recTraktMiscData.getTypeOf() == flist.getIdentifer() && 
								m_recTraktMiscData.getTSetTraktID() == getActiveTrakt()->getTraktID())
						{
							m_sExchPricelistStr = flist.getName();
							nIndex = j;
							break;
						}
					}
			}	// for (UINT j = 0;j < func_list.size();j++)
			// Also setup list of Pricelists in PropertyGrid; 080328 p�d
			CXTPPropertyGridItem *pItem = m_wndPropertyGrid.FindItem(ID_EXCH_FUNC_CB);
			if (pItem != NULL)
			{
				pItem->GetConstraints()->SetCurrent(nIndex);
				setupExchangefunctionsInPropertyGrid(pItem);
			}

			m_sPricelistSelected = (m_recTraktMiscData.getName());
			m_nTypeOfPriceList = (m_recTraktMiscData.getTypeOf());
			m_fDiamClass = m_recTraktMiscData.getDiamClass();
			m_sCostTmplBindTo = m_recTraktMiscData.getCostsName();
			// "S�derbergs" specific data, set in esti_trakt_table; 071207 p�d
			//�ndrat h�r, m_recTrakt h�mtades aldrig in fr�n DB vid uppdateringar, anv�nds aldrig n�gon annanstans �n h�r heller 
			//s� jag tog bort den och anv�nde pekaren pTrakt ist�llet Bug #2654 20111213 J�
			/*
			m_bBindToIsAtCoast = m_recTrakt.getTraktNearCoast();
			m_bBindToIsSouthEast = m_recTrakt.getTraktSoutheast();
			m_bBindToIsRegion5 = m_recTrakt.getTraktRegion5();
			m_bBindToIsPartOfPlot	= m_recTrakt.getTraktPartOfPlot();*/
			m_bBindToIsAtCoast		= pTrakt->getTraktNearCoast();
			m_bBindToIsSouthEast	= pTrakt->getTraktSoutheast();
			m_bBindToIsRegion5		= pTrakt->getTraktRegion5();
			m_bBindToIsPartOfPlot	= pTrakt->getTraktPartOfPlot();

			m_sBindToSIH100_Pine = pTrakt->getTraktSIH100_Pine();
/*
		} // if (m_recTraktMiscData.getTSetTraktID() == pTrakt->getTraktID())
		else
		{
			m_sPricelistSelected.Empty();
			m_fDiamClass = 0.0;
			m_sCostTmplBindTo.Empty();
			// "S�derbergs" specific data, set in esti_trakt_table; 071207 p�d
			m_bBindToIsAtCoast = FALSE;
			m_bBindToIsSouthEast = FALSE;
			m_bBindToIsRegion5 = FALSE;
			m_bBindToIsPartOfPlot	= FALSE;
			m_sBindToSIH100_Pine.Empty();
		}
	}	// if (pTrakt != NULL)
*/
	m_wndPropertyGrid.Refresh();
}

// Clear settings for "Utbytesber�kning/Prislista" and Diam.class etc; 070618 p�d
void CMDIStandEntryFormFrame::clearPropertyGrid(void)
{
	m_bIsNewTraktAdded = TRUE;

	m_sExchPricelistStr.Empty();
	m_sPricelistSelected.Empty();
	m_fDiamClass = DEF_DIAMCLASS;
	m_sCostTmplBindTo.Empty();

	m_bBindToIsAtCoast = FALSE;
	m_bBindToIsSouthEast = FALSE;
	m_bBindToIsRegion5 = FALSE;
	m_bBindToIsPartOfPlot = FALSE;
	m_sBindToSIH100_Pine.Empty();

	if (m_wndPropertyGrid.GetSafeHwnd())
	{
//		setReadOnlyInSettingsProperty(FALSE);
		setReadOnlyInSettingsProperty(TRUE);
	}
	m_wndPropertyGrid.Refresh();
}

void CMDIStandEntryFormFrame::setReadOnlyInSettingsProperty(BOOL ro)
{
	if (m_wndPropertyGrid.GetSafeHwnd())
	{
		CXTPPropertyGridItemDouble *pDiamClass = (CXTPPropertyGridItemDouble*)m_wndPropertyGrid.FindItem(ID_DIAM_CLASS);
		if (pDiamClass != NULL)
		{
			pDiamClass->SetReadOnly(ro);
		}	// if (pDiamClass != NULL)
	}	// if (m_wndPropertyGrid.GetSafeHwnd())
}


void CMDIStandEntryFormFrame::OnTBBtnAddSpecieToTrakt()
{
	int nIndex = -1; 
	CXTPTabManagerItem *pManager = NULL;
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		// Need to find out which Tab the USER is on, because
		// we're doin' different thins depending on which tab we're on; 070509 p�d
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			nIndex = pManager->GetIndex();
		}	// if (pManager != NULL)
/*
		if (nIndex == 1)
		{
			CPageTwoFormView *pView1 = pTabView->getPageTwoFormView();
			if (pView1 != NULL)
			{
				pView1->addTraktData();
			}	// if (pView != NULL)
		}
		else 
*/
		if (nIndex == 2)
		{
			CPageThreeFormView *pView2 = pTabView->getPageThreeFormView();
			if (pView2 != NULL)
			{
				pView2->addNewTree();
			}	// if (pView != NULL)
		}

	}	// if (pTabView != NULL)
}

void CMDIStandEntryFormFrame::OnTBBtnPrintOut()
{
	CTransaction_trakt *m_pTraktRecord = NULL;
	CString sArgStr,S;
	CString sReportPathAndFN;
	CString sFileExtension;
	int nIdx = m_nSelPrintOut;

	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	if (nIdx > -1 && m_vecReports.size() > 0)
	{
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView)
		{
			CXTPTabManagerItem *pManager = pTabView->getTabCtrl().getTabPage(1);
			if (pManager)
			{
				m_pTraktRecord = (CTransaction_trakt *)pManager->GetData();
				if (m_pTraktRecord != NULL)
				{
					sArgStr.Format(_T("%d;"),m_pTraktRecord->getTraktID());
					sReportPathAndFN.Format(_T("%s%s\\%s"),
																	getReportsDir(),
																	getLangSet(),
																	m_vecReports[nIdx].getFileName());
					if (fileExists(sReportPathAndFN))
					{
						// 090129 p�d
						// Try to get extension of file, to determin' if it's
						// a FastReport or Crystal Report.
						// I.e.
						//	.fr3 = FastReports
						//	.rpt = Crystal Reports
						sFileExtension = sReportPathAndFN.Right(3);

						if (sFileExtension.CompareNoCase(_T("fr3")) == 0)
						{
							AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
								(LPARAM)&_user_msg(300,	// ID = 333 for CrystalReport, ID = 300 for FastReports
								_T("OpenSuiteEx2"),			// Exported/Imported function
								_T("Reports.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
								(sReportPathAndFN),		// Use this report
								_T(""),
								(sArgStr)));
						}
						else if (sFileExtension.CompareNoCase(_T("rpt")) == 0)
						{
							AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
								(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
								_T("OpenSuiteEx"),			// Exported/Imported function
								_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
								(sReportPathAndFN),		// Use this report
								(sReportPathAndFN),
								(sArgStr)));
						}
					}
				}
			}
		}	// if (pTabView)
	}	// if (nIdx > -1 && nIdx < m_vecReports.size())

//	SetCurSel(-1);

}
//====================================================================
// THIS FUNCTION RUNS THE CALCULATIONS; 070521 p�d
void CMDIStandEntryFormFrame::OnTBBtnDoCalculation()
{
	CPageOneFormView *pView1 = NULL;
	CPageTwoFormView *pView2 = NULL;
	CPageThreeFormView *pView3 = NULL;
	BOOL bReturn = TRUE;
	// Do a check if somethingss changed before recalculation; 070926 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		if ((pView1 = pTabView->getPageOneFormView()) != NULL)
		{
			bReturn = pView1->isHasDataChangedPageOne();
		}
	}
	if (bReturn)
	{

		runDoCalulationInPageThree(-1,FALSE,FALSE);
		if ((pView3 = pTabView->getPageThreeFormView()) != NULL)
		{
			pView3->SetFocus();
		}
	}

	pView1 = NULL;
	pView2 = NULL;
	pView3 = NULL;
	pTabView = NULL;
}

//====================================================================
// THIS FUNCTION DISPLAYES INFORMATION ON PRICELIST ETC. IN TRAKT; 070704 p�d
void CMDIStandEntryFormFrame::OnTBBtnInformation()
{
	// Setup a new record for misc data, and set TypeOf (Pricelist) equal to -1.
	// This will indicate that we'll look at the Cost-template instead of the Pricelist; 091021 p�d
	CTransaction_trakt_misc_data rec = CTransaction_trakt_misc_data(m_recTraktMiscData.getTSetTraktID(),
																																	m_recTraktMiscData.getName(),
																																	m_recTraktMiscData.getTypeOf(),
																																	m_recTraktMiscData.getXMLPricelist(),
																																	_T(""),
																																	-1,
																																	_T(""),
																																	0.0,
																																	m_recTraktMiscData.getCreated());

	showFormView(IDD_FORMVIEW10,m_sLangFN,(LPARAM)&rec);
}

void CMDIStandEntryFormFrame::OnTBBtnINVFile()
{
	_doc_identifer_msg msg;
	CTransaction_trakt traktRec;

	// We'll save data, before open INV/HXL-file reading; 090626 p�d
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView1 = pTabView->getPageOneFormView();
		if (pView1 != NULL)
		{
			pView1->isHasDataChangedPageOne(2);

			traktRec = pView1->getActiveTraktRecord();
			if (traktRec.getTraktID() == -1)
			{
				UMMessageBox(this->GetSafeHwnd(),m_sNoStandsMsg1,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
			}
			else
			{
				BOOL ret = FALSE;
				typedef BOOL *(*funcShowImportDialog)(int traktId, DB_CONNECTION_DATA& conn);
				funcShowImportDialog showImportDialog;

				CString sFN; sFN.Format(_T("%s%s"),getModulesDir(),_T("UMIndata.dll"));
				HINSTANCE hInst = AfxLoadLibrary(sFN);
				if (hInst)
				{
					showImportDialog = (funcShowImportDialog)GetProcAddress((HMODULE)hInst, "ShowImportDialog");
					if (showImportDialog)
					{
						if (showImportDialog(traktRec.getTraktID(), m_dbConnectionData))
						{
							// Populate data and run calculation
							if (global_SHOW_ONLY_ONE_STAND)
							{
								CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
								if (pTabView != NULL)
								{
									CPageOneFormView *pView = pTabView->getPageOneFormView();
									if (pView != NULL)
									{
										ret = populateDataInPageOne(pView->getActiveTraktRecord().getTraktID());
									}
								}
							}
							else
							{
								ret = populateDataInPageOne(); // 'll also polupulate page two and three; 080117 p�d
							}
							if (ret)
							{
								runDoCalulationInPageThree();
							}
						}
					}

					AfxFreeLibrary(hInst);
				}
			}
		}	// if (pView1 != NULL)
	}	// if (pTabView != NULL)
}

void CMDIStandEntryFormFrame::OnTBBtnPreview()
{
	SetFocus();
	CXTPTabManagerItem *pManager = NULL;

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		pManager = pTabView->getTabCtrl().getSelectedTabPage();
		if (pManager != NULL)
		{
			if (pManager->GetIndex() == 2)
			{
				CPageThreeFormView *pView = pTabView->getPageThreeFormView();
				if (pView != NULL)
				{
					pView->doRunPrintPreview();
				}
			}
			else if (pManager->GetIndex() == 3)
			{
				CPageFourFormView *pView4 = pTabView->getPageFourFormView();
				if (pView4 != NULL)
				{
					pView4->doPreview();
				}
			}
	
		}
	}
}
// Added 2008-08-20 P�D
// Open GIS module, displayin' the active Trakt; 080820 p�d
void CMDIStandEntryFormFrame::OnTBBtnGisViewStand()
{
	ViewStandInGis();
}

void CMDIStandEntryFormFrame::OnTBBtnGisUpdateStand()
{
	ViewStandInGis(TRUE);
}

void CMDIStandEntryFormFrame::ViewStandInGis(BOOL updateCoords /*=FALSE*/)
{
	_doc_identifer_msg msg;
	CString sLangFN;
	CTransaction_trakt traktRec;

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageOneFormView *pView1 = pTabView->getPageOneFormView();
		if (pView1 != NULL)
		{
			sLangFN = getLanguageFN(getLanguageDir(),_T("UMGIS"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
			showFormView(888,sLangFN);
			traktRec = pView1->getActiveTraktRecord();
			if (traktRec.getTraktID() == -1)
			{
				UMMessageBox(this->GetSafeHwnd(),m_sNoStandsMsg2,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
			}
			else
			{
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
								(LPARAM)&_doc_identifer_msg(_T("View5000"),_T("Module888"),_T("View5000"),2,traktRec.getTraktID(),updateCoords));
			}
		}	// if (pView1 != NULL)
	}	// if (pTabView != NULL)
}

void CMDIStandEntryFormFrame::OnToolsMatchRandTrees()
{

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			pView->doMatchRandTrees();
		}
	}
}

void CMDIStandEntryFormFrame::OnToolsHandleRandTrees()
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			pView->doUpdateRandTrees();
		}
	}
}

void CMDIStandEntryFormFrame::OnToolsHandleRandTrees2()
{
	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView = pTabView->getPageThreeFormView();
		if (pView != NULL)
		{
			pView->doUpdateRandTrees2();
		}
	}
}

//20121206 J� #3509
void CMDIStandEntryFormFrame::OnSpecSet(UINT nId)
{
	int nSpecId=0;
	CTransaction_trakt_data recTraktData;
	nSpecId=nId-ID_SPEC_SETTINGS_START;
	if(nSpecId>0)	//Ladda inst�llningsf�nstret f�r tr�dlsagsinst�llningar f�r detta tr�dslag
	{
		for(UINT i=0;i<m_vecTraktData.size();i++)
		{
			if(m_vecTraktData[i].getSpecieID()==nSpecId)
			{
				recTraktData = m_vecTraktData[i];
				CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
				if(pTabView != NULL)
				{
					CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
					if (pView2 != NULL)
					{
						pView2->SetTraktDataActive(recTraktData);
						pView2->SetTraktDataOnClick(recTraktData);
						pView2->updTraktData(recTraktData,1);
					}
				}
			}
		}
	}
}

//20121206 J� #3509
void CMDIStandEntryFormFrame::OnSpecAssort(UINT nId)
{
	int nSpecId=0;
	CTransaction_trakt_data recTraktData;
	nSpecId=nId-ID_SPEC_ASSORT_START;
	if(nSpecId>0)	//Ladda inst�llningsf�nstret f�r tr�dlsagsinst�llningar sortiment f�r detta tr�dslag
	{
		for(UINT i=0;i<m_vecTraktData.size();i++)
		{
			if(m_vecTraktData[i].getSpecieID()==nSpecId)
			{
				recTraktData = m_vecTraktData[i];
				CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
				if(pTabView != NULL)
				{
					CPageTwoFormView *pView2 = pTabView->getPageTwoFormView();
					if (pView2 != NULL)
					{
						pView2->updTraktData(recTraktData,2);
					}
				}
			}
		}
	}
}


void CMDIStandEntryFormFrame::OnToolsUpdatePricelist(void)
{
	// Get latest version of pricelist from prn_pricelist_table; 081217 p�d
	getPricelistsFromDB();
	// Save this pricelist to esti_trakt_pricelist_table; 081217 p�d
	if (saveMiscDataToDB_prl(NULL,FALSE))
	{
		// After pricelist have been updated, we'll do a recalculation; 081217 p�d
		runDoCalulationInPageThree();
	}
}

void CMDIStandEntryFormFrame::OnToolsUpdateCosttemplate(void)
{
	// Update Costtemplate; 081217 p�d
	if (saveMiscDataToDB_costtmpl(NULL))
	{
		// After "Kostnadsmall" have been updated, we'll do a recalculation; 081217 p�d
		runDoCalulationInPageThree();
	}
}

void CMDIStandEntryFormFrame::OnToolsCosttemplateInfo(void)
{
	// Setup a new record for misc data, and set TypeOf (Pricelist) equal to -1.
	// This will indicate that we'll look at the Cost-template instead of the Pricelist; 091021 p�d
	CTransaction_trakt_misc_data rec = CTransaction_trakt_misc_data(m_recTraktMiscData.getTSetTraktID(),
																																	_T(""),
																																	-1,
																																	_T(""),
																																	m_recTraktMiscData.getCostsName(),
																																	m_recTraktMiscData.getCostsTypeOf(),
																																	m_recTraktMiscData.getXMLCosts(),
																																	0.0,
																																	m_recTraktMiscData.getCreated());

	showFormView(IDD_FORMVIEW10,m_sLangFN,(LPARAM)&rec);
}

// PRIVATE
void CMDIStandEntryFormFrame::setLanguage(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndFieldChooserDlg.SetWindowText((xml->str(IDS_STRING147)));
			m_wndFieldChooserDlg10.SetWindowText((xml->str(IDS_STRING147)));
			m_wndFieldChooserDlg11.SetWindowText((xml->str(IDS_STRING147)));
			m_wndFieldChooserDlg12.SetWindowText((xml->str(IDS_STRING147)));
			m_wndFieldChooserDlg2.SetWindowText((xml->str(IDS_STRING147)));

			m_wndStatusBar.SetPaneText(0,(xml->str(IDS_STRING3080)));
			m_wndStatusBar.SetPaneText(1,(xml->str(IDS_STRING3082)));

			// Property grid categories; 070423 p�d
			m_sExchange = (xml->str(IDS_STRING260));
			m_sCalculateAs = (xml->str(IDS_STRING2600));
			m_sPricelist = (xml->str(IDS_STRING2601));
			m_sMiscSettings = (xml->str(IDS_STRING259));
			m_sDiamClass = (xml->str(IDS_STRING2594));
			m_sCostTmpl = (xml->str(IDS_STRING2595));
			m_sSoderbergsSettings = (xml->str(IDS_STRING293));
			m_sAtCoast = (xml->str(IDS_STRING2930));
			m_sSouthEast = (xml->str(IDS_STRING2931));
			m_sRegion5 = (xml->str(IDS_STRING2932));
			m_sPartOfPlot = (xml->str(IDS_STRING2933));
			m_sSIH100_Pine = (xml->str(IDS_STRING2934));

			m_sYes = (xml->str(IDS_STRING3100));
			m_sNo = (xml->str(IDS_STRING3101));

			m_sNoStandsMsg1 = (xml->str(IDS_STRING390));
			m_sNoStandsMsg2 = (xml->str(IDS_STRING391));
			m_sMessageCap = 	(xml->str(IDS_STRING289));
			
			m_sUpdatePricelist.Format(_T("%s\n\n%s\n%s"),
				(xml->str(IDS_STRING3300)),
				(xml->str(IDS_STRING3301)),
				(xml->str(IDS_STRING3302)));
			m_sPricelistMissing.Format(_T("%s\n%s\n\n%s"),
				(xml->str(IDS_STRING3306)),
				(xml->str(IDS_STRING3307)),
				(xml->str(IDS_STRING3308)));
			m_sPricelistPulpError.Format(L"%s\n%s\n%s\n",
				(xml->str(IDS_STRING3315)),
				(xml->str(IDS_STRING3316)),
				(xml->str(IDS_STRING3317)));
			m_sDiameterclassErr = (xml->str(IDS_STRING3095));
			m_sPricelistLessSpec.Format(L"%s\n%s\n\n%s\n",
				(xml->str(IDS_STRING3318)),
				(xml->str(IDS_STRING3319)),
				(xml->str(IDS_STRING33190)));
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))

}


// CMDIStandEntryFormFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CPropertySelectListFrame

IMPLEMENT_DYNCREATE(CPropertySelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPropertySelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPropertySelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPropertySelectListFrame::CPropertySelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CPropertySelectListFrame::~CPropertySelectListFrame()
{
}

void CPropertySelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CPropertySelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	setLanguage();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPropertySelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPropertySelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CPropertySelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertySelectListFrame diagnostics

#ifdef _DEBUG
void CPropertySelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPropertySelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPropertySelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPropertySelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPropertySelectListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CPropertySelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

LRESULT CPropertySelectListFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
//	UMMessageBox("CPropertySelectListFrame::OnSuiteMessage");
	return 0L;
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPropertySelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
//	UMMessageBox("CPropertySelectListFrame::OnMessageFromShell");
	return 0L;
}

// MY METHODS

void  CPropertySelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

/////////////////////////////////////////////////////////////////////////////
// CPricelistSelectListFrame

IMPLEMENT_DYNCREATE(CPricelistSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPricelistSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPricelistSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPricelistSelectListFrame::CPricelistSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CPricelistSelectListFrame::~CPricelistSelectListFrame()
{
}

void CPricelistSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PRICELIST_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CPricelistSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	setLanguage();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPricelistSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PRICELIST_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPricelistSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CPricelistSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPricelistSelectListFrame diagnostics

#ifdef _DEBUG
void CPricelistSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPricelistSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPricelistSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELIST_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELIST_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPricelistSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPricelistSelectListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CPricelistSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPricelistSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// MY METHODS

void  CPricelistSelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

/////////////////////////////////////////////////////////////////////////////
// CMDISpecieDataFrame
IMPLEMENT_DYNCREATE(CMDISpecieDataFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDISpecieDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDISpecieDataFrame)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CMDISpecieDataFrame construction/destruction

XTPDockingPanePaintTheme CMDISpecieDataFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDISpecieDataFrame::CMDISpecieDataFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
}

CMDISpecieDataFrame::~CMDISpecieDataFrame()
{
}

void CMDISpecieDataFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_SPECIE_DATA_KEY);
	SavePlacement(this, csBuf);

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((REG_WP_SPECIE_DATA_LAYOUT_KEY));

	m_bFirstOpen = TRUE;

	m_vecHgtFunc.clear();
	m_vecHgtFuncList.clear();
	m_vecVolFunc.clear();
	m_vecVolFuncList.clear();
	m_vecBarkFunc.clear();
	m_vecBarkFuncList.clear();
	m_vecVolUBFunc.clear();
	m_vecVolUBFuncList.clear();
	m_vecTransactionTraktSetSpc.clear();
	CMDIChildWnd::OnDestroy();

}

BOOL CMDISpecieDataFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	cs.style &= ~(ES_AUTOHSCROLL|WS_HSCROLL);	// Enable word-wrapping

	return TRUE;
}

void CMDISpecieDataFrame::OnClose(void)
{
/*
	CSpecieDataFormView *pView = (CSpecieDataFormView *)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		pView->exitViewAction();
	}
*/
	CMDIChildWnd::OnClose();
}


int CMDISpecieDataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Send message to HMSShell, please send back the DB connection; 070503 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// control bars objects have been created and docked.
	m_paneManager.InstallDockingPanes(this);

	EnableDocking(CBRS_ALIGN_ANY);

	// Create docking panes.
	CXTPDockingPane *paneProp = m_paneManager.CreatePane(IDR_PROPSPANE2, CRect(0, 0,210, 120), xtpPaneDockLeft);
	paneProp->SetOptions( xtpPaneNoCloseable );
	m_paneManager.SetTheme(m_themeCurrent);

	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((REG_WP_SPECIE_DATA_LAYOUT_KEY)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	setLanguage();

	// Get information of selected data (e.g. Volume-,Height-,Bark-functoions etc); 070504 p�d
	getTraktSetSpc();

	m_bFirstOpen = TRUE;

	m_bIsDirty = FALSE;

	ModifyStyleEx(0, WS_EX_APPWINDOW);

	return 0; // creation ok
}
/*
void CMDISpecieDataFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}
*/
LRESULT CMDISpecieDataFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{

			if (pPane->GetID() == IDR_PROPSPANE2)
			{
				if (!m_wndPropertyGrid.m_hWnd)
				{
					m_wndPropertyGrid.Create(CRect(0, 0, 0, 0), this, ID_SPECIE_PROPGRID);
					m_wndPropertyGrid.SetOwner(this);
					m_wndPropertyGrid.ShowHelp( FALSE );
					m_wndPropertyGrid.SetViewDivider(0.4);
					m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);

					pPane->Attach(&m_wndPropertyGrid);
				}
				if (m_wndPropertyGrid.m_hWnd)
				{
					setupFunctionData();
				}
			} // if (pPane->GetID() == IDR_PROPSPANE2)

		} // if (!pPane->IsValid())

		return TRUE; // handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)
	if (wParam == XTP_DPN_ACTION)
	{
		XTP_DOCKINGPANE_ACTION* pAction = (XTP_DOCKINGPANE_ACTION*)lParam;
	
		if (pAction->pPane->GetID() == IDR_PROPSPANE2)
		{
			CString sPaneTitle;	
			CSpecieDataFormView *pView = (CSpecieDataFormView *)getFormViewByID(IDD_FORMVIEW8);
			if (pView != NULL)
			{
				sPaneTitle.Format(_T("%s - %s"),
					m_sPropspane2Cap,
					pView->getSpcName());
					pAction->pPane->SetTitle(sPaneTitle);
			}
		}
		return TRUE; // handled
	}
	return FALSE;
}

LRESULT CMDISpecieDataFrame::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	CString S;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
		if (pItem->GetID() == ID_HGT_TYPEOF_FUNC_CB)
		{
			setupHeightfunctionsInPropertyGrid(pItem);
		}
		if (pItem->GetID() == ID_HGT_FUNC_CB)
		{
			saveSetSpcHgtFuncToDB(pItem);
			m_bIsDirty = TRUE;
		}

		if (pItem->GetID() == ID_VOL_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsInPropertyGrid(pItem);
		}
		if (pItem->GetID() == ID_VOL_FUNC_CB)
		{
			saveSetSpcVolFuncToDB(pItem);
			m_bIsDirty = TRUE;
		}

		if (pItem->GetID() == ID_BARK_TYPEOF_FUNC_CB)
		{
			setupBarkseriesInPropertyGrid(pItem);
			m_bIsDirty = TRUE;
		}
		
		if (pItem->GetID() == ID_BARK_FUNC_CB)
		{
			saveSetSpcBarkFuncToDB(pItem);
			m_bIsDirty = TRUE;
		}

		if (pItem->GetID() == ID_VOL_UB_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsUBInPropertyGrid(pItem);
			m_bIsDirty = TRUE;
		}
		if (pItem->GetID() == ID_VOL_UB_FUNC_CB)
		{
			saveSetSpcVolUBFuncToDB(pItem);
			m_bIsDirty = TRUE;
		}
		if (pItem->GetID() == ID_M3SK_TO_M3UB)
		{
			saveSetSpcVolUBFuncToDB_sk_ub(pItem);
			m_bIsDirty = TRUE;
		}

		if (pItem->GetID() == ID_M3SK_TO_M3FUB ||
			  pItem->GetID() == ID_M3FUB_TO_M3TO ||
			  pItem->GetID() == ID_TRANSP_DIST1 ||
			  pItem->GetID() == ID_TRANSP_DIST2 ||
			  pItem->GetID() == ID_ATCOAST ||
			  pItem->GetID() == ID_SOUTHEAST ||
			  pItem->GetID() == ID_REGION5 ||
			  pItem->GetID() == ID_PART_OF_PLOT ||
			  pItem->GetID() == ID_SI_H100_PINE ||
				pItem->GetID() == ID_QUALDESC)
		{
			saveSetSpcMiscDataToDB(pItem);
			m_bIsDirty = TRUE;
		}
	}
	if (wParam == XTP_PGN_RCLICK)
	{
			// Show a Popupmenu on rightclick; 070502 p�d
			CPoint curPos;
			CMenu menu;
			CString sMenu1;
			if (fileExists(m_sLangFN))
			{
				RLFReader *xml = new RLFReader;
				if (xml->Load(m_sLangFN))
				{
					sMenu1 = (xml->str(IDS_STRING261));
				}	// if (xml->Load(m_sLangFN))
				delete xml;
			}
			VERIFY(menu.CreatePopupMenu());
			GetCursorPos(&curPos);
			// create main menu items
			menu.AppendMenu(MF_STRING, ID_POPUPMENU_EXPAND_COLLAPSE, (sMenu1));
			// track menu
			int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
									curPos.x, curPos.y,this, NULL);
			// other general items
			switch (nMenuResult)
			{
				case ID_POPUPMENU_EXPAND_COLLAPSE :
				{
					if (pItem->IsExpanded())
						pItem->Collapse();
					else
						pItem->Expand();
					break;
				}
			}	// switch (nMenuResult)
	}
	return FALSE;
}

void CMDISpecieDataFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_SPECIE_DATA_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDISpecieDataFrame::OnSetFocus(CWnd* pWnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISpecieDataFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

BOOL CMDISpecieDataFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

// CMDISpecieDataFrame diagnostics

#ifdef _DEBUG
void CMDISpecieDataFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDISpecieDataFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDISpecieDataFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SPECIE_DATA;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SPECIE_DATA;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

// PRIVATE

void CMDISpecieDataFrame::setupFunctionData(BOOL setup_as_new)
{
		CXTPPropertyGridItem* pSettings  = NULL;
		//===============================================================================
		// CATEGORY; Functions
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sFunctions));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sFunctions);

		if (pSettings != NULL)
		{
			// Item:Heights
			setupTypeOfHeightfunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item:Volumes m3sk
			setupTypeofVolumefunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item: Bark
			setupTypeOfBarkfunctionsInPropertyGrid(pSettings,setup_as_new);

			// Item:Volumes m3ub
			setupTypeofVolumefunctionsUBInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "Kvalitetsbeskrivning"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sQualDesc));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sQualDesc);

		if (pSettings != NULL)
		{
			// Item: "Kvalitetsbeskrivning(ar)"
			setupQualityDescInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "�vriga inst�llningar"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sMiscSettings));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sMiscSettings);

		if (pSettings != NULL)
		{
			// Item: "Omf�ringstal"
			setupMiscSettingsValuesInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}

		//===============================================================================
		// CATEGORY; "�vriga inst�llningar"
		if (setup_as_new)
			pSettings  = m_wndPropertyGrid.AddCategory((m_sTransport));
		else
			pSettings = m_wndPropertyGrid.FindItem(m_sTransport);

		if (pSettings != NULL)
		{
			setupMiscSettings2ValuesInPropertyGrid(pSettings,setup_as_new);

			pSettings->Expand();
		}
}

void CMDISpecieDataFrame::setLanguage(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{

			m_sPropspane2Cap = (xml->str(IDS_STRING118));
			m_sFunctions	= (xml->str(IDS_STRING258));
			m_sHeight	= (xml->str(IDS_STRING2580));
			m_sCalcHgtAs	= (xml->str(IDS_STRING2581));
			m_sVolume = (xml->str(IDS_STRING2582));
			m_sCalcVolAs = (xml->str(IDS_STRING2583));
			m_sBark = (xml->str(IDS_STRING2584));
			m_sBarkSerie = (xml->str(IDS_STRING2585));
			m_sVolumeUB = (xml->str(IDS_STRING2586));
			m_sCalcVolAsUB = (xml->str(IDS_STRING2587));
			m_sMiscSettings = (xml->str(IDS_STRING259));
			m_sSk_Ub = (xml->str(IDS_STRING2596));
			m_sSk_Fub = (xml->str(IDS_STRING2590));
			m_sFub_To = (xml->str(IDS_STRING2591));
			m_sQualDesc = (xml->str(IDS_STRING292));
			m_sTransport = (xml->str(IDS_STRING2597));
			m_sTranspDist1 = (xml->str(IDS_STRING2598));
			m_sTranspDist2 = (xml->str(IDS_STRING2599));
			m_sCalcQDescAs = (xml->str(IDS_STRING2920));

			m_sCapMsg = xml->str(IDS_STRING289);
			m_sarrValidateMsg.Add(xml->str(IDS_STRING2410));
			m_sarrValidateMsg.Add(xml->str(IDS_STRING2411));
			m_sarrValidateMsg.Add(xml->str(IDS_STRING2412));
			m_sarrValidateMsg.Add(xml->str(IDS_STRING2413));
			/////////////////////////////////////////////////////////////////////////////////////
			// Set caption for Properties pane; 060207 p�d
			CXTPDockingPane* pPaneProp = GetDockingPaneManager()->FindPane(IDR_PROPSPANE2);
			ASSERT(pPaneProp);
			if (pPaneProp) 
			{
				pPaneProp->SetTitle(m_sPropspane2Cap);
			}
			
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))

}
void CMDISpecieDataFrame::setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	CString sInfo;
	UINT nIndex;
	int nActiveFuncIndex;
	// get volume functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getHeightFunctions(m_vecHgtFunc,m_vecHgtFuncList);
	if (m_vecHgtFunc.size() > 0)
	{
		CXTPPropertyGridItem *pHgtItem = NULL;
		if (setup_as_new)
			pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sHeight)));
		else
			pHgtItem = pItem->GetChilds()->FindItem(m_sHeight);
		if (pHgtItem != NULL)
		{
			pHgtItem->BindToString(&m_sHgtTypeOfBindStr);

			for (UINT j = 0;j < m_vecHgtFunc.size();j++)
			{
				UCFunctions flist = m_vecHgtFunc[j];
				sInfo.Format(_T("%s"),flist.getName());
				pHgtItem->GetConstraints()->AddConstraint((sInfo));
			}	// for (UINT j = 0;j < func_list.size();j++)
			pHgtItem->SetFlags(xtpGridItemHasComboButton);
			pHgtItem->SetConstraintEdit( FALSE );
			pHgtItem->SetID(ID_HGT_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

			if (getActiveTypeOfFunction(ID_HGT_TYPEOF_FUNC_CB,m_sHgtTypeOfBindStr,&nIndex,&nActiveFuncIndex))
			{
				pHgtItem->GetConstraints()->SetCurrent(nIndex);
				setupHeightfunctionsInPropertyGrid(pHgtItem);
			}	// if (getActiveTypeOfFunction(ID_HGT_TYPEOF_FUNC_CB,m_sHgtTypeOfBindStr,&nIndex))
		}	// if (pHgtItem != NULL)
	}	// if (m_vecHgtFunc.size() > 0)

}

// This function is called from OnDockingPaneNotify
void CMDISpecieDataFrame::setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		CString sInfo;
		UINT nIndex;
		int nActiveFuncIndex;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions(m_vecVolFunc,m_vecVolFuncList);
		if (m_vecVolFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolItem = NULL;
			if (setup_as_new)
				pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sVolume)));
			else
				pVolItem = pItem->GetChilds()->FindItem(m_sVolume);

			if (pVolItem != NULL)
			{
				pVolItem->BindToString(&m_sVolTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolFunc.size();j++)
				{
					UCFunctions flist = m_vecVolFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolItem->SetFlags(xtpGridItemHasComboButton);
				pVolItem->SetConstraintEdit( FALSE );
				pVolItem->SetID(ID_VOL_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&nIndex,&nActiveFuncIndex))
				{
					pVolItem->GetConstraints()->SetCurrent(nIndex);
					setupVolumefunctionsInPropertyGrid(pVolItem);
				}	// if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&nIndex))
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify
void CMDISpecieDataFrame::setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		UINT nIndex;
		int nActiveFuncIndex;
		CString sInfo;
		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getBarkFunctions(m_vecBarkFunc,m_vecBarkFuncList);
		if (m_vecBarkFunc.size() > 0)
		{
			CXTPPropertyGridItem *pBarkFuncItem = NULL;
			if (setup_as_new)
				pBarkFuncItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sBark)));
			else
				pBarkFuncItem = pItem->GetChilds()->FindItem(m_sBark);
			if (pBarkFuncItem != NULL)
			{
				pBarkFuncItem->BindToString(&m_sBarkTypeOfBindStr);

				for (UINT j = 0;j < m_vecBarkFunc.size();j++)
				{
					UCFunctions flist = m_vecBarkFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pBarkFuncItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pBarkFuncItem->SetFlags(xtpGridItemHasComboButton);
				pBarkFuncItem->SetConstraintEdit( FALSE );
				pBarkFuncItem->SetID(ID_BARK_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_BARK_TYPEOF_FUNC_CB,m_sBarkTypeOfBindStr,&nIndex,&nActiveFuncIndex))
				{
					pBarkFuncItem->GetConstraints()->SetCurrent(nIndex);
					setupBarkseriesInPropertyGrid(pBarkFuncItem);
				}	// if (getActiveTypeOfFunction(ID_BARK_TYPEOF_FUNC_CB,m_sBarkTypeOfBindStr,&nIndex))
			}	// if (pBarkFuncItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify
void CMDISpecieDataFrame::setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
		CString sInfo;
		UINT nIndex;
		int nActiveFuncIndex;
		UCFunctions flist;
		UCFunctions flist_active_func;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions_ub(m_vecVolUBFunc,m_vecVolUBFuncList);
		if (m_vecVolUBFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolUBItem = NULL;
			if (setup_as_new)
				pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sVolumeUB)));
			else
				pVolUBItem = pItem->GetChilds()->FindItem(m_sVolumeUB);
			if (pVolUBItem != NULL)
			{
				pVolUBItem->BindToString(&m_sVolUBTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolUBFunc.size();j++)
				{
					flist = m_vecVolUBFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolUBItem->GetConstraints()->AddConstraint((sInfo));
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolUBItem->SetFlags(xtpGridItemHasComboButton);
				pVolUBItem->SetConstraintEdit( FALSE );
				pVolUBItem->SetID(ID_VOL_UB_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d

				if (getActiveTypeOfFunction(ID_VOL_UB_TYPEOF_FUNC_CB,m_sVolUBTypeOfBindStr,&nIndex,&nActiveFuncIndex))
				{
					pVolUBItem->GetConstraints()->SetCurrent(nIndex);
					setupVolumefunctionsUBInPropertyGrid(pVolUBItem);
				}	// if (getActiveTypeOfFunction(ID_VOL_TYPEOF_FUNC_CB,m_sVolTypeOfBindStr,&nIndex))
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify

void CMDISpecieDataFrame::setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	int nQDescIndex = -1;
	getQualDescData(&nQDescIndex,m_listQDesc);
	if (m_listQDesc.GetCount() > 0)
	{
		CXTPPropertyGridItem *pQDescItem = NULL;
		if (setup_as_new)
			pQDescItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcQDescAs)));
		else
			pQDescItem = pItem->GetChilds()->FindItem(m_sCalcQDescAs);
		if (pQDescItem != NULL)
		{
			pQDescItem->BindToString(&m_sQDescBindStr);

			for (int i = 0;i < m_listQDesc.GetCount();i++)
			{
					pQDescItem->GetConstraints()->AddConstraint((m_listQDesc[i]),i);
			}	// for (UINT j = 0;j < func_list.size();j++)
			pQDescItem->SetFlags(xtpGridItemHasComboButton);
			pQDescItem->SetConstraintEdit( FALSE );
			pQDescItem->SetID(ID_QUALDESC);	// Set ID in PropertyGrid; 070521 p�d
		}
		// Make sure we're inside the array; 070521 p�d
		if (nQDescIndex >= 0 && nQDescIndex < m_listQDesc.GetCount())
		{
			m_sQDescBindStr = m_listQDesc[nQDescIndex];
		}
	}
}

// "Omf�ringstal"
void CMDISpecieDataFrame::setupMiscSettingsValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	if (setup_as_new)
	{
/*	Commented out 2009-06-02 P�D
		NOT USED, "Omf�ringstal f�r m3sk till m3fub", use "gagnvrkesvolym m3fub"; 090602 p�d
		CXTPPropertyGridItemDouble *pM3Sk_M3Fub = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sSk_Fub),0.0,_T("%.2f")));
		pM3Sk_M3Fub->SetID(ID_M3SK_TO_M3FUB);
		pM3Sk_M3Fub->BindToDouble(&m_fM3SkToM3Fub);
*/
		m_fM3SkToM3Fub = 0.0;
		CXTPPropertyGridItemDouble *pM3Fub_M3To = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sFub_To),0.0,_T("%.2f")));
		pM3Fub_M3To->SetID(ID_M3FUB_TO_M3TO);
		pM3Fub_M3To->BindToDouble(&m_fM3FubToM3To);
	}

	getMiscData(2);
}

void CMDISpecieDataFrame::setupMiscSettings2ValuesInPropertyGrid(CXTPPropertyGridItem *pItem,BOOL setup_as_new)
{
	if (setup_as_new)
	{
		CXTPPropertyGridItemNumber *pTranspDist1 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTranspDist1)));
		pTranspDist1->SetID(ID_TRANSP_DIST1);
		pTranspDist1->BindToNumber(&m_nTranspDist1);

		CXTPPropertyGridItemNumber *pTranspDist2 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTranspDist2)));
		pTranspDist2->SetID(ID_TRANSP_DIST2);
		pTranspDist2->BindToNumber(&m_nTranspDist2);
	}

	getMiscData(2);
}

// This function is called from OnGridNotify
void CMDISpecieDataFrame::setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pHgtItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcHgtAs)));
		pHgtItem->BindToString(&m_sHgtBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pHgtItem = pGridChilds->GetAt(0);
			m_sHgtBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pHgtItem != NULL)
	{
		pItem->Expand();
		pHgtItem->GetConstraints()->RemoveAll();
		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecHgtFunc.size())
		{
			nFunctionIdentifer = m_vecHgtFunc[nIndex].getIdentifer();
		}

		if (m_vecHgtFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecHgtFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecHgtFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
					sInfo.Format(_T("%s , %s"),list.getSpcName(),list.getFuncArea());
/*
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
*/
					pHgtItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pHgtItem->SetFlags(xtpGridItemHasComboButton);
		pHgtItem->SetConstraintEdit( FALSE );
		pHgtItem->SetID(ID_HGT_FUNC_CB);
		m_sHgtBindStr = getActiveFunction(ID_HGT_FUNC_CB);

	} // 	if (pHgtItem != NULL)

}

// This function is called from OnGridNotify
void CMDISpecieDataFrame::setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pVolItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcVolAs)));
		pVolItem->BindToString(&m_sVolBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pVolItem = pGridChilds->GetAt(0);
			m_sVolBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pVolItem != NULL)
	{
		pItem->Expand();
		pVolItem->GetConstraints()->RemoveAll();
		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecVolFunc.size())
		{
			nFunctionIdentifer = m_vecVolFunc[nIndex].getIdentifer();
		}

		if (m_vecVolFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecVolFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
					pVolItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pVolItem->SetFlags(xtpGridItemHasComboButton);
		pVolItem->SetConstraintEdit( FALSE );
		pVolItem->SetID(ID_VOL_FUNC_CB);
		m_sVolBindStr = getActiveFunction(ID_VOL_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

// This function is called from OnGridNotify
void CMDISpecieDataFrame::setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pBarkItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pBarkItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sBarkSerie)));
		pBarkItem->BindToString(&m_sBarkBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pBarkItem = pGridChilds->GetAt(0);
			m_sBarkBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pBarkItem != NULL)
	{
		pItem->Expand();
		pBarkItem->GetConstraints()->RemoveAll();

		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecBarkFunc.size() )
		{
			nFunctionIdentifer = m_vecBarkFunc[nIndex].getIdentifer();
		}
		if (m_vecBarkFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecBarkFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecBarkFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == nFunctionIdentifer)
				{
/*
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
*/
					sInfo.Format(_T("%s, %s"),
											list.getSpcName(),
											list.getFuncArea());

					pBarkItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pBarkItem->SetFlags(xtpGridItemHasComboButton);
		pBarkItem->SetConstraintEdit( FALSE );
		pBarkItem->SetID(ID_BARK_FUNC_CB);
		m_sBarkBindStr = getActiveFunction(ID_BARK_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

// This function is called from OnGridNotify
void CMDISpecieDataFrame::setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	BOOL bFuncChanged = FALSE;
	CString sInfo,sTmp;
	CString sActiveFuncName;
	UINT nActiveIndex;
	int nActiveIdentifer;
	UINT nIndex;
	CXTPPropertyGridItem *pVolUBItem = NULL;
	CMyPropGridItemDouble_validate *pM3Sk_M3Fub = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	if (pItem == NULL) return;

	if (!pItem->HasChilds())
	{
			pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcVolAsUB)));
			pVolUBItem->SetID(ID_VOL_UB_FUNC_CB);
			pVolUBItem->BindToString(&m_sVolUBBindStr);
			pVolUBItem->SetFlags(xtpGridItemHasComboButton);
			pVolUBItem->SetConstraintEdit( FALSE );
			
			pM3Sk_M3Fub = (CMyPropGridItemDouble_validate*)pItem->AddChildItem(new CMyPropGridItemDouble_validate((m_sSk_Ub),_T("%.2f"),m_sCapMsg,m_sarrValidateMsg,1.0,VALUE_LE));
			pM3Sk_M3Fub->SetID(ID_M3SK_TO_M3UB);
			pM3Sk_M3Fub->BindToDouble(&m_fM3SkToM3Ub);
	}

	// Get Volume function set; 081204 p�d
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL) nIndex = pItemConstraints->GetCurrent();

	// Get identifer of Function selected; 070416 p�d
	if (nIndex >= 0 && nIndex < m_vecVolUBFunc.size())
	{
		nFunctionIdentifer = m_vecVolUBFunc[nIndex].getIdentifer();

		getActiveTypeOfFunction(ID_VOL_UB_TYPEOF_FUNC_CB,sActiveFuncName,&nActiveIndex,&nActiveIdentifer);
		bFuncChanged = (nActiveIdentifer != nFunctionIdentifer);

		CXTPPropertyGridItems *pChilds = pItem->GetChilds();
		if (pChilds != NULL)
		{
			if (nFunctionIdentifer == ID_BRANDELS_UB || 
				  nFunctionIdentifer == ID_NASLUNDS_UB ||
				  nFunctionIdentifer == ID_HAGBERG_MATERN_UB)
			{
				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL)
				{
					pVolFuncUBFound->GetConstraints()->RemoveAll();
					if (m_vecVolUBFuncList.size() > 0)
					{
						for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
						{
							sInfo.Empty();
							UCFunctionList list = m_vecVolUBFuncList[i];
							// Check the Function identifer; 070416 p�d
							if (list.getID() == nFunctionIdentifer)
							{
								if (_tcslen(list.getSpcName()) > 0)
								{
									sTmp.Format(_T("%s"),list.getSpcName());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncType()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncType());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncArea()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncArea());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncDesc()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncDesc());					
									sInfo += sTmp;
								}
								pVolFuncUBFound->GetConstraints()->AddConstraint((sInfo),i);			
							}	// if (list.getID() == nFunctionIdentifer)
						}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
					}	// if (m_vecVolFuncList.size() > 0)
					if (bFuncChanged)
					{
						m_sVolUBBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
						pItem->GetGrid()->Refresh();
					}
					else
						m_sVolUBBindStr = getActiveFunction(ID_VOL_UB_FUNC_CB);

					pVolFuncUBFound->SetHidden(FALSE);
				}	// if (pVolFuncUBFound != NULL)

				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(TRUE);
			}
			if (nFunctionIdentifer == ID_OMF_FACTOR_UB)
			{
				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(FALSE);

				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL) pVolFuncUBFound->SetHidden(TRUE);
			}
		}

		pItem->Expand();
		getMiscData(1);
	}
}

// Database handling; 070504 p�d
void CMDISpecieDataFrame::saveSetSpcHgtFuncToDB(CXTPPropertyGridItem *pItem)
{
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecHgtFuncList.size() > 0)
	{
		CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	

		if (pConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
			if (pConstraint != NULL)
			{
				UINT nIndex = (UINT)pConstraint->m_dwData;
				if (nIndex >= 0 && nIndex < m_vecHgtFuncList.size())
				{			
					// Get active trakt record; holds information on trakt-id; 070430 p�d
					CTransaction_trakt_data *recTData = getActiveTraktData();

					UCFunctionList funcList = m_vecHgtFuncList[nIndex];
					// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
					if (m_bConnected)
					{
						CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
						if (pDB != NULL && recTData != NULL)
						{

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcHgtFuncToDB 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		recTData->getTDataID(),
																																		recTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		recTData->getSpecieID(),
																																		0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			setupFunctionData(FALSE);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			return;
		}
	}
#endif

							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																					recTData->getTDataID(),
																																					recTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					recTData->getSpecieID(),
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
							if (!pDB->addTraktSetSpc_hgtfunc(rec))
							{
								pDB->updTraktSetSpc_hgtfunc(rec);
							}
							delete pDB;
						}
					}
				}	// if (nIndex > -1 && nIndex < m_vecTransactionPricelist.size())
			}	// if (pConstraint != NULL)
		}	// if (pConstraint != NULL)
	}	// if (m_vecHgtFunc.size() > 0 && m_vecHgtFuncList.size() > 0)
}

void CMDISpecieDataFrame::saveSetSpcVolFuncToDB(CXTPPropertyGridItem *pItem)
{
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecVolFuncList.size() > 0)
	{
		CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	
		if (pConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
			if (pConstraint != NULL)
			{

				UINT nIndex = (UINT)pConstraint->m_dwData;
				if (nIndex >= 0 && nIndex < m_vecVolFuncList.size())
				{			
					// Get active trakt record; holds information on trakt-id; 070430 p�d
					CTransaction_trakt_data *recTData = getActiveTraktData();

					UCFunctionList funcList = m_vecVolFuncList[nIndex];
					// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
					if (m_bConnected)
					{
						CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
						if (pDB != NULL && recTData != NULL)
						{
//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcVolFuncToDB 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc	tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		 recTData->getTDataID(),
																																		 recTData->getTDataTraktID(),
																																		 STMP_LEN_WITHDRAW,
																																		 recTData->getSpecieID(),
																																		 0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			setupFunctionData(FALSE);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			return;
		}
	}
#endif

							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																					recTData->getTDataID(),
																																					recTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					recTData->getSpecieID(),
																																					0,0,0,
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
							if (!pDB->addTraktSetSpc_volfunc(rec))
							{
								pDB->updTraktSetSpc_volfunc(rec);
							}
							delete pDB;
						}
					} // if (pConstraint != NULL)
				}	// if (nIndex > -1 && nIndex < m_vecVolFuncList.size())
			}	// if (pConstraint != NULL)
		}	// if (pConstraints != NULL)
	}	// if (m_vecVolFuncList.size() > 0)
}

void CMDISpecieDataFrame::saveSetSpcBarkFuncToDB(CXTPPropertyGridItem *pItem)
{
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecBarkFuncList.size() > 0)
	{
		CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	
		if (pConstraints != NULL)
		{

			CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
			if (pConstraint != NULL)
			{
				UINT nIndex = (UINT)pConstraint->m_dwData;
				if (nIndex >= 0 && nIndex < m_vecBarkFuncList.size())
				{			
					// Get active trakt record; holds information on trakt-id; 070430 p�d
					CTransaction_trakt_data *recTData = getActiveTraktData();

					UCFunctionList funcList = m_vecBarkFuncList[nIndex];
					// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
					if (m_bConnected)
					{
						CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
						if (pDB != NULL && recTData != NULL)
						{
//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcBarkFuncToDB 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc	tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		 recTData->getTDataID(),
																																		 recTData->getTDataTraktID(),
																																		 STMP_LEN_WITHDRAW,
																																		 recTData->getSpecieID(),
																																		 0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			setupFunctionData(FALSE);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			return;
		}
	}
#endif

							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																					recTData->getTDataID(),
																																					recTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					recTData->getSpecieID(),
																																					0,0,0,0,0,0,
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0,0,0,0.0,
																																					0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
							if (!pDB->addTraktSetSpc_barkfunc(rec))
							{
								pDB->updTraktSetSpc_barkfunc(rec);
							}
							delete pDB;
						}
					}
				}	// if (nIndex > -1 && nIndex < m_vecTransactionPricelist.size())
			}	// if (pConstraint != NULL)
		}	// if (pConstraints != NULL)
	}	// if (m_vecHgtFunc.size() > 0 && m_vecHgtFuncList.size() > 0)
}

void CMDISpecieDataFrame::saveSetSpcVolUBFuncToDB(CXTPPropertyGridItem *pItem)
{
	CString S;
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecVolUBFuncList.size() > 0)
	{
		CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	
		if (pConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
			if (pConstraint != NULL)
			{

				UINT nIndex = (UINT)pConstraint->m_dwData;
				if (nIndex >= 0 && nIndex < m_vecVolUBFuncList.size())
				{			
					// Get active trakt record; holds information on trakt-id; 070430 p�d
					CTransaction_trakt_data *recTData = getActiveTraktData();

					UCFunctionList funcList = m_vecVolUBFuncList[nIndex];
					S.Format(_T("CMDISpecieDataFrame::saveSetSpcVolUBFuncToDB\nnIndex %d"),nIndex);
					UMMessageBox(S);
					// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
					if (m_bConnected)
					{
						CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
						if (pDB != NULL && recTData != NULL)
						{
//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcVolUBFuncToDB 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc	tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		 recTData->getTDataID(),
																																		 recTData->getTDataTraktID(),
																																		 STMP_LEN_WITHDRAW,
																																		 recTData->getSpecieID(),
																																		 0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			setupFunctionData(FALSE);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			return;
		}
	}
#endif

							// Setup record to hold save information for height function; 070503 p�d
							CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																					recTData->getTDataID(),
																																					recTData->getTDataTraktID(),
																																					STMP_LEN_WITHDRAW,
																																					recTData->getSpecieID(),
																																					0,0,0,0,0,0,0,0,0,
																																					funcList.getID(),
																																					funcList.getSpcID(),
																																					funcList.getIndex(),
																																					0.0,0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
							if (!pDB->addTraktSetSpc_volfunc_ub(rec))
							{
								pDB->updTraktSetSpc_volfunc_ub(rec);
							}
							delete pDB;
						}
					} // if (pConstraint != NULL)
				}	// if (nIndex > -1 && nIndex < m_vecVolFuncList.size())
			}	// if (pConstraint != NULL)
		}	// if (pConstraints != NULL)
	}	// if (m_vecVolFuncList.size() > 0)
}

void CMDISpecieDataFrame::saveSetSpcVolUBFuncToDB_sk_ub(CXTPPropertyGridItem *pItem)
{
	BOOL bFound = FALSE;
	UCFunctionList funcList;
	// Check, to make sure we have something to work with; 070503 p�d
	if (m_vecVolUBFuncList.size() > 0)
	{
		// Try to find idenifer for function "Omf. tal" sk to ub; 071022 p�d
		for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
		{
			funcList = m_vecVolUBFuncList[i];	
			if (funcList.getID() == ID_OMF_FACTOR_UB)
			{
				bFound = TRUE;
				break;
			}	// if (funcList.getID() == ID_OMF_FACTOR_UB)
		}	// for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
		if (bFound)
		{
			// Get active trakt record; holds information on trakt-id; 070430 p�d
			CTransaction_trakt_data *recTData = getActiveTraktData();

			// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
			if (m_bConnected)
			{
				CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
				if (pDB != NULL && recTData != NULL)
				{

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcVolUBFuncToDB_sk_ub 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc	tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		 recTData->getTDataID(),
																																		 recTData->getTDataTraktID(),
																																		 STMP_LEN_WITHDRAW,
																																		 recTData->getSpecieID(),
																																		 0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			setupFunctionData(FALSE);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			return;
		}
	}
#endif

					// Setup record to hold save information for height function; 070503 p�d
					CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																			recTData->getTDataID(),
																																			recTData->getTDataTraktID(),
																																			STMP_LEN_WITHDRAW,
																																			recTData->getSpecieID(),
																																			0,0,0,0,0,0,0,0,0,
																																			funcList.getID(),
																																			funcList.getSpcID(),
																																			funcList.getIndex(),
																																			m_fM3SkToM3Ub,
																																			0.0,0.0,0.0,0,_T(""),0,0,_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
					if (!pDB->addTraktSetSpc_volfunc_ub(rec))
					{
						pDB->updTraktSetSpc_volfunc_ub(rec);
					}
					delete pDB;
				}
			} // if (m_bConnected)
		}
	}	// if (m_vecVolFuncList.size() > 0)
}

void CMDISpecieDataFrame::saveSetSpcMiscDataToDB(CXTPPropertyGridItem *pItem)
{
	int nQDescIndex = -1;

	CString sExtraInfo;
	CString sQDescName;

	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *recTData = getActiveTraktData();

	// Retrive data from esti_trakt_misc_data_table.
	// Holds info. about e.g. diameterclass; 070507 p�d
	getTraktMiscDataFromDB();
	// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
	if (m_bConnected)
	{
		CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
		if (pDB != NULL && recTData != NULL)
		{

//************************************************************************************
//	DATASECURITY HANDLING; 090319 p�d
// Client/Server specifics; 090319 p�d
#ifdef __USE_DATA_SECURITY_METHODS
//UMMessageBox(_T("CMDISpecieDataFrame::saveSetSpcMiscDataToDB 1"));
	CString sDateDone,sMsg;
	if (recTData != NULL)
	{
		CTransaction_trakt_set_spc tsetspc = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		recTData->getTDataID(),
																																		recTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		recTData->getSpecieID(),
																																		0,0,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,0,_T(""),0,0,getSetSpcDate(),_T(""));


		if (pDB->checkTraktSetSpcDate(tsetspc,tsetspc.getTSetspcDataType(),sDateDone) == 0)
		{	
			SecurityMessage(this->GetSafeHwnd(),m_sLangFN,sDateDone);
			getTraktSetSpc();
			getMiscData(2);
			if (m_wndPropertyGrid.m_hWnd)
				m_wndPropertyGrid.Refresh();
			setupFunctionData(FALSE);
			return;
		}
	}
#endif

			// Save misc data, depending on selected item in Property; 070531
			// If ID_QUALDESC is selected, we'll also save diamterclass; 070531
			// Get Index of selected Qualitydesc.; 070521 p�d
			// We'll also add the name of the selected Qualitydescription in the
			// "esti_trakt_set_spc_table; tsetspc_qdesc_name"; 090918 p�d
			if (pItem->GetID() == ID_QUALDESC)
			{
				nQDescIndex = getQualDescIndex(pItem,sQDescName);
				// Setup record to hold save information for misc data function; 070504 p�d
				CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		recTData->getTDataID(),
																																		recTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		recTData->getSpecieID(),
																																		0,0,0,0,0,0,0,0,0,0,0,0.0,0,0.0,0.0,
																																		m_recTraktMiscData.getDiamClass(),
																																		nQDescIndex,_T(""),0,0,_T(""),sQDescName,0,0.0,0.0,0.0,0.0, 0.0);
				if (!pDB->addTraktSetSpc_rest_of_misc_data(rec))
				{
					pDB->updTraktSetSpc_rest_of_misc_data(rec);
				}

			}
			else
			{
				// Setup the "Extra information"; 070627 p�d
				sExtraInfo.Format(_T("%d;%d;%d;%d;%s;"),
							m_bIsAtCoast,
							m_bIsSouthEast,
							m_bIsRegion5,
							m_bIsPartOfPlot,
							m_sSIH100_Pine);
				// Setup record to hold save information for misc data function; 070504 p�d
				CTransaction_trakt_set_spc rec = CTransaction_trakt_set_spc(recTData->getSpecieID(),
																																		recTData->getTDataID(),
																																		recTData->getTDataTraktID(),
																																		STMP_LEN_WITHDRAW,
																																		recTData->getSpecieID(),
																																		0,0,0,0,0,0,0,0,0,0,0,0,
																																		0.0,
																																		m_fM3SkToM3Fub,
																																		m_fM3FubToM3To,
																																		0.0,0,
																																		sExtraInfo,
																																		m_nTranspDist1,
																																		m_nTranspDist2,
																																		_T(""),_T(""),0,0.0,0.0,0.0,0.0, 0.0);
				if (!pDB->addTraktSetSpcMiscData(rec))
				{
					pDB->updTraktSetSpcMiscData(rec);
				}
			}
			delete pDB;
		}	// if (pDB != NULL)
	}
}

void CMDISpecieDataFrame::getTraktMiscDataFromDB()
{
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *recTData = getActiveTraktData();

	// Save selected heightfunction to esti_trakt_set_spc_table; 070503 p�d
	if (m_bConnected)
	{
		CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_bConnected = pDB->getTraktMiscData(recTData->getTDataTraktID(),m_recTraktMiscData);
			delete pDB;
		}
	}
}
// Added 2007-06-27 p�d
// This method'll call doCalculation method in Page three after a
// change has occured in functions (volume,bark,height etc.); 070627 p�d
void CMDISpecieDataFrame::doCalculation()
{
}

// Get settings for species in this Trakt; 070504 p�d
void CMDISpecieDataFrame::getTraktSetSpc(void)
{
	if (m_bConnected)
	{
		CUMEstimateDB *pDB = new CUMEstimateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_vecTransactionTraktSetSpc.clear();
			// Only select data for This trakt; 070504 p�d
			m_bConnected = pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,getActiveTraktData()->getTDataTraktID());
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

// Get data from m_vecTransactionTraktSetSpc; 070504 p�d
BOOL CMDISpecieDataFrame::getActiveTypeOfFunction(int which,CString &s,UINT* index,int* identifer)
{
	BOOL bReturn = FALSE;
	int nFuncID;
	UINT j;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *recTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == recTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == recTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					for (j = 0;j < m_vecHgtFunc.size();j++)
					{
						UCFunctions funcs = m_vecHgtFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecHgtFunc.size();j++)
				}
				else if (which == ID_VOL_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					for (j = 0;j < m_vecVolFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
				else if (which == ID_BARK_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					for (j = 0;j < m_vecBarkFunc.size();j++)
					{
						UCFunctions funcs = m_vecBarkFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecBarkFunc.size();j++)
				}
				else if (which == ID_VOL_UB_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					for (j = 0;j < m_vecVolUBFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolUBFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
			}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
		} // for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
	}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)

	return bReturn;

}

CString CMDISpecieDataFrame::getActiveFunction(int which)
{
	CString sInfo,sTmp;
	UINT j;
	int nSpcID;
	int nFuncID;
	int nFuncIndex;
	BOOL bFound = FALSE;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		// getActiveTraktDataOnClick() = Get data from record m_recTraktDataOnClick set
		// in CPageTwoFormView on OnReportItemClick.
		// If this fails, we try to get data deom m_recTraktDataActive, set on
		// populateData in CPageTwoFormView; 081128 p�d
		CTransaction_trakt_data *pTData = getActiveTraktDataOnClick();
		if (pTData == NULL)
			pTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == pTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					nSpcID	= recSetSpc.getHgtFuncSpcID();
					nFuncIndex = recSetSpc.getHgtFuncIndex();
					for (j = 0;j < m_vecHgtFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecHgtFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s , %s"),flist.getSpcName(),flist.getFuncArea());
/*
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
*/
							return sInfo;
						}	// if (fflist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncflist.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					nSpcID	= recSetSpc.getVolFuncSpcID();
					nFuncIndex = recSetSpc.getVolFuncIndex();
					for (j = 0;j < m_vecVolFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecVolFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_BARK_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					nSpcID	= recSetSpc.getBarkFuncSpcID();
					nFuncIndex = recSetSpc.getBarkFuncIndex();
					for (j = 0;j < m_vecBarkFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecBarkFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
/*
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
*/
							sInfo.Format(_T("%s, %s"),flist.getSpcName(),flist.getFuncArea());

							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_UB_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					nSpcID	= recSetSpc.getVolUBFuncSpcID();
					nFuncIndex = recSetSpc.getVolUBFuncIndex();
					for (j = 0;j < m_vecVolUBFuncList.size();j++)
					{
						sInfo.Empty();
						UCFunctionList flist = m_vecVolUBFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							if (_tcslen(flist.getSpcName()) > 0)
							{
								sTmp.Format(_T("%s"),flist.getSpcName());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncType()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncType());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncArea()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncArea());					
								sInfo += sTmp;
							}

							if (_tcslen(flist.getFuncDesc()) > 0)
							{
								sTmp.Format(_T(", %s"),flist.getFuncDesc());					
								sInfo += sTmp;
							}
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)



			}	// if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
		}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
	
	}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	return _T("");
}

// Use this instead (no diamtercalss/specie); 070507 p�d
BOOL CMDISpecieDataFrame::getMiscData(int do_as)
{
	CString S;
	BOOL bReturn = FALSE;
	CString sExtraInfo;
	CStringArray args;
	
	CTransaction_trakt_set_spc recSetSpc;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt *pTrakt = getActiveTrakt();
	CTransaction_trakt_data *pTData = NULL;
	
	// getActiveTraktDataOnClick() = Get data from record m_recTraktDataOnClick set
	// in CPageTwoFormView on OnReportItemClick.
	// If this fails, we try to get data deom m_recTraktDataActive, set on
	// populateData in CPageTwoFormView; 081128 p�d
	pTData = getActiveTraktDataOnClick();
	if (pTData == NULL)
		pTData = getActiveTraktData();


	if (pTrakt != NULL)
	{
		m_bIsAtCoast = (pTrakt->getTraktNearCoast() ? TRUE : FALSE);
		m_bIsSouthEast = (pTrakt->getTraktSoutheast() ? TRUE : FALSE);
		m_bIsRegion5 = (pTrakt->getTraktRegion5() ? TRUE : FALSE);
		m_bIsPartOfPlot = (pTrakt->getTraktPartOfPlot() ? TRUE : FALSE);
		m_sSIH100_Pine = pTrakt->getTraktSIH100_Pine();
	}
	else
	{
		m_bIsAtCoast = FALSE;
		m_bIsSouthEast = FALSE;
		m_bIsRegion5 = FALSE;
		m_bIsPartOfPlot = FALSE;
		m_sSIH100_Pine =  _T("");
	}

	if (pTData != NULL)
	{
		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			// First find id's and indexes; 070504 p�d
			for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
			{
				recSetSpc = m_vecTransactionTraktSetSpc[i];
				// Find Settings info for This trakt; 070504 p�d
				if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
						recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
						recSetSpc.getTSetspcID() == pTData->getSpecieID())
				{	
					if (do_as == 0 || do_as == 1)
					{
						m_fM3SkToM3Ub = recSetSpc.getM3SkToM3Ub();
					}
					if (do_as == 0 || do_as == 2)
					{
						m_fM3SkToM3Fub = recSetSpc.getM3SkToM3Fub();
						m_fM3FubToM3To = recSetSpc.getM3FubToM3To();
						m_nTranspDist1 = recSetSpc.getTranspDist1();
						m_nTranspDist2 = recSetSpc.getTranspDist2();
					}
					break;
				}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
			}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
		}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	}
	return bReturn;
}

int CMDISpecieDataFrame::getQualDescIndex(CXTPPropertyGridItem *pItem,CString& qdesc_name)
{
	CString S;
	CXTPPropertyGridItemConstraints *pConstraints = pItem->GetConstraints();	

	if (pConstraints != NULL)
	{
		CXTPPropertyGridItemConstraint *pConstraint = pConstraints->GetConstraintAt(pConstraints->GetCurrent());
		if (pConstraint != NULL)
		{
			int nIndex = (int)pConstraint->m_dwData;
			if (nIndex >= 0 && nIndex < m_listQDesc.GetCount())
			{
				qdesc_name = m_listQDesc.GetAt(nIndex);
				return nIndex;
			}	// if (nIndex >= 0 && nIndex < m_listQDesc.GetCount())
		}	// if (pConstraint != NULL)
	}	// if (pConstraints != NULL)

	return -1;
}

// Use this instead (no diamtercalss/specie); 070507 p�d
void CMDISpecieDataFrame::getQualDescData(int *qdesc_index,CStringArray& qdesc_list)
{
	CTransaction_trakt_set_spc recSetSpc;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *pTData = getActiveTraktData();
	if (pTData != NULL)
	{
		// Retrive information from selected pricelist, for active trakt.
		// In thiscase, we'll get info. on Qualitydescription(s); 070521 p�d
		getTraktMiscDataFromDB();

		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			// First find id's and indexes; 070504 p�d
			for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
			{
				recSetSpc = m_vecTransactionTraktSetSpc[i];
				// Find Settings info for This trakt; 070504 p�d
				if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
						recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
						recSetSpc.getTSetspcID() == pTData->getSpecieID())
				{
					*qdesc_index = recSetSpc.getSelQDescIndex();
				}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
			}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
		}	// if (m_vecTransactionTraktSetSpc.size() > 0)
/*
		PricelistParser *pPrlParser = new PricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->LoadFromBuffer(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get qualitydescriptions for pricelist; 070521 p�d
				pPrlParser->getQualDescNameForSpecie(pTData->getSpecieID(),m_listQDesc);
			}
			delete pPrlParser;
		}
*/
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(m_recTraktMiscData.getXMLPricelist()))
			{
				//---------------------------------------------------------------------------------
				// Get qualitydescriptions for pricelist; 070521 p�d
				pPrlParser->getQualDescNameForSpecie(pTData->getSpecieID(),m_listQDesc);
			}
			delete pPrlParser;
		}
	}
}

CString CMDISpecieDataFrame::getSetSpcDate(void)
{
	CTransaction_trakt_set_spc recSetSpc;
	// Get active trakt record; holds information on trakt-id; 070430 p�d
	CTransaction_trakt_data *pTData = getActiveTraktData();
	if (pTData != NULL)
	{
		// Retrive information from selected pricelist, for active trakt.
		// In thiscase, we'll get info. on Qualitydescription(s); 070521 p�d
		getTraktMiscDataFromDB();

		if (m_vecTransactionTraktSetSpc.size() > 0)
		{
			// First find id's and indexes; 070504 p�d
			for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
			{
				recSetSpc = m_vecTransactionTraktSetSpc[i];
				// Find Settings info for This trakt; 070504 p�d
				if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
						recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
						recSetSpc.getTSetspcID() == pTData->getSpecieID())
				{
					return recSetSpc.getCreated();
				}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
			}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
		}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	}	// if (pTData != NULL)

	return  _T("");
}


// CMDISpecieDataFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CTraktSelectListFrame

IMPLEMENT_DYNCREATE(CTraktSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTraktSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTraktSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN2, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CTraktSelectListFrame::CTraktSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CTraktSelectListFrame::~CTraktSelectListFrame()
{
}

void CTraktSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_TRAKT_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CTraktSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg.Create(this, IDD_FIELD_CHOOSER1,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN1))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CTraktSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_TRAKT_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CTraktSelectListFrame::OnSetFocus(CWnd *pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CTraktSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CTraktSelectListFrame diagnostics

#ifdef _DEBUG
void CTraktSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CTraktSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CTraktSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TRAKT_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TRAKT_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CTraktSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CTraktSelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CTraktSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CTraktSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CTraktSelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CTraktSelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sToolTipFilter = xml->str(IDS_STRING234);
			m_sToolTipFilterOff = xml->str(IDS_STRING235);
			m_sToolTipPrintOut = xml->str(IDS_STRING236);
			m_sToolTipRefresh = xml->str(IDS_STRING237);

			m_wndFieldChooserDlg.SetWindowText((xml->str(IDS_STRING232)));
			m_wndFilterEdit.SetWindowText((xml->str(IDS_STRING234)));
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CTraktSelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR2)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_UPDATE,m_sToolTipRefresh);	//
					setToolbarBtn(sTBResFN,p->GetAt(4),-1,_T(""),FALSE);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CTraktSelectListFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CPlotSelListFrame

IMPLEMENT_DYNCREATE(CPlotSelListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPlotSelListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPlotSelListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_COMMAND(ID_TBBTN_ADDPLOT, OnAddPlotTBtn)
	ON_COMMAND(ID_TBBTN_DELPLOT, OnDelPlotTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_ADDPLOT, OnUpdateAddPlotTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_DELPLOT, OnUpdateDelPlotTBtn)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPlotSelListFrame::CPlotSelListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));

	m_nInvMethodIndex = -1;
	m_bPossibleToAddGroup = TRUE;
	m_bPossibleToDelGroup = FALSE;

}

CPlotSelListFrame::~CPlotSelListFrame()
{
}

void CPlotSelListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PLOT_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CPlotSelListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR3);


	if (m_wndInvMethodInfoDialog.Create(this,IDD_DIALOGBAR1,CBRS_TOP,0))
	{
		m_wndLblInvMethod.SubclassDlgItem(IDC_LBL_INVMETHOD,&m_wndInvMethodInfoDialog);
	}

	CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
	if (pTabView != NULL)
	{
		CPageThreeFormView *pView3 = DYNAMIC_DOWNCAST(CPageThreeFormView, CWnd::FromHandle(pTabView->getTabCtrl().getTabPage(2)->GetHandle()));
		if (pView3 != NULL)
		{
			m_nInvMethodIndex = pView3->getSelectedInvMethodIndex();
		}	// if (pView3 != NULL)
	}

	EnableDocking(CBRS_ALIGN_ANY);

	setLanguage();

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPlotSelListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PLOT_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPlotSelListFrame::OnSetFocus(CWnd *pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CPlotSelListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPlotSelListFrame diagnostics

#ifdef _DEBUG
void CPlotSelListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPlotSelListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPlotSelListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PLOT_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PLOT_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPlotSelListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPlotSelListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CPlotSelListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if (m_wndLblInvMethod.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndLblInvMethod,1,2,rect.right - 2,30);
	}
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPlotSelListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

// MY METHODS

void  CPlotSelListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sToolTipAdd = xml->str(IDS_STRING279);
			m_sToolTipDel = xml->str(IDS_STRING288);

			m_sarrInventoryMethods.Add(_T("<") + xml->str(IDS_STRING3110) + _T(">"));
			m_sarrInventoryMethods.Add(xml->str(IDS_STRING3111));
			m_sarrInventoryMethods.Add(xml->str(IDS_STRING3112));
			m_sarrInventoryMethods.Add(xml->str(IDS_STRING3113));
			m_sarrInventoryMethods.Add(xml->str(IDS_STRING3114));
			if (m_wndLblInvMethod.GetSafeHwnd() != NULL)
			{
				CString sMsg;
				if (m_nInvMethodIndex >= 0 && m_nInvMethodIndex < m_sarrInventoryMethods.GetCount() )
				{
					// Setup message depending on Inventory method; 070514 p�d
					if (m_nInvMethodIndex == 1 ||		// "Totaltaxering"
							m_nInvMethodIndex == 5)			// "Snabbtaxering"
					{				
						sMsg.Format(_T("%s : <b>%s</b><br>%s : <i><font color=\"red\">%s</font></i>"),
							xml->str(IDS_STRING277),
							m_sarrInventoryMethods.GetAt(m_nInvMethodIndex),
							xml->str(IDS_STRING282),
							xml->str(IDS_STRING2820));
					} // if (m_nInvMethodIndex == 1 && m_nInvMethodIndex == 5)
					else if (m_nInvMethodIndex == 2)	// "Cirkelytor"
					{
						sMsg.Format(_T("%s : <b>%s</b><br>%s : <i><font color=\"red\">%s</font></i>"),
							xml->str(IDS_STRING277),
							m_sarrInventoryMethods.GetAt(m_nInvMethodIndex),
							xml->str(IDS_STRING282),
							xml->str(IDS_STRING2821));
					}
					else if (m_nInvMethodIndex == 3)	// "Rekangul�ra ytor"
					{
						sMsg.Format(_T("%s : <b>%s</b><br>%s : <i><font color=\"red\">%s</font></i>"),
							xml->str(IDS_STRING277),
							m_sarrInventoryMethods.GetAt(m_nInvMethodIndex),
							xml->str(IDS_STRING282),
							xml->str(IDS_STRING2822));
					}
					else if (m_nInvMethodIndex == 4)	// "Plats (ex. Trave)
					{
						sMsg.Format(_T("%s : <b>%s</b><br>%s : <i><font color=\"red\">%s</font></i>"),
							xml->str(IDS_STRING277),
							m_sarrInventoryMethods.GetAt(m_nInvMethodIndex),
							xml->str(IDS_STRING282),
							xml->str(IDS_STRING2823));
					}
				}
				else
					sMsg.Format(_T("%s"),xml->str(IDS_STRING277));

				m_wndLblInvMethod.SetBkColor(INFOBK);
				m_wndLblInvMethod.SetWindowText(sMsg);
			}
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CPlotSelListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR3)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,m_sToolTipAdd);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL,m_sToolTipDel);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CPlotSelListFrame message handlers

// Toolbar button click; 070511 p�d
void CPlotSelListFrame::OnAddPlotTBtn()
{
	int nInvMethodIndex = getInvMethodIndex();
	int nGroupID = getGroupID();
	int nTraktID = getActiveTrakt()->getTraktID();
	CPlotSelListFormView *pView = (CPlotSelListFormView *)getFormViewByID(IDD_REPORTVIEW4);
	if (pView != NULL)
	{
		pView->addGroupToReport(CTransaction_plot(nGroupID,nTraktID,_T(""),nInvMethodIndex,0.0,0.0,0.0,0.0,_T(""),0,_T("")));
		// Set Toolbar button AddPlot; 070514 p�d
		m_bPossibleToAddGroup = (nInvMethodIndex >= 1 && nInvMethodIndex <= 5);
	}
}

void CPlotSelListFrame::OnDelPlotTBtn()
{
	CPlotSelListFormView *pView = (CPlotSelListFormView *)getFormViewByID(IDD_REPORTVIEW4);
	if (pView != NULL)
	{
		pView->delPlot();
		
		// Make sure PageThree is up to date; 070515 p�d
		populatePageThree();
	}	// if (pView != NULL)
}

void CPlotSelListFrame::OnUpdateAddPlotTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bPossibleToAddGroup);
}

void CPlotSelListFrame::OnUpdateDelPlotTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bPossibleToDelGroup);
}

/////////////////////////////////////////////////////////////////////////////
// CMDIInformationFrame

IMPLEMENT_DYNCREATE(CMDIInformationFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIInformationFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIInformationFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIInformationFrame::CMDIInformationFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
}

CMDIInformationFrame::~CMDIInformationFrame()
{
}

void CMDIInformationFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_INFORMATION_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CMDIInformationFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR4);

	EnableDocking(CBRS_ALIGN_ANY);

	setLanguage();

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIInformationFrame::OnEraseBkgnd(CDC*)
{
	return FALSE;
}

// load the placement in OnShowWindow()
void CMDIInformationFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_INFORMATION_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIInformationFrame::OnSetFocus(CWnd *pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CMDIInformationFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIInformationFrame diagnostics

#ifdef _DEBUG
void CMDIInformationFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIInformationFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIInformationFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_INFOVIEW;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_INFOVIEW;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIInformationFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIInformationFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

void CMDIInformationFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIInformationFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

// MY METHODS

void  CMDIInformationFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CMDIInformationFrame::setupToolBarIcons(void)
{

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR4)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_PRINT,_T(""));	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL,_T(""));	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

}

// CMDIInformationFrame message handlers

