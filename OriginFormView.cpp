// CategoryFormView.cpp : implementation file
//

#include "stdafx.h"
#include "OriginFormView.h"

#include "ResLangFileReader.h"

// COriginFormView

IMPLEMENT_DYNCREATE(COriginFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(COriginFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

COriginFormView::COriginFormView()
	: CXTResizeFormView(COriginFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

COriginFormView::~COriginFormView()
{
}

void COriginFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecOriginData.clear();

	m_wndReport1.ClearReport();

	CXTResizeFormView::OnDestroy();
}


void COriginFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL COriginFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL COriginFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void COriginFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL COriginFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// COriginFormView diagnostics

#ifdef _DEBUG
void COriginFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void COriginFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


void COriginFormView::doSetNavigationBar()
{
	if (m_vecOriginData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// COriginFormView message handlers

BOOL COriginFormView::getOrigins(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getOrigins(m_vecOriginData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void COriginFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL COriginFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_TRAKT_TYPE_REPORT, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				m_sMsgCap	= xml->str(IDS_STRING151);
				m_sOriginID	= xml->str(IDS_STRING153);
				m_sOrigin	= xml->str(IDS_STRING164);
				m_sOriginNotes	= xml->str(IDS_STRING166);
				m_sOKBtn	= xml->str(IDS_STRING155);
				m_sCancelBtn	= xml->str(IDS_STRING156);
				m_sText1	= xml->str(IDS_STRING1571);
				m_sText2	= xml->str(IDS_STRING1581);
				m_sText3	= xml->str(IDS_STRING1591);
				m_sDoneSavingMsg = xml->str(IDS_STRING168);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (m_sOriginID), 30));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				pCol->SetVisible( FALSE );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (m_sOrigin), 60));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, (m_sOriginNotes), 120));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getOrigins();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 1,rect.bottom - 1);

			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void COriginFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_TRAKT_TYPE_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT COriginFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addOrigin();
			if (saveOrigin())
			{
				getOrigins();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveOrigin())
			{
				getOrigins();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getOrigins();
			if (removeOrigin())
			{
				getOrigins();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL COriginFormView::populateReport(void)
{
	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecOriginData.size() > 0)
	{
		for (UINT i = 0;i < m_vecOriginData.size();i++)
		{
			CTransaction_origin rec = m_vecOriginData[i];
			m_wndReport1.AddRecord(new COriginReportRec(rec.getID(),rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL COriginFormView::addOrigin(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new COriginReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

BOOL COriginFormView::saveOrigin(void)
{
	CTransaction_origin rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{

				for (int i = 0;i < pRecs->GetCount();i++)
				{
					COriginReportRec *pRec = (COriginReportRec *)pRecs->GetAt(i);

					rec = CTransaction_origin(pRec->getColumnInt(0),
 																		pRec->getColumnText(1),
																		pRec->getColumnText(2),
																		_T(""));

					if (!m_pDB->addOrigin(rec))
						m_pDB->updOrigin(rec);

				}	// for (int i = 0;i < pRecs->GetCount();i++)

	// Commented out (PL); 070402 p�d
	//			UMMessageBox(this->GetSafeHwnd(),m_sDoneSavingMsg,m_sMsgCap,MB_ICONINFORMATION | MB_OK);

			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL COriginFormView::removeOrigin(void)
{
	CXTPReportRow *pRow = NULL;
	COriginReportRec *pRec = NULL;
	CTransaction_origin data;
	CString sMsg;
	if (m_bConnected)	
	{
		pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (COriginReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				data = CTransaction_origin(pRec->getID(),pRec->getColumnText(1),pRec->getColumnText(2),_T(""));
				if (m_pDB != NULL)
				{
					if (!isOriginUsed(data))
					{
						// Setup a message for user upon deleting machine; 061010 p�d
						sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n\n%s\n\n%s"),
												m_sText1,
												//m_sOriginID,
												//data.getID(),
												m_sOrigin,
												data.getOrigin(),
												m_sOriginNotes,
												data.getNotes(),
												m_sText2,
												m_sText3);

						if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						{
							// Delete Machine and ALL underlying 
							//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
							m_pDB->removeOrigin(data);
						}	// if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
					}	// if (!isOriginUsed(data))
				}	// if (m_pDB != NULL)
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
		return TRUE;
	}
	return FALSE;
}

void COriginFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}

BOOL COriginFormView::isOriginUsed(CTransaction_origin &)
{
	return FALSE;
}
