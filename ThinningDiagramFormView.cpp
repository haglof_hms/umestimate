// ThinningDiagramFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "ThinningDiagramFormView.h"

#include "XTPPreviewView.h"

#include "ResLangFileReader.h"

// CThinningDiagramFormView

int CThinningDiagramFormView::m_nCurCBSel = -1;

IMPLEMENT_DYNCREATE(CThinningDiagramFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CThinningDiagramFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
//	ON_WM_ERASEBKGND()
	ON_CBN_SELCHANGE(IDC_COMBO16_1, OnCBoxSpeciesChanged)
END_MESSAGE_MAP()

CThinningDiagramFormView::CThinningDiagramFormView()
	: CXTResizeFormView(CThinningDiagramFormView::IDD)
{
	m_bInitialized = FALSE;
	m_nTraktID = -1;
	m_nShowSpecies = 1;
	m_fSpcGY = 0.0;
	m_fSpcMaxHgt = 0.0;
}

CThinningDiagramFormView::~CThinningDiagramFormView()
{
}

void CThinningDiagramFormView::OnDestroy()
{
	m_fntCaption.DeleteObject();
	m_fntText.DeleteObject();
	m_fntTextBold.DeleteObject();
	m_fntTextSmall.DeleteObject();

	CXTResizeFormView::OnDestroy();	
}

void CThinningDiagramFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CThinningDiagramFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	return 0;
}

BOOL CThinningDiagramFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
#ifdef SHOW_THINNING

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
#endif
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CThinningDiagramFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
#ifdef SHOW_THINNING
	//{{AFX_DATA_MAP(CHelloworldDlg)
	DDX_Control(pDX, IDC_CHART_THINNING, m_chartViewer);
	DDX_Control(pDX, IDC_LBL16_1, m_wndLbl1);
	DDX_Control(pDX, IDC_COMBO16_1, m_wndCBox1);
	//}}AFX_DATA_MAP
#endif
}

void CThinningDiagramFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
#ifdef SHOW_THINNING
	if (!m_bInitialized)
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sDiagramCaption = xml.str(IDS_STRING2541);
				m_sDiagramYLabel = xml.str(IDS_STRING2542);
				m_sDiagramXLabel = xml.str(IDS_STRING2543);			
				m_sStandNumber = xml.str(IDS_STRING1000);
				m_sStandName = xml.str(IDS_STRING101);
				m_sPropertyNameLabel = xml.str(IDS_STRING104);
				m_sMsgCap = xml.str(IDS_STRING151);

				m_wndLbl1.SetWindowText(xml.str(IDS_STRING2544));
			}
			xml.clean();

			m_fntCaption.CreateFont(190, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));

			m_fntText.CreateFont(90, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextBold.CreateFont(90, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
			m_fntTextSmall.CreateFont(70, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Verdana"));
		
	
		}

		m_bInitialized = TRUE;
	}
#endif	
}

BOOL CThinningDiagramFormView::OnEraseBkgnd(CDC* pDC) 
{ 
	// Set brush to desired background color 
	CBrush backBrush(RGB(255, 255, 255)); 

	// Save old brush 
	CBrush* pOldBrush = pDC->SelectObject(&backBrush); 

	// Erase the area needed with the given background color
	CRect rect; 
	pDC->GetClipBox(&rect);
	pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY); 

	// Restore old brush and exit
	pDC->SelectObject(pOldBrush); 
	return TRUE; 
} 


void CThinningDiagramFormView::OnSetFocus(CWnd*)
{
}

void CThinningDiagramFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CThinningDiagramFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
#ifdef SHOW_THINNING
	if (m_chartViewer.GetSafeHwnd()) 
	{
		m_chartViewer.MoveWindow(10,40,cx-20,cy);
		drawChartView();
	}
#endif
}

// CThinningDiagramFormView diagnostics

#ifdef _DEBUG
void CThinningDiagramFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CThinningDiagramFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CThinningDiagramFormView::getTraktSetSpcFromDB(int trakt_id)
{
	if (m_pDB)
	{
		m_pDB->getTraktSetSpc(m_vecTransactionTraktSetSpc,trakt_id);
	}	// if (m_pDB != NULL)
}

void CThinningDiagramFormView::getTraktDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(m_vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	}	// if (pDB != NULL)
}

void CThinningDiagramFormView::getTraktMiscDataFromDB(int trakt_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktMiscData(trakt_id,m_recTraktMiscData);
	}	// if (pDB != NULL)
}

void CThinningDiagramFormView::getSampleTreesFromDB(int trakt_id,int tree_type)
{
	if (m_pDB != NULL)
	{
		m_pDB->getSampleTrees(trakt_id,tree_type,m_vecTraktSampleTrees);
	}	// if (m_pDB != NULL)
}

// CThinningDiagramFormView message handlers

void CThinningDiagramFormView::drawChartView(bool redraw,bool save_to_db,drawType dt,CDC* pDC)
{
#ifdef SHOW_THINNING
	CString S;
	CRect rect;
	GetClientRect(&rect);

	// Create a XYChart
	XYChart *xyChart = new XYChart(rect.right-20, rect.bottom-50);

	if (m_nTraktID != getActiveTrakt()->getTraktID() || redraw)
	{
		getTraktDataFromDB(getActiveTrakt()->getTraktID());
		if (m_vecTraktData.size() > 0)
		{

			m_wndCBox1.ResetContent();
	
			for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			{
				CTransaction_trakt_data recTraktData = m_vecTraktData[i1];
				if (recTraktData.getNumOf() > 0 && recTraktData.getSpecieID() <= 2)
				{
					m_wndCBox1.AddString(recTraktData.getSpecieName());
				}	
				if (recTraktData.getSpecieID() == m_nShowSpecies)
				{
					m_fSpcGY = recTraktData.getGY();
					m_fSpcMaxHgt = m_pDB->getDCLSMaxTreeHeightForSpc(getActiveTrakt()->getTraktID(),m_nShowSpecies);
				}
			}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
			if (m_nCurCBSel == -1)
			{
				m_wndCBox1.SetCurSel(0);
			}
			else
			{
				m_wndCBox1.SetCurSel(m_nCurCBSel);
			}
		}

		m_nTraktID = getActiveTrakt()->getTraktID();
	}

	//-----------------------------------------------------------------------
  // The data for the line chart
	std::vector<DblArr> vecDblArrX,vecDblArrY;
	vecDbl vDblX,vDblY;
	vDblX.clear();
	vDblY.clear();
	vDblX.push_back(m_fSpcMaxHgt/10.0);
	vDblY.push_back(m_fSpcGY);
	vecDblArrX.push_back(DblArr(vDblX));
	vecDblArrY.push_back(DblArr(vDblY));

	if (dt == DRAW_VIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(100, 15, rect.right-400, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);

	}
	else if (dt == DRAW_PREVIEW)
	{
		//-----------------------------------------------------------------------
		// Set the plotarea, with a light grey border (0xc0c0c0). 
		// Turn on both horizontal and vertical grid lines with light
		// grey color (0xc0c0c0)
		xyChart->setPlotArea(45, 10, rect.right-350, rect.bottom-100, 0xc0c0c0, 0xefefef, 0x000000,0xa0a0a0,-1);
	}

	// Add a title to the y axis
	xyChart->yAxis()->setTitle("Grundyta (m�/ha)");

  // Add a title to the x axis
  xyChart->xAxis()->setTitle("�vre h�jd (m)");


  // Plot sampletrees for specie; 100406 p�d
	xyChart->addScatterLayer(DoubleArray(vecDblArrX[0].get(),vecDblArrX[0].numof()),
													 DoubleArray(vecDblArrY[0].get(),vecDblArrY[0].numof()),
													 "",Chart::CircleShape,8);

  xyChart->yAxis()->setLinearScale(0, 48);
  xyChart->xAxis()->setLinearScale(0, 38, 2, 1);

	if (dt == DRAW_VIEW)
	{
	  // Output the chart
		m_chartViewer.setChart(xyChart);
	}
	else if (dt == DRAW_PREVIEW)
	{
		// The scaling factor to adjust for printer resolution
		// (pDC = CDC pointer representing the printer device context)
		double xScaleFactor = pDC->GetDeviceCaps(LOGPIXELSX) / 96.0;
		double yScaleFactor = pDC->GetDeviceCaps(LOGPIXELSY) / 96.0;

		// Output the chart as BMP
		MemBlock bmp = xyChart->makeChart(Chart::BMP);

		// The BITMAPINFOHEADER is 14 bytes offset from the beginning
		LPBITMAPINFO header = (LPBITMAPINFO)(bmp.data + 14);

		// The bitmap data
		LPBYTE bitData = (LPBYTE)(bmp.data) + ((LPBITMAPFILEHEADER)(bmp.data))->bfOffBits;

		int nLeft = 20*xScaleFactor;
		int nTop = 20*yScaleFactor;
		int nWidth = 5220; //header->bmiHeader.biWidth*xScaleFactor*0.6;
		int nHeight = 60*yScaleFactor;

		TEXTMETRIC tm;
		CBrush brush;
		brush.CreateSolidBrush(RGB(0,0,255)); 
		//---------------------------------------------------------------------------
		// Setup Caption
		pDC->FillRect(CRect(nLeft,nTop,nWidth*0.9,nHeight),&brush);
		pDC->SelectObject((CFont*)&m_fntCaption);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->DrawText(L"Gallringsmall",CRect(nLeft,nTop,nWidth*0.9,nHeight),DT_SINGLELINE|DT_CENTER|DT_VCENTER);

		//---------------------------------------------------------------------------
		// Setup date
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextSmall);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nLeft,nHeight,getDateTime());

		//---------------------------------------------------------------------------
		// Setup Name of Stand
		int nTextX = 0;
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SelectObject((CFont*)&m_fntTextBold);
		pDC->GetTextMetrics(&tm);
		nTextX = max(nTextX,m_sStandNumber.GetLength());
		nTextX = max(nTextX,m_sStandName.GetLength());
		nTextX = max(nTextX,m_sPropertyNameLabel.GetLength());
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*2,m_sStandNumber+L":");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*3,m_sStandName+L":");
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft,nHeight+tm.tmHeight*4,m_sPropertyNameLabel+L":");

		pDC->TextOut(nLeft,nHeight+tm.tmHeight*6,L"Tr�dslag:");
		pDC->TextOut(nLeft,nHeight+tm.tmHeight*7,L"H100 (m):");

		pDC->SelectObject((CFont*)&m_fntText);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*2,getActiveTrakt()->getTraktNum());
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*3,getActiveTrakt()->getTraktName());
		if (!m_sPropertyName.IsEmpty())
			pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*4,m_sPropertyName);

		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*6,m_sSpcName);
		pDC->TextOut(nLeft+tm.tmAveCharWidth*nTextX+tm.tmAveCharWidth*5,nHeight+tm.tmHeight*7,getActiveTrakt()->getTraktSIH100());

		//---------------------------------------------------------------------------
		// Output to device context
		StretchDIBits(pDC->m_hDC,
									(int)(10 * xScaleFactor),
									(int)(300 * yScaleFactor),
									(int)(1392 * xScaleFactor * 0.6),
									(int)(583 * yScaleFactor * 0.6),
									0, 0, header->bmiHeader.biWidth, header->bmiHeader.biHeight,
									bitData, header, DIB_RGB_COLORS, SRCCOPY);

	}


	for (UINT i = 0;i < vecDblArrX.size();i++)
		vecDblArrX[i].free();
	for (UINT i = 0;i < vecDblArrY.size();i++)
		vecDblArrY[i].free();

	//free up resources
	delete xyChart;
#endif
}

void CThinningDiagramFormView::OnPrint(CDC* pDC, CPrintInfo *pInfo)
{
	drawChartView(false,false,DRAW_PREVIEW,pDC);
}

BOOL CThinningDiagramFormView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return CXTResizeFormView::DoPreparePrinting(pInfo);
}

void CThinningDiagramFormView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this, RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		UMMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}
}

void CThinningDiagramFormView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	drawChartView();
	CXTResizeFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CThinningDiagramFormView::doRunPrint()	
{ 
	this->OnFilePrint();
}

void CThinningDiagramFormView::OnCBoxSpeciesChanged(void)
{
#ifdef SHOW_THINNING
	m_nCurCBSel = m_wndCBox1.GetCurSel();
	m_wndCBox1.GetWindowText(m_sSpcName);
	m_nShowSpecies = 1;
	if (m_nCurCBSel != CB_ERR)
	{
		if (m_nCurCBSel >= 0)
		{
			if (m_vecTraktData.size() > 0 )
			{		
				for (UINT i = 0;i < m_vecTraktData.size();i++)
				{
					if (m_vecTraktData[i].getSpecieName().Compare(m_sSpcName) == 0)
					{
						m_nShowSpecies = m_vecTraktData[i].getSpecieID();
						break;
					}
				}
			}	
		}
	}
	drawChartView(true);
#endif
}
