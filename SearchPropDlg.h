#pragma once

#include "Resource.h"

// CSearchPropDlg dialog

class CSearchPropDlg : public CDialog
{
	DECLARE_DYNAMIC(CSearchPropDlg)

public:
	CSearchPropDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSearchPropDlg();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
