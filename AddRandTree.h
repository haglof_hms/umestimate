#pragma once

#include "OXMaskedEdit.h"    // COXMaskedEdit

#include "Resource.h"
#include "afxwin.h"


// CAddRandTree dialog

#define COLUMN_ADD_TREE_GROUP	0
#define COLUMN_ADD_TREE_TYPE	1
#define COLUMN_ADD_TREE_SPCID	2
#define COLUMN_ADD_TREE_SPCNAME	3
#define COLUMN_ADD_TREE_DBH		4
#define COLUMN_ADD_TREE_HGT		5
#define COLUMN_ADD_TREE_AGE		6	
#define COLUMN_ADD_TREE_BON		7
#define COLUMN_ADD_TREE_CAT		8
#define COLUMN_ADD_TREE_FAS		9
#define COLUMN_ADD_TREE_TOP		10
#define COLUMN_ADD_TREE_GRK		11
#define COLUMN_ADD_TREE_BRK		12
#define COLUMN_ADD_TREE_GROT	13	
#define COLUMN_ADD_TREE_GROTWH	14
#define COLUMN_ADD_TREE_FROM	15
#define COLUMN_ADD_TREE_TO		16


class CAddRandTree : public CDialog
{
	DECLARE_DYNAMIC(CAddRandTree)

	CString m_sLangFN;

	CMyReportCtrl m_wndReport;
	int m_nTaxeringsTyp;
	int m_nYtNr;
	int m_nTraktId;
	vecTransactionSampleTree m_vecTraktSampleTrees;
	CStringArray m_sarrTreeTypes;
	CStringArray m_sarrInventoryMethods;
	vecTransactionSampleTreeCategory m_vecSmpTreeCategories;
	vecTransactionSpecies m_vecSpecies;
	CTransaction_trakt_misc_data m_recTraktMiscData;

public:
	CAddRandTree(int nTraktId,int nTaxeringsTyp, CStringArray &sarrInventoryMethods, CStringArray &sarrTreeTypes, vecTransactionSampleTreeCategory &vecSmpTreeCategories, vecTransactionSpecies &m_vecSpecies, CTransaction_trakt_misc_data &recTraktMiscData,CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddRandTree();

	void CAddRandTreeToReport(int grp_id = -1,int spc_id = -1);


	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG_ADD_RAND_TREE };

protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CIntItemInplaceBtn : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
		CString m_sLangFN;
		HWND hWndParent;
	public:
		CIntItemInplaceBtn(int nValue,LPCTSTR lang_fn,HWND parent) : CXTPReportRecordItemNumber()
		{
			m_nValue = nValue;
			SetValue(m_nValue);
			m_sLangFN = lang_fn;
			hWndParent = parent;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
		
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(value);
		}
	protected:
		virtual void OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
		{
			int nInvMethod = getInvMethodIndex();
			if (nInvMethod >= 1)
			{
				::UpdateWindow(hWndParent);
				showFormView(IDD_REPORTVIEW4,m_sLangFN,0);
				// On user selects a Group set PageThree dirty; 070516 p�d
				setIsDirtyPageThree();
			}
		}
	};

	class CExTextH100Item : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		int m_nConstraintIndex;
	public:
		CExTextH100Item(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_sText = szText;
			if (m_sText.FindOneOf(_T("TGB")) > -1 || m_sText.IsEmpty())
			{
				m_sText.Delete(3,m_sText.GetLength());
				SetValue(m_sText);
			}
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}

		void setIsBold(void)		{ SetBold();	}

		void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
		void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	};



	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
		afx_msg void OnSize(UINT nType,int cx,int cy);
		afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
		void myOnSize();
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()

private:
	LPCTSTR getValueForCategory(vecTransactionSampleTreeCategory *pvec_categories, int id)
	{
		for (int i = 0; i < pvec_categories->size(); i++)
		{
			if (pvec_categories->at(i).getID() == id)
				return pvec_categories->at(i).getCategory();
		}
		return _T("");
	}
	BOOL CheckData();
	int getIndexForTreeType(LPCTSTR value);
	int getIndexForCategory(LPCTSTR value);
protected:
	CButton m_wndButtonAddTree;
	CButton m_wndButtonCancel;
	CButton m_wndButtonOk;
	CButton m_wndButtonRemoveTree;
	void addConstraintsToReport(void);
	afx_msg void OnReportItemValChange(NMHDR * pNotifyStruct, LRESULT * /*result*/);
public:
	afx_msg void OnBnClickedButtonAddRandtree();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonRemoveRandtree();
};
