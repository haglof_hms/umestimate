#if !defined(AFX_UMESTIMATEDB_H)
#define AFX_UMESTIMATEDB_H

#include "StdAfx.h"


//////////////////////////////////////////////////////////////////////////////////////////
// Indexvector for populateData(...); 080325 p�d
typedef std::vector<long> vecTraktIndex;

//////////////////////////////////////////////////////////////////////////////////////////
// This structure is added to hold information collected in function: 
// getNumOfRandTreesPerSpcAndDCLS(...); 080310 p�d
typedef struct _rand_tree_counter
{
	int nSpcID;
	int nPlotID;
	CString sSpcname;
	double fDCLS_From;
	double fDCLS_To;
	int nNumOf;

	_rand_tree_counter(void)
	{
		nSpcID = -1;
		nPlotID = -1;
		sSpcname = _T("");
		fDCLS_From = 0.0;
		fDCLS_To = 0.0;
		nNumOf = -1;
	}

	_rand_tree_counter(int spc_id,int plot_id,LPCTSTR spc_name,double dcls_from,double dcls_to,int numof)
	{
		nSpcID = spc_id;
		nPlotID = plot_id;
		sSpcname = (spc_name);
		fDCLS_From = dcls_from;
		fDCLS_To = dcls_to;
		nNumOf = numof;
	}

} RAND_TREE_COUNTER;

typedef std::vector<RAND_TREE_COUNTER> vecRandTreeCounter;

typedef struct _rand_tree_match
	{
		int matched_diam_class;
		int num_of_rand_trees;
		_rand_tree_match(void)
		{
			matched_diam_class=0;
			num_of_rand_trees=0;
		}
	} RAND_TREE_MATCH;
	
typedef	std::vector<RAND_TREE_MATCH> vecDiamClExists;
	


class CUMEstimateDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL traktTypeExists(CTransaction_trakt_type &);
	BOOL originExists(CTransaction_origin &);
	BOOL inputMethodExists(CTransaction_input_method &);
	BOOL hgtClassExists(CTransaction_hgt_class &);
	BOOL cutClassExists(CTransaction_cut_class &);
	BOOL traktExists(int trakt_id);
	BOOL traktExists(CTransaction_trakt &);
	BOOL traktDataExists(int trakt_id);
	BOOL traktDataExists(int trakt_id,int tdata_id,int tdata_data_type);
	BOOL traktDataExists(CTransaction_trakt_data &);
	BOOL traktAssExists(CTransaction_trakt_ass &);
	BOOL traktAssExists(int trakt_id);
	BOOL traktAssExists(int trakt_id,int trakt_data_id);
	BOOL traktTransExists(CTransaction_trakt_trans &);
	BOOL traktTransExists(int trakt_data_id,int trakt_id);
	BOOL traktTransExists(int trakt_id);
	BOOL traktSetSpcExists(CTransaction_trakt_set_spc &);
	BOOL traktMiscDataExists(int trakt_id);
	BOOL traktMiscDataExists(CTransaction_trakt_misc_data &);
	BOOL traktPlotExists(CTransaction_plot &);
	BOOL traktPlotExists(int plot_id,int trakt_id);
	BOOL traktPlotExists(int trakt_id);
	BOOL traktSampleTreeExists(CTransaction_sample_tree &);
	BOOL traktSampleTreeExists(int trakt_id);
	BOOL traktSampleTreeExists(long tree_id,int trakt_id);
	BOOL traktDCLSTreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLSTreeExists(int trakt_id);
	BOOL traktDCLSTreeExists(long tree_id,int trakt_id);
	BOOL traktDCLS1TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS1TreeExists(int trakt_id);
	BOOL traktDCLS1TreeExists(long tree_id,int trakt_id);
	BOOL traktDCLS2TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS2TreeExists(int trakt_id);
	BOOL traktDCLS2TreeExists(long tree_id,int trakt_id);
	BOOL traktAssTreeExists(CTransaction_tree_assort &);
	BOOL traktAssTreeExists(int trakt_id);
	BOOL traktRotpostExists(CTransaction_trakt_rotpost &);
	BOOL traktRotpostSpcExists(CTransaction_trakt_rotpost_spc &);
	BOOL traktRotpostSpcExists(int trakt_id);
	BOOL traktRotpostOtcExists(CTransaction_trakt_rotpost_other &);
	BOOL traktRotpostOtcExists(int trakt_id);
	BOOL categoryExists(CTransaction_sample_tree_category &);



public:
	CUMEstimateDB(void);
	CUMEstimateDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMEstimateDB(DB_CONNECTION_DATA &db_connection);

	BOOL getTraktTypes(vecTransactionTraktType &);
	BOOL addTraktType(CTransaction_trakt_type &);
	BOOL updTraktType(CTransaction_trakt_type &);
	BOOL removeTraktType(CTransaction_trakt_type &);

	BOOL getOrigins(vecTransactionOrigin &);
	BOOL addOrigin(CTransaction_origin &);
	BOOL updOrigin(CTransaction_origin &);
	BOOL removeOrigin(CTransaction_origin &);

	BOOL getInputMethods(vecTransactionInputMethod &);
	BOOL addInputMethod(CTransaction_input_method &);
	BOOL updInputMethod(CTransaction_input_method &);
	BOOL removeInputMethod(CTransaction_input_method &);

	BOOL getHgtClass(vecTransactionHgtClass &);
	BOOL addHgtClass(CTransaction_hgt_class &);
	BOOL updHgtClass(CTransaction_hgt_class &);
	BOOL removeHgtClass(CTransaction_hgt_class &);

	BOOL getCutClass(vecTransactionCutClass &);
	BOOL addCutClass(CTransaction_cut_class &);
	BOOL updCutClass(CTransaction_cut_class &);
	BOOL removeCutClass(CTransaction_cut_class &);

	// Handle Trakt and Trakt data; 070301 p�d
	BOOL getTraktIndex(vecTraktIndex &,BOOL select_all);
	BOOL getTrakts(vecTransactionTrakt &);
	BOOL getTrakt(int trakt_id,CTransaction_trakt &);
	BOOL addTrakt(CTransaction_trakt &);
	BOOL addTrakt_Soderbergs_Specifics(int,BOOL,BOOL,BOOL,BOOL,LPCTSTR);
	BOOL updTrakt(CTransaction_trakt &);

	BOOL updTrakt_Soderbergs_Specifics(int,BOOL,BOOL,BOOL,BOOL,LPCTSTR);
	BOOL removeTrakt(CTransaction_trakt &);
	BOOL resetTraktIdentityField(void);
	int getLastTraktIdentity(void);	// Added 070903 p�d
	int getNumOfRecordsInTrakt(void);
	int getNumOfPlotsInTrakt(int trakt_id);
	BOOL removePropTraktConnection(int trakt_id);

	BOOL getTraktData(vecTransactionTraktData &,int trakt_id = -1,int data_type = -1);
	BOOL getTraktData(CTransaction_trakt_data &,int trakt_id = -1,int spc_id = -1,int data_type = -1);
	BOOL addTraktData(CTransaction_trakt_data &);
	BOOL updTraktData(CTransaction_trakt_data &);
	BOOL updTraktData_m3fub(CTransaction_trakt_data &);
	BOOL resetTraktData(int trakt_id);
	BOOL removeTraktData(int trakt_id);
	BOOL removeTraktData(CTransaction_trakt_data &);
	BOOL removeTraktDataSpecie(CTransaction_trakt_data &);
	BOOL removeTraktDataSpecies(int trakt_id,LPCTSTR sql);
	int getNumOfRecordsInTraktData(void);

	BOOL getTraktAss(vecTransactionTraktAss &);
	BOOL getTraktAss(CTransaction_trakt_ass &,CTransaction_trakt_ass);
	BOOL getTraktAss(CTransaction_trakt_ass &,int trakt_id,int trakt_data_id,int data_type,int assort_id);
	BOOL getTraktAss(vecTransactionTraktAss &,int trakt_id);
	BOOL getTraktAss_exclude(vecTransactionTraktAss &,int);
	BOOL addTraktAss(CTransaction_trakt_ass &);
	BOOL updTraktAss(CTransaction_trakt_ass &);
//	BOOL updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price,int price_in);
	BOOL updTraktAss_kr_per_m3(int trakt_id,int trakt_data_id,int data_type,int assort_id,double m3fub_value,double m3to_value,double kr_per_m3);
	BOOL resetTraktAss(int trakt_id);
	BOOL removeTraktAss(CTransaction_trakt_ass &);
	BOOL removeTraktAss(int trakt_id);
	BOOL removeTraktAss(int trakt_id,int trakt_data_id);
	BOOL recalcTraktAss_on_k3_per_m3(int trakt_id);
	int getNumOfRecordsInTraktAss(void);

	// Added 2007-06-01 p�d
	BOOL getTraktTrans(vecTransactionTraktTrans &,int trakt_id = -1);
	BOOL getTraktTrans(CTransaction_trakt_trans &,CTransaction_trakt_trans);
	BOOL addTraktTrans(CTransaction_trakt_trans &);
	BOOL updTraktTrans(CTransaction_trakt_trans &);
	BOOL removeTraktTrans(CTransaction_trakt_trans &);
	BOOL removeTraktTrans(int trakt_data_id,int trakt_id);
	BOOL removeTraktTrans(int trakt_id);
	BOOL removeTraktTrans2(int trakt_id,int spc_id);

	BOOL getTraktSetSpc(vecTransactionTraktSetSpc &,int trakt_id = -1);
	BOOL getTraktSetSpc(CTransaction_trakt_set_spc &,CTransaction_trakt_set_spc);
	BOOL addTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_volfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_volfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpcMiscData(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpcMiscData(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_grot(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_grot(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_default_h25(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_default_h25(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_default_greencrown(CTransaction_trakt_set_spc &);	// #4555: Standard gr�nkroneandel
	BOOL updTraktSetSpc_default_greencrown(CTransaction_trakt_set_spc &);	// #4555: Standard gr�nkroneandel
	BOOL addTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_qdesc_all(CTransaction_trakt_set_spc &);

	BOOL removeTraktSetSpc(CTransaction_trakt_set_spc &);
	int getNumOfRecordsInTraktSetSpc(void);

	BOOL getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &);
	BOOL addTraktMiscData_prl(CTransaction_trakt_misc_data &);
	BOOL updTraktMiscData_prl(CTransaction_trakt_misc_data &);
	BOOL addTraktMiscData_dcls(CTransaction_trakt_misc_data &);
	BOOL updTraktMiscData_dcls(CTransaction_trakt_misc_data &);
	BOOL addTraktMiscData_costtmpl(CTransaction_trakt_misc_data &);
	BOOL updTraktMiscData_costtmpl(CTransaction_trakt_misc_data &);
	BOOL delTraktMiscData(int trakt_id);
	BOOL delTraktMiscData(CTransaction_trakt_misc_data &);

	// Plot (Group); 070510 p�d
	BOOL getPlots(int trakt_id,vecTransactionPlot &);
	BOOL getPlot(int trakt_id,int plot_id,CTransaction_plot &);
	BOOL addPlot(CTransaction_plot &);
	BOOL addPlotType(int id,int trakt_id,int plot_type);
	BOOL updPlot(CTransaction_plot &);
	BOOL updPlotEx(CTransaction_plot &);
	BOOL updPlotType(int trakt_id,int plot_type);
	BOOL delPlot(CTransaction_plot &);
	BOOL delPlot(int trakt_id);

	// Sampletrees; 070510 p�d
	BOOL getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &,BOOL clear_vec = TRUE);
	BOOL getSampleTree(CTransaction_sample_tree &,CTransaction_sample_tree);
	BOOL getSampleTree_date(CString &date,CTransaction_sample_tree);
	BOOL addSampleTrees(CTransaction_sample_tree &);
	BOOL updSampleTrees(CTransaction_sample_tree &);
	BOOL resetSampleTrees(int trakt_id);
	BOOL delSampleTreesInTrakt(int trakt_id,int tree_type);
	BOOL delSampleTree(int tree_id,int trakt_id);
	int getMaxSampleTreeID(int trakt_id);
	BOOL updSampleTrees_age(int trakt_id,int from_age,int to_age);
	BOOL updSampleTrees_bonitet(int trakt_id,LPCTSTR from_bonitet,LPCTSTR to_bonitet);
	BOOL GetCategories(vecTransactionSampleTreeCategory &vec);
	BOOL getCategories(vecTransactionSampleTreeCategory &);
	BOOL addCategory(CTransaction_sample_tree_category &);
	BOOL updCategory(int old_id, CTransaction_sample_tree_category &);
	BOOL removeCategory(CTransaction_sample_tree_category &);
	BOOL removeAllCategories();
	BOOL updateTransportationToIndustriForRotPost(int rId,int distance); //#5026 PH 20160616
	BOOL updateTransportationToVagForRotPost(int rId,int distance); //#5026 PH 20160616

	// DCLStrees; 070510 p�d
	BOOL getDCLSTrees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLSTree(CTransaction_dcls_tree &,CTransaction_dcls_tree);
	BOOL addDCLSTrees(CTransaction_dcls_tree &);
	BOOL addDCLSTreesWithoutNumOfCheck(CTransaction_dcls_tree &);
	BOOL updDCLSTrees(CTransaction_dcls_tree &);
	BOOL updDCLSRandTrees(CTransaction_dcls_tree &);
	BOOL resetDCLSTrees(int trakt_id);
	BOOL resetDCLSTrees_tree_type(int trakt_id);
	BOOL delDCLSTreesInTrakt(int trakt_id);
	BOOL delDCLSTreesInTrakt_plot(int trakt_id,int plot_id);
	BOOL delDCLSTree(int tree_id,int trakt_id);
	BOOL delSampleTreesInDCLS(double dcls_from, double dcls_to, int trakt_id, int plot_id, int spc_id);
	int getNumSampleTreesInDCLS(double dcls_from, double dcls_to, int trakt_id, int plot_id, int spc_id);
	int getMaxDCLSTreeID(int trakt_id);
	double getMaxDCLS(int trakt_id,int spc_id = -1);
	double getDCLSMaxTreeHeightForSpc(int trakt_id,int spc_id);

	
	//#4308 20150603 J�
	//Kontrollera om diameterklassen finns i st�mplingsl�ngd uttag
	BOOL traktDCLSTreeExistsInClass(CTransaction_sample_tree &data);	
	
	//Minska antalet i en diameterklass uttag
	BOOL updRemoveFromDclsUttag(CTransaction_sample_tree &rec);
	
	//�ka antalet tr�d i en diameterklass kvarvarande	
	BOOL updAddNumOfToDclsRemain(CTransaction_sample_tree &data);
	//�ka antalet tr�d i en diameterklass uttag
	BOOL updAddNumOfToDclsUttag(CTransaction_sample_tree &data);
	//#4465 ut�ka antal tr�d i en diameterklass uttag
	BOOL updIncNumOfToDclsUttag(CTransaction_dcls_tree &data);	
	//#4465 ut�ka antal tr�d i en diameterklass kvarl�mnat
	BOOL updIncNumOfToDclsRemain(CTransaction_dcls_tree &data);
	//�ka antalet kanttr�d i en diameterklass uttag
	BOOL updAddNumKanttradToDclsUttag(CTransaction_sample_tree &data);
	
	BOOL updRemoveNumKanttradFromDclsUttag(CTransaction_sample_tree &data);

	//Kontrollera om diameterklassen finns i st�mplingsl�ngd kvarvarande
	BOOL traktDCLS1TreeExistsInClass(CTransaction_sample_tree &data);
	//Minska antalet i en diameterklass kvarvarande
	BOOL updRemoveFromDclsRemain(CTransaction_sample_tree &data);
	//�ka antalet kanttr�d i en diameterklass kvarvarande
	//BOOL updAddNumKanttradToDclsRemain(CTransaction_sample_tree &data);

	BOOL delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsRemain(CTransaction_sample_tree &data);
	BOOL delete_IfZeroNumOf_and_ZeroNumOfRantTrees_Remove_DclsUttag(CTransaction_sample_tree &data);

	// DCLS1trees; 070510 p�d
	BOOL getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS1Tree(CTransaction_dcls_tree &,CTransaction_dcls_tree);
	BOOL addDCLS1Trees(CTransaction_dcls_tree &);
	BOOL updDCLS1Trees(CTransaction_dcls_tree &);
	BOOL resetDCLS1Trees(int trakt_id);
	BOOL delDCLS1TreesInTrakt(int trakt_id);
	BOOL delDCLS1TreesInTrakt_plot(int trakt_id,int plot_id);
	BOOL delDCLS1Tree(int tree_id,int trakt_id);
	int getMaxDCLS1TreeID(int trakt_id);

	// DCLS2trees; 070510 p�d
	BOOL getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS2Tree(CTransaction_dcls_tree &,CTransaction_dcls_tree);
	BOOL addDCLS2Trees(CTransaction_dcls_tree &);
	BOOL updDCLS2Trees(CTransaction_dcls_tree &);
	BOOL resetDCLS2Trees(int trakt_id);
	BOOL delDCLS2TreesInTrakt(int trakt_id);
	BOOL delDCLS2TreesInTrakt_plot(int trakt_id,int plot_id);
	BOOL delDCLS2Tree(int tree_id,int trakt_id);
	int getMaxDCLS2TreeID(int trakt_id);

	int getSumOfDCLSTrees_plot(int trakt_id,int plot_id);

	BOOL delSampleTreesInTrakt_plot(int trakt_id,int plot_id);
	
	// Intermidiate function for Smaple- and DCLS-trees; 080310 p�d
	BOOL getNumOfRandTreesPerSpcAndDCLS(int,vecRandTreeCounter &);
	BOOL updNumOfRandTreesPerSpcAndDCLS(int,vecRandTreeCounter &);
	BOOL getNoNumOfRandTreesPerSpcAndDCLS(int,double,vecRandTreeCounter &);
	BOOL addNoNumOfRandTreesPerSpcAndDCLS(int,vecRandTreeCounter &);

	// Assortments for Tree; 070510 p�d
	BOOL getAssTrees(int trakt_id,vecTransactionTreeAssort &);
	BOOL getAssTrees(int trakt_id,vecTransactionTreeAssort &,int);
	BOOL getAssTree(CTransaction_tree_assort rec,CTransaction_tree_assort &rec1);
	BOOL addAssTree(CTransaction_tree_assort &);
	BOOL updAssTree(CTransaction_tree_assort &);
	BOOL delAssTree(CTransaction_tree_assort &);
	BOOL delAssTree(int trakt_id);

	// "Rotpost"; 071011 p�d
	BOOL getRotpost(vecCTransaction_trakt_rotpost &);
	BOOL getRotpost(vecCTransaction_trakt_rotpost &,int);
	BOOL getRotpost(CTransaction_trakt_rotpost &,int);
	BOOL addRotpost(CTransaction_trakt_rotpost &);
	BOOL updRotpost(CTransaction_trakt_rotpost &);
	BOOL delRotpost(CTransaction_trakt_rotpost &);

	// "Rotpost Transport och Huggningskostnader per tr�dslag"; 071011 p�d
	BOOL getRotpostSpc(vecTransaction_trakt_rotpost_spc &);
	BOOL getRotpostSpc(vecTransaction_trakt_rotpost_spc &,int);
	BOOL getRotpostSpc(CTransaction_trakt_rotpost_spc &,int,int);
	BOOL addRotpostSpc(CTransaction_trakt_rotpost_spc &);
	BOOL updRotpostSpc(CTransaction_trakt_rotpost_spc &);
	BOOL delRotpostSpc(int trakt_id);

	// "Rotpost �vriga kostnader"; 080312 p�d
	BOOL getRotpostOtc(vecTransaction_trakt_rotpost_other &);
	BOOL getRotpostOtc(vecTransaction_trakt_rotpost_other &,int);
	BOOL getRotpostOtc(CTransaction_trakt_rotpost_other &,int,int);
	BOOL addRotpostOtc(CTransaction_trakt_rotpost_other &);
	BOOL updRotpostOtc(CTransaction_trakt_rotpost_other &);
	BOOL delRotpostOtc(int trakt_id);

	// External database item.
	// Properties is set in Forrest SUITE; 070305 p�d
	BOOL getProperties(vecTransactionProperty &);
	BOOL getProperties(LPCTSTR,vecTransactionProperty &);
	BOOL getSpecies(vecTransactionSpecies &);

	// External database item.
	// Pricelist is set in UMPricelists; 070305 p�d
	BOOL getPricelists(vecTransactionPricelist &);

	// External database item.
	// Templates set in UMTemplates; 070903 p�d
	BOOL getTemplates(vecTransactionTemplate &vec,int tmpl_type);

	BOOL getCostTmpls(vecTransaction_costtempl &);
	
	//Lagt till function f�r att plocka alla kostnader fr�n kostnadstabellen oavsett typ
	// Bug #2368 20101011 J�
	BOOL getAllCostTmpls(vecTransaction_costtempl &vec);

	// Special function combining 2 or more tables; 071024 p�d
	BOOL getExchPriceCalulatedInExchangeModule(int trakt_id,vecExchCalcInfo &vec);

	BOOL isStandInObjectCruise(int ecru_id);

	BOOL isTraktConnectedToShape(int trakt_id);

	// Check if the image heightcurve is in DB; 100603 p�d
	int isHeightCurveInDB(int trakt_id);

//---------------------------------------------------------------------------
//	DATASECURITY HANDLING; 090317 p�d
// Client/Server specifics; 090317 p�d
	short checkTraktDate(CTransaction_trakt rec,CString& changed_by,CString& date_done);
	short checkTraktDataDate(CTransaction_trakt_data rec,int data_type,CString& date_done);
	short checkTraktAssDate(CTransaction_trakt_ass rec,int data_type,CString& date_done);
	short checkTraktTransDate(CTransaction_trakt_trans rec,int data_type,CString& date_done);
	short checkTraktSetSpcDate(CTransaction_trakt_set_spc rec,int data_type,CString& date_done);
	short checkTraktMiscDataDate(CTransaction_trakt_misc_data rec,CString& date_done);
	short checkTraktPlotDate(CTransaction_plot rec,CString& date_done);
	short checkTraktSampleTreesDate(CTransaction_sample_tree rec,CString& date_done);
	short checkTraktDCLSTreesDate(CTransaction_dcls_tree rec,CString& date_done);
	short checkTraktDCLS1TreesDate(CTransaction_dcls_tree rec,CString& date_done);
	short checkTraktDCLS2TreesDate(CTransaction_dcls_tree rec,CString& date_done);
//---------------------------------------------------------------------------

	BOOL saveImage(CString path, int id);
	BOOL loadImage(CString path,int id);

};

#endif