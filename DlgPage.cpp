// DlgPage.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "DlgPage.h"


// CDlgPage dialog

IMPLEMENT_DYNAMIC(CDlgPage, CDialog)
CDlgPage::CDlgPage(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPage::IDD, pParent)
{
}

CDlgPage::~CDlgPage()
{
}

void CDlgPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgPage, CDialog)
END_MESSAGE_MAP()

