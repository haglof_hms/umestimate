#pragma once

#include "Resource.h"

#include "UMEstimateDB.h"
#include "OXMaskedEdit.h"    // COXMaskedEdit

#include "DatePickerCombo.h"
#include "afxwin.h"

// CPageOneFormView form view

class CPageOneFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPageOneFormView)
//private:
	BOOL m_bInitialized;
	BOOL m_bIsDirty;
	BOOL m_bSetFocusOnInitDone;
	//BOOL m_bShowOnlyOneStand;
	BOOL m_bIsEnabledData;

	// Strings used in removeTrakt()
	CString m_sRemoveCapMsg;
	CString m_sRemoveMsg1;
	CString m_sExplRemoveMsg;
	CString m_sRemoveMsg2;
	CString m_sArealLimitsMsg1;
	CString m_sArealLimitsMsg2;
	CString m_sArealLimitsMsg3;
	CString m_sCouldNotFindStandMsg;

	CString m_sCaptionMsg;
	CString m_sSaveMsg;
	CString m_sSaveMsg1;	// Tell user that a Traktnumber must be entered; 070618 p�d
	CString m_sSavedMsg;
	CString m_sShouldSaveMsg;
	CString m_sRemovePropSTandConenctionMsg;
	CString m_sTraktNumOf;

	CString m_sTraktNum;
	CString m_sTraktPNum;
	CString m_sTraktName;

	CString m_sNoTraktMsg;

	CString m_sOK;
	CString m_sCancel;

	CString m_sInfringe;

	CString m_sMainWindowCaption;

	enumACTION m_enumAction;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sTemplateXMLFile;

	long m_nDBIndex;
	int m_nTraktId;
	int savedTraktId; //#5026 PH 20160620
	void populateData(long idx);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);
	
	void setLanguage(void);

protected:
	// Components on formview
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLbl13;
	CMyExtStatic m_wndLbl14;
	CMyExtStatic m_wndLbl15;
	CMyExtStatic m_wndLbl16;
	CMyExtStatic m_wndLbl17;
	CMyExtStatic m_wndLbl19;
	CMyExtStatic m_wndLbl20;
	CMyExtStatic m_wndLbl21;
	CMyExtStatic m_wndLbl22;
	CMyExtStatic m_wndLbl23;
	CMyExtStatic m_wndLbl24;
	CMyExtStatic m_wndLbl25;
	CMyExtStatic m_wndLbl27;
	CMyExtStatic m_wndLbl28;
	CMyExtStatic m_wndLbl29;
	CMyExtStatic m_wndLbl30;
	CMyExtStatic m_wndLabelTransport;
	CMyExtStatic m_wndLabelTillVag;
	CMyExtStatic m_wndLabelTillIndustri;

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;
	CXTResizeGroupBox m_wndGroup3;
	CXTResizeGroupBox m_wndGroup4;
	CXTResizeGroupBox m_wndGroup5;
	CXTResizeGroupBox m_wndGroup6;
	CXTResizeGroupBox m_wndGroup7;
	CXTResizeGroupBox m_wndGroup8;
	CXTResizeGroupBox m_wndGroup9;
	CXTResizeGroupBox m_wndGroup10;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;
	CMyExtEdit m_wndEdit12;
	CMyExtEdit m_wndEdit13;
	CMyExtEdit m_wndEdit14;
//	CMyExtEdit m_wndEdit15;
	COXMaskedEdit m_wndEdit15;
	CMyExtEdit m_wndEdit16;
	CMyExtEdit m_wndEdit18;
	CMyExtEdit m_wndEdit19;
	CMyExtEdit m_wndEdit20;
	CMyExtEdit m_wndEdit21;
	CMyExtEdit m_wndEdit22;
	CMyExtEdit m_wndEdit23;
	CMyExtEdit m_wndEdit24;
	CMyExtEdit m_wndEditTransportTillVag;
	CMyExtEdit m_wndEditTransportTillIndustri;

	CMyExtStatic m_wndLblLength;
	CMyExtStatic m_wndLblWidth;

	CMyExtEdit m_wndEditLength;
	CMyExtEdit m_wndEditWidth;

	CButton m_wndCheck1;
	CButton m_wndCheck2;
	CButton m_wndCheck3;
	CButton m_wndCheck4;
	CButton m_wndCheck5;
	CButton m_wndCheckTillfUtnyttj;
	CButton m_wndBtnTransportSpara;

	CMyDatePickerCombo m_wndDateTimeCtrl;
	CMyComboBox m_wndCBoxTraktTypeCtrl;
	CMyComboBox m_wndCBoxOriginCtrl;
	CMyComboBox m_wndCBoxInputDataCtrl;
	CMyComboBox m_wndCBoxHeightClassCtrl;
	CMyComboBox m_wndCBoxCutClassCtrl;

	CXTButton m_wndBtnOpenPropWnd;
	CXTButton m_wndBtnRemovePropWnd;
	//CXTButton m_wndBtnCalc1;
	CXTButton m_wndBtnArea;

 	vecTraktIndex m_vecTraktIndex;

	vecTransactionTrakt m_vecTrakt;
	CTransaction_trakt m_recTraktNew;
	CTransaction_trakt m_recTraktActive;
	void getTrakts(void);
	void getTrakt(int trakt_id,CTransaction_trakt &rec);	// Get specific trakt data; 071207 p�d
	void getTraktIndex(void);
	void getTraktIndex_ObjectSelect(void);
	
	vecTransactionTraktType m_vecTraktTypeData;
	vecTransactionOrigin m_vecOriginData;
	vecTransactionInputMethod m_vecInputMethodData;
	vecTransactionHgtClass m_vecHgtClassData;
	vecTransactionCutClass m_vecCutClassData;

	vecTransactionProperty m_vecProperty;
	void getProperties(void);

	BOOL getPricelistBasedOnTemplateInformation(int,int,CTransaction_pricelist&);
	BOOL getCostsTmplBasedOnTemplateInformation(int,LPCTSTR costs_tmpl_name,CTransaction_costtempl&);

	vecTransactionTemplate m_vecTemplates;
	void getTemplates(void);

	// Added 070903 p�d
	// Traktnumber based on last identity + 1; 070903 p�d
	CString getNextTraktNumber(void);

	// Database connection datamemebers; 070228 p�d
	CUMEstimateDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void resetIsDirty(void);
	BOOL getIsDirty(void);
	BOOL getNeedToRecalculate(void);

	void clearAll();
	void setEnabledData(BOOL enable);

	BOOL getEnteredData(int action);

	vecTransactionTraktAss m_vecTraktAss;
	void getTraktAssFromDB(void);

	//void saveTraktAssTrans(int,int,int,LPCTSTR,vecTransactionTemplateTransfers &);

	BOOL createTraktFromTemplate(void);	// Added 070903 p�d
	BOOL createTraktNoTemplate(void);	// Added 070903 p�d

	void setPageTwoTabOnTrakt(void);
	void setPageThreeTabOnTrakt(void);
	void setPageFourTabOnTrakt(void);
	void setShowSettingsPane(void);

	void setupOnNoTraktData(BOOL add_trakt = TRUE);

	CString setPricelistName(CTransaction_pricelist &);

public:
	// Need to be PUBLIC
	BOOL isHasDataChangedPageOne(int action = 1,BOOL show_pane_settings = TRUE);

	BOOL removeTrakt(void);

	BOOL addTrakt(void);
	BOOL saveTrakt(int action,BOOL show_pane_settings = TRUE);

	void doPropertyPopulate(UINT);
	void doTraktPopulate(UINT);

	// This method reloads trakt data
	// and populates trakt with Last entered
	// trakt; 071001 p�d
	// Populate active trakt; 080407 p�d
	void doPopulateData(void);

	// This method reloads trakt data
	// and populates trakt with the index; 080820 p�d
	void doPopulateData(int trakt_id);

	// Set navigationbuttons, after e.g. ,odule for
	// INV-files have been used; 080304 p�d
	void doSetNavigationButtons(void)	{	setNavigationButtons(m_nDBIndex > 0, (m_nDBIndex) < m_vecTraktIndex.size() - 1);	}

	// This method's called in getActiveTrakt() (set in StdAfx); 070515 p�d
	CTransaction_trakt &getActiveTraktRecord(void)	{	return m_recTraktActive;	}

	// Get num of stands; 090520 p�d
	long getNumOfStands(void)	{ return m_vecTraktIndex.size(); }

	// This method reloads a specific Trakt from DB; 071207 p�d
	void reloadTrakt(int trakt_id)	{		getTrakt(trakt_id,m_recTraktActive);	}

	// Used when creating a stand from GIS
	void SetLatLongArea(int lat, int lon, float area) { m_wndEdit11.setInt(lat); m_wndEdit12.setInt(lon); m_wndEdit8.setFloat(area,3); }

	BOOL isCruiseInObject(int ecru_id);

	// Get m_enumAction set; 070618 p�d
	enumACTION getAction(void)	{		return m_enumAction;	}

	void setArealOnForm(double v)	{ m_wndEdit8.setFloat(v,3); }

	int getTraktId();

	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CPageOneFormView();           // protected constructor used by dynamic creation
	virtual ~CPageOneFormView();
protected:
	void getTableData(void);
	void setTableData(void);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnChangeHandledAreal(void);
	afx_msg void OnChangeTraktLengthOrWidth(void);
	afx_msg void OnChangeConsiderdAreal(void);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnCbnSelchangeCboxDatepicker();
	afx_msg void OnBnClickedBtnPropwnd();
	afx_msg void OnBnClickedBtnPropwnd2();
	afx_msg void OnEnChangeEdit11();
	afx_msg void OnEnChangeEdit12();
	
	// #4205 150114 Tagit bort och lagt l�ngd och bredd under intr�ngsinfo inst�llet
	//afx_msg void OnBnClickedBtnCalc11();
	afx_msg void OnBnClickedBtnArea();
	afx_msg void OnEnChangeEdit24();
	afx_msg void OnBnClickedBtnTransportSpara();
};


