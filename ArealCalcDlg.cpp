// ArealCalcDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "ArealCalcDlg.h"

#include "ResLangFileReader.h"

// CArealCalcDlg dialog

IMPLEMENT_DYNAMIC(CArealCalcDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CArealCalcDlg, CXTResizeDialog)
	ON_EN_CHANGE(IDC_EDIT10_1, &CArealCalcDlg::OnEnChangeEdit101)
	ON_EN_CHANGE(IDC_EDIT10_2, &CArealCalcDlg::OnEnChangeEdit102)
END_MESSAGE_MAP()

CArealCalcDlg::CArealCalcDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CArealCalcDlg::IDD, pParent),
	m_fAreal(0.0)
{

}

CArealCalcDlg::~CArealCalcDlg()
{
}

void CArealCalcDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)
	DDX_Control(pDX, IDC_LBL10_1, m_wndLbl10_1);
	DDX_Control(pDX, IDC_LBL10_2, m_wndLbl10_2);
	DDX_Control(pDX, IDC_LBL10_3, m_wndLbl10_3);
	DDX_Control(pDX, IDC_LBL10_4, m_wndLbl10_4);

	DDX_Control(pDX, IDC_EDIT10_1, m_wndEdit10_1);
	DDX_Control(pDX, IDC_EDIT10_2, m_wndEdit10_2);
	DDX_Control(pDX, IDC_EDIT10_3, m_wndEdit10_3);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

BOOL CArealCalcDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	m_wndEdit10_1.SetRange(0,10000);
	m_wndEdit10_2.SetRange(0,10000);

	m_wndEdit10_3.SetEnabledColor(BLACK,INFOBK);
	m_wndEdit10_3.SetDisabledColor(BLACK,INFOBK);
	m_wndEdit10_3.SetReadOnly();


	m_wndOKBtn.EnableWindow(FALSE);
	CString sLangFN = L"";

	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{

			SetWindowText(xml.str(IDS_STRING7000));
			m_wndLbl10_1.SetWindowText(xml.str(IDS_STRING7002));
			m_wndLbl10_2.SetWindowText(xml.str(IDS_STRING7003));
			m_wndLbl10_3.SetWindowText(xml.str(IDS_STRING7004));
			m_wndLbl10_4.SetWindowText(xml.str(IDS_STRING7001));

			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING7005));
			m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING156));

			xml.clean();
		}
	}
	return TRUE;
}


// CArealCalcDlg message handlers

void CArealCalcDlg::OnEnChangeEdit101()
{
	double fLength = 0.0;
	double fWidth = 0.0;

	// Get values added for Length and Width; 120813 p�d
	fLength = m_wndEdit10_1.getFloat();
	fWidth = m_wndEdit10_2.getFloat();
	if (fLength > 0.0 && fWidth > 0.0)
	{
		m_fAreal = (fLength*fWidth)/10000.0;
		m_wndEdit10_3.setFloat(m_fAreal,3);
		m_wndOKBtn.EnableWindow(TRUE);
	}
	else
	{
		m_fAreal = 0.0;
		m_wndEdit10_3.setFloat(m_fAreal,3);
		m_wndOKBtn.EnableWindow(FALSE);
	}
}

void CArealCalcDlg::OnEnChangeEdit102()
{
	double fLength = 0.0;
	double fWidth = 0.0;

	// Get values added for Length and Width; 120813 p�d
	fLength = m_wndEdit10_1.getFloat();
	fWidth = m_wndEdit10_2.getFloat();
	if (fLength > 0.0 && fWidth > 0.0)
	{
		m_fAreal = (fLength*fWidth)/10000.0;
		m_wndEdit10_3.setFloat(m_fAreal,3);
		m_wndOKBtn.EnableWindow(TRUE);
	}
	else
	{
		m_fAreal = 0.0;
		m_wndEdit10_3.setFloat(m_fAreal,3);
		m_wndOKBtn.EnableWindow(FALSE);
	}
}
