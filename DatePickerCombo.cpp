// DatePickerCombo.cpp : implementation file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DatePickerCombo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDatePickerCombo

BEGIN_MESSAGE_MAP(CMyDatePickerCombo, CComboBox)
	//{{AFX_MSG_MAP(CMyDatePickerCombo)
	ON_CONTROL_REFLECT(CBN_DROPDOWN, OnDropDown)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_CTLCOLORLISTBOX, OnCtlColorListBox)
	ON_WM_CTLCOLOR()
	ON_WM_CHAR()
END_MESSAGE_MAP()

CMyDatePickerCombo::CMyDatePickerCombo()
{
	
  // to changes the colors dynamic
  m_brush = CreateSolidBrush(WHITE);
	m_pEdit = NULL;

#ifdef _USE_LOCALE_USERDEFAULT
	TCHAR szBuff[STR_LEN];
	
	GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SABBREVLANGNAME, szBuff, STR_LEN);
	if (szBuff == L"ENU" || szBuff == L"ENG")
		m_bIsYYMMDD = FALSE;
	else
		m_bIsYYMMDD = TRUE;
#else
	CString sBuff = L"";
	sBuff = getLangSet();
	if (sBuff.CompareNoCase(L"ENU") == 0 || sBuff.CompareNoCase(L"ENG") == 0)
		m_bIsYYMMDD = FALSE;
	else
		m_bIsYYMMDD = TRUE;
#endif
}

CMyDatePickerCombo::~CMyDatePickerCombo()
{
  DeleteObject(m_brush);
}

void CMyDatePickerCombo::setMaskEdit(CXTMaskEdit *edit)	
{ 
	m_pEdit = edit; 
	if (m_pEdit != NULL) 
	{
		m_pEdit->Invalidate(); 
#ifdef _USE_SHORT_YEAR	
		if (m_bIsYYMMDD)
		{
			m_pEdit->SetEditMask(L"00-00-00",L"__-__-__");
		}
		else
		{
			m_pEdit->SetEditMask(L"00/00/00",L"__/__/__");
		}
#else
		if (m_bIsYYMMDD)
		{
			m_pEdit->SetEditMask(L"0000-00-00",L"____-__-__");
		}
		else
		{
			m_pEdit->SetEditMask(L"00/00/0000",L"__/__/____");
		}
#endif
	}
}

void CMyDatePickerCombo::setDateInComboBox(void)
{
	CString sYear = L"";
	COleDateTime dt = COleDateTime::GetCurrentTime();

	sYear.Format(L"%d",dt.GetYear());
#ifdef _USE_SHORT_YEAR	
	if (m_bIsYYMMDD)
	{
		if (m_pEdit == NULL)
			m_sDateStr.Format(_T("%s-%02d-%02d"),sYear.Right(2),dt.GetMonth(),dt.GetDay());
		else
			m_sDateStr.Format(_T("%s%02d%02d"),sYear.Right(2),dt.GetMonth(),dt.GetDay());
	}
	else
	{
		if (m_pEdit == NULL)
			m_sDateStr.Format(_T("%02d/%02d/%s"), dt.GetDay(), dt.GetMonth(), sYear.Right(2));
		else
			m_sDateStr.Format(_T("%02d%02d%s"), dt.GetDay(), dt.GetMonth(), sYear.Right(2));
	}

	if (m_pEdit != NULL)
	{
		m_pEdit->SetMaskedText(m_sDateStr);
	}
	else
	{
		SetWindowText(m_sDateStr);
		SetEditSel(-1, -1);
	}
#else
	if (m_bIsYYMMDD)
	{
		if (m_pEdit == NULL)
			m_sDateStr.Format(_T("%s-%02d-%02d"),sYear,dt.GetMonth(),dt.GetDay());
		else
			m_sDateStr.Format(_T("%s%02d%02d"),sYear,dt.GetMonth(),dt.GetDay());
	}
	else
	{
		if (m_pEdit == NULL)
			m_sDateStr.Format(_T("%02d/%02d/%s"), dt.GetDay(), dt.GetMonth(), sYear);
		else
			m_sDateStr.Format(_T("%02d%02d%s"), dt.GetDay(), dt.GetMonth(), sYear);
	}

	if (m_pEdit != NULL)
	{
		m_pEdit->SetMaskedText(m_sDateStr);
	}
	else
	{
		SetWindowText(m_sDateStr);
		SetEditSel(-1, -1);
	}
#endif
}

void CMyDatePickerCombo::setDateInComboBox(LPCTSTR date)
{
	m_sDateStr = date;
	if (m_pEdit != NULL)
	{
		m_pEdit->SetMaskedText(m_sDateStr);
	}
	else
	{
		SetWindowText(m_sDateStr);
		SetEditSel(-1, -1);
	}
}

CString CMyDatePickerCombo::getYear()
{
	CString sDate = L"",sToken = L"";
	m_pEdit->GetWindowTextW(sDate);
	if (sDate.Find(L"_") > -1)
		sDate.Empty();
	if (m_bIsYYMMDD)
	{
		sDate.Replace(L"-",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,0,';');
	}
	else
	{
		sDate.Replace(L"/",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,2,';');
	}

	return sToken;
}

CString CMyDatePickerCombo::getMonth()
{
	CString sDate = L"",sToken = L"";
	m_pEdit->GetWindowTextW(sDate);
	if (sDate.Find(L"_") > -1)
		sDate.Empty();
	if (m_bIsYYMMDD)
	{
		sDate.Replace(L"-",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,1,';');
	}
	else
	{
		sDate.Replace(L"/",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,1,';');
	}

	
	return sToken;
}

CString CMyDatePickerCombo::getDay()
{
	CString sDate = L"",sToken = L"";
	m_pEdit->GetWindowTextW(sDate);
	if (sDate.Find(L"_") > -1)
		sDate.Empty();
	if (m_bIsYYMMDD)
	{
		sDate.Replace(L"-",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,2,';');
	}
	else
	{
		sDate.Replace(L"/",L";");
		sDate += L";";
		AfxExtractSubString(sToken,sDate,0,';');
	}

	return sToken;
}

CString CMyDatePickerCombo::getDate() 
{ 
#ifdef _USE_SHORT_YEAR	
	if (m_bIsYYMMDD)
	{
		m_sDateStr.Format(L"%s-%s-%s",getYear().Right(2),getMonth(),getDay());
	}
	else
	{
		m_sDateStr.Format(L"%s/%s/%s",getMonth(),getDay(),getYear().Right(2));
	}
#else
	if (m_bIsYYMMDD)
	{
		m_sDateStr.Format(L"%s-%s-%s",getYear(),getMonth(),getDay());
	}
	else
	{
		m_sDateStr.Format(L"%s/%s/%s",getMonth(),getDay(),getYear());
	}
#endif

	return m_sDateStr; 
}

/////////////////////////////////////////////////////////////////////////////
// CMyDatePickerCombo message handlers



//////////////////////////////////////////////////////////////////////////
// Process combobox DropDown notification:
// Show DatePicker and format results.
//
void CMyDatePickerCombo::OnDropDown() 
{

	CString sYear = L"";
	// Identify size and coordinates of the popup DatePicker window
	CXTPWindowRect rcPopup(this);

	CXTPDatePickerControl wndDatePicker;
	
	// Ask DatePicker control about how much space we'd need 
	// to show one full month item.
	CRect rc;
	wndDatePicker.GetMinReqRect(&rc);
	
	// Make small size correction for pretty result
	rc.InflateRect(0 ,0, 4, 4);
	// Shift the prepared rectangle near to the ComboBox
	rc.OffsetRect(rcPopup.right - rc.Width(), rcPopup.bottom);
	
	// Enable some visual effects
	wndDatePicker.SetButtonsVisible(TRUE, FALSE);
	wndDatePicker.SetShowWeekNumbers(TRUE);
	wndDatePicker.SetBorderStyle(xtpDatePickerBorderOffice);
	wndDatePicker.GetButton(0)->SetCaption(m_sBtnText);
	CString strPopup;
	COleDateTime dtFrom;
	COleDateTime dtTo;

	// Run DatePicker on the prepared rectangle in the Modal mode.
	if (wndDatePicker.GoModal(rc, this))
	{

		// Retrieve the selection result from the DatePicker
		if (wndDatePicker.GetSelRange(dtFrom, dtTo))
		{
			sYear.Format(L"%d",dtFrom.GetYear());
			if (dtFrom == dtTo)
			{
				// When only 1 day has been selected, format string to show
				// it in the following way
#ifdef _USE_SHORT_YEAR	
				if (m_bIsYYMMDD)
				{
					if (m_pEdit == NULL)
						strPopup.Format(_T("%s-%02d-%02d"),sYear.Right(2), dtFrom.GetMonth(), dtFrom.GetDay());
					else
						strPopup.Format(_T("%s%02d%02d"),sYear.Right(2), dtFrom.GetMonth(), dtFrom.GetDay());
				}
				else
				{
					if (m_pEdit == NULL)
						strPopup.Format(_T("%02d/%02d/%s"),dtFrom.GetDay(), dtFrom.GetMonth(), sYear.Right(2));
					else
						strPopup.Format(_T("%02d%02d%s"),dtFrom.GetDay(), dtFrom.GetMonth(), sYear.Right(2));
				}
#else
				if (m_bIsYYMMDD)
				{
					if (m_pEdit == NULL)
						strPopup.Format(_T("%s-%02d-%02d"),sYear, dtFrom.GetMonth(), dtFrom.GetDay());
					else
						strPopup.Format(_T("%s%02d%02d"),sYear, dtFrom.GetMonth(), dtFrom.GetDay());
				}
				else
				{
					if (m_pEdit == NULL)
						strPopup.Format(_T("%02d/%02d/%s"),dtFrom.GetDay(), dtFrom.GetMonth(), sYear);
					else
						strPopup.Format(_T("%02d%02d%s"),dtFrom.GetDay(), dtFrom.GetMonth(), sYear);
				}
#endif
			} 

			m_sDateStr = strPopup;
			// Show retrieved result inside the ComboBox
			if (m_pEdit != NULL)
			{
				m_pEdit->SetMaskedText(strPopup);
			}
			else
			{
				SetWindowText(strPopup);
			}
		}
		else
		{
			if (m_pEdit != NULL)
			{
				m_pEdit->SetMaskedText(m_sDateStr);
			}
			else
			{
				SetWindowText(m_sDateStr);
			}
		}
	
	}

	// No selection
	if (m_pEdit == NULL)
		SetEditSel(-1, -1);
	
	// Hide the list box part of the ComboBox
	PostMessage(CB_SHOWDROPDOWN, FALSE);
	//ShowDropDown(FALSE);
}

LRESULT CMyDatePickerCombo::OnCtlColorListBox(WPARAM wParam, LPARAM lParam) 
{
	HWND hWnd = (HWND)lParam;

	if (hWnd != 0 && hWnd != m_hWnd) 
	{
		::ShowWindow(hWnd , SW_HIDE);
	}

  return DefWindowProc(WM_CTLCOLORLISTBOX, wParam, lParam);
}

HBRUSH CMyDatePickerCombo::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	pDC->SetTextColor(BLACK);
	pDC->SetBkColor(WHITE);
 
	HBRUSH hbr = CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);
  return hbr;
}

void CMyDatePickerCombo::EnableWindow(BOOL bEnable)
{
	m_bIsEnabled = bEnable;
	CComboBox::EnableWindow(bEnable);
	UpdateWindow();
}