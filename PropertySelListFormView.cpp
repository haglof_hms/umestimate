// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "StandEntryDoc.h"
#include "MDITabbedView.h"
#include "PropertySelListFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CPropertySelListFormView

IMPLEMENT_DYNCREATE(CPropertySelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertySelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
END_MESSAGE_MAP()

CPropertySelListFormView::CPropertySelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
}

CPropertySelListFormView::~CPropertySelListFormView()
{
}

void CPropertySelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getProperties();

	setupReport();

	LoadReportState();
}

BOOL CPropertySelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMEstimateDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CPropertySelListFormView::OnDestroy()
{
	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	m_vecPropertyData.clear();
	// Try to clear records on exit (for memory deallocation); 080215 p�d
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		pRecs->RemoveAll();
	}

	CXTPReportView::OnDestroy();	
}

BOOL CPropertySelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertySelListFormView diagnostics

#ifdef _DEBUG
void CPropertySelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPropertySelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPropertySelListFormView message handlers

// CPropertySelListFormView message handlers
void CPropertySelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CPropertySelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CPropertySelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING189)), 100));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING190)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
/*
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING191)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
*/
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING192)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING193)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING194)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING195)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
/*
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, (xml->str(IDS_STRING196)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, (xml->str(IDS_STRING197)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
*/

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnReorder(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnResize( TRUE );
					GetReportCtrl().GetReportHeader()->AllowColumnSort( FALSE );
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( TRUE );
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );

					populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPropertySelListFormView::populateReport(void)
{
	GetReportCtrl().GetRecords()->RemoveAll();
	for (UINT i = 0;i < m_vecPropertyData.size();i++)
	{
		CTransaction_property data = m_vecPropertyData[i];
		GetReportCtrl().AddRecord(new CPropertyReportDataRec(i,data));
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}


void CPropertySelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CPropertyReportDataRec *pRec = (CPropertyReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDITabbedView *pTabView = (CMDITabbedView *)getFormViewByID(ID_VIEW_5000);
		if (pTabView)
		{
			// Get View
			CPageOneFormView *pView = (CPageOneFormView *)pTabView->getPageOneFormView();
			if (pView)
			{
				pView->doPropertyPopulate(pRec->getRecord().getID());
			}	// if (pView)
		}	// if (pTabView)
	}	// if (pItemNotify->pRow)
}

// CPropertySelListFormView message handlers

void CPropertySelListFormView::getProperties(void)
{
	if (m_bConnected)	
	{
		if (m_pDB != NULL)
		{
			m_pDB->getProperties(m_vecPropertyData);
		}	// if (m_pDB != NULL)
	}
}

void CPropertySelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CPropertySelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

