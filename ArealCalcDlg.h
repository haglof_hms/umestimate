#pragma once

#include "Resource.h"

// CArealCalcDlg dialog

class CArealCalcDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CArealCalcDlg)

	CMyExtStatic m_wndLbl10_1;
	CMyExtStatic m_wndLbl10_2;
	CMyExtStatic m_wndLbl10_3;
	CMyExtStatic m_wndLbl10_4;

	CMyExtEdit m_wndEdit10_1;
	CMyExtEdit m_wndEdit10_2;
	CMyExtEdit m_wndEdit10_3;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	double m_fAreal;
public:
	CArealCalcDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CArealCalcDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG9 };


	double getAreal()	const { return m_fAreal; }
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
	afx_msg void OnEnChangeEdit101();
	afx_msg void OnEnChangeEdit102();
};
