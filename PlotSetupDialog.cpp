// PlotSetupDialog.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "PlotSetupDialog.h"
#include "PlotSelListFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CPlotReportRec

class CPlotReportRec : public CXTPReportRecord
{
	CTransaction_plot recPlot;
	int m_nGroupID;
	int m_nInvMethodIndex;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CPlotReportRec(void)
	{
		m_nGroupID = -1;
		m_nInvMethodIndex = -1;
		AddItem(new CIntItem(0));						// Plot (Group) ID
		AddItem(new CTextItem(_T("")));			// Inventory type
		AddItem(new CTextItem(_T("")));			// Name of Plot (Group)
	}

	CPlotReportRec(int group_id,int inv_method_idx,CTransaction_plot& rec)
	{
		m_nGroupID = group_id;
		m_nInvMethodIndex = inv_method_idx;
		recPlot = rec;
		// Setup columns based on Inventory method; 070514 p�d
		if (m_nInvMethodIndex == 1 || // "Totaltaxering"
			m_nInvMethodIndex == 5)		// "Laserscanning"
		{
			AddItem(new CIntItem((rec.getPlotID())));				// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));		// Name of Plot (Group)
			AddItem(new CTextItem(rec.getCoord()));						// Koordinater
		}
		else if (m_nInvMethodIndex == 2 ||	// "Cirkelytor"
						 m_nInvMethodIndex == 4)		// "Snabbtaxering"
		{
			AddItem(new CIntItem((rec.getPlotID())));					// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));			// Name of Plot (Group)
			AddItem(new CFloatItem(rec.getArea(),sz2dec ));			// Area (m2)
			AddItem(new CFloatItem(rec.getRadius(),sz1dec ));		// Radius (m)
			AddItem(new CIntItem((rec.getNumOfTrees())));	// HMS-134 20250116 J�
			AddItem(new CTextItem(rec.getCoord()));							// Koordinater			
		}
		else if (m_nInvMethodIndex == 3) // "Rektangul�ra"
		{
			AddItem(new CIntItem((rec.getPlotID())));						// Plot (Group) ID
			AddItem(new CTextItem((rec.getPlotName())));				// Name of Plot (Group)
			AddItem(new CFloatItem(rec.getArea(),sz2dec ));				// Area (m2)
			AddItem(new CFloatItem(rec.getLength1(),sz2dec ));		// Length (m)
			AddItem(new CFloatItem(rec.getWidth1(),sz2dec ));			// Width (m)
			AddItem(new CTextItem(rec.getCoord()));								// Koordinater
		}
	}

	CTransaction_plot& getRecord(void)
	{
		return recPlot;
	}

	int getGroupID(void)
	{
		return m_nGroupID;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};


// CPlotSetupDialog dialog

IMPLEMENT_DYNAMIC(CPlotSetupDialog, CXTResizeDialog)


BEGIN_MESSAGE_MAP(CPlotSetupDialog, CXTResizeDialog)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_GROUPS, OnReportValueChanged)
	ON_BN_CLICKED(IDC_BUTTON7_1, &CPlotSetupDialog::OnBnClickedAdd)
	ON_BN_CLICKED(IDC_BUTTON7_2, &CPlotSetupDialog::OnBnClickedRemove)
	ON_BN_CLICKED(IDOK, &CPlotSetupDialog::OnBnClickedOk)
END_MESSAGE_MAP()

CPlotSetupDialog::CPlotSetupDialog(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CPlotSetupDialog::IDD, pParent)
{
	m_bPlotDeleted = FALSE;
}

CPlotSetupDialog::CPlotSetupDialog(CUMEstimateDB *db)
	: CXTResizeDialog(CPlotSetupDialog::IDD, NULL),
	m_pDB(db)
{

}

CPlotSetupDialog::~CPlotSetupDialog()
{
}

void CPlotSetupDialog::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSpcSettingsDlg)
	DDX_Control(pDX, IDC_GROUP7_1, m_wndGroup1);

	DDX_Control(pDX, IDC_BUTTON7_1, m_wndBtnAdd);
	DDX_Control(pDX, IDC_BUTTON7_2, m_wndBtnRemove);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP

}

INT_PTR CPlotSetupDialog::DoModal()
{
	if( DisplayMsg() )
	{
		return CXTResizeDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL CPlotSetupDialog::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();


	m_nInvMethodIndex = getInvMethodIndex();

	setupReport();

	getPlots();

	populateReport();			

	return TRUE;
}

void CPlotSetupDialog::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
  XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pRow)
	{
		CPlotReportRec *pRec = (CPlotReportRec*)pItemNotify->pItem->GetRecord();
		// Depending on Inventory method, calculate Area if user enters data
		// into fields for Radius,Length and Width; 070514 p�d
		if ((m_nInvMethodIndex == 2 && pItemNotify->pColumn->GetIndex() == 2) ||	// "Cirkelytor"
				(m_nInvMethodIndex == 4 && pItemNotify->pColumn->GetIndex() == 2))		// "Snabbtaxering"
		{
			double fArea = pRec->getColumnFloat(2);
			double fRadius = 0.0;
			if (fArea > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fRadius = sqrt(fArea/M_PI);

				pRec->setColumnFloat(3,fRadius);

				m_bIsDirty = TRUE;
			}
		}
		else if ((m_nInvMethodIndex == 2 && pItemNotify->pColumn->GetIndex() == 3) ||	// "Cirkelytor"
	  				 (m_nInvMethodIndex == 4 && pItemNotify->pColumn->GetIndex() == 3))		// "Snabbtaxering"
		{
			double fRadius = pRec->getColumnFloat(3);
			double fArea = 0.0;
			if (fRadius > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fArea = pow(fRadius,2) * M_PI;

				pRec->setColumnFloat(2,fArea);

				m_bIsDirty = TRUE;
			}
		}
		else if (m_nInvMethodIndex == 3 && pItemNotify->pColumn->GetIndex() > 2)	// "Rektangul�r yta"
		{
			double fArea = 0.0;
			double fLength = pRec->getColumnFloat(3);
			double fWidth = pRec->getColumnFloat(4);
			if (fWidth > 0.0 && fLength > 0.0)
			{
				// Caluclate Area and add to Area-column; 070514 p�d
				fArea = fLength*fWidth;
				pRec->setColumnFloat(2,fArea);
				m_bIsDirty = TRUE;
			}
		}
	}	// if (pItemNotify->pRow)
}


// CPlotSetupDialog message handlers

void CPlotSetupDialog::getPlots(void)
{
	if (m_pDB != NULL)
	{
		CTransaction_trakt *pTrakt = getActiveTrakt();
		m_pDB->getPlots(pTrakt->getTraktID(),m_vecTraktPlot);
	}	// if (m_pDB != NULL)
}

void CPlotSetupDialog::savePlots(void)
{
	int nInvMethod = -1;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CXTPReportRow *pRow = NULL;
	// Make suew there's something to save; 070515 p�d
	if (pRows != NULL && m_pDB != NULL && pTrakt != NULL)
	{

		m_pDB->delPlot(pTrakt->getTraktID());

		m_wndReport.Populate();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CPlotReportRec *pRecord = (CPlotReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					nInvMethod = pRecord->getRecord().getPlotType();
					// Name of Group'll always entered; 070515 p�d
					// Only value needed for InvMethods 1 and 5; 070515 p�d
					pRecord->getRecord().setPlotName(pRecord->getColumnText(1));
					// Based on Inventory method (PlotType), set CTransaction_plot record
					// accordingly. E.g. values set in Reportgrid, we need to get from grid.
					// Only information on TraktID,PlotID and InventoryMethod can be
					// read directory from pRecord->getRecord(); 070515 p�d
					if (nInvMethod == 1)	// "Totaltaxering"
					{
						/*
						pRecord->getRecord().setArea(0.0);
						pRecord->getRecord().setRadius(0.0);
						pRecord->getRecord().setLength1(0.0);
						pRecord->getRecord().setWidth1(0.0);
						*/
						pRecord->getRecord().setCoord(pRecord->getColumnText(2));
					}
					
					if (nInvMethod == 2 ||	// "Cirkelytor"
							nInvMethod == 4)		// "Snabbtaxering"
					{
						pRecord->getRecord().setArea(pRecord->getColumnFloat(2));
						pRecord->getRecord().setRadius(pRecord->getColumnFloat(3));
						pRecord->getRecord().setCoord(pRecord->getColumnText(4));
						/*
						pRecord->getRecord().setLength1(0.0);
						pRecord->getRecord().setWidth1(0.0);
						*/
					}
					else if (nInvMethod == 3)	// "Rektangul�r"
					{
						pRecord->getRecord().setArea(pRecord->getColumnFloat(2));
						pRecord->getRecord().setLength1(pRecord->getColumnFloat(3));
						pRecord->getRecord().setWidth1(pRecord->getColumnFloat(4));
						pRecord->getRecord().setCoord(pRecord->getColumnText(5));
						/*
						pRecord->getRecord().setRadius(0.0);
						*/
					}

					if (!m_pDB->addPlot(pRecord->getRecord()))
						m_pDB->updPlot(pRecord->getRecord());
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

BOOL CPlotSetupDialog::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, ID_REPORT_GROUPS, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			// Set Caption for dialog; 091118 p�d
			SetWindowText(xml->str(IDD_REPORTVIEW4));

			m_wndBtnAdd.SetWindowText(xml->str(IDS_STRING279));
			m_wndBtnRemove.SetWindowText(xml->str(IDS_STRING288));
			m_wndBtnOK.SetWindowText(xml->str(IDS_STRING155));
			m_wndBtnCancel.SetWindowText(xml->str(IDS_STRING156));


			m_sMsgCap = xml->str(IDS_STRING151);
			m_sMsgRemove.Format(L"%s\n%s\n%s\n\n",
										xml->str(IDS_STRING6000),
										xml->str(IDS_STRING6001),
										xml->str(IDS_STRING6002));

			m_sPlot = xml->str(IDS_STRING6003);

			// Get text from languagefile; 061207 p�d
			if (m_wndReport.GetSafeHwnd() != NULL)
			{
				m_wndReport.ShowWindow( SW_NORMAL );
				m_nInvMethodIndex = getInvMethodIndex();
				// Setup columns in Report, depending on selected
				// Inverntory-method; 070514 p�d
				if (m_nInvMethodIndex == 1 ||  // "Totaltaxering"
								m_nInvMethodIndex == 5)			// "Laserscanning"
				{
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 200));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING287)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				} 
				else if (m_nInvMethodIndex == 2 ||	// "Cirkelytor"
								 m_nInvMethodIndex == 4)		// "Snabbtaxering"
				{
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 150));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING283)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING284)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;

					// HMS-134 20250116 J� visa antal tr�d p� ytan
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING2872)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING287)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;


				}
				else if (m_nInvMethodIndex == 3)	// "Rektangul�ra ytor"
				{
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING262)), 50));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING280)), 150));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING283)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING285)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING286)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING287)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				}

				m_wndReport.GetReportHeader()->AllowColumnRemove(TRUE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.AllowEdit(TRUE);

				CRect rect;
				GetClientRect(&rect);
				setResize(&m_wndReport,20,50,rect.right-40,rect.bottom-105,TRUE);
				}	// if (m_wndReport.GetSafeHwnd() != NULL)
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPlotSetupDialog::populateReport(void)
{
	int nInvMethodIndex = getInvMethodIndex();
	CTransaction_plot data;
	BOOL bEnable = TRUE;
	m_wndReport.GetRecords()->RemoveAll();
	getPlots();
	if (m_vecTraktPlot.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktPlot.size();i++)
		{
			CTransaction_plot data = m_vecTraktPlot[i];
			if (data.getPlotType() == nInvMethodIndex)
			{
				m_wndReport.AddRecord(new CPlotReportRec(data.getPlotID(),data.getPlotType(), data));
		
				// Criteria for enabling Toobar button AddPlot (group); 070515 p�d
				bEnable = (data.getPlotType() >= 1 && data.getPlotType() <= 5);
			}	// if (data.getPlotType() == nInvMethodIndex)
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	// Depending on Inventory-method and if there's any plots (groups) for
	// this Trakt, set AddPlot Toolbar button; 070515 p�d
/*
	CPlotSelListFrame *pFrame = (CPlotSelListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pFrame != NULL)
	{
		pFrame->setOnUpdateAddPlotTBBtnEnable(bEnable);
	}
*/
}

void CPlotSetupDialog::delPlot(void)
{
	int nNumOf = 0;
	CString sMsg;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	m_bPlotDeleted = FALSE;
	m_nSelectedTraktID_delete = -1;
	m_nSelectedPlotID_delete = -1;

	if (pRow != NULL)
	{
		CPlotReportRec *pRec = (CPlotReportRec *)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			nNumOf = m_pDB->getSumOfDCLSTrees_plot(pRec->getRecord().getTraktID(),pRec->getRecord().getPlotID());
			sMsg.Format(m_sMsgRemove,nNumOf);
			if (UMMessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
			{
				m_nSelectedTraktID_delete = pRec->getRecord().getTraktID();
				m_nSelectedPlotID_delete = pRec->getRecord().getPlotID();

				m_wndReport.RemoveRowEx(pRow);
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();

				m_bPlotDeleted = TRUE;
			}
		}	// if (pRecord != NULL && m_pDB != NULL)
	}	// if (pRow!= NULL)
}

void CPlotSetupDialog::addGroup(CTransaction_plot &rec)
{
	int nNumOfRows = 0,nMaxPlotID = 0;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		nNumOfRows = pRows->GetCount();
	}
	if (m_nInvMethodIndex == 1 ||	// "Totaltaxering"
		m_nInvMethodIndex == 5)		// "Laserscanning"
	{
		if (nNumOfRows == 0)
		{
			m_wndReport.AddRecord(new CPlotReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			// Get max PlotNumber entered; 100422 p�d
			for (int i = 0; i < pRows->GetCount();i++)
			{
				CXTPReportRow *pRow1 = (CXTPReportRow *)pRows->GetAt(i);
				if (pRow1)
				{
					CPlotReportRec *pRecord1 = (CPlotReportRec *)pRow1->GetRecord();
					if (pRecord1)
						nMaxPlotID = max(nMaxPlotID,pRecord1->getColumnInt(0));
				}
			}

			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotReportRec *pRecord = (CPlotReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nMaxPlotID+1,
																												rec.getTraktID(),
																												m_sPlot, //pRecord->getColumnText(1),
																												m_nInvMethodIndex,
																												0.0, //pRecord->getColumnFloat(2),
																												0.0,
																												0.0,
																												0.0,
																												_T(""),0,_T(""));
					m_wndReport.AddRecord(new CPlotReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	else 	if (m_nInvMethodIndex == 2)	// "Cirkalytor"
	{
		if (nNumOfRows == 0)
		{
			m_wndReport.AddRecord(new CPlotReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			// Get max PlotNumber entered; 100422 p�d
			for (int i = 0; i < pRows->GetCount();i++)
			{
				CXTPReportRow *pRow1 = (CXTPReportRow *)pRows->GetAt(i);
				if (pRow1)
				{
					CPlotReportRec *pRecord1 = (CPlotReportRec *)pRow1->GetRecord();
					if (pRecord1)
						nMaxPlotID = max(nMaxPlotID,pRecord1->getColumnInt(0));
				}
			}

			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotReportRec *pRecord = (CPlotReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nMaxPlotID+1,
																												rec.getTraktID(),
																												m_sPlot,//pRecord->getColumnText(1),
																												m_nInvMethodIndex,
																												pRecord->getColumnFloat(3),
																												0.0,0.0,
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					m_wndReport.AddRecord(new CPlotReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	else 	if (m_nInvMethodIndex == 3)	// "Rektangul�ra ytor"
	{
		if (nNumOfRows == 0)
		{
			m_wndReport.AddRecord(new CPlotReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			// Get max PlotNumber entered; 100422 p�d
			for (int i = 0; i < pRows->GetCount();i++)
			{
				CXTPReportRow *pRow1 = (CXTPReportRow *)pRows->GetAt(i);
				if (pRow1)
				{
					CPlotReportRec *pRecord1 = (CPlotReportRec *)pRow1->GetRecord();
					if (pRecord1)
						nMaxPlotID = max(nMaxPlotID,pRecord1->getColumnInt(0));
				}
			}

			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);	
			if (pRow != NULL)
			{
				CPlotReportRec *pRecord = (CPlotReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nMaxPlotID+1,
																												rec.getTraktID(),
																												m_sPlot, //pRecord->getColumnText(1),
																												m_nInvMethodIndex,
																												0.0,
																												pRecord->getColumnFloat(3),
																												pRecord->getColumnFloat(4),
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					m_wndReport.AddRecord(new CPlotReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d

	}
	else 	if (m_nInvMethodIndex == 4)	// "Snabbtaxering"
	{
		if (nNumOfRows == 0)
		{
			m_wndReport.AddRecord(new CPlotReportRec(getGroupID(),m_nInvMethodIndex,rec));
		}
		else	// Add a copy of previous row; 070514 p�d
		{
			// Get max PlotNumber entered; 100422 p�d
			for (int i = 0; i < pRows->GetCount();i++)
			{
				CXTPReportRow *pRow1 = (CXTPReportRow *)pRows->GetAt(i);
				if (pRow1)
				{
					CPlotReportRec *pRecord1 = (CPlotReportRec *)pRow1->GetRecord();
					if (pRecord1)
						nMaxPlotID = max(nMaxPlotID,pRecord1->getColumnInt(0));
				}
			}

			CXTPReportRow *pRow = (CXTPReportRow *)pRows->GetAt(nNumOfRows-1);
			if (pRow != NULL)
			{
				CPlotReportRec *pRecord = (CPlotReportRec *)pRow->GetRecord();
				if (pRecord != NULL)
				{
					CTransaction_plot rec_tmp = CTransaction_plot(nMaxPlotID+1,
																												rec.getTraktID(),
																												m_sPlot, //pRecord->getColumnText(1),
																												m_nInvMethodIndex,
																												pRecord->getColumnFloat(3),
																												0.0,0.0,
																												pRecord->getColumnFloat(2),
																												_T(""),0,_T(""));
					m_wndReport.AddRecord(new CPlotReportRec(pRecord->getGroupID(),m_nInvMethodIndex,rec_tmp));
				}	// if (pRecord != NULL)
			}	// if (pRow != NULL)
		}	// else	// Add a copy of previous row; 070514 p�d
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CPlotSetupDialog::OnBnClickedAdd()
{
	int nInvMethodIndex = getInvMethodIndex();
	int nGroupID = getGroupID();
	int nTraktID = getActiveTrakt()->getTraktID();
	addGroup(CTransaction_plot(nGroupID,nTraktID,_T(""),nInvMethodIndex,0.0,0.0,0.0,0.0,_T(""),0,_T("")));
}

void CPlotSetupDialog::OnBnClickedRemove()
{
	delPlot();
}

void CPlotSetupDialog::OnBnClickedOk()
{
	// Added 2010-06-28 p�d
	if (m_pDB && m_bPlotDeleted)
	{
		m_pDB->delDCLSTreesInTrakt_plot(m_nSelectedTraktID_delete,m_nSelectedPlotID_delete);
		m_pDB->delDCLS1TreesInTrakt_plot(m_nSelectedTraktID_delete,m_nSelectedPlotID_delete);
		m_pDB->delDCLS2TreesInTrakt_plot(m_nSelectedTraktID_delete,m_nSelectedPlotID_delete);
		m_pDB->delSampleTreesInTrakt_plot(m_nSelectedTraktID_delete,m_nSelectedPlotID_delete);
		m_pDB->delPlot(CTransaction_plot(m_nSelectedPlotID_delete,m_nSelectedTraktID_delete,L"",0,0.0,0.0,0.0,0.0,L"",0.0,L""));
	}
	savePlots();

	OnOK();
}
