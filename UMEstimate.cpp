// UMEstimate.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "StandEntryDoc.h"
#include "MDITabbedView.h"

#include "TraktTypeFrame.h"
#include "TraktTypeFormView.h"

#include "OriginFrame.h"
#include "OriginFormView.h"

#include "InputMethodFrame.h"
#include "InputMethodFormView.h"

#include "HgtClassFrame.h"
#include "HgtClassFormView.h"

#include "CutClassFrame.h"
#include "CutClassFormView.h"

#include "CategoriesFrame.h"
#include "CategoriesFormView.h"

#include "PropertySelListFormView.h"
#include "PricelistSelListFormView.h"
#include "PlotSelListFormView.h"
#include "SpecieDataDialog.h"
#include "TraktSelListFormView.h"

#include "InformationFormView.h"

#include "PrlViewAndPrint.h"

#include "CreateTables.h"

#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define __BUILD

#ifdef __BUILD
#define DLL_BUILD AFX_EXT_API
#else
#define DLL_BUILD __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines

static AFX_EXTENSION_MODULE UMEstimateDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Global variable, set if we opened from LandValue or NOT; 090910 p�d
bool global_SHOW_ONLY_ONE_STAND = false;
bool global_SHOW_ONLY_ONE_STAND_FROM_GIS = false;


/////////////////////////////////////////////////////////////////////////////////////////////////
// Languagefile Strings
StrMap global_langMap;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Global variable, set if run save; 090917 p�d
//bool global_STAND_SAVED = false;

// Added: EXPORTED function doDatabaseTables; 080131 p�d
extern "C" void AFX_EXT_API DoDatabaseTables(LPCTSTR);
// Added: EXPORTED function doDatabaseTables; 090525 p�d
extern "C" void AFX_EXT_API DoAlterTables(LPCTSTR);

// Added; EXPORTED function for remote calculation; 080116 p�d
extern "C" BOOL AFX_EXT_API remoteCalculation(void);

extern "C" void AFX_EXT_API InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMEstimate.DLL Initializing!\n");
		hInst = hInstance;
		g_hInstanceDLL = hInst;

		CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
		if (fileExists(sLangFN))
		{
			RLFReader xml;
			xml.LoadEx(sLangFN,global_langMap);
		}

		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMEstimateDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMEstimate.DLL Terminating!\n");
		hInst = NULL;

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMEstimateDLL);
	}
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void AFX_EXT_API InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMEstimateDLL);
	CString sLangFN=_T("");
	CString sVersion=_T("");
	CString sCopyright=_T("");
	CString sCompany=_T("");

	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	//if (!License()) return;

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(ID_VIEW_5000,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIStandEntryFormFrame),
			RUNTIME_CLASS(CMDITabbedView)));
	idx.push_back(INDEX_TABLE(ID_VIEW_5000,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDITraktTypeFrame),
			RUNTIME_CLASS(CTraktTypeFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIOriginFrame),
			RUNTIME_CLASS(COriginFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIInputMethodFrame),
			RUNTIME_CLASS(CInputMethodFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIHgtClassFrame),
			RUNTIME_CLASS(CHgtClassFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW6,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDICutClassFrame),
			RUNTIME_CLASS(CCutClassFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW7,suite,sLangFN,TRUE));


	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW10,
			RUNTIME_CLASS(CMDIPrlViewAndPrintDoc),
			RUNTIME_CLASS(CMDIPrlViewAndPrintFrame),
			RUNTIME_CLASS(CPrlViewAndPrint)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW10,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW_CATEGORIES,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CCategoriesFrame),
			RUNTIME_CLASS(CCategoriesFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW_CATEGORIES, suite, sLangFN, TRUE));


	///////////////////////////////////////////////////////////////////////////////////////
	// Doc\Views NOT to be displayed on HMSShell navigation bar.
	// I.e. don't include the Item in the vecIndex vector; 070123 p�d

	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW1, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CPropertySelectListFrame),
			RUNTIME_CLASS(CPropertySelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	idx.push_back(INDEX_TABLE(IDD_REPORTVIEW1,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW2, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CPricelistSelectListFrame),
			RUNTIME_CLASS(CPricelistSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	idx.push_back(INDEX_TABLE(IDD_REPORTVIEW2,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDISpecieDataFrame),
			RUNTIME_CLASS(CSpecieDataFormView)));

// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	idx.push_back(INDEX_TABLE(IDD_FORMVIEW8,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW3, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CTraktSelectListFrame),
			RUNTIME_CLASS(CTraktSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	idx.push_back(INDEX_TABLE(IDD_REPORTVIEW3,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW4, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CPlotSelListFrame),
			RUNTIME_CLASS(CPlotSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	idx.push_back(INDEX_TABLE(IDD_REPORTVIEW4,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_INFOVIEW, 
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIInformationFrame),
			RUNTIME_CLASS(CMDIInformationView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
	idx.push_back(INDEX_TABLE(IDD_INFOVIEW,suite,sLangFN,TRUE));


	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	sVersion.ReleaseBuffer();
	sCopyright.ReleaseBuffer();
	sCompany.ReleaseBuffer();

	// Do a check to see if database tables are created; 080131 p�d
	DoDatabaseTables(_T(""));	// Empty arg = use default database; 081001 p�d
	DoAlterTables(_T(""));	// Empty arg = use default database; 090525 p�d
}

void AFX_EXT_API DoDatabaseTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	CString sLang(getLangSet());
	if (getIsDBConSet() == 1)
	{
		//OBS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Only for SQL Server database; 080630 p�d

		vec.push_back(Scripts(TBL_TRAKT,table_EstiTrakt,db_name));
		vec.push_back(Scripts(TBL_TRAKT_MISC_DATA,tableEstiTraktMisc,db_name));
		vec.push_back(Scripts(TBL_TRAKT_DATA,table_EstiTraktData,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SPC_ASS,table_EstiTraktSpcAssort,db_name));
		vec.push_back(Scripts(TBL_TRAKT_TRANS,table_EstiTraktTransAssort,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,table_EstiTraktSetSpc,db_name));
		vec.push_back(Scripts(TBL_TRAKT_PLOT,table_EstiTraktPlot,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SAMPLE_TREES,table_EstiTraktSampleTrees,db_name));
		vec.push_back(Scripts(TBL_TRAKT_DCLS_TREES,table_EstiDclsTrees,db_name));
		vec.push_back(Scripts(TBL_TRAKT_DCLS1_TREES,table_EstiDcls1Trees,db_name));
		vec.push_back(Scripts(TBL_TRAKT_DCLS2_TREES,table_EstiDcls2Trees,db_name));
		vec.push_back(Scripts(TBL_TRAKT_TREES_ASSORT,table_TraktTreesAssort,db_name));
		vec.push_back(Scripts(TBL_TRAKT_TYPE,table_TraktType,db_name));
		vec.push_back(Scripts(TBL_ORIGIN,table_Origin,db_name));
		vec.push_back(Scripts(TBL_INPUT_METHOD,table_InputData,db_name));
		vec.push_back(Scripts(TBL_HGT_CLASS,table_HeightClass,db_name));
		vec.push_back(Scripts(TBL_CUT_CLASS,table_CutClass,db_name));
		vec.push_back(Scripts(TBL_TRAKT_ROTPOST,table_TraktRotpost,db_name));
		vec.push_back(Scripts(TBL_TRAKT_ROTPOST_SPC,table_RotpostSpc,db_name));
		vec.push_back(Scripts(TBL_TRAKT_ROTPOST_OTC,table_RotpostOther,db_name));
		if (sLang.CompareNoCase(_T("SVE")) == 0 || sLang.CompareNoCase(_T("NOR")) == 0)
			vec.push_back(Scripts(TBL_SAMPLE_TREES_CATEGORY, table_SampleTreesCategory_SVE, db_name));
		else
			vec.push_back(Scripts(TBL_SAMPLE_TREES_CATEGORY, table_SampleTreesCategory_ENU, db_name));
		runSQLScriptFileEx1(vec,Scripts::TBL_CREATE);
		vec.clear();
	}	// if (getIsDBConSet() == 1)
}	

void AFX_EXT_API DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	CString sLang(getLangSet());
	if (getIsDBConSet() == 1)
	{
		// Alter tables
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable090630,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiTraktTable090916,db_name));
		//  Adding "trakt_areal_factor"; Added 2009-11-02 p�d
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable091102,db_name));
		// Added 100311 p�d
		// Adding fileds in table for GROT
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiSetSpcTable100311_1,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiSetSpcTable100311_2,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiSetSpcTable100315_1,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiSetSpcTable100316_1,db_name));
		vec.push_back(Scripts(TBL_TRAKT_SET_SPC,alter_EstiSetSpcTable150929_1,db_name));

		vec.push_back(Scripts(TBL_TRAKT_SET_SPC, alter_EstiSetSpcTable151006_1, db_name));	// #4555: Default gr�nkroneandel

		vec.push_back(Scripts(TBL_TRAKT_DATA,alter_EstiTraktDataTable110315_1,db_name));

		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable100306,db_name));

		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable100419,db_name));
		// Added 2010-05-26 P�D
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable100526,db_name));

		// Added 2010-11-10 P�D
		vec.push_back(Scripts(TBL_TRAKT_TRANS,alter_EstiTraktTransTable101110,db_name));

		//J� 20121024 #3429 Add column for coordinates on border trees
		vec.push_back(Scripts(TBL_TRAKT_SAMPLE_TREES,alter_EstiTraktSampleTreesTable121024,db_name));


		//J� 20121024 #3429 Add column for stand point coordinate
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable121024,db_name));

		// Update esti_trakt_sample_trees_table
		vec.push_back(Scripts(TBL_TRAKT_SAMPLE_TREES, alter_EstiSampeTreesTable1, db_name));
		vec.push_back(Scripts(TBL_TRAKT_SAMPLE_TREES, alter_EstiSampeTreesTable2, db_name));

		// Update esti_trakt_sample_trees_table (#4205)
		vec.push_back(Scripts(TBL_TRAKT_SAMPLE_TREES, alter_EstiSampeTreesTable4205, db_name));

			
		//#4205 L�ngd och bredd f�r arealsber�kning 20150112 J�
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable150112_1,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable150112_2,db_name));

		//HMS-44 Tillf�lligt utnyttjande checkbox, bit. 190327 J�
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable190327,db_name));

		

		//#4688 Change image to varbinary(MAX)
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable151210_1,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_EstiTraktTable151210_2,db_name));

		//#4376 Add default categories
		if (sLang.CompareNoCase(_T("SVE")) == 0 || sLang.CompareNoCase(_T("NOR")) == 0)
			vec.push_back(Scripts(TBL_TRAKT,alter_EstiSampleTreesCategory150811_SVE,db_name));
		else
			vec.push_back(Scripts(TBL_TRAKT,alter_EstiSampleTreesCategory150811_ENU,db_name));


		// #4564: �ndra typ p� esti_trakt_table/trakt_coordinates till NVARCHAR(MAX)
		vec.push_back(Scripts(TBL_TRAKT, alter_EstiTraktTable151009, db_name));

		runSQLScriptFileEx1(vec,Scripts::TBL_ALTER);
		vec.clear();
	}	// if (getIsDBConSet() == 1)
}	

BOOL AFX_EXT_API remoteCalculation(void)
{
	runDoCalulationInPageThree();

	return TRUE;
}

// Exported function used by NorrServer to calculate a stand
BOOL AFX_EXT_API calculateStand(int trakt_id, DB_CONNECTION_DATA &conn)
{
	CTransaction_trakt recTrakt;
	CStringArray log;
	int ret;

	// Prepare, do calculation
	CUMEstimateDB db(conn);
	db.resetTraktData(trakt_id);
	db.resetSampleTrees(trakt_id);
	db.getTrakt(trakt_id, recTrakt);


	//#4600 Kontrollera best�nd�lder om s�derbergs anv�nds som barkfunktion 20151020 J�

	CStringArray arrLogBark,arrLogHgt,arrTraktSoderbergErrorLog;
	arrLogBark.RemoveAll();
	arrLogHgt.RemoveAll();
	arrTraktSoderbergErrorLog.RemoveAll();	
	
	int nReturnValue=doSoderbergHgtandVolCheckonTrakt(conn,recTrakt,arrLogBark,arrLogHgt);								
	if(nReturnValue!=1)
	{
		CString sAgeForBarkSoderbergUnreasonable=_T("");
		CString sAgeForHgtSoderbergUnreasonable=_T("");
		CString sAgeForSoderbergUnreasonable=_T("");
		CString msg=_T("");
		CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(sLangFN))
			{
				sAgeForBarkSoderbergUnreasonable= (xml->str(IDS_STRING353));
				sAgeForHgtSoderbergUnreasonable= (xml->str(IDS_STRING354));
				sAgeForSoderbergUnreasonable = (xml->str(IDS_STRING355));
			}
			delete xml;
		}
		arrTraktSoderbergErrorLog.Add(formatData(_T("%s - %s"),recTrakt.getTraktNum(),recTrakt.getTraktName()));
		if (arrLogBark.GetCount() > 0) //Barkfunktion anv�nds med best�nd�lder >500
		{
			arrTraktSoderbergErrorLog.Add(sAgeForBarkSoderbergUnreasonable);									
			arrTraktSoderbergErrorLog.Append(arrLogBark);
		}
		if (arrLogHgt.GetCount() > 0) //H�jd anv�nds med best�nd�lder >500
		{
			arrTraktSoderbergErrorLog.Add(sAgeForHgtSoderbergUnreasonable);									
			arrTraktSoderbergErrorLog.Append(arrLogHgt);
		}
		for (int i = 0;i < arrTraktSoderbergErrorLog.GetCount();i++)
			msg += arrTraktSoderbergErrorLog.GetAt(i) + _T("\r\n");
		UMMessageBox(NULL,msg,sAgeForSoderbergUnreasonable,MB_ICONEXCLAMATION | MB_OK);
	}

	// #4618: Kontrollera om h�jd �r st�rre �n 35m och H25 anv�nds
	nReturnValue = CheckH25MinMax(conn, recTrakt, arrLogHgt);
	if (nReturnValue == FALSE)
	{
		// visa varning om att det finns provtr�d utanf�r H25s omr�de
		CString m_sHeightForH25Unreasonable, m_sHeightForH25Unreasonable2, msg;
		CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(sLangFN))
			{
				m_sHeightForH25Unreasonable = xml->str(IDS_STRING356);
				m_sHeightForH25Unreasonable2 = xml->str(IDS_STRING357);
			}
			delete xml;
		}

		msg = _T("");

		CString msg2;
		msg2.Format(m_sHeightForH25Unreasonable2, (int)H25_MIN_VALUE*10, (int)H25_MAX_VALUE*10);
		msg += msg2;
		msg += _T("\r\n");

		for (int i = 0; i < arrLogHgt.GetCount(); i++)
			msg += arrLogHgt.GetAt(i) + _T("\r\n");

		UMMessageBox(NULL, msg, m_sHeightForH25Unreasonable, MB_ICONEXCLAMATION | MB_OK);
	}


	ret = doCruisingCalculations(-1, FALSE, log, TRUE, conn, recTrakt);

	if(log.GetCount() > 0 || ret < 0)
	{
		// Load language file
		CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(sLangFN))
			{
				CString msg;
				msg.Format(_T("%s\n\n%s %s\n\n%s\n%s\n%s"),
					(xml->str(IDS_STRING3443)),
					(xml->str(IDS_STRING3444)),
					getDateTime(),
					(xml->str(IDS_STRING3445)),
					(xml->str(IDS_STRING3446)),
					(xml->str(IDS_STRING3447)));
				LogDialogMessage(msg);
			}
			delete xml;
		}

		// Log error messages
		for( int i=0; i < log.GetCount(); i++ )
		{
			LogDialogMessage(log.GetAt(i));
		}
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

BOOL AFX_EXT_API changeTemplateFix(int trakt_id, LPCTSTR templateXml, LPCTSTR pricelistXml, DB_CONNECTION_DATA &conn)
{
	// This function will update transfers and specie assortments with data from new template
	CUMEstimateDB *pDB = new CUMEstimateDB(conn);
	TemplateParser pars;
	xmllitePricelistParser prlPars;
	vecTransactionAssort vecAssort;
	vecTransactionTemplateTransfers vecTransfers;
	vecTransactionTraktData vecTraktData;
	vecTransactionSpecies vecTmplSpecies;

	if( !pDB )
		return FALSE;

	if( !pars.LoadFromBuffer(templateXml) )
		return FALSE;

	if( !prlPars.loadStream(pricelistXml) )
		return FALSE;


	// Transfers
	getTemplateSpecies(&pars,vecTmplSpecies);
	for( int i = 0; i < vecTmplSpecies.size(); i++ )
	{
		// Try to get transafers set in Template by specie; 091016 p�d
		getTemplateTransfersPerSpcecie(&pars,vecTmplSpecies[i].getSpcID(),vecTransfers);
		for (UINT i1 = 0;i1 < vecTransfers.size();i1++)
		{
			CTransaction_trakt_trans recTraktTrans = CTransaction_trakt_trans(vecTmplSpecies[i].getSpcID(),
																				trakt_id,
																				vecTransfers[i1].getFromAssortID(),
																				vecTransfers[i1].getToAssortID(),
																				vecTmplSpecies[i].getSpcID(),
																				STMP_LEN_WITHDRAW,
																				vecTransfers[i1].getFromAssort(),
																				vecTransfers[i1].getToAssort(),
																				vecTmplSpecies[i].getSpcName(),
																				i1+1.0,
																				vecTransfers[i1].getPercent(),
																				0.0,
																				_T(""));

			if (!pDB->addTraktTrans(recTraktTrans))
				pDB->updTraktTrans(recTraktTrans);
		}
	}


	// Specie assortments
	pDB->getTraktData(vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
	prlPars.getAssortmentPerSpecie(vecAssort);

	// Setup assortments form selected pricelist to "esti_trakt_spc_assort_table"; 080521 p�d
	int nCounter = 1;
	for (UINT i1 = 0;i1 < vecTraktData.size();i1++)
	{
		CTransaction_trakt_data recTraktData = vecTraktData[i1];
		nCounter = 1;
		for (UINT i2 = 0;i2 < vecAssort.size();i2++)
		{
			CTransaction_assort recAssort = vecAssort[i2];
			if (recAssort.getSpcID() == recTraktData.getSpecieID())
			{
				CTransaction_trakt_ass rec = CTransaction_trakt_ass(nCounter,
																	trakt_id,
																	recTraktData.getSpecieID(),
																	STMP_LEN_WITHDRAW,
																	recAssort.getAssortName(),
																	recAssort.getPriceM3fub(),
																	recAssort.getPriceM3to(),
																	0.0,0.0,0.0,0.0,_T(""),0.0);
				pDB->addTraktAss(rec);
				nCounter++;
			}
		}
	}

	delete pDB;

	return TRUE;
}

// THIS METHOD IS USED IN AN EXPORTED METHOD (Peter L.); 091014 p�d
BOOL AFX_EXT_API export_createTraktFromTemplate(CString sTemplateXMLFile,DB_CONNECTION_DATA& conn,int *trakt_id,TCHAR *username/*= NULL*/)
{
	int nTraktID=0;
	int nHgtOverSea=0;
	int nLatitude=0;
	int nLongitude=0;
	CString sNCoord=_T("");
	TCHAR szGrowthArea[127]=_T("");
	TCHAR szSI_H100[127]=_T("");
	double fCorrFac=0.0;

	int nExchangeID=0;
	TCHAR szExchange[127]=_T("");
	int nPricelistID=0;
	int nPricelistTypeOf=0;
	TCHAR szPricelist[127]=_T("");
	double fDCLS=0.0;
	int nCostsTmplID=0;
	TCHAR szCostsTmplName[127]=_T("");

	int nQualDescIndex=0;
	TCHAR szQualDesc[127]=_T("");
	double fSkToFub=0.0;
	double fFubToTo=0.0;
	double fSkToUb=0.0;
	bool bIsAtCoast=false;
	bool bIsSouthEast=false;
	bool bIsRegion5=false;
	bool bIsPartOfPlot=false;
	TCHAR szSI_H100_Pine[127]=_T("");
	double fH25=0.0;
	double fGreenCrown=0.0;
	int nTranspDist1=0;
	int nTranspDist2=0;

	int nGrotIndex=0;
	double fGrotPercent=0.0;
	double fGrotPrice=0.0;
	double fGrotCost=0.0;

	CString sExtraInfo=_T("");

	CUMEstimateDB *pDB = new CUMEstimateDB(conn);

	CTransaction_pricelist recPricelist;
	CTransaction_costtempl recCostsTmpl;

	CTransaction_trakt recTrakt;
	CTransaction_trakt_misc_data recTraktMiscData;
	CTransaction_trakt_trans recTraktTrans;
	CTransaction_trakt_set_spc recTraktSetSpc;
	CTransaction_trakt_data recTData;
	CTransaction_trakt_ass recTAss;

	vecTransactionAssort vecAssort;
	CTransaction_assort recAssort;

	vecTransactionSpecies vecTmplSpecies;
	vecUCFunctionList vec2;
	vecTransactionTemplateTransfers vecTransfers;

	TemplateParser *pars = new TemplateParser();
	if (!pars->LoadFromBuffer(sTemplateXMLFile)) return FALSE;

	getTemplateMainSettings(pars,&nHgtOverSea,&nLatitude,szGrowthArea,szSI_H100,&fCorrFac,&nCostsTmplID,szCostsTmplName,
													&bIsAtCoast,&bIsSouthEast,&bIsRegion5,&bIsPartOfPlot,szSI_H100_Pine);
	getTemplateLongitudeSettings(pars,&nLongitude);

	// Calculate NKoord RT90; 100317 p�d
	CLatLongToRT90_data recRT90;
	SweRef99LatLongtoRT90xy(&(recRT90 = CLatLongToRT90_data(nLatitude*100,nLongitude*100)));
	sNCoord.Format(L"%.0f",recRT90.getX()/100000.0);

	// Setup the "Extra information"; 070906 p�d
	// Extra info is set on Trakt level; 071207 p�d
	sExtraInfo.Format(_T("%d;%d;%d;%d;%s;"),
										bIsAtCoast,
										bIsSouthEast,
										bIsRegion5,
										bIsPartOfPlot,
										szSI_H100_Pine);

		getTemplateSettings(pars,&nExchangeID,szExchange,&nPricelistID,
												&nPricelistTypeOf,szPricelist,&fDCLS);

		// Try to find pricelist in prl_pricelist_table, based on information in selected template; 091014 p�d
		vecTransactionPricelist vecPricelist;

		if (pDB != NULL)
		{
			if (pDB->getPricelists(vecPricelist))
			{
				if (vecPricelist.size() > 0)
				{
					for (UINT i = 0;i < vecPricelist.size();i++)
					{
						recPricelist = vecPricelist[i];
						if (recPricelist.getID() == nPricelistID && recPricelist.getTypeOf() == nPricelistTypeOf)
						{
							break;
						}
					}	// for (UINT i = 0;i < vecPricelist.size();i++)
				}	// if (vecPricelist.size() > 0)
			}	// if (pDB->getPricelists(vecPricelist))
		}	// if (pDB != NULL)
	
		vecTransaction_costtempl vecCostsTmpl;
		if (pDB != NULL)
		{
			if (pDB->getCostTmpls(vecCostsTmpl))
			{
				if (vecCostsTmpl.size() > 0)
				{
					for (UINT i = 0;i < vecCostsTmpl.size();i++)
					{
						recCostsTmpl = vecCostsTmpl[i];
						// OBS! Check for "Kostnadsmallar f�r b�de Rotpost och Intr�ng"; 091014 p�d
//						if (recCostsTmpl.getID() == nCostsTmplID && recCostsTmpl.getTemplateName().Compare(szCostsTmplName) == 0)
						if (recCostsTmpl.getTemplateName().Compare(szCostsTmplName) == 0)
						{
							break;
						}
					}	// for (UINT i = 0;i < vecCostsTmpl.size();i++)
				}	// if (vecCostsTmpl.size() > 0)
			}	// if (pDB->getCostTmpls(vecCostsTmpl))
		}	// if (pDB != NULL)

	// Setup the Traktnumber; 091015 p�d
	TCHAR szTraktNumber[127]=_T("");
	int nIdentity=0;
	int nNumOf=0;
	if (pDB != NULL)
	{
		nIdentity = pDB->getLastTraktIdentity();
		nNumOf = pDB->getNumOfRecordsInTrakt();
		if (nIdentity == 1 && nNumOf == 0)
			_stprintf(szTraktNumber,_T("T%05d"),nIdentity);
		else
			_stprintf(szTraktNumber,_T("T%05d"),nIdentity+1);
	}	// if (pDB != NULL)

	// Save trakt information. I.e. create a new Trakt; 091015 p�d

	recTrakt = CTransaction_trakt(-1,	// -1 equals new trakt
																szTraktNumber,	// Traktnummer
																_T(""),					// Delnummer
																_T(""),					// Traktnamn
																_T(""),					// Trakttyp
																getDateEx(),		// Datum YYYY-MM-DD
																-1,							// ID f�r fastighet
																0.0,						// Areal 1
																0.0,						// Areal 2
																0.0,						// Areal 3
																0,							// �lder
																szSI_H100,			// SI f�r trakt
																0,							// Uttag
																nHgtOverSea,		// H�jd �ver havet
																0,							// G
																0,							// L
																0,							// Y
															  _T(""),					// Mapdata
															  nLatitude,			// Latitude (Breddgrad)
															  nLongitude,			// Longitude
															  sNCoord,				// X-koordinat
															  _T(""),					// Y-koordinat
															  _T(""),					// Ursprung
															  _T(""),					// Input metod
															  _T(""),					// H�jdklass
															  _T(""),					// Huggningsklass
															  FALSE,//bIsSimData,
															  bIsAtCoast,
															  bIsSouthEast,
															  bIsRegion5,
															  bIsPartOfPlot,
															  szSI_H100_Pine,	// SI H100 Tall S�derbergs
															  _T(""),					// Fastighetsnummer
															  _T(""),					// Namn p� fastighet
															  _T(""),					// Noteringar
															  username ? username : getUserName().MakeUpper(),
															  getDateTime(),
																1,							// Sida 1 = default
																0,							// 0 = default; anv�nd �ven i intr�ng	
																0,							//L�ngd
																0,							//Bredd
																FALSE,						//HMS-44 Tillf�lligt utnyttjande 190327 J�
																_T(""),	// HMS-120 trakt point
																_T("")	// HMS-120 trakt  coordinates
																);

		if (pDB != NULL)
		{
			if (pDB->addTrakt(recTrakt))
			{
				// If adding trakt was successfull, we'll try to get the
				// ID for this trakt; 091015 p�d
				nTraktID = pDB->getLastTraktIdentity();
				if (nTraktID > -1)
				{
					recTraktMiscData = CTransaction_trakt_misc_data(nTraktID,
																												 szPricelist,
																												 nPricelistTypeOf,
																												 recPricelist.getPricelistFile(),
																												 recCostsTmpl.getTemplateName(),
																												 recCostsTmpl.getTypeOf(),
																												 recCostsTmpl.getTemplateFile(),
																												 fDCLS,
																												 _T(""));
					if (!pDB->addTraktMiscData_prl(recTraktMiscData))
						pDB->updTraktMiscData_prl(recTraktMiscData);

					if (!pDB->addTraktMiscData_dcls(recTraktMiscData))
						pDB->updTraktMiscData_dcls(recTraktMiscData);

					if (!pDB->addTraktMiscData_costtmpl(recTraktMiscData))
						pDB->updTraktMiscData_costtmpl(recTraktMiscData);

					// get species in Template; 070905 p�d
					// NB! Species in Template equals species in pricelist; 091015 p�d
					getTemplateSpecies(pars,vecTmplSpecies);

					xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
					if (pPrlParser != NULL)
					{
						if (pPrlParser->loadStream(recPricelist.getPricelistFile()))
						{
							//---------------------------------------------------------------------------------
							// Get assortments per specie; 070430 p�d
							pPrlParser->getAssortmentPerSpecie(vecAssort);
						}
						delete pPrlParser;
					}

					// Make sure there's species; 070905 p�d
					if (vecTmplSpecies.size() > 0)
					{
						for (UINT i = 0;i < vecTmplSpecies.size();i++)
						{
							getTemplateFunctionsPerSpcecie(pars,vecTmplSpecies[i].getSpcID(),vec2,&fFubToTo,&fSkToUb);
							getTemplateMiscFunctionsPerSpcecie(pars,vecTmplSpecies[i].getSpcID(),
																								 &nQualDescIndex,szQualDesc,&fSkToFub,&fFubToTo,
																								 &fH25,&nTranspDist1,&nTranspDist2,&fGreenCrown);
							getTemplateGrotFunctionsPerSpcecie(pars,vecTmplSpecies[i].getSpcID(),&nGrotIndex,&fGrotPercent,&fGrotPrice,&fGrotCost);

							//-----------------------------------------------------------------------------------------------
							// Add data to TraktData table; 091015 p�d
							recTData = CTransaction_trakt_data(vecTmplSpecies[i].getSpcID(),
																								 nTraktID,
																								 STMP_LEN_WITHDRAW,
																								 vecTmplSpecies[i].getSpcID(),
																								 vecTmplSpecies[i].getSpcName(),
																								 0.0,
																								 0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 fH25,
																								 fGreenCrown,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 0.0,
																								 _T(""));

							// Save trakt data for "St�mplingsl�ngd - Uttag"
							if (!pDB->addTraktData(recTData))
								pDB->updTraktData(recTData);
							// Also save Trakt data for "St�mplingsl�ngd - Kvarl�mmnat"
							recTData.setTDataType(STMP_LEN_TO_BE_LEFT);
							if (!pDB->addTraktData(recTData))
								pDB->updTraktData(recTData);
							// Also save Trakt data for "St�mplingsl�ngd - Uttag i Stickv�g"
							recTData.setTDataType(STMP_LEN_WITHDRAW_ROAD);
							if (!pDB->addTraktData(recTData))
								pDB->updTraktData(recTData);
							//-----------------------------------------------------------------------------------------------
							// Add assortments for each specie, set in table "esti_trakt_spc_assort_table"; 091015 p�d
							pDB->removeTraktAss(recTData.getTDataTraktID(),recTData.getTDataID());
							// .. and get information in pricelist (xml-file), saved; 070502 p�d
							/*
							PricelistParser prlParser;
							if (prlParser.LoadFromBuffer(recPricelist.getPricelistFile()))
							{
								//---------------------------------------------------------------------------------
								// Get assortments per specie; 070430 p�d
								prlParser.getAssortmentPerSpecie(vecAssort);
							}
							*/
							// Add assortments for each specie; 070430 p�d
							if (vecAssort.size() > 0)
							{
								for (UINT j = 0;j < vecAssort.size();j++)
								{
									recAssort = vecAssort[j];
									// Check that we add only assortments for Specie; 070430 p�d
									if (recAssort.getSpcID() == vecTmplSpecies[i].getSpcID())
									{
											//-----------------------------------------------------
											// Save trakt assort
											recTAss = CTransaction_trakt_ass(j+1,
																											 recTData.getTDataTraktID(),
																											 recTData.getTDataID(),
 	 																										 STMP_LEN_WITHDRAW,
																											 recAssort.getAssortName(),
																											 recAssort.getPriceM3fub(),
																											 recAssort.getPriceM3to(),
																											 0.0,
																											 0.0,
																											 0.0,																										 
																											 0.0,
																											 _T(""),
																											 0.0);
											// Remove Trakt Assortments if we are goin'
											// to update. Maybe user has removed assortments
											// or added one. We just enter data again; 070315 opd
											if (!pDB->addTraktAss(recTAss))
												pDB->updTraktAss(recTAss);

											//-----------------------------------------------------

									}	// if (recAssort.getSpcID() == vecTmplSpecies[i].getSpcID())
								}	// for (UINT j = 0;j < vecAssort.size();j++)
							}	// if (vecAssort.size() == 0)

							//-----------------------------------------------------------------------------------------------
							// Check to see if we have functions specified for specie; 070905 p�d
							if (vec2.size() == 4)	// 4 equals Height,Volume and Barkfunctions; 070905 p�d
							{
								recTraktSetSpc = CTransaction_trakt_set_spc(vecTmplSpecies[i].getSpcID(),
																													 vecTmplSpecies[i].getSpcID(),
																													 nTraktID,
	 																												 STMP_LEN_WITHDRAW,
																													 vecTmplSpecies[i].getSpcID(),
																													 vec2[0].getID(),
																													 vec2[0].getSpcID(),
																													 vec2[0].getIndex(),
																													 vec2[1].getID(),
																													 vec2[1].getSpcID(),
																													 vec2[1].getIndex(),
																													 vec2[2].getID(),
																													 vec2[2].getSpcID(),
																													 vec2[2].getIndex(),
																													 vec2[3].getID(),
																													 vec2[3].getSpcID(),
																													 vec2[3].getIndex(),
																													 fSkToUb,
																													 fSkToFub,
																													 fFubToTo,
																													 fDCLS,	// Diameterclass
																													 nQualDescIndex,
																													 _T(""),
																													 nTranspDist1,
																													 nTranspDist2,
																													 _T(""),
																													 szQualDesc,
																													 nGrotIndex,
																													 fGrotPercent,
																													 fGrotPrice,
																													 fGrotCost,
																													 fH25,
																													 fGreenCrown);	// #4555: Standard gr�nkroneandel

									if (!pDB->addTraktSetSpc_hgtfunc(recTraktSetSpc))
										pDB->updTraktSetSpc_hgtfunc(recTraktSetSpc);

									if (!pDB->addTraktSetSpc_volfunc(recTraktSetSpc))
										pDB->updTraktSetSpc_volfunc(recTraktSetSpc);
									
									if (!pDB->addTraktSetSpc_barkfunc(recTraktSetSpc))
										pDB->updTraktSetSpc_barkfunc(recTraktSetSpc);
									
									if (!pDB->addTraktSetSpcMiscData(recTraktSetSpc))
										pDB->updTraktSetSpcMiscData(recTraktSetSpc);

									if (!pDB->addTraktSetSpc_volfunc_ub(recTraktSetSpc))
										pDB->updTraktSetSpc_volfunc_ub(recTraktSetSpc);					

									if (!pDB->addTraktSetSpc_rest_of_misc_data(recTraktSetSpc))
										pDB->updTraktSetSpc_rest_of_misc_data(recTraktSetSpc);						

									if (!pDB->addTraktSetSpc_grot(recTraktSetSpc))
										pDB->updTraktSetSpc_grot(recTraktSetSpc);						

									if (!pDB->addTraktSetSpc_default_h25(recTraktSetSpc))
										pDB->updTraktSetSpc_default_h25(recTraktSetSpc);

									if (!pDB->addTraktSetSpc_default_greencrown(recTraktSetSpc))	// #4555: Standard gr�nkroneandel
										pDB->updTraktSetSpc_default_greencrown(recTraktSetSpc);
							}	// if (vec2.size() == 4)

							// Try to get transafers set in Template by specie; 091016 p�d			
							getTemplateTransfersPerSpcecie(pars,vecTmplSpecies[i].getSpcID(),vecTransfers);
							// Do we have any transfers for this specie; 091016 p�d
							if (vecTransfers.size() > 0)
							{
								for (UINT i1 = 0;i1 < vecTransfers.size();i1++)
								{
									recTraktTrans = CTransaction_trakt_trans(vecTmplSpecies[i].getSpcID(),
																													nTraktID,
																													vecTransfers[i1].getFromAssortID(),
																													vecTransfers[i1].getToAssortID(),
																													vecTmplSpecies[i].getSpcID(),
																													STMP_LEN_WITHDRAW,
																													vecTransfers[i1].getFromAssort(),
																													vecTransfers[i1].getToAssort(),
																													vecTmplSpecies[i].getSpcName(),
																													i1+1.0,
																													vecTransfers[i1].getPercent(),
																													0.0,
																													_T(""));

									if (!pDB->addTraktTrans(recTraktTrans))
										pDB->updTraktTrans(recTraktTrans);
								}	// for (UINT i1 = 0;i1 < vecTransfers.size();i1++)
						
							}

						}	// for (UINT i = 0;i < vecTmplSpecies.size();i++)
					}	// if (vecTmplSpecies.size() > 0			
				}	// if (nTraktID > -1)
			}	// if (pDB->addTrakt(recTrakt))
		}	// if (pDB != NULL)

	if (pDB != NULL) delete pDB;
	if (pars != NULL) delete pars;
	*trakt_id = nTraktID;
	return (nTraktID > -1);	// Maybe > 0 ?; 091015 p�d
}
