// OnNewTraktDialog.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "OnNewTraktDialog.h"

#include "ResLangFileReader.h"


// COnNewTraktDialog dialog

IMPLEMENT_DYNAMIC(COnNewTraktDialog, CXTResizeDialog)

BEGIN_MESSAGE_MAP(COnNewTraktDialog, CXTResizeDialog)
//	ON_BN_CLICKED(IDC_RADIO1, &COnNewTraktDialog::OnBnClickedRadio1)
//	ON_BN_CLICKED(IDC_RADIO2, &COnNewTraktDialog::OnBnClickedRadio2)
//	ON_CBN_SELCHANGE(IDC_COMBO1, &COnNewTraktDialog::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDOK, OnOKBtn)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &COnNewTraktDialog::OnNMClickList1)
END_MESSAGE_MAP()

COnNewTraktDialog::COnNewTraktDialog(CWnd* pParent /*=NULL*/,CUMEstimateDB *pDB)
	: CXTResizeDialog(COnNewTraktDialog::IDD, pParent)
{
	m_pDB=pDB;
}

COnNewTraktDialog::~COnNewTraktDialog()
{
	m_vecTransactionTemplate.clear();
}

void COnNewTraktDialog::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
//	DDX_Control(pDX, IDC_RADIO1, m_rbNoTemplate);
//	DDX_Control(pDX, IDC_RADIO2, m_rbUseTemplate);
//	DDX_Control(pDX, IDC_COMBO1, m_cbTemplates);
	DDX_Control(pDX, IDC_LIST1, m_wndLCtrl);
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

INT_PTR COnNewTraktDialog::DoModal()
{
	if( DisplayMsg() )
	{
		return CXTResizeDialog::DoModal();
	}
	else
	{
		return IDCANCEL;
	}
}

BOOL COnNewTraktDialog::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndLCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_Header.SubclassWindow(hWndHeader);
	m_Header.SetTheme(new CXTHeaderCtrlThemeOffice2003());
	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	setLanguage();
	setTemplatesInList();
	m_btnOK.EnableWindow( FALSE );
/*
	setTemplatesInCBox();

	// Set "Skapa ny Trakt utan mall" as default selection; 070308 p�d
	m_rbUseTemplate.SetCheck( TRUE );

	m_rbNoTemplate.SetCheck( FALSE );

	// Enable ComboBox on startup; 070308 p�d
	m_cbTemplates.EnableWindow( TRUE );
*/	
	return TRUE;
}

// COnNewTraktDialog message handlers
/*
void COnNewTraktDialog::setTemplatesInCBox(void)
{
	// Make sure we have some templates to choose from; 070903 p�d
	if (m_vecTransactionTemplate.size() > 0)
	{
		m_cbTemplates.Clear();
		for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		{
			CTransaction_template rec = m_vecTransactionTemplate[i];
			m_cbTemplates.AddString(rec.getTemplateName());
		}
	}
}
*/
void COnNewTraktDialog::setTemplatesInList(void)
{
	CTransaction_template rec;
//	m_wndLCtrl.RemoveAllGroups();
	// Make sure there's any data to work with; 080404 p�d
	if (m_vecTransactionTemplate.size() > 0)
	{	
		for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		{
			rec = m_vecTransactionTemplate[i];
			InsertRow(m_wndLCtrl,i,3,(rec.getTemplateName()),(rec.getCreatedBy()),(rec.getCreated()));
		}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
	}	// if (m_vecTransactionTemplate.size() > 0)
}

void COnNewTraktDialog::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING200)));
			m_wndGroup1.SetWindowText((xml->str(IDS_STRING202)));
/*
			m_rbNoTemplate.SetWindowText((xml->str(IDS_STRING201)));
			m_rbUseTemplate.SetWindowText((xml->str(IDS_STRING202)));
*/
			m_btnOK.SetWindowText((xml->str(IDS_STRING155)));
			m_btnCancel.SetWindowText((xml->str(IDS_STRING156)));

			m_wndLCtrl.InsertColumn(0,(xml->str(IDS_STRING2001)),LVCFMT_LEFT,150);
			m_wndLCtrl.InsertColumn(1,(xml->str(IDS_STRING2002)),LVCFMT_LEFT,100);
			m_wndLCtrl.InsertColumn(2,(xml->str(IDS_STRING2003)),LVCFMT_LEFT,110);
			m_wndLCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT);

			m_sPricelistErrorStatus	=xml->str(IDS_STRING701);
			m_sCostErrorStatus		=xml->str(IDS_STRING702);
			m_sErrorStatus			=xml->str(IDS_STRING703);

		}
		delete xml;
	}

}
/*
void COnNewTraktDialog::OnBnClickedRadio1()
{
	// Disable ComboBox on startup; 070308 p�d
	m_cbTemplates.EnableWindow( FALSE );
}

void COnNewTraktDialog::OnBnClickedRadio2()
{
	// Disable ComboBox on startup; 070308 p�d
	m_cbTemplates.EnableWindow(m_bIsTemplates);
}

void COnNewTraktDialog::OnCbnSelchangeCombo1()
{
	int nIndex = m_cbTemplates.GetCurSel();
	if (nIndex > -1 && nIndex < (int)m_vecTransactionTemplate.size())
	{
			CTransaction_template rec = m_vecTransactionTemplate[nIndex];
			m_sXMLFile = rec.getTemplateFile();
	}	// if (nIndex > -1 && nIndex < m_vecTransactionTemplate.size())
}
*/
void COnNewTraktDialog::OnOKBtn()
{
	m_nCreateTraktSelection = ID_TRAKT_USE_TEMPLATE;
	int	nIndex = GetSelectedItem(m_wndLCtrl),nRet=0;
	CString sMsg=_T("");
	if (nIndex > -1 && nIndex < (int)m_vecTransactionTemplate.size())
	{
		//Kontrollera mallen h�r s� att ing�ende prislista samt kostnad inte �r satta under utveckling, 
		//i s� fall returnera OnCancel eller annan l�mplig avbryt Bug 2368 J� 20111011		
		CTransaction_template rec = m_vecTransactionTemplate[nIndex];
		nRet=CheckStandTemplate(rec);
		switch(nRet)
		{
		case 1:
			nRet=1;
			m_sXMLFile = rec.getTemplateFile();
			break;
		case -1://Prislista sparad som under utveckling
			sMsg=m_sPricelistErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		case -2://Kostnadsmall sparad som under utveckling
			sMsg=m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		case -3://Prislista & Kostnadsmall sparad som under utveckling
			sMsg=m_sPricelistErrorStatus+_T("\n")+m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		}
	}	// if (nIndex > -1 && nIndex < m_vecTransactionTemplate.size())

/*
	if (m_rbUseTemplate.GetCheck() == BST_CHECKED)
		m_nCreateTraktSelection = ID_TRAKT_USE_TEMPLATE;
	if (m_rbNoTemplate.GetCheck() == BST_CHECKED)
		m_nCreateTraktSelection = ID_TRAKT_NO_TEMPLATE;
*/	
	if(nRet==1)
		CDialog::OnOK();
	else
		CDialog::OnCancel();
}

void COnNewTraktDialog::OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	m_btnOK.EnableWindow( m_wndLCtrl.GetSelectedCount() > 0);
	*pResult = 0;
}

//Kontrollera mallen h�r s� att ing�ende prislista samt kostnad inte �r satta under utveckling, 
//i s� fall returnera false Bug 2368 J� 20111011
//Returnerar 1 om ok, -1 om prislista sparad under utvecklin, -2 om kostnad sparad under utv, -3 om b�gge sparad under utv.
int COnNewTraktDialog::CheckStandTemplate(CTransaction_template recTmpl)
{
	int nRet=1,nPrlId=0,nCostId=0;
	int nPrlID=0,nPrlTypeof=0,nCostTypeof=0;
	TCHAR szFuncName[50];
	TemplateParser pars;
	vecTransactionPricelist vecPrl;
	vecTransaction_costtempl vecCost;
	if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
	{		
		//H�mta id f�r prislista
		pars.getTemplatePricelist(&nPrlId,szFuncName);

		if (m_pDB != NULL)
		{
			if(m_pDB->getPricelists(vecPrl))
			{
				//Leta r�tt p� r�tt prislista och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecPrl.size();i++)
					if(vecPrl[i].getID()==nPrlId)
					{
						nPrlTypeof=vecPrl[i].getTypeOf();
						break;
					}
			}

			pars.getTemplateCostsTmpl(&nCostId,szFuncName);

			if(m_pDB->getAllCostTmpls(vecCost))
			{
				//Leta r�tt p� r�tt kostnadsmall och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecCost.size();i++)
					if(vecCost[i].getID()==nCostId)
					{
						nCostTypeof=vecCost[i].getTypeOf();
						break;
					}
			}
		}

	}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))

	if(nPrlTypeof<0 && nCostTypeof<0)
		nRet=-3;
	else
	{
		if(nPrlTypeof<0)
			nRet=-1;
		else
		{
			if(nCostTypeof<0)
			nRet=-2;
		}
	}

	return nRet;
}
