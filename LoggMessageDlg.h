#pragma once

#include "Resource.h"

#include "CPage.h"

// CLoggMessageDlg dialog

class CLoggMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CLoggMessageDlg)

	CButton m_wndCancelBtn;
	CButton m_wndPrintOutBtn;
	CButton m_wndSaveToFileBtn;

	CMyExtEdit m_wndMessage;
	CString m_sMessage;

	CString m_sCaption;
	CString m_sCancel;
	CString m_sPrintOut;
	CString m_sSaveToFile;
public:
	CLoggMessageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoggMessageDlg();

	virtual INT_PTR DoModal();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

	void setMessage(LPCTSTR msg)
	{
		m_sMessage = msg;
	}

	void setupLanguage(LPCTSTR cap,LPCTSTR cancel,LPCTSTR print_out,LPCTSTR save_to_file)
	{
		m_sCaption = (cap);
		m_sCancel = (cancel);
		m_sPrintOut = (print_out);
		m_sSaveToFile = (save_to_file);
	}

protected:
	void PrintForm(CPage *page);
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo*);
	//}}AFX_MSG

 DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedPrintOut();
	afx_msg void OnBnClickedSaveToFile();
};
