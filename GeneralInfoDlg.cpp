// GeneralInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMEstimate.h"
#include "GeneralInfoDlg.h"

#include "ResLangFileReader.h"

// CGeneralInfoDlg


IMPLEMENT_DYNCREATE(CGeneralInfoDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CGeneralInfoDlg, CXTResizeDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
  ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

CGeneralInfoDlg::CGeneralInfoDlg()
	: CXTResizeDialog(CGeneralInfoDlg::IDD)
{

}

CGeneralInfoDlg::~CGeneralInfoDlg()
{
	m_fontData.DeleteObject();
}

void CGeneralInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_HTML, m_wndHTML);
	//}}AFX_DATA_MAP
}

BOOL CGeneralInfoDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	// Setup fonts to be used in 
	// Trakt-information window OnPaint(); 070215 p�d
	VERIFY(m_fontData.CreateFont(
   15,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,									// nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Arial")));                 // lpszFacename

	m_fAreal=0.0;
	setLanguage();

	m_wndHTML.SetBkColor(INFOBK);
	m_wndHTML.SetFont(&m_fontData);
	return TRUE;
}

BOOL CGeneralInfoDlg::OnEraseBkgnd(CDC* pDC) 
{
	return FALSE;
}


void CGeneralInfoDlg::OnPaint()
{

	CDC *pDC = GetDC();
	CRect rect;
	CRect rrect;
	GetClientRect(rect);
	GetClientRect(rrect);
	
	rrect.DeflateRect(1,1);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(0,0,rect.right,rect.bottom,INFOBK);
	pDC->RoundRect(rrect,CPoint(4,4));

	
	CString sText1;
	CString sText2;
	CString sText3;
	sText1.Format(_T("%s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b><br>"),	
			m_sLblTraktLnr,m_sTraktLnr,
			m_sLblTraktNum,m_sTraktNum,
			m_sLblTraktPNum,m_sTraktPNum,
			m_sLblTraktName,m_sTraktName,
			m_sLblTraktAreal,m_sTraktAreal
			);
	sText2.Format(_T("<b>%s</b><br>%s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b><br>"),	
			m_sWithDraw,
			m_sLblSumTrees,m_sSumTrees,
			m_sLblSumM3Sk,m_sSumM3Sk,
			m_sLblSumM3Sk_ha,m_sSumM3Sk_ha,
			m_sLblSumM3Fub,m_sSumM3Fub,
			m_sLblSumM3Fub_ha,m_sSumM3Fub_ha,
			m_sLblSumGY,m_sSumGY
			);

	sText3.Format(_T("<b>%s</b><br>%s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b>"),	
			m_sToBeLeft,
			m_sLblSumTrees,m_sSumTrees2,
			m_sLblSumM3Sk,m_sSumM3Sk2,
			m_sLblSumM3Sk_ha,m_sSumM3Sk2_ha,
			m_sLblSumGY,m_sSumGY2
			);
	
	sText3.Format(_T("<b>%s</b><br>%s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b> %s : <b>%s</b>"),	
			m_sToBeLeft,
			m_sLblSumTrees,m_sSumTrees2,
			m_sLblSumM3Sk,m_sSumM3Sk2,
			m_sLblSumM3Sk_ha,m_sSumM3Sk2_ha,
			m_sLblSumM3Fub,m_sSumM3Fub2,
			m_sLblSumGY,m_sSumGY2
			);
			
	m_wndHTML.SetWindowText(sText1+sText2+sText3);
	
	m_wndHTML.UpdateWindow();

	CXTResizeDialog::OnPaint();
}

void CGeneralInfoDlg::OnSize(UINT nType,int cx,int cy)
{
	if (m_wndHTML.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndHTML,1,1,cx-2,cy-2);
	}

	Invalidate();
	CXTResizeDialog::OnSize(nType,cx,cy);
}

// CGeneralInfoDlg diagnostics

#ifdef _DEBUG
void CGeneralInfoDlg::AssertValid() const
{
	CXTResizeDialog::AssertValid();
}

#ifndef _WIN32_WCE
void CGeneralInfoDlg::Dump(CDumpContext& dc) const
{
	CXTResizeDialog::Dump(dc);
}
#endif
#endif //_DEBUG


// CGeneralInfoDlg message handlers

void CGeneralInfoDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			m_sLblTraktLnr = (xml->str(IDS_STRING311));
			m_sLblTraktNum = (xml->str(IDS_STRING1000));
			m_sLblTraktPNum = (xml->str(IDS_STRING1001));
			m_sLblTraktName = (xml->str(IDS_STRING101));
			m_sLblTraktAreal = (xml->str(IDS_STRING1050));

			m_sWithDraw	= (xml->str(IDS_STRING3140));
			m_sToBeLeft	= (xml->str(IDS_STRING3141));

			m_sLblSumTrees = (xml->str(IDS_STRING1350));
			m_sLblSumM3Sk = (xml->str(IDS_STRING1410));
			m_sLblSumM3Sk_ha = (xml->str(IDS_STRING1411));
			m_sLblSumM3Fub = (xml->str(IDS_STRING1420));
			m_sLblSumM3Fub_ha = (xml->str(IDS_STRING1421));
			//m_sLblSumM3Ub = (xml->str(IDS_STRING3190));
			m_sLblSumGY = (xml->str(IDS_STRING140));

		}
		delete xml;
	}
}
